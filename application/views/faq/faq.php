<!DOCTYPE html>
<html lang="eng">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <title>topsonia.com - FAQ</title>
    <link rel="icon" href="<?php echo base_url(); ?>asset/faq/images/cart.png" sizes="32x32" />
    <link rel="icon" href="<?php echo base_url(); ?>asset/faq/images/cart.png" sizes="192x192" />
    <link rel="apple-touch-icon" href="<?php echo base_url(); ?>asset/faq/images/cart.png" />
    <meta name="msapplication-TileImage" content="<?php echo base_url(); ?>asset/faq/images/cart.png" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>asset/faq/css/vendor/vendor.min.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>asset/faq/css/style.min.css">

</head>

<body>
    
    <div class="header section">

        <div class="bg-white d-none d-lg-block sticky-nav">
            <div class="container position-relative">
                <div class="row">
                    <div class="col-md-12 align-self-center">
						<div class="main-menu">
							<p><a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>asset/faq/images/logo.png" height="40px" width="auto"></a></p>
						</div>
                    </div>
                </div>
            </div>
        </div>

    </div>


    <div class="offcanvas-overlay"></div>
    <div class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="row breadcrumb_box  align-items-center">
                        <div class="col-sm-12 text-center text-md-center">
                            <h2 class="breadcrumb-title" style="color:#FFFFFF;">Halo!<br>Apa yang bisa kami bantu?</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="blog-grid pb-100px pt-100px main-blog-page single-blog-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 order-lg-last col-md-12 order-md-first">
                    <div class="login-register-area pb-10px pt-10px faq-area">

						<div class="checkout-wrapper">
							<div class="inner-descripe" data-aos="fade-up" data-aos-delay="200">
								<h4 class="title"><?php if($idk !=''){ echo $record4['kategori_faq']; }else{ echo "Frequently Asked Questions (FAQ)"; } ?>
								</h4>
							</div>
							<div id="faq" class="panel-group">
							
							<?php
							 if($idk != '' ){
								if($cek < 1){
									echo "Belum ada pertanyaan mengenai kategori tersebut.";
								}else{
									$no = 1;
								foreach ($record3->result_array() as $rows){
							?>
								<div class="panel panel-default single-my-account" data-aos="fade-up" data-aos-delay="200">
									<div class="panel-heading my-account-title">
										<h3 class="panel-title"><span><?php echo $no; ?> .</span> <a data-bs-toggle="collapse" href="#my-account-<?php echo $no; ?>" class="collapsed" aria-expanded="true"><?php echo $rows['judul_faq']; ?></a></h3>
									</div>
									<div id="my-account-<?php echo $no; ?>" class="collapse show" data-bs-parent="#faq">
										<div class="panel-body">
											<?php echo $rows['isi_faq']; ?>
										</div>
									</div>
								</div>
								<?php
									$no++;
									}
								}
							?>
								
							<?php
							 }else{
								$no = 1;
								foreach ($record->result_array() as $rows){
							?>
								<div class="panel panel-default single-my-account" data-aos="fade-up" data-aos-delay="200">
									<div class="panel-heading my-account-title">
										<h3 class="panel-title"><span><?php echo $no; ?> .</span> <a data-bs-toggle="collapse" href="#my-account-<?php echo $no; ?>" class="collapsed" aria-expanded="false"><?php echo $rows['judul_faq']; ?></a></h3>
									</div>
									<div id="my-account-<?php echo $no; ?>" class="panel-collapse collapse" data-bs-parent="#faq">
										<div class="panel-body">
											<?php echo $rows['isi_faq']; ?>
										</div>
									</div>
								</div>
								<?php
									$no++;
									}
									}
								?>
							</div>
						</div>
					</div>
                </div>
				
                <div class="col-lg-3 order-lg-first col-md-12 order-md-last mt-md-10px mt-lm-10px" data-aos="fade-up" data-aos-delay="200">
                    <div class="left-sidebar shop-sidebar-wrap">
					
                        <!-- <div class="sidebar-widget">
                            <h3 class="sidebar-title mt-0">Cari</h3>
                            <div class="search-widget">
                                <form action="#">
                                    <input placeholder="Cari Topik..." type="text" />
                                    <button type="submit"><i class="icon-magnifier"></i></button>
                                </form>
                            </div>
                        </div> -->
						
						
                        <div class="sidebar-widget">
                            <h3 class="sidebar-title mt-0">Kategori</h3>
                            <div class="category-post">
                                <ul>
								<li><a href="<?php echo base_url() ?>faq" class="selected">Semua Kategori</a></li>
								<?php
									$no_kategori = 1;
									foreach ($record2->result_array() as $row){
								?>
                                    <li><a href="<?php echo base_url() ?>faq?id=<?php echo $row['id_kategori_faq']; ?>" class="selected"><?php echo $row['kategori_faq']; ?></a></li>
								<?php
								$no_kategori++;
									}
								?>
                                </ul>
                            </div>
                        </div>
						
                    </div>
                </div>
				
            </div>
        </div>
    </div>

    <div class="footer-area">
        <div class="footer-container">
            <div class="footer-bottom">
                <div class="container">
                    <div class="row flex-sm-row-reverse">
					<hr>
                        <div class="col-md-12 text-left">
                            <p class="copy-text">2021 Copyright © 2020. All Rights Reserved<br><a class="company-name" href="https://topsonia.com/">
                                    www.topsonia.com</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	
    <script src="<?php echo base_url(); ?>asset/faq/js/vendor/vendor.min.js"></script>
</body>

</html>