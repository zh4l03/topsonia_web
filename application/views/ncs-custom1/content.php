
<link rel="stylesheet" href="<?php echo base_url(); ?>template/<?php echo template(); ?>/bar_rating/themes/fontawesome-stars.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>template/<?php echo template(); ?>/bar_rating/themes/css-stars.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>template/<?php echo template(); ?>/bar_rating/themes/bootstrap-stars.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>template/<?php echo template(); ?>/bar_rating/themes/fontawesome-stars-o.css">

<style>
.MultiCarousel { float: left; overflow: hidden; padding: 15px; width: 97%; position:relative; margin-bottom: 20px;}
.MultiCarousel .MultiCarousel-inner { transition: 1s ease all; float: left; }
.MultiCarousel .MultiCarousel-inner .item { float: left;}
.MultiCarousel .MultiCarousel-inner .item > div { text-align: center; padding:10px; margin:10px; background:#f1f1f1; color:#666;}
.MultiCarousel .leftLst, .MultiCarousel .rightLst { position:absolute; border-radius:50%;top:calc(50% - 20px); }
.MultiCarousel .leftLst { left:0; }
.MultiCarousel .rightLst { right:0; }

.MultiCarousel .leftLst.over, .MultiCarousel .rightLst.over { pointer-events: none; background:#ccc; }
</style>

<script type='text/javascript'>
	

	$(function() { $(window).scroll(function() {

		if($(this).scrollTop()>400) { $('#Back-to-top').fadeIn(); }else { $('#Back-to-top').fadeOut();}});

	$('#Back-to-top').click(function() {

		$('body,html')

		.animate({scrollTop:0},300)

		.animate({scrollTop:40},200)

		.animate({scrollTop:0},130)

		.animate({scrollTop:15},100)

		.animate({scrollTop:0},70);

	});

});
</script>

<div class="paragraph-row">
	<div class="column10">
		<div id="myCarousel" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<?php 
				$ii = 0;
				unset($slider);
				$slider = $this->model_utama->view('iklanatas');
				foreach ($slider->result_array() as $R11) {
					if($ii == 0){
						$aactive = 'active';
					}else{
						$aactive = '';
					}
					?>
					<li data-target="#myCarousel" data-slide-to="<?php echo $ii; ?>" class="<?php echo $aactive; ?>"></li>
					<?php
					$ii++;
				}
				?>
			</ol>
			
			<!-- deklarasi carousel -->
			<div class="carousel-inner" role="listbox">
				<?php 
				$i = 0;
					// unset($slider);
					// $slider = $this->model_utama->view('iklanatas');
				foreach ($slider->result_array() as $R1) {
					if($i == 0){
						$active = 'active';
					}else{
						$active = '';
					}
					?>
					<div class="item <?php echo $active; ?>">
						<a href="<?php echo $R1[url]; ?>"> 
							<img src="<?php echo base_url('asset/foto_iklanatas/'.$R1['gambar']) ?>" style="width:100%" >
						</a>
					</div>
					<?php
					$i++;
				}
				?>
				
				
			</div>
			<!-- membuat panah next dan previous -->
			<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				<span class="sr-only"></span>
			</a>
			<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				<span class="sr-only"></span>	
			</a>
			
		</div>
	</div>
	<!--<div class='column6'>
		<a target='_BLANK' href='$ik1[url]'><img src='".base_url()."asset/foto_iklanatas/$ik1[gambar]' style='width:100%'></a>
	</div>
	<div class='column4' style='margin-left: 1%'>
		<div class='paragraph-row'>
			<div class='column12'>
				<a target='_BLANK' href='$ik2[url]'><img src='".base_url()."asset/foto_iklanatas/$ik2[gambar]' style='width:100%; height: 190px;'></a>
			</div>
		</div>
		<div class='paragraph-row'>
			<div class='column6' style='margin-top:10px'>
				<a target='_BLANK' href='$ik3[url]'><img src='".base_url()."asset/foto_iklanatas/$ik3[gambar]' style='width:100%; height: 180px;'></a>
			</div>
			<div class='column6' style='margin-top:10px'>
				<a target='_BLANK' href='$ik4[url]'><img src='".base_url()."asset/foto_iklanatas/$ik4[gambar]' style='width:100%; height: 180px;'></a>
			</div>
		</div>
	</div>-->
	<?php 
	echo "<div class='column2 hidden-xs' style='margin-left: 1%'>
	<a target='_BLANK' href='$ik6[url]'><img src='".base_url()."asset/foto_iklansidebar/$ik6[gambar]' style='width:100%; min-height: 346px;'></a>
	</div>";
	echo "</div>
	<br>";
	?>
</div>
<br>




<p class='sidebar-title text-danger'>KATEGORI PRODUK</p>
<div class="MultiCarousel" data-items="1,3,5,6" data-slide="1" id="MultiCarousel"  data-interval="1000">
	<div class="MultiCarousel-inner">
		<?php

		$kategori2 = $this->model_utama->view('rb_kategori_produk');

		foreach ($kategori2->result_array() as $kat) {

			if(file_exists(FCPATH."asset/foto_kp/".$kat['foto'])=== false || $kat['foto']==null){
				$gambar = base_url()."asset/foto_kp/null/kosong.png";
			}else{
				$gambar = base_url()."asset/foto_kp/$kat[foto]";
			}
			$seo = $kat['kategori_seo'];
			?>

			<a href="<?=base_url()?>produk/kategori/<?=$seo?>" onclick="pindah('<?=base_url()?>produk/kategori/<?=$seo?>');">
				<div class="item">
					<div class="pad15">
						<center><img style='border:1px solid #cecece; height:85px; width:85px' src='<?=$gambar?>' class='img-circle img-thumbnail'><br><?=$kat['nama_kategori']?>	 
					</center>
				</div>
			</div>
		</a>
	<?php } ?>
	
</div>
<button class="btn btn-primary leftLst"><</button>
<button class="btn btn-primary rightLst">></button>
</div>
<p><br>&nbsp;</p>
<?php

echo "<p><br>&nbsp;</p>";

$no = 1;
$kategori = $this->model_utama->view('rb_kategori_produk');

foreach ($kategori->result_array() as $kat) {
	$produk = $this->model_reseller->produk_perkategori(0,0,$kat['id_kategori_produk'],6);
	$produkk = $this->db->query("SELECT COUNT(id_kategori_produk) AS jml_kat FROM `rb_produk` WHERE id_kategori_produk = '".$kat['id_kategori_produk']."'")->row_array();
	if ($produkk['jml_kat'] < 1) {
		echo "<div class='container'>";
	}else{
		echo "<p class='sidebar-title text-danger'>$kat[nama_kategori]</p>
		<div class='container'>";
	}
	foreach ($produk->result_array() as $row){
		$terjual = $this->db->query("select sum(a.jumlah) as jml from rb_penjualan_detail a join rb_penjualan b on a.id_penjualan=b.id_penjualan where b.selesai='1' and a.id_produk='$row[id_produk]' and b.status_pembeli='konsumen'")->row_array();
		if($terjual['jml']<1){
			$jml_terjual = "<div style='min-height: 20px !important;'></div>";
		}else{
			$jml_terjual = "<div style='min-height: 20px !important;'><i>Terjual ".$terjual['jml']."</i></div>";
		}
		$ex = explode(';', $row['gambar']);
		if (trim($ex[0])==''){ $foto_produk = 'no-image.png'; }else{ $foto_produk = $ex[0]; }
		if (strlen($row['nama_produk']) > 38){ $judul = substr($row['nama_produk'],0,38).',..';  }else{ $judul = $row['nama_produk']; }
		$jual = $this->model_reseller->jual_reseller($row['id_reseller'],$row['id_produk'])->row_array();
		$beli = $this->model_reseller->beli_reseller($row['id_reseller'],$row['id_produk'])->row_array();
		if ($beli['beli']-$jual['jual']<=0){ $stok = '<b style="color:#000">Stok Habis</b>'; }else{ $stok = "<span style='color:green'>Stok ".($beli['beli']-$jual['jual'])." $row[satuan]</span>"; }

		$disk = $this->model_app->view_where("rb_produk_diskon",array('id_produk'=>$row['id_produk']))->row_array();
		$diskon = rupiah(($disk['diskon']/$row['harga_konsumen'])*100,0)."%";
		if ($diskon>0){ $diskon_persen = "<div class='top-right'>$diskon</div>"; }else{ $diskon_persen = ''; }
		if ($diskon>=1){ 
			$harga =  "<del style='color:#8a8a8a'><small>Rp ".rupiah($row['harga_konsumen'])."</small></del> Rp ".rupiah($row['harga_konsumen']-$disk['diskon']);
		}else{
			$harga =  "Rp ".rupiah($row['harga_konsumen']);
		}
		echo "<div class='produk col-md-2 col-xs-6'>
		<center>
		<div style='border: 1px solid whitesmoke;border-radius: 10px;box-shadow: 1px 1px whitesmoke;overflow:hidden;background:transparent;min-height: 290px !important;'>
		<div style='overflow:hidden'>
		<a title='$row[nama_produk]' href='".base_url()."produk/detail/$row[produk_seo]'><img style='width:100%' src='".base_url()."asset/foto_produk/$foto_produk'></a>
		$diskon_persen
		</div>
		<h4 class='produk-title'><a title='$row[nama_produk]' href='".base_url()."produk/detail/$row[produk_seo]'>$judul</a></h4>";
		$rating = $this->db->query("select count(rating) as jml_rating, avg(rating) avg_rating from rb_produk_ulasan where id_produk='$row[id_produk]'")->row_array();
		if($rating['jml_rating']<1){
			echo "<div style='min-height: 30px !important;'></div>";
		}else{    
			echo "<div class='stars stars-example-bootstrap' style='min-height: 30px !important;'>
			<select id='example-bootstrap' name='rating' autocomplete='off'>";
			for ($i=1; $i<=5; $i++){
				if($i<=$rating['avg_rating']){
					echo "<option value='1'>$i</option>";
				}else{
					echo "<option value='2'>$i</option>";
				}
			}
			echo "</select> </div>";
		}
		echo "<span class='harga'>$harga</span><br>
		<i>$stok</i><br> $jml_terjual <small>$row[nama_kota]</small>";
		
		echo "</div></center>
		</div>";

		
	}
	echo "</div>";

	echo "<div style='clear:both'><br></div>";

	$no++;
	
}
?>
<br><br>
<div class="block">
<!--
<div class="block-content">
	<ul class="article-block-big">
		<?php 
			$no = 1;
			$hot = $this->model_utama->view_join_two('berita','users','kategori','username','id_kategori',array('utama' => 'Y','status' => 'Y'),'id_berita','DESC',0,6);
			foreach ($hot->result_array() as $row) {	
			$total_komentar = $this->model_utama->view_where('komentar',array('id_berita' => $row['id_berita']))->num_rows();
			$tgl = tgl_indo($row['tanggal']);
			echo "<li style='width:180px'>
					<div class='article-photo'>
						<a href='".base_url()."$row[judul_seo]' class='hover-effect'>";
							if ($row['gambar'] ==''){
								echo "<a class='hover-effect' href='".base_url()."$row[judul_seo]'><img style='height:110px; width:200px' src='".base_url()."asset/foto_berita/no-image.jpg' alt='' /></a>";
							}else{
								echo "<a class='hover-effect' href='".base_url()."$row[judul_seo]'><img style='height:110px; width:200px' src='".base_url()."/asset/foto_berita/$row[gambar]' alt='' /></a>";
							}
					echo "</a>
					</div>
					<div class='article-content'>
						<h4><a href='".base_url()."$row[judul_seo]'>$row[judul]</a><a href='".base_url()."$row[judul_seo].html' class='h-comment'>$total_komentar</a></h4>
						<span class='meta'>
							<a href='".base_url()."$row[judul_seo]'><span class='icon-text'>&#128340;</span>$row[jam], $tgl</a>
						</span>
					</div>
				  </li>";
			}
		?>
	</ul>
</div>
-->

</div>
<div class="container">		
	<div class="col-md-8 col-md-offset-2">
		
	</div>
</div>

<script type="text/javascript" src="<?php echo base_url(); ?>template/<?php echo template(); ?>/bar_rating/jquery.barrating.min.js"></script>

<script type="text/javascript">
	$(window).load(function() {
		$("[id=example-bootstrap]").barrating({
			theme: 'bootstrap-stars',
			showSelectedRating: false,
			readonly: true,
		});
	});
</script>

<script>
	$(document).ready(function () {
		var itemsMainDiv = ('.MultiCarousel');
		var itemsDiv = ('.MultiCarousel-inner');
		var itemWidth = "";

		$('.leftLst, .rightLst').click(function () {
			var condition = $(this).hasClass("leftLst");
			if (condition)
				click(0, this);
			else
				click(1, this)
		});

		ResCarouselSize();




		$(window).resize(function () {
			ResCarouselSize();
		});

    //this function define the size of the items
    function ResCarouselSize() {
    	var incno = 0;
    	var dataItems = ("data-items");
    	var itemClass = ('.item');
    	var id = 0;
    	var btnParentSb = '';
    	var itemsSplit = '';
    	var sampwidth = $(itemsMainDiv).width();
    	var bodyWidth = $('body').width();
    	$(itemsDiv).each(function () {
    		id = id + 1;
    		var itemNumbers = $(this).find(itemClass).length;
    		btnParentSb = $(this).parent().attr(dataItems);
    		itemsSplit = btnParentSb.split(',');
    		$(this).parent().attr("id", "MultiCarousel" + id);


    		if (bodyWidth >= 1200) {
    			incno = itemsSplit[3];
    			itemWidth = sampwidth / incno;
    		}
    		else if (bodyWidth >= 992) {
    			incno = itemsSplit[2];
    			itemWidth = sampwidth / incno;
    		}
    		else if (bodyWidth >= 768) {
    			incno = itemsSplit[1];
    			itemWidth = sampwidth / incno;
    		}
    		else {
    			incno = itemsSplit[0];
    			itemWidth = sampwidth / incno;
    		}
    		$(this).css({ 'transform': 'translateX(0px)', 'width': itemWidth * itemNumbers });
    		$(this).find(itemClass).each(function () {
    			$(this).outerWidth(itemWidth);
    		});

    		$(".leftLst").addClass("over");
    		$(".rightLst").removeClass("over");

    	});
    }


    //this function used to move the items
    function ResCarousel(e, el, s) {
    	var leftBtn = ('.leftLst');
    	var rightBtn = ('.rightLst');
    	var translateXval = '';
    	var divStyle = $(el + ' ' + itemsDiv).css('transform');
    	var values = divStyle.match(/-?[\d\.]+/g);
    	var xds = Math.abs(values[4]);
    	if (e == 0) {
    		translateXval = parseInt(xds) - parseInt(itemWidth * s);
    		$(el + ' ' + rightBtn).removeClass("over");

    		if (translateXval <= itemWidth / 2) {
    			translateXval = 0;
    			$(el + ' ' + leftBtn).addClass("over");
    		}
    	}
    	else if (e == 1) {
    		var itemsCondition = $(el).find(itemsDiv).width() - $(el).width();
    		translateXval = parseInt(xds) + parseInt(itemWidth * s);
    		$(el + ' ' + leftBtn).removeClass("over");

    		if (translateXval >= itemsCondition - itemWidth / 2) {
    			translateXval = itemsCondition;
    			$(el + ' ' + rightBtn).addClass("over");
    		}
    	}
    	$(el + ' ' + itemsDiv).css('transform', 'translateX(' + -translateXval + 'px)');
    }

    //It is used to get some elements from btn
    function click(ell, ee) {
    	var Parent = "#" + $(ee).parent().attr("id");
    	var slide = $(Parent).attr("data-slide");
    	ResCarousel(ell, Parent, slide);
    }

});	
</script>

<script>
	function pindah(halaman){
		window.location.href = halaman;
	}
</script>