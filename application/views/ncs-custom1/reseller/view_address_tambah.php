<?php 
  echo "<p class='sidebar-title text-danger produk-title'> Tambah Alamat</p>";                
		  $attributes = array('id' => 'formku','class'=>'form-horizontal','role'=>'form');
		  echo form_open_multipart('members/tambah_alamat/'.$idp.'/'.$back,$attributes); 
		  $ko = $this->db->query("SELECT * FROM rb_kota where kota_id='$rows[kota_id]'")->row_array();
		   echo "
			<div class='row'>
				<div class='col-md-6'>
				<table class='table table-hover table-condensed'>
					<thead>
					</thead>
					<tbody>
						<tr>
							<td><b>Nama Alamat</b></td>   
							<td><input class='required form-control' type='text' name='a' placeholder='Rumah, toko, gedung, sekolah dll' width='100px'></td>
						</tr>
						<tr>
							<td><b>PIC</b></td>   
							<td><input class='required form-control' type='text' name='f' placeholder='Person in contact' width='100px' required></td>
						</tr>
						<tr>
							<th><b>Alamat Pengiriman</b></th>         
							<td><textarea class='required form-control' style='width:93%' name='b' placeholder='Alamat Pengiriman' required></textarea></td>
						</tr>
						<tr>
							<th scope='row'>Provinsi</th>                    
							<td>
								<select class='form-control' name='ag' id='state_reseller' required>
									<option value=''>- Pilih -</option>";
										foreach ($provinsi as $rows) {
										  if ($ko['provinsi_id']==$rows['provinsi_id']){
											echo "<option value='$rows[provinsi_id]' selected>$rows[nama_provinsi]</option>";
										  }else{
											echo "<option value='$rows[provinsi_id]'>$rows[nama_provinsi]</option>";
										  }
										}
								echo "</select>
							</td>
						</tr>
						<tr>
							<th scope='row'>Kota</th>                         
							<td><select class='form-control' name='ga' id='city_reseller' required>
									<option value=''>- Pilih -</option>";
										$kota = $this->model_app->view_where_ordering('rb_kota',array('provinsi_id'=>$ko['provinsi_id']),'kota_id','DESC');
										foreach ($kota as $rows) {
										  if ($ko['kota_id']==$rows['kota_id']){
											echo "<option value='$rows[kota_id]' selected>$rows[nama_kota]</option>";
										  }else{
											echo "<option value='$rows[kota_id]'>$rows[nama_kota]</option>";
										  }
										}
								echo "</select>
							</td
						</tr>
						<tr>
							<th><b>Kecamatan</b></th>  
							<td><input type='text' class='required form-control' name='c' placeholder='Kecamatan' required></td>
						</tr>
						<tr>
							<th><b>Kode Pos</b></th>                  
							<td><input class='required number form-control' type='number' name='d' placeholder='Kode pos' required></td>
						</tr>
						<tr>
							<th><b>No Hp</b></th>                  
							<td><input class='required number form-control' type='number' name='e' placeholder='Telpon' required></td>
						</tr>

					</tbody>
					<tr><br/><td></td><td><br/><input class='btn btn-sm btn-primary' type='submit' name='submit' value='Tambah Alamat	'>  <a class='btn btn-sm btn-default' href='$url' name='submit'>Kembali</a></td>
						 </tr>
				</table>
			</div>
		</div>";
                  echo form_close();
?>