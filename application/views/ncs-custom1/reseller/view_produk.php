<script type="text/javascript" src="<?php echo base_url(); ?>template/<?php echo template(); ?>/jscript/jquery.mask.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>template/<?php echo template(); ?>/bar_rating/themes/fontawesome-stars.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>template/<?php echo template(); ?>/bar_rating/themes/css-stars.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>template/<?php echo template(); ?>/bar_rating/themes/bootstrap-stars.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>template/<?php echo template(); ?>/bar_rating/themes/fontawesome-stars-o.css">
 
  <div class='container' >
<?php
if($this->input->post('kata')!=null){ ?>
  <input type='hidden' value="<?php echo $this->input->post('kata');?>" id="cari">
<?php }else{ ?>
  <input type='hidden' value="<?php echo $this->input->get('kata');?>" id="cari">
<?php } ?>
  
  
  <div align="center">          
    <table border="0" width="100%">
      <tr>
        <td style="text-align: left;" width="auto"><br>
          <!-- <div align="center"> -->
            <!-- <div class="dropdown"> 
              <h2 class="btn btn-secondary"><label>Filter  &nbsp;&nbsp; : &nbsp;&nbsp;</label></h2>
              <!-- </div> -->
            </td>

            <td><br>
              <div class="dropdown">
                <button class="btn btn-default dropdown-toggle">Lokasi &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-chevron-down on-opened"></span></button>
                <div class="dropdown-kota">
                  <br/>
                  <input type="text" id="searchkota" value="" name="searchkota" placeholder="Cari" style="width: 245px;"/>
                  <br/>
                  <div class="padding-v-xs" id='results' >
                    <label class="btn btn-default btn-xs ico">
                      <input type="checkbox" onclick='select_all_kota()' id="select_all_kota" />
                      </label>
                    <span>Select All</span> &nbsp;&nbsp;

                    <label class="btn btn-default btn-xs ico">
                      <input type="checkbox" onclick='unselect_all_kota()' id="unselect_all_kota" />
                      </label>
                    <span>Unselect All</span>

                  </div>
                  <br/>

                  <div id="coll-2" class="scroll-v-250px collapse in">
                    <?php    
                    foreach ($kota->result_array() as $row){ ?>
                      <div id="results_kota">
                        <div class="padding-v-xs" id='results' >
                          <label class="btn btn-default btn-xs ico">
                            <!-- <?php $cek = explode("','",$this->input->get('kota')); ?> -->
                            <input type="checkbox" id="kota" class="kota" name="kota" onclick="filter_kota()" value="<?php echo $row["kota_id"]; ?>"  autocomplete="off"
                            <?php
                            $cek = explode("','",$this->input->get('kota'));
                            foreach ($cek as &$val) {
                              if($row["kota_id"]==$val){
                                echo "checked";
                              }
                            }?>/>
                           </label>
                          <span><?php echo $row["nama_kota"]; ?></span>
                        </div>
                      </div>
                    <?php } ?>
                  </div>
                </div>
                <!-- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->
              </div>

              <div class="dropdown">
                <button class="btn btn-default dropdown-toggle">Harga &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-chevron-down on-opened"></span></button>
                <div class="dropdown-content">
                  <div class="filter-setting">
                    <br/>
                    <h3 style="text-align:center">Rentang Harga</h3> 
                    <p align="center">           
                      <input type="text" name="min" id="min" class="min" value="<?php echo trim($this->input->get('minimal_harga')); ?>" placeholder="RP MIN" style="width: 240px;"/> 
                    </p>
                    <p style="font-size: 31px; text-align:center;">
                      -
                    </p>
                    <p align="center">
                      <input type="text" name="max" id="max" class="max" value="<?php echo trim($this->input->get('maximal_harga')); ?>" placeholder="RP MAKS" style="width: 240px;"/> 
                    </p> 
                    <p align="center">
                      <button type="button" id="filter_harga" class="btn btn-block btn-warning">
                        <span class="">Terapkan</span>
                      </button> 
                    </p>      
                  </div>
                </div>
                <!-- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->
              </div>

              <div class="dropdown">
                <button class="btn btn-default dropdown-toggle">Merchant &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-chevron-down on-opened"></span></button>
                <div class="dropdown-reseller">
                  <br/>
                  <input type="text" id="searchmerchant" value="" name="searchmerchant" placeholder="Cari"/>
                  <br/>
                  <div class="padding-v-xs" id='results' >
                    <label class="btn btn-default btn-xs ico">
                      <input type="checkbox" onclick='select_all_reseller()' id="select_all_reseller" />
                    </label>
                    <span>Select All</span> &nbsp;&nbsp;

                    <label class="btn btn-default btn-xs ico">
                      <input type="checkbox" onclick='unselect_all_reseller()' id="unselect_all_reseller" />
                    </label>
                    <span>Unselect All</span>

                  </div>
                  <br/>     
                  <div id="coll-2" class="scroll-v-250px collapse in">
                    <?php
                    foreach ($reseller->result_array() as $row){ ?>
                      <div id="results_merchant">
                        <div class="padding-v-xs" id='results' >
                          <label class="btn btn-default btn-xs ico">
                            <input type="checkbox" id="reseller" class="reseller" name="reseller" onclick="filter_reseller()" value="<?php echo $row["id_reseller"]; ?>"  autocomplete="off"
                            <?php
                            $cek = explode("','",$this->input->get('reseller'));
                            foreach ($cek as &$val) {
                              if($row["id_reseller"]==$val){
                                echo "checked";
                              }
                            }?>/>
                            </label>
                          <span><?php echo $row["nama_reseller"]; ?></span>
                        </div>
                      </div>
                    <?php } ?>
                  </div>
                </div>
                <!-- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->
              </div>

              <div class="dropdown">
                <button class="btn btn-default dropdown-toggle">Satuan &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-chevron-down on-opened"></span></button>
                <div class="dropdown-satuan">
                  <!-- <input type="text" id="searchsatuan" value="" name="searchsatuan"/> -->
                  <div id="coll-2" class="">
                    <br/>
                    <?php
                    foreach ($satuan->result_array() as $row){ ?>
                      <div id="results_satuan">
                        <div class="padding-v-xs" id='results' >
                          <label class="btn btn-default btn-xs ico">
                            <input type="checkbox" id="satuan" class="satuan" name="satuan" onclick="filter_satuan()" value="<?php echo $row["nama_produk_satuan"]; ?>"  autocomplete="off"
                            <?php
                            $cek = explode("','",$this->input->get('satuan'));
                            foreach ($cek as &$val) {
                              if($row["nama_produk_satuan"]==$val){
                                echo "checked";
                              }
                            }?>/>
                           </label>
                          <span><?php echo strtoupper($row["nama_produk_satuan"]); ?></span>
                        </div>
                      </div>
                    <?php } ?>
                  </div>
                </div>
              </div>

              <div class="dropdown">
                <button class="btn btn-default dropdown-toggle">Sort By &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-chevron-down on-opened"></span></button>
                <div class="dropdown-sort-by">
                  <!-- <input type="text" id="searchsatuan" value="" name="searchsatuan"/> -->
                  <div id="coll-2" class="">
                    <br/>
                      <div id="results_satuan">
                        <div class="padding-v-xs" id='results' >

                          <label class="btn btn-default btn-xs ico">
                            <input type="radio" id="sort_by" name="sort_by" onclick="filter();" value="" <?php if($this->input->get('sort_by')==''){ echo 'checked'; } ?>  autocomplete="off"/>
                           </label>
                          <span></span>Terbaru
                          <br/>

                          <label class="btn btn-default btn-xs ico">
                            <input type="radio" id="sort_by" name="sort_by" onclick="filter();" value="a.harga_konsumen ASC," <?php if($this->input->get('sort_by')=='a.harga_konsumen ASC,'){ echo 'checked'; } ?>  autocomplete="off"/>
                           </label>
                          <span></span>Harga Terendah
                          <br/>

                          <label class="btn btn-default btn-xs ico">
                            <input type="radio" id="sort_by" name="sort_by" onclick="filter();" value="a.harga_konsumen DESC," <?php if($this->input->get('sort_by')=='a.harga_konsumen DESC,'){ echo 'checked'; } ?>  autocomplete="off"/>
                           </label>
                          <span></span>Harga Tertinggi
                          <br/>

                        </div>
                      </div>
                    
                  </div>
                </div>
              </div>

            </td>

          </tr>
        </table>
      </div>
    
 

  
  </div>
  <br/>
  <br/>
  
  <div id="refresh_produk">
  
  <?php 
    $no = 1;
    if (($this->input->post('kata'))){
      echo "<p class='sidebar-title text-danger produk-title'>$title</p>";
    }
      echo "<div class='container'>";
      foreach ($record->result_array() as $row){
      $terjual = $this->db->query("select sum(a.jumlah) as jml from rb_penjualan_detail a join rb_penjualan b on a.id_penjualan=b.id_penjualan where b.selesai='1' and a.id_produk='$row[id_produk]' and b.status_pembeli='konsumen'")->row_array();
      if($terjual['jml']<1){
        $jml_terjual = "<div style='min-height: 20px !important;'></div>";
      }else{
        $jml_terjual = "<div style='min-height: 20px !important;'><i>Terjual ".$terjual['jml']."</i></div>";
      }
      $ex = explode(';', $row['gambar']);
      if (trim($ex[0])==''){ $foto_produk = 'no-image.png'; }else{ $foto_produk = $ex[0]; }
      if (strlen($row['nama_produk']) > 25){ $judul = substr($row['nama_produk'],0,25).',..';  }else{ $judul = $row['nama_produk']; }
      $jual = $this->model_reseller->jual_reseller($row['id_reseller'],$row['id_produk'])->row_array();
      $beli = $this->model_reseller->beli_reseller($row['id_reseller'],$row['id_produk'])->row_array();
      if ($beli['beli']-$jual['jual']<=0){ $stok = '<b style="color:000">Stok Habis</b>'; }else{ $stok = "<span style='color:green'>Stok ".($beli['beli']-$jual['jual'])." $row[satuan]</span>"; }
  
      $disk = $this->db->query("SELECT * FROM rb_produk_diskon where id_produk='$row[id_produk]'")->row_array();
      $diskon = rupiah(($disk['diskon']/$row['harga_konsumen'])*100,0)."%";
      if ($diskon>0){ $diskon_persen = "<div class='top-right'>$diskon</div>"; }else{ $diskon_persen = ''; }
      if ($diskon>=1){ 
        $harga =  "<del style='color:red'><small>Rp ".rupiah($row['harga_konsumen'])."</small></del> Rp ".rupiah($row['harga_konsumen']-$disk['diskon']);
      }else{
        $harga =  "Rp ".rupiah($row['harga_konsumen']);
      }
      echo "<div class='produk col-md-2 col-xs-12'>
                <center>
        <div style='border: 1px solid whitesmoke;border-radius: 10px;box-shadow: 1px 1px whitesmoke;overflow:hidden;background:transparent;min-height: 290px !important;'>
                  <div style='overflow:hidden'>
                    <a title='$row[nama_produk]' href='".base_url()."produk/detail/$row[produk_seo]'><img style='width:99%' src='".base_url()."asset/foto_produk/$foto_produk'></a>
                  $diskon_persen
                </div>
                
                <h4 class='produk-title produk-title-list'><a title='$row[nama_produk]' href='".base_url()."produk/detail/$row[produk_seo]'>$judul</a></h4>";
                
                $rating = $this->db->query("select count(rating) as jml_rating, avg(rating) avg_rating from rb_produk_ulasan where id_produk='$row[id_produk]'")->row_array();
                if($rating['jml_rating']<1){
                echo "<div style='min-height: 30px !important;'>
                    
                    </div>";
                }else{    
                echo "<div class='stars stars-example-bootstrap' style='min-height: 30px !important;'>
                <select id='example-bootstrap' name='rating' autocomplete='off'>";
                    for ($i=1; $i<=5; $i++){
                        if($i<=$rating['avg_rating']){
                            echo "<option value='1'>$i</option>";
                        }else{
                            echo "<option value='2'>$i</option>";
                        }
                    }
                echo "</select> </div>";
                }

                echo "<span style='color:black;'>$harga</span><br>
                  <i>$stok</i><br> $jml_terjual <small>$row[nama_kota]</small>
                </div></center><br>
            </div>";
  
        $no++;
      }
      echo "</div>
      <div class='pagination'>";
           echo $this->pagination->create_links();
      echo "</div>
    <div style='clear:both'><br></div>";
   ?>
   </div>
  
  <style>
  .collapsed-icon-toggle.collapsed .on-opened {
    display: none;
  }
  .collapsed-icon-toggle.collapsed .on-closed {
    display: initial;
  }
  
  .collapsed-icon-toggle .on-closed {
    display: none;
  }
  .collapsed-icon-toggle .on-opened {
    display: initial;
  }
  .scroll-v-250px {
    max-height: 200px;
    overflow-y: scroll;
  }
  .padding-v-xs {
    padding-top: 2px;
    padding-bottom: 2px;
  }
  .btn.ico span.icon {
    opacity: 0;
  }
  .btn.ico.active span.icon {
    opacity: 1;
  }
  </style>
  
  <style>
  .dropbtn {
    background-color: #4CAF50;
    color: white;
    padding: 16px;
    font-size: 16px;
    border: none;
  }
  
  .dropdown {
    position: relative;
    display: inline-block;
    
  }
  
  .dropdown-content {
    display: none;
    position: absolute;
    background-color: #f1f1f1;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
    width: 250px !important;
    height: 250px !important;
  }

  .dropdown-kota {
    display: none;
    position: absolute;
    background-color: #f1f1f1;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
    width: 350px !important;
    height: 330px !important;
  }

  .dropdown-reseller {
    display: none;
    position: absolute;
    background-color: #f1f1f1;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
    width: 250px !important;
    height: 330px !important;
  }

  .dropdown-satuan {
    display: none;
    position: absolute;
    background-color: #f1f1f1;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
    width: 150px !important;
    height: 180px !important;
  }

  .dropdown-sort-by {
    display: none;
    position: absolute;
    background-color: #f1f1f1;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
    width: 140px !important;
    height: 110px !important;
  }
  
  
  .dropdown-content a {
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
  }
  
  .dropdown-content a:hover {background-color: #ddd;}
  
  .dropdown:hover .dropdown-content {display: block;}

  .dropdown:hover .dropdown-satuan {display: block;}

  .dropdown:hover .dropdown-sort-by {display: block;}

  .dropdown:hover .dropdown-kota {display: block;}

  .dropdown:hover .dropdown-reseller {display: block;}
  
  .dropdown:hover .dropbtn {background-color: #3e8e41;}
  </style>
  
  <script>          
  $("#searchkota").keyup(function() {
  var filter = $(this).val(),
    count = 0;
  $('#results_kota div').each(function() {
    if ($(this).text().search(new RegExp(filter, "i")) < 0) {
      $(this).hide();  
    } else {
      $(this).show();
      count++;
    }
  });
  });

  $("#searchmerchant").keyup(function() {
  var filter = $(this).val(),
    count = 0;
  $('#results_merchant div').each(function() {
    if ($(this).text().search(new RegExp(filter, "i")) < 0) {
      $(this).hide();  
    } else {
      $(this).show();
      count++;
    }
  });
  });

  
  </script>
  
  <script>

    function filter(){
      
      minimal = document.getElementById("min").value;
      maximal = document.getElementById("max").value;
      kota = $('input:checkbox:checked.kota').map(function(){return this.value; }).get().join("','");
      reseller = $('input:checkbox:checked.reseller').map(function(){return this.value; }).get().join("','");  
      satuan = $('input:checkbox:checked.satuan').map(function(){return this.value; }).get().join("','");  
      sort_by = document.querySelector('input[name="sort_by"]:checked').value;
      $.ajax({
          type: "GET",
          data: { minimal_harga : minimal, 
                  maximal_harga: maximal, 
                  kota : kota,
                  filter : 'aktif',
                  reseller : reseller,
                  cari : document.getElementById("cari").value,
                  satuan: satuan,
                  sort_by: sort_by,
                },
          url: '<?php echo base_url(); ?>produk/index',
          dataType: 'JSON',
          
          success: function(response) {
          $('#refresh_produk').html(response);
          $("[id=example-bootstrap]").barrating({
              theme: 'bootstrap-stars',
              showSelectedRating: false,
              readonly: true,
            });
          }
        });
    }

    document.getElementById('filter_harga').onclick = function(){
        filter();
    }
    
    function check(all,name) {
      var ele=document.getElementsByName(name);  
        for(var i=0; i<ele.length; i++){  
          if(ele[i].type=='checkbox')  
              ele[i].checked=all;              
          }  
    }

    function filter_kota(){
      filter();
      document.getElementById("unselect_all_kota").checked = false
      document.getElementById("select_all_kota").checked = false
    }

    function select_all_kota(){
      check(true,'kota');
      document.getElementById("unselect_all_kota").checked = false
      filter();
    }

    function unselect_all_kota(){
      check(false,'kota');
      document.getElementById("select_all_kota").checked = false
      filter();
    }

    function filter_reseller(){
      filter();
      document.getElementById("unselect_all_reseller").checked = false
      document.getElementById("select_all_reseller").checked = false
    }

    function select_all_reseller(){
      check(true,'reseller');
      document.getElementById("unselect_all_reseller").checked = false
      filter();
    }

    function unselect_all_reseller(){
      check(false,'reseller');
      document.getElementById("select_all_reseller").checked = false
      filter();
    }

    function filter_satuan(){
      filter();
    } 

   
</script>

<script type="text/javascript">
            $(document).ready(function(){
                // Format mata uang.
                $( '.min' ).mask('000.000.000.000.000.000', {reverse: true});
                $( '.max' ).mask('000.000.000.000.000.000', {reverse: true});

               
            })
        
</script>

<script type="text/javascript" src="<?php echo base_url(); ?>template/<?php echo template(); ?>/bar_rating/jquery.barrating.min.js"></script>

<script type="text/javascript">
$(window).load(function() {
    $("[id=example-bootstrap]").barrating({
              theme: 'bootstrap-stars',
              showSelectedRating: false,
              readonly: true,
            });
});
</script>

<!-- <script>
var max = document.getElementById("max");
max.addEventListener("keyup", function(e) {
  max.value = formatmax(this.value, "Rp. ");
});
function formatmax(angka, prefix) {
  var number_string = angka.replace(/[^,\d]/g, "").toString(),
    split = number_string.split(","),
    sisa = split[0].length % 3,
    max = split[0].substr(0, sisa),
    ribuan = split[0].substr(sisa).match(/\d{3}/gi);
  if (ribuan) {
    separator = sisa ? "." : "";
    max += separator + ribuan.join(".");
  }
  max = split[1] != undefined ? max + "," + split[1] : max;
  return prefix == undefined ? max : max ? "Rp. " + max : "";
}
</script> -->