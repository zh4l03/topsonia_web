<?php 
echo "<p class='sidebar-title text-danger produk-title'> Edit Alamat</p>";                
$attributes = array('id' => 'formku','class'=>'form-horizontal','role'=>'form');
echo form_open_multipart('members/edit_address/'.$row['id_alamat'].'/'.$idp.'/'.$back,$attributes); 
$ko = $this->db->query("SELECT * FROM rb_kota where kota_id='$row[kota_id]'")->row_array();
echo "
<div class='row'>
<div class='col-md-6'>
<table class='table table-hover table-condensed'>
<thead>
</thead>
<tbody>
<tr>
<td><b>Nama Alamat</b></td>   
<td><input class='required form-control' type='text' name='a' value='$row[nama_alamat]' width='100px' placeholder='Rumah, toko, gedung, sekolah dll'></td>
</tr>
<tr>
<td><b>PIC</b></td>   
<td><input class='required form-control' type='text' name='f' value='$row[pic]' width='100px' placeholder='Person in contact'></td>
</tr>
<tr>
<th><b>Alamat Pengiriman</b></th>         
<td><textarea class='required form-control' style='width:93%' name='b' placeholder='Alamat lengkap' required>$row[alamat_lengkap]</textarea></td>
</tr>
<tr>
<th scope='row'>Provinsi</th>                    
<td>
<select class='form-control' name='ag' id='state_reseller' required>
<option value=''>- Pilih -</option>";
foreach ($provinsi as $rows) {
	if ($ko['provinsi_id']==$rows['provinsi_id']){
		echo "<option value='$rows[provinsi_id]' selected>$rows[nama_provinsi]</option>";
	}else{
		echo "<option value='$rows[provinsi_id]'>$rows[nama_provinsi]</option>";
	}
}
echo "</select>
</td>
</tr>
<tr>
<th scope='row'>Kota</th>                         
<td><select class='form-control' name='ga' id='city_reseller' required>
<option value=''>- Pilih -</option>";
$kota = $this->model_app->view_where_ordering('rb_kota',array('provinsi_id'=>$ko['provinsi_id']),'kota_id','DESC');
foreach ($kota as $rows) {
	if ($ko['kota_id']==$rows['kota_id']){
		echo "<option value='$rows[kota_id]' selected>$rows[nama_kota]</option>";
	}else{
		echo "<option value='$rows[kota_id]'>$rows[nama_kota]</option>";
	}
}
echo "</select>
</td
</tr>
<tr>
<th><b>Kecamatan</b></th>  
<td><input type='text' class='required form-control' name='c' value='$row[kecamatan]' placeholder='Kecamatan' required></td>
</tr>
<tr>
<th><b>Kode Pos</b></th>                  
<td><input class='required number form-control' type='number' name='d' value='$row[kode_pos]' placeholder='Kode pos' required></td>
</tr>
<tr>
<th><b>No Hp</b></th>                  
<td><input class='required number form-control' type='number' name='e' value='$row[no_hp]' placeholder='telpon' required></td>
</tr>

</tbody>
<tr><br/><td></td><td><input class='btn btn-sm btn-primary' type='submit' name='submit' value='Simpan Perubahan'>  <a class='btn btn-sm btn-default' href='$url' name='submit'>Kembali</a></td>
</tr>
</table>
</div>
</div>";
echo form_close();
?>