
<?php
echo"<a class='pull-right btn btn-default btn-sm' href='".base_url()."members/orders_report'>Kembali</a>";
?>
<p class='sidebar-title text-danger produk-title'> Detail Pesanan Anda</p>
<div class="col-sm-12">
	<p ><span class="glyphicon glyphicon-map-marker"/>Alamat Pengiriman</p>
	<?php 		$res = $this->db->query("SELECT a.*, b.nama_kota, c.nama_provinsi FROM rb_reseller a JOIN rb_kota b ON a.kota_id=b.kota_id 
		JOIN rb_provinsi c ON b.provinsi_id=c.provinsi_id where a.id_reseller='".$rows['id_reseller']."'")->row_array(); 
		?>
<!--<table class='table table-condensed'>
<tbody>
<tr><th scope='row' style='width:90px'>Pengirim</th> <td><strong><?php echo $res['nama_reseller']?></strong></td></tr>
<tr><th scope='row'>No Telpon</th> <td><?php echo $res['no_telpon']; ?></td></tr>
<tr><th scope='row'>Alamat</th> <td><?php echo $res['alamat_lengkap'].', '.$res['nama_kota'].', '.$res['nama_provinsi']; ?></td></tr>
</tbody>
</table>-->

<?php 		$usr = $this->db->query("SELECT a.*, b.nama_kota, c.nama_provinsi FROM rb_konsumen a JOIN rb_kota b ON a.kota_id=b.kota_id 
	JOIN rb_provinsi c ON b.provinsi_id=c.provinsi_id where a.id_konsumen='".$this->session->id_konsumen."'")->row_array(); 

if ($rows['id_alamat']!=''){
	$al = $this->model_app->view_where('alamat',array('id_alamat'=>$rows['id_alamat']))->row_array(); 
	$als = $this->db->query("SELECT a.*, b.nama_kota, c.nama_provinsi FROM alamat a JOIN rb_kota b ON a.kota_id=b.kota_id
		JOIN rb_provinsi c ON b.provinsi_id=c.provinsi_id
		where a.id_alamat='".$rows['id_alamat']."'")->row_array(); 
		?>
		
		<table class='table table-condensed'>
			<tbody>
				<tr><th scope='row' style='width:90px'>Penerima</th> <td><strong><?php echo $al['pic']?></strong> (<?php echo $al['nama_alamat']?>)</td></tr>
				<tr><th scope='row'>No Telpon</th> <td><?php echo $al['no_hp']; ?></td></tr>
				<tr><th scope='row'>Alamat</th> <td><?php echo $al['alamat_lengkap'].', '.$als['nama_kota'].', '.$als['nama_provinsi'].', '.$als['kecamatan'].', '.$als['kode_pos']; ?></td></tr>
			</tbody>
		</table>
	<?php }else{ ?>
		
		<table class='table table-condensed'>
			<tbody>
				<tr class=''><th scope='row' style='width:90px'>Penerima</th> <td><?php echo $usr['nama_lengkap']?></td></tr>
				<tr><th scope='row'>No Telpon</th> <td><?php echo $usr['no_hp']; ?></td></tr>
				<tr><th scope='row'>Alamat</th> <td><?php echo $usr['alamat_lengkap'].', '.$usr['nama_kota'].', '.$usr['nama_provinsi'].', '.$usr['kecamatan'].', '.$usr['kode_pos']; ?></td></tr>
			</tbody>
		</table>
	<?php } ?>
</div>

<div class="col-sm-12">
	<table class="table table-striped table-condensed">
		<tbody>
			<?php 
			$no = 1;
			$total_berat = 0;
			$total_belanja = 0;
			$total_diskon = 0;
			$total_ongkir = 0;
			$diskon_ongkir = 0;
			$total_asuransi = 0;
			foreach ($record->result_array() as $ro){
				
				echo "<tr><td colspan='2'>$ro[nama_reseller]</td></tr>";
				$produk = $this->model_app->view_join_where('rb_penjualan_detail','rb_produk','id_produk',array('id_penjualan'=>$ro['id_penjualan']),'id_penjualan_detail','ASC');	
				foreach($produk as $row){
					$ex = explode(';', $row['gambar']);
					if (trim($ex[0])==''){ $foto_produk = 'no-image.png'; }else{ $foto_produk = $ex[0]; }
					$sub_total = ($row['harga_jual']*$row['jumlah'])-$row['diskon'];
					echo "<tr><td>$no</td>
					<td width='70px'><img style='border:1px solid #cecece; width:60px' src='".base_url()."asset/foto_produk/$foto_produk'></td>
					<td><a style='color:#ab0534' href='".base_url()."produk/detail/$row[produk_seo]'><b>$row[nama_produk]</b></a>
					<br>Qty. <b>$row[jumlah]</b>, Harga. Rp ".rupiah($row['harga_jual']-$row['diskon'])." / $row[satuan], 
					<br>Berat. <b>".($row['berat']*$row['jumlah'])." Gram</b></td>
					<td>Rp ".rupiah($sub_total)."</td>
					</tr>";
					$no++;
					$total_berat = $total_berat + ($row['berat']*$row['jumlah']);
					$total_belanja =  $total_belanja + ($row['harga_jual']*$row['jumlah']);
				}
				$total_ongkir = $total_ongkir + $ro['ongkir'];
				$total_diskon =  $total_diskon + $ro['diskon']+$ro['diskon_buyer'];
				$diskon_ongkir = $diskon_ongkir + $ro['diskon_ongkir'];
				$total_asuransi = $total_asuransi + $ro['asuransi'];
				$no=1;
			}
			$detail = $this->db->query("SELECT * FROM rb_penjualan where kode_transaksi='".$this->uri->segment(3)."'")->row_array();
// $total = $this->db->query("SELECT sum((a.harga_jual*a.jumlah)-a.diskon) as total, sum(b.berat*a.jumlah) as total_berat, sum(a.diskon_topsonia) as disko FROM `rb_penjualan_detail` a JOIN rb_produk b ON a.id_produk=b.id_produk where a.kode_transaksi='".$this->uri->segment(3)."'")->row_array();

			if((($rows['bayar'] == '1' || $rows['bayar'] == '2') && $rows['status_pembayaran']=='gagal') || $rows['batal'] == '1'){
				$proses = '<i class="text-danger">Transaksi Dibatalkan</i>';
				if($rows['via']=='midtrans'){
					$statusproses = "<b>Note: Waktu Pembayaran Sudah Habis.</b><br>Silahkan Memilih Produk Kembali <a class='btn btn-info btn-xs' title='Detail data pesanan' href='".base_url()."produk'>Lanjut Belanja</a>";
				}else{
					$statusproses = "<b>Note: ".$rows['remarks'].".</b><br>Silahkan melanjutkan ke proses <a class='btn btn-info btn-xs' title='Detail data pesanan' href='".base_url()."konfirmasi/refund?idp=$row[id_penjualan]'>Refund</a>";
				}
			}elseif(($rows['bayar'] == '1' || $rows['bayar'] == '2') && $rows['status_pembayaran'] == 'diperiksa'){
				$proses = '<i class="text-warning">Menunggu Konfirmasi Pembayaran</i>';
			}elseif(($rows['bayar'] == '1' || $rows['bayar'] == '2') && $rows['status_pembayaran'] == 'lunas'){
				$proses = '<i class="text-success">Pembayaran Berhasil</i>';
			}elseif(($rows['bayar'] == '1' || $rows['bayar'] == '2') && $rows['status_pembayaran'] == 'proses refund'){
				$proses = '<i class="text-warning">Refund Sedang Diproses</i>';
				$statusproses = '<i class="text-warning">Refund Sedang Diproses</i>';
			}elseif(($rows['bayar'] == '1' || $rows['bayar'] == '2') && $rows['status_pembayaran'] == 'refund'){
				$proses = '<i class="text-success">Refund Berhasil</i>';
				$statusproses = 'Pembayaran refund selesai<br><button type=button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#myModal">Lihat Bukti Transfer Refund</button>';
			}elseif(($rows['bayar'] == '1' || $rows['bayar'] == '2') && $rows['proses'] == '1'){
				$proses = '<i class="text-success">Pesanan Diproses</i>';
			}elseif(($rows['bayar'] == '1' || $rows['bayar'] == '2') && $rows['status_pembayaran'] == 'lunas' && $rows['proses'] == '0'){
				$proses = '<i class="text-success">Menunggu Konfirmasi Pesanan</i>';
			}

// if($rows['status_pembayaran']=='gagal' || $detail['batal']=='1'){ $proses = '<i class="text-danger">Pembayaran Gagal</is>'; }elseif($rows['status_pembayaran']=='proses refund'){ $proses = '<i class="text-danger">Proses Refund</is>'; }elseif($rows['status_pembayaran']=='refund'){ $proses = '<i class="text-danger">Refund</is>'; }elseif ($rows['proses']=='0'){ $proses = '<i class="text-danger">Pending</i>'; $status = 'Proses'; }elseif($rows['proses']=='1'){ $proses = '<i class="text-success">Proses</i>'; }elseif($rows['status_pembayaran']=='gagal'){ $proses = '<i class="text-danger">Pembayaran Gagal</i>'; }else{ $proses = '<i class="text-info">Konfirmasi</i>'; }
			echo "
			<tr>
			<td colspan='3'><b>Berat</b> <small><i class='pull-right'>(".terbilang($total_berat)." Gram)</i></small></td>
			<td><b>$total_berat Gram</b></td>
			</tr>
			
			<tr>
			<td colspan='3'><b>Total Belanja</b> <small><i class='pull-right'>(".terbilang($total_belanja)." Rupiah)</i></small></td>
			<td><b>Rp ".rupiah($total_belanja)."</b></td>
			</tr>

			<tr>
			<td style='color:Red' colspan='3'><b>Diskon Belanja </b> <small><i class='pull-right'>(".terbilang($total_diskon)." Rupiah)</i></small></td>
			<td style='color:Red'><b>(Rp ".rupiah($total_diskon).")</b></td>
			</tr>

			<tr>
			<td colspan='3'><b>Ongkir <small><i class='pull-right'>(".terbilang($total_ongkir).")</i></small></td>
			<td><b>Rp ".rupiah($total_ongkir)."</b></td>
			</tr>
			
			<tr>
			<td style='color:Red' colspan='3'><b>Diskon Ongkir</b> <small><i class='pull-right'>(".terbilang($diskon_ongkir)." Rupiah)</i></small></td>
			<td style='color:Red'><b>(Rp ".rupiah($diskon_ongkir).")</b></td>
			</tr>";
			
			echo"<tr>
			<td colspan='3'><b>Asuransi </b> <small><i class='pull-right'>(".terbilang($total_asuransi).")</i></small></td>
			<td><b>Rp ".rupiah($total_asuransi)."</b></td>
			</tr>";
			
			if ($record2['pajak'] !=0){ 
				echo"<tr>
				<td colspan='3'><b>Pajak PPN 10% </b> <small><i class='pull-right'>(".terbilang($record2['pajak']).")</i></small></td>
				<td><b>Rp ".rupiah($record2['pajak'])."</b></td>
				</tr>";
			}

			echo"<tr>
			<td colspan='3'><b>Biaya Administrasi </b> <small><i class='pull-right'>(".terbilang($record2['mdr']).")</i></small></td>
			<td><b>Rp ".rupiah($record2['mdr'])."</b></td>
			</tr>";

			echo"<tr class='success'>
			<td colspan='3'><b>Total Bayar </b> <small><i class='pull-right'>(".terbilang($record2['total_bayar'])." Rupiah)</i></small></td>
			<td><b>Rp ".rupiah($record2['total_bayar'])."</b></td>
			</tr><br/>

			
			<tr><td align=center colspan='4'><b>$proses</b></td></tr>
			
			<tr><td align=justify colspan='4'>";
			echo $statusproses;
// if($rows['status_pembayaran']=='gagal' && $rows['batal']=='1' ){
// 	echo "<b>Note: Anda Telah Membatalkan Transaksi .</b><br>
// 	Silahkan Memilih Produk Kembali				
// 	<a class='btn btn-info btn-xs' title='Detail data pesanan' href='".base_url()."produk'>Lanjut Belanja</a>";

// }else if($detail['batal']=='1'){
// 	echo "<b>Note: Anda Telah Membatalkan Transaksi .</b><br>
// 	Silahkan Memilih Produk Kembali				
// 	<a class='btn btn-info btn-xs' title='Detail data pesanan' href='".base_url()."produk'>Lanjut Belanja</a>";

// } else if($rows['status_pembayaran']=='gagal'){
// 	if($rows['via']=='midtrans'){
// 		echo "<b>Note: Waktu Pembayaran Sudah Habis .</b><br>
// 				Silahkan Memilih Produk Kembali				
// 				<a class='btn btn-info btn-xs' title='Detail data pesanan' href='".base_url()."produk'>Lanjut Belanja</a>";

// 	}else{
// 	echo "<b>Note: ".$rows['remarks'].".</b><br>
// 				Silahkan melanjutkan ke proses 				
// 				<a class='btn btn-info btn-xs' title='Detail data pesanan' href='".base_url()."konfirmasi/refund?idp=$row[id_penjualan]'>Refund</a>";
// 	}
// }elseif($rows['status_pembayaran']=='refund'){
// 	echo '
// 				Pembayaran refund selesai<br>
// 				<button type=button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#myModal">Lihat Bukti Transfer Refund</button>
// 				';
			
// }elseif($rows['status_pembayaran']=='proses refund'){
// 	echo "
// 				Refund sedang diproses";
// }
			echo "
			</td></tr>
			</tbody>
			</table>";

			if ($detail['awb'] == '' ){
				?>
				<?php
			}else{ 
				?>
				<table class="table table-striped table-condensed">
					<thead>
						<tr>
							PENGIRIMAN
						</tr>
						<!--<hr/>
						<tr><td style='width:40%'>NCS Kurir | No Transaksi</td></tr>-->
						
					</thead>
					<tbody>
						<table class="table table-striped table-condensed">
							<tr>
								<td width="70px">AWB</td>
								<td>: <?php echo $record2['awb']; ?></td>
							</tr>
							<tr>
								<td>Service</td>
								<td>: <?php echo $record2['service']; ?></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
						</table>
						<div class="row">
							<div class="col-md-2">
								<br><br><br><br><br>
								<?php
								echo"<br><img style='width:100%' src='".base_url()."asset/foto_pasangiklan/ekpedisi2.png'>";?>
								<!--<div style="border: 1px #c3c4c6 solid; height: 250px; width: 0px;"></div>-->
							</div>
							<div class="col-md-8">
								<ul class="timelinee">
									<?php 
									$url = 'https://apimobile.ptncs.com/php/ncs_deal/getCheckpointListiDss.php?AWB='.$detail['awb'];
									
									$ch = curl_init();
									curl_setopt($ch,CURLOPT_URL, $url);
									curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
									
									$server_output = curl_exec($ch);
									
									curl_close($ch);
									
									
									$data = json_decode($server_output, true);
									for($i=0;$i<count($data);$i++){
										?>
										<li>
											<a target="_blank" href="#"><?php echo $data[$i]['Checkpointt']?></a>
											<a href="#" class="float-right"><?php echo $data[$i]['Date']?> (<?php echo $data[$i]['Time']?>)</a>
											<p><?php echo $data[$i]['Branch']?></p>
										<!--<p><?php echo $data[$i]['Remarks']?></p>
										<p><?php echo $data[$i]['Reason']?></p>
										<p><?php echo $data[$i]['Recipient']?></p>
										<p><?php echo $data[$i]['Courier']?></p>-->
									</li>
									<?php
								}
								?>
							</ul>
						</div>
					</div>
				</tbody>
			</table>
			<?php
		}
		?>


	</div>
	<hr>


	<div id="myModal" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<!-- konten modal-->
			<div class="modal-content">
				<!-- heading modal -->
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Bukti Transfer Refund</h4>
				</div>
				<!-- body modal -->
				<div class="modal-body">
					<p>
						<table>
							<tr>
								<?php
								if($record3['bukti_transfer_refund']!=''){
									?>
									<td style="text-align:center;">
										<img src='<?php echo base_url()."asset/bukti_transfer_refund/".$record3['bukti_transfer_refund']; ?>' border='1px' width=70% height='auto'>
									</td>
									<?php
								}else{
									?>
									<td style="text-align:center;">
										<img src='<?php echo base_url()."asset/bukti_transfer_refund/img_not_available.jpg"; ?>' border='1px' width=70% height='auto'>
									</td>
									<?php 
								}
								?>
							</tr>
						</table>
					</p>
				</div>
			</div>
		</div>
	</div>