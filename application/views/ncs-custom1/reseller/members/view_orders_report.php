<!-- Custom -->
<link rel="stylesheet" href="<?php echo base_url(); ?>template/<?php echo template(); ?>/bar_rating/themes/fontawesome-stars.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>template/<?php echo template(); ?>/bar_rating/themes/css-stars.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>template/<?php echo template(); ?>/bar_rating/themes/bootstrap-stars.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>template/<?php echo template(); ?>/bar_rating/themes/fontawesome-stars-o.css">


<p class='sidebar-title text-danger produk-title'> <?php echo $title?></p>

<ul id='myTabs' class='nav nav-tabs' role='tablist'>
  <li role='presentation' class='active'><a href='#semua' id='semua-tab' role='tab' data-toggle='tab' aria-controls='profile' aria-expanded='true'>Data per Transaksi </a></li>
  <li role='presentation' class=''><a href='#detail' role='tab' id='detail-tab'  data-toggle='tab' aria-controls='status1' aria-expanded='false'>Data per Merchant</a></li>
</ul><br>
<!-- 
<ul id='myTabs' class='nav nav-tabs' role='tablist'>
  <li role='presentation' class='active'><a href='#status1' id='status1-tab' role='tab' data-toggle='tab' aria-controls='profile' aria-expanded='true'>Menunggu Pembayaran </a></li>
  <li role='presentation' class=''><a href='#status2' role='tab' id='status2-tab' data-toggle='tab' aria-controls='status2' aria-expanded='false'>Dikemas</a></li>
  <li role='presentation' class=''><a href='#status3' role='tab' id='status3-tab' data-toggle='tab' aria-controls='status3' aria-expanded='false'>Dikirim</a></li>
  <li role='presentation' class=''><a href='#status4' role='tab' id='status4-tab' data-toggle='tab' aria-controls='status4' aria-expanded='false'>Selesai</a></li>
  <li role='presentation' class=''><a href='#status5' role='tab' id='status5-tab' data-toggle='tab' aria-controls='status5' aria-expanded='false'>Batal</a></li>
</ul><br> -->

<div id='profileTabContent' class='tab-content'>
	<?php 
		if ($this->uri->segment(3)=='success'){
		  echo "<div class='alert alert-success'><b>SUCCESS</b> - Terima kasih telah Melakukan Konfirmasi Pembayaran!</div>";
		}elseif($this->uri->segment(3)=='orders'){
		  echo "<div class='alert alert-success'><b>SUCCESS</b> - Orderan anda sukses terkirim, silahkan melakukan pembayaran ke rekening reseller pesanan anda dan selanjutnya lakukan konfirmasi pembayaran!</div>";
		}
	?>
		  
	<div role='tabpanel' class='tab-pane fade active in' id='semua' aria-labelledby='semua-tab'>
		  
		<table id='example7' style='overflow-x:scroll; width:96%;' class="table table-striped table-condensed">
			<thead>
			  <tr>
				<th width="20px">No </th>
				<th>Kode Transaksi</th>
				<th>Total Belanja</th>
				<th>Ongkir</th>
				<th>Biaya Admin</th>
				<th>Total Bayar</th>
				<th>Pembayaran</th>
				<th>Waktu Pembayaran</th>
				<th>Status</th>
				<th></th>
			  </tr>
			</thead>
			<tbody>
			  <?php 
			
				$no = 1;
				foreach ($semua->result_array() as $row){
				if($row['pembayaran']=='manual-transfer' && $row['bukti_bayar']!=null  && $row['status_pembayaran']=='diperiksa' ){
					$proses = "Validasi Pembayaran";
				}elseif($row['pembayaran']=='manual-transfer' && $row['bukti_bayar']!=null  && $row['status_pembayaran']=='proses refund' ){
					$proses = "Proses Refund";
				}elseif($row['pembayaran']=='manual-transfer' && $row['bukti_bayar']!=null  && $row['status_pembayaran']=='refund'){
					$proses = "Refund";
				}elseif($row['batal']=='1'){
					$proses = "Batal";
				}elseif($row['bayar']=='0'){ 
					$proses = '<i class="text-info">Belum Bayar</i>'; 
				}elseif($row['status_pembayaran']=='lunas'){ 
					$proses = '<i class="text-success">Lunas</i>'; 
				}else{ 
					$proses = '<i class="text-warning">Gagal</i>'; 
				}
				$jml = $this->db->query("SELECT sum(b.harga_jual*jumlah) as total_belanja, sum(a.diskon) as tot_disk,sum(a.ongkir-a.diskon_ongkir+a.asuransi) as tot_ongkir from rb_penjualan a join rb_penjualan_detail b on a.id_penjualan=b.id_penjualan where kode_transaksi='".$row['kode_transaksi']."'")->row_array();
				// $proses = '<i class="text-danger">Pending</i>';
				echo "<tr><td>$no</td>
						  <td><span class='text-success'>$row[kode_transaksi]</span></td>
						  <td> Rp ";
						  echo rupiah($jml['total_belanja']-$jml['tot_disk']);
						  echo "</a></td>
						  <td><span> Rp ";
							echo rupiah($jml['tot_ongkir']);
						  echo"</span></td>
						  <td>Rp ".rupiah($row['mdr'])."</td>
						  <td style='color:red;'>Rp ".rupiah($row['total_bayar'])."</td>
						  <td>";
						  if (in_array($row['jenis_pembayaran'],array("bca","bri","mandiri","permata","bni"))){
						  echo "Transfer Bank ".strtoupper($row['pembayaran']);
						  }else{
							echo strtoupper($row['pembayaran']);
						  }
						  $tgl_pembayaran = date('Y-m-d H:i:s', strtotime('+2 days', strtotime($row['waktu_order']))); 
						  echo "</td>
						  <td>$tgl_pembayaran</td>
						  <td>";
						   echo $proses;
						  echo "</td>";
						 
						  echo "<td width='150px'>";
						  	
							echo "<a style='margin-right:3px' class='btn btn-success btn-xs' title='Konfirmasi Pembayaran' href='".base_url()."konfirmasi?idp=$row[kode_transaksi]'"; if(($row['pembayaran']=='manual-transfer' && $row['bukti_bayar']!=null) || ($row['status_pembayaran']=='lunas' || $row['status_pembayaran']=='gagal' || $row['status_pembayaran']=='refund' || $row['status_pembayaran']=='proses refund') || $row['batal']=='1' || ($row['pembayaran']=='cod')){echo 'disabled="disabled"'; } echo ">Pembayaran</a>";
							echo "<button type='button' class='btn btn-danger btn-xs' data-toggle='modal' data-target='#exampleModal$row[id_penjualan]'"; 
							if(($row['pembayaran']=='manual-transfer' && $row['bukti_bayar']!=null) || ($row['status_pembayaran']=='lunas' || $row['status_pembayaran']=='gagal' || $row['status_pembayaran']=='refund' || $row['status_pembayaran']=='proses refund') || $row['batal']=='1' || ($row['pembayaran']=='cod' && $row['proses']=='1') ){echo 'disabled="disabled"'; } echo ">
							Batal
						  </button>";
							// echo "<a class='btn btn-danger btn-xs' title='Batalkan Pesanan' href='".base_url()."members/orders_report_delete/".$row['id_penjualan']."'"; if($row['pembayaran']=='manual-transfer' && $row['bukti_bayar']!=null){echo 'disabled="disabled"'; } echo ">Batal</a>
							  
							echo "<a class='btn btn-info btn-xs' title='Detail data pesanan' href='".base_url()."members/transaksi_detail/$row[kode_transaksi]'><span class='glyphicon glyphicon-search'></span></a> 
							  
						  </td>
					  </tr>

					  ";
					  $no++;
?>
					 <div class="modal fade" id="exampleModal<?php echo $row['id_penjualan']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					  <div class="modal-dialog modal-xs" role="document">
						<form action='<?php echo base_url()."members/orders_report_delete/$row[kode_transaksi]"; ?>' method="POST">
						<div class="modal-content">
						  <div class="modal-header">
							<h3 class="modal-title">Batal</h3>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							  <span aria-hidden="true">&times;</span>
							</button>
						  </div>
						  <div class="modal-body">
							<p>Apakah Anda Yakin Ingin Membatalkan Transaksi <?php echo $row['kode_transaksi']; ?></p>
						  </div>
						  <div class="modal-footer">
							<button type="Submit" class="btn btn-primary">Ya</button>
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						  </div>
						</div>
						</form>
					  </div>
					</div>
				<?php
				  
				}
			  ?>
			</tbody>
		  </table>
	</div>

	<div role='tabpanel' class='tab-pane fade in' id='detail' aria-labelledby='detail-tab'>
	
	

	<ul id='myTabs' class='nav nav-tabs' role='tablist'>
  	<li role='presentation' class='active'><a href='#status1' id='status1-tab' role='tab' data-toggle='tab' aria-controls='profile' aria-expanded='false'>Menunggu Pembayaran </a></li>
  	<li role='presentation' class=''><a href='#status2' role='tab' id='status2-tab' data-toggle='tab' aria-controls='status2' aria-expanded='false'>Dikemas</a></li>
  	<li role='presentation' class=''><a href='#status3' role='tab' id='status3-tab' data-toggle='tab' aria-controls='status3' aria-expanded='false'>Dikirim</a></li>
  	<li role='presentation' class=''><a href='#status4' role='tab' id='status4-tab' data-toggle='tab' aria-controls='status4' aria-expanded='false'>Selesai</a></li>
  	<li role='presentation' class=''><a href='#status5' role='tab' id='status5-tab' data-toggle='tab' aria-controls='status5' aria-expanded='false'>Batal</a></li>
	</ul>
	<br> 	
	<div id='hapus'>
	<table id='example9' style='overflow-x:scroll; width:96%;' class="table table-striped table-condensed">
			<thead>
			  <tr>
				<th width="20px">No</th>
				<th>Kode Transaksi</th>
				<th>Nama Merchant</th>
				<th>Jumlah Produk</th>
			<th>Total Belanja</th>
				<th>Pembayaran</th>
				<th>Waktu Order</th>
				<th>Status</th>
				<th></th>
			  </tr>
			</thead>
			<tbody>
			  <?php 
				$no = 1;
				foreach ($record1->result_array() as $row){
				// if ($row['proses']=='0'){ $proses = '<i class="text-danger">Pending</i>'; }elseif($row['proses']=='1'){ $proses = '<i class="text-success">Proses</i>'; }else{ $proses = '<i class="text-info">Konfirmasi</i>'; }
				$proses = '<i class="text-danger">Pending</i>';
				$total = $this->db->query("SELECT sum((a.harga_jual*a.jumlah)-a.diskon) as total FROM `rb_penjualan_detail` a where a.id_penjualan='$row[id_penjualan]'")->row_array();
				echo "<tr><td>$no</td>
						  <td><span class='text-success'>$row[kode_transaksi]</span></td>
						  <td>$row[nama_reseller]</a></td>
						  
						  <td><span style='color:blue;'>";
							echo $this->db->query("SELECT id_penjualan from rb_penjualan_detail where id_penjualan='".$row['id_penjualan']."'")->num_rows();
						  echo"</span></td>
						  <td style='color:red;'>Rp ".rupiah($total['total']+$row['ongkir']+$row['asuransi']-$row['diskon']-$row['diskon_ongkir'])."</td>
						  <td>";
						  if (in_array($row['jenis_pembayaran'],array("bca","bri","mandiri","permata","bni"))){
						  echo "Transfer Bank ".strtoupper($row['pembayaran']);
						  }else{
							echo strtoupper($row['pembayaran']);
						  }
						  echo "</td>
						  <td>$row[waktu_transaksi]</td>
						  <td>";
						  if($row['pembayaran']=='manual-transfer' && $row['bukti_bayar']!=null){
							echo "Validasi Pembayaran";
						  }else{ echo $proses;}
						  echo "</td>";
						 
						  echo "<td width='150px'>";
						  	
							echo "<a style='margin-right:3px' class='btn btn-success btn-xs' title='Konfirmasi Pembayaran' href='".base_url()."konfirmasi?idp=$row[kode_transaksi]'"; if($row['pembayaran']=='manual-transfer' && $row['bukti_bayar']!=null){echo 'disabled="disabled"'; } echo ">Pembayaran</a>";
							echo "<button type='button' class='btn btn-danger btn-xs' data-toggle='modal' data-target='#1exampleModal$row[id_penjualan]'"; if($row['pembayaran']=='manual-transfer' && $row['bukti_bayar']!=null){echo 'disabled="disabled"'; } echo ">
							Batal
						  </button>";
							// echo "<a class='btn btn-danger btn-xs' title='Batalkan Pesanan' href='".base_url()."members/orders_report_delete/".$row['id_penjualan']."'"; if($row['pembayaran']=='manual-transfer' && $row['bukti_bayar']!=null){echo 'disabled="disabled"'; } echo ">Batal</a>
							  
							echo "<a class='btn btn-info btn-xs' title='Detail data pesanan' href='".base_url()."members/keranjang_detail/$row[id_penjualan]'><span class='glyphicon glyphicon-search'></span></a> 
							  
						  </td>
					  </tr>

					  ";
					  $no++;
?>
					 <div class="modal fade" id="1exampleModal<?php echo $row['id_penjualan']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					  <div class="modal-dialog modal-xs" role="document">
						<form action='<?php echo base_url()."members/orders_report_delete/$row[kode_transaksi]"; ?>' method="POST">
						<div class="modal-content">
						  <div class="modal-header">
							<h3 class="modal-title">Batal</h3>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							  <span aria-hidden="true">&times;</span>
							</button>
						  </div>
						  <div class="modal-body">
							<p>Apakah Anda Yakin Ingin Membatalkan Transaksi <?php echo $row['kode_transaksi']; ?></p>
						  </div>
						  <div class="modal-footer">
							<button type="Submit" class="btn btn-primary">Ya</button>
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						  </div>
						</div>
						</form>
					  </div>
					</div>
				<?php
				  
				}
			  ?>
			</tbody>
		  </table>
	</div>
<div id='profileTabContent' class='tab-content'>		  
	<div role='tabpanel' class='tab-pane fade in active' id='status1' aria-labelledby='status1-tab'>
		  <table id='example8' style='overflow-x:scroll; width:96%;' class="table table-striped table-condensed">
			<thead>
			  <tr>
				<th width="20px">No</th>
				<th>Kode Transaksi</th>
				<th>Nama Merchant</th>
				<th>Jumlah Produk </th>
			<th>Total Belanja</th>
				<th>Pembayaran</th>
				<th>Waktu Order</th>
				<th>Status</th>
				<th></th>
			  </tr>
			</thead>
			<tbody>
			  <?php 
				$no = 1;
				foreach ($record1->result_array() as $row){
				// if ($row['proses']=='0'){ $proses = '<i class="text-danger">Pending</i>'; }elseif($row['proses']=='1'){ $proses = '<i class="text-success">Proses</i>'; }else{ $proses = '<i class="text-info">Konfirmasi</i>'; }
				$proses = '<i class="text-danger">Pending</i>';
				$total = $this->db->query("SELECT sum((a.harga_jual*a.jumlah)-a.diskon) as total FROM `rb_penjualan_detail` a where a.id_penjualan='$row[id_penjualan]'")->row_array();
				echo "<tr><td>$no</td>
						  <td><span class='text-success'>$row[kode_transaksi]</span></td>
						  <td>$row[nama_reseller]</a></td>
						  
						  <td><span style='color:blue;'>";
							echo $this->db->query("SELECT id_penjualan from rb_penjualan_detail where id_penjualan='".$row['id_penjualan']."'")->num_rows();
						  echo"</span></td>
						  <td style='color:red;'>Rp ".rupiah($total['total']+$row['ongkir']+$row['asuransi']-$row['diskon']-$row['diskon_ongkir'])."</td>
						  <td>";
						  if (in_array($row['jenis_pembayaran'],array("bca","bri","mandiri","permata","bni"))){
						  echo "Transfer Bank ".strtoupper($row['pembayaran']);
						  }else{
							echo strtoupper($row['pembayaran']);
						  }
						  echo "</td>
						  <td>$row[waktu_transaksi]</td>
						  <td>";
						  if($row['pembayaran']=='manual-transfer' && $row['bukti_bayar']!=null){
							echo "Validasi Pembayaran";
						  }else{ echo $proses;}
						  echo "</td>";
						 
						  echo "<td width='150px'>";
						  	
							echo "<a style='margin-right:3px' class='btn btn-success btn-xs' title='Konfirmasi Pembayaran' href='".base_url()."konfirmasi?idp=$row[kode_transaksi]'"; if($row['pembayaran']=='manual-transfer' && $row['bukti_bayar']!=null){echo 'disabled="disabled"'; } echo ">Pembayaran</a>";
							echo "<button type='button' class='btn btn-danger btn-xs' data-toggle='modal' data-target='#2exampleModal$row[id_penjualan]'"; if($row['pembayaran']=='manual-transfer' && $row['bukti_bayar']!=null){echo 'disabled="disabled"'; } echo ">
							Batal
						  </button>";
							// echo "<a class='btn btn-danger btn-xs' title='Batalkan Pesanan' href='".base_url()."members/orders_report_delete/".$row['id_penjualan']."'"; if($row['pembayaran']=='manual-transfer' && $row['bukti_bayar']!=null){echo 'disabled="disabled"'; } echo ">Batal</a>
							  
							echo "<a class='btn btn-info btn-xs' title='Detail data pesanan' href='".base_url()."members/keranjang_detail/$row[id_penjualan]'><span class='glyphicon glyphicon-search'></span></a> 
							  
						  </td>
					  </tr>

					  ";
					  $no++;
?>
					 <div class="modal fade" id="2exampleModal<?php echo $row['id_penjualan']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					  <div class="modal-dialog modal-xs" role="document">
						<form action='<?php echo base_url()."members/orders_report_delete/$row[kode_transaksi]"; ?>' method="POST">
						<div class="modal-content">
						  <div class="modal-header">
							<h3 class="modal-title">Batal</h3>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							  <span aria-hidden="true">&times;</span>
							</button>
						  </div>
						  <div class="modal-body">
							<p>Apakah Anda Yakin Ingin Membatalkan Transaksi <?php echo $row['kode_transaksi']; ?></p>
						  </div>
						  <div class="modal-footer">
							<button type="Submit" class="btn btn-primary">Ya</button>
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						  </div>
						</div>
						</form>
					  </div>
					</div>
				<?php
				  
				}
			  ?>
			</tbody>
		  </table>
	</div>
	<div role='tabpanel' class='tab-pane fade in' id='status2' aria-labelledby='status2-tab'>
		<table id='example3' style='overflow-x:scroll;width:96%' class="table table-striped table-condensed">
			<thead>
			  <tr>
				<th width="20px">No</th>
				<th>Kode Transaksi</th>
				<th>Nama Merchant</th>
				<th>Jumlah Produk</th>
			<th>Total Belanja</th>
				<th>Pembayaran</th>
				<th>Waktu Order</th>
				<th>Status</th>
				<th width='70px'></th>
			  </tr>
			</thead>
			<tbody>
			  <?php 
				$no = 1;
				foreach ($record2->result_array() as $row){
				// if ($row['proses']=='0'){ $proses = '<i class="text-danger">Pending</i>'; }elseif($row['proses']=='1'){ $proses = '<i class="text-success">Proses</i>'; }else{ $proses = '<i class="text-info">Konfirmasi</i>'; }
				
				// $proses = '<i class="text-warning">Menunggu Konfirmasi</i>';
				
				if ($row['status_pembayaran']=='gagal'){ $proses = '<i class="text-danger">Pembayaran Gagal</i>'; }elseif ($row['status_pembayaran']=='proses refund'){ $proses = '<i class="text-danger">Proses Refund</i>'; }elseif ($row['status_pembayaran']=='refund'){ $proses = '<i class="text-danger">Refund</i>'; }else{ $proses = '<i class="text-warning">Dikemas</i>'; }
				
				$total = $this->db->query("SELECT sum((a.harga_jual*a.jumlah)-a.diskon) as total FROM `rb_penjualan_detail` a where a.id_penjualan='$row[id_penjualan]'")->row_array();
				echo "<tr><td>$no</td>
						  <td><span class='text-success'>$row[kode_transaksi]</span></td>
						  <td><a href='".base_url()."members/detail_reseller/$row[id_reseller]'>$row[nama_reseller]</a></td>
						  <td><span style='color:blue;'>";
						  echo $this->db->query("SELECT id_penjualan from rb_penjualan_detail where id_penjualan='".$row['id_penjualan']."'")->num_rows();
						  echo"</span></td>
						  <td style='color:red;'>Rp ".rupiah($total['total']+$row['ongkir']+$row['asuransi']-$row['diskon']-$row['diskon_ongkir'])."</td>
						  <td>";
						  if (in_array($row['jenis_pembayaran'],array("bca","bri","mandiri","permata","bni"))){
						  echo "Transfer Bank ".strtoupper($row['pembayaran']);
						  }else{
							echo strtoupper($row['pembayaran']);
						  }
						  echo "</td>";
						  echo "<td>$row[waktu_transaksi]</td>";
						  echo "<td>$proses</td>
						  <td width='70px'>";						  
						  // echo "<a class='btn btn-danger btn-xs' title='Batalkan Pesanan' href='".base_url()."members/orders_report_delete/$row[id_penjualan]'>Batal</a>";  
						  echo " <a class='btn btn-info btn-xs' title='Detail data pesanan' href='".base_url()."members/keranjang_detail/$row[id_penjualan]'><span class='glyphicon glyphicon-search'> Detail</span></a>
						  
						  </td>
					  </tr>

					  ";
				  $no++;
				}
			  ?>
			</tbody>
		</table>
	</div>
	<div role='tabpanel' class='tab-pane fade in' id='status3' aria-labelledby='status3-tab'>
		<table id='example4' style='overflow-x:scroll; width: 96%' class="table table-striped table-condensed">
			<thead>
			  <tr>
				<th width="20px">No</th>
				<th>Kode Transaksi</th>
				<th>Nama Merchant</th>
				<th>Jumlah Produk</th>
			<th>Total Belanja</th>
				<th>Pembayaran</th>
				<th>Waktu Order</th>
				<th>Status</th>
				<th width='100px'></th>
			  </tr>
			</thead>
			<tbody>
			  <?php 
				$no = 1;
				foreach ($record3->result_array() as $row){
				// if ($row['proses']=='0'){ $proses = '<i class="text-danger">Pending</i>'; }elseif($row['proses']=='1'){ $proses = '<i class="text-success">Proses</i>'; }else{ $proses = '<i class="text-info">Konfirmasi</i>'; }
				$proses = '<i class="text-success">Proses</i>';
				$total = $this->db->query("SELECT sum((a.harga_jual*a.jumlah)-a.diskon) as total FROM `rb_penjualan_detail` a where a.id_penjualan='$row[id_penjualan]'")->row_array();
				echo "<tr><td>$no</td>
						  <td><span class='text-success'>$row[kode_transaksi]</span></td>
						  <td><a href='".base_url()."members/detail_reseller/$row[id_reseller]'>$row[nama_reseller]</a></td>
						  <td><span style='color:blue;'>";
						  echo $this->db->query("SELECT id_penjualan from rb_penjualan_detail where id_penjualan='".$row['id_penjualan']."'")->num_rows();
						  echo"</span></td>						 
						  <td style='color:red;'>Rp ".rupiah($total['total']+$row['ongkir']+$row['asuransi']-$row['diskon']-$row['diskon_ongkir'])."</td>
						  <td>";
						  
						  if (in_array($row['jenis_pembayaran'],array("bca","bri","mandiri","permata","bni"))){
						  echo "Transfer Bank ".strtoupper($row['pembayaran']);
						  }else{
							echo strtoupper($row['pembayaran']);
						  }
						 
						  $cek_kirim = $this->db->query("select status from rb_status_pengiriman where awb='".$row['awb']."' and lastStatus='1'")->row_array();
						  
						  echo "</td>";
						  echo "<td>$row[waktu_transaksi]</td>";
						  echo "<td>$proses </td>
						  <td width='100px'>";
						  if($cek_kirim['status']=="OK"){
							echo "<a style='margin-right:3px' class='btn btn-primary btn-xs' title='Diterima' href='".base_url()."members/terima_pesanan?idp=$row[id_penjualan]'>Diterima</a>";
							echo "<a class='btn btn-info btn-xs' title='Detail data pesanan' href='".base_url()."members/keranjang_detail/$row[id_penjualan]'><span class='glyphicon glyphicon-search'></span></a>";
						  }else{
							echo "<a class='btn btn-info btn-xs' title='Detail data pesanan' href='".base_url()."members/keranjang_detail/$row[id_penjualan]'><span class='glyphicon glyphicon-search'></span></a>";
						    
						  }
							// echo "<a class='btn btn-danger btn-xs' title='Delete' href='".base_url()."members/orders_report_delete/$row[id_penjualan]'><span class='glyphicon glyphicon-remove'></span></a> ";
							  
							
						 echo" </td>
					  </tr>

					  ";
				  $no++;
				}
			  ?>
			</tbody>
		</table>
	</div>
	<div role='tabpanel' class='tab-pane fade in' id='status4' aria-labelledby='status4-tab'>
		<table id='example5' style='overflow-x:scroll; width: 96%' class="table table-striped table-condensed">
			<thead>
			  <tr>
				<th width="20px">No</th>
				<th>Kode Transaksi</th>
				<th>Nama Merchant</th>
				<th>Jumlah Produk</th>
				<th>Total Belanja</th>
				<th>Pembayaran</th>
				<th>Waktu Order</th>
				<th>Status</th>
				<th width='50px'></th>
			  </tr>
			</thead>
			<tbody>
			  <?php 
				$no = 1;
				foreach ($record4->result_array() as $row){
				// if ($row['proses']=='0'){ $proses = '<i class="text-danger">Pending</i>'; }elseif($row['proses']=='1'){ $proses = '<i class="text-success">Proses</i>'; }else{ $proses = '<i class="text-info">Konfirmasi</i>'; }
				$proses = '<i class="text-success">Selesai</i>';
				$total = $this->db->query("SELECT sum((a.harga_jual*a.jumlah)-a.diskon) as total FROM `rb_penjualan_detail` a where a.id_penjualan='$row[id_penjualan]'")->row_array();
				echo "<tr><td>$no</td>
						  <td><span class='text-success'>$row[kode_transaksi]</span></td>
						  <td><a href='".base_url()."members/detail_reseller/$row[id_reseller]'>$row[nama_reseller]</a></td>
						  <td><span style='color:blue;'>";
						  echo $this->db->query("SELECT id_penjualan from rb_penjualan_detail where id_penjualan='".$row['id_penjualan']."'")->num_rows();
						  echo"</span></td>
						  <td style='color:red;'>Rp ".rupiah($total['total']+$row['ongkir']+$row['asuransi']-$row['diskon']-$row['diskon_ongkir'])."</td>
						  <td>";
						  if (in_array($row['jenis_pembayaran'],array("bca","bri","mandiri","permata","bni"))){
						  echo "Transfer Bank ".strtoupper($row['pembayaran']);
						  }else{
							echo strtoupper($row['pembayaran']);
						  }
						  echo "</td>";
						  echo "<td>$row[waktu_transaksi]</td>";
						  echo "<td>$proses</td>
						  <td width='50px'>";
						  echo "<button type='button' class='btn btn-warning btn-xs' data-toggle='modal' data-target='#rating$row[id_penjualan]'>
						  <span class='glyphicon glyphicon-star-empty'>Ulasan</span>
						  </button>";
						//   echo"<a class='btn btn-info btn-xs' title='Detail data pesanan' href='".base_url()."members/keranjang_detail/$row[id_penjualan]'><span class='glyphicon glyphicon-star-empty'>Ulasan</span></a>";
						//  echo "<a class='btn btn-info btn-xs' title='Detail data pesanan' href='".base_url()."members/keranjang_detail/$row[id_penjualan]'><span class='glyphicon glyphicon-search'></span></a>";
						  echo"</td></tr>";
				  $no++;
				  ?>
				  <div class="modal fade" id="rating<?php echo $row['id_penjualan']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				   <div class="modal-dialog modal-xs" role="document">
					 <form action='<?php echo base_url()."members/save_ulasan/$row[id_penjualan]"; ?>' method="POST">
					 <div class="modal-content">
					   <div class="modal-header">
						 <h3 class="modal-title">Beri Ulasan</h3>
						 <button type="button" class="close" data-dismiss="modal" aria-label="Close">
						   <span aria-hidden="true">&times;</span>
						 </button>
					   </div>
					   <div class="modal-body">
					   <?php $query = $this->db->query("select a.id_penjualan_detail,a.id_produk,b.nama_produk from rb_penjualan_detail a join rb_produk b on a.id_produk=b.id_produk where id_penjualan='$row[id_penjualan]'")->result_array(); 
					 	foreach ($query as $produk) {
						$cek_ulasan = $this->db->query("select rating from rb_produk_ulasan where id_penjualan_detail='$produk[id_penjualan_detail]'")->num_rows();
							if($cek_ulasan>0){
								$ulasan = $this->db->query("select rating,ulasan from rb_produk_ulasan where id_penjualan_detail='$produk[id_penjualan_detail]'")->row_array();
						?>
							<h5 style="text-align:center;"><?php echo $produk['nama_produk']; ?></h5>
							<div class='stars stars-example-bootstrap'>
                            <select id="rat<?php echo $produk['id_penjualan_detail']; ?>" name="example-bootstrap2" class="rat<?php echo $produk['id_penjualan_detail']; ?>" autocomplete='off'>
							<?php
                            for ($i=1; $i<=5; $i++){
                                if($i<=$ulasan['rating']){
                                echo "<option value='1'>$i</option>";
                                }else{
                                echo "<option value='2'>$i</option>";
                                }
                            }
							?>
                            </select> </div>
							<input type="hidden" name="rating<?php echo $produk['id_penjualan_detail']; ?>" id="rating<?php echo $produk['id_penjualan_detail']; ?>" value="">
							<textarea name="ulasan<?php echo $produk['id_penjualan_detail']; ?>" rows="7" cols="67" disabled><?php echo $ulasan['ulasan']; ?></textarea>
						<?php		
							}else{
						?>
					   <h5 style="text-align:center;"><?php echo $produk['nama_produk']; ?></h5>
					   	<div class='stars stars-example-bootstrap' style="text-align:center;">
            				<select id="rat<?php echo $produk['id_penjualan_detail']; ?>" name="example-bootstrap" class="rat<?php echo $produk['id_penjualan_detail']; ?>"  onchange="set_rating(<?php echo $produk['id_penjualan_detail']; ?>);" autocomplete='off'>
								<option value='1'>1</option>
            					<option value='2'>2</option>
            					<option value='3'>3</option>
            					<option value='4'>4</option>
            					<option value='5'>5</option>
            				</select>
            			</div>
						<input type="hidden" name="rating<?php echo $produk['id_penjualan_detail']; ?>" id="rating<?php echo $produk['id_penjualan_detail']; ?>" value="">
						<textarea name="ulasan<?php echo $produk['id_penjualan_detail']; ?>" rows="7" cols="67"></textarea>
						<hr/>
						<br/>
					   <?php } } ?>
					   </div>
					   <div class="modal-footer">
						 <button type="Submit" class="btn btn-primary">Kirim</button>
						 <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					   </div>
					 </div>
					 </form>
				   </div>
				 </div>
			 <?php
			   
			 }
		   ?>
			</tbody>
		</table>
	</div>
	<div role='tabpanel' class='tab-pane fade in' id='status5' aria-labelledby='status5-tab'>
		<table id='example6' style='overflow-x:scroll; width: 96%' class="table table-striped table-condensed">
			<thead>
			  <tr>
				<th width="20px">No</th>
				<th>Kode Transaksi</th>
				<th>Nama Merchant</th>
				<th>Jumlah Produk</th>
			<th>Total Belanja</th>
				<th>Pembayaran</th>
				<th>Waktu Order</th>
				<th>Status</th>
				<th width='50px'></th>
			  </tr>
			</thead>
			<tbody>
			  <?php 
				$no = 1;
				foreach ($record5->result_array() as $row){
				// if ($row['proses']=='0'){ $proses = '<i class="text-danger">Pending</i>'; }elseif($row['proses']=='1'){ $proses = '<i class="text-success">Proses</i>'; }else{ $proses = '<i class="text-info">Konfirmasi</i>'; }
				$proses = '<i class="text-danger">Pembayaran Gagal</i>';
				$total = $this->db->query("SELECT sum((a.harga_jual*a.jumlah)-a.diskon) as total FROM `rb_penjualan_detail` a where a.id_penjualan='$row[id_penjualan]'")->row_array();
				echo "<tr><td>$no</td>
						  <td><span class='text-success'>$row[kode_transaksi]</span></td>
						  <td><a href='".base_url()."members/detail_reseller/$row[id_reseller]'>$row[nama_reseller]</a></td>
						  <td><span style='color:blue;'>";
						  echo $this->db->query("SELECT id_penjualan from rb_penjualan_detail where id_penjualan='".$row['id_penjualan']."'")->num_rows();
						  echo"</span></td>
						  <td style='color:red;'>Rp ".rupiah($total['total']+$row['ongkir']+$row['asuransi']-$row['diskon']-$row['diskon_ongkir'])."</td>
						  <td>";
						  if (in_array($row['jenis_pembayaran'],array("bca","bri","mandiri","permata","bni"))){
						  echo "Transfer Bank ".strtoupper($row['pembayaran']);
						  }else{
							echo strtoupper($row['pembayaran']);
						  }
						  echo "</td>";
						  echo "<td>$row[waktu_transaksi]</td>";
						  echo "<td>$proses</td>
						  <td width='50px'>";
						  
						  echo "<a class='btn btn-info btn-xs' title='Detail data pesanan' href='".base_url()."members/keranjang_detail/$row[id_penjualan]'><span class='glyphicon glyphicon-search'></span></a>
						  
						  </td>
					  </tr>

					  ";
				  $no++;
				}
			  ?>
			</tbody>
		</table>
	</div>
</div>
	</div>	
</div>


<style>
    .dataTables_filter{
        float: left !important;
    }
</style>

<script>
$(document).ready(function() {
    $('#example6').DataTable({"paging": true,
		          "lengthChange": false,          
				  "searching": true,          
				  "ordering": true,
				  "info": true,
				  "autoWidth": false,
				});
	$('#example7').DataTable({"paging": true,
		          "lengthChange": false,          
				  "searching": true,          
				  "ordering": true,
				  "info": true,
				  "autoWidth": false,
				});
	$('#example8').DataTable({"paging": true,
		          "lengthChange": false,          
				  "searching": true,          
				  "ordering": true,
				  "info": true,
				  "autoWidth": false,
				});
	$('#example9').DataTable({"paging": true,
		          "lengthChange": false,          
				  "searching": true,          
				  "ordering": true,
				  "info": true,
				  "autoWidth": false,
				});
				
} );
</script>

<style type="text/css">
    a[disabled="disabled"] {
		opacity: .4;
  		cursor: default !important;
  		pointer-events: none;
    }
</style>

<script>
$("#detail-tab").click(function(){
	$("#hapus").show();
	// alert('bisa');
	$('.nav-tabs a[href="#status1"]').tab('show');
});
$("#status1-tab").click(function(){
	$("#hapus").removeAttr("style").hide();
	// alert('bisa');
    // $('[href="' + lastTab + '"]').tab('show');
 });
 $("#status2-tab").click(function(){
	$("#hapus").removeAttr("style").hide();
	// alert('bisa');
    // $('[href="' + lastTab + '"]').tab('show');
 });
 $("#status3-tab").click(function(){
	$("#hapus").removeAttr("style").hide();
	// alert('bisa');
    // $('[href="' + lastTab + '"]').tab('show');
 });
 $("#status4-tab").click(function(){
	$("#hapus").removeAttr("style").hide();
	// alert('bisa');
    // $('[href="' + lastTab + '"]').tab('show');
 });
 $("#status5-tab").click(function(){
	$("#hapus").removeAttr("style").hide();
	// alert('bisa');
    // $('[href="' + lastTab + '"]').tab('show');
 });
</script>

<script type="text/javascript" src="<?php echo base_url(); ?>template/<?php echo template(); ?>/bar_rating/jquery.barrating.min.js"></script>

    <script type="text/javascript">
    $(window).load(function() {
        $("[name=example-bootstrap]").barrating({
                  theme: 'bootstrap-stars',
                  showSelectedRating: false,
                });
		$("[name=example-bootstrap2]").barrating({
                  theme: 'bootstrap-stars',
                  showSelectedRating: false,
				  readonly: true,
                });
        $('.sp-wrap').smoothproducts();
    });

	function set_rating(set) {
	
	document.getElementById("rating"+set).value = document.getElementById("rat"+set).value;
		
	}
    </script>