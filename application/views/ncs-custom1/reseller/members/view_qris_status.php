<style>
	.row {
		margin-right: 0 !important;
		margin-left: 0 !important;
	}
	
	.content-status{
		padding: 0;
	}
	
	.content-status .icon-status{
		text-align:center
	}
	
	.body-status, .body-status-main{
		display: block;
	}
	
	.content-status .body-status{
		width:500px;
		min-height:300px;
		background-color:#dff0d8;
		margin-top:-40px;
		border-radius:10px;
		width:100%;
		padding : 0px;
	}
	
	.body-status-main{
		padding : 50px 20px 20px 20px;
		color:#696f74;
		font:'Arial';
	}
</style>


<p class='sidebar-title block-title'> Konfirmasi Pembayaran Pesanan Anda</p>

<?php 
	if($status == 'sukses'){
		?>
			<div class='alert alert-success' style='margin:10px 0px'><center>Pembayaran Berhasil</center></div>
		<?php
	}else{
		?>
			<div class='alert alert-warning' style='margin:10px 0px'><center>Pembayaran Tidak Terkonfirmasi</center></div>
		<?php
	}
?>		



<?php
$row = $this->db->query("SELECT * FROM rb_penjualan where id_penjualan='$record[id_penjualan]'")->row_array();
?>
<div id="paymentinfo">
	<div>
		<div class="row" style="margin-bottom:40px">
			<div class="col-3 col-md-3 col-sm-3 col-xs-12">
			</div>
			<div class="col-6 col-md-6 col-sm-6 col-xs-12 style="padding: 10px 0">
					<?php 
						if($status == 'sukses'){
							foreach ($record as $row){
							?>	
								<div class="content-status">
									<div class="icon-status">
										<img height="80px" src="<?php echo base_url()?>asset/gif/success transparent.gif?x=<?php echo time();?>"/>
									</div>
									<div class="body-status">
										<div class="body-status-main">
											<table style="width:100%;margin-bottom:20px;">
												<tr>
													<td style="text-align:left" colspan="2">
														<span>Status</span>
													</td>
												</tr>
												<tr>
													<td style="text-align:left">
														<span style="color:#f08519">Pembayaran Berhasil</span>
													</td>
													<td style="text-align:right">
														<span><?php echo $row['res_paymentDate']; ?></span>
													</td>
												</tr>
											</table>
											<hr />
											<br />
											<table style="width:100%;margin-bottom:15px;">
												<tr>
													<td style="text-align:left">
														<span>No Pemesanan</span>
													</td>
													<td style="text-align:right">
														<span><?php echo $row['kode_transaksi']; ?></span>
													</td>
												</tr>
											</table>
											<table style="width:100%;margin-bottom:15px;">
												<tr>
													<td style="text-align:left">
														<span>Metode Pembayaran</span>
													</td>
													<td style="text-align:right">
														<span><?php echo $row['via']; ?></span>
													</td>
												</tr>
											</table>
											<table style="width:100%;margin-bottom:15px;">
												<tr>
													<td style="text-align:left">
														<span>Total Bayar</span>
													</td>
													<td style="text-align:right">
														<span>Rp. <?php echo rupiah($row['nominal']); ?></span>
													</td>
												</tr>
											</table>
										</div>
									</div>
								</div>
							<?php
							}
						}else{
							?>
								<a class='btn btn-link btn-md' style='margin-left:5px;' href='<?php echo base_url();?>konfirmasi/qris?idp=<?php echo $idp;?>'>Generate Ulang QRCode</a> <br /> <br />
							<?php
						}
					?>		
			</div>			
			<div class="col-3 col-md-3 col-sm-3 col-xs-12">
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<a class='btn btn-info btn-md' style='margin-left:5px;' href='<?php echo base_url();?>members/orders_report'><span class='glyphicon glyphicon-shopping-cart'></span> Lihat daftar pesanan</a>
			</div>
		</div>
	</div>
</div>



