
	<p class='sidebar-title text-danger produk-title'>Pesanan anda | Keranjang</p>
	<div class='col-md-12'>
	<?php 
		echo "<form action='".base_url()."members/selesai_belanja' method='POST'>";
		echo $error_reseller; 
		  if ($total_checkout == 0){
			echo "<center style='padding:10%'><i class='text-danger'>Maaf, Keranjang belanja anda saat ini masih kosong,...</i><br>
					<a class='btn btn-warning btn-sm' href='".base_url()."members/reseller'>Klik Disini Untuk mulai Belanja!</a></center>";
		  }else{
	?>

	<div id="div_refresh">
      <div style="clear:both"><br></div>
      <table class="table table-striped table-condensed">
          <tbody>
				<?php 
				  $no = 1;
				  $no_s = 1;
				  foreach ($rows as $ro){
					
					echo "<tr><td colspan='6' style='background:#cacaca;'> $ro[nama_reseller]</td></tr>";
					$produk = $this->db->query("SELECT * FROM `rb_penjualan_temp` a join rb_produk b on a.id_produk=b.id_produk join rb_reseller c ON b.id_reseller=c.id_reseller JOIN rb_kota d ON c.kota_id=d.kota_id where a.session='".$this->session->id_konsumen."' and c.id_reseller='".$ro['id_reseller']."' ORDER BY a.id_penjualan_detail ASC")->result_array();
					foreach ($produk as $row){

						$jual = $this->db->query("SELECT sum(jumlah) as jual FROM `rb_penjualan` a JOIN rb_penjualan_detail b ON a.id_penjualan=b.id_penjualan where a.status_pembeli='konsumen' AND a.status_penjual='reseller' AND b.id_produk='".$row['id_produk']."' AND a.proses='1' AND a.batal='0' ")->row_array()['jual'];
						$beli = $this->db->query("SELECT sum(jumlah) as beli FROM `rb_penjualan` a JOIN rb_penjualan_detail b ON a.id_penjualan=b.id_penjualan where a.status_pembeli='reseller' AND a.status_penjual='admin' AND b.id_produk='".$row['id_produk']."' AND a.proses='1'")->row_array()['beli'];
						$stok = $beli-$jual;

						if($row['jumlah']>$stok){
							if($stok>0){
								$alert_stock="<p style='color:red;'>Quantity Barang Melebihi Jumlah Stock (Max quantity $stok)</p> ";
							}else{ 
								$alert_stock ="<p style='color:red;'>Stock Habis</p>";
							}
						}else{
							$alert_stock='';
						}

				  $ex = explode(';', $row['gambar']);
				  if (trim($ex[0])==''){ $foto_produk = 'no-image.png'; }else{ $foto_produk = $ex[0]; }

				  $sub_total = ($row['harga_jual']*$row['jumlah'])-$row['diskon'];
				  echo "<tr><td>$no</td>
							<td width='70px'><img style='border:1px solid #cecece; width:60px' src='".base_url()."asset/foto_produk/$foto_produk'></td>
							<td><a style='color:#ab0534' href='".base_url()."produk/detail/$row[produk_seo]'><b>$row[nama_produk]</b></a>
								<br>Qty. <b>$row[jumlah]</b>, Harga Rp ".rupiah($row['harga_jual']-$row['diskon'])." / $row[satuan], 
								<br>Berat. <b>".($row['berat']*$row['jumlah'])." Gram</b>
								$alert_stock
								</td>

								<td> 
								<input type='button' value='+' onclick='tambah_$row[id_penjualan_detail]()' style='width: 50px;'>
								<input type='text' id='jml_qty_$row[id_penjualan_detail]' value='$row[jumlah]' style='width: 50px; text-align:center;' onfocus='let value = this.value; this.value = null; this.value=value'> 
								<input type='hidden' id='id_penjualan_detail_$row[id_penjualan_detail]' value='$row[id_penjualan_detail]' > 
								<input type='button' id='kur_$row[id_penjualan_detail]' name='kur_$row[id_penjualan_detail]' value='-' onclick='kurang_$row[id_penjualan_detail]()' style='width: 50px;'>
								</td>

							<td>Rp ".rupiah($sub_total)."</td>
							<td width='30px'><a class='btn btn-danger btn-xs' title='Delete' href='".base_url()."members/keranjang_delete/$row[id_penjualan_detail]/$row[id_reseller]'><span class='glyphicon glyphicon-remove'></span></a></td>
						</tr>";
					$no++;
					
				  }
				  $no=1;
				  $no_s++;
				}
				// $total = $this->db->query("SELECT sum((a.harga_jual*a.jumlah)-a.diskon) as total, sum(b.berat*a.jumlah) as total_berat FROM `rb_penjualan_detail` a JOIN rb_produk b ON a.id_produk=b.id_produk where a.id_penjualan='".$this->session->idp."'")->row_array();
				$total = $this->db->query("SELECT sum((a.harga_jual*a.jumlah)-a.diskon) as total, sum(b.berat*a.jumlah) as total_berat FROM `rb_penjualan_temp` a JOIN rb_produk b ON a.id_produk=b.id_produk where a.session='".$this->session->id_konsumen."'")->row_array();
          echo "<tr class='success'>
                  <td colspan='3'><b>Total Berat</b></td>
                  <td colspan='2'><b>$total[total_berat] Gram</b></td>
                  <td></td>
                </tr>
			</tbody>
		</table>

		<div class='col-md-4 pull-right'>
			<center>Total Bayar <br><h2 id='totalbayar'></h2> 
			<a class='btn btn-success btn-sm' href='".base_url()."members/checkout'>Checkout Belanja</a>
			<!--<button type='submit' name='submit' class='btn btn-success btn-flat btn-sm'>Checkout Belanja</button>-->
			</center>
		</div>";
      $ket = $this->db->query("SELECT * FROM rb_keterangan where id_reseller='".$rows['id_reseller']."'")->row_array();
      $diskon_total = '0';
?>

	<input type="hidden" name="total" id="total" value="<?php echo $total['total']; ?>"/>
	<input type="hidden" name="ongkir" id="ongkir" value="0"/>
	<input type="hidden" name="berat" value="<?php echo $total['total_berat']; ?>"/>
	<input type="hidden" name="diskonnilai" id="diskonnilai" value="<?php echo $diskon_total; ?>"/>
	<!--<div class="form-group">
		<label class="col-sm-12 control-label" for="">Pilih Kurir</label>
		<div class="col-md-10">
			<?php       
			//$kurir=array('ncs','jne','tiki','pos');
			$kurir=array('ncs kurir');
			foreach($kurir as $rkurir){
				?>          
					<label class="radio-inline">
					<input type="radio" name="kurir" class="kurir" value="<?php echo $rkurir; ?>"/> <?php echo strtoupper($rkurir); ?>
					</label>
				<?php
			}
			?>
			 <!-- <label class="radio-inline"><input type="radio" name="kurir" class="kurir" value="cod"/> COD (Cash on delivery)</label> -->
		<!--</div>
	</div>
	<div id="kuririnfo" style="display: none;">
		<div class="form-group">
			<div class="col-md-12">
				<br/>
				<br/>
				<div class='alert alert-info' style='padding:5px; border-radius:0px; margin-bottom:0px' >SERVICE</div>
				<br/>
				<p class="form-control-static" id="kurirserviceinfo"></p>
			</div>
		</div>
	</div>-->
	</div>
<?php echo form_close();?>


<script>
	$(document).ready(function(){
	hitung();
	});

	function hitung(){
		var total=$('#total').val();
		$("#totalbayar").html(toDuit(total));
	}
</script>

	<?php 
		echo "<div style='clear:both'></div><hr> <br>$ket[keterangan]"; 
	?>
	<?php 
        echo "<a class='btn btn-success btn-sm' href='".base_url()."produk'>Lanjut Belanja</a>
              <a class='btn btn-danger btn-sm' href='".base_url()."members/batalkan_transaksi' onclick=\"return confirm('Apa anda yakin untuk Batalkan Transaksi ini?')\">Batalkan Transaksi</a>"; 
			  
			  
	}
?>

	
<script>

	function hitung(){
		var diskon=$('#diskonnilai').val();
		var total=$('#total').val();
		var ongkir=$('#ongkir').val();
		var bayar=(parseFloat(total)+parseFloat(ongkir));
		if(parseFloat(ongkir) > 0){
			$('#oksimpan').show();
		}else{
			$('#oksimpan').hide();
		}
		$('#totalbayar').html(toDuit(bayar));
	}

<?php
	foreach ($record as $row){		
		
	echo "function tambah_$row[id_penjualan_detail](){
		var jml_qty = parseInt(document.getElementById('jml_qty_$row[id_penjualan_detail]').value);
		var id_penjualan_detail = document.getElementById('id_penjualan_detail_$row[id_penjualan_detail]').value;
		var total= jml_qty + 1;";
		$jual = $this->model_reseller->jual_reseller($row['id_reseller'],$row['id_produk'])->row_array();
    	$beli = $this->model_reseller->beli_reseller($row['id_reseller'],$row['id_produk'])->row_array();
	?>
		if(total><?php echo ($beli['beli']-$jual['jual']); ?>){
			alert('Gagal Tidak Bisa Melebihi Stock');
		}else{
		$.ajax({
		type: 'POST',
		url: '<?php echo base_url(); ?>members/refresh_keranjang',
		dataType: 'JSON',
		data: { id: id_penjualan_detail, total: total},
		success: function(response) {
		$('#div_refresh').html(response);
		hitung();
		
		}
		});
		}
	<?php echo "}"; ?>

	<?php
	echo "function kurang_$row[id_penjualan_detail](){
		var jml_qty = parseInt(document.getElementById('jml_qty_$row[id_penjualan_detail]').value);
		var id_penjualan_detail = document.getElementById('id_penjualan_detail_$row[id_penjualan_detail]').value;
		var total= jml_qty - 1;";
	?>
		if(total<1){
			alert('Gagal Tidak Bisa Mengurangi');
		}else{
		$.ajax({
		type: 'POST',
		url: '<?php echo base_url(); ?>members/refresh_keranjang',
		dataType: 'JSON',
		data: { id: id_penjualan_detail, total: total},
		success: function(response) {
		$('#div_refresh').html(response);
		hitung();
		}
		});
		}
	<?php echo "}";?>

$(document).on('keypress keydown keyup', '#jml_qty_<?php echo $row["id_penjualan_detail"];?>', function(){
var $this = $(this);
	setTimeout(function() {
		var total = $this.val();
			if ((total.trim()!='') && (total>0)){
				var id_penjualan_detail = document.getElementById('id_penjualan_detail_<?php echo $row["id_penjualan_detail"];?>').value;
				<?php
					$jual = $this->model_reseller->jual_reseller($row['id_reseller'],$row['id_produk'])->row_array();
					$beli = $this->model_reseller->beli_reseller($row['id_reseller'],$row['id_produk'])->row_array();
				?>
				if(total><?php echo ($beli['beli']-$jual['jual']); ?>){
					alert('Gagal Tidak Bisa Melebihi Stock');
					document.getElementById('<?php echo "jml_qty_".$row["id_penjualan_detail"];?>').focus();
				}else{
				$.ajax({
				type: 'POST',
				url: '<?php echo base_url(); ?>members/refresh_keranjang',
				dataType: 'JSON',
				data: { id: id_penjualan_detail, total: total},
				success: function(response) {
		   		$('#div_refresh').html(response);
				document.getElementById('<?php echo "jml_qty_".$row["id_penjualan_detail"];?>').focus();
				hitung();
				}
			});
			}
		}
	}, 0);
})

	<?php } ?>
	
</script>

</div>
<!--<div class="col-sm-4 colom4">
	<?php $res = $this->db->query("SELECT a.*, b.nama_kota, c.nama_provinsi FROM rb_reseller a JOIN rb_kota b ON a.kota_id=b.kota_id 
								JOIN rb_provinsi c ON b.provinsi_id=c.provinsi_id
								where a.id_reseller='$rows[id_reseller]'")->row_array(); ?>
	<table class='table table-condensed'>
		<tbody>
			<tr class='alert alert-info'><th scope='row' style='width:90px'>Pengirim</th> <td><?php echo $res['nama_reseller']?></td></tr>
			<tr class='alert alert-info'><th scope='row'>Alamat</th> <td><?php echo $res['alamat_lengkap'].', '.$res['nama_kota'].', '.$res['nama_provinsi']; ?></td></tr>
		</tbody>
	</table>

	<?php $usr = $this->db->query("SELECT a.*, b.nama_kota, c.nama_provinsi FROM rb_konsumen a JOIN rb_kota b ON a.kota_id=b.kota_id 
									JOIN rb_provinsi c ON b.provinsi_id=c.provinsi_id
									where a.id_konsumen='".$this->session->id_konsumen."'")->row_array(); ?>
	<table class='table table-condensed'>
		<tbody>
			<tr class='alert alert-danger'><th scope='row' style='width:90px'>Penerima</th> <td><?php echo $usr['nama_lengkap']?></td></tr>
			<tr><th scope='row'>Alamat</th> <td><?php echo $usr['alamat_lengkap'].', '.$usr['nama_kota'].', '.$usr['nama_provinsi']; ?></td></tr>
		</tbody>
	</table>
    <img style='width:100%' src='<?php echo base_url(); ?>asset/foto_pasangiklan/ekpedisi2.jpg'>
</div>-->


