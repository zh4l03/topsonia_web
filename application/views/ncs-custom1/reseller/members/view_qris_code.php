<p class='sidebar-title block-title'> Konfirmasi Pembayaran Pesanan Anda</p>

<?php 

?>

<style>
	#clockdiv{
	  font-family: sans-serif;
	  color: #fff;
	  display: inline-block;
	  font-weight: 100;
	  text-align: center;
	  font-size: 30px;
	}

	#clockdiv > div{
	  padding: 10px;
	  border-radius: 3px;
	  background: #efb95e;
	  display: inline-block;
	}

	#clockdiv div > span{
	  padding: 15px;
	  border-radius: 3px;
	  background: #f08519;
	  display: inline-block;
	}

	.smalltext{
	  padding-top: 5px;
	  font-size: 16px;
	}
</style>

<?php 
	if($status == 'ok'){
		?>
			<div class='alert alert-success' style='margin:10px 0px'><center>Buka aplikasi E-Wallet anda dan scan QR Code dibawah <br> untuk menyelesaikan pembayaran dalam waktu 5 menit!</center></div>
		<?php
	}else{
		?>
			<div class='alert alert-danger' style='margin:10px 0px'><center>Tidak dapat memuat QR Code</center></div>
		<?php
	}
?>	


<?php
$row = $this->db->query("SELECT * FROM rb_penjualan where id_penjualan='$record[id_penjualan]'")->row_array();
?>
<div id="paymentinfo">
	<div class="form-group">
		<div class="row" style="margin-bottom:10px;">
			<div class="col-12">
				<center>
					<div style="min-height:40px;width:400px;background-color:#eeeeee;border-radius:5px;padding:10px">
						Total Bayar <br />
						<span style="color:#f08519;font-size:20px">Rp. <b><?php echo rupiah($totalbayar);?></b></span>
					</div>
				</center>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<center>
					<?php 
						if($status == 'ok'){
							?>
								<img style="height:250px" src="<?php echo base_url()?>asset/qrcode/<?php echo $row[kode_transaksi].'.png'?>" /> <br />
								<img style="height:30px" src="<?php echo base_url()?>asset/ket_pembayaran/logo_qris_black_full.png" /> <br /> <br />
								<img style="height:60px" src="<?php echo base_url()?>asset/ket_pembayaran/logo_qris_partner.png"/> <br /> <br />
								<img style="height:60px" src="<?php echo base_url()?>asset/gif/loading-transparent.gif"/> <br />
								<div id="clockdiv">
									<div>
										<span class="minutes"></span>
										<div class="smalltext">Menit</div>
									</div>
									<div>
										<span class="seconds"></span>
										<div class="smalltext">Detik</div>
									</div>
								</div>
							<?php
						}else{
							?>
								<a class='btn btn-link btn-md' style='margin-left:5px;' href='<?php echo base_url();?>konfirmasi/qris?idp=<?php echo $idp;?>'>Generate Ulang QRCode</a> <br /> <br />
							<?php
						}
					?>	
				</center>
			</div>
		</div>
	</div>
</div>

<div class='box-footer'>
	<a class='btn btn-info btn-md' style='margin-left:5px;' href='<?php echo base_url();?>members/orders_report'><span class='glyphicon glyphicon-shopping-cart'></span> Lihat Daftar Pesanan</a>
	
	<a class='btn btn-info btn-md pull-right' style='margin-left:5px;' href='<?php echo base_url();?>konfirmasi/checkStatusManual?qrisno=<?php echo $no_qris;?>&idp=<?php echo $idp;?>'>Cek Status Pembayaran</a>
</div>

<script>
	$(document).ready(function(){	
		
		<?php $query = $this->db->query("SELECT DATE_ADD(waktu_order, INTERVAL 5 MINUTE) as TimeOrder FROM `rb_penjualan` WHERE kode_transaksi = '".$kode_trx."'")->row_array(); ?>
		
		const deadline="<?php echo $query['TimeOrder']; ?>";	
		var trxno="<?php echo $kode_trx; ?>";
		var qrisno="<?php echo $no_qris; ?>";
		var idp="<?php echo $idp; ?>";
		var isPaused = false;

		// const deadline = new Date(Date.parse(new Date()) + 15 * 24 * 60 * 60 * 1000);
		initializeClock('clockdiv', deadline);
		
		function getTimeRemaining(endtime) {
			const total = Date.parse(endtime) - Date.parse(new Date());
			const seconds = Math.floor((total / 1000) % 60);
			const minutes = Math.floor((total / 1000 / 60) % 60);
			const hours = Math.floor((total / (1000 * 60 * 60)) % 24);
			const days = Math.floor(total / (1000 * 60 * 60 * 24));

			return {
				total,
				days,
				hours,
				minutes,
				seconds
			};
		}

		function initializeClock(id, endtime) {
			const clock = document.getElementById(id);
			// const daysSpan = clock.querySelector('.days');
			// const hoursSpan = clock.querySelector('.hours');
			const minutesSpan = clock.querySelector('.minutes');
			const secondsSpan = clock.querySelector('.seconds');

			function updateClock() {
				const t = getTimeRemaining(endtime);

				// daysSpan.innerHTML = t.days;
				// hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
				minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
				secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

				if (t.total <= 0) {
					
					clearInterval(timeinterval);
					// alert('tadaaaa');
					// var trxno="<?php echo $kode_trx; ?>";
					window.location.href = "<?php echo base_url(); ?>konfirmasi/checkStatusPayment?qrisno="+qrisno+"&idp="+idp;
					
				}
			}

			updateClock();
			const timeinterval = setInterval(updateClock, 1000);
		}
		
		
		setInterval(function(){ checkStatusPayment(); }, 3000);	
		
		function checkStatusPayment() {	
			if(!isPaused) {
				isPaused = true;
				$.ajax({
					method: "get",
					dataType:"html",
					url: "<?php echo base_url(); ?>konfirmasi/checkStatusRealtime",
					data: "qrisno="+qrisno+"&idp="+idp,
					beforeSend:function(){
						// $("#oksimpan").hide();
					}
				})
				.done(function( result ) {  
					var data = JSON.parse(result);
					var response = data['responseStatus'];
					
					if(response == 'Success'){
						window.location.href = "<?php echo base_url(); ?>konfirmasi/qrisStatus?idp="+idp+"&status=sukses";
					}	

					isPaused = false;					
				})
				.fail(function() {
					isPaused = false;
				});
			}
		}
		
	});
</script>



