<p class='sidebar-title text-danger produk-title'> Detail Pesanan Anda</p>
<div class="col-sm-12">
	<div class="col-sm-8">
      <table class="table table-striped table-condensed">
          <tbody>
        <?php 
          $no = 1;
          foreach ($record as $row){
          $ex = explode(';', $row['gambar']);
          if (trim($ex[0])==''){ $foto_produk = 'no-image.png'; }else{ $foto_produk = $ex[0]; }
          $sub_total = ($row['harga_jual']*$row['jumlah'])-$row['diskon'];
          echo "<tr><td>$no</td>
                    <td width='70px'><img style='border:1px solid #cecece; width:60px' src='".base_url()."asset/foto_produk/$foto_produk'></td>
                    <td><a style='color:#ab0534' href='".base_url()."produk/detail/$row[produk_seo]'><b>$row[nama_produk]</b></a>
                        <br>Qty. <b>$row[jumlah]</b>, Harga. Rp ".rupiah($row['harga_jual']-$row['diskon'])." / $row[satuan], 
                        <br>Berat. <b>".($row['berat']*$row['jumlah'])." Gram</b></td>
                    <td>Rp ".rupiah($sub_total)."</td>
                </tr>";
            $no++;
          }
          $detail = $this->db->query("SELECT * FROM rb_penjualan where id_penjualan='".$this->uri->segment(3)."'")->row_array();
          $total = $this->db->query("SELECT sum((a.harga_jual*a.jumlah)-a.diskon) as total, sum(b.berat*a.jumlah) as total_berat FROM `rb_penjualan_detail` a JOIN rb_produk b ON a.id_produk=b.id_produk where a.id_penjualan='".$this->uri->segment(3)."'")->row_array();
          if ($rows['proses']=='0'){ $proses = '<i class="text-danger">Pending</i>'; $status = 'Proses'; }elseif($rows['proses']=='1'){ $proses = '<i class="text-success">Proses</i>'; }else{ $proses = '<i class="text-info">Konfirmasi</i>'; }
          echo "
                <tr>
                  <td colspan='3'><b>Berat</b> <small><i class='pull-right'>(".terbilang($total['total_berat'])." Gram)</i></small></td>
                  <td><b>$total[total_berat] Gram</b></td>
                </tr>

                <tr>
                  <td colspan='3'><b>Ongkir | <span style='text-transform:uppercase'>$detail[kurir]</span> - $detail[service]</b> <small><i class='pull-right'>(".terbilang($detail['ongkir']).")</i></small></td>
                  <td><b>Rp ".rupiah($detail['ongkir'])."</b></td>
                </tr>

                <tr>
                  <td colspan='3'><b>Total Belanja</b> <small><i class='pull-right'>(".terbilang($total['total'])." Rupiah)</i></small></td>
                  <td><b>Rp ".rupiah($total['total'])."</b></td>
                </tr>

                <tr>
                  <td style='color:Red' colspan='3'><b>Diskon </b> <small><i class='pull-right'>(".terbilang($record2['diskon'])." Rupiah)</i></small></td>
                  <td style='color:Red'><b>(Rp ".rupiah($record2['diskon']).")</b></td>
                </tr>

                <tr class='success'>
                  <td colspan='3'><b>Total Bayar </b> <small><i class='pull-right'>(".terbilang($record2['total_bayar'])." Rupiah)</i></small></td>
                  <td><b>Rp ".rupiah($record2['total_bayar'])."</b></td>
                </tr>

                
                <tr><td align=center colspan='4'><b>$proses</b></td></tr>

        </tbody>
      </table>";
	?>
	</div>

	<div class="col-sm-4 colom44">
	  <?php $res = $this->db->query("SELECT a.*, b.nama_kota, c.nama_provinsi FROM rb_reseller a JOIN rb_kota b ON a.kota_id=b.kota_id 
					JOIN rb_provinsi c ON b.provinsi_id=c.provinsi_id
					  where a.id_reseller='$rows[id_reseller]'")->row_array(); ?>
	  <table class='table table-condensed'>
	  <tbody>
		<tr><th scope='row' style='width:90px'>Pengirim</th> <td><?php echo $res['nama_reseller']?></td></tr>
		<tr><th scope='row'>Telpon</th> <td><?php echo $res['no_telpon']; ?></td></tr>
		<tr><th scope='row'>Alamat</th> <td><?php echo $res['alamat_lengkap'].', '.$res['nama_kota'].', '.$res['nama_provinsi']; ?></td></tr>
	  </tbody>
	  </table>
	  
	<?php $usr = $this->db->query("SELECT a.*, b.nama_kota, c.nama_provinsi FROM rb_konsumen a JOIN rb_kota b ON a.kota_id=b.kota_id 
                JOIN rb_provinsi c ON b.provinsi_id=c.provinsi_id
                  where a.id_konsumen='".$this->session->id_konsumen."'")->row_array(); ?>
	<?php 
		if ($rows['id_alamat']!=''){
			$al = $this->model_app->view_where('alamat',array('id_alamat'=>$rows['id_alamat']))->row_array(); 
			$als = $this->db->query("SELECT a.*, b.nama_kota, c.nama_provinsi FROM alamat a JOIN rb_kota b ON a.kota_id=b.kota_id
									JOIN rb_provinsi c ON b.provinsi_id=c.provinsi_id
									where a.id_alamat='".$rows['id_alamat']."'")->row_array(); 
		?>
	<table class='table table-condensed'>
	<tbody>
		<tr><th scope='row' style='width:90px'>Penerima</th> <td><strong><?php echo $al['pic']?></strong> (<?php echo $al['nama_alamat']?>)</td></tr>
		<tr><th scope='row'>No Telpon</th> <td><?php echo $al['no_hp']; ?></td></tr>
		<tr><th scope='row'>Alamat</th> <td><?php echo $al['alamat_lengkap'].', '.$als['nama_kota'].', '.$als['nama_provinsi'].', '.$als['kecamatan'].', '.$als['kode_pos']; ?></td></tr>
	</table>
	<?php }else{ ?>
		
	<table class='table table-condensed'>
	  <tbody>
		<tr class='alert alert-danger'><th scope='row' style='width:90px'>Penerima</th> <td><?php echo $usr['nama_lengkap']?></td></tr>
		<tr><th scope='row'>No Telpon</th> <td><?php echo $usr['no_hp']; ?></td></tr>
		<tr><th scope='row'>Alamat</th> <td><?php echo $usr['alamat_lengkap'].', '.$usr['nama_kota'].', '.$usr['nama_provinsi'].', '.$usr['kecamatan'].', '.$usr['kode_pos']; ?></td></tr>
	  </tbody>
	</table>
	<?php } ?>
	</div>
	<hr>
	<?php
	if ($detail['awb'] != 0){
	?>
		<table class="table table-striped table-condensed">
		<thead>
			<tr>
				PENGIRIMAN
			</tr>
			<hr/>
			<!--<tr><td style='width:40%'>NCS Kurir | No Transaksi</td></tr>-->
			
		</thead>
		<tbody>
			<div class="row">
				<div class="col-md-2">
				<br><br><br><br><br>
					<?php
					echo"<br><img style='width:100%' src='".base_url()."asset/foto_pasangiklan/ekpedisi2.png'>";?>
					<!--<div style="border: 1px #c3c4c6 solid; height: 250px; width: 0px;"></div>-->
				</div>
				<div class="col-md-8">
					<ul class="timelinee">
						<?php 
							$url = 'https://apimobile.ptncs.com/php/ncs_deal/getCheckpointListiDss.php?AWB='.$detail['awb'];
							
							$ch = curl_init();
							curl_setopt($ch,CURLOPT_URL, $url);
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
							
							$server_output = curl_exec($ch);
							
							curl_close($ch);
							
							
							$data = json_decode($server_output, true);
							for($i=0;$i<count($data);$i++){
								?>
									<li>
										<a target="_blank" href="#"><?php echo $data[$i]['Checkpointt']?></a>
										<a href="#" class="float-right"><?php echo $data[$i]['Date']?> (<?php echo $data[$i]['Time']?>)</a>
										<p><?php echo $data[$i]['Branch']?></p>
										<!--<p><?php echo $data[$i]['Remarks']?></p>
										<p><?php echo $data[$i]['Reason']?></p>
										<p><?php echo $data[$i]['Recipient']?></p>
										<p><?php echo $data[$i]['Courier']?></p>-->
									</li>
								<?php
							}
						?>
					</ul>
				</div>
			</div>
		</tbody>
	</table>
	<?php
	}else{
	?>	
	<table class="table table-striped table-condensed">
		<thead>
			<tr><center></p>PENGIRIMAN</p></center></tr><br/><br/><hr/>
			<tr><center></p>Airwabill tidak tersedia</p></center></tr>
			
			
	</table>
		
	<?php
	}
	?>
	 
	<?php
	echo"<a class='btn btn-default btn-sm' href='".base_url()."members/orders_report'>Kembali</a>";
	?>
</div>