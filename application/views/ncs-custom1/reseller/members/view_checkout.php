<?php if($this->session->userdata('flag_midtrans_payment')==='1'){ ?>
	<script type="text/javascript"
            src="https://app.sandbox.midtrans.com/snap/snap.js"
            data-client-key="SB-Mid-client-Qfhvh3O-_JgrNnxY"></script>
</script>
<?php } ?>

<link rel="stylesheet" type="text/css" href="<?php base_url(); ?>template/ncs-custom1/font-awesome/font-awesome.min.js" />

<p class='sidebar-title text-danger produk-title' style="text-align: center;">Pesanan anda | Checkout</p>

<form action='<?php echo base_url(); ?>members/selesai_belanja/<?php echo $this->uri->segment(4); ?>' method='post'>

  <div class='col-md-12'>

    <h4><span style="color:#f08519;" class="glyphicon glyphicon-map-marker" />ALAMAT PENERIMA</h4>

    <?php if ($alm['id_alamat'] != '') { ?>
      <table class='table table-condensed'>
        <tbody>
          <tr>
            <th scope='row' style='width:90px'>Penerima</th>
            <td><strong>(<?php echo $alm['nama_alamat'] ?>)</strong> <?php echo $alm['pic'] ?></td>
          </tr>
          <tr>
            <th scope='row'>Alamat</th>
            <td><?php echo $alm['alamat_lengkap'] . ', ' . $alm['nama_provinsi'] . ', ' . $alm['nama_kota'] . ', ' . $alm['kecamatan'] . ', ' . $alm['kode_pos']; ?></td>
          </tr>
        </tbody>
      </table>
      <div class="form-group">
        <table class='table table-condensed'>
          <tbody>
            <?php echo "<td><center><a class='btn btn-warning btn-xs pull-left' style='margin-left:5px;' href='" . base_url() . "members/view_address/checkout/$kode_transaksi'>Ganti alamat pengiriman</a></center>"; ?>
          </tbody>
        </table>
      </div>
    <?php
    } else {
    ?>
      <table class='table table-condensed'>
        <tbody>
          <tr>
            <th scope='row' style='width:150px'>Alamat Pengiriman</th>
            <td>
              <p id="alamat_alert">Alamat Pengiriman belum tersedia</p><a class='btn btn-success btn-sm pull-left' style='margin-left:5px;' <?php echo " href='" . base_url() . "members/view_address/checkout/$kode_transaksi'"; ?>><span class='glyphicon glyphicon glyphicon-plus'></span> Tambah Alamat</a>
        </tbody>
      </table>

    <?php } ?>

    <hr />
    <br />

  </div>


  <div class="col-sm-12">
    <H3 style="text-align: center;">DAFTAR PESANAN</H3>
    <hr style='border: 1px #c3c4c6 solid;width:94%;text-align:left;margin-left:0' />
  </div>

  <?php
  $no = 1;
  $noo = 1;
  $komisi_user = 0;
  $total_belanja = 0;
  $total_diskon_all = 0;
  foreach ($record as $row) {
  ?>

    <div class='col-md-7'>
      <table class='table table-striped table-condensed'>
        <tbody>
          <?php
          $produk = $this->db->query("SELECT * FROM `rb_penjualan_temp` a join rb_produk b on a.id_produk=b.id_produk join rb_reseller c ON b.id_reseller=c.id_reseller JOIN rb_kota d ON c.kota_id=d.kota_id where a.session='" . $this->session->id_konsumen . "' and c.id_reseller='" . $row['id_reseller'] . "' ORDER BY a.id_penjualan_detail ASC")->result_array();
          foreach ($produk as $prod) {
            $ex = explode(';', $prod['gambar']);
            if (trim($ex[0]) == '') {
              $foto_produk = 'no-image.png';
            } else {
              $foto_produk = $ex[0];
            }

            $sub_total = ($prod['harga_jual'] * $prod['jumlah']) - $prod['diskon'];
           
          ?>

            <tr class="spacer" style="margin-bottom:15px !important;">
              <td><?= $no ?></td>
              <td width="70px"><img style="border:1px solid #cecece; width:60px" src="<?= base_url() ?>asset/foto_produk/<?= $foto_produk ?>"></td>
              <td>
                <a style="color:#ab0534" href="<?= base_url() ?>produk/detail/<?= $prod['produk_seo'] ?>"><b><?= $prod['nama_produk'] ?></b></a>
                <br>Qty <b> <?= $prod['jumlah'] ?></b> , Harga Rp.<?= rupiah($prod['harga_jual'] - $prod['diskon']) ?>/<?= $prod['satuan'] ?>,
                <br>Berat <b> <?= ($prod['berat'] * $prod['jumlah']) ?> Gram</b>
              </td>
              <td style="text-align:center">Rp <?= rupiah($sub_total) ?></td>
              <td width="30px"></td>
            </tr>

          <?php
            $no++;
          }
          ?>
          <tr>
            <td colspan="4"><input type="text" class="form-control" placeholder="Catatan Pelapak" name="catatan_pelapak<?= $noo ?>" id="catatan_pelapak<?= $noo ?>">
              <input type="hidden" class="form-control" placeholder="Catatan Pelapak" name="berat_total<?= $noo ?>" id="berat_total<?= $noo ?>" value="<?php echo $row['brt']; ?>">
              <?php 
              $id_tipe_buyer =  $this->db->query("SELECT id_tipe_buyer FROM rb_konsumen where id_konsumen='" . $this->session->id_konsumen . "' ")->row_array()['id_tipe_buyer'];
              if($id_tipe_buyer =='3'){
                $total_b_seller = $row['total'];
                $disk = $row['komisi_top']*($row['diskon_buyer']/100);
                $diskon_belanja = $disk;
                $total_diskon_all = $diskon_belanja+$total_diskon_all;
              }else{
                $total_b_seller = $row['total'];
                $diskon_belanja = 0;
              }
              ?>
              <input type="hidden" class="form-control" name="seller_total<?= $noo ?>" id="seller_total<?= $noo ?>" value="<?=$total_b_seller?>">
              <input type="hidden" class="form-control" name="diskon_belanja<?= $noo ?>" id="diskon_belanja<?= $noo ?>" value="<?=$diskon_belanja?>">
            </td>
          </tr>
        </tbody>
      </table>
    </div>

    <div class="col-sm-4">
      <div class="border">
        <table class='table table-condensed'>

          <?php
          $komisi_user = $row['komisi_user'] + $komisi_user;
          $total_belanja = $row['total'] + $total_belanja;
          ?>

          <tbody>
            <tr>
              <th scope='row' style='width:90px'>Toko</th>
              <td><a href='" . base_url() . "members/produk_reseller/$row[id_reseller]' style='color:#f08519;'><b><?= $row['nama_reseller'] ?></b></a></td>
            </tr>

            <tr>
              <th scope='row' style='width:90px'>Pilih Alamat</th>
              <td>
                <select class='form-control' id='kota<?= $noo ?>' name='kota<?= $noo ?>' onchange='reset_kurir(<?= $noo ?>);'>
                  <?php
                  $reseller_alamat = $this->db->query("select a.id_alamat_reseller,b.nama_kota,a.kota_id as kot from alamat_reseller a join rb_kota b on a.kota_id=b.kota_id where a.id_reseller='$row[id_reseller]' order by alamat_default desc");
                  if ($reseller_alamat->num_rows() == 0 || $reseller_alamat->num_rows() > 1) {
                    echo "<option value=''>Pilih Alamat</option>";
                    foreach ($reseller_alamat->result_array() as $pilih_al) {
                      echo "<option value='$pilih_al[kot],$pilih_al[id_alamat_reseller]'>$pilih_al[nama_kota]</option>";
                    }
                  } else {
                    $pilih_al = $reseller_alamat->row_array();
                      echo "<option value='$pilih_al[kot],$pilih_al[id_alamat_reseller]'>$pilih_al[nama_kota]</option>";
                  }
                  echo "</select>";
                  ?>
              </td>
            </tr>

            <tr>
              <th scope='row' style='width:90px'>Total</th>
              <td>Rp <?= rupiah($row['total']) ?></td>
            </tr>

            <?php if($id_tipe_buyer =='3' && $diskon_belanja > 0){ ?>
            <tr>
              <th scope='row' style='width:90px'>Diskon</th>
              <td style="color:red">Rp <?= rupiah($diskon_belanja) ?></td>
            </tr>
            <?php } ?>

            <tr>
              <th scope='row' style='width:90px'>Berat</th>
              <td><b><?= $row['brt'] ?></b> Gram</td>
            </tr>

            <tr>
              <th scope='row'>Pengiriman</th>
              <td>

                <?php
                $kurir = array('1;ncs kurir');
                foreach ($kurir as $rkurir) {
                  $val = explode(';', $rkurir);
                ?>

                  <label class='radio-inline'>
                    <input type='radio' name='kurir<?= $noo ?>' id='kurir<?= $noo ?>' class='kurir<?= $noo ?>' value='<?= $val[1] ?>' onclick="pengiriman(<?= $noo ?>);" <?php if ($reseller_alamat->num_rows() == 0 || $reseller_alamat->num_rows() > 1) {
                                                                                                                                                                            echo "selected disabled";
                                                                                                                                                                          } ?> />
                    <?= strtoupper($val[1]) ?>
                  </label>

                <?php } ?>

              </td>
            </tr>

            <tr>
              <th scope='row'></th>
              <td>
                <div id='kuririnfo<?= $noo ?>' style='display: none;'>
                  <div class='col-12'>
                    <p class='form-control-static' id='kurirserviceinfo<?= $noo ?>'></p>
                  </div>
                </div>
              </td>
            </tr>

            <tr>
              <td colspan='2'>
                <input type='checkbox' id='myInsurance<?= $noo ?>' name='insurance<?= $noo ?>' value='0' class='assur' onclick='myFunc(<?= $noo ?>)'>
                <label for='myCheck' aria-hidden='true'>Asuransi Pengiriman</label> <i class='fa fa-info-circle' data-toggle='modal' data-target='#insurance'>
              </td>
              <td style='text-align:center'><b id='isr<?= $noo ?>' style='display:none'><b></td>
            </tr>

          </tbody>
        </table>
      </div>
    </div>

    <div class="col-sm-12">
      <hr style='border: 1px #c3c4c6 solid;width:94%;text-align:left;margin-left:0' />
    </div>

  <?php $noo++;
  } ?>

  <div class="col-sm-12">
    <table class='table table-striped table-condensed'>
      <tbody>

        <?php

        $kon = $this->session->userdata('flag_PPN');
        if ($kon == '1') {
          if ($rows['include_PPN'] == 'on') {
            echo "<input type='hidden' name='ppn' id='ppn'  value='" . $kon . "'/>
        <tr'>
        
          <td colspan='3'><b>Pajak PPN 10%</b></td>
          <td colspan='9' style='text-align:center'><b id='totalpajak'></b></td>
          <td></td>
        </tr><input type='hidden' name='pajak' id='pajak'  value='0'/>";
          }
        } else {
          echo "<input type='hidden' name='ppn' id='ppn'  value='0'/>";
        }

        ?>

        <tr>
          <td colspan='3'><b>Biaya Pengiriman NCS</b></td>
          <td colspan='9' style='text-align:center'><b id='totalongkir'></b></td>
        </tr>
        <tr>
          <td colspan='3'><b>Potongan Pengiriman</b></td>
          <td colspan='9' style='text-align:center; color:red;'><b id='totalkomisi'></b></td>
        </tr>
        <tr>
          <td colspan='3'><b>Total Belanja</b></td>
          <td colspan='9' style='text-align:center'><b>Rp <?php echo rupiah($total_belanja); ?></b></td>
        </tr>
        <tr>
          <td colspan='3'><b>Total Diskon</b></td>
          <td colspan='9' style='text-align:center; color:red;'><b> (Rp <?php echo rupiah($total_diskon_all); ?>)</b></td>
        </tr>
        <tr>
          <td colspan='3'><b>CashBack</b></td>
          <td colspan='9' style='text-align:center'><b> (Rp <?php echo rupiah($komisi_user); ?>)</b></td>
        </tr>
        <tr>
          <td colspan='3'><b>Biaya Administrasi</b></td>
          <td colspan='9' style='text-align:center'><b id='biaya_admin'></b></td>

        </tr>
        <tr>
          <td colspan='3'>
            <label for="myCheck" aria-hidden="true">Total Asuransi Pengiriman</label> <i class="fa fa-info-circle" data-toggle='modal' data-target='#insurance'></i>
          </td>
          <td colspan='9' style='text-align:center'><b id="ins" style="display:none"><b></td>
        </tr>
      </tbody>
    </table>

    <div style="margin-bottom:10px">
      <H3>PEMBAYARAN</H3>
      <div class="col-md-11">

        <div class="col-md-11">

          <?php if ($this->session->userdata('flag_midtrans_payment') === '1') { ?>
            <label class="radio-inline">
              <input type="radio" name="pembayaran" class="checkout-payment" value="credit_card" disabled /> Kartu Kredit
            </label>
            <label class="radio-inline">
              <input type="radio" name="pembayaran" class="checkout-payment" value="va" /> Virtual Account
            </label>
            <label class="radio-inline">
              <input type="radio" name="pembayaran" class="checkout-payment" value="direct_debit" disabled /> Internet Banking
            </label>
          <?php }

          if ($this->session->userdata('flag_qris') === '1') { ?>
            <label class="radio-inline">
              <input type="radio" name="pembayaran" class="checkout-payment" value="qris" /> QRIS
            </label>
          <?php }

          if ($this->session->userdata('flag_manual_transfer') === '1') { ?>
            <label class="radio-inline">
              <input type="radio" name="pembayaran" class="checkout-payment" value="manual-transfer" />Manual Transfer
            </label>
          <?php }

          if ($this->session->userdata('flag_cod') === '1') { ?>
            <label class="radio-inline">
              <input type="radio" name="pembayaran" class="checkout-payment" value="cod" <?php if($cek_cod>0){ echo "disabled"; } ?>/> COD
            </label>
          <?php } ?>

        </div>
      </div>
    </div>

    <div class="col-md-12" id='va' style="display: none;"><br />
      <label class="radio-inline">
        <input type="radio" name="va_bank" id="va_bank" class="va_bank" value="bca_va" disabled />BCA
      </label>
      <label class="radio-inline">
        <input type="radio" name="va_bank" id="va_bank" class="va_bank" value="bni_va" />BNI
      </label>
      <label class="radio-inline">
        <input type="radio" name="va_bank" id="va_bank" class="va_bank" value="bri_va" disabled />BRI
      </label>
      <label class="radio-inline">
        <input type="radio" name="va_bank" id="va_bank" class="va_bank" value="echannel" />Mandiri
      </label>
      <label class="radio-inline">
        <input type="radio" name="va_bank" id="va_bank" class="va_bank" value="permata_va" />Permata
      </label>
      <label class="radio-inline">
        <input type="radio" name="va_bank" id="va_bank" class="va_bank" value="other_va" />Lainnya
      </label>
      <br />
    </div>

    <div class="col-md-12" id='direct_debit' style="display: none;"><br />
      <label class="radio-inline">
        <input type="radio" name="debit" id="debit" class="debit" value="bca_klikpay" />BCA KLIKPAY
      </label>
      <label class="radio-inline">
        <input type="radio" name="debit" id="debit" class="debit" value="cimb_clicks" />OCTO KLIK
      </label>
      <!-- <label class="radio-inline">
          <input type="radio" name="debit"  id="debit"  class="debit" value="bri_va"/>BRI E PAY
        </label> -->
      <label class="radio-inline">
        <input type="radio" name="debit" id="debit" class="debit" value="danamon_online" />DANAMON ONLINE
      </label>

      <br /><br />

    </div>
  </div>

  <div id="paymentinfo" style="display: none;">
    <div class="form-group">
      <div class="col-12">
        <center>
          <img style="height:120px" src="<?php echo base_url() ?>asset/ket_pembayaran/illus_payment_qris.png" /> <br />
          <img style="height:40px" src="<?php echo base_url() ?>asset/ket_pembayaran/logo_qris_partner.png" /> <br /> <br />
          <span><b>QRIS Payment</b></span> <br />
          <span><b>Satu QR Code untuk Seluruh Pembayaran</b></span>
        </center>
      </div>
    </div>
  </div>

  <div id="gopay" style="display: none;">
    <div class="form-group">
      <div class="col-12">
        <center>
          <img style="height:120px" src="<?php echo base_url() ?>asset/ket_pembayaran/logo_gopay.png" /> <br />
          <span><b>Gopay Payment</b></span> <br />
        </center>
      </div>
    </div>
  </div>

  <div id="payment_va" style="display: none;">
    <div class="form-group">
      <div class="col-12">
        <center>
          <img style="height:130px" src="<?php echo base_url() ?>asset/ket_pembayaran/midtrans_va.png" /> <br /> <br />
          <span><b>Virtual Account Ke semua Bank</b></span> <br />
        </center>
      </div>
    </div>
  </div>

  <div id="payment_kartu_kredit" style="display: none;"><br />
    <div class="form-group">
      <div class="col-12">
        <center>
          <img style="height:80px" src="<?php echo base_url() ?>asset/ket_pembayaran/credit_card.png" /> <br /> <br />
          <span><b>kartu Kredit</b></span> <br />
        </center>
      </div>
    </div>
  </div>

  <div id="payment_direct_debit" style="display: none;"><br />
    <div class="form-group">
      <div class="col-12">
        <center><br />
          <img style="height:100px" src="<?php echo base_url() ?>asset/ket_pembayaran/direct_debit.png" /> <br /> <br />
          <span><b>Internet Banking</b></span> <br />
        </center>
      </div>
    </div>
  </div>

  <div id="paymentmidtrans" style="display: none;">
    <div class="form-group">
      <div class="col-12">
        <center>
          <img style="height:300px" src="<?php echo base_url() ?>asset/ket_pembayaran/midtrans_partner.png" /> <br /> <br />
          <span><b>Midtrans Payment</b></span> <br />
        </center>
      </div>
    </div>
  </div>

  <div id="paymentmanual" style="display: none;">
    <div class="form-group">
      <div class="col-12">

        <?php
        unset($rekening);
        $rekening = $this->db->query("SELECT * FROM rb_rekening")->row_array();
        if ($rekening['no_rekening'] != '') {
        ?>
          <center>
            <img style="height:100px" src="<?php echo base_url() ?>asset/ket_pembayaran/manual.png" /> <br />
            <br />
            <span>Dicek maksimal 1 x 24 jam setelah bukti pembayaran diupload</span><br />
            <span>Silahkan transfer pembayaran sesuai Total Bayar ke Rekening berikut :</span><br /><br />
            <H3><b><?php echo $rekening['nama_bank'] ?></b></H3>
            <H3><b><?php echo $rekening['no_rekening'] ?></b></H3>
            <H3><b><?php echo $rekening['pemilik_rekening'] ?></b></H3>
          </center>
        <?php
        } else {
          echo " <span><b>No Rekening Tidak tersedia</b></span><br/>";
        }
        ?>
      </div>
    </div>
  </div>

  <div class='col-md-3 pull-right'>
    <input type="hidden" name="id_alamat" id="id_alamat" class="id_alamat" value="<?php echo $alm['id_alamat'] ?>" />
    <input type="hidden" name="total" id="total" class="total" value="<?php echo $total['total']-$total_diskon_all; ?>" />
    <input type="hidden" name="komisi" id="komisi" class="komisi" value="0" />

    <?php $y = 1;
    foreach ($record as $tot) {
      $asuransi = str_replace(".", ",", ($tot['total'] * ((0.20) / 100))); ?>
      <input type='hidden' name='free_ongkir<?=$y?>' id='free_ongkir<?=$y?>' value='<?= $tot['gratis_ongkir'] ?>'> 
      <input type="hidden" name="diskon_ongkir<?php echo $y; ?>" id="diskon_ongkir<?php echo $y; ?>" class="diskon_ongkir<?php echo $y; ?>" value="" />
      <input type="hidden" name="komisi_user<?php echo $y; ?>" id="komisi_user<?php echo $y; ?>" class="komisi_user<?php echo $y; ?>" value="<?php echo $tot['komisi_user']; ?>" />
      <input type="hidden" name="komisi_top<?php echo $y; ?>" id="komisi_top<?php echo $y; ?>" class="komisi_top<?php echo $y; ?>" value="<?php echo $tot['komisi_top']; ?>" />
      
      <?php $komisi_referal = $this->db->query("SELECT ifnull(round(round(((sum(a.harga_jual*a.jumlah))*(c.komisi/100))*(f.komisi_topsonia/100))*(1/100)),0) AS komisi_referal FROM `rb_penjualan_temp` a JOIN rb_produk b ON a.id_produk=b.id_produk JOIN rb_reseller c ON b.id_reseller=c.id_reseller JOIN rb_kota d ON c.kota_id=d.kota_id JOIN rb_konsumen e ON (a.SESSION=e.id_konsumen and c.referral=e.kode_referall) JOIN tipe_buyer f ON e.id_tipe_buyer=f.id_tipe_buyer WHERE a.id_penjualan_detail='".$tot['id_penjualan_detail']."' and a.waktu_order<DATE_ADD(c.tanggal_daftar, INTERVAL 6 MONTH) GROUP BY c.id_reseller ORDER BY c.id_reseller ASC ")->row_array()['komisi_referal']; ?>
      <input type="hidden" name="komisi_referal<?php echo $y; ?>" id="komisi_referal<?php echo $y; ?>" class="komisi_referal<?php echo $y; ?>" value="<?php if($komisi_referal==''){echo'0';}else{echo $komisi_referal;} ?>" />
      
      <input type="hidden" name="sub_tot<?php echo $y; ?>" id="sub_tot<?php echo $y; ?>" class="sub_tot<?php echo $y; ?>" value="<?php echo $tot['total']; ?>" />
      <input type="hidden" name="sub_ttl<?php echo $y; ?>" id="sub_ttl<?php echo $y; ?>" class="sub_ttl<?php echo $y; ?>" value="<?php echo $asuransi; ?>" />
      <input type="hidden" name="asr<?php echo $y; ?>" id="asr<?php echo $y; ?>" class="asr<?php echo $y; ?>" value="" />
    <?php $y++;
    } ?>

    <input type="hidden" name="komisi_tp" id="komisi_tp" class="komisi_tp" value="0" />
    <input type="hidden" name="diskon_ongkir" id="diskon_ongkir" class="diskon_ongkir" value="<?php echo $diskon['komisi']; ?>" />
    <input type="hidden" name="berat" class="berat" value="<?php echo $total['total_berat']; ?>" />
    <input type="hidden" name="total_bayar" id="total_bayar" class="total_bayar" value="0" />
    <input type="hidden" name="totalKomisiTopsonia" id="k_total_top" class="k_total_top" value="0" />
    <input type="hidden" name="asuransi" id="asuransi" class="asuransi" value="0" />
    <input type="hidden" name="mdr" id="mdr" class="mdr" value="0" />
    <input type="hidden" name="persen_mdr" id="persen_mdr" value="0" />
    <input type="hidden" name="nominal_mdr" id="nominal_mdr" value="0" />
    <input type="hidden" name="dis_ongkir" id="dis_ongkir" value="0" />

    <?php for ($x = 1; $x < $jumlah_seller; $x++) { ?>
      <input type="hidden" name="service_code<?php echo $x; ?>" id="service_code<?php echo $x; ?>" class="service_code<?php echo $x; ?>" value="0" />
      <input type="hidden" name="ongkir<?php echo $x; ?>" id="ongkir<?php echo $x; ?>" class="ongkir<?php echo $x; ?>" value="" />
    <?php } ?>

    <input type="hidden" name="ongkir" id="ongkir" class="ongkir" value="0" />


    <div style="text-align: center;">
      <b>TOTAL BAYAR</b><br>
      <h2 id='totalbayar'></h2>
      <?php
      if ($alm['id_alamat'] != '') {
      ?>
        <p id="qris">
          <button type='button' id='oksimpan2' class='btn btn-danger btn-flat btn-sm' data-toggle='modal' data-target='#myModal'>Buat Pesanan</button>
          <button type='submit' name='submit' id='oksimpan' class='btn btn-warning btn-flat btn-sm' disabled>Buat Pesanan</button>
        </p>
        <p id='midtrans' style="display: none;">
          <input type="hidden" id="mid_tot">
          <button type='button' class='btn btn-warning btn-flat btn-sm' id='pay-button' disabled>Buat Pesanan</button>
        </p>
      <?php } else { ?>
        <button type='button' name='submit' id='oksimpan' onclick="cek_alamat()" class='btn btn-warning btn-flat btn-sm'>Buat Pesanan</button>

        <button type='button' id='oksimpan2' class='btn btn-danger btn-flat btn-sm' data-toggle='modal' data-target='#myModal'>Buat Pesanan</button>

      <?php } ?>
    </div>

  </div>

</form>


</div>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- konten modal-->
    <div class="modal-content">
      <!-- heading modal -->
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">QRIS Payment</h4>
      </div>
      <!-- body modal -->
      <div class="modal-body">
        <p>
          <img src="<?php echo base_url(); ?>/asset/ket_pembayaran/qrismax.png" width=100% height="auto">
        </p>
      </div>
    </div>
  </div>
</div>

<div id="insurance" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- konten modal-->
    <div class="modal-content">
      <!-- heading modal -->
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Asuransi Pengiriman</h4>
      </div>
      <!-- body modal -->
      <div class="modal-body">
        <p>
          <center><img src="<?php echo base_url(); ?>/asset/shipment/insurance.png" height="150px"></center>
        </p>
        <p>Asuransi pengiriman merupakan biaya ganti rugi senilai harga produk, dengan biaya sebesar 0,20% dari harga produk dan berlaku pembulatan ke atas</p>
      </div>
    </div>
  </div>
</div>


<form id='payment-form' method='post' action='<?php echo base_url(); ?>members/selesai_belanja/<?php echo $this->uri->segment(4); ?>' method='POST'>
  <input type="hidden" name="result_type" id="result-type" value="">
  <input type="hidden" name="result_data" id="result-data" value="">
  <input type="hidden" name="pembayaran" id="pembayaran" value="midtrans" />
  <input type="hidden" name="service" id='ser'>
  <input type="hidden" name="ongkir" id='ong'>
  <?php $y = 1;
  foreach ($record as $tot) {
    $asuransi = str_replace(".", ",", ($tot['total'] * ((0.20) / 100))); ?>
    <input type="hidden" name="diskon_belanja<?= $y ?>" id="diskon_bel<?= $y ?>" value="0">
    <input type="hidden" name="diskon_ongkir<?php echo $y; ?>" id="diskon_ong<?php echo $y; ?>" value="0" />
    <input type="hidden" name="komisi_user<?php echo $y; ?>" value="<?php echo $tot['komisi_user']; ?>" />
    <input type="hidden" name="komisi_top<?php echo $y; ?>" value="<?php echo $tot['komisi_top']; ?>" />
    <input type="hidden" name="sub_tot<?php echo $y; ?>" value="<?php echo $tot['total']; ?>" />
    <input type="hidden" name="sub_ttl<?php echo $y; ?>" value="<?php echo $asuransi; ?>" />
    <input type="hidden" name="kurir<?php echo $y; ?>" id='kur<?php echo $y; ?>'>
    <input type="hidden" name="asr<?php echo $y; ?>" id="asrm<?php echo $y; ?>" class="asr<?php echo $y; ?>" value="" />
    <input type="hidden" name="service_code<?php echo $y; ?>" id="ser<?php echo $y; ?>" />
    <input type="hidden" name="ongkir<?php echo $y; ?>" id="ong<?php echo $y; ?>" />
    <input type="hidden" name="kota<?php echo $y; ?>" id="ko<?php echo $y; ?>" value="" />
    <input type="hidden" name="catatan_pelapak<?php echo $y; ?>" id="ctt_plk<?php echo $y; ?>">
  
    <?php $komisi_referal = $this->db->query("SELECT ifnull(round(round(((sum(a.harga_jual*a.jumlah))*(c.komisi/100))*(f.komisi_topsonia/100))*(1/100)),0) AS komisi_referal FROM `rb_penjualan_temp` a JOIN rb_produk b ON a.id_produk=b.id_produk JOIN rb_reseller c ON b.id_reseller=c.id_reseller JOIN rb_kota d ON c.kota_id=d.kota_id JOIN rb_konsumen e ON (a.SESSION=e.id_konsumen and c.referral=e.kode_referall) JOIN tipe_buyer f ON e.id_tipe_buyer=f.id_tipe_buyer WHERE a.id_penjualan_detail='".$tot['id_penjualan_detail']."' and a.waktu_order<DATE_ADD(c.tanggal_daftar, INTERVAL 6 MONTH) GROUP BY c.id_reseller ORDER BY c.id_reseller ASC ")->row_array()['komisi_referal']; ?>
    <input type="hidden" name="komisi_referal<?php echo $y; ?>" value="<?php if($komisi_referal==''){echo'0';}else{echo $komisi_referal;} ?>" />
  <?php $y++;
  } ?>

  <input type="hidden" name="diskon" id='dis'>
  <input type="hidden" name="mdr" id='adm'>
  <input type="hidden" name="total_bayar" id='total_bay'>
  <input type="hidden" name="id_alamat" id='id_alm'>
  <input type="hidden" name="jenis_mdr" id="jenis_mdr" value='0' />
  <input type="hidden" name="asuransi" id="asr" value="0" />
  <input type='hidden' name='pajak' id='pjk' value='0' />

</form>

<script>
  $(document).ready(function() {

    function cek_mdr() {
      $.ajax({
        type: 'POST',
        url: '<?= site_url() ?>/members/get_mdr',
        data: {
          jenis_mdr: document.getElementById('jenis_mdr').value,
        },
        cache: false,
        dataType: 'json',
        success: function(ajax) {
          document.getElementById('persen_mdr').value = ajax.persen_mdr;
          document.getElementById('nominal_mdr').value = ajax.nominal_mdr;
          hitung();
        }
      });

    }

    $("#oksimpan2").hide();
    $(".checkout-payment").each(function(o_index, o_val) {
      $(this).on("change", function() {
        var did = $(this).val();
        if (did == 'qris') {
          $("#qris").show();
          if ($("#total_bayar").val() > 2000000) {
            $("#oksimpan2").show();
            $("#oksimpan").hide();
          }
          $("#gopay").hide();
          $("#midtrans").hide();
          $("#paymentinfo").show();
          $("#paymentmanual").hide();
          $("#payment_va").hide();
          $("#payment_kartu_kredit").hide();
          $("#va").hide();
          $("#direct_debit").hide();
          $("#payment_direct_debit").hide();
        } else if (did == 'manual-transfer') {
          $("#oksimpan2").hide();
          $("#oksimpan").show();
          $("#qris").show();
          $("#midtrans").hide();
          $("#gopay").hide();
          $("#paymentmanual").show();
          $("#paymentinfo").hide();
          $("#payment_va").hide();
          $("#va").hide();
          $("#direct_debit").hide();
          $("#payment_direct_debit").hide();
        } else if (did == 'va') {
          $("#oksimpan2").hide();
          $("#oksimpan").show();
          $("#gopay").hide();
          $("#direct_debit").hide();
          $("#payment_direct_debit").hide();
          $("#va").show();
          $("#payment_va").show();
          $("#midtrans").show();
          $("#payment_kartu_kredit").hide();
          $("#qris").hide();
          $("#paymentinfo").hide();
          $("#paymentmanual").hide();
          $("#jenis_mdr").val(null);
          $("input[id=va_bank]").prop("checked", false);
        } else if (did == 'credit_card') {
          $("#oksimpan2").hide();
          $("#oksimpan").show();
          $("#gopay").hide();
          $("#direct_debit").hide();
          $("#payment_direct_debit").hide();
          $("#va").hide();
          $("#payment_va").hide();
          $("#payment_kartu_kredit").show();
          $("#midtrans").show();
          $("#qris").hide();
          $("#paymentinfo").hide();
          $("#paymentmanual").hide();
        } else if (did == 'direct_debit') {
          $("#oksimpan2").hide();
          $("#oksimpan").show();
          $("#gopay").hide();
          $("#direct_debit").show();
          $("#payment_direct_debit").show();
          $("#va").hide();
          $("#payment_va").hide();
          $("#midtrans").show();
          $("#payment_kartu_kredit").hide();
          $("#qris").hide();
          $("#paymentinfo").hide();
          $("#paymentmanual").hide();
          $("#mdr").val('');
          $("#jenis_mdr").val(null);
          $("#biaya_admin").html((toDuit(0)));
          $("input[id=debit]").prop("checked", false);
        } else if (did == 'gopay') {
          if ($("#total_bayar").val() > 2000000) {
            $("#oksimpan2").show();
            $("#oksimpan").hide();
          }
          $("#gopay").show();
          $("#qris").hide();
          $("#midtrans").show();
          $("#paymentinfo").hide();
          $("#paymentmanual").hide();
          $("#payment_va").hide();
          $("#payment_kartu_kredit").hide();
          $("#va").hide();
          $("#direct_debit").hide();
          $("#payment_direct_debit").hide();
        } else if (did == 'qris_m') {
          if ($("#total_bayar").val() > 2000000) {
            $("#oksimpan2").show();
            $("#oksimpan").hide();
          }
          $("#gopay").hide();
          $("#qris").hide();
          $("#midtrans").show();
          $("#paymentinfo").show();
          $("#paymentmanual").hide();
          $("#payment_va").hide();
          $("#payment_kartu_kredit").hide();
          $("#va").hide();
          $("#direct_debit").hide();
          $("#payment_direct_debit").hide();

        }
        $("#jenis_mdr").val(did);
        cek_mdr();
        hitung();

      });
    });

    $(".va_bank").each(function(o_index, o_val) {
      $(this).on("change", function() {
        var did = $(this).val();
        $("#jenis_mdr").val(did);
        cek_mdr();
        hitung();
      });
    });

    $(".debit").each(function(o_index, o_val) {
      $(this).on("change", function() {
        var did = $(this).val();
        $("#jenis_mdr").val(did);
        cek_mdr();
        hitung();
      });
    });

    // $("#diskon").html(toDuit(0));

    hitung();

  });

  function pengiriman(nom) {

    document.getElementById("myInsurance" + nom).value = document.getElementById("sub_ttl" + nom).value;

    $("#ss-tampil").css("display", "none");
    $(".kurir" + nom).each(function(o_index, o_val) {
      $(this).on("change", function() {

        document.getElementById("oksimpan").disabled = true;
        var did = $(this).val();
        var berat = document.getElementById("berat_total" + nom).value;
        let seller_total = document.getElementById("seller_total" + nom).value;
        var ambil_kota = document.getElementById("kota" + nom).value.split(",")[0];
        var kotaShp = ambil_kota;
        var kotaCne = "<?php echo $alm['kota_id']; ?>";
        let free_ongkir = document.getElementById("free_ongkir" + nom).value;

        if (did == '2') {
          $("#ss-tampil").show();
          $("#kuririnfo").hide();

        } else {
          $.ajax({
              method: "get",
              dataType: "html",
              url: "<?php echo base_url(); ?>produk/kurirdata",
               data: "berat=" + berat + "&kotaCne=" + kotaCne + "&kotaShp=" + kotaShp + "&nomor=" + nom + "&free_ongkir="+free_ongkir+"&jumlah_seller=<?php echo $jumlah_seller; ?>&seller_total="+seller_total,
              beforeSend: function() {}
            })

            .done(function(x) {

              $("#kurirserviceinfo" + nom).html(x);
              $("#kuririnfo" + nom).show();
              $("#ss-tampil").css("display", "none");
            })
            .fail(function() {

            });
        }
      });
    });
  }

  function hitung() {

    let persen_mdr = $("#persen_mdr").val();
    let nominal_mdr = $("#nominal_mdr").val();

    var total = $('#total').val();
    if ($("#ongkir").val() === '') {
      var ongkir = 0;
      let potongan_ongkir = 0;
    } else {
      var ongkir = $("#ongkir").val();
      
      if('<?=$id_tipe_buyer?>'=='3'){
        var potongan_ongkir = parseFloat($("#dis_ongkir").val());
      }else{
        var potongan_ongkir = (parseFloat(ongkir) * (parseFloat($("#diskon_ongkir").val()) / 100));
      }
    }

    var diskon = $('#komisi_tp').val();
    var komisi = $('#komisi').val();
    var komisi_total;
    var total_diskon = "<?php echo $komisi_user; ?>";
    var pajak = $('#pajak').val();
    var ppn = 0;
    var bayar1 = parseFloat(total) - parseFloat(potongan_ongkir) - parseFloat(diskon);
    var hitung = parseFloat(total) - parseFloat(diskon);
    var hasil_pajak = (parseFloat(hitung) * (parseFloat(ppn) / 100));
    var asuransi = 0;


    if ($('input:checkbox:checked.assur').map(function() {
        return this.value;
      }).get() != '') {
      asur = $('input:checkbox:checked.assur').map(function() {
        return this.value;
      }).get().join("-");
      let strArr = asur.split("-");
      asuransi = strArr.reduce(function(total, num) {
        return parseFloat(total) + parseFloat(num);
      });
    } else {
      asuransi = 0;
    }

    // var total_semua = (parseFloat(total) + parseFloat(ongkir)) - parseFloat(potongan_ongkir) - parseFloat(hasil_pajak) + parseInt(asuransi) - parseInt(total_diskon);
    var total_semua = (parseFloat(total) + parseFloat(ongkir)) - parseFloat(potongan_ongkir) - parseFloat(hasil_pajak) + parseInt(asuransi)
    var mdr = Math.ceil((total_semua / ((100 - persen_mdr) / 100)) - total_semua) + parseInt(nominal_mdr);
    var bayar = total_semua + mdr;
    $("#mdr").val(mdr);
    $("#biaya_admin").html((toDuit(mdr)));
    $("#total_komisi").val(potongan_ongkir);
    $("#total_bayar").val(bayar);
    $("#pajak").val(hasil_pajak);
    $("#asuransi").val(asuransi);
    $("#totalkomisi").html("(" + (toDuit(potongan_ongkir)) + ")");
    $("#totalbayar").html((toDuit(bayar)));
    $("#totalongkir").html(toDuit(ongkir));
    $("#totalpajak").html(toDuit(hasil_pajak));
    $("#ins").html(toDuit(asuransi));

    let alamat = "<?php echo $alm['kota_id']; ?>";
    if ((alamat != "" || alamat === null) && $("input[name='pembayaran']:checked").val() != null <?php for ($x = 1; $x < $jumlah_seller; $x++) { ?> && $("#ongkir<?php echo $x; ?>").val() != ''
      <?php } ?>) {

      if ($("#jenis_mdr").val() != '') {
        document.getElementById("oksimpan").disabled = false;
        document.getElementById("pay-button").disabled = false;
      } else {
        document.getElementById("oksimpan").disabled = true;
        document.getElementById("pay-button").disabled = true;
      }
    } else {
      document.getElementById("oksimpan").disabled = true;
      document.getElementById("pay-button").disabled = true;
    }
  }

  function reset_kurir(id) {
    if (document.getElementById("kota" + id).value === '') {
      document.getElementById("ko" + id).value = "";
      document.getElementById("kurir" + id).disabled = true;
    } else {
      document.getElementById("kurir" + id).disabled = false;
      document.getElementById("ko" + id).value = "ddd," + document.getElementById("kota" + id).value.split(",")[1];
    }
    $('.kurir' + id).attr('checked', false);
    $("#kurirserviceinfo" + id).html('');
    $("#ongkir").val('');
    hitung();
    document.getElementById("oksimpan").disabled = true;
    document.getElementById("pay-button").disabled = true;

  }

  function cek_alamat() {
    window.scroll({
      top: 0,
      behavior: 'smooth' // 
    });
    document.getElementById("alamat_alert").className = "cek_alamat";
    document.getElementById("alamat_alert").innerHTML = "Alamat Belum Di Pilih";
    alert('Alamat Belum Di Pilih');
  }

  function myInsurance() {
    var popwindow = document.getElementById("insurance");
    if (popwindow.style.display === "none") {
      popwindow.style.display = "block";
    } else {
      popwindow.style.display = "none";
    }
  };

  function myFunc(no) {
    var checkBox = document.getElementById("myInsurance" + no);
    var ins = document.getElementById("ins");

    if (checkBox.checked == true) {
      document.getElementById('asr' + no).value = document.getElementById('sub_ttl' + no).value;

      document.getElementById('asrm' + no).value = document.getElementById('sub_ttl' + no).value;

      ins.style.display = "block";
    } else {
      document.getElementById('asr' + no).value = '';
      document.getElementById('asrm' + no).value = '';

    }
    hitung();

  }
</script>

<style>
  .cek_alamat {
    color: red;
  }

  .border {
    border-width: 1px !important;
  }
</style>


<script type="text/javascript">
  $('#pay-button').click(function(event) {

    document.getElementById("ser").value = $('input[class="service"]:checked').val();

    <?php for ($x = 1; $x < $jumlah_seller; $x++) { ?>
      document.getElementById("diskon_ong<?php echo $x; ?>").value = $('input[class="diskon_ongkir<?php echo $x; ?>"]').val();
      document.getElementById("kur<?php echo $x; ?>").value = $('input[class="kurir<?php echo $x; ?>"]:checked').val();
      document.getElementById("ong<?php echo $x; ?>").value = $('input[class="ongkir<?php echo $x; ?>"]').val();
      document.getElementById("ser<?php echo $x; ?>").value = $('input[class="service<?php echo $x; ?>"]').val();
      document.getElementById("ctt_plk<?php echo $x; ?>").value = $("#catatan_pelapak<?php echo $x; ?>").val();
      document.getElementById("ko<?php echo $x; ?>").value = $("#kota<?php echo $x; ?>").val();
      document.getElementById("diskon_bel<?php echo $x; ?>").value = $("#diskon_belanja<?php echo $x; ?>").val();
    <?php } ?>
    document.getElementById("total_bay").value = $('input[class="total_bayar"]').val();
    document.getElementById("id_alm").value = $('input[class="id_alamat"]').val();
    document.getElementById("adm").value = $('input[class="mdr"]').val();
    document.getElementById("asr").value = $('input[class="asuransi"]').val();
    document.getElementById("pjk").value = $("#pajak").val();
    

    event.preventDefault();
    $(this).attr("disabled", "disabled");
    $.ajax({
      type: 'POST',
      url: '<?= site_url() ?>/members/token/',
      data: {
        id: '333',
        total: document.getElementById("total_bayar").value,
        <?php for ($x = 1; $x < $jumlah_seller; $x++) { ?>
          ongkir<?php echo $x; ?>: $('input[class="ongkir<?php echo $x; ?>"]').val(),
          diskon_ongkir<?php echo $x; ?>: $('input[class="diskon_ongkir<?php echo $x; ?>"]').val(),
          service_code<?php echo $x; ?>: $('input[class="service_code<?php echo $x; ?>"]').val(),
          kurir<?php echo $x; ?>: $('input[class="kurir<?php echo $x; ?>"]:checked').val(),
          komisi_top<?php echo $x; ?>: document.getElementById("komisi_top<?php echo $x; ?>").value,
          komisi_user<?php echo $x; ?>: document.getElementById("komisi_user<?php echo $x; ?>").value,
          asuransi<?php echo $x; ?>: document.getElementById("asr<?php echo $x; ?>").value,
          kota<?php echo $x; ?>: document.getElementById("ko<?php echo $x; ?>").value,
          catatan_pelapak<?php echo $x; ?>: document.getElementById("catatan_pelapak<?php echo $x; ?>").value,
          komisi_referal<?php echo $x; ?>: document.getElementById("komisi_referal<?php echo $x; ?>").value,
          diskon_belanja<?php echo $x; ?>: $("#diskon_belanja<?php echo $x; ?>").val(),

        <?php } ?>
        pembayaran: $('input[class="checkout-payment"]:checked').val(),
        id_alamat: $('input[class="id_alamat"]').val(),
        jenis_midtrans: $("#jenis_mdr").val(),
        total_mdr: $("#mdr").val(),
        asuransi: $('input[class="asuransi"]').val(),
        pajak: $("#pajak").val()
      },
      cache: false,

      success: function(data) {
        //location = data;
        console.log('token = ' + data);
        var resultType = document.getElementById('result-type');
        var resultData = document.getElementById('result-data');

        function changeResult(type, data) {
          $("#result-type").val(type);
          $("#result-data").val(JSON.stringify(data));

          //resultType.innerHTML = type;
          //resultData.innerHTML = JSON.stringify(data);
        }

        snap.pay(data, {
          onSuccess: function(result) {
            changeResult('success', result);
            console.log(result.status_message);
            console.log(result);
            // document.getElementById('payment-form').submit();
            $("#payment-form").submit();
          },
          onPending: function(result) {
            changeResult('pending', result);
            console.log(result.status_message);
            // document.getElementById('payment-form').submit();
            $("#payment-form").submit();
          },
          onError: function(result) {
            changeResult('error', result);
            console.log(result.status_message);
            window.location.replace("<?= site_url() ?>members/checkout/<?php echo $this->uri->segment(3) . "/" . $this->uri->segment(4); ?>");
          },
          onClose: function() {
            window.location.replace("<?= site_url() ?>members/checkout/<?php echo $this->uri->segment(3) . "/" . $this->uri->segment(4); ?>");
          }

        });
      }
    });
  });
</script>