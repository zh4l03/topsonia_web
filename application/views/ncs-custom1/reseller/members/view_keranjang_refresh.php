<div id="div_refresh">
      <div style="clear:both"><br></div>
      <table class="table table-striped table-condensed">
          <tbody>
				<?php 
				  $no = 1;
				  $no_s = 1;
				  foreach ($rows as $ro){
					echo "<tr><td colspan='6' style='background:#cacaca;'> $ro[nama_reseller]</td></tr>";
					$produk = $this->db->query("SELECT * FROM `rb_penjualan_temp` a join rb_produk b on a.id_produk=b.id_produk join rb_reseller c ON b.id_reseller=c.id_reseller JOIN rb_kota d ON c.kota_id=d.kota_id where a.session='".$this->session->id_konsumen."' and c.id_reseller='".$ro['id_reseller']."' ORDER BY a.id_penjualan_detail ASC")->result_array();
					foreach ($produk as $row){
				  $ex = explode(';', $row['gambar']);
				  if (trim($ex[0])==''){ $foto_produk = 'no-image.png'; }else{ $foto_produk = $ex[0]; }

				  $sub_total = ($row['harga_jual']*$row['jumlah'])-$row['diskon'];
				  echo "<tr><td>$no</td>
							<td width='70px'><img style='border:1px solid #cecece; width:60px' src='".base_url()."asset/foto_produk/$foto_produk'></td>
							<td><a style='color:#ab0534' href='".base_url()."produk/detail/$row[produk_seo]'><b>$row[nama_produk]</b></a>
								<br>Qty. <b>$row[jumlah]</b>, Harga. Rp ".rupiah($row['harga_jual']-$row['diskon'])." / $row[satuan], 
								<br>Berat. <b>".($row['berat']*$row['jumlah'])." Gram</b></td>

								<td> 
								<input type='button' value='+' onclick='tambah_$row[id_penjualan_detail]()' style='width: 50px;'>
								<input type='text' id='jml_qty_$row[id_penjualan_detail]' value='$row[jumlah]' style='width: 50px; text-align:center;' onfocus='let value = this.value; this.value = null; this.value=value'> 
								<input type='hidden' id='id_penjualan_detail_$row[id_penjualan_detail]' value='$row[id_penjualan_detail]' > 
								<input type='button' id='kur_$row[id_penjualan_detail]' name='kur_$row[id_penjualan_detail]' value='-' onclick='kurang_$row[id_penjualan_detail]()' style='width: 50px;'>
								</td>

							<td>Rp ".rupiah($sub_total)."</td>
							<td width='30px'><a class='btn btn-danger btn-xs' title='Delete' href='".base_url()."members/keranjang_delete/$row[id_penjualan_detail]/$row[id_reseller]'><span class='glyphicon glyphicon-remove'></span></a></td>
						</tr>";
					$no++;
					
				  }
				  $no=1;
				  $no_s++;
				}
				// $total = $this->db->query("SELECT sum((a.harga_jual*a.jumlah)-a.diskon) as total, sum(b.berat*a.jumlah) as total_berat FROM `rb_penjualan_detail` a JOIN rb_produk b ON a.id_produk=b.id_produk where a.id_penjualan='".$this->session->idp."'")->row_array();
				$total = $this->db->query("SELECT sum((a.harga_jual*a.jumlah)-a.diskon) as total, sum(b.berat*a.jumlah) as total_berat FROM `rb_penjualan_temp` a JOIN rb_produk b ON a.id_produk=b.id_produk where a.session='".$this->session->id_konsumen."'")->row_array();
          echo "<tr class='success'>
                  <td colspan='3'><b>Total Berat</b></td>
                  <td colspan='2'><b>$total[total_berat] Gram</b></td>
                  <td></td>
                </tr>
			</tbody>
		</table>

		<div class='col-md-4 pull-right'>
			<center>Total Bayar <br><h2 id='totalbayar'></h2> 
			<a class='btn btn-success btn-sm' href='".base_url()."members/checkout'>Checkout Belanja</a>
			<!--<button type='submit' name='submit' class='btn btn-success btn-flat btn-sm'>Checkout Belanja</button>-->
			</center>
		</div>";
      $ket = $this->db->query("SELECT * FROM rb_keterangan where id_reseller='".$rows['id_reseller']."'")->row_array();
      $diskon_total = '0';
?>

	<input type="hidden" name="total" id="total" value="<?php echo $total['total']; ?>"/>
	<input type="hidden" name="ongkir" id="ongkir" value="0"/>
	<input type="hidden" name="berat" value="<?php echo $total['total_berat']; ?>"/>
	<input type="hidden" name="diskonnilai" id="diskonnilai" value="<?php echo $diskon_total; ?>"/>
	<!--<div class="form-group">
		<label class="col-sm-12 control-label" for="">Pilih Kurir</label>
		<div class="col-md-10">
			<?php       
			//$kurir=array('ncs','jne','tiki','pos');
			$kurir=array('ncs kurir');
			foreach($kurir as $rkurir){
				?>          
					<label class="radio-inline">
					<input type="radio" name="kurir" class="kurir" value="<?php echo $rkurir; ?>"/> <?php echo strtoupper($rkurir); ?>
					</label>
				<?php
			}
			?>
			 <!-- <label class="radio-inline"><input type="radio" name="kurir" class="kurir" value="cod"/> COD (Cash on delivery)</label> -->
		<!--</div>
	</div>
	<div id="kuririnfo" style="display: none;">
		<div class="form-group">
			<div class="col-md-12">
				<br/>
				<br/>
				<div class='alert alert-info' style='padding:5px; border-radius:0px; margin-bottom:0px' >SERVICE</div>
				<br/>
				<p class="form-control-static" id="kurirserviceinfo"></p>
			</div>
		</div>
	</div>-->
	</div>
<?php echo form_close();?>