<style>
	.blue {
		background-color: #f08519 !important;
	}

	.active {
		background-color: #00FF7F !important;
	}
	
	.blue.darken-1 {
		background-color: #f08519 !important;
	}

	.blue-text.text-darken-1 {
		color: #f08519 !important;
	}

	.blue.darken-2 {
		background-color: #f08519 !important;
	}

	.blue-text.text-darken-2 {
		color: #f08519 !important;
	}

	.blue.darken-3 {
		background-color: #f08519 !important;
	}

	.blue-text.text-darken-3 {
		color: #f08519 !important;
	}

	.blue.darken-4 {
		background-color: #f08519 !important;
	}

	.blue-text.text-darken-4 {
		color: #f08519 !important;
	}
	
	.blue.accent-1 {
	  background-color: #f08519 !important;
	}

	.blue-text.text-accent-1 {
	  color: #f08519 !important;
	}

	.blue.accent-2 {
	  background-color: #f08519 !important;
	}

	.blue-text.text-accent-2 {
	  color: #f08519 !important;
	}

	.blue.accent-3 {
	  background-color: #f08519 !important;
	}

	.blue-text.text-accent-3 {
	  color: #f08519 !important;
	}

	.blue.accent-4 {
	  background-color: #f08519 !important;
	  color:white;
	}

	.blue-text.text-accent-4 {
	  color: #f08519 !important;
	}
	.thumbnail {
		padding:0px;
	}
	.panel-comment {
		position:relative;width:45%;
	}
	
	.panel-comment > .panel-heading{
		background-color:#E6E2E2 !important;
	}
	.panel-comment>.panel-heading:after,.panel>.panel-heading:before{
		position:absolute;
		top:11px;left:-16px;
		right:100%;
		width:0;
		height:0;
		display:block;
		content:" ";
		border-color:transparent;
		border-style:solid solid outset;
		pointer-events:none;
	}
	
	.panel-comment>.panel-heading:after{
		border-width:7px;
		border-right-color:#E6E2E2;
		margin-top:1px;
		margin-left:2px;
	}
	.panel-comment>.panel-heading:before{
		border-right-color:#ddd;
		border-width:8px;
	}

</style>
<body style="font-family: 'Ubuntu', sans-serif;">
<!-- <nav class="navbar navbar-default blue darken-4 navbar-fixed">
	<div class="container">

		<div class="navbar-header">
		  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		  </button>
		  <a class="navbar-brand" href="#" style="color:white"><b>Daftar Pesan</b></a>
		</div>
	</div>
</nav> -->
<div class="container">
	<div class="row">
		<div class="col-md-3">
			<ul class="list-group">
				<li class="list-group-item blue darken-4 " style="color:white;font-weight:bold"><i class="fa fa-users"></i> Chatt List</li>
				<?php foreach($user->result_array() as $row){ 
					$isi_p = $this->db->query("select count(id_penjual) as jml from chat where id_pembeli='".$this->session->id_konsumen."' and id_penjual='".$row['id_penjual']."' and flag=0 and ket=2 order by id_chat")->row_array();
					?>
					<li class="list-group-item list-group-item-success" onclick="aktifkan('<?php echo $row['id_penjual'];  ?>')" id="aktif-<?php echo $row['id_penjual']; ?>" id="<?php echo $row['id_penjual']; ?>" id="user" >
					<a href="javascript:void(0)" style="text-decoration:none" onClick="getChatAll('<?php echo $row['id_penjual']; ?>')"><i class="fa fa-angle-right"></i> <?php echo $row['nama_reseller']; ?></a>
					<span class='badge badgee'><?php echo $isi_p['jml']; ?></span>
					<span type="button" class='glyphicon glyphicon-remove' onclick="hapus_pesan('<?php echo $row['id_penjual']; ?>');"></span>
					</li>
				<?php } ?>
			</ul>
		</div>
		<div class="col-md-7">
			<div class="panel panel-info">
				<div class="panel-heading  blue darken-4" style="color:white;font-weight:bold" ><i class="fa fa-comments"></i> Chatt Box</div>
				<div class="panel-body" style="height:400px;overflow-y:auto" id="box">
					<div id="chat-box">
						<div class='panel-body'><h2 style='text-align:center;color:grey'>Click User on Chatt List to Start Chatt</h2></div>
						<!--br/>
						<div id="loading" style="display:none"><center><i class="fa fa-spinner fa-spin"></i> Loading...</center></div>
						</br !-->
					</div>
				</div>
				<div class="panel-footer" style="display:none">
					<div class="row">
						<div class="col-md-11">
							<textarea class="form-control " id="pesan" style="margin-right:10px;"></textarea>
							<button id="send" type="button" class="btn btn-success pull-right" style="margin-top:10px;"  onClick="sendMessage()" ><i class="fa fa-send"></i> Send Message</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="<?php echo base_url()?>assets/bootstrap/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/bootstrap/js/bootstrap.min.js"></script>
<script>
var refresh_chat="";
// $(document).ready(function(){
// 	//getChat(0);
// 	$("#user").click(function(){
// 		$("#id_max").val('0');
// 	});
	
// 	setInterval(function(){ 
// 		if($("#id_user").val() > 0){
// 			getLastId($("#id_user").val(),$("#id_max").val()); 
// 			getChat($("#id_user").val(),$("#id_max").val()); 
// 			autoScroll();
// 		}else{
			
// 		}
// 	},3000);
// });

function getChatAll(id_penjual){
	// alert(id_penjual);
	clearInterval(refresh_chat);
	
	refresh_chat = window.setInterval(function(){
	$.ajax({
		url		: "<?=site_url()?>/members/chat/",
		type	: 'POST',
		dataType: 'html',
		data 	: {id_penjual:id_penjual},
		beforeSend	: function(){
			$("#loading").show();
		},
		success	: function(result){
            // alert(id_pembeli);
			$("#loading").hide();
			$("#chat-box").html(result);
			$(".panel-footer").show();
			
			// autoScroll();
			document.getElementById('pesan').focus();
		}
	});

	}, 1000);
}

function sendMessage(){
	var pesan 	= $("#pesan").val();
	var id_penjual = $("#id_penjual").val();
	
	if(pesan == ''){
		document.getElementById('pesan').focus();
	}else{
		$.ajax({
			url		: "<?=site_url()?>/members/kirim_pesan/",
			type	: 'POST',
			dataType: 'json',
			data 	: {id_penjual:id_penjual,pesan:pesan},
			beforeSend	: function(){
			},
			success	: function(result){
				// getChatAll(id_penjual);
				$("#pesan").val('');
				autoScroll();
			}
		});
		// getChatAll(id_penjual);
		$("#pesan").val('');
		autoScroll();
	}
}

function autoScroll(){
	var elem = document.getElementById('box');
	elem.scrollTop = elem.scrollHeight;
}

function aktifkan(i){
	$("li").removeClass("active");
	$("#aktif-"+i).addClass("active");
}

function hapus_pesan(id_penjual){
	var r = confirm("apakah anda yakin ingin menghapus pesan ini ?");
  if (r == true) {
	
	$.ajax({
			url		: "<?php  echo base_url(); ?>members/hapus_pesan",
			type	: 'POST',
			dataType: 'json',
			data 	: {id_penjual:id_penjual},
			success: function (data) {
				alert("success!");
				location.reload();
			},
			error: function (data) {
                alert('gagal');
            }
		});
  	} else {
		// location.reload();
  	}
	
}
</script>