<?php
if ($this->session->level == 'reseller' || $this->session->level == 'admin') {
    $disabled = "disabled";
}
?>

<style>
.form-control2 {
    display: block;
    width: 100%;
    height: 34px;
    background: #fff;
    padding: 6px 13px;
    font-size: 13px;
    border: 1px solid #d3d3d3;
    font-family: 'Arial';
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

</style>


<center><br/><br/><h1 class='sidebar-title text-danger produk-title'> Login Users</h1></center>
    <!--<div class='alert alert-info'>Masukkan username dan password pada form berikut untuk login,...</div>-->
    <br>
    <?php 
    echo $this->session->flashdata('message'); 
    $this->session->unset_userdata('message');
    ?>
    <div class="logincontainer">
        <form method="post" action="<?php echo base_url(); ?>auth/login" role="form" id='formku'>
            <div class="form-group">
                <label for="inputEmail">Username</label>
                <input type="text" name="a" class="form-control2" placeholder="Masukkan Username" autofocus="" onkeyup="nospaces(this)" required <?php echo $disabled; ?>>
            </div>

            <div class="form-group">
                <label for="inputPassword">Password</label>
                <input class="form-control2" name='b' type="password" placeholder="Masukkan Password" onkeyup="nospaces(this)" required <?php echo $disabled; ?>>
            </div>

            <div align="center">
                <input name='login' type="submit" class="btn btn-primary" value="Login" <?php echo $disabled; ?>> 						<a href="#" class="btn btn-default" data-toggle='modal' data-target='#lupapass'>Lupa Password?</a>  <br>
               
                        <br> Anda Belum Punya akun? <a href="<?php echo base_url(); ?>auth/register" title="Mari gabung bersama Kami" class="link">Daftar Disini</a>
                   
            </div>
        </form>
    </div>
    <div style='clear:both'></div>
</div>
</center>
</div>
</div>
</div>

<div class="modal fade" id="lupapass" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h5 class="modal-title" id="myModalLabel">Lupa Password Login?</h5>
            </div>
            <div class="modal-body">
				<?php
				$attributes = array('class'=>'form-horizontal');
				echo form_open($this->uri->segment(1).'/lupa_password',$attributes);
				?>
				<div class="form-group">
					<center>Masukkan Email yang terkait dengan akun</center><br>
					<center><label for="inputEmail3" class="col-sm-2 control-label"></label></center>
					<div style='background:#fff;' class="input-group col-sm-6">
						<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
						<input style='text-transform:lowercase;' type="email" class="required form-control" name="email">
					</div>
				</div>
				<br>
				<div class="form-group">
					<center>
						<button type="submit" name='lupa' class="btn btn-primary btn-sm">Kirimkan Permintaan</button>
						&nbsp; &nbsp; &nbsp;
						<a class="btn btn-default btn-sm" data-dismiss="modal" aria-hidden="true" data-toggle='modal' href='#login' data-target='#login' title="Lupa Password Members">Kembali</a>
					</center>
				</div>
			</form>
			</div>
		</div>
	</div>
</div>