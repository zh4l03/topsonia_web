<?php
echo"<div class='panel-body'>
      <ul id='myTabs' class='nav nav-tabs' role='tablist'>
        <li role='presentation' class='active'><a href='#profile' id='profile-tab' role='tab' data-toggle='tab' aria-controls='profile' aria-expanded='true'>Profile Saya</a></li>
        <li role='presentation' class=''><a href='#alamat' role='tab' id='alamat-tab' data-toggle='tab' aria-controls='alamat' aria-expanded='false'>Alamat Pengiriman</a></li>
		";
		if($row['id_tipe_buyer']=='2'){
		echo "<li role='presentation' class=''><a href='#referal' role='tab' id='referal-tab' data-toggle='tab' aria-controls='referal' aria-expanded='false'>Komisi Referal</a></li>
		"; }
		$cek_buyer = $this->db->query("select skema_diskon from tipe_buyer where id_tipe_buyer='".$row['id_tipe_buyer']."'")->row_array();
		if($cek_buyer['skema_diskon']>0){
			echo "<li role='presentation' class=''><a href='#komisi' role='tab' id='komisi-tab' data-toggle='tab' aria-controls='komisi' aria-expanded='false'>Komisi Agent</a></li>
		"; }
		echo "<!--<li role='presentation' class=''><a href='#pembelian' role='tab' id='pembelian-tab' data-toggle='tab' aria-controls='pembelian' aria-expanded='false'>History Penjualan</a></li>-->
      </ul><br>

      <div id='myTabContent' class='tab-content'>
        <div role='tabpanel' class='tab-pane fade active in' id='profile' aria-labelledby='profile-tab'>
            <div class='col-md-12'>
              <p class='sidebar-title text-danger produk-title'> Data Profile Anda 				<span>        <a class='btn btn-success btn-xs pull-right' style='margin-left:5px;' href='".base_url()."members/edit_profile'><span class='glyphicon glyphicon-edit'></span> Edit Profile</a>		</span>		<span>		<a class='btn btn-primary btn-xs pull-right' style='margin-left:5px;' href='".base_url()."members/orders_report'><span class='glyphicon glyphicon-shopping-cart'></span> History Pembelian</a>		</span>				</p>";
					echo $this->session->flashdata('message'); 
					$this->session->unset_userdata('message');
					echo "<p>Berikut Informasi Data Profile anda.<br> 
					   Pastikan data-data dibawah ini sudah benar, agar tidak terjadi kesalahan saat transaksi.</p>";                
					  echo "<table class='table table-hover table-condensed'>
							<thead>
							  <tr><td width='170px'><b>Username</b></td> <td><b style='color:red'>$row[username]</b></td></tr>
							  <tr><td><b>Nama Lengkap</b></td>           <td>$row[nama_lengkap]</td></tr>
							  <tr><td><b>Email</b></td>                  <td>$row[email]</td></tr>
							  <tr><td><b>Jenis Kelamin</b></td>          <td>$row[jenis_kelamin]</td></tr>
							  <tr><td><b>Tanggal Lahir</b></td>          <td>".tgl_indo($row['tanggal_lahir'])."</td></tr>
							  <tr><td><b>Tempat Lahir</b></td>           <td>$row[tempat_lahir]</td></tr>
							  <tr><td><b>Alamat Agen</b></td>            <td>$row[alamat_lengkap]</td></tr>
							  
							  <tr><td><b>Propinsi</b></td>               <td>".$row['propinsi']."</td></tr>
							  <tr><td><b>Kota</b></td>                   <td>".$row['kota']."</td></tr>
							  <tr><td><b>Kecamatan</b></td>              <td>$row[kecamatan]</td></tr>
							  <tr><td><b>No Hp</b></td>                  <td>$row[no_hp]</td></tr>
							 "; 
						if($row['id_tipe_buyer']=='2'){
						
							echo  "<tr><td><b>Code Referal</b></td>";                  
							 if($row['kode_referall']==="" || $row['kode_referall']===null ) {
								echo "<td id='code'><button type='button' onclick='create_code();'>Get Code</button></td></tr>";
							 }else{
								echo "<td id='code'>$row[kode_referall]</td></tr>";
							 }
						}
						if($cek_buyer['skema_diskon']>0){
							echo  "<tr><td><b>No Rekening</b></td>
							<td id='code'>$row[no_rekening]</td></tr>";
						}


						echo "</thead>
						</table> 
            </div>
            <div style='clear:both'></div>
        </div>

       <div role='tabpanel' class='tab-pane fade' id='alamat' aria-labelledby='alamat-tab'>";
		echo "<div id='div_address'></div>";
			// echo $address;
            echo"
        </div>";

		$all_referal = $this->db->query("select round((sum(c.komisi_topsonia)*1)/100) as bonus_referal from rb_reseller a join rb_konsumen b on a.referral=b.kode_referall join rb_penjualan c on a.id_reseller=c.id_penjual where c.proses='1' and b.id_konsumen='".$this->session->id_konsumen."' and c.waktu_order<DATE_ADD(a.tanggal_daftar, INTERVAL 6 MONTH) GROUP BY b.id_konsumen")->row_array();
		
		$all_pencairan = $this->db->query("select sum(jumlah_pencairan) as jumlah_pencairan from rb_pencairan_referal where id_konsumen='".$this->session->id_konsumen."' GROUP BY id_konsumen ")->row_array();
						
		$referal_detail = $this->db->query("select YEAR(c.waktu_order) as tahun, MONTHNAME(STR_TO_DATE(MONTH(c.waktu_order), '%m')) as bulan, a.id_reseller,nama_reseller,b.id_konsumen,b.nama_lengkap, a.tanggal_daftar,c.waktu_order, DATE_ADD(a.tanggal_daftar, INTERVAL 6 MONTH) as 'next_6_bulan', sum(c.komisi_topsonia) as komisi_topsonia, round((sum(c.komisi_topsonia)*1)/100) as bonus_referal from rb_reseller a join rb_konsumen b on a.referral=b.kode_referall join rb_penjualan c on a.id_reseller=c.id_penjual where c.proses='1' and b.id_konsumen='".$this->session->id_konsumen."' and c.waktu_order<DATE_ADD(a.tanggal_daftar, INTERVAL 6 MONTH) GROUP BY YEAR(c.waktu_order),MONTH(c.waktu_order),a.id_reseller")->result_array();

		$rekening = $this->db->query("select no_rekening from rb_konsumen where id_konsumen='".$this->session->id_konsumen."' ")->row_array();


		echo "<div role='tabpanel' class='tab-pane fade' id='referal' aria-labelledby='referal-tab'>
			<div id=''>
							 
				<div style='margin-buttom:30px;'>
				Total Bonus : Rp. ".rupiah($all_referal['bonus_referal'])."
				</div>
				<div style='margin-buttom:30px;'>
				Total Pencairan Bonus : Rp. ".rupiah($all_pencairan['jumlah_pencairan'])."
				</div>
				<div style='margin-buttom:30px;'>
				Sisa Bonus : Rp. ".rupiah($all_referal['bonus_referal']-$all_pencairan['jumlah_pencairan'])."
				</div>
				<div style='margin-buttom:30px;'>
				No Rekening : $rekening[no_rekening]
				</div>
			
				<table class='table table-hover table-condensed'>
					<tr>
						<th>No</th>
						<th>Nama Reseller</th>
						<th>Tahun</th>
						<th>Bulan</th>
						<th>Tanggal Penarikan</th>
                        <th>Jumlah Penarikan</th>
                        <th>Sisa Bonus</th>
					</tr>";
				
				 	$no=1; foreach($referal_detail as $ref){ 
					
						$id_reseller = $ref['id_reseller'];
						$id_konsumen = $ref['id_konsumen'];
						$tahun = $ref['tahun'];
						$bulan = date('m',strtotime($ref['bulan']));
		
						$pencairan = $this->db->query("select sum(jumlah_pencairan) as jumlah_pencairan, max(tanggal_pencairan) as tanggal_pencairan from rb_pencairan_referal where id_reseller='$id_reseller' and id_konsumen='$id_konsumen' and tahun='$tahun' and bulan='$bulan' GROUP BY id_reseller,tahun,bulan ORDER BY id_pencairan_bonus desc");
						$pencairan_cek = $pencairan->num_rows();
						
						if($pencairan_cek>0){
		
						  $pencairan_detail = $pencairan->row_array();
						  $tanggal_pencairan = $pencairan_detail['tanggal_pencairan'];
						  $jumlah_pencairan = "Rp ".rupiah($pencairan_detail['jumlah_pencairan']);
						  $sisa_saldo = "Rp ".rupiah($ref['bonus_referal']-$pencairan_detail['jumlah_pencairan']);
					   
						}else{
						  $tanggal_pencairan = "";
						  $jumlah_pencairan = "";
						  $sisa_saldo = "Rp ".rupiah($ref['bonus_referal']);
						}

					echo "<tr>
							<td> $no </td>
							<td> $ref[nama_reseller] </td>
							<td> $ref[tahun] </td>
							<td> $ref[bulan] </td>
							<td>$tanggal_pencairan</td>
                  			<td>$jumlah_pencairan</td>
                  			<td>$sisa_saldo</td>
						</tr>";
				
						$no++;
					}
			echo "</table>
			</div>
        </div>";
		
		$all_komisi = $this->db->query("select sum(b.diskon) as diskon from rb_konsumen a join rb_penjualan b on a.id_konsumen=b.id_pembeli where b.proses='1' and a.id_konsumen='".$this->session->id_konsumen."' and b.status_pembeli='konsumen' and b.waktu_order>='2021-09-01' GROUP BY a.id_konsumen")->row_array();
		
		$all_pencairan_komisi = $this->db->query("select sum(jumlah_pencairan) as jumlah_pencairan from rb_pencairan_komisi where id_konsumen='".$this->session->id_konsumen."' and status='Lunas' GROUP BY id_konsumen ")->row_array();
						
		$komisi_detail = $this->db->query("select a.id_pencairan_bonus,a.status,a.jumlah_pencairan,a.tanggal_request,a.tanggal_pencairan,a.id_konsumen,b.nama_lengkap from rb_pencairan_komisi a join rb_konsumen b on a.id_konsumen=b.id_konsumen where a.id_konsumen='".$this->session->id_konsumen."' ORDER BY status desc,id_pencairan_bonus asc")->result_array();

		$rekening = $this->db->query("select no_rekening from rb_konsumen where id_konsumen='".$this->session->id_konsumen."' ")->row_array();
		
		$cek_status_komisi = $this->db->query("select jumlah_pencairan from rb_pencairan_komisi where id_konsumen='".$this->session->id_konsumen."' and status='Request' ")->num_rows();
		
		if($cek_status_komisi>0){
			$alert = '"Tidak Bisa Melakukan Request Pencairan Jika Pencairan Yang Di request sebelumnya belum di proses"';
			$btn= "<data-toggle='modal' data-target='#exampleModal' type='button' class='btn btn-info btn-xs' style='float: right; margin: 10px 0;'  onClick='alert($alert)'>Request Pencairan</button>";
		}else{
			$btn = "<button data-toggle='modal' data-target='#exampleModal' type='button' class='btn btn-info btn-xs' style='float: right; margin: 10px 0;'>Request Pencairan</button> ";
		}

		echo "<div role='tabpanel' class='tab-pane fade' id='komisi' aria-labelledby='komisi-tab'>
			<div id=''>
							 
				<div style='margin-buttom:30px;'>
				Total Komisi : Rp. ".rupiah($all_komisi['diskon'])."
				</div>
				<div style='margin-buttom:30px;'>

				Total Pencairan Komisi : Rp. ".rupiah($all_pencairan_komisi['jumlah_pencairan'])."
				</div>
				<div style='margin-buttom:30px;'>
				Sisa Komisi : Rp. ".rupiah($all_komisi['diskon']-$all_pencairan_komisi['jumlah_pencairan'])."
				</div>
				<div style='margin-buttom:30px;'>
				No Rekening : $rekening[no_rekening]
				</div>

				<p>
				$btn				
				</p>

				<form id='form_request' action='".base_url()."members/request_pencairan_komisi' method='POST'>
				<div class='modal fade' id='exampleModal' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel' aria-hidden='true'>
				  <div class='modal-dialog modal-sm' role='document'>
					<div class='modal-content'>
					  <div class='modal-header'>
						<h5 class='modal-title' id='exampleModalLabel'>Request Pencairan Komisi</h5>
						<button type='button' class='close' data-dismiss='modal' aria-label='Close'>
						  <span aria-hidden='true'>&times;</span>
						</button>
					  </div>
					  <div class='modal-body'>
					  	<div class='form-group'>
					  		<label for='recipient-name' class='col-form-label'>Jumlah Pencairan</label>
					  		<input type='text' class='form-control' id='jumlah_pencairan' name='jumlah_pencairan'>
							<input type='hidden' class='form-control' id='id_konsumen_r' name='id_konsumen' value='".$this->session->id_konsumen."'>
							<input type='hidden' class='form-control' id='sisa_komisi_r' name='sisa_komisi' value='".($all_komisi['diskon']-$all_pencairan_komisi['jumlah_pencairan'])."'>
				
						</div>
					  </div>
					  <div class='modal-footer'>
						<button type='button' class='btn btn-secondary btn-xs' data-dismiss='modal'>Close</button>
						<button type='button' class='btn btn-primary btn-xs' onclick='send_request();'>Request</button>
					  </div>
					</div>
				  </div>
				</div>
				</form>

				<table class='table table-hover table-condensed'>
					<tr>
						<th>No</th>
						<th>Tanggal Request</th>
                        <th>Jumlah Penarikan</th>
                        <th>Tanggal Pencairan</th>
						<th>Status</th>
					</tr>";
				
				 	$no=1; foreach($komisi_detail as $row){ 

                        echo "<tr><td>$no</td>
								<td>$row[tanggal_request]</td>
								<td>$row[jumlah_pencairan]</td>
								<td>$row[tanggal_pencairan]</td>
								<td>$row[status]</td>
                          	  </tr>";
                        $no++;
					}
			echo "</table>
			</div>
        </div>";

      echo "</div>
    </div>";
	?>
		
<script>
	$(document).ready(function(){
		var id;
		$(myTabs).click(function(e) {
			e.preventDefault();
			// $(this).tab('show');
		  
			$("ul.nav-tabs > li > a").on("shown.bs.tab", function(e) {
			  id = $(e.target).attr("href").substr(1);
			  window.history.replaceState(null, null, "?tab="+id);
			});
		});
		
		const urlParams = new URLSearchParams(window.location.search);
		const tabID = urlParams.get('tab');
		
		// if(tabID != null){
			$('#myTabs a[href="#' + tabID +'"]').tab('show');
		// }else{
			// $('#myTabs a[href="#profile"]').tab('show');
		// }
		
		
		$.ajax({
			type: 'POST',
			url: '<?php echo base_url(); ?>members/view_address_json',
			dataType: 'JSON',
			success: function(response) {
			$('#div_address').html(response);
			}
		});
		
		window.onpopstate = function(e){
			if(e.state){
				document.getElementById("div_address").innerHTML = e.state.html;
				document.title = e.state.pageTitle;
			}
		};
	});

function create_code() {
	$.ajax({
			type: 'POST',
			data : { username : "<?php echo $row['username']; ?>" },
			url: '<?php echo base_url(); ?>members/get_kode_referal',
			dataType: 'JSON',
			success: function(response) {
			document.getElementById('code').innerHTML =response.kode_referall;	
			}
		});
	
}

function send_request(){
	
	let id_konsumen = document.getElementById('id_konsumen_r').value;
	let sisa_komisi = document.getElementById('sisa_komisi_r').value;
	let jumlah_pencairan = document.getElementById('jumlah_pencairan').value;
	
	if(parseInt(jumlah_pencairan) > parseInt(sisa_komisi)){
		alert('Jumlah Pencarian Tidak Bisa Melebihi Sisa Komisi');
	}else if(parseInt(jumlah_pencairan)<10000){
		alert('Jumlah Pencarian Minimal Rp. 10.000')
	}else{
		document.getElementById("form_request").submit(); 
	}
}

</script>
