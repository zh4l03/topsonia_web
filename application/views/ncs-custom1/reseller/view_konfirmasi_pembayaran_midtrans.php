
<p class='sidebar-title block-title'> Konfirmasi Pembayaran Pesanan Anda</p>
<div id="paymentinfo">
	<div class="form-group">
		<div class="row" style="margin-bottom:10px;">
			<div class="col-12">
				<center>
				<?php if($detail['status_pembayaran']=='diperiksa'){ ?>
				<div style="min-height:40px;width:400px;background-color:#eeeeee;border-radius:5px;padding:10px">
						Waktu Pembayaran <br>
						<span style="color:#f08519;font-size:20px" id="waktu_bayar"></span>
					</div>
				<?php } ?>
					<div style="min-height:40px;width:400px;background-color:#eeeeee;border-radius:5px;padding:10px">
						Total Bayar <br>
						<span style="color:#f08519;font-size:20px">Rp. <b><?php echo $rows['total_bayar']; ?></b></span>
					</div>
				</center>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
			<?php
			if(in_array($detail['jenis_pembayaran'],array("bca","bri","mandiri","permata","bni","other_bank"))){
				
				$jenis_pembayaran = "Nama Bank : ". strtoupper($detail['jenis_pembayaran']);
				$pmb = "Va Number : " .$detail['kode_pembayaran'];
				$ket ="Silahkan transfer pembayaran sesuai Total Bayar ke Rekening Virtual Acount Berikut :";
				$logo = "virtual_account.png";
				if($detail['jenis_pembayaran']=='bca'){
					$btn="bank-bca";
				}elseif($detail['jenis_pembayaran']=='bri'){
					$btn="bank-bri";
				}elseif($detail['jenis_pembayaran']=='mandiri'){
					$btn="bank-mandiri";
				}elseif($detail['jenis_pembayaran']=='permata'){
					$btn="bank-permata";
				}elseif($detail['jenis_pembayaran']=='bni'){
					$btn="bank-bni";
				}elseif($detail['jenis_pembayaran']=='other_bank'){
					$btn="other-bank";
					$jenis_pembayaran = "Semua Bank";
				}
			} else if(in_array($detail['jenis_pembayaran'],array("alfamart","indomaret"))){
				$jenis_pembayaran = "Pembayaran Via : ". strtoupper($detail['jenis_pembayaran']);
				$pmb = "Kode Pembayaran : " .$detail['kode_pembayaran'];
				$ket="Silahkan melakukan pembayaran sesuai Total Bayar ke ".$detail['jenis_pembayaran']. " Terdekat :";
				$logo ="alfaindo.png";
				$btn="alfaindo";
				$mer = "<H2><b>Nama Merchant : NCS</b></H2>";
			}
			?>
				<center>
				<div id="paymentmanual">
			<div class="form-group">
				<div class="col-12">
					<?php if($this->midtrans->status($rows['kode_transaksi'])->transaction_status=="settlement"){ 
						$ipt="hidden";
						$this->model_app->update('rb_penjualan',array('bayar'=>'1'), array('kode_transaksi'=>$rows['kode_transaksi'],'bayar'=>'0'));
						$this->model_app->update('rb_konfirmasi_pembayaran_konsumen',array('status_pembayaran'=>'lunas'), array('kode_transaksi'=>$rows['kode_transaksi'],'status_pembayaran'=>'diperiksa'));
					?>
					<center>
						<img style="height:120px" src="<?php echo base_url()."asset/ket_pembayaran/".$logo; ?>"/> <br />
						<br/>
						<span>Transaksi Sudah Selesai Pembayaran Berhasil</span><br/><br/>
						<H2><b>Id Transaksi : <?php echo $rows['kode_transaksi'] ?></b></H2>
						<H2><b><?php echo $jenis_pembayaran; ?></b></H2>
					</center>
					<?php } else if($this->midtrans->status($rows['kode_transaksi'])->transaction_status=="expire"){ 
						$ipt="hidden";
						$this->model_app->update('rb_penjualan',array('bayar'=>'2'), array('kode_transaksi'=>$rows['kode_transaksi']));
						$this->model_app->update('rb_konfirmasi_pembayaran_konsumen',array('status_pembayaran'=>'gagal'), array('kode_transaksi'=>$rows['kode_transaksi'],'status_pembayaran'=>'diperiksa'));
					?>
					<center>
						<img style="height:120px" src="<?php echo base_url()."asset/ket_pembayaran/".$logo; ?>"/> <br />
						<br/>
						<span>Waktu Transaksi selesai  Pembayaran Gagal</span><br/><br/>
						<H2><b>Id Transaksi : <?php echo $rows['kode_transaksi'] ?></b></H2>
						<H2><b><?php echo $jenis_pembayaran; ?></b></H2>
					</center>
					<?php }else{
						$ipt ='visible';
					?>
					<center>
						<img style="height:120px" src="<?php echo base_url()."asset/ket_pembayaran/".$logo; ?>"/> <br />
						<br/>
						<span><?php echo $ket; ?></span><br/><br/>
						<H2><b><?php echo $jenis_pembayaran; ?></b></H2>
						<H2><b><?php echo $pmb ?></b></H2>
						<?php echo $mer; ?>
					</center>
					<?php } ?>
				</div>
			</div>
		</div>

				
				</Table>
				</center>
			</div>
		</div>
	</div>
</div>
<div class="box-footer">
	<center>
	<a class="btn btn-info btn-md pull-left" style="margin-left:5px;" href="<?php echo base_url(); ?>members/orders_report"><span class="glyphicon glyphicon-shopping-cart"></span> Lihat Daftar Pesanan</a>
	<a class="btn btn-info btn-md pull-right" style="margin-left:5px;" href="<?php echo base_url(); ?>konfirmasi?idp=<?php echo $rows['kode_transaksi']; ?>">Cek Status Pembayaran</a>
	<button style="visibility : <?php echo $ipt; ?>" class="btn btn-info btn-md"  style="margin-left:5px;" data-toggle="modal" data-target="#<?php echo $btn; ?>">Tata Cara Pembayaran</button>
	</center>
</div>


<div class="modal fade" id="bank-bca" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                 <h2 class="modal-title" id="myModalLabel">Intruksi</h2>
            </div>
            <div class="modal-body">
                <div role="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#atm" aria-controls="atm" role="tab" data-toggle="tab"><h2> ATM BCA  </h2></a>
                        </li>
                        <li role="presentation"><a href="#klick" aria-controls="klick" role="tab" data-toggle="tab"><h2>Klik BCA </h2></a>
                        </li>
						<li role="presentation"><a href="#mbanking" aria-controls="mbanking" role="tab" data-toggle="tab"><h2> M BCA </h2></a>
                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="atm"><br/>
						<h3>1.Di menu utama, pilih Transaksi Lainnya.</h3>
						<h3>2.Pilih Transfer.</h3>
						<h3>3.Pilih Transfer Ke Virtual Account BCA.</h3>
						<h3>4.Masukkan Kode Pembayaran Anda (<?php echo $detail['kode_pembayaran'];?>) dan tekan Benar.</h3>
						<h3>5.Masukkan jumlah penuh yang harus dibayar dan tekan Benar.</h3>
						<h3>6.Detail pembayaran Anda akan muncul di halaman konfirmasi pembayaran. Jika informasinya benar tekan Yes.</h3>
						</div>
                        <div role="tabpanel" class="tab-pane" id="klick"><br/>
						<h3>1. Pilih Menu Transfer Dana.</h3>
						<h3>2. Pilih Transfer Ke Virtual Account BCA.</h3>
						<h3>3. Masukkan Nomor Virtual Account BCA (<?php echo $detail['kode_pembayaran'];?>) atau Pilih dari daftar Transfer dan klik Lanjutkan.</h3>
						<h3>4. Jumlah yang harus dibayar, nomor rekening dan nama Merchant akan muncul pada halaman konfirmasi pembayaran, jika informasinya klik kanan Lanjutkan.</h3>
						<h3>5. Dapatkan token BCA Anda dan masukkan KEYBCA Response APPLI 1 dan klik Kirim.</h3>
						<h3>6. Transaksi Anda Selesai.</h3> 
						</div>
						<div role="tabpanel" class="tab-pane" id="mbanking"><br/>
						<h3>1. Login ke aplikasi BCA Mobile Anda.</h3>
						<h3>2. Pilih m-BCA, lalu masukkan kode akses m-BCA Anda.</h3>
						<h3>3. Pilih m-Transfer, lalu pilih BCA Virtual Account.</h3>
						<h3>4. Masukkan Nomor Virtual Account (<?php echo $detail['kode_pembayaran'];?>) atau pilih akun yang ada dari Daftar Transfer.</h3>
						<h3>5. Masukkan jumlah yang harus dibayar.</h3>
						<h3>6. Masukkan pin m-BCA Anda.</h3>
						<h3>7. Pembayaran selesai. Simpan notifikasi sebagai tanda terima pembayaran Anda.</h3> 
						</div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="bank-bri" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                 <h2 class="modal-title" id="myModalLabel">Intruksi</h2>
            </div>
            <div class="modal-body">
                <div role="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#atm-bri" aria-controls="atm-bri" role="tab" data-toggle="tab"><h2> ATM BRI </h2></a>
                        </li>
                        <li role="presentation"><a href="#ibri" aria-controls="ibri" role="tab" data-toggle="tab"><h2> IB BRI</h2></a>
                        </li>
						<li role="presentation"><a href="#brimo" aria-controls="brimo" role="tab" data-toggle="tab"><h2> BRImo</h2></a>
                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="atm-bri"><br/>
						<h3>1. Pada menu utama, pilih Transaksi Lainnya.</h3>
						<h3>2. Pilih Pembayaran.</h3>
						<h3>3. Pilih Lainnya.</h3>
						<h3>4. Pilih BRIVA.</h3>
						<h3>5. Masukkan Nomor BRIVA Anda (<?php echo $detail['kode_pembayaran'];?>) dan tekan Benar.</h3>
						<h3>6. Jumlah yang harus dibayar, kode pembayaran, dan nama merchant akan muncul di halaman konfirmasi pembayaran. Jika informasinya benar, tekan Yes.</h3>
						<h3>7. Pembayaran selesai. Simpan tanda terima pembayaran Anda.</h3> 
						</div>
                        <div role="tabpanel" class="tab-pane" id="ibri"><br/>
						<h3>1. Login ke Internet Banking BRI Anda.</h3>
						<h3>2. Pilih Pembayaran & Pembelian.</h3>
						<h3>3. Pilih sub menu BRIVA.</h3>
						<h3>4. Masukkan nomor BRIVA (<?php echo $detail['kode_pembayaran'];?>).</h3>
						<h3>5. Jumlah yang harus dibayar, kode pembayaran, dan nama merchant akan muncul di halaman konfirmasi pembayaran. Jika informasinya benar, pilih Send.</h3>
						<h3>6. Masukkan kata sandi dan mToken, pilih Kirim.</h3>
						<h3>7. Pembayaran selesai, pilih Cetak untuk mendapatkan bukti pembayaran.</h3>
						</div>
						<div role="tabpanel" class="tab-pane" id="brimo"><br/>
						<h3>1. Login ke aplikasi BRI Mobile Anda, pilih Mobile Banking BRI.</h3>
						<h3>2. Pilih Pembayaran, lalu pilih BRIVA.</h3>
						<h3>3. Masukkan Nomor BRIVA (<?php echo $detail['kode_pembayaran'];?>).</h3>
						<h3>4. Jumlah yang harus dibayar, kode pembayaran, dan nama merchant akan muncul di halaman konfirmasi pembayaran. Jika informasinya benar, pilih Lanjutkan.</h3>
						<h3>5. Masukkan PIN BRI Mobile Banking Anda, pilih Ok.</h3>
						<h3>6. Pembayaran selesai. Simpan notifikasi sebagai tanda terima pembayaran Anda.</h3> 
						</div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="bank-mandiri" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                 <h2 class="modal-title" id="myModalLabel">Intruksi</h2>
            </div>
            <div class="modal-body">
                <div role="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#atm-mandiri" aria-controls="atm-mandiri" role="tab" data-toggle="tab"><h2> ATM MANDIRI </h2></a>
                        </li>
                        <li role="presentation"><a href="#i-mandiri" aria-controls="i-mandiri" role="tab" data-toggle="tab"><h2>Internet Banking </h2></a>
                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="atm-mandiri"><br/>
						<h3>1.Di menu utama, pilih Bayar / Beli.</h3>
						<h3>2.Pilih Lainnya.</h3>
						<h3>3.Pilih Multi Pembayaran.</h3>
						<h3>4.Masukkan 70012 (kode perusahaan Midtrans) dan tekan Benar.</h3>
						<h3>5.Masukkan Kode Pembayaran Anda(<?php echo $detail['kode_pembayaran'];?>) dan tekan Benar.</h3>
						<h3>6.Detail pembayaran Anda akan muncul di halaman konfirmasi pembayaran. Jika informasinya benar tekan Yes.</h3> 
						</div>
                        <div role="tabpanel" class="tab-pane" id="i-mandiri"><br/>
						<h3>1. Login ke Mandiri Internet Banking (https://ibank.bankmandiri.co.id/).</h3>
						<h3>2. Dari menu utama pilih Pembayaran, lalu pilih Multi Pembayaran.</h3>
						<h3>3. Pilih akun Anda di Dari Akun, lalu di Nama Penagihan pilih Midtrans.</h3>
						<h3>4. Masukkan Kode Pembayaran (<?php echo $detail['kode_pembayaran'];?>) dan Anda akan menerima rincian pembayaran Anda.</h3>
						<h3>5. Konfirmasi pembayaran Anda menggunakan Token Mandiri Anda.</h3> 
						</div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="bank-permata" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                 <h2 class="modal-title" id="myModalLabel">Intruksi</h2>
            </div>
            <div class="modal-body">
                <div role="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#atm" aria-controls="atm" role="tab" data-toggle="tab"><h2> ATM PERMATA </h2></a>
                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="atm"><br/>
						<H3>1.Di menu utama, pilih Transaksi Lainnya.</H3>
						<H3>2.Pilih Pembayaran.</H3>
						<H3>3.Pilih Pembayaran Lain.</H3>
						<H3>4.Pilih Akun Virtual.</H3>
						<H3>5.Masukkan 16 digit No. Rekening (<?php echo $detail['kode_pembayaran'];?>) dan tekan Benar.</H3>
						<H3>6.Jumlah yang harus dibayar, nomor rekening, dan nama merchant akan muncul di halaman konfirmasi pembayaran. Jika informasinya benar, tekan Benar.</H3>
						<H3>7.Pilih akun pembayaran Anda dan tekan Benar.</H3> 
						</div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="bank-bni" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                 <h2 class="modal-title" id="myModalLabel">Intruksi</h2>
            </div>
            <div class="modal-body">
                <div role="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#atm-bni" aria-controls="atm-bni" role="tab" data-toggle="tab"><h2>ATM BNI  </h2></a>
                        </li>
                        <li role="presentation"><a href="#i-bni" aria-controls="i-bni" role="tab" data-toggle="tab"><h2>IBanking </h2></a>
                        </li>
						<li role="presentation"><a href="#m-bni" aria-controls="m-bni" role="tab" data-toggle="tab"><h2>M Banking </h2></a>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="atm-bni"><br/>
						<h3>1. Di menu utama, pilih Lainnya.</h3>
						<h3>2. Pilih Transfer.</h3>
						<h3>3. Pilih Rekening Tabungan.</h3>
						<h3>4. Pilih Ke Rekening BNI.</h3>
						<h3>5. Masukkan nomor rekening pembayaran(<?php echo $detail['kode_pembayaran'];?>) dan tekan Ya.</h3>
						<h3>6. Masukkan jumlah penuh yang akan dibayarkan. Jika jumlah yang dimasukkan tidak sama dengan jumlah yang ditagih, transaksi akan ditolak.</h3>
						<h3>7. Jumlah yang harus dibayar, nomor rekening, dan nama merchant akan muncul di halaman konfirmasi pembayaran. Jika informasinya benar, tekan Yes.</h3>
						<h3>8. Anda sudah selesai.</h3> 
						</div>
                        <div role="tabpanel" class="tab-pane" id="i-bni"><br/>
						<h3>1. Buka https://ibank.bni.co.id lalu klik Login.</h3>
						<h3>2. Lanjutkan login dengan User ID dan Password Anda.</h3>
						<h3>3. Klik Transfer lalu Tambah Rekening Favorit dan pilih Antar Rekening BNI.</h3>
						<h3>4. Masukkan nama akun, nomor akun(<?php echo $detail['kode_pembayaran'];?>), dan email, lalu klik Lanjutkan.</h3>
						<h3>5. Masukkan Kode Otentikasi dari token Anda, lalu klik Lanjutkan.</h3>
						<h3>6. Kembali ke menu utama dan pilih Transfer lalu Transfer Antar Rekening BNI.</h3>
						<h3>7. Pilih akun yang baru saja Anda buat pada langkah sebelumnya sebagai Rekening Tujuan dan isi selebihnya sebelum mengklik Lanjutkan.</h3>
						<h3>8. Periksa apakah detailnya sudah benar, jika sudah, silahkan masukkan Kode Otentikasi dan klik Lanjutkan dan Anda selesai.</h3> 
						</div>
						<div role="tabpanel" class="tab-pane" id="m-bni"><br/>
						<h3>1. Buka aplikasi BNI Mobile Banking dan login</h3>
						<h3>2. Pilih menu Transfer</h3>
						<h3>3. Pilih menu Virtual Account Billing</h3>
						<h3>4. Pilih rekening bank yang ingin Anda gunakan</h3>
						<h3>5. Masukkan 16 digit nomor rekeningnya(<?php echo $detail['kode_pembayaran'];?>)</h3>
						<h3>6. Informasi billing akan muncul di halaman validasi pembayaran</h3>
						<h3>7. Jika informasinya benar, masukkan password Anda untuk melanjutkan pembayaran</h3>
						<h3>8. Transaksi Anda akan diproses</h3> 
						</div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="bank-bni" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                 <h2 class="modal-title" id="myModalLabel">Intruksi</h2>
            </div>
            <div class="modal-body">
                <div role="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#atm-bni" aria-controls="atm-bni" role="tab" data-toggle="tab"><h2>ATM BNI  </h2></a>
                        </li>
                        <li role="presentation"><a href="#i-bni" aria-controls="i-bni" role="tab" data-toggle="tab"><h2>IBanking </h2></a>
                        </li>
						<li role="presentation"><a href="#m-bni" aria-controls="m-bni" role="tab" data-toggle="tab"><h2>M Banking </h2></a>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="atm-bni"><br/>
						<h3>1. Di menu utama, pilih Lainnya.</h3>
						<h3>2. Pilih Transfer.</h3>
						<h3>3. Pilih Rekening Tabungan.</h3>
						<h3>4. Pilih Ke Rekening BNI.</h3>
						<h3>5. Masukkan nomor rekening pembayaran(<?php echo $detail['kode_pembayaran'];?>) dan tekan Ya.</h3>
						<h3>6. Masukkan jumlah penuh yang akan dibayarkan. Jika jumlah yang dimasukkan tidak sama dengan jumlah yang ditagih, transaksi akan ditolak.</h3>
						<h3>7. Jumlah yang harus dibayar, nomor rekening, dan nama merchant akan muncul di halaman konfirmasi pembayaran. Jika informasinya benar, tekan Yes.</h3>
						<h3>8. Anda sudah selesai.</h3> 
						</div>
                        <div role="tabpanel" class="tab-pane" id="i-bni"><br/>
						<h3>1. Buka https://ibank.bni.co.id lalu klik Login.</h3>
						<h3>2. Lanjutkan login dengan User ID dan Password Anda.</h3>
						<h3>3. Klik Transfer lalu Tambah Rekening Favorit dan pilih Antar Rekening BNI.</h3>
						<h3>4. Masukkan nama akun, nomor akun(<?php echo $detail['kode_pembayaran'];?>), dan email, lalu klik Lanjutkan.</h3>
						<h3>5. Masukkan Kode Otentikasi dari token Anda, lalu klik Lanjutkan.</h3>
						<h3>6. Kembali ke menu utama dan pilih Transfer lalu Transfer Antar Rekening BNI.</h3>
						<h3>7. Pilih akun yang baru saja Anda buat pada langkah sebelumnya sebagai Rekening Tujuan dan isi selebihnya sebelum mengklik Lanjutkan.</h3>
						<h3>8. Periksa apakah detailnya sudah benar, jika sudah, silahkan masukkan Kode Otentikasi dan klik Lanjutkan dan Anda selesai.</h3> 
						</div>
						<div role="tabpanel" class="tab-pane" id="m-bni"><br/>
						<h3>1. Buka aplikasi BNI Mobile Banking dan login</h3>
						<h3>2. Pilih menu Transfer</h3>
						<h3>3. Pilih menu Virtual Account Billing</h3>
						<h3>4. Pilih rekening bank yang ingin Anda gunakan</h3>
						<h3>5. Masukkan 16 digit nomor rekeningnya(<?php echo $detail['kode_pembayaran'];?>)</h3>
						<h3>6. Informasi billing akan muncul di halaman validasi pembayaran</h3>
						<h3>7. Jika informasinya benar, masukkan password Anda untuk melanjutkan pembayaran</h3>
						<h3>8. Transaksi Anda akan diproses</h3> 
						</div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="other-bank" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                 <h2 class="modal-title" id="myModalLabel">Intruksi</h2>
            </div>
            <div class="modal-body">
                <div role="tabpanel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#bank-lainnya" aria-controls="bank-lainnya" role="tab" data-toggle="tab"><h2>Bank Lainnya  </h2></a>
                        </li>
                        </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
						<div role="tabpanel" class="tab-pane active" id="bank-lainnya"><br/>
						<h3>1. Di menu utama, pilih Transaksi Lainnya.</h3>
						<h3>2. Pilih Transfer.</h3>
						<h3>3. Pilih Rekening Bank Lain.</h3>
						<h3>4. Masukkan 009 (kode Bank BNI) dan pilih Benar.</h3>
						<h3>5. Masukkan jumlah penuh yang akan dibayarkan. Jika jumlah yang dimasukkan tidak sama dengan jumlah yang ditagih, transaksi akan ditolak.</h3>
						<h3>6. Masukkan 16 digit No. Rekening pembayaran dan tekan Benar.</h3>
						<h3>7. Jumlah yang harus dibayar, nomor rekening, dan nama merchant akan muncul di halaman konfirmasi pembayaran. Jika informasinya benar, tekan Benar.</h3> 
						</div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<!-- Modal alfamaret dan indo -->
<div class="modal fade" id="alfaindo" role="dialog">
<div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h2 class="modal-title">Intruksi</h2>
        </div>
        <div class="modal-body">
		<h3>1. Catat Kode Pembayaran Anda dan jumlah totalnya</h3>
		<h3>2. Kunjungi toko <?php echo $detail['jenis_pembayaran']; ?> terdekat dan berikan nomor Kode Pembayaran kepada kasir.</h3>
		<h3>3. Kasir kemudian akan mengkonfirmasi transaksi dengan menanyakan jumlah transaksi dan nama merchant.</h3>
		<h3>4. Konfirmasi pembayaran dengan kasir.</h3>
		<h3>5. Transaksi Anda berhasil!. Harap simpan bukti pembayaran Indomaret Anda untuk berjaga-jaga jika Anda membutuhkan bantuan melalui dukungan.</h3> 
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>      
    </div>
  </div>
</div>

<?php if($detail['status_pembayaran']=='diperiksa'){ ?>
<script>
// Silahkan anda atur tanggal anda
// var countDownDate = new Date("Sep 5, 2021 15:37:25").getTime();
// var countDownDate = new Date("2021-07-28 15:37:25").getTime();
var countDownDate = new Date("<?php echo date('Y-m-d H:i:s', strtotime('+2 days', strtotime($rows['waktu_order']))); ?>").getTime();

// Hitungan Mundur Waktu Dilakukan Setiap Satu Detik
var x = setInterval(function() {
  // Mendapatkan Tanggal dan waktu Pada Hari ini
  var now = new Date().getTime();
  //Jarak Waktu Antara Hitungan Mundur
  var distance = countDownDate - now;
  // Perhitungan waktu hari, jam, menit dan detik
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
  // Tampilkan hasilnya di elemen id = "carasingkat"
  document.getElementById("waktu_bayar").innerHTML = days + "hari " + hours + ":"
  + minutes + ":" + seconds;
  // Jika hitungan mundur selesai,
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("waktu_bayar").innerHTML = "EXPIRED";
  }
}, 1000);
</script>
<?php } ?>