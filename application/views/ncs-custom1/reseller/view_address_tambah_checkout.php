<?php 
  echo "<p class='sidebar-title text-danger produk-title'> Tambah alamat baru</p>";                
		  $attributes = array('id' => 'formku','class'=>'form-horizontal','role'=>'form');
		  echo form_open_multipart('members/tambah_alamat_checkout/',$attributes); 
		  $ko = $this->db->query("SELECT * FROM rb_kota where kota_id='$row[kota_id]'")->row_array();
		   echo "
			<div class='row'>
				<div class='col-md-6'>
				<table class='table table-hover table-condensed'>
					<thead>
					</thead>
					<tbody>
						<tr>
							<td><b>Nama</b></td>   
							<td><input class='required form-control' type='text' name='a' value='$row[nama_lengkap]' width='100px'></td>
						</tr>
						<tr>
							<th><b>Alamat Pengiriman</b></th>         
							<td><textarea class='required form-control' style='width:93%' name='b'>$row[alamat_lengkap]</textarea></td>
						</tr>
						<tr>
							<th scope='row'>Provinsi</th>                    
							<td>
								<select class='form-control' name='ag' id='state_reseller' required>
									<option value=''>- Pilih -</option>";
										foreach ($provinsi as $rows) {
										  if ($ko['provinsi_id']==$rows['provinsi_id']){
											echo "<option value='$rows[nama_provinsi]' selected>$rows[nama_provinsi]</option>";
										  }else{
											echo "<option value='$rows[nama_provinsi]'>$rows[nama_provinsi]</option>";
										  }
										}
								echo "</select>
							</td>
						</tr>
						<tr>
							<th scope='row'>Kota</th>                         
							<td><select class='form-control' name='d' id='city_reseller' required>
									<option value=''>- Pilih -</option>";
										$kota = $this->model_app->view_where_ordering('rb_kota',array('provinsi_id'=>$ko['provinsi_id']),'kota_id','DESC');
										foreach ($kota as $rows) {
										  if ($ko['kota_id']==$rows['kota_id']){
											echo "<option value='$rows[kota_id]' selected>$rows[nama_kota]</option>";
										  }else{
											echo "<option value='$rows[kota_id]'>$rows[nama_kota]</option>";
										  }
										}
								echo "</select>
							</td
						</tr>
						<tr>
							<th><b>Kecamatan</b></th>  
							<td><input type='text' class='required form-control' name='c' value='$row[kecamatan]'></td>
						</tr>
						
						<tr>
							<th><b>Kode Pos</b></th>                  
							<td><input class='required number form-control' type='number' name='e' value='$row[kode_pos]'></td>
						</tr>
						
						<tr>
							<th><b>No Hp</b></th>                  
							<td><input class='required number form-control' type='number' name='f' value='$row[no_hp]'></td>
						</tr>
					  <br/>
					  <tr><td></td><td><input class='btn btn-sm btn-primary' type='submit' name='submit' value='Tambah Alamat'><a class='btn btn-sm btn-default' href='".base_url()."members/profile' name='submit'>Kembali</a></td></tr>
					</tbody>
				</table>
			</div>
		</div>";
                  echo form_close();
?>