<div id="refresh_produk">

<?php 
  $no = 1;
  if ($this->input->get('cari')){
    echo '<p class="sidebar-title text-danger produk-title">Hasil Pencarian - "'.$this->input->get('cari').'"</p>';
  }
  if($record->num_rows()<1){
    echo "<p class='sidebar-title text-danger produk-title'>Data Tidak Di Temukan</p>";
  }
    echo "<div class='container'>";
    foreach ($record->result_array() as $row){
      $terjual = $this->db->query("select sum(a.jumlah) as jml from rb_penjualan_detail a join rb_penjualan b on a.id_penjualan=b.id_penjualan where b.selesai='1' and a.id_produk='$row[id_produk]' and b.status_pembeli='konsumen'")->row_array();
      if($terjual['jml']<1){
        $jml_terjual = "<div style='min-height: 20px !important;'></div>";
      }else{
        $jml_terjual = "<div style='min-height: 20px !important;'><i>Terjual ".$terjual['jml']."</i></div>";
      }
    $ex = explode(';', $row['gambar']);
    if (trim($ex[0])==''){ $foto_produk = 'no-image.png'; }else{ $foto_produk = $ex[0]; }
    if (strlen($row['nama_produk']) > 25){ $judul = substr($row['nama_produk'],0,25).',..';  }else{ $judul = $row['nama_produk']; }
    $jual = $this->model_reseller->jual_reseller($row['id_reseller'],$row['id_produk'])->row_array();
    $beli = $this->model_reseller->beli_reseller($row['id_reseller'],$row['id_produk'])->row_array();
    if ($beli['beli']-$jual['jual']<=0){ $stok = '<b style="color:000">Stok Habis</b>'; }else{ $stok = "<span style='color:green'>Stok ".($beli['beli']-$jual['jual'])." $row[satuan]</span>"; }

    $disk = $this->db->query("SELECT * FROM rb_produk_diskon where id_produk='$row[id_produk]'")->row_array();
    $diskon = rupiah(($disk['diskon']/$row['harga_konsumen'])*100,0)."%";
    if ($diskon>0){ $diskon_persen = "<div class='top-right'>$diskon</div>"; }else{ $diskon_persen = ''; }
    if ($diskon>=1){ 
      $harga =  "<del style='color:#8a8a8a'><small>Rp ".rupiah($row['harga_konsumen'])."</small></del> Rp ".rupiah($row['harga_konsumen']-$disk['diskon']);
    }else{
      $harga =  "Rp ".rupiah($row['harga_konsumen']);
    }
    echo "<div class='produk col-md-2 col-xs-12'>
              <center>
                <div style='overflow:hidden'>
                  <a title='$row[nama_produk]' href='".base_url()."produk/detail/$row[produk_seo]'><img style='width:99%' src='".base_url()."asset/foto_produk/$foto_produk'></a>
                $diskon_persen
              </div>
              <h4 class='produk-title produk-title-list'><a title='$row[nama_produk]' href='".base_url()."produk/detail/$row[produk_seo]'>$judul</a></h4>
              ";
              $rating = $this->db->query("select count(rating) as jml_rating, avg(rating) avg_rating from rb_produk_ulasan where id_produk='$row[id_produk]'")->row_array();
              if($rating['jml_rating']<1){
                echo "<div style='min-height: 30px !important;'></div>";
              }else{    
              echo "<div class='stars stars-example-bootstrap' style='min-height: 30px !important;'>
              <select id='example-bootstrap' name='rating' autocomplete='off'>";
                  for ($i=1; $i<=5; $i++){
                      if($i<=$rating['avg_rating']){
                          echo "<option value='1'>$i</option>";
                      }else{
                          echo "<option value='2'>$i</option>";
                      }
                  }
              echo "</select> </div>";
              }
              echo "<span style='color:red;'>$harga</span><br>
              <i>$stok</i><br> $jml_terjual <small>$row[nama_kota]</small>
              </center><br>
          </div>";

      $no++;
    }
    echo "</div>
    <div class='pagination'>";
         echo $this->pagination->create_links();
    echo "</div>
  <div style='clear:both'><br></div>";
 ?>
 </div>