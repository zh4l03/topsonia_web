<style>
.form-control2 {
    display: block;
    width: 100%;
    height: 34px;
    background: #fff;
    padding: 6px 13px;
    font-size: 13px;
    border: 1px solid #d3d3d3;
    font-family: 'Arial';
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

</style>


<center><br/><br/><h1 class='sidebar-title text-danger produk-title'>RESET PASSWORD</h1></center>
    <br>
    <?php 
	if ($this->input->post('id_konsumen')!=''){
		echo $this->session->flashdata('message'); 
    }
   
  
    ?>
    <div class="logincontainer">
        <form method="post" action="<?php echo base_url(); ?>auth/reset_password" role="form" id='formku'>
			<div class="form-group has-feedback">
				 <label for="inputPassword">Password baru</label>
				<input type="password" class="form-control" name='a' placeholder="Input Password Baru" onkeyup="nospaces(this)" required>
				<span class="glyphicon glyphicon-lock form-control-feedback"></span>
			</div>
			<div class="form-group has-feedback">
				<label for="inputPassword"> Re-submit password</label>
				<input type="password" class="form-control" name='b' placeholder="Konfirmasi Password" onkeyup="nospaces(this)" required>
				<input type="hidden" name='id_konsumen' value='<?php echo $this->uri->segment(3) ?>'>
				<span class="glyphicon glyphicon-lock form-control-feedback"></span>
			</div>
            <div align="center">
                <input name='reset' type="submit" class="btn btn-primary" value="Reset Password"> 						
				<a href="<?php echo base_url(); ?>auth/login" class="btn btn-default" data-toggle='modal' >Kembali</a>  <br>
            </div>
        </form>
    </div>
    <div style='clear:both'></div>
</center>


<div class="modal fade" id="lupapass" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h5 class="modal-title" id="myModalLabel">Lupa Password Login?</h5>
            </div>
            <div class="modal-body">
				<?php
				$attributes = array('class'=>'form-horizontal');
				echo form_open($this->uri->segment(1).'/lupa_password',$attributes);
				?>
				<div class="form-group">
					<center>Masukkan Email yang terkait dengan akun</center><br>
					<center><label for="inputEmail3" class="col-sm-2 control-label"></label></center>
					<div style='background:#fff;' class="input-group col-sm-6">
						<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
						<input style='text-transform:lowercase;' type="email" class="required form-control" name="email">
					</div>
				</div>
				<br>
				<div class="form-group">
					<center>
						<button type="submit" name='lupa' class="btn btn-primary btn-sm">Kirimkan Permintaan</button>
						&nbsp; &nbsp; &nbsp;
						<a class="btn btn-default btn-sm" data-dismiss="modal" aria-hidden="true" data-toggle='modal' href='#login' data-target='#login' title="Lupa Password Members">Kembali</a>
					</center>
				</div>
			</form>
			</div>
		</div>
	</div>
</div>