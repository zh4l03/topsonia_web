<p class='sidebar-title block-title'> Konfirmasi Pembayaran Pesanan Anda</p>
<div class="row" style="margin-bottom:10px;">
      <div class="col-12">
        <center>
        <?php if($detail['status_pembayaran']=='diperiksa' || $detail['status_pembayaran']==null ){ ?>
        <div style="min-height:40px;width:400px;background-color:#eeeeee;border-radius:5px;padding:10px">
            Waktu Pembayaran <br>
            <span style="color:#f08519;font-size:20px" id="waktu_bayar"></span>
          </div>
        <?php } ?>
        </center>
      </div>
    </div>
<?php 
if ($this->uri->segment(3)=='success'){
    echo "<div class='alert alert-success' style='margin:10% 0px'><center>Success Melakukan Konfirmasi pembayaran... <br>
                                                                          akan segera kami cek dan proses!</center></div>";
}else{
    $attributes = array('class'=>'form-horizontal','role'=>'form','name'=>'demo');
    $ongk = $this->db->query("SELECT * FROM rb_penjualan where id_penjualan='$rows[id_penjualan]'")->row_array();
    echo form_open_multipart('konfirmasi/index',$attributes); 
    echo "
      <table class='table table-condensed'>
        <tbody>
          <input type='hidden' name='id' value='$rows[id_penjualan]'>
          <input type='hidden' name='idk' value='$rows[kode_transaksi]'>
          <tr><th scope='row' width='120px'>No Invoice</th>       <td><input type='text' name='a' class='form-control' style='width:100%' value='$rows[kode_transaksi]' placeholder='TRX-0000000000' readonly required></td></tr>";
          if ($rows['kode_transaksi']!=''){
            echo "<tr><th scope='row'>Total</th>                  <td><input type='text' name='b2' class='form-control' style='width:50%' value='Rp ".rupiah($rows['total_bayar'])."' readonly required> <input type='hidden' name='b' class='form-control' style='width:50%' value='".$rows['total_bayar']."'></td></tr>
            <tr><th scope='row'>Transfer Ke</th>                  <td><select name='c' class='form-control'  onchange='document.getElementById('rek_penerima').value=this.options[this.selectedIndex].text;'>";
                                                                    foreach ($record->result_array() as $row){
                                                                        echo "<option value='$row[id_rekening]'>$row[nama_bank] - $row[no_rekening], A/N : $row[pemilik_rekening]</option>";
                                                                    }
                                  
            echo "</select> <input type='hidden' name='rek_penerima' id='rek_penerima' value='$row[no_rekening], A/N $row[pemilik_rekening]'></td></tr>
      <tr><th width='130px' scope='row'>Rek Pengirim</th>  <td><input type='text' class='form-control' name='f' required></td></tr>
            <tr><th scope='row'>Nama Pengirim</th>  <td><input type='text' class='form-control' style='width:70%' name='d' required></td></tr>
            <tr><th scope='row'>Tanggal Transfer</th>             <td><input type='text' class='datepicker form-control' style='width:40%' name='e' data-date-format='yyyy-mm-dd' value='".date('Y-m-d')."'></td></tr>
            <tr><th scope='row'>Bukti Transfer</th>               <td><input type='file' id='fileupload' class='form-control' name='userfile' required>Multiple Upload, Allowed File : .gif, jpg, png</td></tr>";
          }
        echo "</tbody>
      </table>

    <div class='box-footer'>";
        if ($rows['kode_transaksi']!=''){
          echo "<button type='submit' name='submit' class='btn btn-info'>Kirimkan</button>";
        }else{
          echo "<button type='submit' name='submit1' class='btn btn-info'>Cek Invoice</button>";
        }
    echo "</div>";
    echo form_close();
}

if($detail['status_pembayaran']=='diperiksa' || $detail['status_pembayaran']==null){ ?>
  <script>
  // Silahkan anda atur tanggal anda
  // var countDownDate = new Date("Sep 5, 2021 15:37:25").getTime();
  // var countDownDate = new Date("2021-07-28 15:37:25").getTime();
  var countDownDate = new Date("<?php echo date('Y-m-d H:i:s', strtotime('+2 days', strtotime($rows['waktu_order']))); ?>").getTime();
  
  // Hitungan Mundur Waktu Dilakukan Setiap Satu Detik
  var x = setInterval(function() {
    // Mendapatkan Tanggal dan waktu Pada Hari ini
    var now = new Date().getTime();
    //Jarak Waktu Antara Hitungan Mundur
    var distance = countDownDate - now;
    // Perhitungan waktu hari, jam, menit dan detik
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    // Tampilkan hasilnya di elemen id = "carasingkat"
    document.getElementById("waktu_bayar").innerHTML = days + "hari " + hours + ":"
    + minutes + ":" + seconds;
    // Jika hitungan mundur selesai,
    if (distance < 0) {
      clearInterval(x);
      document.getElementById("waktu_bayar").innerHTML = "EXPIRED";
    }
  }, 1000);
  </script>
  <?php } ?>

<script>
const input = document.getElementById('fileupload')
input.addEventListener('change', (event) => {
  const target = event.target
  	if (target.files && target.files[0]) {

      /*Maximum allowed size in bytes
        5MB Example
        Change first operand(multiplier) for your needs*/
      const maxAllowedSize = 5 * 1024 * 1024;
      if (target.files[0].size > maxAllowedSize) {
      	// Here you can ask your users to load correct file
        alert('File Harus Kurang Dari 5 MB')
       	target.value = null;
      }
  }
})
</script>