<link rel="stylesheet" href="<?php echo base_url(); ?>template/<?php echo template(); ?>/bar_rating/themes/fontawesome-stars.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>template/<?php echo template(); ?>/bar_rating/themes/css-stars.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>template/<?php echo template(); ?>/bar_rating/themes/bootstrap-stars.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>template/<?php echo template(); ?>/bar_rating/themes/fontawesome-stars-o.css">
 
<p class='sidebar-title text-danger produk-title'>Detail Data Produk Merchant</p>
  <table class='table table-condensed'>
  <tbody>
    <?php if (trim($rows['foto'])==''){ $foto_user = 'blank.png'; }else{ $foto_user = $rows['foto']; } 
    
    $terjual = $this->db->query("select sum(a.jumlah) as jml from rb_penjualan_detail a join rb_penjualan b on a.id_penjualan=b.id_penjualan where b.selesai='1' and b.id_penjual='$rows[id_reseller]' and b.status_pembeli='konsumen'")->row_array();
      if($terjual['jml']<1){
        $jml_terjual = "";
      }else{
        $jml_terjual = "<tr><th scope='row'>Produk Terjual</th> <td>$terjual[jml]</td></tr>";
      }
    ?>
    <tr bgcolor='#e3e3e3'><td rowspan='12' width='110px'><center><?php echo "<img style='border:1px solid #cecece; height:85px; width:85px' src='".base_url()."asset/foto_user/$foto_user' class='img-circle img-thumbnail'>"; ?></center></td></tr>
    <tr><th scope='row' width='140px'>Nama Merchant</th> <td><?php echo $rows['nama_reseller']?></td></tr>
    <tr><th scope='row'>Kota</th> <td><?php echo $rows['nama_kota']?></td></tr>
    <!-- <tr><th scope='row'>No Hp</th> <td><?php //echo $rows['no_telpon']?></td></tr> -->
    <!-- <tr><th scope='row'>Alamat Email</th> <td><?php //echo $rows['email']?></td></tr> -->
    <tr><th scope='row'>Keterangan</th> <td><?php echo $rows['keterangan']?></td></tr>
    <?php echo $jml_terjual; ?>
  </tbody>
  </table>
  <hr>

      <?php 
      if ($record->num_rows()<=0){
          echo "<center style='margin:40px 0px; font-weight:bold; color:#ab0534'>Belum ada Produk pada Merchant ini...</center>";
      }else{
        $no = 1;
        foreach ($record->result_array() as $row){
          $terjual = $this->db->query("select sum(a.jumlah) as jml from rb_penjualan_detail a join rb_penjualan b on a.id_penjualan=b.id_penjualan where b.selesai='1' and a.id_produk='$row[id_produk]' and b.status_pembeli='konsumen'")->row_array();
          if($terjual['jml']<1){
            $jml_terjual = "<div style='min-height: 20px !important;'></div>";
            }else{
            $jml_terjual = "<div style='min-height: 20px !important;'><i>Terjual ".$terjual['jml']."</i></div>";
            }
        $jual = $this->model_reseller->jual_reseller($this->uri->segment(3),$row['id_produk'])->row_array();
        $beli = $this->model_reseller->beli_reseller($this->uri->segment(3),$row['id_produk'])->row_array();
        if ($beli['beli']-$jual['jual']<=0){ $stok = '<b style="color:red">Stok Habis</b>'; }else{ $stok = "Stok ".($beli['beli']-$jual['jual'])." $row[satuan]"; }
        $disk = $this->model_app->edit('rb_produk_diskon',array('id_produk'=>$row['id_produk'],'id_reseller'=>$this->uri->segment(3)))->row_array();
        if ($disk['diskon']==''){ $diskon = '0'; $line = ''; $harga = ''; }else{ $diskon = $disk['diskon']; $line = 'line-through'; $harga = "/ <span style='color:black'>".rupiah($row['harga_konsumen']-$disk['diskon'])."</span>";}

        $ex = explode(';', $row['gambar']);
        if (trim($ex[0])==''){ $foto_produk = 'no-image.png'; }else{ $foto_produk = $ex[0]; }
        if (strlen($row['nama_produk']) > 25){ $judul = substr($row['nama_produk'],0,25).',..';  }else{ $judul = $row['nama_produk']; }
        $disk = $this->db->query("SELECT * FROM rb_produk_diskon where id_produk='$row[id_produk]'")->row_array();
        $diskon = rupiah(($disk['diskon']/$row['harga_konsumen'])*100,0)."%";
        if ($diskon>0){ $diskon_persen = "<div class='top-right'>$diskon</div>"; }else{ $diskon_persen = ''; }
        if ($diskon>=1){ 
          $harga =  "<del style='color:red'><small>Rp ".rupiah($row['harga_konsumen'])."</small></del> Rp ".rupiah($row['harga_konsumen']-$disk['diskon']);
        }else{
          $harga =  "Rp ".rupiah($row['harga_konsumen']);
        }
        echo "<div class='col-md-2 col-xs-6 '>
                  <center>
          <div style='border: 1px solid whitesmoke;border-radius: 10px;box-shadow: 1px 1px whitesmoke;overflow:hidden;background:transparent;min-height: 290px !important;'>
                    <div style='height:140px; overflow:hidden'>
                      <a title='$row[nama_produk]' href='".base_url()."produk/detail/$row[produk_seo]'><img style='min-height:140px; width:99%' src='".base_url()."asset/foto_produk/$foto_produk'></a>
                      $diskon_persen
                    </div>
                    <h4 class='produk-title produk-title-list'><a title='$row[nama_produk]' href='".base_url()."produk/detail/$row[produk_seo]'>$judul</a></h4>";

                    $rating = $this->db->query("select count(rating) as jml_rating, avg(rating) avg_rating from rb_produk_ulasan where id_produk='$row[id_produk]'")->row_array();
                    if($rating['jml_rating']<1){
                      echo "<div style='min-height: 30px !important;'></div>";
                    }else{    
                    echo "<div class='stars stars-example-bootstrap' style='min-height: 30px !important;'>
                    <select id='example-bootstrap' name='rating' autocomplete='off'>";
                    for ($i=1; $i<=5; $i++){
                        if($i<=$rating['avg_rating']){
                            echo "<option value='1'>$i</option>";
                        }else{
                            echo "<option value='2'>$i</option>";
                        }
                    }
                      echo "</select> </div>";
                    }
                    
                    echo "<span style='color:black;'>$harga</span><br>
                    <i>$stok</i><br>
                    $jml_terjual";
                    if ($beli['beli']-$jual['jual']<=0){
                      echo "<a class='btn btn-default btn-block btn-sm' href='#'>Beli Sekarang</a>";
                    }else{
                      if($this->session->level=='konsumen'){
                        echo "<a class='btn btn-default btn-block btn-sm' href='".base_url()."produk/detail/$row[produk_seo]'>Beli Sekarang</a>";
                      }else{
                        echo "<a class='btn btn-default btn-block btn-sm' href='".base_url()."produk/detail/$row[produk_seo]'>Beli Sekarang</a>";
                      }
                    }
                    echo "</center>
              </div>";
          $no++;
        }
      }
      echo "<div style='clear:both'></div>";
      echo "<div class='pagination'>";
      echo $this->pagination->create_links(); 
      echo "</div>";
      echo "<div style='clear:both'></div>";
      ?>

<script type="text/javascript" src="<?php echo base_url(); ?>template/<?php echo template(); ?>/bar_rating/jquery.barrating.min.js"></script>

<script type="text/javascript">
$(window).load(function() {
    $("[id=example-bootstrap]").barrating({
              theme: 'bootstrap-stars',
              showSelectedRating: false,
              readonly: true,
            });
});
</script>