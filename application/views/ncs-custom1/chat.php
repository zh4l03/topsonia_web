<style>
	.blue {
		background-color: #2196F3 !important;
	}
	
	.blue.darken-1 {
		background-color: #1E88E5 !important;
	}

	.blue-text.text-darken-1 {
		color: #1E88E5 !important;
	}

	.blue.darken-2 {
		background-color: #1976D2 !important;
	}

	.blue-text.text-darken-2 {
		color: #1976D2 !important;
	}

	.blue.darken-3 {
		background-color: #1565C0 !important;
	}

	.blue-text.text-darken-3 {
		color: #1565C0 !important;
	}

	.blue.darken-4 {
		background-color: #f08519 !important;
	}

	.blue-text.text-darken-4 {
		color: #0D47A1 !important;
	}
	
	.blue.accent-1 {
	  background-color: #82B1FF !important;
	}

	.blue-text.text-accent-1 {
	  color: #82B1FF !important;
	}

	.blue.accent-2 {
	  background-color: #448AFF !important;
	}

	.blue-text.text-accent-2 {
	  color: #448AFF !important;
	}

	.blue.accent-3 {
	  background-color: #2979FF !important;
	}

	.blue-text.text-accent-3 {
	  color: #2979FF !important;
	}

	.blue.accent-4 {
	  background-color: #2962FF !important;
	  color:white;
	}

	.blue-text.text-accent-4 {
	  color: #2962FF !important;
	}
	

	.thumbnail {
		padding:0px;
	}
	.panel-comment {
		position:relative;width:85%;
	}
	
	.panel-comment > .panel-heading{
		background-color:#E6E2E2 !important;
	}
	.panel-comment>.panel-heading:after,.panel>.panel-heading:before{
		position:absolute;
		top:11px;left:-16px;
		right:100%;
		width:0;
		height:0;
		display:block;
		content:" ";
		border-color:transparent;
		border-style:solid solid outset;
		pointer-events:none;
	}
	
	.panel-comment>.panel-heading:after{
		border-width:7px;
		border-right-color:#E6E2E2;
		margin-top:1px;
		margin-left:2px;
	}
	.panel-comment>.panel-heading:before{
		border-right-color:#ddd;
		border-width:8px;
	}

</style>
<div>
<input type="hidden" id="id_seller" value="<?php echo $id_penjual; ?>" />
<!-- <input type="hidden" id="ref_chat" value="<?php echo $ref_chat; ?>" /> -->
<input type="hidden" id="id_max" value="<?php echo isset($id_max) ? $id_max : '' ; ?>" />
<?php $nama_r = $this->db->query("select nama_reseller from rb_reseller where id_reseller='".$id_penjual."'")->row_array()['nama_reseller']; ?>

	<?php if($chat->num_rows() > 0){ ?>	
		<?php foreach($chat->result_array() as $row){ ?>
			<?php if($row['ket'] == 1){ ?>
				<div class="col-md-12">
					<div class="panel panel-default panel-comment pull-right">
						<div class="panel-heading" >
							<b style="font-size: 13px;">Anda :</b><small class="pull-right" style="color:grey;margin-top:0px;font-size: 13px;"><?php echo date("d-m H:i:s", strtotime($row['created_at'])); ?></small>
							<p style="font-size: 13px;"><?php echo $row['pesan']; ?></p>
						</div>
					</div>
				</div>
			<?php }else{ ?>
				<div class="col-md-12">
					<div class="panel panel-default panel-comment pull-left">
						<div class="panel-heading" >
							<b style="font-size: 13px; "><?php echo $nama_r; ?> :</b><small class="pull-left" style="color:grey;margin-bottom:0px;font-size: 13px;"><?php echo date("d-m H:i:s", strtotime($row['created_at'])); ?></small>
							<p style="font-size: 13px; "><?php echo $row['pesan']; ?></p>
						</div>
					</div>
				</div>
				<!-- <div class="col-md-12">
					<div class="panel panel-default panel-comment" style="background-color: #0D47A1  !important;">
						<div class="panel-heading" >
							<?php ?>
							<b> <?php echo $row['id_penjual'];?> : </b><small class="pull-right" style="color:grey;margin-top:0px;"><?php echo date("Y-M-d H:i:s", strtotime($row['created_at'])); ?></small></br>
							<?php echo $row['pesan']; ?>
						</div>
					</div>
				</div> -->
			<?php } ?>
		<?php } ?>
<?php } else { echo "<h4>Chatt with $nama_r ; </h4>"; } ?>
</div>