<div class="main-content">
	<div class="full-width">
		<div class="double-block">
			<div class="full-width">
				<div class="block">
					<div class="block-title" style="background: #cf6314;">
						<!--<a href="<?php //echo base_url(); ?>" class="right">Back to homepage</a>-->
						<h2><?php echo $title; ?></h2>
					</div>
					<div class="block-content">
						<?php
						foreach ($berita->result_array() as $r) {
							$isi_berita =(strip_tags($r['deskripsi'])); 
							$isi = substr($isi_berita,0,220); 
							$isi = substr($isi_berita,0,strrpos($isi," ")); 
							$judul = substr($r['nama_layanan'],0,50); 
						//	$total_komentar = $this->model_utama->view_where('komentar',array('id_berita' => $r['id_berita']))->num_rows();
							?>
							<div class='article-big'>
								<div style='height:120px;width: 200px;background:#e3e3e3;overflow:hidden;' class='article-photo'>
									<a href='<?php echo $r[link]; ?>' target='_BLANK' class='hover-effect'>
										<?php
										if ($r['logo_layanan'] == ''){
											echo "<img style='height:150px;width:200px;' src='".base_url()."asset/foto_berita/no-image.jpg' alt='no-image.jpg' />";
										}else{
											echo "<img style='height:150px;width:200px;' src='".base_url()."asset/layanan/$r[logo_layanan]' alt='$r[logo_layanan]' />";
										}
										?>
									</a>
								</div>
								<div class='article-content'>
									<h2><a title='<?php echo $r[nama_layanan]; ?>' href='<?php echo $r[link]; ?>' target='_BLANK' ><?php echo $judul; ?>...</a></h2>
									<!--<a href='".base_url()."$r[judul_seo]' class='h-comment'>$total_komentar</a>-->
									<!-- <span class='meta'>
										<a href='<?php //echo base_url().$r[judul_seo]; ?>'><span class='icon-text'>&#128100;</span><?php //echo $r[nama_lengkap]; ?></a>
										<a href='<?php //echo base_url().$r[judul_seo]; ?>'><span class='icon-text'>&#128340;</span><?php //echo $r[jam].", ".tgl_indo($r['tanggal']); ?></a>
									</span> -->
									<p><?php echo getSearchTermToBold($isi, $this->input->post('kata')).'...'; ?></p>
									<span class='meta'>
										<a href='<?php echo $r[link]; ?>' target='_BLANK' class='more'>Login / Registrasi <span class='icon-text'>&#9656;</span></a>
									</span>
								</div>
							</div>
							<?php
						}
						?>
						<div class="pagination">
							<?php echo $this->pagination->create_links(); ?>
						</div>
					</div>
				</div>
			</div>
			<!--<div class="content-block right">
				<?php //include "sidebar_kiri.php"; ?>
			</div>-->
		</div>
	</div>
	<!-- <div class="main-sidebar right">
		<?php //include "sidebar_kanan.php"; ?>
	</div> -->
	<div class="clear-float"></div>
</div>