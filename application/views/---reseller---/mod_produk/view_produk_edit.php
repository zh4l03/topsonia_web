<script language="JavaScript" type="text/JavaScript">
function showSub(){
  <?php
  $query = $this->db->query("SELECT * FROM rb_kategori_produk");
  foreach ($query->result_array() as $data) {
    $id_kategori_produk = $data['id_kategori_produk'];
    echo "if (document.demo.a.value == \"".$id_kategori_produk."\")";
    echo "{";
      $query_sub_kategori = $this->db->query("SELECT * FROM rb_kategori_produk_sub where id_kategori_produk='$id_kategori_produk'");
      $content = "document.getElementById('sub_kategori_produk').innerHTML = \"  <option value=''>- Pilih Sub Kategori Produk -</option>";
      foreach ($query_sub_kategori->result_array() as $data2) {
        $content .= "<option value='".$data2['id_kategori_produk_sub']."'>".$data2['nama_kategori_sub']."</option>";
      }
      $content .= "\"";
      echo $content;
      echo "}\n";
    }
    ?>
  }
</script>

<?php
echo "<div class='col-md-12'>
<div class='box box-info'>
<div class='box-header with-border'>
<h3 class='box-title'>Edit Produk Terpilih</h3>
</div>
<div class='box-body'>";
$attributes = array('class'=>'form-horizontal','role'=>'form','name'=>'demo',"onsubmit"=>"return cek_berat();");
echo form_open_multipart('reseller/edit_produk',$attributes);
$disk = $this->model_app->edit('rb_produk_diskon',array('id_produk'=>$rows['id_produk'],'id_reseller'=>$this->session->id_reseller))->row_array();
$jual = $this->model_reseller->jual_reseller($this->session->id_reseller,$rows['id_produk'])->row_array();
$beli = $this->model_reseller->beli_reseller($this->session->id_reseller,$rows['id_produk'])->row_array();
echo "<div class='col-md-12'>
<table class='table table-condensed table-bordered'>
<tbody>
<input type='hidden' name='id' id='id_produk' value='$rows[id_produk]'>
<tr><th scope='row'>Kategori</th>                   <td><select name='a' class='form-control' onchange=\"showSub()\" required>
<option value='' selected>- Pilih Kategori Produk -</option>";
foreach ($record as $row){
  if ($rows['id_kategori_produk']==$row['id_kategori_produk']){
    echo "<option value='$row[id_kategori_produk]' selected>$row[nama_kategori]</option>";
  }else{
    echo "<option value='$row[id_kategori_produk]'>$row[nama_kategori]</option>";
  }
}
echo "</select></td></tr>
<tr><th scope='row'>Sub Kategori</th>                   <td><select name='aa' class='form-control' id='sub_kategori_produk'>
<option value='' selected>- Pilih Sub Kategori Produk -</option>";
$sub_kategori_produk = $this->db->query("SELECT * FROM rb_kategori_produk_sub");
foreach ($sub_kategori_produk->result_array() as $row){
  if ($rows['id_kategori_produk_sub']==$row['id_kategori_produk_sub']){
    echo "<option value='$row[id_kategori_produk_sub]' selected>$row[nama_kategori_sub]</option>";
  }else{
    echo "<option value='$row[id_kategori_produk_sub]'>$row[nama_kategori_sub]</option>";
  }
}
echo "</select></td></tr>
<tr><th width='130px' scope='row'>Nama Produk</th>  <td><input type='text' class='form-control' name='b' value='$rows[nama_produk]' required></td></tr>
<tr><th scope='row'>Satuan</th>                     						
	<td><select name='c' class='form-control' style='width:100%;' required>
	<option value='' selected>- Pilih Satuan Produk -</option>";								
	$satuan =  $this->db->query("SELECT * FROM rb_produk_satuan");								
	foreach ($satuan->result_array() as $row) {		
		 if ($rows['satuan']==$row['nama_produk_satuan']){
			echo "<option value='$row[nama_produk_satuan]' selected>$row[nama_produk_satuan]</option>";	
		}else{
			echo "<option value='$row[nama_produk_satuan]'>$row[nama_produk_satuan]</option>";
		}		
	}echo"</select>	
	</td>					
</tr>
<tr><th scope='row'>Berat</th>                 <td><input type='number' class='form-control' name='berat' id='berat' value='$rows[berat]'></td></tr>
<tr><th scope='row'>Harga Modal</th>                 <td><input type='number' class='form-control' name='d' value='$rows[harga_beli]'></td></tr>
<input type='hidden' class='form-control' name='e' value='$rows[harga_reseller]'>
<tr><th scope='row'>Harga Jual</th>             <td><input type='number' class='form-control' name='f' value='$rows[harga_konsumen]'></td></tr>
<tr><th scope='row'>Diskon</th>                 <td><input type='number' class='form-control' name='diskon' value='$disk[diskon]'></td></tr>
<tr><th scope='row'>Stok</th>                 <td><input style='display:inline-block; width:80px; color:red' type='number' class='form-control' value='".($beli['beli']-$jual['jual'])."' disabled>
+ <input style='display:inline-block; width:80px' type='number' class='form-control' name='stok'> </td></tr>

<tr><th scope='row'>Keterangan</th>                 <td><textarea class='textarea form-control' name='ff' style='height:180px'>$rows[keterangan]</textarea></td></tr>
<tr><th scope='row'>Foto Produk</th>                     <td>";

    
    $gambar = $rows['gambar'];
    $arr_gambar = explode (";",$gambar);
    echo "<table>";

    echo"<tr><td>";
    if($arr_gambar[0]==null){echo "<img id='tampil0' style='border:1px solid #cecece; width:100px' src='".base_url()."asset/foto_produk/thumb/no-image-icon.gif'>";}
    else{echo "<img id='tampil0' style='border:1px solid #cecece; width:100px' src='".base_url()."asset/foto_produk/$arr_gambar[0]'>";}
    echo "</td>";
    echo "<td>&nbsp;&nbsp;&nbsp;</td>";
    echo "<td><a class='btn btn-danger btn-xs' title='Delete' style='float: right;'><span class='glyphicon glyphicon-remove' id='gambar0'></span></a> 
    <input type='file' value='$arr_gambar[0]' class='form-control'  id='userfile[0]' name='userfile[0]' onchange='document.getElementById(`tampil0`).src = window.URL.createObjectURL(this.files[0])'> Allowed File : .gif, jpg, png
    <input type='hidden' value='$arr_gambar[0]' name='file_lama[0]' id='file_lama[0]'></td>";
    
    echo "<td>&nbsp;&nbsp;&nbsp;</td>";

    echo"<td>";
    if($arr_gambar[1]==null){echo "<img id='tampil1' style='border:1px solid #cecece; width:100px' src='".base_url()."asset/foto_produk/thumb/no-image-icon.gif'>";}
    else{echo "<img id='tampil1' style='border:1px solid #cecece; width:100px' src='".base_url()."asset/foto_produk/$arr_gambar[1]'>";}
    echo "</td>";
    echo "<td>&nbsp;&nbsp;&nbsp;</td>";
    echo "<td><a class='btn btn-danger btn-xs' title='Delete' style='float: right;'><span class='glyphicon glyphicon-remove' id='gambar1'></span></a> 
    <input type='file' class='form-control'  id='userfile[1]' name='userfile[1]' onchange='document.getElementById(`tampil1`).src = window.URL.createObjectURL(this.files[0])'> Allowed File : .gif, jpg, png
    <input type='hidden' value='$arr_gambar[1]' name='file_lama[1]' id='file_lama[1]'></td></tr>";
    echo "<tr><td>&nbsp;</td></tr>";

    echo"<tr><td>";
    if($arr_gambar[2]==null){echo "<img id='tampil2' style='border:1px solid #cecece; width:100px' src='".base_url()."asset/foto_produk/thumb/no-image-icon.gif'>";}
    else{echo "<img id='tampil2' style='border:1px solid #cecece; width:100px' src='".base_url()."asset/foto_produk/$arr_gambar[2]'>";}
    echo "</td>";
    echo "<td>&nbsp;&nbsp;&nbsp;</td>";
    echo "<td><a class='btn btn-danger btn-xs' title='Delete' style='float: right;'><span class='glyphicon glyphicon-remove' id='gambar2'></span></a> 
    <input type='file' class='form-control'  id='userfile[2]' name='userfile[2]' onchange='document.getElementById(`tampil2`).src = window.URL.createObjectURL(this.files[0])' > Allowed File : .gif, jpg, png
    <input type='hidden' value='$arr_gambar[2]' name='file_lama[2]' id='file_lama[2]'></td>";
    
    echo "<td>&nbsp;&nbsp;&nbsp;</td>";

    echo"<td>";
    if($arr_gambar[3]==null){echo "<img id='tampil3' style='border:1px solid #cecece; width:100px' src='".base_url()."asset/foto_produk/thumb/no-image-icon.gif'>";}
    else{echo "<img id='tampil3' style='border:1px solid #cecece; width:100px' src='".base_url()."asset/foto_produk/$arr_gambar[3]'>";}
    echo "</td>";
    echo "<td>&nbsp;&nbsp;&nbsp;</td>";
    echo "<td><a class='btn btn-danger btn-xs' title='Delete' style='float: right;'><span class='glyphicon glyphicon-remove' id='gambar3'></span></a> 
    <input type='file' class='form-control'  id='userfile[3]' name='userfile[3]' onchange='document.getElementById(`tampil3`).src = window.URL.createObjectURL(this.files[0])'> Allowed File : .gif, jpg, png
    <input type='hidden' value='$arr_gambar[3]' name='file_lama[3]' id='file_lama[3]'></td></tr>";
    echo "<tr><td>&nbsp;</td></tr>";
    
    echo "</table>";
    
    // for($i=0; $i<4; $i++){
    //   echo "<table><tr><td>";
    //   if($arr_gambar[$i]==null){echo "<br/><img id='tampil$i' style='border:1px solid #cecece; width:50px' src='".base_url()."asset/foto_produk/thumb/no-image-icon.gif'>";}
    //   else{echo "<br/><img id='tampil$i' style='border:1px solid #cecece; width:50px' src='".base_url()."asset/foto_produk/$arr_gambar[$i]'>";}
    //   echo "</td>";
    //   echo "<td><a class='btn btn-danger btn-xs' title='Delete' style='float: right;'><span class='glyphicon glyphicon-remove' id='gambar$i'></span></a> 
    //   <input type='file' class='form-control'  id='userfile[$i]' name='userfile[$i]' onchange='document.getElementById(`tampil$i`).src = window.URL.createObjectURL(this.files[0])' > Allowed File : .gif, jpg, png
    //   <input type='hidden' value='$arr_gambar[$i]' name='file_lama[$i]' id='file_lama[$i]'></td></tr></table>";
    // }
?>

<b>Multiple Upload, Allowed File :</b> .gif, jpg, png || <b>Max Size :</b> 5MB
<div id='dvPreview'></div>
<?php
// echo "<div id='hilang1'><input type='file' id='fileupload' class='form-control' name='userfile[1]' multiple> Allowed File : .gif, jpg, png
// <input type='text' value='$arr_gambar[1]' name='file_lama[1]' id='file_lama[1]'>
// <a class='btn btn-danger btn-xs' title='Delete'><span class='glyphicon glyphicon-remove' id='gambar1'></span></a>";
// if($arr_gambar['1']==null){echo "<br/><b>Gambar Kosong</b>";}
// echo"</div>";

echo "<div id='dvPreview'></div>";
// if ($rows['gambar'] != ''){ echo "<i style='color:red'>Gambar Saat ini : </i><a target='_BLANK' href='".base_url()."asset/foto_produk/$rows[gambar]'>$rows[gambar]</a>"; }
echo "</td></tr>
</tbody>
</table>
</div>
</div>
<div class='box-footer'>
<button type='submit' name='submit' class='btn btn-info'>Update</button>
<a href='".base_url()."reseller/produk'><button type='button' id='cek' class='btn btn-default pull-right'>Cancel</button></a>
</div>
</div>";

?>

<script>

$(document).ready(function(){
  for(let i=0; i<4; i++){
  $('#gambar'+i).click(function(){
    document.getElementById("tampil"+i).src = "<?php echo base_url().'asset/foto_produk/thumb/no-image-icon.gif'; ?>"; 
    document.getElementById('file_lama['+i+']').value =null;
    document.getElementById('userfile['+i+']').value =null;
  });

var uploadField = document.getElementById('userfile['+i+']');
uploadField.onchange = function() {
    if(this.files[0].size > 5000000){ // ini untuk ukuran 800KB, 1000000 untuk 1 MB.
       alert("Maaf. File Terlalu Besar ! Maksimal Upload 5 mb");
       this.value = "";
    };
};

}
});

// $(document).ready(function(){
//   for(let i=0; i<4; i++){
//   $('#gambar'+i).click(function(){
//     $.ajax({
// 		type: 'POST',
// 		url: '<?php echo base_url(); ?>reseller/hapus_gambar',
// 		dataType: 'JSON',
// 		data: {id_produk: document.getElementById('id_produk').value, gambar: document.getElementById('file_lama['+i+']').value},
// 		success: function(response) {
// 		}
// 		});
//     document.getElementById("tampil"+i).src = "<?php echo base_url().'asset/foto_produk/no-image.jpg'; ?>"; 
//     document.getElementById('file_lama['+i+']').value =null;
//   });
//   }

// });

function cek_berat(){
  let berat = document.getElementById("berat").value;
  let merchant_id = "<?php echo $this->session->id_reseller; ?>";
  if(berat>0){
    
  }else{
    if(merchant_id==='21' || merchant_id==='24'){

    }else{
      alert("Berat Tidak Boleh Kosong");
      return false;
    }
  }
  
}

</script>
