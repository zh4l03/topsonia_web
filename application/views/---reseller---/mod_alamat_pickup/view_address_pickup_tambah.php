<?php 
    echo "<div class='col-md-12'>
              <div class='box box-info'>
                <div class='box-header with-border'>
                  <h3 class='box-title'>Tambah alamat Pickup</h3>
                </div>
              <div class='box-body'>";
              $attributes = array('class'=>'form-horizontal','role'=>'form');
              echo form_open_multipart($this->uri->segment(1).'/tambah_adress_pickup',$attributes); 
          echo "<div class='col-md-10'>
                  <table class='table table-condensed table-bordered'>
                  <tbody>
                    <input type='hidden' name='id' value=''>
					<tr><th scope='row'>PIC</th>                 		  	<td><input type='text' class='form-control' name='a'></td></tr>
                    <tr><th scope='row'>Telpon</th>                 	 	<td><input type='number' class='form-control' name='b'></td></tr>
					<tr><th width='200px' scope='row'>Alamat Lengkap</th>   <td><textarea class='form-control' id='alamat_merchant' name='c' rows='5' maxlength='350' ></textarea></td></tr>
                    <tr><th scope='row'>Provinsi</th>                 	  	<td><select class='form-control' name='e' id='state_reseller' style='width:40%' required>                                                                            
																				<option value=''>- Pilih -</option>";                                                                            
																				$provinsi = $this->model_app->view_ordering('rb_provinsi','provinsi_id','ASC');                                                                           
																				foreach ($provinsi as $row) {                                                                              
																					if ($ko['provinsi_id']==$row['provinsi_id']){                                                                                
																					echo "<option value='$row[provinsi_id]' selected>$row[nama_provinsi]</option>";                                                                              
																					}else{                                                                               
																					echo "<option value='$row[provinsi_id]'>$row[nama_provinsi]</option>"; 
																						}                                                                           
																				}                                                                          
																				echo "</select></td></tr>
                    <tr><th scope='row'>Kota</th>                 		 	<td><select class='form-control' name='f' id='city_reseller' style='width:40%' required>                                                                                
																				<option value=''>- Pilih -</option>";                                                                            
																					$kota = $this->model_app->view_where_ordering('rb_kota',array('provinsi_id'=>$ko['provinsi_id']),'kota_id','DESC');                                                                          
																					foreach ($kota as $row) {                                                                             
																						if ($ko['kota_id']==$row['kota_id']){                                                                                
																							echo "<option value='$row[kota_id]' selected>$row[nama_kota]</option>";                                                                            
																						}else{                                                                               
																							echo "<option value='$row[kota_id]'>$row[nama_kota]</option>";                                                                             
																						}                                                                           
																					}                                                                             
																				echo "</select></td></tr>
					<tr><th scope='row'>Kecamatan</th>               	<td><input type='text' class='form-control' name='d' style='width:40%'></td></tr>
                    <tr><th scope='row'>Kode Pos</th>                 	  	<td><input type='number' class='form-control' name='g' style='width:40%'></td></tr>
                  </tbody>
                  </table>
                </div>
              </div>
              <div class='box-footer'>
                    <button type='submit' name='submit' class='btn btn-info'>Tambahkan</button>
                    <a href='".base_url().$this->uri->segment(1)."/adress_pickup'><button type='button' class='btn btn-default pull-right'>Cancel</button></a>
                </div>
            </div>";
