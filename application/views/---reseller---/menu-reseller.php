        <section class="sidebar">
          <!-- Sidebar user panel -->
          <?php
          $log = $this->model_app->edit('rb_reseller',array('id_reseller'=>$this->session->id_reseller))->row_array(); 
          if ($log['foto']==''){ $foto = 'blank.png'; }else{ $foto = $log['foto']; }
            echo "<div class='user-panel'>
              <div class='pull-left image'>
                <img src='".base_url()."asset/foto_user/$foto' class='img-circle' alt='User Image'>
              </div>
              <div class='pull-left info'>
                <p>$log[nama_reseller]</p>
                <a href=''><i class='fa fa-circle text-success'></i> Online</a>
              </div>
            </div>";
          ?>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header" style='text-transform:uppercase; border-bottom:2px solid #00c0ef'>MENU MERCHANT</li>
            <li><a href="<?php echo base_url().$this->uri->segment(1); ?>/home"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
            <li class="treeview">
                <a href="#"><i class="fa fa-th-large"></i> <span>Referensi</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <?php 
                        echo "<li><a href='".base_url().$this->uri->segment(1)."/rekening'><i class='fa fa-circle-o'></i> No Rekening Anda</a></li>";
                        echo "<li><a href='".base_url().$this->uri->segment(1)."/keterangan'><i class='fa fa-circle-o'></i> Info/Keterangan</a></li>";
                        echo "<li><a href='".base_url().$this->uri->segment(1)."/produk'><i class='fa fa-circle-o'></i> Data Produk Anda</a></li>";
                        // echo "<li><a href='".base_url().$this->uri->segment(1)."/alamat_cod'><i class='fa fa-circle-o'></i> Alamat COD</a></li>";
                    ?>
                </ul>
            </li>
            
             <?php $pesanan = $this->db->query("select count(DISTINCT(a.kode_transaksi)) as jml from rb_penjualan a join rb_konfirmasi_pembayaran_konsumen b on a.kode_transaksi=b.kode_transaksi  where a.id_penjual='".$this->session->id_reseller."' and a.flag_reseller='0' and status_pembayaran='lunas'");
                $jml_pesanan = $pesanan->row_array()['jml'];
            ?>

            <li class="treeview">
                <a href="#"><i class="fa fa-shopping-cart"></i> <span>Transaksi</span> <i class="fa fa-angle-left pull-right"></i> <?php if($jml_pesanan>0){ ?> <span class='badge bg-orange'><?php echo $jml_pesanan;  ?></span> <?php }?></a>
                <ul class="treeview-menu">
                    <?php 
                        // echo "<li><a href='".base_url().$this->uri->segment(1)."/pembelian'><i class='fa fa-circle-o'></i> Pembelian Ke Pusat</a></li>";
                        echo "<li><a href='".base_url().$this->uri->segment(1)."/penjualan'><i class='fa fa-circle-o'></i> Penjualan Ke Agen";
                         if($jml_pesanan>0) { echo "<span class='badge bg-orange'>$jml_pesanan</span>"; }
                        echo "</a></li>";
                        echo "<li><a href='".base_url().$this->uri->segment(1)."/pembayaran_konsumen'><i class='fa fa-circle-o'></i> Pembayaran Agen</a></li>";
                        // echo "<li><a href='".base_url().$this->uri->segment(1)."/request_pickup'><i class='fa fa-circle-o'></i> Request Pickup</a></li>";
                    ?>
                </ul>
            </li>
            
            <li class="treeview">
                <a href="#"><i class="fa fa-book"></i> <span>Laporan</span> <i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <?php 
                        echo "<li><a href='".base_url().$this->uri->segment(1)."/keuangan'><i class='fa fa-circle-o'></i> Data Keuangan</a></li>";
                    ?>
                </ul>
            </li>

			<li><a href="<?php echo base_url(); ?>reseller/adress_pickup/<?php echo $this->session->id_reseller; ?>"><i class="fa fa-truck"></i> <span>Alamat Pickup</span></a></li>


            <?php $isi_pesan = $this->db->query("select count(id_penjual) as jml from chat where id_penjual='".$this->session->id_reseller."' and flag=0 and ket=1 group by id_penjual order by id_chat")->num_rows();?>
            <?php $isi_pesan_admin = $this->db->query("select count(id_penjual) as jml from chat_admin where id_penjual='".$this->session->id_reseller."' and flag=0 and ket=1 group by id_penjual order by id_chat")->num_rows();?>
            <li><a href="<?php echo base_url(); ?>reseller/edit_reseller/<?php echo $this->session->id_reseller; ?>"><i class="fa fa-user"></i> <span>Edit Profile</span></a></li>

            <li class="treeview">
                <a href="#"><i class="fa fa-envelope"></i> <span>Live Chat </span><span class='badge badgee'><?php echo $isi_pesan+$isi_pesan_admin; ?></span></i></a>
                <ul class="treeview-menu">
                    <?php 
                        echo "<li><a href='".base_url().$this->uri->segment(1)."/pesan/".$this->session->id_reseller."'><i class='fa fa-circle-o'></i><span>Kosumen</span><span class='badge badgee'>".$isi_pesan."</span></a></li>";
                        echo "<li><a href='".base_url().$this->uri->segment(1)."/pesan_admin'><i class='fa fa-circle-o'></i>Admin<span class='badge badgee'>".$isi_pesan_admin."</span></a></li>";
                    ?>
                </ul>
            </li>
            
            <li><a href="<?php echo base_url(); ?>reseller/logout"><i class="fa fa-power-off"></i> <span>Logout</span></a></li>
          </ul>
        </section>