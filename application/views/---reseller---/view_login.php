<?php
if ($this->session->level == 'konsumen' || $this->session->level == 'admin') {
    $disabled = "disabled";
}

if($this->session->userdata('id_reseller') != ""){
  redirect(base_url("reseller/home"));
}else{
?>

  <!DOCTYPE html>
  <html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title; ?></title>
    <meta name="author" content="phpmu.com">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>/asset/admin/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>/asset/admin/dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>/asset/admin/plugins/iCheck/square/blue.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>asset/admin/dist/css/custom-ncs.css">
    <style type="text/css">
    ::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
      color: white !important;
      opacity: 0.6; /* Firefox */
    }

    :-ms-input-placeholder { /* Internet Explorer 10-11 */
      color: white !important;
    }

    ::-ms-input-placeholder { /* Microsoft Edge */
      color: white !important;
    }
    </style>
  </head>
  <body class="hold-transition login-merchant-page">
    <div class="login-box">
      <div class="login-logo">
        <a href="#"><b>MERCHANT </b> Login</a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg">Silahkan Login Pada Form dibawah ini</p>

        <!--<form action="" method="post">-->
        <?php
        echo $this->session->flashdata('message');
        echo form_open($this->uri->segment(1).'/index');
        ?>
        <div class="form-group has-feedback">
          <input type="text" class="form-control form-login" name='a' placeholder="Username" value="" required <?php echo $disabled; ?>>
          <span class="glyphicon glyphicon-user form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
          <input type="password" class="form-control form-login" name='b' placeholder="Password" value="" required <?php echo $disabled; ?>>
          <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>

        <div class="form-group has-feedback">
          <?php echo $image; ?>
        </div>
        <div class="form-group has-feedback">
          <input type="text" class="form-control form-login" name='security_code' placeholder="Security Code" required <?php echo $disabled; ?>>
        </div>
        
        <div class="row">
          <div class="col-xs-8">
            <div class="checkbox icheck">
              <label>
                <input type="checkbox"> Remember Me
              </label>
            </div>
          </div><!-- /.col -->
          <div class="col-xs-4">
            <button name='submit' type="submit" class="btn btn-primary btn-block btn-flat btn-login" <?php echo $disabled; ?>>Sign In</button>
          </div><!-- /.col -->
        </div>
      </form>

    </div><!-- /.login-box-body -->
  </div><!-- /.login-box -->

  <!-- jQuery 2.1.4 -->
  <script src="<?php echo base_url(); ?>/asset/admin/plugins/jQuery/jQuery-2.1.4.min.js"></script>
  <!-- Bootstrap 3.3.5 -->
  <script src="<?php echo base_url(); ?>/asset/admin/bootstrap/js/bootstrap.min.js"></script>
  <!-- iCheck -->
  <script src="<?php echo base_url(); ?>/asset/admin/plugins/iCheck/icheck.min.js"></script>
  <script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue check_login',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
  </script>
</body>
</html>

<?php
}
?>
