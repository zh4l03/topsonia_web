<?php $detail = $this->db->query("SELECT * FROM rb_penjualan where kode_transaksi='".$this->uri->segment(3)."' AND id_penjual='".$this->session->id_reseller."'")->row_array(); ?>

            <div class="col-xs-12">  
              <div class="box">
                <div class="box-header">
					<?php 
						echo $this->session->flashdata('message');
					?>
                  <h3 class="box-title">Data Detail Transaksi Penjualan</h3>
                  <span style="margin-left:10px"><a class='pull-right btn btn-default btn-sm' href='<?php echo base_url().$this->uri->segment(1); ?>/penjualan'>Kembali</a></span>
				  			  
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table class='table table-condensed table-bordered'>
                  <tbody>
                    <tr><th width='140px' scope='row'>Kode Pembelian</th>  <td><?php echo "$rows[kt]"; ?></td></tr>
                    <tr><th scope='row'>Nama Pembeli</th>                 <td><?php echo "<a href='".base_url().$this->uri->segment(1)."/detail_konsumen/$rows[id_konsumen]'>$rows[nama_lengkap]</a>"; ?></td></tr>
                    <tr><th scope='row'>Waktu Transaksi</th>               <td><?php echo "$rows[waktu_order]"; ?></td></tr>
                    <tr><th scope='row'>Pembayaran</th>               <td><?php echo "<span style='text-transform:uppercase'>$detail[pembayaran]</span>"; ?></td></tr> 
                    <tr><th scope='row'>Kurir</th>               <td><?php echo "<span style='text-transform:uppercase'>$detail[kurir]</span> - $detail[service]"; ?></td></tr> 
					<tr><th scope='row'>Airwaybill (AWB)</th>               <td><?php echo "$rows[awb]"; ?></td></tr>
                    <tr><th scope='row'>Status</th>                        <td>
						<?php
                        if ($rows['proses']=='0' && $rows['bayar']=='0'){ 
							$proses = '<i class="text-danger">Pending (Menunggu Pembayaran)</i>'; 
							$button = 'btn-default invisible'; 
							$status = 'Proses'; 
							$icon = 'hourglass'; 
							$href = "disabled"; 
						}elseif($rows['proses']=='0' && ($rows['bayar']=='1' or $rows['bayar']=='2')){ 
							if (($rows['bayar']=='1' or $rows['bayar']=='2') && $pemb['status_pembayaran']=='lunas'){ 
								$proses = '<i class="text-warning">Pembayaran Dikonfirmasi (Lunas)</i>';
								$button = 'btn-primary'; 
								$status = 'Proses'; 
								$icon = 'bitcoin'; 
								$href = "href='".base_url().$this->uri->segment(1)."/proses_penjualan_detail/$detail[id_penjualan]/1/0/$detail[kode_transaksi]' onclick=\"return confirm('Apa anda yakin untuk ubah status jadi $status?')\""; 
							}elseif ($pemb['status_pembayaran']=='' || $pemb['status_pembayaran']=='diperiksa'){
								// $proses = '<i class="text-warning">Bayar (Menunggu Konfirmasi)</i> &nbsp; <a data-toggle="collapse" href="#collapse1">Detail Pembayaran <i class="fa fa-angle-down" aria-hidden="true"></i></a>';
								$proses = '<i class="text-warning">Bayar (Menunggu Konfirmasi Topsonia)</i></a>';
								$button = 'btn-primary'; 
								$status = 'Proses'; 
								$icon = 'bitcoin'; 
								$href = "disabled"; 
							}elseif ($pemb['status_pembayaran']=='gagal'){
								$proses = '<i class="text-warning">Pembayaran Gagal (Menunggu Pengajuan Refund)</i></a>';
								$button = 'btn-primary'; 
								$status = 'Proses'; 
								$icon = 'bitcoin'; 
								$href = "disabled"; 
							}elseif ($pemb['status_pembayaran']=='proses refund'){
								$proses = '<i class="text-warning">Transaksi dalam proses refund</i></a>';
								$button = 'btn-primary'; 
								$status = 'Proses'; 
								$icon = 'bitcoin'; 
								$href = "disabled"; 
							}elseif ($pemb['status_pembayaran']=='refund'){
								$proses = '<i class="text-warning">Transaksi - refund</i></a>';
								$button = 'btn-primary'; 
								$status = 'Proses'; 
								$icon = 'bitcoin'; 
								$href = "disabled"; 
							}else{
								$proses = '<i class="text-warning">Bayar (Menunggu Konfirmasi)</i></a>';
								$button = 'btn-primary'; 
								$status = 'Proses'; 
								$icon = 'bitcoin'; 
								$href = "disabled"; 
							}
						}elseif($rows['proses']=='1' && $rows['selesai']=='0'){ 
							$proses = '<i class="text-success">Proses (Pengemasan - Pengiriman)</i>'; 
							
							// $day = ($rows['tanggal_proses']= date('w'));
							$day = date('w');
							$week_start = $rows['tanggal_proses']= date('m-d-Y', strtotime('-'.$day));
							$week_end = date('m-d-Y', strtotime('+'.(6-$day)));
							if ($week_start > $week_end ){ 
								$proses = '<i class="text-success">Proses (Pengemasan - Pengiriman)</i>'; 
								$button = 'btn-success'; 
								$status = ''; 
								$icon = 'star'; 
								$href = "disabled"; 
							}else{
									
								$button = 'btn-success'; 
								$status = 'Selesai'; 
								$icon = 'star'; 
								$ubah = 0; 
								$href = "href='".base_url().$this->uri->segment(1)."/proses_penjualan_detail/$detail[id_penjualan]/1/1/$detail[kode_transaksi]' onclick=\"return confirm('Apa anda yakin untuk ubah status jadi $status?')\""; 	
							} 
						}else{ 		
							$proses = '<i class="text-success">Selesai</i>'; 
							$button = 'btn-success invisible'; 
							$status = ''; 
							$icon = 'star'; 
							$href = "disabled"; 
						} 
						
                        echo "$proses <br/>"; 
						
						if ($rows['bayar']=='2'){ 
						?>
							<div id="collapse1" class="panel-collapse collapse">
								<table class="table table-bordered table-striped">
									<thead>
									  <tr>
										<th>Rek Pengirim</th>
										<th>Rek Tujuan</th>
										<th>Nominal</th>
										<th>Tgl Bayar</th>
										<th>Bukti Transfer</th>
									  </tr>
									</thead>
									<tbody>
									<?php 
										$no = 1;
										foreach ($rec_bayar as $row2){
										echo "<tr>
												  <td>$row2[rekening_pengirim] - $row2[nama_pengirim]</td>
												  <td>$row2[nama_penerima]</td>
												  <td style='text-align:right'>".$row2['nominal']."</td>
												  <td style='text-align:right'>$row2[tanggal_bayar]</td>
												  <td><a href='".base_url()."asset/files/".$this->uri->segment(3).".jpg' target='_blank'><img style='border:1px solid #cecece; width:50px' src='".base_url()."asset/files/".$this->uri->segment(3).".jpg'></td>
											  </tr>";
										  $no++;
										}
									?>
									</tbody>
								</table>
							</div>
						<?php
						}
						
						echo "<br/><a class='btn $button btn-xs' style='margin-left:5px' title='$status Data' $href ><span class='glyphicon glyphicon-$icon'></span> Ubah Status</a>"; 
                      ?>
                    </td></tr>

                    <tr><th scope='row'>Catatan</th><td><?php echo "$rows[catatan_pelapak]"; ?></td></tr>
                    
                    <tr><th scope='row'></th>                        
					<td>
						<?php
							echo "<a class='btn btn-default btn-sm' style='margin-right:5px' title='$status Data'"; 							
							if ($rows['awb']=='' and $rows['proses']=='1'){ 		
								echo "href='".base_url().$this->uri->segment(1)."/request_pickup/$detail[kode_transaksi]/$detail[id_penjualan]'"; 
							}else{
								$aktifpickup = 'disabled';
							} 												
							echo " $aktifpickup ><i class='fa fa-truck'></i> Request Pickup</a>"; 
							
							
							echo "<span class='dropdown'> <button class='btn btn-default btn-sm dropdown-toggle' type='button' style='margin-right:5px' title='$status Data' ";
							if ($rows['awb']!=''){  
								$aktifprint = '';
							}else{
								$aktifprint = 'disabled';
							} 				
							echo "$aktifprint data-toggle='dropdown' ><span class='glyphicon glyphicon-print'></span> Print Label</button>"; 
						?>
								<ul class="dropdown-menu">
								  <li><a href="<?php echo base_url().$this->uri->segment(1).'/print_label/'.$detail[id_penjualan].'/3' ?>" target="_blank">A4/3</a></li>
								  <li><a href="<?php echo base_url().$this->uri->segment(1).'/print_label/'.$detail[id_penjualan].'/6' ?>" target="_blank">A4/6</a></li>
								  <li><a href="<?php echo base_url().$this->uri->segment(1).'/print_label/'.$detail[id_penjualan].'/12' ?>" target="_blank">A4/12</a></li>
								</ul>
							</span>
							<a href="<?php echo base_url().$this->uri->segment(1).'/print_invoice/'.$detail[id_penjualan].'/' ?>" class='btn btn-default btn-sm' style='margin-right:5px' target="_blank"><span class='glyphicon glyphicon-print'></span> Invoice</button>
                    </td>
					</tr>
                  </tbody>
                  </table>
                  <hr>
                  <table class="table table-bordered table-striped table-condensed">
                    <thead>
                      <tr>
                        <th style='width:40px'>No</th>
                        <th>Nama Produk</th>
                        <th style="text-align:center">Harga Jual</th>
                        <th style="text-align:center">Jumlah Jual</th>
                        <th>Satuan</th>
                        <th style="text-align:center">Sub Total</th>
                      </tr>
                    </thead>
                    <tbody>
                  <?php 
                    $no = 1;
                    foreach ($record as $row){
                    $sub_total = ($row['harga_jual']*$row['jumlah'])-$row['diskon'];
                    echo "<tr><td>$no</td>
                              <td>$row[nama_produk]</td>
                              <td style='text-align:right'>Rp ".rupiah($row['harga_jual'])."</td>
                              <td style='text-align:right'>$row[jumlah]</td>
                              <td>$row[satuan]</td>
                              <td style='text-align:right'>Rp ".rupiah($sub_total)."</td>
                          </tr>";
                      $no++;
                    }
                    $total = $this->db->query("SELECT sum((a.harga_jual*a.jumlah)-a.diskon) as total FROM `rb_penjualan_detail` a JOIN rb_penjualan b ON a.id_penjualan=b.id_penjualan where b.kode_transaksi='".$this->uri->segment(3)."' AND b.id_penjual='".$detail['id_penjual']."'")->row_array();
					$tot = ($total['total']+$detail['ongkir']+$rows['asuransi'])-$rows['diskon_ongkir']-$rows['diskon'];
                    echo "<tr>
                            <td colspan='5'><b>Belanja</b></td>
                            <td style='text-align:right'><b>Rp ".rupiah($total['total'])."</b></td>
                          </tr>
						  <tr>
                            <td colspan='5'><b>Diskon Belanja</b></td>
                            <td style='text-align:right;color:red;'><b>(Rp ".rupiah($detail['diskon']).")</b></td>
                          </tr>
						  <tr>
                            <td colspan='5'><b>Ongkir</b></td>
                            <td style='text-align:right'><b>Rp ".rupiah($detail['ongkir'])."</b></td>
                          </tr>
						   <tr>
                            <td colspan='5'><b>Diskon Ongkir</b></td>
                            <td style='text-align:right;color:red;'><b>(Rp ".rupiah($detail['diskon_ongkir']).")</b></td>
                          </tr>";
						  if ($rows['asuransi'] !=0){ 
						   echo"<tr>
                            <td colspan='5'><b>Asuransi</b></td>
                            <td style='text-align:right'><b>(Rp ".rupiah($rows['asuransi']).")</b></td>
                          </tr>";
						  }
						  if ($rows['mdr'] !=0){ 
						   echo"<tr>
                            <td colspan='5'><b>Biaya Admin</b></td>
                            <td style='text-align:right'><b>(Rp ".rupiah($rows['mdr']).")</b></td>
                          </tr>";
						  }else{
							  
						  }
						  if ($rows['pajak'] !=0){ 
						   echo"<tr>
                            <td colspan='5'><b>Pajak PPN 10%</b></td>
                            <td style='text-align:right'><b>(Rp ".rupiah($rows['pajak']).")</b></td>
                          </tr>";
						  }
                          echo"<tr class='success'>
                            <td colspan='5'><b>Total</b></td>
                            <td style='text-align:right'><b>Rp ".rupiah($tot)."</b></td>
                          </tr>";
                  ?>
                  </tbody>
                </table>
              </div>