
	<script type="text/javascript">
        $(document).ready(function(){

            $("#select_address").change(function(){
                var address_id = $(this).val();

                $.ajax({
                    url: "<?php echo site_url(); ?>admin/get_data_pickup/",
                    type: 'post',
                    data: {address:address_id},
                    dataType: 'json',
                    success:function(response){

                        var len = response.length;

                        $("#pickup_city_id").empty();
                        for( var i = 0; i<len; i++){
                            var nama_kota = response[i]['nama_kota'];
							var pickup_city_id = $(".pickup_city_id").val();

                        }
                    }
                });
            });

        });
    </script>

<div class="col-xs-12"> 
	<div class="box">
		<div class="box-header">
		  <h3 class="box-title">Request Pickup</h3>
		  <span style="margin-left:10px"><a class='pull-right btn btn-default btn-sm' href='<?php echo base_url().$this->uri->segment(1); ?>/penjualan'>Kembali</a></span>
		</div><!-- /.box-header -->
		<div class="box-body">
			<table class='table table-condensed table-bordered'>
				<tbody>
					<tr>
						<th width='140px' scope='row'>Kode Pembelian</th>  <td><?php echo "$record[kode_transaksi]"; ?></td>
						<th width='140px' scope='row'>Waktu Transaksi</th>  <td><?php echo "$record[waktu_transaksi]"; ?></td>
					</tr>
					<tr>
						<th scope='row'>Nama Pembeli</th><td><?php echo "<a href='".base_url().$this->uri->segment(1)."/detail_konsumen/$record[id_konsumen]'>$record[nama_konsumen]</a>"; ?></td>
						<th scope='row'>Kurir</th>               <td><?php echo "<span style='text-transform:uppercase'>$record[kurir]</span> - $record[service]"; ?></td>
					</tr> 
				</tbody>
			</table>
			<table class='table table-condensed'>
				<tbody>
					<tr>
						<td style="text-align:center"><a data-toggle="collapse" href="#collapse1">Detail Item <i class="fa fa-angle-down" aria-hidden="true"></i></a></td>
					</tr> 
				</tbody>
			</table>			
			<div id="collapse1" class="panel-collapse collapse">
				<table class="table table-bordered table-striped">
					<thead>
					  <tr>
						<th style='width:40px'>No</th>
						<th>Nama Produk</th>
						<th>Service</th>
						<th>Jumlah Koli</th>
						<th>Berat (gram)</th>
						<th>Harga Barang</th>
					  </tr>
					</thead>
					<tbody>
					<?php 
						$no = 1;
						foreach ($prod_detail as $row){
						$sub_total = ($row['harga_jual']*$row['jumlah'])-$row['diskon'];

						echo "<tr><td>$no</td>
								  <td>$row[nama_produk]</td>
								  <td style='text-align:right'>$record[ongkir]</td>
								  <td style='text-align:right'>$row[jumlah]</td>
								  <td style='text-align:right'>$row[berat]</td>
								  <td style='text-align:right'>Rp ".rupiah($row['harga_jual'])."</td>
							  </tr>";
						  $no++;
						}
						$total = $this->db->query("SELECT sum((a.harga_jual*a.jumlah)-a.diskon) as total FROM `rb_penjualan_detail` a JOIN rb_penjualan b ON a.id_penjualan=b.id_penjualan where b.kode_transaksi='".$this->uri->segment(3)."' AND b.id_penjual='".$this->session->id_reseller."'")->row_array();
						$ongkir = ($record['ongkir'])+$row['asuransi'];
						$tot = ($total['total']+$row['ongkir']+$row['asuransi'])-$row['diskon_ongkir']-$row['diskon'];
						
							echo "<tr><td width='140px' scope='row' td Colspan='2'><b>Total Belanja</b></td>
								<td></td>
								<td></td>
								<td></td>
								<td style='text-align:right'>Rp ".rupiah($total[total])."</td>
							</tr>
							<tr><td style='color:red;' width='140px' scope='row' Colspan='2'><b>Diskon Belanja</b></td>
								<td></td>
								<td></td>
								<td></td>
								<td style='text-align:right;color:red;'>(Rp ".rupiah($row['diskon']).")</td>
							</tr>
							<tr><td width='140px' scope='row' Colspan='2'><b>Ongkir /Asuransi</b></td>
								<td></td>
								<td></td>
								<td></td>
								<td style='text-align:right'>Rp ".rupiah($ongkir)."</td>
							</tr>
							<tr><td style='color:red;' width='140px' scope='row' Colspan='2'><b>Diskon Ongkir</b></td>
								<td></td>
								<td></td>
								<td></td>
								<td style='text-align:right;color:red;'>(Rp ".rupiah($row['diskon_ongkir']).")</td>
							</tr>
							<tr><td width='140px' scope='row' Colspan='2'><b>Total Transaksi</b></td>
								<td></td>
								<td></td>
								<td></td>
								<td style='text-align:right'>Rp ".rupiah($tot)."</td>
							</tr>";
					?>
					</tbody>
				</table>
			</div>
			
			
			<hr/>
			
			<?php  echo $this->session->flashdata('message'); ?>
				
			<?php 
			   if($success == '1'){
			?>
				<table class='table table-condensed table-bordered'>
					<tbody>
						<tr>
							<th width='140px' scope='row'>Request ID</th>  
							<td><?php echo "$response[res_request_id]"; ?></td>
						</tr>
						<tr>
							<th scope='row'>AWB</th>
							<td><?php echo "$response[awb]"; ?></td>
						</tr>
					</tbody>
				</table>
			<?php
			   }else{
			?>
				
				
				<form method="post" action="<?php echo base_url().$this->uri->segment(1).'/request_pickup/'.$this->uri->segment(3)."/".$this->uri->segment(4); ?>" enctype="multipart/form-data">
					<input type="hidden" value="<?php echo $this->uri->segment(4);?>" name='idp'>
					<input type="hidden" value="<?php echo $this->uri->segment(3);?>" name='kode_transaksi'>
					<input type="hidden" value="<?php echo $record['account_ncs'];?>" name='account_ncs'>
					<input type="hidden" value="<?php echo $address['id_kota_merchant'];?>" name='id_kota_merchant'>
					<input type="hidden" value="<?php echo $record['nama_konsumen'];?>" name='nama_konsumen'>
					<input type="hidden" value="<?php echo $record['alamat_konsumen'];?>" name='alamat_konsumen'>
					<input type="hidden" value="<?php echo $record['id_kota_konsumen'];?>" name='id_kota_konsumen'>
					<input type="hidden" value="<?php echo $record['kota_konsumen'];?>" name='kota_konsumen'>
					<input type="hidden" value="<?php echo $record['prov_konsumen'];?>" name='prov_konsumen'>
					<input type="hidden" value="<?php echo $record['zip_konsumen'];?>" name='zip_konsumen'>
					<input type="hidden" value="<?php echo $record['telp_konsumen'];?>" name='telp_konsumen'>
					<input type="hidden" value="<?php echo $record['email_konsumen'];?>" name='email_konsumen'>
					<input type="hidden" value="<?php echo $record['TotalBelanja'];?>" name='TotalBelanja'>
					<input type="hidden" value="<?php echo $record['ongkir'];?>" name='ongkir'>
					<input type="hidden" value="<?php echo $record['service_code'];?>" name='service_code'>
					<input type="hidden" value="<?php echo $record['TotalBarang'];?>" name='TotalBarang'>
					<input type="hidden" value="<?php echo $record['TotalBerat'];?>" name='TotalBerat'>
					<input type="hidden" value="<?php echo $record['konten'];?>" name='konten'>
					<input type="hidden" value="<?php echo $record['pembayaran'];?>" name='pembayaran'>
					<input type="hidden" value="<?php echo $record['username'];?>" name='username'>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group" id="row-permintaan"  style="padding-bottom:10px;border-bottom:1px solid #eee">
								<div class="row" >
									<div class="col-md-12">
										<label>Jenis Permintaan</label>
									</div>
									<div class="col-md-12">
										<label class="radio-inline">
											<input type="radio" name="request-type"  <?php if($record['drop']!='1'){echo 'checked';} ?> class="jenis-permintaan" value="0" required/> Request
										</label>
										<label class="radio-inline">
											<input type="radio" name="request-type" <?php if($record['drop']=='1'){echo 'checked';} ?> class="jenis-permintaan" value="1"/> Drop 
										</label>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-12" id="pickup-form" >
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Nama Merchant</label>
										<input type="text" class="form-control" name="nama_merchant" placeholder="Nama Merchant" value="<?php echo $record['nama_merchant']; ?>" required readonly/>
									</div>
									<div class="form-group">
										<label>Email</label>
										<input type="text" class="form-control" id="merchant_email" name="merchant_email" placeholder="Email" value="<?php echo $record['email']; ?>" />
									</div>
									
									<div class="form-group">
										<label>Tanggal Pengambilan</label>
										<input type="date" class="form-control" id="pickup_date" name="pickup_date" value=""  required/>
									</div>
									<div class="form-group">
										<div class="row">
											<label class="col-xs-12">Waktu Pengambilan</label>
										</div>
										<div class="row">
											<div class="col-xs-12 col-sm-6">
												<label>Start</label>
												<input type="text" id="pickup_time_start" name="pickup_time_start" class="form-control time timefrom" required>
											</div>
											<div class="col-xs-12 col-sm-6">
												<label>End</label>
												<input type="text" id="pickup_time_end" name="pickup_time_end" class="form-control time timeto" required>
											</div>
										</div>
									</div>
									<div class="form-group">
										<label>Jenis Transportasi</label>
										<div class="select_transport">
											<select class="form-control" name="pickup_transportation" id="pickup_transportation" value="" required>
												<option selected disabled value="">--pilih--</option>
												<?php
												$url = 'https://apimobile.ptncs.com/php/ncs_deal/get_pickuptransportmode.php';

												$ch = curl_init();
												curl_setopt($ch,CURLOPT_URL, $url);
												curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
												$server_output = curl_exec($ch);
												
												curl_close($ch);
												
												$data = json_decode($server_output, true);
												
												$i = 0;
												foreach($data as $R1){
													?>													
													<option value="<?php echo $data[$i]['PuTransportID']; ?>" ><?php echo $data[$i]['PuTransportName']; ?></option>
													<?php
													$i++;
												}
												?>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label>Instruksi Khusus</label>
										<textarea class="form-control" name="pickup_instruction" rows="3" maxlength="350"  ></textarea>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<div class="row">
											<div class="col-xs-12 col-sm-6">
												<label>PIC</label>
												<input type="text" class="form-control" id="pic" name="pic" placeholder="Person in contact" value="<?php echo $address['pic']?>" readonly>
											</div>
											<div class="col-xs-12 col-sm-6">
												<label>Telpon</label>
												<input type="text" class="form-control" id="telp_merchant" name="telp_merchant" placeholder="Telpon" value="<?php echo $address['no_hp']?>"  readonly>
											</div>
										</div>
									</div>
									<div class="form-group">
										<label>Alamat Pickup</label>
										<textarea class="form-control" id="alamat_merchant" name="alamat_merchant" rows="5" maxlength="350"  readonly><?php echo $address['alamat_lengkap']?></textarea>
									</div>
									
									<div class="form-group">
										<label>Provinsi</label>
											<input type="text" class="form-control" id="prov_merchant" name="prov_merchant" placeholder="Provinsi" value="<?php echo $address['nama_provinsi']?>" readonly>
										<!--<div class="select_prov">
											<select class="form-control" name="pickup_province_id" id="pickup_province_id" onchange="document.getElementById('prov_merchant').value=this.options[this.selectedIndex].text;" required>
												<option selected disabled value="">--pilih--</option>
												<?php
												// $url = 'https://apimobile.ptncs.com/php/ncs_deal/getProvinsi.php';

												$server_output = file_get_contents($url);
												
												// $ch = curl_init();
												// curl_setopt($ch,CURLOPT_URL, $url);
												// curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
												// $server_output = curl_exec($ch);
												
												// curl_close($ch);
												
												// $data = json_decode($server_output, true);
												
												// $i = 0;
												// foreach($data as $R1){
													// if (substr($record['id_kota_merchant'], 0, 2)== $R1['ProID']){
														// echo "<option value='$R1[ProID]' selected>$R1[ProName]</option>";
													  // }else{
														// echo "<option value='$R1[ProID]'>$R1[ProName]</option>";
													  // }
													  
													// $i++;
												// }
												?>
											</select>
											<input type="hidden" name="prov_merchant" id="prov_merchant" value="<?php echo $record['prov_merchant'];?>">
										</div>-->
									</div>
									<div class="form-group">
										<label>Kota</label>
										<input type="text" class="form-control" id="kota_merchant" name="kota_merchant" placeholder="Kota" value="<?php echo $address['nama_kota']?>" readonly>
										<!--<select class="form-control" name="pickup_city_id" id="pickup_city_id" value="" onchange="document.getElementById('kota_merchant').value=this.options[this.selectedIndex].text;" required>
											<option selected disabled value="">--pilih--</option>
											<?php
												// if($record['id_kota_merchant'] != ''){
													// $kota = $this->model_app->view_where_ordering('rb_kota',array('provinsi_id'=>substr($record['id_kota_merchant'], 0, 2)),'kota_id','ASC');
													// foreach ($kota as $row) {
													  // if ($record['id_kota_merchant']==$row['kota_id']){
														// echo "<option value='$row[kota_id]' selected>$row[nama_kota]</option>";
													  // }else{
														// echo "<option value='$row[kota_id]'>$row[nama_kota]</option>";
													  // }
													// }
												// }
											?>
										</select>
										<input type="hidden" name="kota_merchant" id="kota_merchant" value="<?php echo $record['kota_merchant'];?>">-->
									</div>
									<div class="form-group">
										<label>Kode Pos</label>
										<input type="text" class="form-control" id="merchant_zipcode" name="merchant_zipcode" placeholder="Kode Pos" value="<?php echo $address['kode_pos'] ?>" readonly>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<hr/>
					<table class="table table-bordered table-striped">
						<center> <input class='btn btn-primary btn-sm' type="submit" name='submit' value='Send Request'></center>
					</table>
				</form>
			<?php 
			   }
			?>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		setForm();
		
		$(function() { 
		 $('html, body').animate({
			scrollTop: $('#message-form').offset().top}, 1000);
		}); 
		
		$('.timefrom').clockTimePicker({
			precision:10,
			required:true,
			duration:false,
			minimum:'10:00',
			alwaysSelectHoursFirst:true,
			onChange:function(newVal, oldVal) {
				var t1 = new Date ('2020-08-01 ' + newVal);
				var t2 = new Date ( t1 );
				t2.setMinutes ( t1.getMinutes() + 60 );					
				var t3 = t2.getHours()+':'+t2.getMinutes();
				
				$(".timeto").val(t3);
				
				$('.timeto').clockTimePicker({
					minimum:t3,
					precision:10,
					required:true,
					duration:false,
					alwaysSelectHoursFirst:true,
					vibrate:true
				});
			},
		});
		
		$('.timeto').clockTimePicker({
			precision:10,
			required:true,
			duration:false,
			alwaysSelectHoursFirst:true,
			vibrate:true
		});
		
		$(".jenis-permintaan").each(function(o_index,o_val){	
			$(this).on("change",function(){
				var did=$(this).val();

				if(did == '0'){
					$("#pickup-form").slideDown("slow");
				}else {
					$("#pickup-form").slideUp("slow");
				}
			
				setForm();
			});	
		});

	});
	
	function setForm(){	
		//  document.getElementById("pic").value = "<?php echo $address['pic']; ?>";
				
		// $("div.select_prov select").val("<?php echo substr($address['id_kota_merchant'], 0, 2); ?>");
		
		// document.getElementById("prov_merchant").value = "<?php echo $address['prov_merchant']; ?>";
		
		// $("div.select_kota select").val("<?php echo $address['id_kota_merchant']; ?>");
		
		// document.getElementById("kota_merchant").value = "<?php echo $address['kota_merchant']; ?>";
		
		
		// document.getElementById("telp_merchant").value = "<?php echo $address['telp_merchant']; ?>";
		
		// document.getElementById("merchant_email").value = "<?php echo $address['email_merchant']; ?>";
		
		// $("textarea#alamat_merchant").val("<?php echo $address['alamat_merchant']; ?>");
		
		var today = new Date();
		document.getElementById("pickup_date").value = today.getFullYear() + '-' + ('0' + (today.getMonth() + 1)).slice(-2) + '-' + ('0' + today.getDate()).slice(-2);
		
		var t1 = new Date(today);
		t1.setMinutes ( today.getMinutes() + 120 );	
		var timeFrom = t1.getHours()+':00';
		var t2 = new Date (t1);
		t2.setMinutes ( t1.getMinutes() + 60 );	
		var timeTo = t2.getHours()+':00';
		$(".timefrom").val(timeFrom);
		$(".timeto").val(timeTo);
		
		$("div.select_transport select").val("102");
	}
</script>

<!--</form> -->