<style>
	.blue {
		background-color: #2196F3 !important;
	}
	
	.blue.darken-1 {
		background-color: #1E88E5 !important;
	}

	.blue-text.text-darken-1 {
		color: #1E88E5 !important;
	}

	.blue.darken-2 {
		background-color: #1976D2 !important;
	}

	.blue-text.text-darken-2 {
		color: #1976D2 !important;
	}

	.blue.darken-3 {
		background-color: #1565C0 !important;
	}

	.blue-text.text-darken-3 {
		color: #1565C0 !important;
	}

	.blue.darken-4 {
		background-color: #0D47A1 !important;
	}

	.blue-text.text-darken-4 {
		color: #0D47A1 !important;
	}
	
	.blue.accent-1 {
	  background-color: #82B1FF !important;
	}

	.blue-text.text-accent-1 {
	  color: #82B1FF !important;
	}

	.blue.accent-2 {
	  background-color: #448AFF !important;
	}

	.blue-text.text-accent-2 {
	  color: #448AFF !important;
	}

	.blue.accent-3 {
	  background-color: #2979FF !important;
	}

	.blue-text.text-accent-3 {
	  color: #2979FF !important;
	}

	.blue.accent-4 {
	  background-color: #2962FF !important;
	  color:white;
	}

	.blue-text.text-accent-4 {
	  color: #2962FF !important;
	}
	
	//comment panel
	.thumbnail {
		padding:0px;
	}
	.panel-comment {
		position:relative;width:45%;
	}
	
	.panel-comment > .panel-heading{
		background-color:#E6E2E2 !important;
	}
	.panel-comment>.panel-heading:after,.panel>.panel-heading:before{
		position:absolute;
		top:11px;left:-16px;
		right:100%;
		width:0;
		height:0;
		display:block;
		content:" ";
		border-color:transparent;
		border-style:solid solid outset;
		pointer-events:none;
	}
	
	.panel-comment>.panel-heading:after{
		border-width:7px;
		border-right-color:#E6E2E2;
		margin-top:1px;
		margin-left:2px;
	}
	.panel-comment>.panel-heading:before{
		border-right-color:#ddd;
		border-width:8px;
	}

</style>
<body style="font-family: 'Ubuntu', sans-serif;">
<nav class="navbar navbar-default blue darken-4 navbar-fixed">
	<div class="container">
	<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
		  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		  </button>
		  <a class="navbar-brand" href="#" style="color:white"><b>Daftar Pesan</b></a>
		</div>
	</div>
</nav>
<div class="container">
	<div class="row">
		<div class="col-md-3">
			<ul class="list-group">
				<li class="list-group-item blue darken-4 " style="color:white;font-weight:bold"><i class="fa fa-users"></i> Chatt List</li>
				<?php foreach($user->result_array() as $row){ 
					$isi_p = $this->db->query("select count(id_penjual) as jml from chat_admin where id_penjual='".$this->session->id_reseller."'and flag=0 and ket=1 order by id_chat")->row_array();
					?>
					<a href="javascript:void(0)" style="text-decoration:none"><li class="list-group-item " id="user" ><i class="fa fa-angle-right"></i>
					Admin</a>
					<span type="button" class='glyphicon glyphicon-remove' onclick="hapus_pesan();"></span>
					</li>
				<?php } ?>
			</ul>
		</div>
		<div class="col-md-7">
			<div class="panel panel-info">
				<div class="panel-heading  blue darken-4" style="color:white;font-weight:bold" ><i class="fa fa-comments"></i> Chatt Box</div>
				<div class="panel-body" style="height:400px;overflow-y:auto" id="box">
					<div id="chat-box">
						<div class='panel-body'><h2 style='text-align:center;color:grey'>Click User on Chatt List to Start Chatt</h2></div>
						<!--br/>
						<div id="loading" style="display:none"><center><i class="fa fa-spinner fa-spin"></i> Loading...</center></div>
						</br !-->
					</div>
				</div>
				<div class="panel-footer" style="display:none">
					<div class="row">
						<div class="col-md-12">
							<textarea class="form-control " id="pesan" style="margin-right:10px;"></textarea>
							<button id="send" type="button" class="btn btn-primary pull-right" style="margin-top:10px;"  onClick="sendMessage()" ><i class="fa fa-send"></i> Send Message</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="<?php echo base_url()?>assets/bootstrap/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/bootstrap/js/bootstrap.min.js"></script>
<script>
var refresh_chat="";
$(document).ready(function(){
getChatAll();
	//getChat(0);
	$("#user").click(function(){
		$("#id_max").val('0');
	});
	
	setInterval(function(){ 
		if($("#id_user").val() > 0){
			getLastId($("#id_user").val(),$("#id_max").val()); 
			getChat($("#id_user").val(),$("#id_max").val()); 
			autoScroll();
		}else{
			
		}
	},3000);
});

function getChatAll(){
	// alert(id_pembeli);
	clearInterval(refresh_chat);
	refresh_chat = window.setInterval(function(){
	// if(id_pembeli==''){
	// 	id_pembeli = $("#id_pembeli").val();
	// }
	$.ajax({
		url		: "<?php echo site_url().$this->uri->segment(1);?>/chat_admin/",
		type	: 'POST',
		dataType: 'html',
		data 	: { },
		beforeSend	: function(){
			$("#loading").show();
		},
		success	: function(result){
            // alert(id_pembeli);
			$("#loading").hide();
			$("#chat-box").html(result);
			$(".panel-footer").show();
			
			// autoScroll();
			document.getElementById('pesan').focus();
		}
	});
	}, 1000);
}

function sendMessage(){
	var pesan 	= $("#pesan").val();
	
	if(pesan == ''){
		document.getElementById('pesan').focus();
	}else{
		$.ajax({
			url		: "<?php echo site_url().$this->uri->segment(1);?>/kirim_pesan_admin/",
			type	: 'POST',
			dataType: 'json',
			data 	: {pesan:pesan},
			beforeSend	: function(){
			},
			success	: function(result){
				// getChatAll(id_pembeli);
				$("#pesan").val('');
				autoScroll();
			}
		});
		// getChatAll(id_pembeli);
				$("#pesan").val('');
				autoScroll();
	}
}

function autoScroll(){
	var elem = document.getElementById('box');
	elem.scrollTop = elem.scrollHeight;
}

function aktifkan(i){
	$("li").removeClass("active");
	$("#aktif-"+i).addClass("active");
}

function hapus_pesan(){
	var r = confirm("apakah anda yakin ingin menghapus pesan ini ?");
  if (r == true) {
	
	$.ajax({
			url		: "<?php echo site_url().$this->uri->segment(1);?>/hapus_pesan_admin",
			type	: 'POST',
			dataType: 'json',
			data 	: {id_pembeli:'222'},
			success: function (data) {
				alert("success!");
				location.reload();
			},
			error: function (data) {
                alert('gagal');
            }
		});
  	} else {
		// location.reload();
  	}
	
}
</script>