<style>
  .onoffswitch3 {
    position: relative;
    width: 200px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
  }

  .onoffswitch3-checkbox {
    display: none;
  }

  .onoffswitch3-label {
    display: block;
    overflow: hidden;
    cursor: pointer;
    border: 0px solid #999999;
    border-radius: 0px;
  }

  .onoffswitch3-inner {
    display: block;
    width: 200%;
    margin-left: -100%;
    -moz-transition: margin 0.3s ease-in 0s;
    -webkit-transition: margin 0.3s ease-in 0s;
    -o-transition: margin 0.3s ease-in 0s;
    transition: margin 0.3s ease-in 0s;
  }

  .onoffswitch3-inner>span {
    display: block;
    float: left;
    position: relative;
    width: 50%;
    height: 30px;
    padding: 0;
    line-height: 30px;
    font-size: 14px;
    color: white;
    font-family: Trebuchet, Arial, sans-serif;
    font-weight: bold;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
  }

  .onoffswitch3-inner .onoffswitch3-active {
    padding-left: 10px;
    background-color: #EEEEEE;
    color: #FFFFFF;
  }

  .onoffswitch3-inner .onoffswitch3-inactive {
    padding-right: 10px;
    background-color: #EEEEEE;
    color: #FFFFFF;
    text-align: right;
  }

  .onoffswitch3-switch {
    display: block;
    width: 50%;
    margin: 0px;
    text-align: center;
    border: 0px solid #999999;
    border-radius: 0px;
    position: absolute;
    top: 0;
    bottom: 0;
  }

  .onoffswitch3-active .onoffswitch3-switch {
    background: #27A1CA;
    left: 0;
  }

  .onoffswitch3-inactive .onoffswitch3-switch {
    background: #A1A1A1;
    right: 0;
  }

  .onoffswitch3-checkbox:checked+.onoffswitch3-label .onoffswitch3-inner {
    margin-left: 0;
  }
</style>

<div class="col-xs-12">
  <div class="box">
    <div class="box-header">
      <h3 class="box-title">Alamat Request Pickup</h3>

    </div><!-- /.box-header -->

    <div class="box-header">
      <table style="width: 100%;" border=0px>
        <tr>
          <td style="padding-right: 5px; text-align: right; width: 100%;">
            Cash On Delivery (COD)
          </td>

          <td style="padding-left: 5px; padding-right: 10px; width:auto;" class='pull-right'>
            <div class="onoffswitch3">
              <input type="checkbox" name="onoffswitch3" class="onoffswitch3-checkbox" id="cod" onclick="set_cod();" <?php if ($seller['cod'] == '1') {
                                                                                                                        echo "checked";
                                                                                                                      } ?>>
              <label class="onoffswitch3-label" for="cod">
                <span class="onoffswitch3-inner">
                  <span class="onoffswitch3-active"><span class="onoffswitch3-switch">Active</span></span>
                  <span class="onoffswitch3-inactive"><span class="onoffswitch3-switch">Inactive</span></span>
                </span>
              </label>
            </div>
          </td>

          <td>
            <a class='pull-right btn btn-primary btn-sm' href='<?php echo base_url() . $this->uri->segment(1); ?>/tambah_adress_pickup'>Tambahkan Data</a>
          </td>
        </tr>
      </table>

    </div>
    <div class="box-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th style='width:20px'>No</th>
            <th style='width:300px'>Alamat</th>
            <th>PIC</th>
            <th style='width:200px'>Kota</th>
            <th>Telpon</th>
            <th style='width:70px'>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $no = 1;
          foreach ($record->result_array() as $row) {
            echo "<tr><td>$no</td>
                              <td>$row[alamat_lengkap]</td>
                              <td>$row[pic]</td>
                              <td>$row[nama_kota]</td>
                              <td>$row[no_hp]</td>
                              <td><center>
                                <a class='btn btn-success btn-xs' title='Edit Data' href='" . base_url() . $this->uri->segment(1) . "/edit_adress_pickup/$row[id_alamat_reseller]'><span class='glyphicon glyphicon-edit'></span></a>
                                <a class='btn btn-danger btn-xs' title='Delete Data' href='" . base_url() . $this->uri->segment(1) . "/delete_adress_pickup/$row[id_alamat_reseller]' onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\"><span class='glyphicon glyphicon-remove'></span></a>
                              </center></td>
                          </tr>";
            $no++;
          }
          ?>
        </tbody>
      </table>
    </div>

    <script>
      function set_cod() {

        if (document.getElementById("cod").checked) {
          let cod = '1'
          let pesan = 'COD Sudah Di Aktifkan';

          $.ajax({
            type: 'POST',
            url: '<?php echo base_url() . $this->uri->segment(1); ?>/save_cod',
            dataType: "json",
            data: ({
              "cod": cod,
            }),
            success: function(data) {
              if (data.sukses == '1') {
                alert(pesan);
              } else {
                alert('gagal');
              }
            }
          });

        } else {
          let cod = '0'
          let pesan = 'COD Di Non Aktifkan';
          $.ajax({
            type: 'POST',
            url: '<?php echo base_url() . $this->uri->segment(1); ?>/save_cod',
            dataType: "json",
            data: ({
              "cod": cod,
            }),
            success: function(data) {
              if (data.sukses == '1') {
                alert(pesan);
              } else {
                alert('gagal');
              }
            }
          });
        }
      }
    </script>