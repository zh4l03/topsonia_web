<style>
  .onoffswitch3 {
    position: relative;
    width: 200px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
  }

  .onoffswitch3-checkbox {
    display: none;
  }

  .onoffswitch3-label {
    display: block;
    overflow: hidden;
    cursor: pointer;
    border: 0px solid #999999;
    border-radius: 0px;
  }
  
  .onoffswitch3-inner {
    display: block;
    width: 200%;
    margin-left: -100%;
    -moz-transition: margin 0.3s ease-in 0s;
    -webkit-transition: margin 0.3s ease-in 0s;
    -o-transition: margin 0.3s ease-in 0s;
    transition: margin 0.3s ease-in 0s;
  }

  .onoffswitch3-inner>span {
    display: block;
    float: left;
    position: relative;
    width: 50%;
    height: 30px;
    padding: 0;
    line-height: 30px;
    font-size: 14px;
    color: white;
    font-family: Trebuchet, Arial, sans-serif;
    font-weight: bold;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
  }

  .onoffswitch3-inner .onoffswitch3-active {
    padding-left: 10px;
    background-color: #EEEEEE;
    color: #FFFFFF;
  }

  .onoffswitch3-inner .onoffswitch3-inactive {
    padding-right: 10px;
    background-color: #EEEEEE;
    color: #FFFFFF;
    text-align: right;
  }

  .onoffswitch3-switch {
    display: block;
    width: 50%;
    margin: 0px;
    text-align: center;
    border: 0px solid #999999;
    border-radius: 0px;
    position: absolute;
    top: 0;
    bottom: 0;
  }

  .onoffswitch3-active .onoffswitch3-switch {
    background: #27A1CA;
    left: 0;
  }

  .onoffswitch3-inactive .onoffswitch3-switch {
    background: #A1A1A1;
    right: 0;
  }

  .onoffswitch3-checkbox:checked+.onoffswitch3-label .onoffswitch3-inner {
    margin-left: 0;
  }
</style>
<script>
 $(function(){
  $("#fileupload").on("change", function() {
    if ($("#fileupload")[0].files.length > 4) {
      alert("Anda hanya dapat mengunggah maksimal 4 foto");
      document.getElementById("fileupload").value = "";
    }
  });
});
</script>

<script language="JavaScript" type="text/JavaScript">
	function showSub(){
    <?php
    $query = $this->db->query("SELECT * FROM rb_kategori_produk");
    foreach ($query->result_array() as $data) {
     $id_kategori_produk = $data['id_kategori_produk'];
     echo "if (document.demo.a.value == \"".$id_kategori_produk."\")";
     echo "{";
     $query_sub_kategori = $this->db->query("SELECT * FROM rb_kategori_produk_sub where id_kategori_produk='$id_kategori_produk'");
     $content = "document.getElementById('sub_kategori_produk').innerHTML = \"  <option value=''>- Pilih Sub Kategori Produk -</option>";
     foreach ($query_sub_kategori->result_array() as $data2) {
       $content .= "<option value='".$data2['id_kategori_produk_sub']."'>".$data2['nama_kategori_sub']."</option>";
     }
     $content .= "\"";
     echo $content;
     echo "}\n";
   }
   ?>
 }
</script><script language="JavaScript" type="text/JavaScript">	// function tampilkan(){		// var tes=document.getElementById("sat").value;            // document.getElementById("satuanlabel").value=tes;	// }</script>

<?php 
echo "<div class='col-md-12'>
<div class='box box-info'>
<div class='box-header with-border'>
<h3 class='box-title'>Tambah Produk Baru</h3>
</div>
<div class='box-body'>";			 
$attributes = array('class'=>'form-horizontal','role'=>'form','name'=>'demo',"onsubmit"=>"return cek_berat();");
echo form_open_multipart($this->uri->segment(1).'/tambah_produk',$attributes); 
echo "<div class='col-md-12'>
<table class='table table-condensed table-bordered'>
<tbody>
<input type='hidden' name='id' value=''>
<tr>						<th scope='row'>Kategori</th>                   						<td><select name='a' class='form-control' onchange=\"showSub()\" required>
<option value='' selected>- Pilih Kategori Produk -</option>";
foreach ($record as $row){
 echo "<option value='$row[id_kategori_produk]'>$row[nama_kategori]</option>";
}
echo "</td></tr>
<tr>						<th scope='row'>Sub Kategori</th>                  						<td><select name='aa' class='form-control' id='sub_kategori_produk'>
<option value='' selected>- Pilih Sub Kategori Produk -</option>
</td>					</tr>
<tr><th width='130px' scope='row'>Nama Produk</th>  <td><input type='text' class='form-control' name='b' required></td></tr>
<tr><th scope='row'>Satuan</th>                     						<td>							<select id='sat' name='c' class='form-control' style='width:100%;' onchange=\"tampilkan()\" required>								<option value='' selected>- Pilih Kategori Produk -</option>";								$satuan =  $this->db->query("SELECT * FROM rb_produk_satuan");								foreach ($satuan->result_array() as $row) {																		echo "<option value='$row[nama_produk_satuan]'>$row[nama_produk_satuan]</option>";								}							echo"</select>						</td>						<!--<td><input type='text' id='satuanlabel' class='form-control' name='cc'></td>-->					</tr>
<tr><th scope='row'>Berat / Gram</th>                      <td><input type='number' class='form-control' name='berat' id='berat'></td></tr>
<tr><th scope='row'>Harga Modal</th>                 <td><input type='number' class='form-control' name='d'></td></tr>
<input type='hidden' class='form-control' name='e' value='0'>
<tr><th scope='row'>Harga Jual</th>             <td><input type='number' class='form-control' name='f'></td></tr>
<tr><th scope='row'>Diskon</th>                 <td><input type='number' class='form-control' name='diskon'></td></tr>
<tr><th scope='row'>Stok Awal</th>                 <td><input type='number' class='form-control' name='stok' required></td></tr>";

if(in_array($this->session->id_reseller,array("21","24"))){

echo "<tr><th scope='row'>Free Ongkir</th>                 <td>
<div class='row'>
<div class='col-sm-3'>
<div class='onoffswitch3'>
<input type='checkbox' name='free_ongkir' class='onoffswitch3-checkbox' id='free_ongkir' value='Active'>
<label class='onoffswitch3-label' for='free_ongkir'>
  <span class='onoffswitch3-inner'>
    <span class='onoffswitch3-active'><span class='onoffswitch3-switch'>Active</span></span>
    <span class='onoffswitch3-inactive'><span class='onoffswitch3-switch'>Inactive</span></span>
  </span>
</label>
</div>
</div>
</div> 
</td></tr>";

}

echo "
<tr><th scope='row'>Keterangan</th>                 <td><textarea class='textarea form-control' name='ff' style='height:180px'></textarea></td></tr>
<tr><th scope='row'>Foto Produk</th><td>";
?>

<table>
  <tr>
    <td><img id='tampil0' style='border:1px solid #cecece; width:100px; height: auto;' src='<?php echo base_url() ?>asset/foto_produk/thumb/no-image-icon.gif'></td><td>&nbsp;&nbsp;&nbsp;</td>
    <td><input type='file' id='fileupload' class='form-control' name='userfile[0]' onchange='document.getElementById("tampil0").src = window.URL.createObjectURL(this.files[0])' required></td>
    <td>&nbsp;&nbsp;&nbsp;</td>
    <td><img id='tampil1' style='border:1px solid #cecece; width:100px; height: auto;' src='<?php echo base_url() ?>asset/foto_produk/thumb/no-image-icon.gif'></td><td>&nbsp;&nbsp;&nbsp;</td>
    <td><input type='file' id='fileupload' class='form-control' name='userfile[1]' onchange='document.getElementById("tampil1").src = window.URL.createObjectURL(this.files[0])'></td>
  </tr>
  <tr><td>&nbsp;</td></tr>
  <tr>
    <td><img id='tampil2' style='border:1px solid #cecece; width:100px; height: auto;' src='<?php echo base_url() ?>asset/foto_produk/thumb/no-image-icon.gif'></td><td>&nbsp;&nbsp;&nbsp;</td>
    <td><input type='file' id='fileupload' class='form-control' name='userfile[2]' onchange='document.getElementById("tampil2").src = window.URL.createObjectURL(this.files[0])'></td>
    <td>&nbsp;&nbsp;&nbsp;</td>
    <td><img id='tampil3' style='border:1px solid #cecece; width:100px; height: auto;' src='<?php echo base_url() ?>asset/foto_produk/thumb/no-image-icon.gif'></td><td>&nbsp;&nbsp;&nbsp;</td>
    <td><input type='file' id='fileupload' class='form-control' name='userfile[3]' onchange='document.getElementById("tampil3").src = window.URL.createObjectURL(this.files[0])'></td>
  </tr>

</table>

<b>Allowed File :</b> .gif, jpg, png || <b>Max Size :</b> 5MB
<div id='dvPreview'></div>

<?php
echo "</td></tr>
</tbody>
</table>
</div>
</div>
<div class='box-footer'>
<button type='submit' name='submit' class='btn btn-info'>Tambahkan</button>
<a href='".base_url().$this->uri->segment(1)."/produk'><button type='button' class='btn btn-default pull-right'>Cancel</button></a>

</div>
</div>";

?>

<script type="text/javascript">
  $(document).ready(function(){
  for(let i=0; i<4; i++){
  
var uploadField = document.getElementById('userfile['+i+']');
uploadField.onchange = function() {
    if(this.files[0].size > 5000000){ // ini untuk ukuran 800KB, 1000000 untuk 1 MB.
       alert("Maaf. File Terlalu Besar ! Maksimal Upload 5 mb");
       this.value = "";
    };
};

}
});

function cek_berat(){
  let berat = document.getElementById("berat").value;
  let merchant_id = "<?php echo $this->session->id_reseller; ?>";
  if(berat>0){
    
  }else{
    if(merchant_id==='21' || merchant_id==='24'){

    }else{
      alert("Berat Tidak Boleh Kosong");
      return false;
    }
  }
  
}

</script>
