<div class="col-xs-12">
	<div class="box">
		<div class="box-header">
			<h3 class="box-title">Data Transaksi Penjualan / Orderan Agen</h3>
			<!--<a class='pull-right btn btn-primary btn-sm' href='<?php echo base_url().$this->uri->segment(1); ?>/tambah_penjualan'>Tambah Penjualan</a> -->
		</div>
		<!-- /.box-header -->
		<div class="box-body">
			<table id="example1" class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th style='width:40px'>No</th>
						<th>Kode Transaksi</th>
						<th>Nama</th>
						<th>Pengiriman</th>
						<th>Status</th>
						<th>Pembayaran</th>
						<th>Total Bayar</th>
						<th/>
					</tr>
				</thead>
				<tbody>
					<?php $no = 1;
					// $pemb = $this->db->query("SELECT * FROM rb_konfirmasi_pembayaran_konsumen where kode_transaksi='$row[kt]'")->result_array(); 
					foreach ($record->result_array() as $row){
						if ($row['proses']=='0' && $row['bayar']=='0' && $row['kategori_pembayaran']!='cod' ){	
							$proses = '<i class="text-danger">Pending</i>';	
							$button = 'btn-default';	
							$status = 'Proses';	
							$icon = 'hourglass';	
							$href = "disabled";	
						}else if ($row['proses']=='0' && $row['bayar']=='0' && $row['kategori_pembayaran']=='cod'){
							$proses = '<i class="text-warning">COD</i>';
							$button = 'btn-primary';	
							$status = 'Proses';	
							$icon = 'bitcoin';	
							$href = "href='".base_url().$this->uri->segment(1)."/proses_penjualan/$row[idp]/1/0' onclick=\"return confirm('Apa anda yakin untuk ubah status jadi $status?')\"";	
						}elseif($row['proses']=='0' && ($row['bayar']=='1' or $row['bayar']=='2')){ 
						if ($row['status_pembayaran']=='lunas'){
								$proses = '<i class="text-warning">Lunas</i>';
								$button = 'btn-primary';	
								$status = 'Proses';	
								$icon = 'bitcoin';	
								$href = "href='".base_url().$this->uri->segment(1)."/proses_penjualan/$row[idp]/1/0' onclick=\"return confirm('Apa anda yakin untuk ubah status jadi $status?')\"";	
							}elseif ($row['status_pembayaran']=='' || $row['status_pembayaran']=='diperiksa'){
								$proses = '<i class="text-warning">Menunggu Konfirmasi</i></a>';
								$button = 'btn-primary';	
								$status = 'Proses';	
								$icon = 'bitcoin';	
								$href = "disabled";	
							}elseif ($row['status_pembayaran']=='gagal'){
								$proses = '<i class="text-warning">Gagal</i></a>';
								$button = 'btn-primary';	
								$status = 'Proses';	
								$icon = 'bitcoin';	
								$href = "disabled";	
							}elseif ($row['status_pembayaran']=='proses refund'){
								$proses = '<i class="text-warning">Proses refund</i></a>';
								$button = 'btn-primary';	
								$status = 'Proses';	
								$icon = 'bitcoin';	
								$href = "disabled";	
							}elseif ($row['status_pembayaran']=='refund'){
								$proses = '<i class="text-warning">Refund</i></a>';
								$button = 'btn-primary';	
								$status = 'Proses';	
								$icon = 'bitcoin';	
								$href = "disabled";	
							}else{
								$proses = '<i class="text-warning">Menunggu Konfirmasi</i></a>';
								$button = 'btn-primary';	
								$status = 'Proses';	
								$icon = 'bitcoin';	
								$href = "disabled";	
							}
						}elseif($row['proses']=='1' && $row['selesai']=='0'){

							if($this->db->query("select awb from rb_status_pengiriman where awb='".$row['awb']."'")->num_rows()>0){
								$proses = '<i class="text-success">Dikirim</i>';	
							}else if($row['request_pickup']=='1' && $row['drop']=='0'){
								$proses = '<i class="text-success">Request Pickup</i>';	
							}else if($row['request_pickup']=='1' && $row['drop']=='1'){
								$proses = '<i class="text-success">Drop</i>';	
							}else{
								$proses = '<i class="text-success">Proses</i>';	
							}
							
							$button = 'btn-primary';	
							$status = 'Selesai';	
							$icon = 'star';	$ubah = 0;	
							$href = "href='".base_url().$this->uri->segment(1)."/proses_penjualan/$row[kode_transaksi]/1/1' onclick=\"return confirm('Apa anda yakin untuk ubah status jadi $status?')\"";	
						}else{	
							$proses = '<i class="text-success">Selesai</i>';	
							$button = 'btn-success';	
							$status = '';	
							$icon = 'ok';	
							$href = "disabled";	
						}
					
					$total = $this->db->query("SELECT sum((a.harga_jual*a.jumlah)-a.diskon) as total FROM `rb_penjualan_detail` a JOIN rb_penjualan b ON a.id_penjualan=b.id_penjualan where b.kode_transaksi='$row[kt]' AND b.id_penjual='$row[id_penjual]'")->row_array();                    
					$tot = ($total['total']+$row['ongkir']+$row['asuransi'])-$row['diskon_ongkir']-$row['diskon'];
					echo "<tr><td>$no</td>

                              <td>";

							if($row['flag_reseller']=='0' && ($row['status_pembayaran']=='lunas' || ($row['pembayaran']=='cod' && $row['proses']=='0')) ){
								echo "<b>".$row['kt']."*</b>";
							}else{
								echo $row['kt'];
							}
                              

                              echo "</td><td> <a href='".base_url().$this->uri->segment(1)."/detail_konsumen/$row[id_pembeli]'>$row[nama_lengkap]</a> </td>
							<td> <span style='text-transform:uppercase'>$row[kurir]</span></td>
							<td>$proses</td>
							<td>$row[kategori_pembayaran]</td>
							<td style='color:red;'>Rp ".rupiah($tot)."</td>
							<td>
								<center>
									<a class='btn btn-success btn-xs' title='Detail Data' href='".base_url().$this->uri->segment(1)."/detail_penjualan/$row[kt]'> <i class='fa fa-search' aria-hidden='true'></i> Detail</a>
									<a class='btn $button btn-xs' title='$status Data' $href >
										<span class='glyphicon glyphicon-$icon'></span>
									</a>";
									if ($row['awb']=='' and $row['proses']=='1'){
										$button = 'btn-primary';
										$href = "href='".base_url().$this->uri->segment(1)."/request_pickup/$row[kode_transaksi]/$row[id_penjualan]'";
										$aktifpickup = '';
										$text = 'text-white';
									}else{
										$button = 'btn-default';
										$href = "";
										$aktifpickup = 'disabled';
										$text = 'white';
									}
									echo "<a class='btn $button white btn-sm' style='margin-left:3px;margin-right:3px;padding: 1px 7px !important;'
									title='Request Pickup' $href $aktifpickup > <i class='fa fa-truck $text'></i> </a>";
								
									if ($row['awb']!=''){
										$button = 'btn-primary';
										$href = "href='".base_url().$this->uri->segment(1)."/print_label/$row[idp]'";
										$aktifprint = '';
										$text = 'text-white';
									}else{
										$button = 'btn-default';
										$href = "";
										$aktifprint = 'disabled';
										$text = 'white';
									}
									echo "<span class='dropdown'> <button class='btn $button white btn-sm dropdown-toggle' style='padding: 1px 7px !important;'
									title='Print Label' target='_blank' $aktifprint data-toggle='dropdown'> <i class='fa fa-print $text'></i> </button>
									<ul class='dropdown-menu'>
									  <li><a href='".base_url().$this->uri->segment(1)."/print_label/$row[idp]/3' target='_blank'>A4/3</a></li>
									  <li><a href='".base_url().$this->uri->segment(1)."/print_label/$row[idp]/6' target='_blank'>A4/6</a></li>
									  <li><a href='".base_url().$this->uri->segment(1)."/print_label/$row[idp]/12' target='_blank'>A4/12</a></li>
									</ul> </span>";
									
									echo "<!--<a class='btn btn-danger btn-xs' title='Delete Data' href='".base_url().$this->uri->segment(1)."/delete_penjualan/$row[id_penjualan]' onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\"><span class='glyphicon glyphicon-remove'></span></a> -->
								</center>
							</td>
						</tr>";
					$no++;
					}
					?>                  
				</tbody>
			</table>
		</div>
	</div>
</div>