<div class='col-md-12'>
	<div class='box box-info'>
		<div class='box-header with-border'>
			<h3 class='box-title'>Konfigurasi Metode Pembayaran</h3>
			<!--   <a class='pull-right btn btn-warning btn-sm' href='<?php //echo base_url(); ?>administrator/reseller'>Kembali</a> -->
		</div>
		<div class='box-body'>
			<table id="example1" class="table table-striped">
				<thead>
					<tr>
						<th style='width:30px; text-align:center;'>No</th>
						<th style="text-align: left;">Jenis Pembayaran</th>
						<th style="text-align: center;">Status</th>
						<th style="text-align: center;"></th>
					</tr>
				</thead>
				<tbody>
					<?php 
					$no = 1;
					foreach ($configtampil->result_array() as $row){
					?>
					<tr>
						<td style="text-align:center;"><?php echo $no; ?></td>
						<td>
							<?php
								$target = $row['nama_konfig'];
								$r = str_replace("_"," ",$target);
								$nama_konfig = ucwords($r);

								echo $nama_konfig;
							?>
						</td>
						<td style="text-align:center;">
							<?php
								if ($row['flag']=='1') {
									echo "<span style='color:green;'>Aktif</span>";
								}else{
									echo "<span style='color:red;'>Tidak Aktif</span>";
								}
							?>
						</td>
						<td style="text-align:center;">
							<?php
								if ($row['flag']=='1') {
							?>
									<a href="<?php echo base_url();?>administrator/onoffpayment?konfig=<?php echo $row['id_konfig']; ?>&onoff=<?php echo $row['flag']; ?>" class="btn btn-success btn-xs"><span class='glyphicon glyphicon-off'></span></a>
							<?php
								}else{
							?>
									<a href="<?php echo base_url();?>administrator/onoffpayment?konfig=<?php echo $row['id_konfig']; ?>&onoff=<?php echo $row['flag']; ?>" class="btn btn-danger btn-xs"><span class='glyphicon glyphicon-off'></span></a>
							<?php
								}
							?>
							

							<!--<a class='btn btn-danger btn-xs' title='Delete Data' href='<?php //echo base_url();?>administrator/delete_tipe_buyer/<?php //echo $row['id_tipe_buyer'];?>' onclick=\"return confirm('Data ini terhubung ke daftar konsumen, Apa anda yakin menghapus Data ini?')\"><span class='glyphicon glyphicon-remove'></span></a>-->
						</td>
					</tr>
					<?php
						$no++;
					}
					?>
				</tbody>
			</table>

			<div style='clear:both'></div>
		</div>
	</div>
</div>