<?php 
    echo "<div class='col-md-12'>
              <div class='box box-info'>
                <div class='box-header with-border'>
                  <h3 class='box-title'>Edit waktu timeout trnsaksi</h3>
                </div>
              <div class='box-body'>";
              $attributes = array('class'=>'form-horizontal','role'=>'form');
              echo form_open_multipart($this->uri->segment(1).'/config_timeout_edit',$attributes); 
          echo "<div class='col-md-12'>
                  <table class='table table-condensed table-bordered'>
                  <tbody>
                    <input type='hidden' name='id' value='$rows[id_konfig]'>
                    <tr><th width='200px' scope='row'>Nama Konfigurasi</th>    <td><input type='text' class='form-control' name='a' value='$rows[nama_konfig]' disabled></td></tr>
                    <tr><th width='200px' scope='row'>Waktu Timeout 1</th>    <td><input type='text' class='form-control' name='b' value='$rows[value1]' required></td></tr>
                    <tr><th width='200px' scope='row'>Waktu Timeout 2</th>    <td><input type='text' class='form-control' name='c' value='$rows[value2]' required></td></tr>
                    <tr><th></th><td><div style='color:red;'><span>* Waktu konfigurasi dalam hitungan jam (Contoh : 3 hari = 72)</span></div></td></tr>
					</tbody>
                  </table>
                </div>
              
              <div class='box-footer'>
                    <button type='submit' name='submit' class='btn btn-info'>Update</button>
                    <a href='".base_url().$this->uri->segment(1)."/config_timeout'><button type='button' class='btn btn-default pull-right'>Cancel</button></a>
                    
                  </div>
            </div></div></div>";
            echo form_close();