<?php 
    echo "<div class='col-md-12'>
            <div class='box box-info'>
                <div class='box-header with-border'>
					<h3 class='box-title'>Tambah Kategori Produk</h3>
                </div>
            <div class='box-body'>";
				$attributes = array('class'=>'form-horizontal','role'=>'form');
				echo form_open_multipart('administrator/tambah_kategori_produk',$attributes); 
				echo "<div class='col-md-12'>
						<table class='table table-condensed table-bordered'>
							<tbody>
								<input type='hidden' name='id' value=''>
								<tr>									<th width='120px' scope='row'>Nama Kategori</th>    									<td><input type='text' class='form-control' name='a' required></td>								</tr>
								<tr>									<th width='120px' scope='row'>Foto</th>    									<td><input type='file' class='form-control' name='foto_kategori' id='foto_kategori' required accept='.gif,.jpg,.png,.jpeg'></td>								</tr>
							</tbody>
						</table>
					</div>
            </div>
            <div class='box-footer'>
				<button type='submit' name='submit' class='btn btn-info'>Tambahkan</button>
				<a href='".base_url()."administrator/kategori_produk'><button type='button' class='btn btn-default pull-right'>Cancel</button></a>
            </div>
        </div>";

?>

<script>

$('#foto_kategori').bind('change', function() {

//this.files[0].size gets the size of your file.
if((this.files[0].size/1048576).toFixed(2)>1.00){
	document.getElementById('foto_kategori').value="";
	alert('File Maksimal 1 MB');
}

let ext = /(\.jpg|\.jpeg|\.gif|\.png)$/i;

if(!ext.exec(document.getElementById('foto_kategori').value)){
	alert("File extension not supported!");
	document.getElementById('foto_kategori').value="";
}


});
	
</script>