  <script language="JavaScript" type="text/JavaScript">
function showSub(){
	<?php
	$query = $this->db->query("SELECT * FROM rb_kategori_produk");
	foreach ($query->result_array() as $data) {
		$id_kategori_produk = $data['id_kategori_produk'];
		echo "if (document.demo.a.value == \"".$id_kategori_produk."\")";
		echo "{";
		$query_sub_kategori = $this->db->query("SELECT * FROM rb_kategori_produk_sub where id_kategori_produk='$id_kategori_produk'");
		$content = "document.getElementById('sub_kategori_produk').innerHTML = \"  <option value=''>- Pilih Sub Kategori Produk -</option>";
		foreach ($query_sub_kategori->result_array() as $data2) {
			$content .= "<option value='".$data2['id_kategori_produk_sub']."'>".$data2['nama_kategori_sub']."</option>";
		}
		$content .= "\"";
		echo $content;
		echo "}\n";
	}
	?>
}
</script>

<?php 
echo "<div class='col-md-12'>
			<div class='box box-info'>
				<div class='box-header with-border'>
				<h3 class='box-title'>Tambah Produk Baru</h3>
				</div>
			<div class='box-body'>";
$attributes = array('class'=>'form-horizontal','role'=>'form','name'=>'demo');
echo form_open_multipart('administrator/tambah_produk',$attributes); 
echo "<div class='col-md-12'>
				<table class='table table-condensed table-bordered'>
				<tbody>
					<input type='hidden' name='id' value=''>
					<tr><th scope='row'>Kategori</th><td><select name='a' class='form-control' onchange=\"showSub()\" required>
						<option value='' selected>- Pilih Kategori Produk -</option>";
foreach ($record as $row){
	echo "<option value='$row[id_kategori_produk]'>$row[nama_kategori]</option>";
}
echo "</td></tr>
					<tr><th scope='row'>Sub Kategori</th><td><select name='aa' class='form-control' id='sub_kategori_produk'>
							<option value='' selected>- Pilih Sub Kategori Produk -</option>
						</td></tr>
					<tr><th width='130px' scope='row'>Nama Produk</th><td><input type='text' class='form-control' name='b' required><input type='hidden' class='form-control' name='id_reseller' value='".$this->uri->segment(3)."'></td></tr>
					<tr><th scope='row'>Satuan</th><td><select id='sat' name='c' class='form-control' style='width:100%;' onchange=\"tampilkan()\" required><option value='' selected>- Pilih Satuan -</option>";	
						$satuan =  $this->db->query("SELECT * FROM rb_produk_satuan");								
						foreach ($satuan->result_array() as $row) {																		
						echo "<option value='$row[nama_produk_satuan]'>$row[nama_produk_satuan]</option>";								
						}							
						echo"</select>						
					</td></tr>		
					<tr><th scope='row'>Berat / Gram</th><td><input type='number' class='form-control' name='berat'></td></tr>
					<!--<tr><th scope='row'>Harga Modal</th><td><input type='number' class='form-control' name='d'></td></tr>
					<tr><th scope='row'>Harga Reseller</th><td><input type='number' class='form-control' name='e'></td></tr>-->
					<tr><th scope='row'>Harga Produk</th><td><input type='number' class='form-control' name='f'></td></tr>
					<tr><th scope='row'>Diskon</th><td><input type='number' class='form-control' name='diskon'></td></tr>
					<tr><th scope='row'>Stok Awal</th><td><input type='number' class='form-control' name='stok'></td></tr>
					<tr><th scope='row'>Keterangan</th><td><textarea id='editor1' class='form-control' name='ff'></textarea></td></tr>
					<tr><th scope='row'>Foto Produk</th>                     						<td>";
// <input type='file' id='fileupload' class='form-control' name='userfile[]' multiple>Multiple Upload, Allowed File : .gif, jpg, png
// <div id='dvPreview'></div>
?>
<table>
<tr>
<td><img id='tampil0' style='border:1px solid #cecece; width:100px; height: auto;' src='<?php echo base_url() ?>asset/foto_produk/thumb/no-image-icon.gif'></td><td>&nbsp;&nbsp;&nbsp;</td>
<td><input type='file' id='fileupload' class='form-control' name='userfile[0]' onchange='document.getElementById("tampil0").src = window.URL.createObjectURL(this.files[0])' required></td>
<td>&nbsp;&nbsp;&nbsp;</td>
<td><img id='tampil1' style='border:1px solid #cecece; width:100px; height: auto;' src='<?php echo base_url() ?>asset/foto_produk/thumb/no-image-icon.gif'></td><td>&nbsp;&nbsp;&nbsp;</td>
<td><input type='file' id='fileupload' class='form-control' name='userfile[1]' onchange='document.getElementById("tampil1").src = window.URL.createObjectURL(this.files[0])'></td>
</tr>
<tr><td>&nbsp;</td></tr>
<tr>
<td><img id='tampil2' style='border:1px solid #cecece; width:100px; height: auto;' src='<?php echo base_url() ?>asset/foto_produk/thumb/no-image-icon.gif'></td><td>&nbsp;&nbsp;&nbsp;</td>
<td><input type='file' id='fileupload' class='form-control' name='userfile[2]' onchange='document.getElementById("tampil2").src = window.URL.createObjectURL(this.files[0])'></td>
<td>&nbsp;&nbsp;&nbsp;</td>
<td><img id='tampil3' style='border:1px solid #cecece; width:100px; height: auto;' src='<?php echo base_url() ?>asset/foto_produk/thumb/no-image-icon.gif'></td><td>&nbsp;&nbsp;&nbsp;</td>
<td><input type='file' id='fileupload' class='form-control' name='userfile[3]' onchange='document.getElementById("tampil3").src = window.URL.createObjectURL(this.files[0])'></td>
</tr>

</table>

<?php

echo "</td></tr>
				</tbody>
			</table>
		</div>
		</div>
		<div class='box-footer'>
			<button type='submit' name='submit' class='btn btn-info'>Tambahkan</button>
			<a href='".base_url()."administrator/produk/".$this->uri->segment(3)."'><button type='button' class='btn btn-default pull-right'>Cancel</button></a>
		</div>
	</div>";
