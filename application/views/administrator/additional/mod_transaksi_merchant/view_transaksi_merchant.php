<style>
	#example_m thead tr{ background-color: #e3e3e3; } 
</style>

<div class="col-xs-12">
	<ul class="nav nav-tabs">
		<li class="active">
			<a href="#1" data-toggle="tab">Data Per Transaksi</a>
		</li>
		<li><a href="#2" data-toggle="tab">Data Per Merchant</a>
		</li>
	</ul>
	<div class="tab-content ">
		<div class="box tab-pane active" id="1">

			<div class="box-header">

				<h3 class="box-title">Data Per Transaksi</h3>

			</div><!-- /.box-header -->

			<div class="box-body">

				<table id="example1" class="table table-bordered table-striped">

					<thead>

						<tr>

							<th style='width:40px'>No</th>

							<th>Kode Transaksi</th>

							<th>Waktu Transaksi</th>

							<th>Nama Pembeli</th>

							<th>Jenis Pembayaran</th>

							<th>Status</th>


							<th>Total</th>

							<!--<th>Proses / Keterangan</th>-->

							<th style='width:100px'>Action</th>

						</tr>

					</thead>

					<tbody>

						<?php

						$no = 1;

						foreach ($record->result_array() as $row) {

							if ($row['bayar']=='0'){	
								$proses = '<i class="text-danger">Menunggu Pembayaran</i>';
							}else if ($row['status_pembayaran']=='diperiksa') {
								$proses = '<i class="text-warning">Menunggu<br />Konfirmasi</i>';
							}elseif ($row['status_pembayaran']=='lunas') {
								$proses = '<i class="text-warning">Lunas</i>';
							}elseif ($row['status_pembayaran']=='gagal') {
								$proses = '<i class="text-warning">Gagal</i>';
							}elseif ($row['status_pembayaran']=='proses refund'){
								$proses = '<i class="text-warning">Proses Refund</i>';
							}else if ($row['status_pembayaran']=='refund'){
								$proses = '<i class="text-warning">Refund</i>';
							}else{
									$proses = '<i class="text-warning">Menunggu<br />Konfirmasi</i>';
							}

							$total = $this->db->query("SELECT sum((a.harga_jual*a.jumlah)-a.diskon) as total FROM `rb_penjualan_detail` a where a.id_penjualan='$row[idp]'")->row_array();

							echo "<tr><td>$no</td>

                              <td>";

							if ($row['status_pembayaran'] == 'lunas' && $row['flag_admin'] == '0') {
								echo "<b>" . $row['kode_trx'] . "*</b>";
							} elseif ($row['flag_admin'] == '0' && $row['status_pembayaran'] == 'diperiksa' && $row['pembayaran'] == 'manual-transfer' && $row['bukti_bayar'] != '') {
								echo "<b>" . $row['kode_trx'] . "*</b>";
							} else {
								echo $row['kode_trx'];
							}

							echo "</td>

                              <td>$row[waktu_order]</td>

							  <td><a href='" . base_url() . $this->uri->segment(1) . "/detail_konsumen/$row[id_konsumen]'>$row[nama_lengkap]</a></td>

                              <td>$row[pembayaran]</td>

                              <td>$proses</td>

                               <td style='color:red;'>Rp " . rupiah($row['total_bayar']) . "</td>

                               <!--<td>$service</td>-->

                              <td><center>";

							if ($row['status_pembayaran'] == 'proses refund' || $row['status_pembayaran'] == 'refund') {

								echo "<a class='btn btn-primary btn-xs' title='Detail Data' href='" . base_url() . "administrator/detail_refund/$row[kode_trx]'><span class='glyphicon glyphicon-retweet'></span> Refund</a>";
							} elseif ($row['order'] == '1' && $row['proses'] == '0' && $row['bayar'] == '0') {

								echo "<a class='btn btn-success btn-xs' title='Detail Data' href='" . base_url() . "administrator/detail_transaksi/$row[kode_trx]'><span class='glyphicon glyphicon-search'></span> Detail</a><br/>";
							} else {

								echo "<a class='btn btn-success btn-xs' title='Detail Data' href='" . base_url() . "administrator/detail_transaksi/$row[kode_trx]'><span class='glyphicon glyphicon-search'></span> Detail</a><br/>";
							}

							echo "</center></td>

                          </tr>";

							$no++;
						}

						?>

					</tbody>

				</table>

			</div>

		</div>

		<div class="box tab-pane" id="2">
			<div class="box-header">
				<h3 class="box-title">Data Per Merchant</h3>
			</div>

			<div class="box-body">

				<table id="example_m" class="table table-bordered table-striped">

					<thead>

						<tr>

							<th>No</th>

							<th>Kode Transaksi</th>

							<th>Nama Pembeli</th>

							<th>Nama Merchant</th>

							<th>Waktu Transaksi</th>

							<!-- <th>Jenis Pembayaran</th> -->

							<th style='width:90px'>Status</th>

							<th>Total Belanja</th>

							<th>Action</th>

						</tr>

					</thead>

					<tbody>

						<?php

						$no = 1;

						foreach ($record2->result_array() as $row) {
							if ($row['proses'] == '0' && $row['bayar'] == '0') {
								$proses = '<i class="text-danger">Menunggu Pembayaran</i>';
							} elseif ($row['proses'] == '0' && ($row['bayar'] == '1' or $row['bayar'] == '2')) {
								if ($row['status_pembayaran'] == 'lunas') {
									$proses = '<i class="text-warning">Lunas</i>';
								} elseif ($row['status_pembayaran'] == '' || $row['status_pembayaran'] == 'diperiksa') {
									$proses = '<i class="text-warning">Menunggu Konfirmasi</i></a>';
								} elseif ($row['status_pembayaran'] == 'gagal') {
									$proses = '<i class="text-warning">Gagal</i></a>';
								} elseif ($row['status_pembayaran'] == 'proses refund') {
									$proses = '<i class="text-warning">Proses refund</i></a>';
								} elseif ($row['status_pembayaran'] == 'refund') {
									$proses = '<i class="text-warning">Refund</i></a>';
								} else {
									$proses = '<i class="text-warning">Menunggu Konfirmasi</i></a>';
								}
							} elseif ($row['proses'] == '1' && $row['selesai'] == '0') {
								if ($row['request_pickup'] == '1' && $row['drop'] == '0') {
									$req = "Request Pickup";
								} else if ($row['request_pickup'] == '1' && $row['drop'] == '1') {
									$req = "Drop";
								}else {
									$req = "Sedang diproses";
								}
								$proses = "<i class='text-success'> $req </i>";
							} else {
								$proses = '<i class="text-success">Selesai</i>';
							}

							$total = $this->db->query("SELECT sum((a.harga_jual*a.jumlah)-a.diskon) as total FROM `rb_penjualan_detail` a where a.id_penjualan='$row[idp]'")->row_array();

							echo "<tr><td>$no</td>

                              <td>";

							if ($row['status_pembayaran'] == 'lunas' && $row['flag_admin'] == '0') {
								echo "<b>" . $row['kode_trx'] . "*</b>";
							} elseif ($row['flag_admin'] == '0' && $row['status_pembayaran'] == 'diperiksa' && $row['pembayaran'] == 'manual-transfer' && $row['bukti_bayar'] != '') {
								echo "<b>" . $row['kode_trx'] . "*</b>";
							} else {
								echo $row['kode_trx'];
							}

							echo "</td>

							  <td><a href='" . base_url() . $this->uri->segment(1) . "/detail_konsumen/$row[id_konsumen]' target='_blank'>$row[nama_lengkap]</a></td>

							  <td><a href='" . base_url() . "administrator/detail_reseller/$ro[id_reseller]' target='_blank'>$row[nama_reseller]</a></td>

                              <td>$row[waktu_transaksi]</td>

                              <td>$proses</td>

                               <td style='color:red;'>Rp " . rupiah($total['total']+$row['ongkir']+$row['asuransi']-$row['diskon_ongkir']) . "</td>

                               <!--<td>$service</td>-->

                              <td><center>";

							if ($row['status_pembayaran'] == 'proses refund' || $row['status_pembayaran'] == 'refund') {

								echo "<a class='btn btn-primary btn-xs' title='Detail Data' href='" . base_url() . "administrator/detail_refund/$row[kode_trx]'><span class='glyphicon glyphicon-retweet'></span> Refund</a>";
							} elseif ($row['order'] == '1' && $row['proses'] == '0' && $row['bayar'] == '0') {

								echo "<a class='btn btn-success btn-xs' title='Detail Data' href='" . base_url() . "administrator/detail_transaksi/$row[kode_trx]/$row[idp]'><span class='glyphicon glyphicon-search'></span> Detail</a><br/>";
							} else {

								echo "<a class='btn btn-success btn-xs' title='Detail Data' href='" . base_url() . "administrator/detail_transaksi/$row[kode_trx]/$row[idp]'><span class='glyphicon glyphicon-search'></span> Detail</a><br/>";
							}

							echo "</center></td>

                          </tr>";

							$no++;
						}

						?>

					</tbody>

				</table>

			</div>
		</div>

	</div>

</div>

<script>
	$(function() {
		$("#example_m").DataTable();
	});
</script>