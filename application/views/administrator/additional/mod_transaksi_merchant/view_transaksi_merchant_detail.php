<?php $detail = $this->db->query("SELECT * FROM rb_penjualan where kode_transaksi='" . $this->uri->segment(3) . "'")->row_array(); ?>


<div class="col-xs-12">
	<div class="box">
		<div class="box-header">
			<h3 class="box-title">Data Detail Transaksi Penjualan</h3>
			<a class='pull-right btn btn-default btn-sm' href='<?php echo base_url(); ?>administrator/transaksi'>Kembali</a>
		</div><!-- /.box-header -->
		<div class="box-body">
			<table class='table table-condensed table-bordered'>
				<tbody>
					<tr>
						<th width='140px' scope='row'>Kode Pembelian</th>
						<td><?php echo "$rows[kode_transaksi]"; ?></td>
					</tr>
					<tr>
						<th width='140px' scope='row'>Nama Pembeli</th>
						<td><?php echo "<a href='" . base_url() . $this->uri->segment(1) . "/detail_konsumen/$rows[id_konsumen]'>$rows[nama_lengkap]</a>"; ?></td>
					</tr>
					<tr>
						<th width='140px' scope='row'>Jenis Pembayaran</th>
						<td><?php echo "$rows[pembayaran]"; ?></td>
					</tr>
					<tr>
						<th width='140px' scope='row'>Waktu Transaksi</th>
						<td><?php echo "$rows[waktu_order]"; ?></td>
					</tr>
					<tr>
						<th width='140px' scope='row'>Alamat </th>
						<td><?php echo "$rows[alamat_lengkap]"; ?></td>
					</tr>
					<tr>
						<th width='140px' scope='row'><a href="<?php echo base_url() . $this->uri->segment(1) . '/print_invoice/' . $rows[kode_transaksi] . '/' ?>" class='btn btn-default btn-sm' style='margin-right:5px' target="_blank"><span class='glyphicon glyphicon-print'></span> Invoice For Buyer</button></th>
							<th scope='row'><a href="<?php echo base_url() . $this->uri->segment(1) . '/print_invoice_admin/' . $rows[kode_transaksi] . '/' ?>" class='btn btn-default btn-sm' style='margin-right:5px' target="_blank"><span class='glyphicon glyphicon-print'></span> Invoice For Merchant</button></th>
					</tr>

				</tbody>
			</table>
			<hr />
			<?php 
			$asr = 0;
			$total_belanja = 0;
			$total_ongkir = 0;
			$total_diskon_belanja = 0;
			$total_diskon_ongkir = 0;
			$total_cashback = 0;
			?>
			
			<?php foreach ($rows2 as $ro) {

				if ($ro['proses'] == '0' && $ro['bayar'] == '0') {
					$proses = '<i class="text-danger">Menunggu Pembayaran</i>';
				} elseif ($ro['proses'] == '0' && ($ro['bayar'] == '1' or $ro['bayar'] == '2')) {
					if ($pemb['status_pembayaran'] == 'diperiksa') {
						$proses = '<i class="text-warning">Menunggu Konfirmasi</i>';
					} elseif ($pemb['status_pembayaran'] == '' && $ro['bayar'] == '1') {
						$proses = '<i class="text-warning">Lunas</i>';
					} elseif ($pemb['status_pembayaran'] == 'lunas') {
						$proses = '<i class="text-warning">Lunas</i>';
					} elseif ($pemb['status_pembayaran'] == 'gagal') {
						$proses = '<i class="text-warning">Gagal</i>';
					} elseif ($pemb['status_pembayaran'] == 'proses refund') {
						$proses = '<i class="text-warning">Proses Refund</i>';
					} else {
						$proses = '<i class="text-warning">Refund</i>';
					}
				} elseif ($ro['proses'] == '1' && $ro['selesai'] == '0') {
					if ($ro['request_pickup'] == '1' && $ro['drop'] == '0') {
						$req = "(Request Pickup)";
					} else {
						$req = "";
					}
					$proses = "<i class='text-success'>Sedang diproses $req </i>";
				} elseif ($ro['selesai'] == '1') {
					$proses = '<i class="text-success">Selesai</i>';
				}
			?>
				<table class='table table-condensed table-bordered'>
					<tbody>
						<tr>
							<th width='140px' scope='row'>ID Penjualan</th>
							<td><?php echo "$ro[id_penjualan]"; ?></td>
						</tr>
						<tr>
							<th width='140px' scope='row'>Nama Merchant</th>
							<td><?php echo "<a href='" . base_url() . "administrator/detail_reseller/$ro[id_reseller]' target='_blank'>"; ?> <?php echo "$ro[nama_reseller]"; ?> </a>
							</td>
						</tr>
						<tr>
							<th width='140px' scope='row'>Service</th>
							<td><?php echo "$ro[service]"; ?></td>
						</tr>
						<tr>
							<th width='140px' scope='row'>Status</th>
							<td><?php echo "$proses"; ?></td>
						</tr>
						<tr>
							<th width='140px' scope='row'>Catatan</th>
							<td><?php echo "$ro[catatan_pelapak]"; ?></td>
						</tr>
					</tbody>
				</table>
				<br />

				<table class="table table-bordered table-striped">
					<thead>
						<tr>
							<th style='width:40px'>No</th>
							<th>Nama Produk</th>
							<th>Satuan</th>
							<th>Harga Jual</th>
							<th>Diskon</th>
							<th>Jumlah</th>
							<th style='text-align:right'>Sub Total</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$barang = $this->db->query('select * from rb_penjualan_detail a join rb_produk b on a.id_produk=b.id_produk where id_penjualan="' . $ro['idp'] . '" order by a.id_penjualan_detail asc')->result_array();

						$no = 1;
						$tot_harga = 0;
						$tot_disk = 0;
						foreach ($barang as $row) {
							$sub_total = ($row['harga_jual'] * $row['jumlah']) - $row['diskon'];
							$tot_harga = $sub_total + $tot_harga;
							$tot_disk = $row['diskon']+$tot_disk;
							echo "<tr><td>$no</td>
									  <td>$row[nama_produk]</td>
									  <td>$row[satuan]</td>
									  <td>Rp " . rupiah($row['harga_jual']) . "</td>
									  <td>(Rp " . rupiah($row['diskon']) . ")</td>
									  <td>$row[jumlah]</td>
									  <td style='text-align:right'>Rp " . rupiah($sub_total) . "</td>
								  </tr>";
							$no++;
						}
						$tot_disk = $tot_disk+$ro['disk_b'];
						$total_belanja = $tot_harga + $total_belanja;
						$total_ongkir = $ro['ongkir'] + $total_ongkir;
						$total_diskon_belanja = $tot_disk + $total_diskon_belanja;
						$total_diskon_ongkir = $ro['diskon_ongkir'] + $total_diskon_ongkir;
						$asr = $asr + $ro['asuransi'];
						$total_cashback = $ro['diskon'] + $total_cashback;
						echo "<tr><td width='140px' scope='row' td Colspan='3'><b>Total Belanja</b></td>
								<td></td>
								<td></td>
								<td></td>
								<td style='text-align:right'>Rp " . rupiah($tot_harga) . "</td>
							</tr>";
						echo "<tr><td width='140px' scope='row' td Colspan='3'><b>Diskon Belanja</b></td>
							<td></td>
							<td></td>
							<td></td>
							<td style='text-align:right;color:red;'>(Rp " . rupiah($tot_disk) . ")</td>
							</tr>";
						echo "<tr><td width='140px' scope='row' td Colspan='3'><b>CashBack</b></td>
							<td></td>
							<td></td>
							<td></td>
							<td style='text-align:right'>Rp " . rupiah($ro['diskon']) . "</td>
							</tr>";
						echo "<tr><td width='140px' scope='row' Colspan='3'><b>Ongkir</b></td>
								<td></td>
								<td></td>
								<td></td>
								<td style='text-align:right'>Rp " . rupiah($ro['ongkir']) . "</td>
							</tr>";
						echo "<tr><td width='140px' scope='row' td Colspan='3'><b>Diskon Ongkir</b></td>
							<td></td>
							<td></td>
							<td></td>
							<td style='text-align:right;color:red;'>(Rp " . rupiah($ro['diskon_ongkir']) . ")</td>
							</tr>";
						echo "<tr><td width='140px' scope='row' td Colspan='3'><b>Asuransi</b></td>
							<td></td>
							<td></td>
							<td></td>
							<td style='text-align:right'>Rp " . rupiah($ro['asuransi']) . "</td>
							</tr>";
						echo "<tr><td width='140px' scope='row' Colspan='3'><b>Total Transaksi Merchant</b></td>
								<td></td>
								<td></td>
								<td></td>
								<td style='text-align:right'>Rp " . rupiah($tot_harga + $ro['ongkir'] - $tot_disk - $ro['diskon_ongkir'] + $ro['asuransi']) . "</td>
							</tr>";
							
						?>
					</tbody>
				</table>
				<hr />
			<?php } if(trim($this->uri->segment(4))!=''){}else{ ?>
		
			<center>
				<h4 class="box-title">Total Pembayaran</h4>
			</center>
			<table class="table table-bordered table-striped">
				<tr>
					<td><b>Total Belanja</b></td>
					<td style="text-align:right">Rp<?php echo rupiah($total_belanja); ?></td>
				</tr>
				<tr>
					<td><b>Total Diskon Belanja</b></td>
					<td style="text-align:right;color:red;">Rp<?php echo rupiah($total_diskon_belanja); ?></td>
				</tr>
				<tr>
					<td><b>Total Cashback</b></td>
					<td style="text-align:right">(Rp<?php echo rupiah($total_cashback); ?>)</td>
				</tr>
				<tr>
					<td><b>Total Ongkir</b></td>
					<td style="text-align:right">Rp<?php echo rupiah($total_ongkir); ?></td>
				</tr>
				<td><b>Diskon Ongkir</b></td>
				<td style="text-align:right;color:red;">Rp<?php echo rupiah($total_diskon_ongkir); ?></td>
				</tr>
				<tr>
					<td><b>Biaya Administrasi</b></td>
					<td style="text-align:right">Rp<?php echo rupiah($rows['mdr']); ?></td>
				</tr>
				<tr>
					<td><b>Asuransi</b></td>
					<td style="text-align:right">Rp<?php echo rupiah($asr); ?></td>
				</tr>
				<tr>
					<td><b>Total Transaksi</b></td>
					<td style="text-align:right">Rp<?php echo rupiah($rows['total_bayar']); ?></td>
				</tr>
			</table>
			<hr />
			<!-- <?php if ($rows['kategori_pembayaran'] == 'qris') { ?>
					<br/>					
					 <hr/>
					  <center><h5 class="box-title">Detail Pembayaran</h5></center>									   
					  <table class='table table-condensed table-bordered'>                  
						<tbody>										
						<?php
						echo "<br/><br/><table class='table table-bordered table-striped'>
									<thead>
										<tr><th width='30px'>Keterangan Konfirmasi Pembayaran</th>
											<td width='170px'>
												<strong>Pembayaran Berhasil - Melalui QRIS</strong>
											</td>
										</tr>
										<tr><th width='30px'>Remarks</th>
											<td width='170px'>
											Pembayaran berhasil <br>transaksi siap diproses oleh $rows[nama_reseller]
											</td>
										</tr>	
									</tbody>
								</table>";
					}
						?>
					  </tbody>
					  </table>
					 <br/>					
					 <hr/>	 -->
			<?php if ($rows['kategori_pembayaran'] == 'manual-transfer') { ?>
				<center>
					<h5 class="box-title">Detail Pembayaran</h5>
				</center>
				<table class='table table-condensed table-bordered'>
					<tbody>
						<?php foreach ($rec_bayar as $reks) {  ?>
							<tr>
								<th scope='row' width='310px'>Nama Pengirim</th>
								<td><?php echo "$reks[nama_pengirim]"; ?></td>
							<tr>
								<th scope='row' width='310px'>Rekening Tujuan</th>
								<td><?php echo "$reks[rekening_pengirim] - $reks[nama_penerima]"; ?></td>
							<tr>
								<th scope='row' width='310px'>Tanggal Bayar</th>
								<td><?php echo "$reks[tanggal_bayar]"; ?></td>
							<tr>
								<th scope='row' width='310px'>Nominal</th>
								<td><?php echo "$reks[nominal]"; ?></td>
							<tr>
								<th scope='row' width='310px'>Bukti Pembayaran</th>
								<td><?php echo "<a href='" . base_url() . "asset/bukti_transfer/" . $this->uri->segment(3) . ".jpg' target='_blank'><img style='border:1px solid #cecece; height:180px' src='" . base_url() . "asset/bukti_transfer/" . $this->uri->segment(3) . ".jpg'> </a>"; ?>
									<br />
									<button type='button' class='btn btn-primary btn-xs' data-toggle='modal' data-target='#exampleModal'>
										Upload Bukti Transfer
									</button>
								</td>
							<?php
						}

							?>
					</tbody>
				</table>

			<?php

				if ($pemb['status_pembayaran'] == 'diperiksa') {
					$attributes = array('class' => 'form-horizontal', 'role' => 'form');
					echo form_open_multipart('administrator/konfirmasi_pembayaran/' . $this->uri->segment(3), $attributes);


					echo "<br/><br/><table class='table table-bordered table-striped'>
								<thead>
									<tr><th width='70px'>Konfirmasi Pembayaran</th>
									<td width='100px'>
										<select class='form-control' name='status_pembayaran' id='combo' onChange='check();' style='width:auto' id='dropdownlist' onchange='myFunction()' required>";
					echo "<option value='' selected disabled>Pilih</option> 
													 <option value='lunas' >Pembayaran Dikonfirmasi</option>            
													 <option value='gagal' >Pembayaran ditolak</option>";
					echo "</select>
									</td>
									</tr>					
								</tbody>
							</table>
						
							<table class='table table-bordered table-striped' id ='remarks' visible='true'>
									<thead>
										<tr><th><center>Remarks</center></th></tr>
									</thead>
									<tbody>
										<tr  width='50px'><td><textarea  rows='5' cols='173'  class='required form-control' name='remarks' placeholder='masukan alasan pembayaran ditolak' >$pemb[remarks]</textarea></td></tr>
									</tbody>
							</table>";

					echo "<center><input class='btn btn-sm btn-primary' type='submit' name='submit' value='Konfirmasi'></center>";
					echo form_close();
				} elseif ($pemb['status_pembayaran'] == 'lunas' || ($rows['proses'] == '1')) {
					echo "<br/><br/><table class='table table-bordered table-striped'>
									<thead>
										<tr><th width='30px'>Keterangan Konfirmasi Pembayaran</th>
											<td width='170px'>
												<strong>Pembayaran Berhasil</strong>
											</td>
										</tr>
									</tbody>
								</table>";
				} elseif ($pemb['status_pembayaran'] == 'gagal') {
					echo "<br/><br/><table class='table table-bordered table-striped'>
									<thead>
										<tr><th width='30px'>Keterangan Konfirmasi Pembayaran</th>
											<td width='170px'>
												 <strong>Pembayaran Gagal</strong>
											</td>
										</tr>	
										<tr><th width='30px'>Remarks</th>
											<td width='170px' style='color:red;'>
											<i>$pemb[remarks]<i/>	
											</td>
										</tr>	
									</tbody>
								</table>
								";
				} elseif ($pemb['status_pembayaran'] == '') {
					echo "<br/><br/><table class='table table-bordered table-striped'>
									<thead>
										<tr><th width='30px'>Remarks</th>
											<td width='500px'>
											Menunggu Pembayaran
											</td>
										</tr>
										<tr>
										<th width='30px'><button type='button' class='btn btn-primary' data-toggle='modal' data-target='#exampleModal'>
												Upload Bukti Transfer
									  	</button></th>
										</tr>
										
									</thead>
								</table>
								";
				} else {
				}
			}
		}
			?>

			<center />

			<!-- Modal -->
			<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<form method="POST" action="<?php echo base_url(); ?>administrator/upload_transfer" enctype="multipart/form-data">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">Upload Transfer</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<table class='table table-condensed'>
									<tbody>
										<tr>
											<th scope='row' width='200px'>Kode Transaksi</th>
											<td><input type='text' name='kode_transaksi' class='form-control' style='width:100%' value="<?= $rows['kode_transaksi'] ?>" placeholder='TRX-0000000000' readonly required></td>
										</tr>
										<tr>
											<th scope='row'>Total</th>
											<td>
												<input type='hidden' name='metod' value="<?php if ($pemb['status_pembayaran'] == '') {
																								echo '0';
																							} else {
																								echo '1';
																							} ?>">
												<input type='text' name='jumlah' class='form-control' style='width:50%' value="Rp.<?php echo rupiah($rows['total_bayar']); ?>" readonly required>
											</td>
										</tr>
										<tr>
											<th scope='row'>Transfer Ke</th>
											<td><select name='transfer_ke' class='form-control'>";
													<?php
													$rekening = $this->db->query('select * from rb_rekening');
													foreach ($rekening->result_array() as $rek) {
														echo "<option value='" . $rek['id_rekening'] . ";" . $row['no_rekening'] . ", A/N" . $row['pemilik_rekening'] . "'>" . $rek['nama_bank'] . " - $rek[no_rekening], A/N : $rek[pemilik_rekening]</option>";
													}
													?>
										</tr>
										<tr>
											<th scope='row'>Rek Pengirim</th>
											<td><input type='text' class='form-control' name='rekening_pengirim' required></td>
										</tr>
										<tr>
											<th scope='row'>Nama Pengirim</th>
											<td><input type='text' class='form-control' style='width:70%' name='nama_pengirim' required></td>
										</tr>
										<tr>
											<th scope='row'>Tanggal Transfer</th>
											<td><input type='text' class='datepicker form-control' style='width:40%' name='tanggal_transfer' data-date-format='yyyy-mm-dd'></td>
										</tr>
										<tr>
											<th scope='row'>Konfirmasi Pembayaran</th>
											<td>
												<select class="form-control" name="status_pembayaran" style="width:auto">
													<option value="diperiksa">Diperiksa</option>
													<option value="lunas">Pembayaran Dikonfirmasi</option>
													<option value="gagal">Pembayaran ditolak</option>
												</select>
											</td>
										</tr>

										<tr>
											<th scope='row'>Bukti Transfer</th>
											<td><input type='file' name='bukti_transfer' id='bukti_transfer' class='form-control' accept='.jpg, .png, .jpeg' required>
												<p style="color:red;">Multiple Upload, Allowed File : gif, jpg, png, <br /> Maks Size 1 mb</p>
											</td>
										</tr>
									</tbody>
								</table>

							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
								<button type="Submit" class="btn btn-primary">Upload</button>
							</div>
						</div>
					</form>
				</div>
			</div>

		</div>
	</div>
</div>

<script>
	window.onload = function() {
		document.getElementById('remarks').style.display = 'none';
	}

	function check() {

		var el = document.getElementById("combo");
		var str = el.options[el.selectedIndex].text;
		if (str == "Pembayaran Dikonfirmasi" || str == "Pilih") {
			hide();
		} else {
			show();
		}

	}

	function hide() {
		document.getElementById('remarks').style.display = 'none';
	}

	function show() {
		document.getElementById('remarks').style.display = 'inline';
	}

	$('#bukti_transfer').bind('change', function() {

		//this.files[0].size gets the size of your file.
		if ((this.files[0].size / 1048576).toFixed(2) > 1.00) {
			document.getElementById('bukti_transfer').value = "";
			alert('File Maksimal 1 MB');
		} else {

		}

		if (/\.(jpe?g|png|gif)$/i.test(this.files[0].name) === false) {
			alert("ini bukan gambar!");
			document.getElementById('bukti_transfer').value = "";
		}

	});
</script>