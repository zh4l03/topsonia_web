		<div class="col-xs-12">  
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Form Pengajuan Refund</h3>
                  <a class='pull-right btn btn-default btn-sm' href='<?php echo base_url(); ?>administrator/transaksi'>Kembali</a>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table class='table table-condensed table-bordered'>
					<tbody>				  
						<tr><th width='140px' scope='row'>Kode Pembelian</th>  <td><?php echo "$rows[kode_transaksi]"; ?></td></tr>
						<tr><th scope='row'>Nama Reseller</th>                 <td><?php echo "$rows[nama_reseller]"; ?></td></tr>
						<tr><th scope='row'>Waktu Transaksi</th>               <td><?php echo "$rows[waktu_transaksi]"; ?></td></tr>
						<tr><th width='140px' scope='row'>Airwaybill</th><td><?php echo "$rows[awb]"; ?></td>
						<tr><th width='140px' scope='row'>Status</th><td>
						<?php
							if ($pemb['status_pembayaran']=='' || $pemb['status_pembayaran']=='diperiksa') {
								$proses = '<i class="text-warning">Menunggu Konfirmasi</i>';
							}elseif ($pemb['status_pembayaran']=='' && $pemb['bayar']=='1') {
								$proses = '<i class="text-warning">Lunas</i>';
							}elseif ($pemb['status_pembayaran']=='lunas') {
								$proses = '<i class="text-warning">Lunas</i>';
							}elseif ($pemb['status_pembayaran']=='gagal') {
							$proses = '<i class="text-warning">Gagal</i>';
							}elseif ($pemb['status_pembayaran']=='proses refund'){
								$proses = '<i class="text-warning">Proses Refund</i>';
							}else{
								$proses = '<i class="text-warning">Refund</i>';
							}
						
						echo "$proses <br/>"; 
						
						// if ($rows['bayar']=='2'){ 
						?>
						</td></tr>				
					</tbody>				                  
				  </table>						
				   <!--<center><h5 class="box-title">Detail Pembayaran</h5></center>									   
				 <table class='table table-condensed table-bordered'>                  
					<tbody>										
					<?php foreach ($rec_bayar as $reks){  ?>
						<tr><th scope='row'>Nama Pengirim</th><td><?php echo "$reks[nama_pengirim]"; ?></td>												
						<tr><th scope='row'>Rekening Tujuan</th><td><?php echo "$reks[rekening_pengirim] - $reks[nama_penerima]"; ?></td>												
						<tr><th scope='row'>Tanggal Bayar</th><td><?php echo "$reks[tanggal_bayar]"; ?></td>												
						<tr><th scope='row'>Nominal</th><td><?php echo "$reks[nominal]"; ?></td>
						<tr><th scope='row'>Bukti Pembayaran</th><td><?php echo "<a href='".base_url()."asset/files/".$this->uri->segment(3).".jpg' target='_blank'><img style='border:1px solid #cecece; height:180px' src='".base_url()."asset/files/".$this->uri->segment(3).".jpg'>"; ?></td>					
					<?php }
						
					?>
                  </tbody>
                  </table>-->
				  <center><h5 class="box-title">Detail Produk</h5></center>
                  <table class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th style='width:40px'>No</th>
                        <th>Nama Produk</th>
                        <th>Harga Jual</th>
                        <th>Diskon</th>
                        <th>Jumlah Jual</th>
                        <th>Satuan</th>
                        <th>Sub Total</th>
                      </tr>
                    </thead>
                    <tbody>
                  <?php 
                    $no = 1;
                    foreach ($record as $row){
					$sub_total = ($row['harga_jual']*$row['jumlah'])-$row['diskon'];
					$tot_harga = $sub_total + $tot_harga;
           
                    echo "<tr><td>$no</td>
                              <td>$row[nama_produk]</td>
                              <td>Rp ".rupiah($row['harga_jual'])."</td>
                              <td>Rp ".rupiah($row['diskon'])."</td>
                              <td>$row[jumlah]</td>
                              <td>$row[satuan]</td>
                              <td style='text-align:right'>Rp ".rupiah($sub_total)."</td>
                          </tr>";
                      $no++;
                    }
                    $total = $this->db->query("SELECT sum((a.harga_jual*a.jumlah)-a.diskon) as total FROM rb_penjualan b JOIN `rb_penjualan_detail` a ON b.id_penjualan = a.id_penjualan where b.kode_transaksi='".$this->uri->segment(3)."'")->row_array();
                    $total_belanja = $tot_harga + $total_belanja;
					$total_ongkir = $rows['ongkir'] + $total_ongkir;
					$total_diskon_belanja = $rows['diskon'] + $total_diskon_belanja;
					$total_diskon_ongkir = $rows['diskon_ongkir'] + $total_diskon_ongkir;
					$asr = $asr + $rows['asuransi'];					
                    echo "<tr>
							<td colspan='6'><b>Total Belanja</b></td>
							<td style='text-align:right'>Rp".rupiah($total_belanja)."</td>
							</tr>
							<tr>
							<td colspan='6'><b>Diskon Belanja</b></td>
							<td style='text-align:right;color:red;'>Rp".rupiah($total_diskon_belanja)."</td>
							</tr>
							<tr>
							<td colspan='6'><b>Total Ongkir</b></td>
							<td style='text-align:right'>Rp".rupiah($total_ongkir)."</td>
							</tr>
							<td colspan='6'><b>Diskon Ongkir</b></td>
							<td colspan='6' style='text-align:right;color:red;''>Rp".rupiah($total_diskon_ongkir)."</td>
							</tr>
							<tr>
							<td colspan='6'><b>Biaya Administrasi</b></td>
							<td colspan='6' style='text-align:right'>Rp".rupiah($rows['mdr'])."</td>
							</tr>
							<tr>
							<td colspan='6'><b>Asuransi</b></td>
							<td colspan='6' style='text-align:right'>Rp".rupiah($asr)."</td>
							</tr>
							<tr>
							<td colspan='6'><b>Total Transaksi</b></td>
							<td colspan='6'style='text-align:right'>Rp".rupiah($rows['total_bayar'])."</td>
							</tr>";
                  ?>				  
					</tbody>
					</table>
					
					<?php if ($pemb['status_pembayaran']=='proses refund') { ?>
					
					<hr/>Anda Mempunyai Pengajuan Refund oleh <?php echo "<a href='".base_url().$this->uri->segment(1)."/detail_konsumen/$rows[id_konsumen]'>$rows[nama_lengkap]</a>"; ?> dengan detail berikut :</hr>
					<br/><hr/>
					<center><h5 class="box-title">Detail Refund</h5> 
					<?php 
						$bank = $this->db->query("SELECT * FROM bank where id_bank='".$ref['id_bank']."'")->row_array();
						$attributes = array('class'=>'form-horizontal','role'=>'form');
						echo form_open_multipart('administrator/refund/'.$row[kode_transaksi],$attributes); 
					?>
					</center>
					
					<table class='table table-condensed table-bordered'>                  
					<tbody>	
						<tr><th width="30%" scope='row'>Rekening Penerima</th><td><?php echo "$bank[nama_bank]"; ?> - <strong><?php echo "$ref[no_rekening_penerima]"; ?></strong></td>												
						<tr><th scope='row'>Nama</th><td><?php echo "$ref[nama_penerima]"; ?></td>																						
						<tr><th scope='row'>Waktu Pengajuan</th><td><?php echo "$ref[waktu_pengajuan_refund]"; ?></td>
						<tr><th scope='row'>Remarks</th><td height='80px'><?php echo "$ref[remarks]"; ?></td></tr>
					</tbody>
					</table>
					<?php echo "<hr/>Silahkan untuk <strong> mentransfer uang pengembalian refund sebesar Rp. ".rupiah($rows['total_bayar'])." </strong> sesuai dengan nomor rekening yang tertera diatas, dan <strong> upload bukti transfer </strong></hr> "; ?>
					<br/>
					<br/>
					<table class='table table-condensed table-bordered'>                  
					<tbody>	
						<tr>
							<th width="2%" scope='row'>
								<?php echo "<center>";
								if ($ref['bukti_transfer_refund']!=''){ 
									echo"<img style='border:1px solid #cecece; height:100px' src='".base_url()."asset/bukti_transfer_refund/".$ref['bukti_transfer_refund']."' "; 
								}else{
									echo"<img style='border:1px solid #cecece; height:100px' src='".base_url()."asset/bukti_transfer_refund/img_not_available.jpg' "; 
								} ?>
								</center></th>
							<td width="200px"><input type='file' class='form-control' name='foto' required>Allowed File : .jpeg, jpg, png
							<br/><center><button type='submit' name='submit' class='btn btn-primary'>Upload</button></center></td>
						</tr>
					</table>
					<?php }else{
						
						echo"<hr/>Refund berhasil diproses, dana pengembalian telah dikirimkan dengan detail berikut :<hr/>
							<br/>";	 
					
						$bank = $this->db->query("SELECT * FROM bank where id_bank='".$ref['id_bank']."'")->row_array();
						$attributes = array('class'=>'form-horizontal','role'=>'form');
						echo form_open_multipart('administrator/refund/'.$row[id_penjualan],$attributes); 
					?>
					
					<table class='table table-condensed table-bordered'>                  
					<tbody>	
						<tr><th width='30%' scope='row'>Rekening Penerima</th><td><?php echo "$bank[nama_bank]"; ?> - <strong><?php echo "$ref[no_rekening_penerima]"; ?></strong></td>												
						<tr><th scope='row'>Nama</th><td><?php echo "$ref[nama_penerima]"; ?></td>																						
						<tr><th scope='row'>Waktu Pengajuan</th><td><?php echo "$ref[waktu_pengajuan_refund]"; ?></td>
						<tr><th scope='row'>Status</th><td>Berhasil</td></tr>
					</tbody>
					</table>
					<?php
					}
					?>
				</div>
			</div>
		</div>
		
	<script>
		$(document).ready(function(){
		  for(let i=0; i<4; i++){
		  $('#gambar'+i).click(function(){
			document.getElementById("tampil"+i).src = "<?php echo base_url().'asset/foto_produk/thumb/no-image-icon.gif'; ?>"; 
			document.getElementById('file_lama['+i+']').value =null;
			document.getElementById('userfile['+i+']').value =null;
		  });
		}
		});
	</script>	