<div class="col-xs-12">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"> Pencairan Komisi</h3>
        </div><!-- /.box-header -->
        <?php
        $attributes = array('class' => 'form-horizontal', 'role' => 'form', 'name' => 'demo');
        echo form_open_multipart('administrator/history_komisi', $attributes);
        ?>
        <div class="box-body">

            <div class="col-xs-6" style="margin-bottom:20px; margin-left:2px;">
                <div class="col-xs-4">
                    <label>Nama Konsumen </label>
                </div>
                <div class="col-xs-8" style="margin-bottom:10px;">
                    : <?php echo $record['nama_lengkap']; ?>
                    <input type="hidden" name="id_pencairan_bonus" value="<?php echo $record['id_pencairan_bonus']; ?>">
                </div>

                <div class="col-xs-4">
                    <label>Nomor Rekening </label>
                </div>
                <div class="col-xs-8" style="margin-bottom:10px;">
                    : <?php echo $record['no_rekening']; ?>
                </div>

                <div class="col-xs-4">
                    <label>Tanggal Request </label>
                </div>
                <div class="col-xs-8" style="margin-bottom:10px;">
                    : <?php echo $record['tanggal_request']; ?>
                </div>

                <div class="col-xs-4">
                    <label>Total Pencairan </label>
                </div>
                <div class="col-xs-8" style="margin-bottom:10px;">
                    : <?php echo "Rp." . rupiah($record['jumlah_pencairan']); ?>
                    <input type='hidden' value="<?= $record['jumlah_pencairan'] ?>" class="form-control" placeholder='Jumlah Pencairan' name='jumlah_pencairan' id='jumlah_pencairan'>
                </div>

                <div class="col-xs-4">
                    <button id='smp' type='submit' name='submit' class='btn btn-info'>Cairkan</button>  
                </div>
                <div class="col-xs-8" style="margin-bottom:10px;">
                </div>

            </div>

            <?php echo form_close(); ?>

        </div>
    </div>
</div>