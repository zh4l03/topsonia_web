<div class="col-xs-12">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"> Pencairan Referral</h3>
        </div><!-- /.box-header -->
        <?php 
            $attributes = array('class'=>'form-horizontal','role'=>'form','name'=>'demo');
		    echo form_open_multipart('administrator/history_referral',$attributes);
        ?>
        <div class="box-body">
            
            <div class="col-xs-6" style="margin-bottom:10px; margin-left:2px;">
                <div class="col-xs-4" >
                    <label>Nama Konsumen </label>
                </div>
                <div class="col-xs-8" style="margin-bottom:10px;">
                    &nbsp;&nbsp; : <?php echo $record['nama_lengkap']; ?>
                    <input type="hidden" name="id_konsumen" value="<?php echo $record['id_konsumen']; ?>">
                </div>

                <div class="col-xs-4" >
                    <label>Nama Reseller </label>
                </div>
                <div class="col-xs-8" style="margin-bottom:10px;">
                    &nbsp;&nbsp; : <?php echo $record['nama_reseller']; ?>
                    <input type="hidden" name="id_reseller" value="<?php echo $record['id_reseller']; ?>">
                </div>

                <div class="col-xs-4" >
                    <label>Tahun </label>
                </div>
                <div class="col-xs-8" style="margin-bottom:10px;">
                    &nbsp;&nbsp; : <?php echo $record['tahun']; ?>
                    <input type="hidden" name="tahun" value="<?php echo $record['tahun']; ?>">
                </div>

                <div class="col-xs-4" >
                    <label>Bulan </label>
                </div>
                <div class="col-xs-8" style="margin-bottom:10px;">
                    &nbsp;&nbsp; : <?php echo $record['bulan']; ?>
                    <input type="hidden" name="bulan" value="<?php echo date('m',strtotime($record['bulan'])); ?>">
                </div>

            </div>

            <div class="col-xs-5" style="margin-bottom:10px; margin-left:0px;" >

                <div class="col-xs-4" >
                    <label>Nomor Rekening </label>
                </div>
                <div class="col-xs-8" style="margin-bottom:10px;">
                    : <?php echo $record['no_rekening']; ?>
                </div>

                <div class="col-xs-4" >
                    <label>Total Pencairan </label>
                </div>
                <div class="col-xs-8" style="margin-bottom:10px;">
                    : <?php echo "Rp.".rupiah($record['bonus_referal']); ?>
                </div>

                <div class="col-xs-4" >
                    <label>Sisa Pencairan </label>
                </div>
                <div class="col-xs-8" style="margin-bottom:10px;">
                
                <?php

                $id_reseller = $record['id_reseller'];
                $id_konsumen = $record['id_konsumen'];
                $tahun = $record['tahun'];
                $bulan = date('m',strtotime($record['bulan']));

                $pencairan = $this->db->query("select sum(jumlah_pencairan) as jumlah_pencairan from rb_pencairan_referal where id_reseller='$id_reseller' and id_konsumen='$id_konsumen' and tahun='$tahun' and bulan='$bulan' GROUP BY id_reseller,tahun,bulan ORDER BY id_pencairan_bonus desc");
                $pencairan_cek = $pencairan->num_rows();
                
                if($pencairan_cek>0){

                  $pencairan_detail = $pencairan->row_array();
                  $jumlah_pencairan = "Rp ".rupiah($pencairan_detail['jumlah_pencairan']);
                  $sisa_saldo = "Rp ".rupiah($record['bonus_referal']-$pencairan_detail['jumlah_pencairan']);
                  $sisa_saldo_2 = $record['bonus_referal']-$pencairan_detail['jumlah_pencairan'];
               
                }else{
                  $jumlah_pencairan = "";
                  $sisa_saldo = "Rp ".rupiah($row['bonus_referal']);
                  $sisa_saldo_2 = "Rp ".rupiah($row['bonus_referal']);
                }

                echo ": ". $sisa_saldo;

                echo "<input type='hidden' name='sisa' id='sisa' value='$sisa_saldo_2'>";

                ?>

                </div>

                <div class="col-xs-4" >
                    &nbsp;&nbsp;
                </div>
                <div class="col-xs-8" style="margin-bottom:10px;">
                    &nbsp;&nbsp;  
                </div>

                <div class="col-xs-4" >
                    &nbsp;&nbsp;
                </div>
                <div class="col-xs-8" style="margin-bottom:10px;">
                    &nbsp;&nbsp;
                </div>

            </div>
            
            

            <div class="col-xs-2" style="margin-bottom:10px; margin-left:14px;">
                <label>Jumlah Pencairan</label>
            </div>
        
            <div class="col-xs-9" style="margin-bottom:10px;">
                <input type='number' class="form-control" placeholder='Jumlah Pencairan' name='jumlah_pencairan' id='jumlah_pencairan' onchange='cek_sisa();' style="width: 200px;" required>
            </div>

           

            
        </div>

        <div class='box-footer' style="margin-bottom:10px; margin-left:30px;">
            <button id='smp' type='submit' name='submit' class='btn btn-info' style='display: block;'>Simpan</button>
            <button id='cek_s' type='button' name='btn_cek' class='btn btn-info' style='display: none;' onclick="cek_sisa()">Simpan</button>
        </div>

        <?php echo form_close(); ?>
    
    </div>
</div>

<script>
    function cek_sisa(){
        
        let sisa_saldo = document.getElementById('sisa').value;
        let jumlah_pencairan = document.getElementById('jumlah_pencairan').value;
        
        // alert(jumlah_pencairan);
        // alert(sisa_saldo);

        if(parseInt(jumlah_pencairan)>parseInt(sisa_saldo)){
            
            document.getElementById("cek_s").style.display = "block";
			document.getElementById("smp").style.display = "none";
            alert('Mohon Maaf Jumlah Pencairan Lebih Besar Dari Pada Sisa Saldo');

        }else if(parseInt(jumlah_pencairan)<10000){

            document.getElementById("cek_s").style.display = "block";
			document.getElementById("smp").style.display = "none";
            alert('Jumlah Pencairan Min 10000 Rupiah');

        }else{
            document.getElementById("cek_s").style.display = "none";
			document.getElementById("smp").style.display = "block";

        }
        
    }

</script>