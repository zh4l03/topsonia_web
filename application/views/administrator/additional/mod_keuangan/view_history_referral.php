            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">History Pembayaran Referral</h3>
                </div><!-- /.box-header -->
                <div class="box-body">

                <div class="col-xs-4" style="margin-bottom:10px; margin-left:2px;">
                    Nama kosumen :
                    <select name='id_konsumen' id='id_konsumen' onchange='filter_referal();'>
                      <option value="p">Pilih Konsumen</option>
                      <?php
                      $konsumen = $this->db->query("select b.id_konsumen,b.nama_lengkap from rb_reseller a join rb_konsumen b on a.referral=b.kode_referall join rb_penjualan c on a.id_reseller=c.id_penjual where c.proses='1' GROUP BY b.id_konsumen");
                      $konsumen_cek = $konsumen->num_rows();
                      if ($konsumen_cek>0) {

                        $konsumen_detail = $konsumen->result_array();

                        foreach($konsumen_detail as $kos){
                          echo "<option value='$kos[id_konsumen]'>$kos[nama_lengkap]</option>";
                        }
                       
                      } else {
                       
                      }
                      ?>
                    </select>
                  </div>

                  <div class="col-xs-2" style="margin-bottom:10px; margin-left:2px;">
                    Tahun :
                    <select name='tahun' id='tahun' onchange='filter_referal();'>
                      <option value="p">Pilih Tahun</option>
                      <?php
                      $tahun = $this->db->query("select MIN(YEAR(waktu_order)) as min_tahun, MAX(YEAR(waktu_order)) as max_tahun from rb_penjualan where waktu_order IS NOT NULL and proses='1'")->row_array();
                      if ($tahun['min_tahun'] == $tahun['max_tahun']) {
                        echo "<option>$tahun[min_tahun]</option>";
                      } else {
                        for ($y = $tahun['min_tahun']; $y <= $tahun['max_tahun']; $y++) {
                          echo "<option>$y</option>";
                        }
                      }
                      ?>
                    </select>
                  </div>

                  <div class="col-xs-3" style="margin-bottom:10px;">
                    Bulan :
                    <select name='bulan' id='bulan' onchange='filter_referal();'>
                      <option value="p">Pilih Bulan</option>
                      <option value='1'>Januari</option>
                      <option value='2'>Februari</option>
                      <option value='3'>Maret</option>
                      <option value='4'>April</option>
                      <option value='5'>Mei</option>
                      <option value='6'>Juni</option>
                      <option value='7'>Juli</option>
                      <option value='8'>Agustus</option>
                      <option value='9'>September</option>
                      <option value='10'>Oktober</option>
                      <option value='11'>November</option>
                      <option value='12'>Desember</option>
                    </select>
                  </div>
                  
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th style='width:20px'>No</th>
                        <th>Nama Reseller</th>
                        <th>Nama Konsumen</th>
                        <th>Bulan</th>
                        <th>Tahun</th>
                        <th>Tanggal Penarikan</th>
                        <th>Jumlah Penarikan</th>
                        <th>Sisa Saldo</th>
                        <th style='width:50px'>Action</th>
                      </tr>
                    </thead>
                    <tbody id='tabel_body'>
                      <?php
                      $no = 1;
                      foreach ($record->result_array() as $row) {
                        $id_reseller = $row['id_reseller'];
                        $id_konsumen = $row['id_konsumen'];
                        $tahun = $row['tahun'];
                        $bulan = date('m',strtotime($row['bulan']));

                        $pencairan = $this->db->query("select sum(jumlah_pencairan) as jumlah_pencairan, max(tanggal_pencairan) as tanggal_pencairan from rb_pencairan_referal where id_reseller='$id_reseller' and id_konsumen='$id_konsumen' and tahun='$tahun' and bulan='$bulan' GROUP BY id_reseller,tahun,bulan ORDER BY id_pencairan_bonus desc");
                        $pencairan_cek = $pencairan->num_rows();
                        
                        if($pencairan_cek>0){

                          $pencairan_detail = $pencairan->row_array();
                          $tanggal_pencairan = $pencairan_detail['tanggal_pencairan'];
                          $jumlah_pencairan = "Rp ".rupiah($pencairan_detail['jumlah_pencairan']);
                          $sisa_saldo = "Rp ".rupiah($row['bonus_referal']-$pencairan_detail['jumlah_pencairan']);
                       
                        }else{
                          $tanggal_pencairan = "";
                          $jumlah_pencairan = "";
                          $sisa_saldo = "Rp ".rupiah($row['bonus_referal']);
                        }

                        echo "<tr><td>$no</td>
                              <td>$row[nama_reseller]</td>
                              <td>$row[nama_lengkap]</td>
                              <td>$row[bulan]</td>
                              <td>$row[tahun]</td>
                              <td>$tanggal_pencairan</td>
                              <td>$jumlah_pencairan</td>
                              <td>$sisa_saldo</td>
                              <td><center>
                              <a class='btn btn-warning btn-xs' title='Pencairan Bonus' href='" . base_url() . "administrator/pencairan_referral/$row[id_reseller]/$row[id_konsumen]/$row[bulan]/$row[tahun]'><span class='fa fa-money'></span></a>
                              </center></td>
                          </tr>";
                        $no++;
                      }
                      ?>
                    </tbody>
                  </table>
                </div>

                <script>
                  function filter_referal() {
                    let thn = document.getElementById('tahun').value;
                    let bln = document.getElementById('bulan').value;
                    let konsumen_id = document.getElementById('id_konsumen').value;

                    $.ajax({
                      type: 'POST',
                      url: '<?php echo base_url(); ?>administrator/filter_referal',
                      dataType: 'JSON',
                      data: {
                        tahun: thn,
                        bulan: bln,
                        id_konsumen : konsumen_id
                      },
                      success: function(data) {
                        if (data.result == '0') {
                          alert('Data Not Found');
                        } else {
                          document.getElementById('tabel_body').innerHTML = data.filter;
                          // alert(data.result);
                        }
                      },
                      error: function(data) {
                        alert('Data Not Found');
                      },
                    });
                    // alert(tahun);
                    // alert(bulan);
                  }
                </script>