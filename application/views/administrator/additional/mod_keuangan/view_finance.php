<script type="text/javascript">
  $(document).ready(function() {
    $('#example').DataTable( {
      "scrollX": true,
      "scrollY": 500,
      "paging": false,
      "searching": true
    } );
  } );

</script>
<div class='col-md-12'>
  <div class='box box-info'>
    <div class='box-body'>
      <div class='panel-body'>
        <ul id='myTabs' class='nav nav-tabs' role='tablist'>
          <li role='presentation' class='active'><a href='#finance' id='finance-tab' role='tab' data-toggle='tab' aria-controls='finance' aria-expanded='true'>Laporan Transaksi <?php echo $tgl_awal." ... ".$tgl_akhir; ?></a></li>
          <!-- <li role='presentation' class=''><a href='#referall' role='tab' id='referall-tab' data-toggle='tab' aria-controls='referall' aria-expanded='false'>Laporan Referall</a></li> -->
        </ul><br>

        <div id='myTabContent' class='tab-content'>
          <div role='tabpanel' class='tab-pane fade active in' id='finance' aria-labelledby='finance-tab'>

            <div class='col-md-12'>
              <div class='box box-info'>
                <div class='box-header with-border'>
                  <h3 class='box-title'><span style="color: blue;"><b>Laporan Transaksi </b></span></h3>
                </div>
                <div class='box-body'>

                  <div class='col-md-12'>
                    <table width="100%" border="0px">
                      <tr>
                        <?php
                        $attributes = array('class'=>'form-horizontal','role'=>'form');
                        echo form_open_multipart('administrator/finance', $attributes); 
                        ?>
                        <form action="finance?bln=<?php echo $bulan2; ?>" method="POST">
                          <td style="font-weight: bold;">Filter &nbsp;&nbsp;&nbsp;&nbsp;</td>
                          <td style="text-align: right;" width="200px">

                            <select class="form-control" name="merchant">
                              <option value="">Semua Merchant</option>
                              <?php
                              $list_merchant = $this->db->query("SELECT * FROM rb_reseller ORDER BY nama_reseller ASC")->result_array();
                              $a=1;
                              foreach ($list_merchant as $list) {
                                ?>
                                <option value="<?php echo $list['id_reseller']; ?>"><?php echo $list['nama_reseller']; ?></option>
                                <?php
                              }
                              ?>
                            </select>

                          </td>

                          <td width="170px" style="padding-left: 20px;">
                            <select class="form-control" name="pembayaran">
                              <option value=""><span class='glyphicon glyphicon-list'></span> Semua Pembayaran</option>
                              <?php
                              $list_pembayaran = $this->db->query("SELECT DISTINCT kategori_pembayaran FROM `tabel_mdr` ORDER BY kategori_pembayaran")->result_array();
                              $a=1;
                              foreach ($list_pembayaran as $list) {
                                ?>
                                <option value="<?php echo $list['kategori_pembayaran']; ?>">
                                  <?php if ($list['kategori_pembayaran'] == 'qris') { echo "Qris"; }elseif($list['kategori_pembayaran'] == 'midtrans'){ echo "Midtrans Payment"; }elseif($list['kategori_pembayaran'] == 'manual-transfer'){ echo "Manual Transfer"; } ?>
                                </option>
                                <?php
                              }
                              ?>
                            </select>
                          </td>

                          <td width="110px" style="padding-left: 20px"><input type="text" placeholder="Tanggal Awal" name="tanggal_awal"  class="form-control datepicker" /></td>
                          <td style="text-align:center;"> s/d </td>
                          <td width="110px" ><input type="text" name="tanggal_akhir" placeholder="Tanggal Akhir" class="form-control datepicker" /></td>

                          <td width="80px" style="text-align:center;"><button type="submit" name="submit" class="btn btn-info btn-sm"><span class='glyphicon glyphicon-search'></span> Search</button></td>

                          <?php
                          echo form_close();
                          ?>

                          <td style="text-align: right;">&nbsp;
                            &nbsp;
                            <span class='dropdown'>
                              <button class='btn btn-info btn-sm dropdown-toggle' type='button' style='margin-right:5px' data-toggle='dropdown' ><span class='glyphicon glyphicon-export'></span> Export</button> 
                              <ul class="dropdown-menu">
                                <li>
                                  <?php
                                  if ($reseler != '') {
                                    ?>
                                    <a href="<?php echo base_url(); ?>administrator/finance_excel/<?php echo $merchant; ?>/<?php echo $pembayaran; ?>/<?php echo $tanggal_awal; ?>/<?php echo $tanggal_akhir; ?>" target="_blank">Excel</a>
                                    <?php
                                  }else{
                                    ?>
                                    <a href="<?php echo base_url(); ?>administrator/finance_excel/<?php echo $merchant; ?>/<?php echo $pembayaran; ?>/<?php echo $tanggal_awal; ?>/<?php echo $tanggal_akhir; ?>" target="_blank">Excel</a>
                                  <?php } ?>
                                </li>
                                <li>
                                  <?php
                                  if ($reseler != '') {
                                    ?>
                                    <a href="<?php echo base_url(); ?>administrator/finance_pdf/<?php echo $merchant; ?>/<?php echo $pembayaran; ?>/<?php echo $tanggal_awal; ?>/<?php echo $tanggal_akhir; ?>" target="_blank">PDF</a>
                                    <?php
                                  }else{
                                    ?>
                                    <a href="<?php echo base_url(); ?>administrator/finance_pdf/<?php echo $merchant; ?>/<?php echo $pembayaran; ?>/<?php echo $tanggal_awal; ?>/<?php echo $tanggal_akhir; ?>" target="_blank">PDF</a>
                                  <?php } ?>
                                </li>
                              </ul>
                            </span>
                            <!-- <a href="javascript:history.go(-1)" class="btn btn-info btn-sm"><span class='glyphicon glyphicon-circle-arrow-left'></span> Kembali</a> -->
                          </td>
                        </tr>
                      </table>
                    </div>

                  </div>
                </div>
              </div>


              <div class='col-md-12'>
                <div class='box box-info'>
                <!-- <div class='box-header with-border'>
                  <h3 class='box-title'><span style="color: blue;"><b>Daftar Frequently Asked Question</b></span></h3>
                </div> -->
                <div class="box-body">
                  <table id="example" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th style='width:30px; text-align: center;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No</th>
                        <th>Merchant</th>
                        <th>Kode Transaksi</th>
                        <th>Pembayaran</th>
                        <th>Waktu Order</th>
                        <th>Harga Publish</th>
                        <th>Diskon</th>
                        <th>Total Harga Jual</th>
                        <th>Total Merchant</th>
                        <!-- <th>Total Topsonia Gross</th> -->
                        <th>Merchant Discount Rate<br>(MDR)</th>
                        <th>Total Topsonia</th>
                        <th>PPN</th>
                        <th>Ongkos Kirim</th>
                        <th>Diskon Ongkir</th>
                        <th>Total Ongkir Net</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      $no = 1;
                      foreach ($detail_perbulan->result_array() as $rows) {
                        ?>
                        <tr>
                          <td style='text-align: center;'><?php echo $no; ?></td>
                          <td><?php echo $rows['nama_reseller']; ?></td>
                          <td><?php echo $rows['kode_transaksi']; ?></td>
                          <td><?php echo $rows['kategori_pembayaran']; ?></td>
                          <td><?php echo date('d M Y', strtotime($rows["tanggal_order"])); ?></td>
                          <td><?php echo 'Rp&nbsp;'.rupiah($rows['harga_publish']); ?></td>
                          <td><?php echo 'Rp&nbsp;'.rupiah($rows['diskon']); ?></td>
                          <td><?php echo 'Rp&nbsp;'.rupiah($rows['harga_jual']); ?></td>
                          <td><?php echo 'Rp&nbsp;'.rupiah($rows['harga_merchant']); ?></td>
                          <!-- <td><?php //echo 'Rp&nbsp;'.rupiah($rows['komisi_topsonia']); ?></td> -->
                          <td><?php echo 'Rp&nbsp;'.rupiah($rows['mdr_net']); ?></td>
                          <td><?php echo 'Rp&nbsp;'.rupiah($rows['topsonia_net']); ?></td>
                          <td><?php echo 'Rp&nbsp;'.rupiah($rows['pajak']); ?></td>
                          <td><?php echo 'Rp&nbsp;'.rupiah($rows['ongkir']); ?></td>
                          <td><?php echo 'Rp&nbsp;'.rupiah($rows['diskon_ongkir']); ?></td>
                          <td><?php echo 'Rp&nbsp;'.rupiah($rows['ongkir_net']); ?></td>
                          <td style='text-align: center;'>
                            <button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#myModal<?php echo $rows['id_penjualan']; ?>"><span class='glyphicon glyphicon-search'></span> Detail
                            </button>

                            <!-- <a class="btn btn-info btn-xs" href=""><span class='glyphicon glyphicon-search'></span></a> -->
                          </td>

                        </tr>

                        <?php
                        $no++;
                      }
                      ?>
                    </tbody>
                    <tr style="background-color: yellow;">
                      <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
                      <td style='text-align: right;'><b>TOTAL</b></td>
                      <td><b><?php echo 'Rp&nbsp;'.rupiah($total_perbulan['total_harga_publish']); ?></b></td>
                      <td><b><?php echo 'Rp&nbsp;'.rupiah($total_perbulan['total_diskon']); ?></b></td>
                      <td><b><?php echo 'Rp&nbsp;'.rupiah($total_perbulan['total_harga_jual']); ?></b></td>
                      <td><b><?php echo 'Rp&nbsp;'.rupiah($total_perbulan['total_harga_merchant']); ?></b></td>
                      <!-- <td><b><?php //echo 'Rp&nbsp;'.rupiah($total_perbulan['total_komisi_topsonia']); ?></b></td> -->
                      <td><b><?php echo 'Rp&nbsp;'.rupiah($total_perbulan['total_total_mdr']); ?></b></td>
                      <td><b><?php echo 'Rp&nbsp;'.rupiah($total_perbulan['total_topsonia_net']); ?></b></td>
                      <td><b><?php echo 'Rp&nbsp;'.rupiah($total_perbulan['total_total_pajak']); ?></b></td>
                      <td><b><?php echo 'Rp&nbsp;'.rupiah($total_perbulan['total_ongkir']); ?></b></td>
                      <td><b><?php echo 'Rp&nbsp;'.rupiah($total_perbulan['total_diskon_ongkir']); ?></b></td>
                      <td><b><?php echo 'Rp&nbsp;'.rupiah($total_perbulan['total_ongkir_net']); ?></b></td>
                      <td>&nbsp;</td>
                    </tr>
                  </table>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>


  <script>
    $(document).ready(function(){
      var id;
      $(myTabs).click(function(e) {
        e.preventDefault();
      // $(this).tab('show');
      
      $("ul.nav-tabs > li > a").on("shown.bs.tab", function(e) {
        id = $(e.target).attr("href").substr(1);
        window.history.replaceState(null, null, "?tab="+id);
      });
    });

      const urlParams = new URLSearchParams(window.location.search);
      const tabID = urlParams.get('tab');

      if(tabID != null){
        $('#myTabs a[href="#' + tabID +'"]').tab('show');
      }else{
        $('#myTabs a[href="#faq"]').tab('show');
      }

    });
  </script>
