<?php

$bulan2 = $bulan['bulan'];
if ($bulan2 !='') {

	if ($bulan2 == 1) {
		$bln = "Januari";
	}elseif ($bulan2 == 2) {
		$bln =  "Februari";
	}elseif ($bulan2 == 3) {
		$bln =  "Maret";
	}elseif ($bulan2 == 4) {
		$bln =  "April";
	}elseif ($bulan2 == 5) {
		$bln =  "Mei";
	}elseif ($bulan2 == 6) {
		$bln =  "Juni";
	}elseif ($bulan2 == 7) {
		$bln =  "Juli";
	}elseif ($bulan2 == 8) {
		$bln =  "Agustus";
	}elseif ($bulan2 == 9) {
		$bln =  "September";
	}elseif ($bulan2 == 10) {
		$bln =  "Oktober";
	}elseif ($bulan2 == 11) {
		$bln =  "November";
	}elseif ($bulan2 == 12) {
		$bln =  "Desember";
	}
}else{
	$bln =  "-";
}

if ($resel != '') {
	$r = '<tr><td align="center"><h2>Merchant : '.$resel['nama_reseller'].'</h2></td></tr>';
}else{
	$r = '&nbsp;';
}

if ($id_r != "") {
	if ($id_r > -1 ) {
		?>

		<table>
			<tr><td align="center"><h1>Data Penjualan Bulan <?php echo $bln; ?></h1></td></tr>
			<?php echo $r; ?>
			<tr><td>&nbsp;</td></tr>
			<tr><td>
				<table style="border:1px solid; font-size: 15px;" cellpadding="3px" cellspacing="0" width="1084.7px" height="auto" >
					<thead>
						<tr>
							<th align="center" style='width:30px; background-color: grey; border-bottom: 1px solid #000;'>&nbsp;&nbsp;No&nbsp;&nbsp;</th>
							<th align="left" style="background-color: grey; border-bottom: 1px solid #000; border-left: 1px solid #000;">Merchant</th>
							<th align="center" style="background-color: grey; border-bottom: 1px solid #000; border-left: 1px solid #000;">&nbsp;&nbsp;Waktu Order&nbsp;&nbsp;</th>
							<th align="right" style="background-color: grey; border-bottom: 1px solid #000; border-left: 1px solid #000;">&nbsp;&nbsp;Harga Publish&nbsp;&nbsp;</th>
							<th align="right" style="background-color: grey; border-bottom: 1px solid #000; border-left: 1px solid #000;">&nbsp;&nbsp;Diskon&nbsp;&nbsp;</th>
							<th align="right" style="background-color: grey; border-bottom: 1px solid #000; border-left: 1px solid #000;">&nbsp;&nbsp;Harga Jual&nbsp;&nbsp;</th>
							<th align="right" style="background-color: grey; border-bottom: 1px solid #000; border-left: 1px solid #000;">&nbsp;&nbsp;Harga<br>Merchant</th>
					<!-- <th align="center" style=" background-color: grey; border-bottom: 1px solid #000; border-left: 1px solid #000;">&nbsp;&nbsp;Topsonia&nbsp;&nbsp;<br>Gross</th> 
						<th align="right" style="background-color: grey; border-bottom: 1px solid #000; border-left: 1px solid #000;">&nbsp;&nbsp;MDR&nbsp;&nbsp;</th>-->
						<th align="right" style="background-color: grey; border-bottom: 1px solid #000; border-left: 1px solid #000;">&nbsp;&nbsp;Topsonia&nbsp;&nbsp;</th>
						<th align="right" style="background-color: grey; border-bottom: 1px solid #000; border-left: 1px solid #000;">&nbsp;&nbsp;PPN&nbsp;&nbsp;</th>
						<th align="right" style="background-color: grey; border-bottom: 1px solid #000; border-left: 1px solid #000;">&nbsp;&nbsp;Asuransi&nbsp;&nbsp;</th>
						<!-- <th style="background-color: grey; border-bottom: 1px solid #000; border-left: 1px solid #000;">&nbsp;&nbsp;Diskon Ongkir&nbsp;&nbsp;</th> -->
						<th align="right" style="background-color: grey; border-bottom: 1px solid #000; border-left: 1px solid #000;">&nbsp;&nbsp;Ongkir Net&nbsp;&nbsp;</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$no = 1;
					foreach ($detail_perbulan_merchant->result_array() as $rows) {
						?>

						<tr>
							<td align="center" style='border-bottom: 1px solid #000; border-right: 1px solid #000'>&nbsp;&nbsp;<?php echo $no; ?></td>
							<td align="left" style="border-bottom: 1px solid #000; border-right: 1px solid #000"><?php echo $rows['nama_reseller']; ?></td>
							<?php
						// if ($resel != '') {
						// 	echo "<td style='text-align: left; border-bottom: 1px solid #000; border-right: 1px solid #000'>&nbsp;&nbsp;".$rows['kode_transaksi']."</td>";
						// }else{
						// 	echo "<td style='text-align: left; border-bottom: 1px solid #000; border-right: 1px solid #000'>&nbsp;&nbsp;".$rows['nama_reseller']."</td>";
						// }
							?>
							<!-- <td><?php //echo $rows['nama_reseller']; ?></td> -->
							<td align="center" style='border-bottom: 1px solid #000; border-right: 1px solid #000'>&nbsp;<?php echo date('d M Y', strtotime($rows["tanggal_order"])); ?></td>

							<td style='border-bottom: 1px solid #000; border-right: 1px solid #000'><table style="border:0px solid; font-size: 15px" cellspacing="0" width="100%"><tr><td>&nbsp;&nbsp;Rp&nbsp;</td><td align="right"><?php echo rupiah($rows['harga_awal']); ?></td></tr></table></td>

							<td align="right" style='border-bottom: 1px solid #000; border-right: 1px solid #000'><table style="border:0px solid; font-size: 15px" cellspacing="0" width="100%"><tr><td>&nbsp;Rp&nbsp;</td><td align="right"><?php echo rupiah($rows['diskaun']); ?></td></tr></table></td>

							<td align="right" style='border-bottom: 1px solid #000; border-right: 1px solid #000'><table style="border:0px solid; font-size: 15px" cellspacing="0" width="100%"><tr><td>&nbsp;Rp&nbsp;</td><td align="right"><?php echo rupiah($rows['harga_fix']); ?></td></tr></table></td>

							<td align="right" style='border-bottom: 1px solid #000; border-right: 1px solid #000'><table style="border:0px solid; font-size: 15px" cellspacing="0" width="100%"><tr><td>&nbsp;Rp&nbsp;</td><td align="right"><?php echo rupiah($rows['komisi_merchant']); ?></td></tr></table></td>

						<!-- <td style='border-bottom: 1px solid #000; border-right: 1px solid #000'><table style="border:0px solid; font-size: 15px" cellspacing="0" width="100%"><tr><td>&nbsp;Rp&nbsp;</td><td align="right"><?php// echo rupiah($rows['komisi_topsonia']); ?></td></tr></table></td> 

							<td align="right" style='border-bottom: 1px solid #000; border-right: 1px solid #000'><table style="border:0px solid; font-size: 15px" cellspacing="0" width="100%"><tr><td>&nbsp;Rp&nbsp;</td><td align="right"><?php// echo rupiah($rows['mdr']); ?></td></tr></table></td>-->

							<td align="right" style='border-bottom: 1px solid #000; border-right: 1px solid #000'><table style="border:0px solid; font-size: 15px" cellspacing="0" width="100%"><tr><td>&nbsp;Rp&nbsp;</td><td align="right"><?php echo rupiah($rows['topsonia_gross']); ?></td></tr></table></td>

							<td align="right" style='border-bottom: 1px solid #000; border-right: 1px solid #000'><table style="border:0px solid; font-size: 15px" cellspacing="0" width="100%"><tr><td>&nbsp;Rp&nbsp;</td><td align="right"><?php echo rupiah($rows['pajak']); ?></td></tr></table></td>

							<td align="right" style='border-bottom: 1px solid #000; border-right: 1px solid #000'><table style="border:0px solid; font-size: 15px" cellspacing="0" width="100%"><tr><td>&nbsp;Rp&nbsp;</td><td align="right"><?php echo rupiah($rows['asurans']); ?></td></tr></table></td>

						<!-- <td align="right" style='border-bottom: 1px solid #000; border-right: 1px solid #000'><table style="border:0px solid; font-size: 15px" cellspacing="0" width="100%"><tr><td>&nbsp;Rp&nbsp;</td><td align="right"><?php// echo rupiah($rows['ongkir']); ?></td></tr></table></td>
							<td align="right" style='border-bottom: 1px solid #000; border-right: 1px solid #000'><table style="border:0px solid; font-size: 15px" cellspacing="0" width="100%"><tr><td>&nbsp;Rp&nbsp;</td><td align="right"><?php// echo rupiah($rows['diskon_ongkir']); ?></td></tr></table></td> -->

							<td align="right" style='border-bottom: 1px solid #000;'><table style="border:0px solid; font-size: 15px" cellspacing="0" width="100%"><tr><td>&nbsp;Rp&nbsp;</td><td align="right"><?php echo rupiah($rows['sub_ongkir_net']); ?></td></tr></table></td>
						</tr>

						<?php
						$no++;
					}
					?>
				</tbody>
				<tr>
					<td style=' border-right: 1px solid #000' colspan="<?php if ($resel != '') { echo "3"; }else{ echo "3"; } ?>"><b>TOTAL</b></td>
					<td style=' border-right: 1px solid #000'><table style="border:0px solid; font-size: 15px" cellspacing="0" width="100%"><tr><td>&nbsp;Rp&nbsp;</td><td align="right"><b><?php echo rupiah($total_perbulan['total_harga_publish']); ?></b></td></tr></table></td>
					<td style=' border-right: 1px solid #000'><table style="border:0px solid; font-size: 15px" cellspacing="0" width="100%"><tr><td>&nbsp;Rp&nbsp;</td><td align="right"><b><?php echo rupiah($total_perbulan['total_diskon']); ?></b></td></tr></table></td>
					<td style=' border-right: 1px solid #000'><table style="border:0px solid; font-size: 15px" cellspacing="0" width="100%"><tr><td>&nbsp;Rp&nbsp;</td><td align="right"><b><?php echo rupiah($total_perbulan['total_harga_jual']); ?></b></td></tr></table></td>
					<td style=' border-right: 1px solid #000'><table style="border:0px solid; font-size: 15px" cellspacing="0" width="100%"><tr><td>&nbsp;Rp&nbsp;</td><td align="right"><b><?php echo rupiah($total_perbulan['total_harga_merchant']); ?></b></td></tr></table></td>
				<!-- <td style=' border-right: 1px solid #000'><table style="border:0px solid; font-size: 15px" cellspacing="0" width="100%"><tr><td>&nbsp;Rp&nbsp;</td><td align="right"><b><?php //echo rupiah($total_perbulan['total_komisi_topsonia']); ?></b></td></tr></table></td> 
					<td style=' border-right: 1px solid #000'><table style="border:0px solid; font-size: 15px" cellspacing="0" width="100%"><tr><td>&nbsp;Rp&nbsp;</td><td align="right"><b><?php// echo rupiah($total_perbulan['total_mdr']); ?></b></td></tr></table></td>-->
					<td style=' border-right: 1px solid #000'><table style="border:0px solid; font-size: 15px" cellspacing="0" width="100%"><tr><td>&nbsp;Rp&nbsp;</td><td align="right"><b><?php echo rupiah($total_perbulan['total_topsonia_net']); ?></b></td></tr></table></td>
					<td style=' border-right: 1px solid #000'><table style="border:0px solid; font-size: 15px" cellspacing="0" width="100%"><tr><td>&nbsp;Rp&nbsp;</td><td align="right"><b><?php echo rupiah($total_perbulan['total_pajak']); ?></b></td></tr></table></td>
					<td style=' border-right: 1px solid #000'><table style="border:0px solid; font-size: 15px" cellspacing="0" width="100%"><tr><td>&nbsp;Rp&nbsp;</td><td align="right"><b><?php echo rupiah($total_perbulan['total_asuransi']); ?></b></td></tr></table></td>
				<!-- <td style=' border-right: 1px solid #000'><table style="border:0px solid; font-size: 15px" cellspacing="0" width="100%"><tr><td>&nbsp;Rp&nbsp;</td><td align="right"><b><?php //echo rupiah($total_perbulan['total_ongkir']); ?></b></td></tr></table></td>
					<td style=' border-right: 1px solid #000'><table style="border:0px solid; font-size: 15px" cellspacing="0" width="100%"><tr><td>&nbsp;Rp&nbsp;</td><td align="right"><b><?php// echo rupiah($total_perbulan['total_diskon_ongkir']); ?></b></td></tr></table></td> -->
					<td style=''><table style="border:0px solid; font-size: 15px" cellspacing="0" width="100%"><tr><td align="center">&nbsp;Rp&nbsp;</td><td align="right"><b><?php echo rupiah($total_perbulan['total_ongkir_net']); ?></b></td></tr></table></td>

				</tr>
			</table>
		</td></tr>
	</table>
	<?php
}
}else{
	?>
	<table>
		<tr><td align="center" style=""><h1>Data Penjualan Bulan <?php echo $bln; ?></h1></td></tr>
		<?php echo $r; ?>
		<tr><td>&nbsp;</td></tr>
		<tr><td>
			<table style="border:1px solid; font-size: 15px;" cellpadding="3px" cellspacing="0" width="1084.7px" height="auto" >
				<thead>
					<tr>
						<th align="center" style='width:30px; background-color: grey; border-bottom: 1px solid #000;'>&nbsp;&nbsp;No&nbsp;&nbsp;</th>
						<th align="center" style="background-color: grey; border-bottom: 1px solid #000; border-left: 1px solid #000;">Kode Transaksi</th>
						<?php
					// if ($resel != '') {
					// 	echo "<th align="center"background-color: grey; border-bottom: 1px solid #000; border-left: 1px solid #000;'>&nbsp;&nbsp;Kode Transaksi&nbsp;&nbsp;</th>";
					// }else{
					// 	echo "<th align="center"background-color: grey; border-bottom: 1px solid #000; border-left: 1px solid #000;'>&nbsp;&nbsp;Merchant&nbsp;&nbsp;</th>";
					// }
						?>
						<!-- <th>Merchant</th> -->
						<th align="center" style="background-color: grey; border-bottom: 1px solid #000; border-left: 1px solid #000;">&nbsp;&nbsp;Waktu Order&nbsp;&nbsp;</th>
						<th align="right" align="center" style=" background-color: grey; border-bottom: 1px solid #000; border-left: 1px solid #000;">&nbsp;&nbsp;Harga Publish&nbsp;&nbsp;</th>
						<th align="right" style="background-color: grey; border-bottom: 1px solid #000; border-left: 1px solid #000;">&nbsp;&nbsp;Diskon&nbsp;&nbsp;</th>
						<th align="right" style="background-color: grey; border-bottom: 1px solid #000; border-left: 1px solid #000;">&nbsp;&nbsp;Harga Jual&nbsp;&nbsp;</th>
						<th align="right" style="background-color: grey; border-bottom: 1px solid #000; border-left: 1px solid #000;">&nbsp;&nbsp;Harga<br>Merchant</th>
						<!-- <th align="center" style=" background-color: grey; border-bottom: 1px solid #000; border-left: 1px solid #000;">&nbsp;&nbsp;Topsonia&nbsp;&nbsp;<br>Gross</th> -->
						<th align="right" style="background-color: grey; border-bottom: 1px solid #000; border-left: 1px solid #000;">&nbsp;&nbsp;MDR&nbsp;&nbsp;</th>
						<th align="right" style="background-color: grey; border-bottom: 1px solid #000; border-left: 1px solid #000;">&nbsp;&nbsp;Topsonia&nbsp;&nbsp;</th>
						<th align="right" style="background-color: grey; border-bottom: 1px solid #000; border-left: 1px solid #000;">&nbsp;&nbsp;PPN&nbsp;&nbsp;</th>
						<th align="right" style="background-color: grey; border-bottom: 1px solid #000; border-left: 1px solid #000;">&nbsp;&nbsp;Asuransi&nbsp;&nbsp;</th>
						<!-- <th style="background-color: grey; border-bottom: 1px solid #000; border-left: 1px solid #000;">&nbsp;&nbsp;Diskon Ongkir&nbsp;&nbsp;</th> -->
						<th align="right" style="background-color: grey; border-bottom: 1px solid #000; border-left: 1px solid #000;">&nbsp;&nbsp;Ongkir Net&nbsp;&nbsp;</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$no = 1;
					foreach ($detail_perbulan->result_array() as $rows) {
						?>

						<tr>
							<td align="center" align="center" style='border-bottom: 1px solid #000; border-right: 1px solid #000'>&nbsp;&nbsp;<?php echo $no; ?></td>
							<td align="center" style=" border-bottom: 1px solid #000; border-right: 1px solid #000"><?php echo $rows['kode_transaksi']; ?></td>
							<?php
						// if ($resel != '') {
						// 	echo "<td align="center"border-bottom: 1px solid #000; border-right: 1px solid #000'>&nbsp;&nbsp;".$rows['kode_transaksi']."</td>";
						// }else{
						// 	echo "<td align="center"border-bottom: 1px solid #000; border-right: 1px solid #000'>&nbsp;&nbsp;".$rows['nama_reseller']."</td>";
						// }
							?>
							<!-- <td><?php //echo $rows['nama_reseller']; ?></td> -->
							<td align="center" style='border-bottom: 1px solid #000; border-right: 1px solid #000'>&nbsp;<?php echo date('d M Y', strtotime($rows["tanggal_order"])); ?></td>
							<td align="right" style='border-bottom: 1px solid #000; border-right: 1px solid #000'><table style="border:0px solid; font-size: 15px" cellspacing="0" width="100%"><tr><td align="center">&nbsp;&nbsp;Rp&nbsp;</td><td align="right"><?php echo rupiah($rows['harga_publish']); ?></td></tr></table></td>
							<td align="right" style='border-bottom: 1px solid #000; border-right: 1px solid #000'><table style="border:0px solid; font-size: 15px" cellspacing="0" width="100%"><tr><td align="center">&nbsp;Rp&nbsp;</td><td align="right"><?php echo rupiah($rows['diskon']); ?></td></tr></table></td>
							<td align="right" style='border-bottom: 1px solid #000; border-right: 1px solid #000'><table style="border:0px solid; font-size: 15px" cellspacing="0" width="100%"><tr><td align="center">&nbsp;Rp&nbsp;</td><td align="right"><?php echo rupiah($rows['harga_jual']); ?></td></tr></table></td>
							<td align="right" style='border-bottom: 1px solid #000; border-right: 1px solid #000'><table style="border:0px solid; font-size: 15px" cellspacing="0" width="100%"><tr><td align="center">&nbsp;Rp&nbsp;</td><td align="right"><?php echo rupiah($rows['harga_merchant']); ?></td></tr></table></td>
							<!-- <td align="center" style='border-bottom: 1px solid #000; border-right: 1px solid #000'><table style="border:0px solid; font-size: 15px" cellspacing="0" width="100%"><tr><td align="center">&nbsp;Rp&nbsp;</td><td align="right"><?php// echo rupiah($rows['komisi_topsonia']); ?></td></tr></table></td> -->
							<td align="right" style='border-bottom: 1px solid #000; border-right: 1px solid #000'><table style="border:0px solid; font-size: 15px" cellspacing="0" width="100%"><tr><td align="center">&nbsp;Rp&nbsp;</td><td align="right"><?php echo rupiah($rows['mdr']); ?></td></tr></table></td>
							<td align="right" style='border-bottom: 1px solid #000; border-right: 1px solid #000'><table style="border:0px solid; font-size: 15px" cellspacing="0" width="100%"><tr><td align="center">&nbsp;Rp&nbsp;</td><td align="right"><?php echo rupiah($rows['topsonia_net']); ?></td></tr></table></td>
							<td align="right" style='border-bottom: 1px solid #000; border-right: 1px solid #000'><table style="border:0px solid; font-size: 15px" cellspacing="0" width="100%"><tr><td align="center">&nbsp;Rp&nbsp;</td><td align="right"><?php echo rupiah($rows['pajak']); ?></td></tr></table></td>
							<td align="right" style='border-bottom: 1px solid #000; border-right: 1px solid #000'><table style="border:0px solid; font-size: 15px" cellspacing="0" width="100%"><tr><td align="center">&nbsp;Rp&nbsp;</td><td align="right"><?php echo rupiah($rows['t_asuransi']); ?></td></tr></table></td>
						<!-- <td align="right" style='border-bottom: 1px solid #000; border-right: 1px solid #000'><table style="border:0px solid; font-size: 15px" cellspacing="0" width="100%"><tr><td align="center">&nbsp;Rp&nbsp;</td><td align="right"><?php// echo rupiah($rows['ongkir']); ?></td></tr></table></td>
							<td align="right" style='border-bottom: 1px solid #000; border-right: 1px solid #000'><table style="border:0px solid; font-size: 15px" cellspacing="0" width="100%"><tr><td align="center">&nbsp;Rp&nbsp;</td><td align="right"><?php// echo rupiah($rows['diskon_ongkir']); ?></td></tr></table></td> -->
							<td align="right" style='border-bottom: 1px solid #000;'><table style="border:0px solid; font-size: 15px" cellspacing="0" width="100%"><tr><td align="center">&nbsp;Rp&nbsp;</td><td align="right"><?php echo rupiah($rows['ongkir_net']); ?></td></tr></table></td>
						</tr>

						<?php
						$no++;
					}
					?>
				</tbody>
				<tr>
					<td align="center" style=' border-right: 1px solid #000' colspan="<?php if ($resel != '') { echo "3"; }else{ echo "3"; } ?>"><b>TOTAL</b></td>
					<td align="center" style=' border-right: 1px solid #000'><table style="border:0px solid; font-size: 15px" cellspacing="0" width="100%"><tr><td align="center">&nbsp;Rp&nbsp;</td><td align="right"><b><?php echo rupiah($total_perbulan['total_harga_publish']); ?></b></td></tr></table></td>
					<td align="center" style=' border-right: 1px solid #000'><table style="border:0px solid; font-size: 15px" cellspacing="0" width="100%"><tr><td align="center">&nbsp;Rp&nbsp;</td><td align="right"><b><?php echo rupiah($total_perbulan['total_diskon']); ?></b></td></tr></table></td>
					<td align="center" style=' border-right: 1px solid #000'><table style="border:0px solid; font-size: 15px" cellspacing="0" width="100%"><tr><td align="center">&nbsp;Rp&nbsp;</td><td align="right"><b><?php echo rupiah($total_perbulan['total_harga_jual']); ?></b></td></tr></table></td>
					<td align="center" style=' border-right: 1px solid #000'><table style="border:0px solid; font-size: 15px" cellspacing="0" width="100%"><tr><td align="center">&nbsp;Rp&nbsp;</td><td align="right"><b><?php echo rupiah($total_perbulan['total_harga_merchant']); ?></b></td></tr></table></td>
					<!-- <td align="center" style=' border-right: 1px solid #000'><table style="border:0px solid; font-size: 15px" cellspacing="0" width="100%"><tr><td align="center">&nbsp;Rp&nbsp;</td><td align="right"><b><?php //echo rupiah($total_perbulan['total_komisi_topsonia']); ?></b></td></tr></table></td> -->
					<td align="center" style=' border-right: 1px solid #000'><table style="border:0px solid; font-size: 15px" cellspacing="0" width="100%"><tr><td align="center">&nbsp;Rp&nbsp;</td><td align="right"><b><?php echo rupiah($total_perbulan['total_mdr']); ?></b></td></tr></table></td>
					<td align="center" style=' border-right: 1px solid #000'><table style="border:0px solid; font-size: 15px" cellspacing="0" width="100%"><tr><td align="center">&nbsp;Rp&nbsp;</td><td align="right"><b><?php echo rupiah($total_perbulan['total_topsonia_net']); ?></b></td></tr></table></td>
					<td align="center" style=' border-right: 1px solid #000'><table style="border:0px solid; font-size: 15px" cellspacing="0" width="100%"><tr><td align="center">&nbsp;Rp&nbsp;</td><td align="right"><b><?php echo rupiah($total_perbulan['total_pajak']); ?></b></td></tr></table></td>
					<td align="center" style=' border-right: 1px solid #000'><table style="border:0px solid; font-size: 15px" cellspacing="0" width="100%"><tr><td align="center">&nbsp;Rp&nbsp;</td><td align="right"><b><?php echo rupiah($total_perbulan['total_asuransi']); ?></b></td></tr></table></td>
				<!-- <td align="center" style=' border-right: 1px solid #000'><table style="border:0px solid; font-size: 15px" cellspacing="0" width="100%"><tr><td align="center">&nbsp;Rp&nbsp;</td><td align="right"><b><?php //echo rupiah($total_perbulan['total_ongkir']); ?></b></td></tr></table></td>
					<td align="center" style=' border-right: 1px solid #000'><table style="border:0px solid; font-size: 15px" cellspacing="0" width="100%"><tr><td align="center">&nbsp;Rp&nbsp;</td><td align="right"><b><?php// echo rupiah($total_perbulan['total_diskon_ongkir']); ?></b></td></tr></table></td> -->
					<td align="center" style=''><table style="border:0px solid; font-size: 15px" cellspacing="0" width="100%"><tr><td align="center">&nbsp;Rp&nbsp;</td><td align="right"><b><?php echo rupiah($total_perbulan['total_ongkir_net']); ?></b></td></tr></table></td>

				</tr>
			</table>
		</td></tr>
	</table>
	<?php
}
?>