<div class='col-md-12'>
	<div class='box box-info'>
		<div class='box-header with-border'>
			<h3 class='box-title'>Merchant Discount Rate</h3>
			<!--   <a class='pull-right btn btn-warning btn-sm' href='<?php //echo base_url(); ?>administrator/reseller'>Kembali</a> -->
		</div>
		<div class='box-body'>
			<table id="example1" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th style='width:30px; text-align:center;'>No</th>
						<th>Kategori Pembayaran</th>
						<th style="text-align:center;">Jenis Pembayaran</th>
						<th style="text-align:center;">Persentase MDR (%)</th>
						<th style="text-align:center;">Currency MDR (Rp)</th>
						<!--<th style="text-align: center;">Action</th>-->
					</tr>
				</thead>
				<tbody>
					<?php 
					$no = 1;
					foreach ($record->result_array() as $row){
					?>
					<tr>
						<td style="text-align:center;"><?php echo $no; ?></td>
						<td><?php echo $row['kategori_pembayaran']; ?></td>
						<td style="text-align:center;"><?php echo $row['jenis_pembayaran']; ?></td>
						<td style="text-align:center;"><?php if($row['persentase_mdr']!=''){ echo $row['persentase_mdr']."%"; }else{ echo "-"; } ?></td>
						<td style="text-align:center;"><?php echo $row['nominal_mdr']; ?></td>
						<!--<td style="text-align:center;">
							<button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#myModal<?php //echo $row['id_mdr']; ?>"><span class='glyphicon glyphicon-pencil'></span> Update</button>

							<a class='btn btn-danger btn-xs' title='Delete Data' href='<?php //echo base_url();?>administrator/delete_tipe_buyer/<?php //echo $row['id_tipe_buyer'];?>' onclick=\"return confirm('Data ini terhubung ke daftar konsumen, Apa anda yakin menghapus Data ini?')\"><span class='glyphicon glyphicon-remove'></span></a>
						</td>-->
					</tr>
					<?php
						$no++;
					}
					?>
				</tbody>
			</table>

			<div style='clear:both'></div>
		</div>
	</div>
</div>


<!-- <div class='col-md-12'>
	<div class='box box-info'>
		<div class='box-header with-border'>
			<h3 class='box-title'>Update Merchant Discount Rate</h3>
		</div>
		<div class='box-body'> -->
			<?php
// $attributes = array('class'=>'form-horizontal','role'=>'form','name'=>'demo');
// echo form_open_multipart('administrator/tambah_tipe_konsumen',$attributes); 
			?>

			<!-- <table class='table table-condensed table-bordered'>
				<tbody>
					<tr>
						<th scope='row'>Kategori Pembayaran<span style='color:red;'><small> *</small></span></th>
						<td><input type='text' placeholder='Jenis Pembayaran' class='form-control' name='tipe_buyer' disabled></td>
					</tr>
					<tr>
						<th scope='row'>Jenis Pembayaran<span style='color:red;'><small> *</small></span></th>
						<td><input type='text' placeholder='Jenis Pembayaran' class='form-control' name='tipe_buyer' disabled></td>
					</tr>
					<tr>
						<th scope='row'>Persentase MDR (%)<span style='color:red;'><small> *</small></span></th>
						<td><input type='text' class='form-control' placeholder='Jika MDR dalam bentuk persen (%)' name='mdr_persen' id='mdr_persen' disabled><div style='color:red;'><span></span></div></td>
					</tr>
					<tr><th scope='row'>Nominal MDR (Rp)</th>
						<td><input type='text' placeholder='Jika MDR dalam bentuk Rupiah' class='form-control' name='komisi' id='komisi_topsonia' disabled>
							<input type='hidden' name='komisi_topsonia' id='komisi_topsonia_hidden'>
							<div style='color:red;'><span></span></div></td>
						</tr>
						<tr>
							<th scope='row'></th>
							<td><button type='submit' name='submit' class='btn btn-info' disabled>Update</button></td>
						</tr>
					</tbody>
				</table>

				<div style='clear:both'></div>
			</div>
		</div>
	</div> -->



		
<?php
	$no = 1;
	foreach ($record ->result_array() as $tampil) {
		// $id_penjualan = $tampil_header['id_penjualan'];
?>
		<div id="myModal<?php echo $tampil['id_mdr']; ?>" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- konten modal-->
		<div class="modal-content">
			<!-- heading modal -->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Update MDR</h4>
			</div>
			<!-- body modal -->
			<div class="modal-body">
				<p>
					<?php
$attributes = array('class'=>'form-horizontal','role'=>'form','name'=>'demo');
echo form_open_multipart('administrator/mdr',$attributes); 
			?>

			<table class='table table-condensed table-bordered'>
				<tbody>
					<tr>
						<th scope='row'>Kategori Pembayaran<span style='color:red;'><small> *</small></span></th>
						<td>
							<input type='text' name='id_mdr' value="<?php echo $tampil['id_mdr']; ?>">
							<input type='text' value="<?php echo $tampil['kategori_pembayaran']; ?>" class='form-control' name='kategori_pembayaran' required>
						</td>
					</tr>
					<tr>
						<th scope='row'>Jenis Pembayaran<span style='color:red;'><small> *</small></span></th>
						<td><input type='text' value="<?php echo $tampil['jenis_pembayaran']; ?>" class='form-control' name='jenis_pembayaran' required></td>
					</tr>
					<tr>
						<th scope='row'>Persentase MDR (%)<span style='color:red;'><small> *</small></span></th>
						<td><input type='text' class='form-control' value="<?php echo $tampil['persentase_mdr']; ?>" name='persentase_mdr'><div style='color:red;'><span></span></div></td>
					</tr>
					<tr><th scope='row'>Nominal MDR (Rp)</th>
						<td><input type='text' value="<?php echo $tampil['nominal_mdr']; ?>" class='form-control' name='nominal_mdr'>
							<div style='color:red;'><span></span></div></td>
						</tr>
						<tr>
							<th scope='row'></th>
							<td><button type='submit' name='submit' class='btn btn-info'>Update</button></td>
						</tr>
					</tbody>
				</table>
				</p>
			</div>
		</div>
	</div>
</div>

<?php
	}
?>