<div class="col-xs-12">

  <ul id='myTabs' class='nav nav-tabs' role='tablist'>
    <li role='presentation' class='active'><a href='#semua' id='semua-tab' role='tab' data-toggle='tab' aria-controls='profile' aria-expanded='true' onclick="tab_komisi(event, 'history')">Data Komisi </a></li>
    <li role='presentation' class=''><a href='#detail' role='tab' id='detail-tab' data-toggle='tab' aria-controls='status1' aria-expanded='false' onclick="tab_komisi(event, 'pencairan_kom')">Request Pencairan</a></li>
  </ul><br>

  <div id="history" class="tabcontent" style="display: block;">
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">History Pembayaran Komisi</h3>
      </div>
      <div class="box-body">
        
        <div class="col-xs-4" style="margin-bottom:10px; margin-left:2px;">
          Nama kosumen :
          <select name='id_konsumen' id='id_konsumen' onchange='filter_komisi();'>
            <option value="p">Pilih Konsumen</option>
            <?php
            $konsumen = $this->db->query("select a.id_konsumen,a.nama_lengkap from rb_konsumen a join rb_penjualan b on a.id_konsumen=b.id_pembeli join tipe_buyer c on a.id_tipe_buyer=c.id_tipe_buyer where b.proses='1' and c.skema_diskon>0 and b.status_pembeli='konsumen' GROUP BY a.id_konsumen ORDER BY a.id_konsumen ");
            $konsumen_cek = $konsumen->num_rows();
            if ($konsumen_cek > 0) {

              $konsumen_detail = $konsumen->result_array();

              foreach ($konsumen_detail as $kos) {
                echo "<option value='$kos[id_konsumen]'>$kos[nama_lengkap]</option>";
              }
            } else {
            }
            ?>
          </select>
        </div>

        <div class="col-xs-2" style="margin-bottom:10px;">
          Tahun :
          <select name='tahun' id='tahun' onchange='filter_komisi();'>
            <option value="p">Pilih Tahun</option>
            <?php
            $tahun = $this->db->query("select MIN(YEAR(waktu_order)) as min_tahun, MAX(YEAR(waktu_order)) as max_tahun from rb_penjualan where waktu_order IS NOT NULL and proses='1'")->row_array();
            if ($tahun['min_tahun'] == $tahun['max_tahun']) {
              echo "<option>$tahun[min_tahun]</option>";
            } else {
              for ($y = $tahun['min_tahun']; $y <= $tahun['max_tahun']; $y++) {
                echo "<option>$y</option>";
              }
            }
            ?>
          </select>
        </div>

        <div class="col-xs-3" style="margin-bottom:10px;">
          Bulan :
          <select name='bulan' id='bulan' onchange='filter_komisi();'>
            <option value="p">Pilih Bulan</option>
            <option value='1'>Januari</option>
            <option value='2'>Februari</option>
            <option value='3'>Maret</option>
            <option value='4'>April</option>
            <option value='5'>Mei</option>
            <option value='6'>Juni</option>
            <option value='7'>Juli</option>
            <option value='8'>Agustus</option>
            <option value='9'>September</option>
            <option value='10'>Oktober</option>
            <option value='11'>November</option>
            <option value='12'>Desember</option>
          </select>
        </div>

        <table id="example1" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th style='width:20px'>No</th>
              <th>Nama Konsumen</th>
              <th>Jumlah Penarikan</th>
              <th id="n_saldo">Sisa Saldo</th>
              <!-- <th style='width:50px'>Action</th> -->
            </tr>
          </thead>
          <tbody id='tabel_body'>
            <?php
            $no = 1;
            foreach ($record->result_array() as $row) {
              $id_konsumen = $row['id_konsumen'];

              $pencairan = $this->db->query("select sum(jumlah_pencairan) as jumlah_pencairan from rb_pencairan_komisi where id_konsumen='$id_konsumen' and status='Lunas' GROUP BY id_konsumen ORDER BY id_pencairan_bonus desc");
              $pencairan_cek = $pencairan->num_rows();

              if ($pencairan_cek > 0) {

                $pencairan_detail = $pencairan->row_array();
                $tanggal_pencairan = $pencairan_detail['tanggal_pencairan'];
                $jumlah_pencairan = "Rp " . rupiah($pencairan_detail['jumlah_pencairan']);
                $sisa_saldo = "Rp " . rupiah($row['diskon'] - $pencairan_detail['jumlah_pencairan']);
              } else {
                $tanggal_pencairan = "";
                $jumlah_pencairan = "";
                $sisa_saldo = "Rp " . rupiah($row['diskon']);
              }

              echo "<tr><td>$no</td>
                              <td>$row[nama_lengkap]</td>
                              <td>$jumlah_pencairan</td>
                              <td>$sisa_saldo</td>
                          </tr>";
              $no++;
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>

  <div id="pencairan_kom" class="tabcontent" style="display: none;">
  <div class="box">
      <div class="box-header">
        <h3 class="box-title">Request Pembayaran Komisi</h3>
      </div>
      <div class="box-body">

        <div class="col-xs-4" style="margin-bottom:10px; margin-left:2px;">
          Nama kosumen :
          <select name='id_konsumen_r' id='id_konsumen_r' onchange='filter_request();'>
            <option value="p">Pilih Konsumen</option>
            <?php
            $konsumen = $this->db->query("select a.id_konsumen,a.nama_lengkap from rb_konsumen a join rb_pencairan_komisi b on a.id_konsumen=b.id_konsumen GROUP BY a.id_konsumen ORDER BY a.id_konsumen ");
            $konsumen_cek = $konsumen->num_rows();
            if ($konsumen_cek > 0) {

              $konsumen_detail = $konsumen->result_array();

              foreach ($konsumen_detail as $kos) {
                echo "<option value='$kos[id_konsumen]'>$kos[nama_lengkap]</option>";
              }
            } else {
            }
            ?>
          </select>
        </div>

        <div class="col-xs-3" style="margin-bottom:10px;">
          Status :
          <select name='status_r' id='status_r' onchange='filter_request();'>
            <option value="p">Pilih Status</option>
            <option value='Request'>Request</option>
            <option value='Lunas'>Lunas</option>
          </select>
        </div>

        <table id="example1" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th style='width:20px'>No</th>
              <th>Nama Konsumen</th>
              <th>Tanggal Request</th>
              <th>Jumlah Pencairan</th>
              <th>Tanggal Pencairan</th>
              <th>Status</th>
              <th style='width:50px'>Action</th>
            </tr>
            
          </thead>
          <tbody id='tabel_body_request'>
            <?php 

             $pencairan = $this->db->query("select a.id_pencairan_bonus,a.status,a.jumlah_pencairan,a.tanggal_request,a.tanggal_pencairan,a.id_konsumen,b.nama_lengkap from rb_pencairan_komisi a join rb_konsumen b on a.id_konsumen=b.id_konsumen ORDER BY status desc,id_pencairan_bonus asc")->result_array();
             $no =1 ;
             foreach($pencairan as $pen){
               if($pen['status']=='Lunas'){
                 $disabled='disabled';
               }else{$disabled='';}
              echo "<tr><td>$no</td>
              <td>$pen[nama_lengkap]</td>
              <td>$pen[tanggal_request]</td>
              <td>$pen[jumlah_pencairan]</td>
              <td>$pen[tanggal_pencairan]</td>
              <td>$pen[status]</td>
              <td><center>
              <a class='btn btn-warning btn-xs' title='Pencairan Bonus' href='" . base_url() . "administrator/pencairan_komisi/$pen[id_pencairan_bonus]/' $disabled><span class='fa fa-money'></span></a>
              </center></td>
              </tr>";
              $no++;
             }
            
              
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>

</div>



<script>
  function filter_komisi() {

    let thn = document.getElementById('tahun').value;
    let bln = document.getElementById('bulan').value;
    let konsumen_id = document.getElementById('id_konsumen').value;

    $.ajax({
      type: 'POST',
      url: '<?php echo base_url(); ?>administrator/filter_komisi',
      dataType: 'JSON',
      data: {
        tahun: thn,
        bulan: bln,
        id_konsumen: konsumen_id
      },
      success: function(data) {
        if (data.result == '0') {
          alert('Data Not Found');
        } else {
          document.getElementById('n_saldo').innerHTML = "Pendapatan";
          document.getElementById('tabel_body').innerHTML = data.filter;
          // alert(data.result);
        }
      },
      error: function(data) {
        alert('Data Not Found');
      },
    });
    // alert(tahun);
    // alert(bulan);
  }

  function filter_request() {
    let konsumen_id = document.getElementById('id_konsumen_r').value;
    let status = document.getElementById('status_r').value;
    $.ajax({
      type: 'POST',
      url: '<?php echo base_url(); ?>administrator/filter_request_komisi',
      dataType: 'JSON',
      data: {
        status: status,
        id_konsumen: konsumen_id
      },
      success: function(data) {
        if (data.result == '0') {
          alert('Data Not Found');
        } else {
          document.getElementById('tabel_body_request').innerHTML = data.filter;
          // alert(data.result);
        }
      },
      error: function(data) {
        alert('Data Not Found');
      },
    });
    // alert(tahun);
    // alert(bulan);
  }
</script>

<script>
  function tab_komisi(evt, nametab) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(nametab).style.display = "block";
    evt.currentTarget.className += " active";
  }
</script>