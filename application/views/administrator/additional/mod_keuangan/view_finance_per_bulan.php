<script type="text/javascript">
	$(document).ready(function() {
		$('#example').DataTable( {
			"scrollX": true,
			"searching": false
		} );
	} );
</script>
<?php
$thn 	= $this->input->get('thn');
$bulan2 = $bulan['bulan'];
if ($bulan2 !='') {

	if ($bulan2 == 1) {
		$bln = "Januari";
	}elseif ($bulan2 == 2) {
		$bln =  "Februari";
	}elseif ($bulan2 == 3) {
		$bln =  "Maret";
	}elseif ($bulan2 == 4) {
		$bln =  "April";
	}elseif ($bulan2 == 5) {
		$bln =  "Mei";
	}elseif ($bulan2 == 6) {
		$bln =  "Juni";
	}elseif ($bulan2 == 7) {
		$bln =  "Juli";
	}elseif ($bulan2 == 8) {
		$bln =  "Agustus";
	}elseif ($bulan2 == 9) {
		$bln =  "September";
	}elseif ($bulan2 == 10) {
		$bln =  "Oktober";
	}elseif ($bulan2 == 11) {
		$bln =  "November";
	}elseif ($bulan2 == 12) {
		$bln =  "Desember";
	}
}else{
	$bln =  "-";
}
?>

<div class='col-md-12'>
	<div class='box box-info'>
		<div class='box-header with-border'>
			<h3 class='box-title'>
				Laporan Finance Bulan <?php echo $bln;?>
				<!-- <a class='pull-right btn btn-warning btn-sm' href='<?php echo base_url(); ?>administrator/finance'>Kembali</a> -->
			</h3>
		</div>

		<div class='box-header with-border'>
			<div class='col-md-12'>
				<table width="100%" border="0px">
					<tr>
						<?php
		                  $attributes = array('class'=>'form-horizontal','role'=>'form');
		                  echo form_open_multipart('administrator/viewfinance?bln='.$bulan2.'&thn='.$thn, $attributes); 
		                  ?>
						<form action="finance?bln=<?php echo $bulan2; ?>" method="POST">
						<td style="font-weight: bold;">Filter &nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td style="text-align: right;" width="200px">
							<!-- <span class='dropdown'>
								<button class='btn btn-default btn-sm dropdown-toggle' type='button' style='margin-right:5px' data-toggle='dropdown' ><span class='glyphicon glyphicon-list'></span> Pilih Merchant</button> 
								<ul class="dropdown-menu">
									<li><a href="<?php //echo base_url(); ?>administrator/finance?bln=<?php //echo $bulan2 ?>">Semua Merchant</a></li>
									<?php
										//$list_merchant = $this->db->query("SELECT * FROM rb_reseller")->result_array();
										//$a=1;
										//foreach ($list_merchant as $list) {
									?>
									<li><a href="<?php //echo base_url(); ?>administrator/finance_bln?bulan=<?php echo $bulan2 ?>&find=<?php //echo $list['id_reseller']; ?>"><?php //echo $list['nama_reseller']; ?></a></li>
									<?php
								//}
								?>
								</ul>
							</span> -->

							<select class="form-control" name="merchant">
								<option value="">Semua Merchant</option>
								<?php
									$list_merchant = $this->db->query("SELECT * FROM rb_reseller")->result_array();
									$a=1;
									foreach ($list_merchant as $list) {
								?>
										<option value="<?php echo $list['id_reseller']; ?>"><?php echo $list['nama_reseller']; ?></option>
								<?php
									}
								?>
							</select>

						</td>

						<td width="170px" style="padding-left: 20px;">
							<select class="form-control" name="pembayaran">
								<option value=""><span class='glyphicon glyphicon-list'></span> Semua Pembayaran</option>
								<?php
									$list_pembayaran = $this->db->query("SELECT DISTINCT kategori_pembayaran FROM `tabel_mdr`")->result_array();
									$a=1;
									foreach ($list_pembayaran as $list) {
								?>
										<option value="<?php echo $list['kategori_pembayaran']; ?>">
											<?php if ($list['kategori_pembayaran'] == 'qris') { echo "Qris"; }elseif($list['kategori_pembayaran'] == 'midtrans'){ echo "Midtrans Payment"; }elseif($list['kategori_pembayaran'] == 'manual-transfer'){ echo "Manual Transfer"; } ?>
										</option>
								<?php
									}
								?>
							</select>
						</td>

						<td width="110px" style="padding-left: 20px"><input type="text" placeholder="Tanggal Awal" name="tanggal_awal"  class="form-control datepicker" disabled /></td>
						<td style="text-align:center;"> s/d </td>
						<td width="110px" ><input type="text" name="tanggal_akhir" placeholder="Tanggal Akhir" class="form-control datepicker" disabled /></td>

						<td width="80px" style="text-align:center;"><button type="submit" name="submit" class="btn btn-info btn-sm"><span class='glyphicon glyphicon-search'></span> Search</button></td>

						<?php
						echo form_close();
						?>
						
						<td style="text-align: right;">&nbsp;
							&nbsp;
							<span class='dropdown'>
								<button class='btn btn-info btn-sm dropdown-toggle' type='button' style='margin-right:5px' data-toggle='dropdown' ><span class='glyphicon glyphicon-export'></span> Export</button> 
								<ul class="dropdown-menu">
									<li>
										<?php
											if ($reseler != '') {
												?>
												<a href="<?php echo base_url(); ?>administrator/finance_excel?bln=<?php echo $bulan2 ?>&reseler=<?php echo $reseler ?>" target="_blank">Excel</a>
												<?php
											}else{
										?>
										<a href="<?php echo base_url(); ?>administrator/finance_excel?bln=<?php echo $bulan2 ?>" target="_blank">Excel</a>
									<?php } ?>
									</li>
									<li>
										<?php
											if ($reseler != '') {
												?>
												<a href="<?php echo base_url(); ?>administrator/finance_pdf?bln=<?php echo $bulan2 ?>&reseler=<?php echo $reseler ?>" target="_blank">PDF</a>
												<?php
											}else{
										?>
										<a href="<?php echo base_url(); ?>administrator/finance_pdf?bln=<?php echo $bulan2 ?>" target="_blank">PDF</a>
									<?php } ?>
									</li>
								</ul>
							</span>
							<a href="javascript:history.go(-1)" class="btn btn-info btn-sm"><span class='glyphicon glyphicon-circle-arrow-left'></span> Kembali</a>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<div class='box-body'>
			<table id="example" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th style='width:30px; text-align: center;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No</th>
						<th>Merchant</th>
						<th>Kode Transaksi</th>
						<th>Pembayaran</th>
						<th>Waktu Order</th>
						<th>Harga Publish</th>
						<th>Diskon</th>
						<th>Total Harga Jual</th>
						<th>Total Merchant</th>
						<!-- <th>Total Topsonia Gross</th> -->
						<th>Merchant Discount Rate<br>(MDR)</th>
						<th>Total Topsonia</th>
						<th>PPN</th>
						<th>Ongkos Kirim</th>
						<th>Diskon Ongkir</th>
						<th>Total Ongkir Net</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php
					$no = 1;
					foreach ($detail_perbulan->result_array() as $rows) {
						?>
						<tr>
							<td style='text-align: center;'><?php echo $no; ?></td>
							<td><?php echo $rows['nama_reseller']; ?></td>
							<td><?php echo $rows['kode_transaksi']; ?></td>
							<td><?php echo $rows['kategori_pembayaran']; ?></td>
							<td><?php echo date('d M Y', strtotime($rows["tanggal_order"])); ?></td>
							<td><?php echo 'Rp&nbsp;'.rupiah($rows['harga_publish']); ?></td>
							<td><?php echo 'Rp&nbsp;'.rupiah($rows['diskon']); ?></td>
							<td><?php echo 'Rp&nbsp;'.rupiah($rows['harga_jual']); ?></td>
							<td><?php echo 'Rp&nbsp;'.rupiah($rows['harga_merchant']); ?></td>
							<!-- <td><?php //echo 'Rp&nbsp;'.rupiah($rows['komisi_topsonia']); ?></td> -->
							<td><?php echo 'Rp&nbsp;'.rupiah($rows['mdr_net']); ?></td>
							<td><?php echo 'Rp&nbsp;'.rupiah($rows['topsonia_net']); ?></td>
							<td><?php echo 'Rp&nbsp;'.rupiah($rows['pajak']); ?></td>
							<td><?php echo 'Rp&nbsp;'.rupiah($rows['ongkir']); ?></td>
							<td><?php echo 'Rp&nbsp;'.rupiah($rows['diskon_ongkir']); ?></td>
							<td><?php echo 'Rp&nbsp;'.rupiah($rows['ongkir_net']); ?></td>
							<td style='text-align: center;'>
								<button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#myModal<?php echo $rows['id_penjualan']; ?>"><span class='glyphicon glyphicon-search'></span> Detail
								</button>

								<!-- <a class="btn btn-info btn-xs" href=""><span class='glyphicon glyphicon-search'></span></a> -->
							</td>
							
						</tr>

						<?php
						$no++;
					}
					?>
				</tbody>
				<tr style="background-color: yellow;">
					<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
					<td style='text-align: right;'><b>TOTAL</b></td>
					<td><b><?php echo 'Rp&nbsp;'.rupiah($total_perbulan['total_harga_publish']); ?></b></td>
					<td><b><?php echo 'Rp&nbsp;'.rupiah($total_perbulan['total_diskon']); ?></b></td>
					<td><b><?php echo 'Rp&nbsp;'.rupiah($total_perbulan['total_harga_jual']); ?></b></td>
					<td><b><?php echo 'Rp&nbsp;'.rupiah($total_perbulan['total_harga_merchant']); ?></b></td>
					<!-- <td><b><?php //echo 'Rp&nbsp;'.rupiah($total_perbulan['total_komisi_topsonia']); ?></b></td> -->
					<td><b><?php echo 'Rp&nbsp;'.rupiah($total_perbulan['total_total_mdr']); ?></b></td>
					<td><b><?php echo 'Rp&nbsp;'.rupiah($total_perbulan['total_topsonia_net']); ?></b></td>
					<td><b><?php echo 'Rp&nbsp;'.rupiah($total_perbulan['total_total_pajak']); ?></b></td>
					<td><b><?php echo 'Rp&nbsp;'.rupiah($total_perbulan['total_ongkir']); ?></b></td>
					<td><b><?php echo 'Rp&nbsp;'.rupiah($total_perbulan['total_diskon_ongkir']); ?></b></td>
					<td><b><?php echo 'Rp&nbsp;'.rupiah($total_perbulan['total_ongkir_net']); ?></b></td>
					<td>&nbsp;</td>
				</tr>
			</table>
			<div style='clear:both'></div>
		</div>
	</div>
</div>


<?php

$noo=1;
foreach ($detail_header->result_array() as $headerr) {

?>
	<div id="myModal<?php echo $headerr['id_penjualan']; ?>" class="modal fade" role="dialog">
		<div class="modal-dialog modal-lg modal-dialog-scrollable">
			<!-- konten modal-->
			<div class="modal-content">
				<!-- heading modal -->
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Detail Transaksi</h4>
				</div>
				<!-- body modal -->
				<div class="modal-body">
					<p>
						<table width="100%" border="0">
							<tr>
								<td>Kode Transaksi</td><td width="300px">: <?php echo $headerr['kode_transaksi']; ?></td>
								<td>Nama Pembeli</td><td width="300px">: <?php echo $headerr['nama_lengkap']; ?></td>
							</tr>
							<tr>
								<td>Merchant</td><td width="300px">: <?php echo $headerr['nama_reseller']; ?></td>
								<td>Ongkir</td><td width="300px"><table border="0"><tr><td>: <?php echo "Rp ".rupiah($headerr['ongkir']); ?>&nbsp;&nbsp;&nbsp;</td><td style="color: red;"> <?php echo "(Rp ".rupiah($headerr['diskon_ongkir']).")"; ?></td><td style="color: green;"> &nbsp;&nbsp;&nbsp;<?php echo "Rp ".rupiah($headerr['net_ongkir']); ?></td></tr></table></td>
							</tr>
							<tr>
								<td>Waktu Order</td><td width="300px">: <?php echo $headerr['waktu_order']; ?></td>
								<td>Jenis Pengiriman</td><td width="300px">: <?php echo $headerr['service']; ?></td>
							</tr>
							<tr>
								<td>Airwaybill</td><td width="300px">: <?php echo $headerr['awb']; ?></td>
								<td>Alamat Pengiriman</td><td width="300px">: <?php echo $headerr['alamat_lengkap'].", Kecamatan ".$headerr['kecamatan']; ?></td>
							</tr>
							<tr>
								<td>&nbsp;</td><td width="300px">&nbsp;</td>
								<td>&nbsp;</td><td width="300px">: <?php echo $headerr['nama_kota'].", ".$headerr['nama_provinsi']." ".$headerr['kode_pos']; ?></td>
							</tr>
							<tr>
								<td colspan="4">&nbsp;</td>
							</tr><tr>
								<td colspan="4">
									<table border="1" width="100%">
										<thead>
											<th style="text-align: center;">No</th>
											<th>&nbsp;Nama Produk</th>
											<th>&nbsp;Harga Jual</th>
											<th style="text-align: center;">Jumlah</th>
											<th>&nbsp;Diskon Merchant</th>
											<th>&nbsp;Subtotal</th>
											<th>&nbsp;Komisi Topsonia</th>
										</thead>
										<tbody>
										<?php
										$id_p = $headerr['id_penjualan'];
											$detail = $this->db->query("SELECT c.nama_produk, b.harga_jual, b.jumlah, c.satuan, b.diskon_merchant, (b.harga_jual*b.jumlah)-b.diskon_merchant AS sub_total, b.diskon_topsonia FROM rb_penjualan a JOIN rb_penjualan_detail b ON a.id_penjualan=b.id_penjualan JOIN rb_produk c ON b.id_produk=c.id_produk JOIN rb_reseller d ON a.id_penjual = c.id_reseller JOIN rb_konsumen e ON a.id_pembeli=e.id_konsumen WHERE a.id_penjualan = '$id_p' GROUP BY c.id_produk");

											$total_detail = $this->db->query("SELECT sum((b.harga_jual*b.jumlah)-b.diskon_merchant) AS totalnya_harga_jual, IFNULL(a.diskon,0) AS total_diskon_merchant, IFNULL(a.komisi_topsonia,0) AS total_komisiTopsonia FROM rb_penjualan a JOIN rb_penjualan_detail b ON a.id_penjualan = b.id_penjualan WHERE a.id_penjualan = '$id_p'")->row_array();

											$nooo = 1;
											foreach ($detail->result_array() as $detail_trx) {
										?>
											<tr>
												<td style="text-align: center;"><?php echo $nooo; ?></td>
												<td><?php echo "&nbsp;".$detail_trx['nama_produk']; ?></td>
												<td><?php echo "&nbsp;Rp ".rupiah($detail_trx['harga_jual']); ?></td>
												<td style="text-align: center;">&nbsp;<?php echo $detail_trx['jumlah']." ".$detail_trx['satuan']; ?></td>
												<td><?php echo "&nbsp;Rp ".rupiah($detail_trx['diskon_merchant']); ?></td>
												<td><?php echo "&nbsp;Rp ".rupiah($detail_trx['sub_total']); ?></td>
												<td><?php echo "&nbsp;Rp ".rupiah($detail_trx['diskon_topsonia']); ?></td>
											</tr>
											<?php
												$nooo++;
												}
											?>
										</tbody>
										<tr style="background-color: yellow;">
											<td colspan="4" style="text-align: center;"><b>TOTAL</b></td>
											<td><?php echo "&nbsp;Rp ".rupiah($total_detail['total_diskon_merchant']); ?></td>
											<td><?php echo "&nbsp;Rp ".rupiah($total_detail['totalnya_harga_jual']); ?></td>
											<td><?php echo "&nbsp;Rp ".rupiah($total_detail['total_komisiTopsonia']); ?></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</p>
				</div>
			</div>
		</div>
	</div>
<?php
	}
?>