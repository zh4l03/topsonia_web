<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Data Penjualan Perbulan.xls");
?>
	<table>
		<tr><td style="text-align: center;"><h1>Data Penjualan</h1></td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>
			<table border="1">
				<thead>
					<tr style="background-color: grey;">
						<th style='width:30px; text-align: center;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No</th>
						<th>Kode Transaksi</th>
						<th>Waktu Order</th>
						<th align="right">Harga Publish</th>
						<th align="right">Diskon</th>
						<th align="right">Total Harga Jual</th>
						<th align="right">Total Merchant</th>
						<!-- <th>Total Topsonia Gross</th> -->
						<th align="right">MDR</th>
						<th align="right">Total Topsonia</th>
						<th align="right">PPN</th>
						<th align="right">Ongkos Kirim</th>
						<th align="right">Diskon Ongkir</th>
						<th align="right">Total Ongkir Net</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$no = 1;
					foreach ($detail_perbulan->result_array() as $rows) {
						?>
						<tr>
							<td style='text-align: center;'><?php echo $no; ?></td>
							<td><?php echo $rows['kode_transaksi']; ?></td>
							<td><?php echo date('d M Y', strtotime($rows["tanggal_order"])); ?></td>
							<td align="right"><?php echo 'Rp&nbsp;'.rupiah($rows['harga_publish']); ?></td>
							<td align="right"><?php echo 'Rp&nbsp;'.rupiah($rows['diskon']); ?></td>
							<td align="right"><?php echo 'Rp&nbsp;'.rupiah($rows['harga_jual']); ?></td>
							<td align="right"><?php echo 'Rp&nbsp;'.rupiah($rows['harga_merchant']); ?></td>
							<!-- <td><?php //echo 'Rp&nbsp;'.rupiah($rows['komisi_topsonia']); ?></td> -->
							<td align="right"><?php echo 'Rp&nbsp;'.rupiah($rows['mdr_net']); ?></td>
							<td align="right"><?php echo 'Rp&nbsp;'.rupiah($rows['topsonia_net']); ?></td>
							<td align="right"><?php echo 'Rp&nbsp;'.rupiah($rows['pajak']); ?></td>
							<td align="right"><?php echo 'Rp&nbsp;'.rupiah($rows['ongkir']); ?></td>
							<td align="right"><?php echo 'Rp&nbsp;'.rupiah($rows['total_diskon_ongkir']); ?></td>
							<td align="right"><?php echo 'Rp&nbsp;'.rupiah($rows['ongkir_net']); ?></td>

						</tr>

						<?php
						$no++;
					}
					?>
				</tbody>
				<tr>
					<td style='text-align: center;' colspan="3">TOTAL</td>
					<td align="right"><?php echo 'Rp&nbsp;'.rupiah($total_perbulan['total_harga_publish']); ?></td>
					<td align="right"><?php echo 'Rp&nbsp;'.rupiah($total_perbulan['total_diskon']); ?></td>
					<td align="right"><?php echo 'Rp&nbsp;'.rupiah($total_perbulan['total_harga_jual']); ?></td>
					<td align="right"><?php echo 'Rp&nbsp;'.rupiah($total_perbulan['total_harga_merchant']); ?></td>
					<!-- <td><?php// echo 'Rp&nbsp;'.rupiah($total_perbulan['total_komisi_topsonia']); ?></td> -->
					<td align="right"><?php echo 'Rp&nbsp;'.rupiah($total_perbulan['total_total_mdr']); ?></td>
					<td align="right"><?php echo 'Rp&nbsp;'.rupiah($total_perbulan['total_topsonia_net']); ?></td>
					<td align="right"><?php echo 'Rp&nbsp;'.rupiah($total_perbulan['total_total_pajak']); ?></td>
					<td align="right"><?php echo 'Rp&nbsp;'.rupiah($total_perbulan['total_ongkir']); ?></td>
					<td align="right"><?php echo 'Rp&nbsp;'.rupiah($total_perbulan['total_diskon_ongkir']); ?></td>
					<td align="right"><?php echo 'Rp&nbsp;'.rupiah($total_perbulan['total_ongkir_net']); ?></td>

				</tr>
			</table>
		</td></tr>
	</table>