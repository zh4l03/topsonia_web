		<script type="text/javascript">	
			$(function() {
				$('#toggle-two').bootstrapToggle({
					on: 'Enabled',
					off: 'Disabled'
				});
			});
			
		</script>

		<?php 
		echo "<div class='col-md-12'>
		<div class='box box-info'>
		<div class='box-header with-border'>
		<h3 class='box-title'>Edit Data Merchant</h3>
		</div>
		<div class='box-body'>";
		$attributes = array('class'=>'form-horizontal','role'=>'form');
		echo form_open_multipart('administrator/edit_reseller',$attributes); 
		$kom = $this->db->query("SELECT * FROM komisi where id_komisi='$row[id_komisi]'")->row_array();
		echo "<div class='col-md-12'>
		<table class='table table-condensed table-bordered'>
		<tbody>
		<input type='hidden'  value='$rows[id_reseller]' name='id'>";
		if (trim($rows['foto'])==''){ $foto_user = 'blank.png'; }else{ $foto_user = $rows['foto']; }
		echo "<tr bgcolor='#e3e3e3'><th rowspan='21' width='110px'><center><img style='border:1px solid #cecece; height:85px; width:85px' src='".base_url()."asset/foto_user/$foto_user' class='img-circle img-thumbnail'></center></th></tr>
		<tr><th width='130px' scope='row'>Username</th>       <td><input class='form-control' type='text' placeholder='Username' name='a' value='$rows[username]' disabled></td></tr>
		<tr><th scope='row'>Password</th>                     <td><input class='form-control' type='password' placeholder='Password' name='b'></td></tr>
		<!--<tr><th scope='row'>Account NCS</th>                  <td><input class='form-control' type='text' placeholder='Account NCS' name='c' value='$rows[account_ncs]' required></td></tr>-->
		<tr><th scope='row'>Nama Merchant</th>                <td><input class='form-control' type='text' placeholder='Nama merchant' name='d' value='$rows[nama_reseller]' required></td></tr>
		<tr><th scope='row'>No PKS Merchant</th>              <td><input class='form-control' type='text' placeholder='Nomor PKS' name='no_pks' value='$rows[no_pks]'></td></tr>												
		<tr><th scope='row'>Upload PKS</th>                   <td><input type='hidden' name='val_file_pks' value='$rows[file_pks]'><input type='file' class='form-control' name='upload_pks' id='upload_pks'>";
		if ($rows['file_pks'] != ''){ echo "<i style='color:red'>File PKS saat ini : </i><a target='_BLANK' href='".base_url()."asset/data_pks/$rows[file_pks]'>$rows[file_pks]</a>"; } echo "</td></tr>
		<tr><th scope='row'>PIC</th>                  		  <td><input class='form-control' type='text' placeholder='Person in contact' name='e' value='$rows[pic]' required></td></tr>
		<tr><th scope='row'>Jenis Kelamin</th>                <td>"; if ($rows['jenis_kelamin']=='Laki-laki'){ echo "<input type='radio' value='Laki-laki' name='f' checked> Laki-laki <input type='radio' value='Perempuan' name='f' required> Perempuan "; }else{ echo "<input type='radio' value='Laki-laki' name='f'> Laki-laki <input type='radio' value='Perempuan' name='f' checked> Perempuan "; } echo "</td></tr>
		<tr><th scope='row'>No Hp</th>                        <td><input class='form-control' type='number' placeholder='Telpon' name='g' value='$rows[no_telpon]' required></td></tr>
		<tr><th scope='row'>Alamat Email</th>                 <td><input class='form-control' type='email' placeholder='Email' name='h' value='$rows[email]' required></td></tr>                    
		<tr><th scope='row'>Alamat Pickup</th>                <td><textarea class='form-control' name='i' placeholder='Nama Gedung, Jalan, dan No Rumah/Kantor Merchant..' class='required' required>$rows[alamat_lengkap]</textarea></td></tr>					
		<tr> <th scope='row'>Provinsi</th>            		  <td> <select class='form-control' name='state' id='prov_id' required> <option value=''>- Pilih -</option>";  foreach ($provinsi as $row) { echo "<option value='$row[provinsi_id]' ";   if(substr($rows['kota_id'],0,2) == $row['provinsi_id']){ echo "selected";}; echo "> $row[nama_provinsi]</option>"; } echo "</select></td> </tr>					                    
		<tr> <th scope='row'>Kota</th>      				  <td> <select class='form-control' name='kota' id='kab_id' required><option value=''>- Pilih -</option>";  										$kab = $this->model_app->view_where_ordering('rb_kota', array('provinsi_id'=>substr($rows['kota_id'],0,2)), 'kota_id','ASC');										foreach ($kab as $row) { echo "<option value='$row[kota_id]' ";   if($rows['kota_id'] == $row['kota_id']){ echo "selected";}; echo "> $row[nama_kota]</option>"; } echo "</select> </td> </tr>					
		<tr><th scope='row'>Kode Pos</th>                     <td><input class='form-control' type='number' placeholder='Kode pos' name='j' value='$rows[kode_pos]' required></td></tr>
		<tr><th scope='row'>Keterangan</th>                   <td><input class='form-control' type='text' name='k' value='$rows[keterangan]'></td></tr>
		<tr><th scope='row'>Referral</th>                     <td><input class='form-control' type='text' placeholder='Referral' name='l' value='$rows[referral]' readonly></td></tr>
		<tr><th scope='row'>Ganti Foto</th>                   <td><input type='hidden' name='val_foto' value='$rows[foto]'><input type='file' class='form-control' name='m' >";
		if ($rows['foto'] != ''){ echo "<i style='color:red'>Foto Profile saat ini : </i><a target='_BLANK' href='".base_url()."asset/foto_user/$rows[foto]'>$rows[foto]</a>"; } echo "</td></tr>
		<tr><th scope='row'>Komisi</th>  					  <td>";														
		if ($rows['komisi_nominal'] ==0){
			echo "<input type='radio' name='kom' value='persen' checked> Komisi dalam persen";
		}else{
			echo "<input type='radio' name='kom' value='nominal' checked> Komisi dalam nominal";
		}
		echo"</td></tr>
		<tr><th scope='row'></th>                 	 		 <td>";
		if ($rows['komisi_nominal'] == 0){
			echo"<select name='kp'  style='width:auto' class='form-control' style='width:100%;' required>								
			<option value='' selected>- Pilih Komisi -</option>";								
			$komisi =  $this->db->query("SELECT * FROM komisi");								
			foreach ($komisi->result_array() as $row) {	
				if ($row['komisi']==$rows['komisi']){		
					echo "<option value='$row[komisi]' selected>$row[komisi]</option>";	
				}else{
					echo "<option value='$row[komisi]'>$row[komisi]</option>";
				}
			}
			echo"</select></td></tr>";
		}else{
			echo"<input class='form-control' id='nominal' style='width:300px;' type='number' placeholder='Komisi dalam nominal' name='kn' value='$rows[komisi_nominal]'></input></td></tr>";
		}


		echo"<tr><th></th><td>
		<table>
		<tr>
		<td>Komisi Topsonia <i style='color:red'>*(dalam persen)</i></td><td style='width:30px'>&nbsp;</td><td>Diskon Produk <i style='color:red'>*Khusus Pembeli Umum (dalam persen)</i></td>
		</tr>
		<tr>
		<td><input class='form-control' type='number' name='komisi_topsonia' value='$rows[komisi_topsonia]'></input></td><td style='width:30px'>&nbsp;</td>
		<td><input class='form-control' type='number' name='diskon_buyer' value='$rows[diskon_buyer]'></input> <i style='color:red'></i></td>
		</tr>
		<tr><td colspan='3'><i style='color:red'>* Note : Merupakan pembagian dari Komisi yang di 100 persenkan (Khusus Pembeli Umum)</i></td></tr>
		</table>
		</td></tr>";


		
		echo"<tr><th scope='row'>Pajak PPN</th>     <td><select name='pajak'  style='width:auto' class='form-control' style='width:100%;' required>								
		<option value=''>- Pilih-</option>";
		if ($rows['include_PPN']=='on'){		
			echo "<option value='on' selected>ON</option>
			<option value='off'>OFF</option>";		
			}else{
				echo "<option value='on' >ON</option>
				<option value='off' selected>OFF</option>";
			}
			echo"</td></tr>
			</tbody>
			</table>
			</div>
			</div>
			<div class='box-footer'>
			<button type='submit' name='submit' class='btn btn-info'>Update</button>
			<a href='".base_url()."administrator/reseller'><button type='button' class='btn btn-default pull-right'>Cancel</button></a>
			
			</div>
			</div>";

			?>
			
			<script>
			
			window.onload = function() {
				document.getElementById('persen').style.display = 'none';
				document.getElementById('nominal').style.display = 'none';
			}
			
			$("input[type='radio']").change(function(){
				if($(this).val()=="persen")
				{
			  $("#persen").show();
			  $("#nominal").hide(); 
			  document.getElementById("nominal").value = 0;
			}
			else
			{
				$("#nominal").show(); 
				$("#persen").hide(); 
				document.getElementById("persen").selectedIndex  = 0;
			}

		});
		</script>

		<script>

		$('#upload_pks').bind('change', function() {

//this.files[0].size gets the size of your file.
			if((this.files[0].size/1048576).toFixed(2)>1.00){
				document.getElementById('upload_pks').value="";
				alert('File Maksimal 1 MB');
			}else{
				
			}

		});
		
		</script>
