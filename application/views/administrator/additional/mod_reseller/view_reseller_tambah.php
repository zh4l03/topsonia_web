<script type="text/javascript">
	$(document).on('change', '#merchant_province_id', function(e) {
		$.ajax({
			type: 'POST',
			data: {
				merchant_province_id: $("#merchant_province_id").val()
			},
			url: "<?php echo site_url(); ?>Administrator/api_get_city_admin/",
			success: function(result) {
				var data = JSON.parse(result);
				$("#merchant_city_id").html(data['merchant_city_id']);
			}
		});
	});

	$(document).ready(function() {
		$("#merchant_province_id").change(function() {
			document.getElementById('merchant_province_name').value = this.options[this.selectedIndex].text;

		});
	});
	$(document).ready(function() {
		$("#merchant_city_id").change(function() {
			document.getElementById('merchant_city_name').value = this.options[this.selectedIndex].text;

		});
	});
	$(function() {
		$('#toggle-two').bootstrapToggle({
			on: 'Enabled',
			off: 'Disabled'
		});
	});
</script>

<?php
echo "<div class='col-md-12'>
<div class='box box-info'>
<div class='box-header with-border'>
<h3 class='box-title'>Tambah Data Merchant</h3>
</div>
<div class='box-body'>";
$attributes = array('class' => 'form-horizontal', 'role' => 'form');
echo form_open_multipart('administrator/tambah_reseller', $attributes);
echo "<div class='col-md-12'>
<table class='table table-condensed table-bordered'>
<tbody>
<tr><th width='130px' scope='row'>Username</th>       <td><input class='form-control' type='text' placeholder='Username' name='a' required></td></tr>
<tr><th scope='row'>Password</th>                     <td><input class='form-control' type='password' placeholder='Password' name='b' required></td></tr>
<!--<tr><th scope='row'>Account NCS</th>                  <td><input class='form-control' type='text' placeholder='Account NCS' name='c' required></td></tr>-->
<tr><th scope='row'>Nama Merchant</th>                <td><input class='form-control' type='text' placeholder='Nama merchant' name='d'required></td></tr>
<tr><th scope='row'>No PKS Merchant</th>              <td><input class='form-control' type='text' placeholder='Nomor PKS' name='no_pks'required></td></tr>
<tr><th scope='row'>Upload PKS</th>                   <td><input type='file' class='form-control' name='upload_pks' id='upload_pks' required> 
<i style='color:red'>File Maksimal 1 MB</i>
</td></tr>
<tr><th scope='row'>PIC</th>                  		  <td><input class='form-control' type='text' placeholder='Person in contact' name='e' required></td></tr>
<tr><th scope='row'>Jenis Kelamin</th>                <td>";
if ($rows['jenis_kelamin'] == 'Laki-laki') {
	echo "<input type='radio' value='Laki-laki' name='f' checked required> Laki-laki <input type='radio' value='Perempuan' name='f'> Perempuan ";
} else {
	echo "<input type='radio' value='Laki-laki' name='f'> Laki-laki <input type='radio' value='Perempuan' name='f' checked> Perempuan ";
}
echo "</td></tr>
<tr><th scope='row'>No Hp</th>                        <td><input class='form-control' type='number' placeholder='Telpon' name='g' required></td></tr>
<tr><th scope='row'>Email</th>                 		  <td><input class='form-control' type='email' placeholder='Email' name='h' required></td></tr>                    
<tr><th scope='row'>Alamat Pickup</th>                <td><textarea class='form-control' name='i' placeholder='Nama Gedung, Jalan, dan No Rumah/Kantor Merchant..' class='required' required></textarea></td></tr>					
<tr>
<!--<th scope='row'>Provinsi</th>
<td>
<select class='form-control' name='merchant_province_id' id='merchant_province_id' value='' required>
<option selected disabled value=''>--pilih--</option>";

$url = 'https://apimobile.ptncs.com/php/ncs_deal/getProvinsi.php';

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$server_output = curl_exec($ch);

curl_close($ch);

$data = json_decode($server_output, true);

$i = 0;
foreach ($data as $R1) {
	?>
	<option value="<?php echo $data[$i]['ProID']; ?>"><?php echo $data[$i]['ProName']; ?></option>
	<?php
	$i++;
}

echo "</select>
<input type='hidden' name='merchant_province_name' id='merchant_province_name'>
</td>
</tr>
<tr>
<th scope='row'>Kota</th>
<td>
<select class='form-control' name='merchant_city_id' id='merchant_city_id' value='' required>
<option selected disabled value=''>--pilih--</option>
</select>
<input type='hidden' name='merchant_city_name' id='merchant_city_name'>
</td>
</tr>-->
<tr> <th scope='row'>Provinsi</th>            		  <td> <select class='form-control' name='state' id='prov_id' required> <option value=''>- Pilih -</option>";
foreach ($provinsi as $row) {
	echo "<option value='$row[provinsi_id]' ";
	if (substr($rows['kota_id'], 0, 2) == $row['provinsi_id']) {
		echo "selected";
	};
	echo "> $row[nama_provinsi]</option>";
}
echo "</select></td> </tr>					                    
<tr> <th scope='row'>Kota</th>      				  <td> <select class='form-control' name='kota' id='kab_id' required><option value=''>- Pilih -</option>";
$kab = $this->model_app->view_where_ordering('rb_kota', array('provinsi_id' => substr($rows['kota_id'], 0, 2)), 'kota_id', 'ASC');
foreach ($kab as $row) {
	echo "<option value='$row[kota_id]' ";
	if ($rows['kota_id'] == $row['kota_id']) {
		echo "selected";
	};
	echo "> $row[nama_kota]</option>";
}
echo "</select> </td> </tr>		
<tr><th scope='row'>Kode Pos</th>                     <td><input class='form-control' type='number' placeholder='Kode pos' name='j' required></td></tr>
<tr><th scope='row'>Keterangan</th>                   <td><input class='form-control' type='text' name='k'></td></tr>
<!-- <tr><th scope='row'>Referral</th>                     <td><input class='form-control' type='text' placeholder='Referral' name='l'></td></tr> --!>
<tr><th scope='row'>Foto</th>                         <td><input type='file' class='form-control' name='m' required></td></tr>
<tr><th scope='row'>Komisi</th>  					  <td><input type='radio' name='kom' value='persen'> Komisi dalam persen &nbsp; <input type='radio' name='kom' value='nominal'> Komisi dalam nominal &nbsp;</td></tr>
<tr><th scope='row'></th>								<td><select name='kp' id='persen' style='width:300px;visibility:hide' class='form-control' style='width:100%;' >								
<option value='' selected>- Pilih Komisi dalam persen -</option>";
$komisi =  $this->db->query("SELECT * FROM komisi");
foreach ($komisi->result_array() as $row) {
	echo "<option value='$row[komisi]'>$row[komisi]</option>";
}
echo "</select>
<input class='form-control' id='nominal' style='width:300px;visibility:hide' type='number' placeholder='Komisi dalam nominal' name='kn'></select></td></tr>

<tr><th></th><td>
<table>
<tr>
<td>Komisi Topsonia</td><td style='width:30px'>&nbsp;</td><td>Diskon Produk</td>
</tr>
<tr>
<td><input class='form-control' type='number' name='komisi_topsonia' value=''></input></td><td style='width:30px'>&nbsp;</td>
<td><input class='form-control' type='number' name='diskon_buyer' value=''></input></td>
</tr>
<tr><td colspan='3'><i style='color:red'>* Note : Merupakan pembagian dari Komisi yang di 100 persenkan (Khusus Pembeli Umum)</i></td></tr>
</table>
</td></tr>


<tr><th scope='row'>Pajak PPN</th>     <td><select name='pajak'  style='width:auto' class='form-control' style='width:100%;' required>								
<option value='' selected>- Pilih-</option>
<option value='on'>ON</option>
<option value='off'>OFF</option></td></tr>

<tr><th scope='row'>Kode Referal</th>                        
<td>"; ?>

<input class='form-control' type='hidden' name='kode_r' id='kode_r' value=''>
<input class='form-control' type='text' placeholder='Kode Referal' name='kode_referal' id='kode_referal' onchange='cek_kode();'>
<!-- <button type='button' onclick='cek_kode()' style="margin-top:10px;">Set Referal</button> -->

<?php
echo "</td>
</tr>


</tbody>
</table>
</div>
</div>
<div class='box-footer'>
<button id='smp' type='submit' name='submit' class='btn btn-info' style='display: block;'>Tambah</button>
<button id='rfl_n' type='button' name='submit_b' class='btn btn-info' style='display: none;' onclick='btn_rfl()'>Tambah</button>

<a href='reseller'><button type='button' class='btn btn-default pull-right'>Cancel</button></a>

</div>
</div>";

?>

<script>
	window.onload = function() {
		document.getElementById('persen').style.display = 'none';
		document.getElementById('nominal').style.display = 'none';
	}

	$("input[type='radio']").change(function() {
		if ($(this).val() == "persen") {
			$("#persen").show();
			$("#nominal").hide();
			document.getElementById("nominal").value = 0;
		} else {
			$("#nominal").show();
			$("#persen").hide();
			document.getElementById("persen").selectedIndex = 0;
		}

	});
</script>

<script>
	function cek_kode() {
		let kode = document.getElementById("kode_referal").value;
		$.ajax({
			type: 'POST',
			url: '<?php echo base_url(); ?>administrator/cek_kode_referal',
			dataType: 'JSON',
			data: {
				kode_referal: kode
			},
			success: function(data) {
				if (data.result == '1') {
					document.getElementById("smp").style.display = "block";
					document.getElementById("rfl_n").style.display = "none";
					document.getElementById('kode_r').value = '2';
					alert('Kode Berhasil Di Pasang Atas Nama : ' + data.nama);
				} else {
					document.getElementById("smp").style.display = "none";
					document.getElementById("rfl_n").style.display = "block";
					document.getElementById('kode_r').value = '1';
					alert('Kode Referal Tidak Terdaftar');
				}
			},
			error: function(response) {
				document.getElementById("smp").style.display = "none";
				document.getElementById("rfl_n").style.display = "block";
				document.getElementById('kode_r').value = '1';
				alert('Kode Referal Tidak Terdaftar');
			},
		});
	}

	// function cek_r() {
	// 	if(document.getElementById('kode_referal').value==''){
	// 		document.getElementById("smp").style.display = "block";
	// 		document.getElementById("rfl_n").style.display = "none";
	// 		document.getElementById('kode_r').value='';
	// 	}else{
	// 		document.getElementById("smp").style.display = "none";
	// 		document.getElementById("rfl_n").style.display = "block";
	// 		document.getElementById('kode_r').value='0';
	// 	}
	// 	// alert('ss');		
	// }

	function btn_rfl() {
		if (document.getElementById('kode_r').value == '0') {
			alert('Kode Referal Belum Di Set')
		} else if (document.getElementById('kode_r').value == '1') {
			alert('Kode Referal Tidak Terdaftar Harap Ganti Kode Referal')
		}
	}
	// $(document).ready(function() {
	// 	$("#kode_r").on('change',function(e){
	// 		// cek_kode();
	// 		e.preventDefault();
	// 		let kode = document.getElementById("kode_r").value;
	// 		// alert("<?php echo base_url(); ?>administrator/cek_kode_referal");
	// 		$.ajax({
	// 			type: 'POST',
	// 			url: '<?php echo base_url(); ?>administrator/cek_kode_referal',
	// 			dataType: 'JSON',
	// 			data: {kode_referal: kode},
	// 			success: function(data) {
	// 				if(data.result=='1'){
	// 					alert('Kode Berhasil Di Pasang');
	// 					document.getElementById("kode_referal").value = document.getElementById("kode_r").value;
	// 				}else{
	// 					document.getElementById("kode_referal").value = "";
	// 					alert('Kode Referal Tidak Terdaftar');
	// 				}	
	// 			},
	// 			error: function (response) {
	//            	alert('Kode Referal Tidak Terdaftar');
	//         	},
	// 		});

	// 	});
	// });
</script>

<script>

	$('#upload_pks').bind('change', function() {

//this.files[0].size gets the size of your file.
if((this.files[0].size/1048576).toFixed(2)>1.00){
	document.getElementById('upload_pks').value="";
	alert('File Maksimal 1 MB');
}else{
	
}

});
	
</script>