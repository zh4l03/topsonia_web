<script type="text/javascript">
    $(document).ready(function() {
        $("#skema_diskon").keyup(function() {
            var skema_diskon = $("#skema_diskon").val();
			
            var komisi_topsonia = 100 - parseFloat(skema_diskon);
            $("#komisi_topsonia").val(komisi_topsonia);
			
			var komisi_topsonia_hidden = 100 - parseFloat(skema_diskon);
			$("#komisi_topsonia_hidden").val(komisi_topsonia_hidden);
        });
    });
</script>

<div class='col-md-12'>
  <div class='box box-info'>
	<div class='box-header with-border'>
	  <h3 class='box-title'>Tambah Tipe Komisi</h3>
	</div>
	<div class='box-body'>
	<?php
		$attributes = array('class'=>'form-horizontal','role'=>'form','name'=>'demo');
		echo form_open_multipart('administrator/edit_tipe_konsumen',$attributes); 
	?>

	<table class='table table-condensed table-bordered'>
		<tbody>
			<tr><th scope='row'>Tipe Buyer</th><td><input type='hidden' name='id_tipe_buyer' value='<?php echo $rows['id_tipe_buyer']; ?>'>
			<input type='text' placeholder='Tipe Konsumen' class='form-control' name='tipe_buyer' value='<?php echo $rows['tipe_buyer']; ?>' required></td></tr>
			<tr><th scope='row'>Komisi Buyer (%)</th><td><input value='<?php echo $rows['skema_diskon']; ?>' type='text' class='form-control' placeholder='Masukan Nominal Diskon dalam Persen (%)' name='skema_diskon' id='skema_diskon'><div style='color:red;' required><span>* komisi diatas akan memotong persenan harga dari diskon yang sudah di berikan oleh merchant</span></div></td></tr>
			<tr><th scope='row'>Komisi Topsonia (%)</th><td><input value='<?php echo $rows['komisi_topsonia']; ?>' type='text' placeholder='Terisi Otomatis dari sisa skema diskon' class='form-control' name='komisi' id='komisi_topsonia' readonly>
			<input type='hidden' name='komisi_topsonia' id='komisi_topsonia_hidden'>
			<div style='color:red;'><span>* komisi topsonia dihitung dari sisa persenan komisi buyer</span></div></td></tr>
			<tr><th scope='row'></th><td><button type='submit' name='submit' class='btn btn-info'>Update</button></td></tr>
		</tbody>
	</table>

	<div style='clear:both'></div>
	</div>
  </div>
</div>