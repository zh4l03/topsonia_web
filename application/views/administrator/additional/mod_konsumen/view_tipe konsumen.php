<script type="text/javascript">
    $(document).ready(function() {
        $("#skema_diskon").keyup(function() {
            var skema_diskon = $("#skema_diskon").val();
			
            var komisi_topsonia = 100 - parseFloat(skema_diskon);
            $("#komisi_topsonia").val(komisi_topsonia);
			
			var komisi_topsonia_hidden = 100 - parseFloat(skema_diskon);
			$("#komisi_topsonia_hidden").val(komisi_topsonia_hidden);
        });
    });
</script>

<div class='col-md-6'>
  <div class='box box-info'>
	<div class='box-header with-border'>
	  <h3 class='box-title'>Skema Komisi</h3>
	<!--   <a class='pull-right btn btn-warning btn-sm' href='<?php //echo base_url(); ?>administrator/reseller'>Kembali</a> -->
	</div>
	<div class='box-body'>
		<table id="example1" class="table table-bordered table-striped">
		  <thead>
			<tr>
			  <th style='width:30px; text-align:center;'>No</th>
			  <th>Tipe Buyer</th>
			  <th style="text-align:center;">Komisi Buyer (%)</th>
			  <th style="text-align:center;">Komisi Topsonia (%)</th>
			  <th style="text-align:center;">Action</th>
			</tr>
			</thead>
			<tbody>
				<?php 

                    $no = 1;
                    foreach ($record->result_array() as $row){
				?>
					<tr>
						<td style="text-align:center;"><?php echo $no; ?></td>
						<td><?php echo $row['tipe_buyer']; ?></td>
						<td style="text-align:center;"><?php echo $row['skema_diskon']."%"; ?></td>
						<td style="text-align:center;"><?php echo $row['komisi_topsonia']."%"; ?></td>
						<td style="text-align:center;">
						<a class='btn btn-success btn-xs' title='Edit Data' href='<?php echo base_url();?>administrator/edit_tipe_konsumen/<?php echo $row['id_tipe_buyer'];?>'><span class='glyphicon glyphicon-edit'></span></a>
						
						<!--<a class='btn btn-danger btn-xs' title='Delete Data' href='<?php //echo base_url();?>administrator/delete_tipe_buyer/<?php //echo $row['id_tipe_buyer'];?>' onclick=\"return confirm('Data ini terhubung ke daftar konsumen, Apa anda yakin menghapus Data ini?')\"><span class='glyphicon glyphicon-remove'></span></a>-->
						</td>
					</tr>
				<?php
					$no++;
                    }
				?>
			</tbody>
	  </table>

	  <div style='clear:both'></div>
	</div>
</div>
</div>


<div class='col-md-6'>
  <div class='box box-info'>
	<div class='box-header with-border'>
	  <h3 class='box-title'>Tambah Tipe Komisi</h3>
	</div>
	<div class='box-body'>
	<?php
		$attributes = array('class'=>'form-horizontal','role'=>'form','name'=>'demo');
		echo form_open_multipart('administrator/tambah_tipe_konsumen',$attributes); 
	?>

	<table class='table table-condensed table-bordered'>
		<tbody>
			<tr><th scope='row'>Tipe Buyer<span style='color:red;'><small> *</small></span></th><td><input type='text' placeholder='Tipe Konsumen' class='form-control' name='tipe_buyer' required></td></tr>
			<tr><th scope='row'>Komisi Buyer (%)<span style='color:red;'><small> *</small></span></th><td><input type='text' class='form-control' placeholder='Masukan Nominal Diskon dalam Persen (%)' name='skema_diskon' id='skema_diskon' required><div style='color:red;'><span>* komisi diatas akan memotong persenan harga dari diskon yang sudah di berikan oleh merchant</span></div></td></tr>
			<tr><th scope='row'>Komisi Topsonia (%)</th><td><input type='text' placeholder='Terisi Otomatis dari sisa skema diskon' class='form-control' name='komisi' id='komisi_topsonia' readonly>
			<input type='hidden' name='komisi_topsonia' id='komisi_topsonia_hidden'>
			<div style='color:red;'><span>* komisi topsonia dihitung dari sisa persenan komisi buyer</span></div></td></tr>
			<tr><th scope='row'></th><td><button type='submit' name='submit' class='btn btn-info'>Tambahkan</button></td></tr>
		</tbody>
	</table>

	<div style='clear:both'></div>
	</div>
  </div>
</div>



