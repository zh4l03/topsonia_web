<script type="text/javascript">
  jQuery('#example1').dataTable({  
    "bAutoWidth": true
  }); 

  jQuery('#example').dataTable({  
    "bAutoWidth": true
  }); 

</script>
<div class='col-md-12'>
  <div class='box box-info'>
    <div class='box-body'>
      <div class='panel-body'>
        <ul id='myTabs' class='nav nav-tabs' role='tablist'>
          <li role='presentation' class='active'><a href='#faq' id='faq-tab' role='tab' data-toggle='tab' aria-controls='faq' aria-expanded='true'>About Us</a></li>
          <li role='presentation' class=''><a href='#kategori' role='tab' id='kategori-tab' data-toggle='tab' aria-controls='kategori' aria-expanded='false'>Slider About Us</a></li>
        </ul><br>

        <div id='myTabContent' class='tab-content'>
          <div role='tabpanel' class='tab-pane fade active in' id='faq' aria-labelledby='faq-tab'>
            <?php
            $attributes = array('class'=>'form-horizontal','role'=>'form');
            echo form_open_multipart('administrator/about_us',$attributes); 
            ?>
            <div class='col-md-12'>
              <table class='table table-condensed '>
                <tbody>
                  <tr>
                    <td>Judul About Us</td>
                    <td>
                      <input type='text' value="<?php echo $view_about['judul_about']; ?>" class='form-control' name='judul_about' required>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2">Isi About Us</td>
                  </tr>
                  <tr>
                    <td colspan="2">
                      <textarea id='editor1' class='form-control' name='isi_about'><?php echo $view_about['isi_about']; ?></textarea>
                    </td>
                  </tr>
                 
                </tbody>
              </table>
            </div>
            <div class='box-footer'>
              &nbsp; &nbsp;<button type='submit' name='update_about' class='btn btn-info'>Update</button>
              <a href='<?php echo base_url().$this->uri->segment(1)."/about_us?tab=faq" ?>' class='btn btn-warning'>Batal</a>&nbsp; &nbsp;
              <span style="color: red; text-align: right;">| Updated by: <?php echo $view_about['user']; ?> | <?php echo $view_about['waktu']; ?></span>
            </div>
            <?php

            echo form_close();
            ?>

          </div>

          <div role='tabpanel' class='tab-pane fade' id='kategori' aria-labelledby='kategori-tab'>
            <div class='col-md-12'>
              <div class='box box-info'>
                <div class='box-header with-border'>
                  <h3 class='box-title'><span style="color: blue;"><b>Slider</b></span></h3>
                </div>
                <div class='box-body'>

                  <?php
                  $attributes = array('class'=>'form-horizontal','role'=>'form');
                  echo form_open_multipart('administrator/about_us',$attributes); 
                  ?>

                  <div class='col-md-12'>

                  </div>

                  <div class='box-footer'>
                    <!-- <button type='submit' name='submit_faq' class='btn btn-info'>Tambah</button>
                      <a href='".base_url().$this->uri->segment(1)."/halamanfaq'><button type='button' class='btn btn-default pull-right'>Cancel</button></a> -->
                    </div>
                  </div>
                </div>
              </div>

              
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


  <script>
    $(document).ready(function(){
      var id;
      $(myTabs).click(function(e) {
        e.preventDefault();
      // $(this).tab('show');
      
      $("ul.nav-tabs > li > a").on("shown.bs.tab", function(e) {
        id = $(e.target).attr("href").substr(1);
        window.history.replaceState(null, null, "?tab="+id);
      });
    });

      const urlParams = new URLSearchParams(window.location.search);
      const tabID = urlParams.get('tab');

      if(tabID != null){
        $('#myTabs a[href="#' + tabID +'"]').tab('show');
      }else{
        $('#myTabs a[href="#faq"]').tab('show');
      }

    });
  </script>
