<script type="text/javascript">
  jQuery('#example1').dataTable({  
    "bAutoWidth": true
  }); 

  jQuery('#example').dataTable({  
    "bAutoWidth": true
  }); 

</script>
<div class='col-md-12'>
  <div class='box box-info'>
    <div class='box-body'>
      <div class='panel-body'>
        <ul id='myTabs' class='nav nav-tabs' role='tablist'>
          <li role='presentation' class='active'><a href='#faq' id='faq-tab' role='tab' data-toggle='tab' aria-controls='faq' aria-expanded='true'>Layanan Topsonia</a></li>
          <!-- <li role='presentation' class=''><a href='#kategori' role='tab' id='kategori-tab' data-toggle='tab' aria-controls='kategori' aria-expanded='false'>Kategori FAQ</a></li> -->
        </ul><br>

        <div id='myTabContent' class='tab-content'>
          <div role='tabpanel' class='tab-pane fade active in' id='faq' aria-labelledby='faq-tab'>


            <div class='col-md-12'>
              <div class='box box-info'>
                <div class='box-header with-border'>
                  <h3 class='box-title'><span style="color: blue;"><b><?php if ($this->uri->segment(3) !='') { echo "Update Layanan"; }else{ echo "Tambah Layanan"; } ?></b></span></h3>
                </div>
                <div class='box-body'>

                  <?php
                  $attributes = array('class'=>'form-horizontal','role'=>'form');
                  echo form_open_multipart('administrator/layanan/'.$this->uri->segment(3),$attributes); 
                  ?>
                  <?php
                  if ($this->uri->segment(3) !='') {
                    ?>
                    <div class='col-md-12'>
                      <table class='table table-condensed '>
                        <tbody>
                          <tr>
                            <td>Nama Layanan</td>
                            <td>
                              <input type='text' placeholder="Nama Layanan" class='form-control' name='nama_layanan' value="<?php echo $update_layanan['nama_layanan']; ?>" required>
                            </td>
                          </tr>
                          <tr>
                            <td>Link Redirect</td>
                            <td>
                              <input type='text' placeholder="Link Layanan" class='form-control' name='link' value="<?php echo $update_layanan['link']; ?>" required>
                            </td>
                          </tr>
                          <tr>
                            <td>Logo / Gambar Layanan</td>
                            <td>
                              <input type='file' placeholder="Logo Layanan" class='form-control' name='userfile[]' onchange="document.getElementById('tampil1').src = window.URL.createObjectURL(this.files[0])" id='userfile'>
                              <input type="hidden" name="file_lama" value="<?php echo $update_layanan['logo_layanan']; ?>" id='file_lama'>
                              <table>
                                <tr>
                                  <td style="padding-top: 10px;">
                                    <?php

                                    if($update_layanan['logo_layanan'] == null){
                                      echo "<img id='tampil1' style='border:1px solid #cecece; width:100px' src='".base_url()."asset/foto_produk/thumb/no-image-icon.gif'>";
                                    }else{
                                      ?>
                                      <img id='tampil1' style='border:1px solid #cecece; height:150px;' src="<?php echo base_url().'asset/layanan/'.$update_layanan['logo_layanan']; ?>">
                                      <?php
                                    }
                                    ?>
                                  </td>
                                  <td style="vertical-align:text-top; padding-left:20px; padding-top: 10px;"><span style="color:red;">Single Upload,<br> 
                                   Allowed File : .gif, jpg, png<br>
                                   Max Size : 1 Mbps,
                                 Dimension : 500px x 375px</span>
                                 <div id='dvPreview'></div></td>
                               </tr>
                             </table>


                           </td>
                         </tr>
                         <tr>
                          <td colspan="2">Deskripsi</td>
                        </tr>
                        <tr>
                          <td colspan="2">
                            <textarea class='form-control' name='deskripsi'><?php echo $update_layanan['deskripsi']; ?></textarea>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div class='box-footer'>
                    <button type='submit' name='update_layanan' class='btn btn-info'>Update</button>
                    <a href='<?php echo base_url().$this->uri->segment(1)."/layanan" ?>' class='btn btn-warning'>Batal</a>
                  </div>
                </div>
              </div>
            </div>
            <?php
          }else{
            ?>
            <div class='col-md-12'>
              <table class='table table-condensed '>
                <tbody>
                  <tr>
                    <td>Nama Layanan</td>
                    <td>
                      <input type='text' placeholder="Nama Layanan" class='form-control' name='nama_layanan' required>
                    </td>
                  </tr>
                  <tr>
                    <td>Link Redirect</td>
                    <td>
                      <input type='text' placeholder="Link Layanan" class='form-control' name='link' required>
                    </td>
                  </tr>
                  <tr>
                    <td>Logo / Gambar Layanan</td>
                    <td>
                      <input type='file' placeholder="Pilih Gambar" class='form-control' name='userfile[]' required>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2">Deskripsi</td>
                  </tr>
                  <tr>
                    <td colspan="2">
                      <textarea class='form-control' name='deskripsi'></textarea>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class='box-footer'>
              <button type='submit' name='insert_layanan' class='btn btn-info'>Tambah</button>
              <!-- <a href='".base_url().$this->uri->segment(1)."/halamanfaq'><button type='button' class='btn btn-default pull-right'>Cancel</button></a> -->
            </div>
          </div>
        </div>
      </div>
      <?php
    }
    echo form_close();
    ?>

    <div class='col-md-12'>
      <div class='box box-info'>
        <div class='box-header with-border'>
          <h3 class='box-title'><span style="color: blue;"><b>Daftar Layanan Tersedia</b></span></h3>
        </div>
        <div class="box-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th style='width:30px; text-align:center;'>No</th>
                <th>Nama Layanan</th>
                <th>Link Redirect</th>
                <th style="text-align:center;"></th>
              </tr>
            </thead>
            <tbody>
              <?php 

              $no2 = 1;
              foreach ($record_layanan->result_array() as $rows){
                ?>
                <tr>
                  <td style="text-align:center;"><?php echo $no2; ?></td>
                  <td><?php echo $rows['nama_layanan']; ?></td>
                  <td><a href="<?php echo $rows['link']; ?>" target="_BLANK"><?php echo substr($rows['link'], 0, 50); ?>....</a></td>
                  <td style="text-align:center;">
                    <button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#myModal<?php echo $rows['id_layanan']; ?>"><span class='glyphicon glyphicon-search'></span></button>

                    <a class='btn btn-info btn-xs' title='Edit Layanan' href='<?php echo base_url();?>administrator/layanan/<?php echo $rows['id_layanan'];?>'><span class='glyphicon glyphicon-edit'></span></a>

                    <a class='btn btn-warning btn-xs' title='Delete Layanan' href='<?php echo base_url();?>administrator/deletelayanan/<?php echo $rows['id_layanan'];?>'><span class='glyphicon glyphicon-trash'></span></a>

                    <?php
                    if ($rows['flag']==1) {
                      ?>
                      <a class='btn btn-success btn-xs' title='Aktif' href='<?php echo base_url();?>administrator/onofflayanan/<?php echo $rows['id_layanan'];?>/<?php echo $rows['flag'];?>'><span class='glyphicon glyphicon-off'></span></a>
                      <?php
                    }else{
                      ?>
                      <a class='btn btn-danger btn-xs' title='Tidak Aktif' href='<?php echo base_url();?>administrator/onofflayanan/<?php echo $rows['id_layanan'];?>/<?php echo $rows['flag'];?>'><span class='glyphicon glyphicon-off'></span></a>
                      <?php
                    }
                    ?>
                  </td>
                </tr>
                <?php
                $no2++;
              }
              ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
</div>

<?php
$no = 1;
foreach ($record_layanan ->result_array() as $tampil) {
  ?>
  <div id="myModal<?php echo $tampil['id_layanan']; ?>" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
      <!-- konten modal-->
      <div class="modal-content">
        <!-- heading modal -->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><?php echo $tampil['nama_layanan']; ?></h4>
        </div>
        <!-- body modal -->
        <div class="modal-body">
          <p>
            <table>
              <tr>
                <td>
                  <img style='height:150px;width:200px;' src="<?php echo base_url().'asset/layanan/'.$tampil['logo_layanan']; ?>" alt='<?php echo $tampil['nama_layanan']; ?>' />
                </td>
                <td style="vertical-align: text-top; padding-left: 20px">
                  <b>Link Redirect:</b><br>[ <a href="<?php echo $tampil['link']; ?>" target="_BLANK"><?php echo $tampil['link']; ?></a> ] <br><br>
                  <b>Deskripsi Layanan:</b><br><?php echo $tampil['deskripsi']; ?>
                </td>
              </tr>
            </table>
            
            <br>
            
          </p>
        </div>
      </div>
    </div>
  </div>

  <?php
}
?>


<script>
  $(document).ready(function(){
    var id;
    $(myTabs).click(function(e) {
      e.preventDefault();
      // $(this).tab('show');
      
      $("ul.nav-tabs > li > a").on("shown.bs.tab", function(e) {
        id = $(e.target).attr("href").substr(1);
        window.history.replaceState(null, null, "?tab="+id);
      });
    });
    
    const urlParams = new URLSearchParams(window.location.search);
    const tabID = urlParams.get('tab');
    
    if(tabID != null){
      $('#myTabs a[href="#' + tabID +'"]').tab('show');
    }else{
      $('#myTabs a[href="#faq"]').tab('show');
    }
    
  });

</script>
