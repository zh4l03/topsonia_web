<script type="text/javascript">
  jQuery('#example1').dataTable({  
    "bAutoWidth": true
  }); 

  jQuery('#example').dataTable({  
    "bAutoWidth": true
  }); 

</script>
<div class='col-md-12'>
  <div class='box box-info'>
    <div class='box-body'>
      <div class='panel-body'>
        <ul id='myTabs' class='nav nav-tabs' role='tablist'>
          <li role='presentation' class='active'><a href='#faq' id='faq-tab' role='tab' data-toggle='tab' aria-controls='faq' aria-expanded='true'>Ferequently Asked Question</a></li>
          <li role='presentation' class=''><a href='#kategori' role='tab' id='kategori-tab' data-toggle='tab' aria-controls='kategori' aria-expanded='false'>Kategori FAQ</a></li>
        </ul><br>

        <div id='myTabContent' class='tab-content'>
          <div role='tabpanel' class='tab-pane fade active in' id='faq' aria-labelledby='faq-tab'>


            <div class='col-md-12'>
              <div class='box box-info'>
                <div class='box-header with-border'>
                  <h3 class='box-title'><span style="color: blue;"><b>Tambah FAQ</b></span></h3>
                </div>
                <div class='box-body'>

                  <?php
                  $attributes = array('class'=>'form-horizontal','role'=>'form');
                  echo form_open_multipart('administrator/halamanfaq',$attributes); 
                  ?>
                  <?php
                  if ($this->input->get('idfaq') !='') {
                    ?>
                    <div class='col-md-12'>
                      <table class='table table-condensed '>
                        <tbody>
                          <tr><td>Judul FAQ</td><td>
                            <input type='hidden' value="<?php echo $edit['id_faq']; ?>" name='id_faq' required>
                            <input type='text' value="<?php echo $edit['judul_faq']; ?>" class='form-control' name='judul_faq' required>
                          </td></tr>
                          <tr><td>Kategori FAQ</td>
                            <td>
                              <select class="form-control" name="kategori_faq" required>
                                <option value="<?php echo $edit['id_kategori_faq']; ?>"><b><?php echo $edit['kategori_faq']; ?></b></option>
                                <?php
                                $id_catwomen = $edit['id_kategori_faq'];
                                $catwomen= $this->db->query("SELECT * from kategori_faq where id_kategori_faq NOT IN('$id_catwomen') ORDER BY kategori_faq ASC");
                                foreach ($catwomen->result_array() as $kategori) { 
                                  ?>
                                  <option value="<?php echo $kategori['id_kategori_faq']; ?>"><?php echo $kategori['kategori_faq']; ?></option>
                                <?php } ?>
                              </select>
                            </td></tr>
                            <tr><td colspan="2">Isi FAQ</td></tr>
                            <tr><td colspan="2"><textarea id='editor1' class='form-control' name='isi_faq'><?php echo $edit['isi_faq']; ?></textarea></td></tr>
                          </tbody>
                        </table>
                      </div>
                      <div class='box-footer'>
                        <button type='submit' name='update_faq' class='btn btn-info'>Update</button>
                        <a href='<?php echo base_url().$this->uri->segment(1)."/halamanfaq?tab=faq" ?>' class='btn btn-warning'>Batal</a>
                      </div>
                    </div>
                  </div>
                </div>
                <?php
              }else{
                ?>
                <div class='col-md-12'>
                  <table class='table table-condensed '>
                    <tbody>
                      <tr><td>Judul FAQ</td><td>
                        <input type='text' placeholder="Judul FAQ" class='form-control' name='judul_faq' required>
                      </td></tr>
                      <tr><td>Kategori FAQ</td>
                        <td>
                          <select class="form-control" name="kategori_faq" required>
                            <option value="">--Pilih Ketegori--</option>
                            <?php
                            foreach ($record->result_array() as $kategori) { 
                              ?>
                              <option value="<?php echo $kategori['id_kategori_faq']; ?>"><?php echo $kategori['kategori_faq']; ?></option>
                            <?php } ?>
                          </select>
                        </td></tr>
                        <tr><td colspan="2">Isi FAQ</td></tr>
                        <tr><td colspan="2"><textarea id='editor1' class='form-control' name='isi_faq'><?php echo $edit['isi_faq']; ?></textarea></td></tr>
                      </tbody>
                    </table>
                  </div>
                  <div class='box-footer'>
                    <button type='submit' name='submit_faq' class='btn btn-info'>Tambah</button>
                    <!-- <a href='".base_url().$this->uri->segment(1)."/halamanfaq'><button type='button' class='btn btn-default pull-right'>Cancel</button></a> -->
                  </div>
                </div>
              </div>
            </div>
            <?php
          }
          echo form_close();
          ?>

          <div class='col-md-12'>
            <div class='box box-info'>
              <div class='box-header with-border'>
                <h3 class='box-title'><span style="color: blue;"><b>Daftar Frequently Asked Question</b></span></h3>
              </div>
              <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th style='width:30px; text-align:center;'>No</th>
                      <th>Kategori FAQ</th>
                      <th>FAQ (Frequenly Asked Question)</th>
                      <th style="text-align:center;"></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 

                    $no2 = 1;
                    foreach ($record_faq->result_array() as $rows){
                      ?>
                      <tr>
                        <td style="text-align:center;"><?php echo $no2; ?></td>
                        <td><?php echo $rows['kategori_faq']; ?></td>
                        <td><?php echo $rows['judul_faq']; ?></td>
                        <td style="text-align:center;">
                          <button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#myModal<?php echo $rows['id_faq']; ?>"><span class='glyphicon glyphicon-search'></span></button>

                          <a class='btn btn-info btn-xs' title='Edit FAQ' href='<?php echo base_url();?>administrator/halamanfaq?tab=faq&idfaq=<?php echo $rows['id_faq'];?>'><span class='glyphicon glyphicon-edit'></span></a>

                          <a class='btn btn-warning btn-xs' title='Delete FAQ' href='<?php echo base_url();?>administrator/deletefaq?idfaq=<?php echo $rows['id_faq'];?>'><span class='glyphicon glyphicon-trash'></span></a>

                          <?php
                            if ($rows['flag']==1) {
                          ?>
                            <a class='btn btn-success btn-xs' title='Aktif' href='<?php echo base_url();?>administrator/onoff?idfaq=<?php echo $rows['id_faq'];?>&flag=<?php echo $rows['flag'];?>'><span class='glyphicon glyphicon-off'></span></a>
                          <?php
                            }else{
                          ?>
                              <a class='btn btn-danger btn-xs' title='Tidak Aktif' href='<?php echo base_url();?>administrator/onoff?idfaq=<?php echo $rows['id_faq'];?>&flag=<?php echo $rows['flag'];?>'><span class='glyphicon glyphicon-off'></span></a>
                          <?php
                            }
                          ?>
                          

                        </td>
                      </tr>
                      <?php
                      $no2++;
                    }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>

        <div role='tabpanel' class='tab-pane fade' id='kategori' aria-labelledby='kategori-tab'>
          <div class='col-md-12'>
            <div class="box-info">
              <div class='box-header with-border'>
                <h3 class='box-title'>Tambah Kategori FAQ</h3>
              </div>
              <div class="box-body">
                <!-- <div class='col-md-6'> -->
                  <?php
                  $attributes = array('class'=>'form-horizontal','role'=>'form');
                  echo form_open_multipart('administrator/halamanfaq',$attributes); 
                  ?>

                  <table width="100%" cellpadding="5" class="table table-striped">
                    <tr>
                      <td width="10%"><b>Kategori FAQ</b></td>
                      <td width="70%">
                        <input type='text' placeholder='Kategori FAQ' class='form-control' name='kategori_faq' required>
                      </td>
                      <td width="10%">&nbsp;&nbsp;&nbsp;<button type='submit' name='submit_kategori_faq' class='btn btn-info btn-sm'><span class='glyphicon glyphicon-plus'></span> Tambahkan</button></td>
                    </tr>
                    <tr></tr>
                  </table>
                  <?php
                  echo form_close();
                  ?>
                  <!-- <table><tr><td>&nbsp;</td></tr></table> -->
                </div>
              </div>
            </div>

            <div class='col-md-12'>
              <div class="box-info">
                <div class='box-header with-border'>
                  <h3 class='box-title'>Daftar Kategori FAQ</h3>
                </div>
                <div class="box-body">
                  <table id="example" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th style='width:30px; text-align:center;'>No</th>
                        <th>Kategori FAQ</th>
                        <th style="text-align:center;"></th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                      $attributes = array('class'=>'form-horizontal','role'=>'form');
                      echo form_open_multipart('administrator/halamanfaq',$attributes);
                      $no = 1;
                      foreach ($record->result_array() as $row){
                        ?>
                        <tr>
                          <td style="text-align:center;"><?php echo $no; ?></td>
                          <td>
                            <?php
                            $id_kate = $row['id_kategori_faq'];
                            $id_kat = $this->input->get('id_kat');
                            if ($id_kat!='' && $id_kat==$id_kate) {
                              ?>
                              <input type='hidden' value='<?php echo $id_kate; ?>' name='id_kategori_faq_edit'>
                              <input type='text' value='<?php echo $row['kategori_faq']; ?>' class='form-control' name='kategori_faq_edit'>
                              <?php
                            }else{
                              ?>
                              <?php echo $row['kategori_faq']; ?>
                              <?php
                            }
                            ?>
                          </td>
                          <td style="text-align:center;">
                            <?php
                            $id_kate = $row['id_kategori_faq'];
                            $id_kat = $this->input->get('id_kat');
                            if ($id_kat!='' && $id_kat==$id_kate) {
                              $query_del = $this->db->query("SELECT COUNT(id_kategori_faq) AS jml_kategori FROM `faq` WHERE id_kategori_faq=$id_kat")->row_array();
                              ?>
                              <button class='btn btn-success btn-xs' type="submit" name="edit_kat" title='Edit Kategori'><span class='glyphicon glyphicon-floppy-saved'></span></button>
                              &nbsp;

                              <a class='btn btn-danger btn-xs' title='<?php if ($query_del['jml_kategori'] > 0) { echo "Terdapat ".$query_del['jml_kategori']." data faq pada kategori ini";}else{ echo "Delete Kategori"; } ?>' href='<?php if ($query_del['jml_kategori'] > 0) { echo "#"; }else{ echo base_url()."administrator/deletekategorifaq?idkategorifaq=".$id_kat;; } ?>' <?php if ($query_del['jml_kategori'] > 0) { echo "disabled";}else{ echo ""; } ?>><span class='glyphicon glyphicon-trash'></span></a>

                              &nbsp;
                              <a class='btn btn-warning btn-xs' title='Kembali' href='<?php echo base_url();?>administrator/halamanfaq?tab=kategori'><span class='glyphicon glyphicon-repeat'></span></a>
                              <?php
                            }else{
                              ?>
                              <a class='btn btn-success btn-xs' title='Edit Data' href='<?php echo base_url();?>administrator/halamanfaq?tab=kategori&id_kat=<?php echo $id_kate; ?>'><span class='glyphicon glyphicon-edit'></span></a>
                              <?php
                            }
                            ?>
                          </td>
                        </tr>
                        <?php
                        $no++;
                      }
                      echo form_close();
                      ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php
$no = 1;
foreach ($record_faq ->result_array() as $tampil) {
  ?>
  <div id="myModal<?php echo $tampil['id_faq']; ?>" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
      <!-- konten modal-->
      <div class="modal-content">
        <!-- heading modal -->
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><?php echo $tampil['judul_faq'] ?></h4><h5>(<?php echo "Kategori ".$tampil['kategori_faq']; ?>)</h5>
        </div>
        <!-- body modal -->
        <div class="modal-body">
          <p>
            <?php echo $tampil['isi_faq']; ?>
          </p>
        </div>
      </div>
    </div>
  </div>

  <?php
}
?>


<script>
  $(document).ready(function(){
    var id;
    $(myTabs).click(function(e) {
      e.preventDefault();
      // $(this).tab('show');
      
      $("ul.nav-tabs > li > a").on("shown.bs.tab", function(e) {
        id = $(e.target).attr("href").substr(1);
        window.history.replaceState(null, null, "?tab="+id);
      });
    });
    
    const urlParams = new URLSearchParams(window.location.search);
    const tabID = urlParams.get('tab');
    
    if(tabID != null){
      $('#myTabs a[href="#' + tabID +'"]').tab('show');
    }else{
      $('#myTabs a[href="#faq"]').tab('show');
    }
    
  });
</script>
