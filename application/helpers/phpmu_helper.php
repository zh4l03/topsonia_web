<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require APPPATH.'libraries/phpmailer/src/Exception.php';
require APPPATH.'libraries/phpmailer/src/PHPMailer.php';
require APPPATH.'libraries/phpmailer/src/SMTP.php';

function cek_session_members(){
  $ci = & get_instance();
  $session = $ci->session->userdata('level');
  if ($session != 'konsumen'){
    // echo "<script>window.alert('Maaf, Halaman ini hanya untuk member yang sudah login!');
    // window.location=('".base_url()."')</script>";

    redirect(base_url()."auth/login");
  }
}

function cek_reseller_login(){
  $ci = & get_instance();
  $session = $ci->session->userdata('id_reseller');
  if ($session == ''){
    redirect(base_url()."reseller");
  }
}

function cek_session_reseller(){
  $ci = & get_instance();
  $session = $ci->session->userdata('level');
  if ($session != 'reseller'){
    redirect(base_url());
  }
}

function filter($str){
  return strip_tags(htmlentities($str, ENT_QUOTES, 'UTF-8'));
}

function rupiah($total){
  return number_format($total,0,",",".");
}

function terbilang($x){
  $abil = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
  if ($x < 12)
  return " " . $abil[$x];
  elseif ($x < 20)
  return Terbilang($x - 10) . " Belas";
  elseif ($x < 100)
  return Terbilang($x / 10) . " Puluh" . Terbilang($x % 10);
  elseif ($x < 200)
  return " Seratus" . Terbilang($x - 100);
  elseif ($x < 1000)
  return Terbilang($x / 100) . " Ratus" . Terbilang($x % 100);
  elseif ($x < 2000)
  return " Seribu" . Terbilang($x - 1000);
  elseif ($x < 1000000)
  return Terbilang($x / 1000) . " Ribu" . Terbilang($x % 1000);
  elseif ($x < 1000000000)
  return Terbilang($x / 1000000) . " Juta" . Terbilang($x % 1000000);
}

function cetak($str){
  return strip_tags(htmlentities($str, ENT_QUOTES, 'UTF-8'));
}

function cetak_meta($str,$mulai,$selesai){
  return strip_tags(html_entity_decode(substr(str_replace('"','',$str),$mulai,$selesai), ENT_COMPAT, 'UTF-8'));
}

function sensor($teks){
  $ci = & get_instance();
  $query = $ci->db->query("SELECT * FROM katajelek");
  foreach ($query->result_array() as $r) {
    $teks = str_replace($r['kata'], $r['ganti'], $teks);
  }
  return $teks;
}

function getSearchTermToBold($text, $words){
  preg_match_all('~[A-Za-z0-9_äöüÄÖÜ]+~', $words, $m);
  if (!$m)
  return $text;
  $re = '~(' . implode('|', $m[0]) . ')~i';
  return preg_replace($re, '<b style="color:red">$0</b>', $text);
}

function tgl_indo($tgl){
  $tanggal = substr($tgl,8,2);
  $bulan = getBulan(substr($tgl,5,2));
  $tahun = substr($tgl,0,4);
  return $tanggal.' '.$bulan.' '.$tahun;
}

function tgl_simpan($tgl){
  $tanggal = substr($tgl,0,2);
  $bulan = substr($tgl,3,2);
  $tahun = substr($tgl,6,4);
  return $tahun.'-'.$bulan.'-'.$tanggal;
}

function tgl_view($tgl){
  $tanggal = substr($tgl,8,2);
  $bulan = substr($tgl,5,2);
  $tahun = substr($tgl,0,4);
  return $tanggal.'-'.$bulan.'-'.$tahun;
}

function tgl_grafik($tgl){
  $tanggal = substr($tgl,8,2);
  $bulan = getBulan(substr($tgl,5,2));
  $tahun = substr($tgl,0,4);
  return $tanggal.'_'.$bulan;
}

function generateRandomString($length = 10) {
  return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
}

function seo_title($s) {
  $c = array (' ');
  $d = array ('-','/','\\',',','.','#',':',';','\'','"','[',']','{','}',')','(','|','`','~','!','@','%','$','^','&','*','=','?','+','–');
  $s = str_replace($d, '', $s); // Hilangkan karakter yang telah disebutkan di array $d
  $s = trim(strtolower(str_replace($c, '-', $s))); // Ganti spasi dengan tanda - dan ubah hurufnya menjadi kecil semua
  return $s;
}

function hari_ini($w){
  $seminggu = array("Minggu","Senin","Selasa","Rabu","Kamis","Jumat","Sabtu");
  $hari_ini = $seminggu[$w];
  return $hari_ini;
}

function getBulan($bln){
  switch ($bln){
    case 1:
    return "Jan";
    break;
    case 2:
    return "Feb";
    break;
    case 3:
    return "Mar";
    break;
    case 4:
    return "Apr";
    break;
    case 5:
    return "Mei";
    break;
    case 6:
    return "Jun";
    break;
    case 7:
    return "Jul";
    break;
    case 8:
    return "Agu";
    break;
    case 9:
    return "Sep";
    break;
    case 10:
    return "Okt";
    break;
    case 11:
    return "Nov";
    break;
    case 12:
    return "Des";
    break;
  }
}

function bulan($bln){
  switch ($bln){
    case 1:
    return "Januari";
    break;
    case 2:
    return "Februari";
    break;
    case 3:
    return "Maret";
    break;
    case 4:
    return "April";
    break;
    case 5:
    return "Mei";
    break;
    case 6:
    return "Juni";
    break;
    case 7:
    return "Juli";
    break;
    case 8:
    return "Agustus";
    break;
    case 9:
    return "September";
    break;
    case 10:
    return "Oktober";
    break;
    case 11:
    return "November";
    break;
    case 12:
    return "Desember";
    break;
  }
}

function cek_terakhir($datetime, $full = false) {
  $today = time();
  $createdday= strtotime($datetime);
  $datediff = abs($today - $createdday);
  $difftext="";
  $years = floor($datediff / (365*60*60*24));
  $months = floor(($datediff - $years * 365*60*60*24) / (30*60*60*24));
  $days = floor(($datediff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
  $hours= floor($datediff/3600);
  $minutes= floor($datediff/60);
  $seconds= floor($datediff);
  //year checker
  if($difftext=="")
  {
    if($years>1)
    $difftext=$years." Tahun";
    elseif($years==1)
    $difftext=$years." Tahun";
  }
  //month checker
  if($difftext=="")
  {
    if($months>1)
    $difftext=$months." Bulan";
    elseif($months==1)
    $difftext=$months." Bulan";
  }
  //month checker
  if($difftext=="")
  {
    if($days>1)
    $difftext=$days." Hari";
    elseif($days==1)
    $difftext=$days." Hari";
  }
  //hour checker
  if($difftext=="")
  {
    if($hours>1)
    $difftext=$hours." Jam";
    elseif($hours==1)
    $difftext=$hours." Jam";
  }
  //minutes checker
  if($difftext=="")
  {
    if($minutes>1)
    $difftext=$minutes." Menit";
    elseif($minutes==1)
    $difftext=$minutes." Menit";
  }
  //seconds checker
  if($difftext=="")
  {
    if($seconds>1)
    $difftext=$seconds." Detik";
    elseif($seconds==1)
    $difftext=$seconds." Detik";
  }
  return $difftext;
}

	function kirim_email($subjek,$message,$tujuan){
        $ci = & get_instance();
        $data['subjek'] = $subjek;
        $data['message'] = $message;
        $message_send = $ci->load->view('email_template',$data,TRUE);
        
        $iden = $ci->db->query("SELECT * FROM identitas where id_identitas='1'")->row_array();
        $ci->load->library('email');

        // PHPMailer object
        $response = false;
        //$mail = new PHPMailer();
        $mail =  new PHPMailer;
        $mail->IsSMTP(); 
        $mail->IsHTML(true);
        $mail->SMTPAuth   = true; 
        $mail->Host     = "smtp.googlemail.com";
        $mail->Port     = 465;
        $mail->SMTPSecure   = "ssl";
        //$mail->SMTPDebug  = 2;
        $mail->Username   = "email.topsonia@gmail.com"; //username SMTP
        $mail->Password   = "topsonia2021!";   //password SMTP
        $mail->From       = "email.topsonia@gmail.com"; //sender email
        $mail->FromName   = "Topsonia.com";      //sender name
        $mail->AddAddress($tujuan, "Topsonia");//recipient: email and name
        // $mail->AddAddress("yumnahasiany@gmail.com", "Topsonia");//recipient: email and name
        $mail->Subject    = $subjek;
        $mail->Body       = $message;
       $mail->isHTML(true);
       $mail->Body = $message_send;
       $mail->send();
         //if(!$mail->send()){
           // echo "Message could not be sent.<br>";
                 //"smtp.googlemail.com/ssl/465"<br>";
           // echo 'Mailer Error: ' . $mail->ErrorInfo;
        //}
    }
