<?php 
/*
-- ---------------------------------------------------------------
-- MARKETPLACE MULTI BUYER MULTI SELLER + SUPPORT RESELLER SYSTEM
-- CREATED BY : ROBBY PRIHANDAYA
-- COPYRIGHT  : Copyright (c) 2018 - 2019, PHPMU.COM. (https://phpmu.com/)
-- LICENSE    : http://opensource.org/licenses/MIT  MIT License
-- CREATED ON : 2019-03-26
-- UPDATED ON : 2019-03-27
-- ---------------------------------------------------------------
*/
class Model_reseller extends CI_model{
    function top_menu(){
        return $this->db->query("SELECT * FROM menu where position='Top' ORDER BY urutan ASC");
    }
    
    function testimoni(){
        return $this->db->query("SELECT a.*, b.nama_lengkap, b.id_konsumen FROM testimoni a JOIN rb_konsumen b ON a.id_konsumen=b.id_konsumen ORDER BY a.id_testimoni DESC");
    }

    function testimoni_update(){
        $datadb = array('isi_testimoni'=>$this->input->post('b'),
                            'aktif'=>$this->input->post('f'));
        $this->db->where('id_testimoni',$this->input->post('id'));
        $this->db->update('testimoni',$datadb);
    }

    function testimoni_edit($id){
        return $this->db->query("SELECT a.*, b.nama_lengkap, b.id_konsumen FROM testimoni a JOIN rb_konsumen b ON a.id_konsumen=b.id_konsumen where a.id_testimoni='$id'");
    }

    function testimoni_delete($id){
        return $this->db->query("DELETE FROM testimoni where id_testimoni='$id'");
    }

    function public_testimoni($sampai, $dari){
        return $this->db->query("SELECT a.*, b.nama_lengkap, b.foto, b.id_konsumen, b.jenis_kelamin FROM testimoni a JOIN rb_konsumen b ON a.id_konsumen=b.id_konsumen  where a.aktif='Y' ORDER BY a.id_testimoni DESC LIMIT $dari, $sampai");
    }

    function hitung_testimoni(){
        return $this->db->query("SELECT * FROM testimoni where aktif='Y'");
    }

    function insert_testimoni(){
            $datadb = array('id_konsumen'=>$this->session->id_konsumen,
                            'isi_testimoni'=>$this->input->post('testimoni'),
                            'aktif'=>'N',
                            'waktu_testimoni'=>date('Y-m-d H:i:s'));
        $this->db->insert('testimoni',$datadb);
    }

    function cari_reseller($kata){
        $pisah_kata = explode(" ",$kata);
        $jml_katakan = (integer)count($pisah_kata);
        $jml_kata = $jml_katakan-1;
        $cari = "SELECT * FROM rb_reseller a LEFT JOIN rb_kota b ON a.kota_id=b.kota_id WHERE";
            for ($i=0; $i<=$jml_kata; $i++){
              $cari .= " a.nama_reseller LIKE '%".$pisah_kata[$i]."%' OR b.nama_kota LIKE '%".$pisah_kata[$i]."%' ";
                if ($i < $jml_kata ){
                    $cari .= " OR "; 
                } 
            }
        $cari .= " ORDER BY a.id_reseller DESC LIMIT 36";
        return $this->db->query($cari);
    }

    public function view_join_rows($table1,$table2,$field,$where,$order,$ordering){
        $this->db->select('*');
        $this->db->from($table1);
        $this->db->join($table2, $table1.'.'.$field.'='.$table2.'.'.$field);
        $this->db->where($where);
        $this->db->order_by($order,$ordering);
        return $this->db->get();
    }
	//ini
    function penjualan_list_konsumen($id,$level){
        // return $this->db->query("SELECT * FROM `rb_penjualan` a JOIN rb_konsumen b ON a.id_pembeli=b.id_konsumen where a.status_penjual='$level' AND a.id_penjual='$id' ORDER BY a.id_penjualan DESC");
         return $this->db->query("SELECT DISTINCT a.*,b.nama_lengkap,a.id_penjualan AS idp,a.kode_transaksi AS kt,d.status_pembayaran,d.tanggal_bayar FROM `rb_penjualan` a JOIN rb_konsumen b ON a.id_pembeli=b.id_konsumen LEFT JOIN rb_konfirmasi_pembayaran_konsumen d ON a.kode_transaksi=d.kode_transaksi WHERE d.status_pembayaran IN('lunas','proses refund','refund') and a.status_penjual='$level' AND a.id_penjual='$id' AND a.`order`='1' ORDER BY a.id_penjualan DESC");
    }
	
	function penjualan_list_transaksi($id,$level){
		return $this->db->query("SELECT *, a.id_penjualan as idp  FROM `rb_penjualan` a JOIN rb_konsumen b ON a.id_pembeli=b.id_konsumen JOIN rb_reseller c ON a.id_penjual=c.id_reseller where a.status_penjual='reseller' ORDER BY a.id_penjualan DESC");
	}
	
	function penjualan_list_transaksi_refund($id,$level){
		return $this->db->query("SELECT *, a.id_penjualan as idp FROM `rb_penjualan` a JOIN rb_konsumen b ON a.id_pembeli=b.id_konsumen JOIN rb_reseller c ON a.id_penjual=c.id_reseller LEFT JOIN rb_konfirmasi_pembayaran_konsumen d ON a.id_penjualan=d.id_penjualan where a.status_penjual='reseller' AND a.`order` = '1' ORDER BY a.id_penjualan DESC");
	}
	

    function jual($id){
        return $this->db->query("SELECT sum(a.jumlah) as jual FROM rb_penjualan_detail a JOIN rb_penjualan b ON a.id_penjualan=b.id_penjualan where a.id_produk='$id' AND b.status_penjual='admin' AND b.proses='1' AND a.batal='0' ");
    }

    function beli($id){
        return $this->db->query("SELECT sum(a.jumlah_pesan) as beli FROM rb_pembelian_detail a where a.id_produk='$id'");
    }

    function jual_reseller($penjual, $produk){
        return $this->db->query("SELECT sum(jumlah) as jual FROM `rb_penjualan` a JOIN rb_penjualan_detail b ON a.id_penjualan=b.id_penjualan where a.status_pembeli='konsumen' AND a.status_penjual='reseller' AND a.id_penjual='$penjual' AND b.id_produk='$produk' AND a.proses='1'");
    }

    function beli_reseller($pembeli, $produk){
        return $this->db->query("SELECT sum(jumlah) as beli FROM `rb_penjualan` a JOIN rb_penjualan_detail b ON a.id_penjualan=b.id_penjualan where a.status_pembeli='reseller' AND a.status_penjual='admin' AND a.id_pembeli='$pembeli' AND b.id_produk='$produk' AND a.proses='1'");
    }

    function penjualan_konsumen_detail($id){
        return $this->db->query("SELECT * FROM `rb_penjualan` a JOIN rb_reseller b ON a.id_penjual=b.id_reseller JOIN rb_kota c ON b.kota_id=c.kota_id JOIN rb_konfirmasi_pembayaran_konsumen d ON a.kode_transaksi=d.kode_transaksi where a.kode_transaksi='$id'");
    }

    function profile_konsumen($id){
        return $this->db->query("SELECT a.no_rekening,a.id_tipe_buyer, a.kode_referall, a.id_konsumen, a.username, a.nama_lengkap, a.email, a.jenis_kelamin, a.tanggal_lahir, a.tempat_lahir, a.alamat_lengkap, a.kecamatan, a.no_hp, a.tanggal_daftar, b.kota_id, b.nama_kota as kota, c.provinsi_id, c.nama_provinsi as propinsi FROM `rb_konsumen` a LEFT JOIN rb_kota b ON a.kota_id=b.kota_id LEFT JOIN rb_provinsi c ON b.provinsi_id=c.provinsi_id where a.id_konsumen='$id'");
    }
	
	function alamat_konsumen($id){
        return $this->db->query("SELECT a.id_alamat, a.nama_alamat, a.alamat_lengkap, a.kecamatan, a.kode_pos, a.no_hp, b.kota_id, b.nama_kota as kota, c.provinsi_id, c.nama_provinsi as propinsi FROM `alamat` a LEFT JOIN rb_kota b ON a.kota_id=b.kota_id LEFT JOIN rb_provinsi c ON b.provinsi_id=c.provinsi_id where a.id_konsumen='$id'");
    }

    function orders_report($id,$level){
        return $this->db->query("SELECT * FROM `rb_penjualan` a JOIN rb_reseller b ON a.id_penjual=b.id_reseller where a.status_penjual='$level' AND a.id_pembeli='$id' ORDER BY a.id_penjualan DESC");
    }

    function orders_report_status($id,$level,$status){
        $kondisi = "";
        switch ($status) {
            case "1":
               $kondisi = "AND( ((a.bayar = '0' AND a.proses='0' AND a.selesai='0' AND a.batal='0') OR (a.bayar='2' AND a.pembayaran='manual-transfer' AND a.batal='0' AND c.status_pembayaran='diperiksa')) AND a.pembayaran<>'cod') group by a.id_penjualan order by a.id_penjualan desc";
                break;
            case "2":
                $kondisi = "AND  (c.status_pembayaran='lunas' OR (a.pembayaran='cod' AND a.batal='0' and a.proses='0' and a.selesai='0') )  group by a.id_penjualan order by a.id_penjualan desc";
                break;
            case "3":
                $kondisi = "AND a.proses='1' AND a.selesai='0' group by a.id_penjualan order by a.id_penjualan desc";
                break;
            case "4":
                $kondisi = "AND a.proses='1' AND a.selesai='1' group by a.id_penjualan order by a.id_penjualan desc";
                break;
            case "5":
                $kondisi = "AND (c.status_pembayaran='gagal' OR a.batal='1') group by a.id_penjualan order by a.id_penjualan desc";
                break;
            case "6":
                $kondisi = " group by a.kode_transaksi order by a.id_penjualan desc";
                break;
            default:
                $kondisi = "";
        }
        
        return $this->db->query("SELECT a.*, b.nama_reseller , c.jenis_pembayaran, c.bukti_bayar, c.status_pembayaran , c.remarks FROM `rb_penjualan` a JOIN rb_reseller b ON a.id_penjual=b.id_reseller LEFT OUTER JOIN rb_konfirmasi_pembayaran_konsumen c ON a.kode_transaksi = c.kode_transaksi AND c.flag = '1' where a.status_penjual='$level' AND a.id_pembeli='$id' AND a.order='1' $kondisi ");

    }

    function agenda_terbaru($limit){
        return $this->db->query("SELECT * FROM agenda ORDER BY id_agenda DESC LIMIT $limit");
    }

    public function view_join_where_one($table1,$table2,$field,$where){
        $this->db->select('*');
        $this->db->from($table1);
        $this->db->join($table2, $table1.'.'.$field.'='.$table2.'.'.$field);
        $this->db->where($where);
        return $this->db->get();
    }

    function modupdatefoto(){
        $config['upload_path'] = 'asset/foto_user/';
        $config['allowed_types'] = 'gif|jpg|png|JPG|gif|JPEG|jpeg';
        $config['max_size']     = '1000'; // kb
        $this->load->library('upload', $config);
        $this->upload->do_upload();
        $hasil=$this->upload->data();

        $config['image_library'] = 'gd2';
        $config['source_image'] = 'asset/foto_user/'.$hasil['file_name'];
        $config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = TRUE;
        $config['height']       = 622;
        $this->load->library('image_lib', $config);
        $this->image_lib->crop();

        $datadb = array('foto'=>$hasil['file_name']);
        $this->db->where('id_konsumen',$this->session->id_konsumen);
        $this->db->update('rb_konsumen',$datadb);
    }

    function modupdatefotoreseller(){
        $config['upload_path'] = 'asset/foto_user/';
        $config['allowed_types'] = 'gif|jpg|png|JPG|gif|JPEG|jpeg';
        $config['max_size']     = '1000'; // kb
        $this->load->library('upload', $config);
        $this->upload->do_upload();
        $hasil=$this->upload->data();

        $config['image_library'] = 'gd2';
        $config['source_image'] = 'asset/foto_user/'.$hasil['file_name'];
        $config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = TRUE;
        $config['height']       = 622;
        $this->load->library('image_lib', $config);
        $this->image_lib->crop();

        $datadb = array('foto'=>$hasil['file_name']);
        $this->db->where('id_reseller',$this->session->id_reseller);
        $this->db->update('rb_reseller',$datadb);
    }

    function profile_update($id){
        if (trim($this->input->post('a')) != ''){
            $datadbd = array('username'=>$this->db->escape_str(strip_tags($this->input->post('aa'))),
                            'password'=>hash("sha512", md5($this->input->post('a'))),
                            'nama_lengkap'=>$this->db->escape_str(strip_tags($this->input->post('b'))),
                            'email'=>$this->db->escape_str(strip_tags($this->input->post('c'))),
                            'jenis_kelamin'=>$this->db->escape_str($this->input->post('d')),
                            'tanggal_lahir'=>$this->db->escape_str($this->input->post('e')),
                            'tempat_lahir'=>$this->db->escape_str(strip_tags($this->input->post('f'))),
                            'alamat_lengkap'=>$this->db->escape_str(strip_tags($this->input->post('g'))),
                            'kecamatan'=>$this->db->escape_str(strip_tags($this->input->post('k'))),
                            'kota_id'=>$this->db->escape_str(strip_tags($this->input->post('ga'))),
                            'no_hp'=>$this->db->escape_str(strip_tags($this->input->post('l'))),
                            'no_rekening'=>$this->db->escape_str(strip_tags($this->input->post('no_rekening'))));
        }else{
           $datadbd = array('username'=>$this->db->escape_str(strip_tags($this->input->post('aa'))),
                            'nama_lengkap'=>$this->db->escape_str(strip_tags($this->input->post('b'))),
                            'email'=>$this->db->escape_str(strip_tags($this->input->post('c'))),
                            'jenis_kelamin'=>$this->db->escape_str($this->input->post('d')),
                            'tanggal_lahir'=>$this->db->escape_str($this->input->post('e')),
                            'tempat_lahir'=>$this->db->escape_str(strip_tags($this->input->post('f'))),
                            'alamat_lengkap'=>$this->db->escape_str(strip_tags($this->input->post('g'))),
                            'kecamatan'=>$this->db->escape_str(strip_tags($this->input->post('k'))),
                            'kota_id'=>$this->db->escape_str(strip_tags($this->input->post('ga'))),
                            'no_hp'=>$this->db->escape_str(strip_tags($this->input->post('l'))),
                            'no_rekening'=>$this->db->escape_str(strip_tags($this->input->post('no_rekening'))));
        }
        $this->db->where('id_konsumen',$id);
        $this->db->update('rb_konsumen',$datadbd);
    }
	
	function alamat_update($id){
    $data_alamat = array('nama_alamat'=>$this->db->escape_str(strip_tags($this->input->post('a'))),
        'pic'=>$this->db->escape_str(strip_tags($this->input->post('f'))),
        'alamat_lengkap'=>$this->db->escape_str(strip_tags($this->input->post('b'))),
        'kecamatan'=>$this->db->escape_str(strip_tags($this->input->post('c'))),
        'provinsi_id'=>$this->db->escape_str(strip_tags($this->input->post('ag'))),
        'kota_id'=>$this->db->escape_str(strip_tags($this->input->post('ga'))),
        'kode_pos'=>$this->db->escape_str(strip_tags($this->input->post('d'))),
        'no_hp'=>$this->db->escape_str(strip_tags($this->input->post('e'))));

        $this->db->where('id_alamat',$id);
        $this->db->update('alamat',$data_alamat);
    }
	
	function insert_alamat(){
        $address = array('id_konsumen'=>$this->session->id_konsumen,
            'nama_alamat'=>$this->db->escape_str(strip_tags($this->input->post('a'))),
            'pic'=>$this->db->escape_str(strip_tags($this->input->post('f'))),
            'alamat_lengkap'=>$this->db->escape_str(strip_tags($this->input->post('b'))),
            'kecamatan'=>$this->db->escape_str(strip_tags($this->input->post('c'))),
            'provinsi_id'=>$this->db->escape_str(strip_tags($this->input->post('ag'))),
            'kota_id'=>$this->db->escape_str(strip_tags($this->input->post('ga'))),
            'kode_pos'=>$this->db->escape_str(strip_tags($this->input->post('d'))),
            'no_hp'=>$this->db->escape_str(strip_tags($this->input->post('e'))));
        $this->db->insert('alamat',$address);
    }
	//ini
     function penjualan_list_konsumen_top($id,$level){
        return $this->db->query("SELECT DISTINCT a.*,a.id_penjualan AS idp,a.kode_transaksi AS kt,d.status_pembayaran,d.tanggal_bayar FROM `rb_penjualan` a JOIN rb_konsumen b ON a.id_pembeli=b.id_konsumen LEFT JOIN rb_konfirmasi_pembayaran_konsumen d ON a.kode_transaksi=d.kode_transaksi WHERE a.status_penjual='$level' AND a.id_penjual='$id' AND a.`order`='1' ORDER BY a.id_penjualan DESC LIMIT 10");
    }

    function reseller_pembelian($id,$level){
        return $this->db->query("SELECT * FROM `rb_penjualan` a JOIN rb_reseller b ON a.id_pembeli=b.id_reseller where a.status_penjual='$level' AND a.id_pembeli='$id' ORDER BY a.id_penjualan DESC");
    }

    function penjualan_detail($id){
        return $this->db->query("SELECT * FROM `rb_penjualan` a JOIN rb_reseller b ON a.id_pembeli=b.id_reseller where a.id_penjualan='$id'");
    }
	//ini
    function penjualan_konsumen_detail_reseller($id){
        return $this->db->query("SELECT *, a.id_penjualan as idp ,a.kode_transaksi as kt FROM `rb_penjualan` a JOIN rb_konsumen b ON a.id_pembeli=b.id_konsumen LEFT JOIN rb_konfirmasi_pembayaran_konsumen c ON a.kode_transaksi=c.kode_transaksi where a.kode_transaksi='$id' AND a.id_penjual='".$this->session->id_reseller."'");
        // return $this->db->query("SELECT * FROM `rb_penjualan` a JOIN rb_konsumen b ON a.id_pembeli=b.id_konsumen where a.id_penjualan='$id'");
        // return $this->db->query("SELECT a.kode_transaksi , a.id_pembeli, a.id_penjual, a.id_alamat, a.status_pembeli, a.status_penjual, a.kurir, a.service, a.ongkir, a.diskon, a.diskon_ongkir, a.total_bayar, a.pembayaran, a.waktu_transaksi, waktu_order , c.status_pembayaran FROM `rb_penjualan` a JOIN rb_konsumen b ON a.id_pembeli=b.id_konsumen LEFT JOIN rb_konfirmasi_pembayaran_konsumen c ON a.id_penjualan=c.id_penjualan where a.id_penjualan='$id'");
    }
	
	function penjualan_transaksi_detail($id){
        return $this->db->query("SELECT * FROM `rb_penjualan` a JOIN rb_konsumen b ON a.id_pembeli=b.id_konsumen JOIN rb_reseller c ON a.id_penjual=c.id_reseller where a.id_penjualan='$id'");
    }
	
	function penjualan_transaksi_merchant_detail($id){
        return $this->db->query("SELECT * FROM `rb_penjualan` a JOIN rb_konsumen b ON a.id_pembeli=b.id_konsumen JOIN rb_reseller c ON a.id_penjual=c.id_reseller JOIN rb_konfirmasi_pembayaran_konsumen d ON a.id_penjualan=d.id_penjualan where a.id_penjualan='374'");
    }

    function penjualan_list($id,$level){
        return $this->db->query("SELECT * FROM `rb_penjualan` a JOIN rb_reseller b ON a.id_pembeli=b.id_reseller where a.status_penjual='$level' AND a.id_penjual='$id' ORDER BY a.id_penjualan DESC");
    }

    function pembelian($id_reseller){
        return $this->db->query("SELECT sum((b.jumlah*b.harga_jual)-b.diskon) as total FROM rb_penjualan a JOIN rb_penjualan_detail b ON a.id_penjualan=b.id_penjualan where a.status_penjual='admin' AND a.id_pembeli='".$id_reseller."' AND a.proses='1'");
    }
	// function pembelian($id_reseller){
        // return $this->db->query("SELECT a.total_bayar as total FROM rb_penjualan a JOIN rb_penjualan_detail b ON a.id_penjualan=b.id_penjualan where a.status_penjual='admin' AND a.id_pembeli='".$id_reseller."' AND a.proses='1'");
    // }

    // function penjualan_perusahaan($id_reseller){
        // return $this->db->query("SELECT sum((a.jumlah*a.harga_jual)-a.diskon) as total, sum(a.jumlah) as produk FROM `rb_penjualan_detail` a JOIN rb_produk b ON a.id_produk=b.id_produk JOIN rb_penjualan c ON a.id_penjualan=c.id_penjualan where c.status_penjual='reseller' AND b.id_produk_perusahaan!='0' AND id_penjual='".$id_reseller."' AND c.proses='1'");
    // }

    function penjualan_perusahaan($id_reseller){
        return $this->db->query("SELECT c.total_bayar as total, sum(a.jumlah) as produk FROM `rb_penjualan_detail` a JOIN rb_produk b ON a.id_produk=b.id_produk JOIN rb_penjualan c ON a.id_penjualan=c.id_penjualan where c.status_penjual='reseller' AND id_penjual='".$id_reseller."' AND c.proses='1'");
    }

    function penjualan($id_reseller){
        return $this->db->query("SELECT sum((a.jumlah*a.harga_jual)-a.diskon) as total, sum(a.jumlah) as produk FROM `rb_penjualan_detail` a JOIN rb_produk b ON a.id_produk=b.id_produk
                                    JOIN rb_penjualan c ON a.id_penjualan=c.id_penjualan where c.status_penjual='reseller' AND b.id_produk_perusahaan='0' AND id_penjual='".$id_reseller."' AND c.proses='1'");
    }

    function modal_perusahaan($id_reseller){
        return $this->db->query("SELECT sum(a.jumlah*b.harga_reseller) as total FROM `rb_penjualan_detail` a JOIN rb_produk b ON a.id_produk=b.id_produk JOIN rb_penjualan c ON a.id_penjualan=c.id_penjualan where c.status_pembeli='konsumen' AND c.proses='1' AND c.id_penjual='".$id_reseller."' AND b.id_produk_perusahaan!='0'");
    }

    function modal_pribadi($id_reseller){
        return $this->db->query("SELECT sum(a.jumlah*b.harga_beli) as total FROM `rb_penjualan_detail` a JOIN rb_produk b ON a.id_produk=b.id_produk JOIN rb_penjualan c ON a.id_penjualan=c.id_penjualan where c.status_pembeli='konsumen' AND c.proses='1' AND c.id_penjual='".$id_reseller."' AND b.id_produk_perusahaan='0'");
    }

    function produk_perkategori($id_reseller,$id_produk_perusahaan,$id_kategori_produk,$limit){
        return $this->db->query("SELECT a.*, b.nama_reseller, c.nama_kota FROM rb_produk a LEFT JOIN rb_reseller b ON a.id_reseller=b.id_reseller
                                    LEFT JOIN rb_kota c ON b.kota_id=c.kota_id where a.id_reseller!='$id_reseller' AND a.id_produk_perusahaan='$id_produk_perusahaan' AND a.id_kategori_produk='$id_kategori_produk' ORDER BY a.id_produk ASC LIMIT $limit");
    }

}