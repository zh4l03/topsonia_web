<?php
/*
-- ---------------------------------------------------------------
-- MARKETPLACE MULTI BUYER MULTI SELLER + SUPPORT RESELLER SYSTEM
-- CREATED BY : ROBBY PRIHANDAYA
-- COPYRIGHT  : Copyright (c) 2018 - 2019, PHPMU.COM. (https://phpmu.com/)
-- LICENSE    : http://opensource.org/licenses/MIT  MIT License
-- CREATED ON : 2019-03-26
-- UPDATED ON : 2019-03-27
-- ---------------------------------------------------------------
*/
defined('BASEPATH') OR exit('No direct script access allowed');
class Members extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
        $params = array('server_key' => 'SB-Mid-server-ZITPm-4tKXc1LbcogWCVqPR3', 'production' => false);
		$this->load->library('midtrans');
		$this->midtrans->config($params);
	}

	public function token()
	{
		
		$kode_transaksi = 'TP'.str_pad($this->session->id_konsumen, 5, '0', STR_PAD_LEFT).date('ymdHis');
		
		$transaction_details = array(
		  'order_id' =>$kode_transaksi,
		  'gross_amount' => $this->input->post('total'), // no decimal allowed for creditcard
		);
		
		$konfig  = $this->db->query("SELECT * FROM konfigurasi WHERE id_konfig = 6")->row_array();
		$exp 	= $konfig['value2'];
		
		$time = time();
        $custom_expiry = array(
            'start_time' => date("Y-m-d H:i:s O",$time),
            'unit' => 'hour', 
            'duration'  => $exp
        );

    
		$payment = array($this->input->post('jenis_midtrans'));

        $transaction_data = array(
            'transaction_details'=> $transaction_details,
			'enabled_payments'   => $payment,
            'expiry'             => $custom_expiry
        );

		error_log(json_encode($transaction_data));
		$snapToken = $this->midtrans->getSnapToken($transaction_data);
		error_log($snapToken);
		echo $snapToken;
		if($snapToken!=null){
			$data1 = array("kode_transaksi"=>$kode_transaksi);
			$where1 = array('session'=>$this->session->id_konsumen);
			$this->model_app->update('rb_penjualan_temp', $data1, $where1);	
		
		$ambil_id = $this->db->query("select id_pembeli,id_penjual from temp_penjualan where id_pembeli='".$this->session->id_konsumen."' order by id_penjual ASC")->result_array();
		$y= 1;
		foreach($ambil_id as $row){
			$id_alamat_reseller =  explode(',', $this->input->post("kota$y"));
			$data1 = array( "id_alamat_reseller" => $id_alamat_reseller[1],	
							"kode_transaksi"=>$kode_transaksi,
							"id_alamat"=>$this->input->post('id_alamat'),
							"status_pembeli"=>"konsumen",
							"status_penjual"=>"reseller",
							"kurir"=>$this->input->post("kurir$y"),
							"service"=>$this->input->post("service_code$y"),
							"ongkir"=>$this->input->post("ongkir$y"),
							"diskon_ongkir"=>$this->input->post("diskon_ongkir$y"),
							"asuransi"=>$this->input->post("asuransi$y"),
							"komisi_topsonia"=>$this->input->post("komisi_top$y"),
							"diskon"=>$this->input->post("komisi_user$y"),
							"mdr"=>$this->input->post("total_mdr"),
							"pajak"=>$this->input->post("ppn$y"),
							
							"total_bayar"=>$this->input->post("total"),
							"kategori_pembayaran"=>"midtrans",
							"waktu_order"=>date('ymdHis'),
							"catatan_pelapak"=>$this->input->post("catatan_pelapak$y"),
							"komisi_referal"=>$this->input->post("komisi_referal$y")
						);
			if($this->input->post('jenis_midtrans')=='other_va'){
				$data1['pembayaran']='other_bank';
			}else{
				$data1['pembayaran']=$this->input->post('jenis_midtrans');
			}
			
			$where1 = array('id_penjual'=>$row['id_penjual'],'id_pembeli'=>$this->session->id_konsumen);
			$this->model_app->update('temp_penjualan', $data1, $where1);	
			$y++;
		}
		}
	}

	function foto(){
		cek_session_members();
		if (isset($_POST['submit'])){
			$this->model_reseller->modupdatefoto();
			redirect('members/profile');
		}else{
			redirect('members/profile');
		}
	}

	function profile(){
		cek_session_members();
		$data['title'] = 'Profile Anda';
		$data['row'] = $this->model_reseller->profile_konsumen($this->session->id_konsumen)->row_array();
		$data['record'] = $this->model_app->view_where('alamat',array('id_konsumen'=>$this->session->id_konsumen));
		// $data['address'] = $this->load->view(template().'/reseller/members/view_address');
		$this->template->load(template().'/template',template().'/reseller/view_profile',$data);
	}
	
	function view_address(){
		cek_session_members();			
		$frompage = $this->uri->segment(3);
		$idp = $this->uri->segment(4);
		
		switch ($frompage) {
			case 'checkout':
			$data['back'] = 'checkout';	
			$data['url'] = base_url().'members/checkout/';
			break;
			default:
			$data['back'] = '';	
			$data['url'] = '';
			break;
		}
		$data['row'] = $this->model_reseller->alamat_konsumen($this->session->id_konsumen)->row_array();
		$data['record'] = $this->model_app->view_where('alamat',array('id_konsumen'=>$this->session->id_konsumen));
		$data['idp'] = $idp;
		
		$this->template->load(template().'/template',template().'/reseller/view_address',$data);
	}
	
	function view_address_json(){	
		$data['back'] = 'account';
		$data['record'] = $this->model_app->view_where('alamat',array('id_konsumen'=>$this->session->id_konsumen));
		echo json_encode($this->load->view(template().'/reseller/view_address',$data,true));
	}

	function edit_profile(){
		cek_session_members();
		$id = $this->uri->segment(3);
		if (isset($_POST['submit'])){
			$this->model_reseller->profile_update($this->session->id_konsumen);
			redirect('members/profile');
		}else{
			$data['title'] = 'Edit Profile Anda';
			$data['row'] = $this->model_reseller->profile_konsumen($this->session->id_konsumen)->row_array();
			$row = $this->model_reseller->profile_konsumen($this->session->id_konsumen)->row_array();
			$data['provinsi'] = $this->model_app->view_ordering('rb_provinsi','provinsi_id','ASC');
			$data['rowse'] = $this->db->query("SELECT provinsi_id FROM rb_kota where kota_id='$row[kota_id]'")->row_array();
			$this->template->load(template().'/template',template().'/reseller/view_profile_edit',$data);
		}
	}
	
	function add_address(){
		cek_session_members();
		$data['idp'] = $this->uri->segment(3);
		$data['back'] = $this->uri->segment(4);
		switch ($this->uri->segment(4)) {
			case 'checkout':
			$data['url'] = base_url().'members/view_address/'.$this->uri->segment(4).'/'.$this->uri->segment(3);
			break;
			default:
			$data['url'] = base_url().'members/profile?tab=alamat';
			break;
		}
		$data['title'] = 'Alamat Anda';
		$data['provinsi'] = $this->model_app->view_ordering('rb_provinsi','provinsi_id','ASC');
		$data['rowse'] = $this->db->query("SELECT provinsi_id FROM rb_kota where kota_id='$row[kota_id]'")->row_array();
		$this->template->load(template().'/template',template().'/reseller/view_address_tambah',$data);
	}
	
	function add_address_checkout(){
		cek_session_members();
		$data['title'] = 'Alamat Anda';
		$data['provinsi'] = $this->model_app->view_ordering('rb_provinsi','provinsi_id','ASC');
		$data['rowse'] = $this->db->query("SELECT provinsi_id FROM rb_kota where kota_id='$row[kota_id]'")->row_array();
		$this->template->load(template().'/template',template().'/reseller/view_address_tambah_checkout',$data);
	}
	
	function edit_address(){
		cek_session_members();
		$ids = $this->uri->segment(3);
		// $data['idp'] = $this->uri->segment(3);
		$data['back'] = $this->uri->segment(4);

		if (isset($_POST['submit'])){
			$this->model_reseller->alamat_update($ids);
			switch ($this->uri->segment(4)) {
				case 'checkout':
				redirect('members/view_address/'.$this->uri->segment(4));
				break;
				default:
				redirect('members/profile?tab=alamat');
				break;
			}
		}else{
			switch ($this->uri->segment(4)) {
				case 'checkout':
				$data['url'] = base_url().'members/view_address/'.$this->uri->segment(4);
				break;
				default:
				$data['url'] = base_url().'members/profile?tab=alamat';
				break;
			}
			$data['title'] = 'Alamat Anda';
			$data['row'] =$this->db->query("SELECT * FROM alamat where id_alamat='$ids'")->row_array();
			$data['provinsi'] = $this->model_app->view_ordering('rb_provinsi','provinsi_id','ASC');
			$data['rows'] = $this->db->query("SELECT provinsi_id FROM rb_kota where kota_id='$row[kota_id]'")->row_array();
			$this->template->load(template().'/template',template().'/reseller/view_address_edit',$data);
		}
	}
	
	function delete_address(){
		$ids = array('id_alamat' => $this->uri->segment(3));
		$this->model_app->delete('alamat',$ids);
		$this->session->set_flashdata('success', 'alamat berhasil dihapus');
		switch($this->uri->segment(4)) {
			case 'checkout':
			redirect('members/view_address/'.$this->uri->segment(4).'/'.$this->uri->segment(5));
			$this->session->set_flashdata('success', '');
			break;
			default:
			redirect('members/profile?tab=alamat');
			$this->session->set_flashdata('success', '');
			break;
		}
	}

	function alamat_utama(){
		$ids = $this->uri->segment(3);
		// $idp = $this->uri->segment(4);
		$frompage = $this->uri->segment(4);
		$id_konsumen = $this->session->id_konsumen;
		
		$data = array('alamat_default'=>'0');
		$where = array('id_konsumen' => $id_konsumen, 'alamat_default' => '1');
		$this->model_app->update('alamat', $data, $where);
		
		$data = array('alamat_default'=>'1');
		$where = array('id_alamat' => $ids, 'alamat_default' => '0');
		$this->model_app->update('alamat', $data, $where);
		
		$data1 = array('id_alamat'=> $ids);
		$where1 = array('id_konsumen' => $id_konsumen);
		$this->model_app->update('rb_konsumen', $data1, $where1);
		
		$this->session->set_flashdata('success', 'Berhasil dijadikan alamat utama');
		
		
		switch ($frompage) {
			case 'checkout':
			redirect('members/view_address/'.$frompage);
			$this->session->set_flashdata('success', '');
			break;
			default:
			redirect('members/profile?tab=alamat');
			$this->session->set_flashdata('success', '');
			break;
		}
	}
	

	// function edit_alamat(){
		// cek_session_members();
		// $idp = $this->uri->segment(3);
		// $frompage = $this->uri->segment(4);
		// if (isset($_POST['submit'])){
				// 'nama_alamat'=>$this->input->post('a'),
				// 'alamat_lengkap'=>$this->input->post('b'),
				// 'kecamatan'=>$this->input->post('c'),
				// 'kota_id'=>$this->input->post('d'),
				// 'kode_pos'=>$this->input->post('e'),
				// 'no_hp'=>$this->input->post('f'));
			// $where = array('id_alamat' => $ids);
			// $this->model_app->update('alamat', $data, $where);
			// $this->model_reseller->alamat_update($this->session->id_konsumen);
			// $this->session->set_flashdata('success', 'Berhasil disimpan');

			// switch ($frompage) {
				// case 'checkout':
					// redirect('members/view_address/'.$frompage.'/'.$idp);
					// break;
				// default:
					// redirect('members/profile?tab=alamat');
					// break;
			// }
		// }else{
			// $data['idp'] = $idp;
			// $data['back'] = $back;	
			// $data['title'] = 'Edit Profile Anda';
			// $data['row'] = $this->model_reseller->profile_konsumen($this->session->id_konsumen)->row_array();
			// $row = $this->model_reseller->profile_konsumen($this->session->id_konsumen)->row_array();
			// $data['provinsi'] = $this->model_app->view_ordering('rb_provinsi','provinsi_id','ASC');
			// $data['rowse'] = $this->db->query("SELECT provinsi_id FROM rb_kota where kota_id='$row[kota_id]'")->row_array();
			// $this->template->load(template().'/template',template().'/reseller/view_profile_edit',$data);
		// }
	// }
	
	function tambah_alamat(){
		cek_session_members();
		// $idp = $this->uri->segment(3);
		$frompage = $this->uri->segment(3);
		if (isset($_POST['submit'])){
			$this->model_reseller->insert_alamat($this->session->id_konsumen);
			// $this->alamat_utama();
			switch ($frompage) {
				case 'checkout':
				redirect('members/view_address/'.$frompage);
				break;
				default:
				redirect('members/profile?tab=alamat');
				break;
			}
		}
		else{
			// $data['idp'] = $idp;
			$data['back'] = $back;	
			$data['title'] = 'Tambah Alamat Baru';
			$data['row'] = $this->model_reseller->profile_konsumen($this->session->id_konsumen)->row_array();
			$row = $this->model_reseller->profile_konsumen($this->session->id_konsumen)->row_array();
			$data['provinsi'] = $this->model_app->view_ordering('rb_provinsi','provinsi_id','ASC');
			$data['rowse'] = $this->db->query("SELECT provinsi_id FROM rb_kota where kota_id='$row[kota_id]'")->row_array();
			$this->template->load(template().'/template',template().'/reseller/view_profile_edit',$data);
		}
	}
	

	function reseller(){
		// cek_session_members();
		$jumlah= $this->model_app->view('rb_reseller')->num_rows();
		$config['base_url'] = base_url().'members/reseller';
		$config['total_rows'] = $jumlah;
		$config['per_page'] = 12;
		if ($this->uri->segment('3')==''){
			$dari = 0;
		}else{
			$dari = $this->uri->segment('3');
		}

		if (is_numeric($dari)) {
			$data['title'] = 'Semua Daftar Reseller';
			$this->pagination->initialize($config);
			if (isset($_POST['submit'])){
				$data['record'] = $this->model_reseller->cari_reseller(filter($this->input->post('cari_reseller')));
			}elseif (isset($_GET['cari_reseller'])){
				$data['record'] = $this->model_reseller->cari_reseller(filter($this->input->get('cari_reseller')));
				$total = $this->model_reseller->cari_reseller(filter($this->input->get('cari_reseller')));
				if ($total->num_rows()==1){
					$row = $total->row_array();
					redirect('produk/keranjang/'.$row['id_reseller'].'/'.$this->session->produk);
				}
			}else{
				$data['record'] = $this->db->query("SELECT * FROM rb_reseller a LEFT JOIN rb_kota b ON a.kota_id=b.kota_id ORDER BY id_reseller DESC LIMIT $dari,$config[per_page]");
			}
			$this->template->load(template().'/template',template().'/reseller/view_reseller',$data);
		}else{
			redirect('main');
		}
	}

	function detail_reseller(){
		cek_session_members();
		$data['title'] = 'Detail Profile Reseller';
		$id = $this->uri->segment(3);
		$data['rows'] = $this->model_app->edit('rb_reseller',array('id_reseller'=>$id))->row_array();
		$data['record'] = $this->model_reseller->penjualan_list_konsumen($id,'reseller');
		$data['rekening'] = $this->model_app->view_where('rb_rekening_reseller',array('id_reseller'=>$id));
		$this->template->load(template().'/template',template().'/reseller/view_reseller_detail',$data);

	}

	function finish_midtrans(){
		$pilih = "SELECT id_penjualan FROM `rb_penjualan` WHERE id_pembeli='".$this->session->id_konsumen."' ORDER BY id_penjualan DESC Limit 1";
		$query = $this->db->query($pilih)->row_array()['id_penjualan'];
		redirect('members/checkout/'.$this->session->id_konsumen.'/'.$query);
	}
	

	function save_ulasan(){
		$idp = $this->uri->segment(3);
		$ulasan = $this->db->query("select id_penjualan_detail,id_produk from rb_penjualan_detail where id_penjualan='$idp'")->result_array();
		foreach ($ulasan as $key) {
			$cek_ulasan = $this->db->query("select rating from rb_produk_ulasan where id_penjualan_detail='$key[id_penjualan_detail]'")->num_rows();
				if($cek_ulasan>0){

				}else{
				$data = array(
				'id_penjualan_detail' 	=> $key['id_penjualan_detail'],
				'id_produk' 			=>$key['id_produk'],
				'id_konsumen' 			=> $this->session->id_konsumen,
				'rating' 				=> $this->input->post("rating$key[id_penjualan_detail]"),
				'ulasan'				=> $this->input->post("ulasan$key[id_penjualan_detail]"),
				'waktu_kirim'			=> date("Y-m-d h:i:sa")
				);
					$this->db->insert('rb_produk_ulasan', $data);
				}	
		}

		redirect('members/orders_report');
	}

	function orders_report(){
		cek_session_members();
		$pilih = $this->db->query('select kode_transaksi from rb_penjualan where kategori_pembayaran="midtrans" and bayar="0" and id_pembeli="'.$this->session->id_konsumen.'" group by kode_transaksi')->result_array();
		$cek = $this->model_app->view_where('rb_penjualan',array('kategori_pembayaran'=>'midtrans','bayar'=>'0','id_pembeli'=>$this->session->id_konsumen))->num_rows();
		if($cek>0){
		foreach ($pilih as $pil){
				if($this->midtrans->status($pil['kode_transaksi'])->transaction_status=='settlement'){
					$this->model_app->update('rb_penjualan',array('bayar'=>'1'), array('kode_transaksi'=>$pil['kode_transaksi'],'bayar'=>'0'));
					$this->model_app->update('rb_konfirmasi_pembayaran_konsumen',array('status_pembayaran'=>'lunas'), array('kode_transaksi'=>$pil['kode_transaksi'],'status_pembayaran'=>'diperiksa'));	
				}elseif($this->midtrans->status($pil['kode_transaksi'])->transaction_status=='capture'){
					$this->model_app->update('rb_penjualan',array('bayar'=>'1'), array('kode_transaksi'=>$pil['kode_transaksi'],'bayar'=>'0'));
					$this->model_app->update('rb_konfirmasi_pembayaran_konsumen',array('status_pembayaran'=>'lunas'), array('kode_transaksi'=>$pil['kode_transaksi'],'status_pembayaran'=>'diperiksa'));	
				}elseif($this->midtrans->status($pil['kode_transaksi'])->transaction_status=='expire'){
					$this->model_app->update('rb_penjualan',array('bayar'=>'2'), array('kode_transaksi'=>$pil['kode_transaksi']));
					$this->model_app->update('rb_konfirmasi_pembayaran_konsumen',array('status_pembayaran'=>'gagal'), array('kode_transaksi'=>$pil['kode_transaksi'],'status_pembayaran'=>'diperiksa'));					
				}elseif($this->midtrans->status($pil['kode_transaksi'])->transaction_status=='deny'){
					$this->model_app->update('rb_penjualan',array('bayar'=>'2'), array('kode_transaksi'=>$pil['kode_transaksi']));
					$this->model_app->update('rb_konfirmasi_pembayaran_konsumen',array('status_pembayaran'=>'gagal'), array('kode_transaksi'=>$pil['kode_transaksi'],'status_pembayaran'=>'diperiksa'));					
				}else{
				}
		}
		}
		$data['title'] = 'Data Pesanan Anda';
		// $data['record'] = $this->model_reseller->orders_report($this->session->id_konsumen,'reseller');
		$data['record1'] = $this->model_reseller->orders_report_status($this->session->id_konsumen,'reseller', '1');
		$data['record2'] = $this->model_reseller->orders_report_status($this->session->id_konsumen,'reseller', '2');
		$data['record3'] = $this->model_reseller->orders_report_status($this->session->id_konsumen,'reseller', '3');
		$data['record4'] = $this->model_reseller->orders_report_status($this->session->id_konsumen,'reseller', '4');
		$data['record5'] = $this->model_reseller->orders_report_status($this->session->id_konsumen,'reseller', '5');
		$data['semua'] = $this->model_reseller->orders_report_status($this->session->id_konsumen,'reseller', '6');
		$this->template->load(template().'/template',template().'/reseller/members/view_orders_report',$data);
	}

	// function orders_report_without_template(){
		// cek_session_members();
		// $data['title'] = 'Data Pesanan Anda';
		// $data['record'] = $this->model_reseller->orders_report($this->session->id_konsumen,'reseller');
		// $data['width'] = '100';
		// $this->load->view(template().'/reseller/members/view_orders_report',$data);
	// }

	function orders_report_delete(){
		$idp = $this->uri->segment(3);
		// $this->model_app->delete('rb_penjualan',$idp);
		// $this->model_app->delete('rb_penjualan_detail',$idp);
		$cek = $this->model_app->view_where('rb_penjualan',array('kode_transaksi'=>$idp,'kategori_pembayaran'=>'midtrans'))->num_rows();
		$cek2 = $this->model_app->view_where('rb_konfirmasi_pembayaran_konsumen',array('kode_transaksi'=>$idp))->num_rows();
		if($cek>=1){
			$this->midtrans->cancel($this->model_app->view_where('rb_penjualan',array('kode_transaksi'=>$idp))->row_array()['kode_transaksi']);
		}
		if($cek2<1){
			$nominal = $this->model_app->view_where('rb_penjualan',array('kode_transaksi'=>$idp))->row_array();
			$simpan =array( 'kode_transaksi' 	 	=>$idp,
				'via' 			 	=>$nominal['pembayaran'],
				'nominal'			=> $nominal['total_bayar'],
				'status_pembayaran' => 'gagal',	
			);
			$this->model_app->insert('rb_konfirmasi_pembayaran_konsumen',$simpan);
			$this->model_app->update('rb_penjualan',array('batal'=>'1','bayar'=>'2'),array('kode_transaksi'=>$idp));
		}else{
			$this->model_app->update('rb_penjualan',array('batal'=>'1','bayar'=>'2'),array('kode_transaksi'=>$idp));
			$this->model_app->update('rb_konfirmasi_pembayaran_konsumen',array('status_pembayaran'=>'gagal'),array('kode_transaksi'=>$idp));
		}
		$this->session->set_flashdata('success', 'Transaksi Berhasil Di Batalkan.');
		redirect('members/orders_report');
	}

	function produk_reseller(){
		// cek_session_members();
		$jumlah= $this->model_app->view('rb_produk')->num_rows();
		$config['base_url'] = base_url().'members/produk_reseller/'.$this->uri->segment('3');
		$config['total_rows'] = $jumlah;
		$config['per_page'] = 36;
		if ($this->uri->segment('4')==''){
			$dari = 0;
		}else{
			$dari = $this->uri->segment('4');
		}

		if (is_numeric($dari)) {
			$data['title'] = 'Data Produk Reseller';
			$id = $this->uri->segment(3);
			// $data['rows'] = $this->db->query("SELECT * FROM rb_reseller a JOIN rb_kota b ON a.kota_id=b.kota_id where a.id_reseller='$id'")->row_array();
			// $data['record'] = $this->model_app->view_where_ordering_limit('rb_produk',array('id_reseller!='=>'0'),'id_produk','DESC',$dari,$config['per_page']);
			$data['rows'] = $this->model_app->edit('rb_reseller',array('id_reseller'=>$id))->row_array();
			$data['record'] = $this->model_app->view_where_ordering_limit('rb_produk',array('id_reseller'=>$id),'id_produk','DESC',$dari,$config['per_page']);
			$this->pagination->initialize($config);
			$this->template->load(template().'/template',template().'/reseller/view_reseller_produk',$data);
		}else{
			redirect('main');
		}
	}

	function keranjang(){
		// cek_session_members();
		try {
			$ambil_data = $this->db->query("SELECT * FROM temp_penjualan where id_pembeli='".$this->session->id_konsumen."' order by id_penjual");
		if($ambil_data->row_array()['kode_transaksi']!='' || $ambil_data->row_array()['kode_transaksi']!=null){
			$idp = $ambil_data->row_array()['kode_transaksi'];
		if($this->midtrans->status($idp)->status_message!=null){
			// $cek_other = $this->model_app->view_where('rb_penjualan_temp',array('kode_transaksi'=>$idp))->result_array();
			// $cek_other2 = $this->model_app->view_where('rb_penjualan_temp',array('kode_transaksi'=>$idp))->row_array(0);
			$data1 = array('order'=>'1');
			$result_midtrans = json_decode(json_encode($this->midtrans->status($idp)),true);
			if($ambil_data->row_array()['pembayaran']==='other_bank'){
				$jenis = 'other_bank';
				$kode = $result_midtrans['va_numbers'][0]['va_number'];
			} else if($result_midtrans['payment_type']=='bank_transfer'){
					$status = 'diperiksa';
					$data1['bayar']='0';	
					if($result_midtrans["permata_va_number"]!=null){
						$jenis = 'permata';
						$kode = $result_midtrans['permata_va_number'];
					}else{
					$jenis = $result_midtrans['va_numbers'][0]['bank'];
					$kode = $result_midtrans['va_numbers'][0]['va_number'];
					}
				}else if($result_midtrans['payment_type']=='echannel'){
					$jenis = "mandiri";
					$kode = $result_midtrans['bill_key'];
					$status = 'diperiksa';
					$data1['bayar']='0';
				}else if($result_midtrans['payment_type']=='cstore'){
					$jenis = $result_midtrans['store'];
					$kode = $result_midtrans['payment_code'];
					$status = 'diperiksa';
					$data1['bayar']='0';
				}else{
					if($result_midtrans['transaction_status']==='settlement'){
					$jenis = $result_midtrans['payment_type'];
					$status = 'lunas';
					$data1['bayar']='1';
					$this->session->set_flashdata('success', 'Pembayaran Berhasil');
					}else if($result_midtrans['transaction_status']==='capture'){
						$jenis = $result_midtrans['payment_type'];
						$status = 'lunas';
						$data1['bayar']='1';
						$this->session->set_flashdata('success', 'Pembayaran Berhasil');
					}else{
					$jenis = $result_midtrans['payment_type'];
					$status = 'gagal';
					$data1['bayar']='2';
					$this->session->set_flashdata('danger', 'Pembayaran Gagal');
					}
				}
			$simpan =array( 'kode_transaksi'	=>$idp,
			'via' 			 	=>'midtrans',
			'jenis_pembayaran'	=> $jenis,
			'kode_pembayaran'	=> $kode,
			'nominal'			=> floor($result_midtrans['gross_amount']),
			'status_pembayaran' => $status,	
			'res_transactionNo'	=> $result_midtrans['transaction_id']);
			$this->model_app->insert('rb_konfirmasi_pembayaran_konsumen',$simpan);

			foreach ($ambil_data->result_array() as $row){
			$data1['id_alamat_reseller']=$row['id_alamat_reseller'];
			$data1['kode_transaksi']=$idp;
			$data1['pembayaran']= $jenis;
			$data1['kategori_pembayaran']='midtrans';
			$data1['id_pembeli']=$row['id_pembeli'];
			$data1['id_alamat']=$row['id_alamat'];	
			$data1['id_penjual']=$row['id_penjual'];	
			$data1['status_pembeli']='konsumen';	
			$data1['status_penjual']='reseller';
			$data1['kurir']=$row['kurir'];
			$data1['service']=$row['service'];
			$data1['ongkir']=$row['ongkir'];
			$data1['diskon']=$row['diskon'];
			$data1['flag_reseller'] = '0';
			$data1['flag_admin'] = '0';
			$data1['diskon_ongkir']=$row['diskon_ongkir'];
			$data1['asuransi']=$row['asuransi'];
			$data1['komisi_topsonia']=$row['komisi_topsonia'];
			$data1['mdr']=$row['mdr'];
			$data1['pajak']=$row['pajak'];
			$data1['total_bayar']=$row['total_bayar'];
			$data1['waktu_transaksi']=$row['waktu_transaksi'];
			$data1['waktu_order']=$row['waktu_order'];
			$data1["catatan_pelapak"]=$row['catatan_pelapak'];
			$data1["komisi_referal"] = $row['komisi_referal'];

			$this->model_app->insert('rb_penjualan', $data1);
			$id = $this->db->insert_id();

			$query_wil = $this->db->query("SELECT provinsi_id FROM alamat WHERE id_konsumen = '".$this->session->id_konsumen."' AND alamat_default = '1'")->row_array();
			$wilayah = $query_wil['provinsi_id'];
			$bulan = date("m");
			$tahun = date("Y");
			$no_invoice = str_pad($id, 10, '0', STR_PAD_LEFT)."/".$wilayah."/TPS/".$bulan."/".$tahun;

			$data_i = array('no_invoice'=>$no_invoice);
		    $where = array('id_penjualan' => $id);
		    $this->model_app->update('rb_penjualan', $data_i, $where);
			
			$produk = $this->db->query("SELECT * FROM rb_penjualan_temp a JOIN rb_produk c ON a.id_produk=c.id_produk JOIN rb_reseller d ON c.id_reseller=d.id_reseller JOIN rb_konsumen e ON a.session=e.id_konsumen JOIN tipe_buyer f ON e.id_tipe_buyer=f.id_tipe_buyer WHERE a.session=".$this->session->id_konsumen." and d.id_reseller=".$row['id_penjual']." order by d.id_reseller asc");
			foreach($produk->result_array() as $idpr){ 
				$kom = $this->db->query("SELECT round(((a.harga_jual*a.jumlah)*(c.komisi/100))) as diskon_merchant, round(((a.harga_jual*a.jumlah)*(c.komisi/100))*(f.skema_diskon/100)) as diskon_top, e.komisi as diskon_ongkir FROM `rb_penjualan_temp` a join rb_produk b on a.id_produk=b.id_produk join rb_reseller c ON b.id_reseller=c.id_reseller JOIN rb_konsumen e ON a.session=e.id_konsumen JOIN tipe_buyer f ON e.id_tipe_buyer=f.id_tipe_buyer where a.session='".$this->session->id_konsumen."' and a.id_produk='".$idpr['id_produk']."'")->row_array();
				$data2 = array('id_penjualan'=>$id,
								'id_produk'=>$idpr['id_produk'],
								'jumlah'=>$idpr['jumlah'],
								'diskon'=>$idpr['diskon'],
								'harga_jual'=>$idpr['harga_jual'],
								'satuan'=>$idpr['satuan'],
								'diskon_topsonia'=>$kom['diskon_top'],
								'diskon_merchant'=>$kom['diskon_merchant']);
				$this->model_app->insert('rb_penjualan_detail', $data2);
			}	
			}

			$this->model_app->delete('temp_penjualan',array('id_pembeli'=>$this->session->id_konsumen));
			$this->model_app->delete('rb_penjualan_temp',array('session'=>$this->session->id_konsumen));

			if(in_array($jenis,array("other_bank","bca","bri","mandiri","permata","bni","alfamart","indomaret"))){
				redirect('konfirmasi?idp='.$idp);
			}else{
				redirect('members/orders_report');
			}
			}else{ }
			}
		} catch (Exception $e) {
			echo 'error';
		}
		if ($this->session->id_konsumen=="") {
				$this->session->set_userdata(array('id_konsumen'=>session_id()));
		}
		$id_reseller = $this->uri->segment(3);
		$id_produk = $this->uri->segment(4);
		$btn_type = $this->uri->segment(5);

		$j = $this->model_reseller->jual_reseller($id_reseller,$id_produk)->row_array();
        $b = $this->model_reseller->beli_reseller($id_reseller,$id_produk)->row_array();
        $stok = $b['beli']-$j['jual'];

		if ($id_produk!=''){
			if ($stok <= '0'){
				$produk = $this->model_app->edit('rb_produk',array('id_produk'=>$id_produk))->row_array();
				$produk_cek = filter($produk['nama_produk']);
				$this->session->set_flashdata('warning', 'Maaf, Stok untuk produk '.$produk_cek.' pada merchant ini telah habis.');

				$dat = $this->db->query("SELECT * FROM rb_produk where id_produk='$id_produk' AND id_reseller!='0'");
				$row = $dat->row();
				$data['title'] = $row->nama_produk;
				$data['record'] = $this->model_app->view_where('rb_produk',array('id_produk'=>$id_produk))->row_array();
				$this->template->load(template().'/template',template().'/reseller/view_produk_detail',$data);

				$this->session->set_flashdata('warning', '');


			}else{
				
				$seller = $this->db->query("SELECT id_penjual FROM temp_penjualan where id_pembeli='".$this->session->id_konsumen."' and id_penjual='".$id_reseller."'");			
					
				if ($seller->num_rows() == 0){
					$data2 = array('id_pembeli'=> $this->session->id_konsumen,
									'id_penjual' =>$id_reseller,
									'waktu_transaksi' => date('Y-m-d H:i:s'));
					$this->model_app->insert('temp_penjualan',$data2);
				}else if($seller->num_rows() >= 5){

				}
				$this->session->unset_userdata('produk');

				$cek_j_seller = $this->db->query("SELECT b.id_reseller FROM `rb_penjualan_temp` a join rb_produk b on a.id_produk=b.id_produk  where a.session='".$this->session->id_konsumen."' group by b.id_reseller ");
				if($cek_j_seller->num_rows()==5){
					$cek5 = $this->db->query("SELECT b.id_reseller FROM `rb_penjualan_temp` a join rb_produk b on a.id_produk=b.id_produk  where a.session='".$this->session->id_konsumen."' and b.id_reseller='".$id_reseller."'");
					if($cek5->num_rows()<1){
						// $data['error_reseller'] = "<div class='alert alert-info'><center>Ingat, 1 Transaksi maksimal ke 5 Toko saja,..</center></div>";
						$this->session->set_flashdata('danger', '1 Transaksi Maksimal 5 Toko saja,..');
						redirect('members/keranjang');
					}
					// $this->template->load(template().'/template',template().'/reseller/members/view_keranjang',$data);
				}
				if($this->input->post('qty')!=null){
					$qty = $this->input->post('qty');
				}else{$qty=0;}

				$checkout = $this->db->query("SELECT b.id_reseller FROM `rb_penjualan_temp` a join rb_produk b on a.id_produk=b.id_produk  where a.session='".$this->session->id_konsumen."' and a.id_produk='$id_produk'");
				if ($checkout->num_rows() == 0){
					// $kode_transaksi = 'TP'.str_pad($this->session->id_konsumen, 5, '0', STR_PAD_LEFT).date('ymdHis');
					$harga = $this->model_app->view_where('rb_produk',array('id_produk'=>$id_produk))->row_array();
					$disk = $this->model_app->edit('rb_produk_diskon',array('id_produk'=>$id_produk,'id_reseller'=>$id_reseller))->row_array();
					$harga_konsumen = $harga['harga_konsumen']-$disk['diskon'];
					$data = array('session'=>$this->session->id_konsumen,
								'id_produk'=>$id_produk,
								'jumlah'=>$qty,
								'harga_jual'=>$harga_konsumen,
								'satuan'=>$harga['satuan'],
								'diskon'=>0,
								'waktu_order'=>date('Y-m-d H:i:s'));
					$this->model_app->insert('rb_penjualan_temp',$data);


				}else{
					$where = array('id_produk'=>$id_produk,'session'=>$this->session->id_konsumen);
					$data = array('jumlah'=>$qty);
					$this->model_app->update('rb_penjualan_temp', $data, $where);
				}
				

			
						if($btn_type == 'lanjut_belanja'){
							if($this->input->post('qty')!=null){
								$this->session->set_flashdata('success', 'Berhasil ditambahkan ke keranjang.');
							}
							
							$dat = $this->db->query("SELECT * FROM rb_produk where id_produk='$id_produk' AND id_reseller!='0'");
							$row = $dat->row();
							$data['title'] = $row->nama_produk;
							$data['record'] = $this->model_app->view_where('rb_produk',array('id_produk'=>$id_produk))->row_array();
							$this->template->load(template().'/template',template().'/reseller/view_produk_detail',$data);
		
							$this->session->set_flashdata('success', '');
		
						}else{
							
							$this->session->set_flashdata('success', 'Berhasil ditambahkan ke keranjang.');
							redirect('members/keranjang');
							$this->session->set_flashdata('success', '');
						}
			}
		}else{
			$checkout = $this->db->query("SELECT a.id_penjualan_detail,b.id_reseller,a.kode_transaksi FROM `rb_penjualan_temp` a join rb_produk b on a.id_produk=b.id_produk  where a.session='".$this->session->id_konsumen."'");
			$data['total_checkout'] = $checkout->num_rows();
			$data['id_penjualan_detail'] = $checkout->row_array()['id_penjualan'];
			
			// if ($this->session->idp != ''){
			if ($checkout->num_rows() > 0){

				$this->session->set_userdata(array('idp'=>$checkout->row_array()['id_penjualan']));
				// $data['rows'] = $this->model_reseller->penjualan_konsumen_detail($this->session->idp)->row_array();

				$data['rows'] = $this->db->query("SELECT * FROM `rb_penjualan_temp` a join rb_produk b on a.id_produk=b.id_produk join rb_reseller c ON b.id_reseller=c.id_reseller JOIN rb_kota d ON c.kota_id=d.kota_id where a.session='".$this->session->id_konsumen."' group by c.id_reseller ORDER BY c.id_reseller ASC")->result_array();

				$data['rowsk'] = $this->model_reseller->view_join_where_one('rb_konsumen','rb_kota','kota_id',array('id_konsumen'=>$this->session->id_konsumen))->row_array();
				
				$data['record'] = $this->db->query("SELECT * FROM `rb_penjualan_temp` a join rb_produk b on a.id_produk=b.id_produk join rb_reseller c ON b.id_reseller=c.id_reseller JOIN rb_kota d ON c.kota_id=d.kota_id where a.session='".$this->session->id_konsumen."' ORDER BY a.id_penjualan_detail ASC")->result_array();
		
				// $data['record'] = $this->model_app->view_join_where('rb_penjualan_detail','rb_produk','id_produk',array('id_penjualan'=>$this->session->idp),'id_penjualan_detail','ASC');

				// $data['filter_seller'] = $this->db->query("SELECT a.*,b.id_penjual, c.* FROM rb_penjualan_detail a JOIN rb_penjualan_temp b ON a.id_penjualan=b.id_penjualan JOIN rb_produk c ON a.id_produk=c.id_produk where b.id_pembeli='".$this->session->id_konsumen."' and b.order = '0' ORDER BY b.id_penjual ASC")->result_array();
			}

			// $data['record2'] = $this->db->query("SELECT a.*, c.* FROM rb_penjualan_detail a JOIN rb_penjualan_temp b ON a.id_penjualan=b.id_penjualan JOIN rb_produk c ON a.id_produk=c.id_produk where b.kode_transaksi='".$this->uri->segment(4)."' and b.order = '0' group by b.id_penjual ORDER BY b.id_penjualan asc")->result_array();
			$data['title'] = 'Keranjang Belanja';
			$this->template->load(template().'/template',template().'/reseller/members/view_keranjang',$data);

		}
	}

	function keranjang_detail(){
		cek_session_members();
		$data['rows'] = $this->model_reseller->penjualan_konsumen_detail($this->uri->segment(3))->row_array();
		$data['record'] = $this->model_app->view_join_where('rb_penjualan_detail','rb_produk','id_produk',array('id_penjualan'=>$this->uri->segment(3)),'id_penjualan_detail','ASC');		
		$data['record2'] = $this->model_app->view_where('rb_penjualan',array('id_penjualan'=>$this->uri->segment(3)))->row_array();
		$data['record3'] = $this->model_app->view_where('rb_konfirmasi_refund',array('id_penjualan'=>$this->uri->segment(3)))->row_array();
		$data['title'] = 'Detail Belanja';
		$this->template->load(template().'/template',template().'/reseller/members/view_keranjang_detail',$data);
	}

	function transaksi_detail(){
		cek_session_members();
		$data['rows'] = $this->model_reseller->penjualan_konsumen_detail($this->uri->segment(3))->row_array();
		$data['record'] = $this->db->query("select a.id_penjualan,b.nama_reseller,a.diskon,a.ongkir,a.diskon_ongkir,a.asuransi from rb_penjualan a join rb_reseller b on a.id_penjual=b.id_reseller where kode_transaksi='".$this->uri->segment(3)."'");
		$data['record2'] = $this->model_app->view_where('rb_penjualan',array('kode_transaksi'=>$this->uri->segment(3)))->row_array();
		$data['record3'] = $this->model_app->view_where('rb_konfirmasi_refund',array('kode_transaksi'=>$this->uri->segment(3)))->row_array();
		$data['title'] = 'Detail Belanja';
		$this->template->load(template().'/template',template().'/reseller/members/view_transaksi_detail',$data);
	}
	
	function keranjang_detail_proses(){
		cek_session_members();
		$data['rows'] = $this->model_reseller->penjualan_konsumen_detail($this->uri->segment(3))->row_array();
		$data['record'] = $this->model_app->view_join_where('rb_penjualan_detail','rb_produk','id_produk',array('id_penjualan'=>$this->uri->segment(3)),'id_penjualan_detail','ASC');
		$data['record2'] = $this->model_app->view_where('rb_penjualan',array('id_penjualan'=>$this->uri->segment(3)))->row_array();
		$data['title'] = 'Detail Belanja';
		$this->template->load(template().'/template',template().'/reseller/members/view_tracking_produk',$data);
	}

	function checkout(){
		cek_session_members();
		try {
			$ambil_data = $this->db->query("SELECT * FROM temp_penjualan where id_pembeli='" . $this->session->id_konsumen . "' order by id_penjual");
			if ($ambil_data->row_array()['kode_transaksi'] != '' || $ambil_data->row_array()['kode_transaksi'] != null) {
				$idp = $ambil_data->row_array()['kode_transaksi'];
				if ($this->midtrans->status($idp)->status_message != null) {
					// $cek_other = $this->model_app->view_where('rb_penjualan_temp',array('kode_transaksi'=>$idp))->result_array();
					// $cek_other2 = $this->model_app->view_where('rb_penjualan_temp',array('kode_transaksi'=>$idp))->row_array(0);
					$data1 = array('order' => '1');
					$result_midtrans = json_decode(json_encode($this->midtrans->status($idp)), true);
					if ($ambil_data->row_array()['pembayaran'] === 'other_bank') {
						$jenis = 'other_bank';
						$kode = $result_midtrans['va_numbers'][0]['va_number'];
					} else if ($result_midtrans['payment_type'] == 'bank_transfer') {
						$status = 'diperiksa';
						$data1['bayar'] = '0';
						if ($result_midtrans["permata_va_number"] != null) {
							$jenis = 'permata';
							$kode = $result_midtrans['permata_va_number'];
						} else {
							$jenis = $result_midtrans['va_numbers'][0]['bank'];
							$kode = $result_midtrans['va_numbers'][0]['va_number'];
						}
					} else if ($result_midtrans['payment_type'] == 'echannel') {
						$jenis = "mandiri";
						$kode = $result_midtrans['bill_key'];
						$status = 'diperiksa';
						$data1['bayar'] = '0';
					} else if ($result_midtrans['payment_type'] == 'cstore') {
						$jenis = $result_midtrans['store'];
						$kode = $result_midtrans['payment_code'];
						$status = 'diperiksa';
						$data1['bayar'] = '0';
					} else {
						if ($result_midtrans['transaction_status'] === 'settlement') {
							$jenis = $result_midtrans['payment_type'];
							$status = 'lunas';
							$data1['bayar'] = '1';
							$this->session->set_flashdata('success', 'Pembayaran Berhasil');
						} else if ($result_midtrans['transaction_status'] === 'capture') {
							$jenis = $result_midtrans['payment_type'];
							$status = 'lunas';
							$data1['bayar'] = '1';
							$this->session->set_flashdata('success', 'Pembayaran Berhasil');
						} else {
							$jenis = $result_midtrans['payment_type'];
							$status = 'gagal';
							$data1['bayar'] = '2';
							$this->session->set_flashdata('danger', 'Pembayaran Gagal');
						}
					}
					$simpan = array(
						'kode_transaksi'	=> $idp,
						'via' 			 	=> 'midtrans',
						'jenis_pembayaran'	=> $jenis,
						'kode_pembayaran'	=> $kode,
						'nominal'			=> floor($result_midtrans['gross_amount']),
						'status_pembayaran' => $status,
						'res_transactionNo'	=> $result_midtrans['transaction_id']
					);
					$this->model_app->insert('rb_konfirmasi_pembayaran_konsumen', $simpan);

					foreach ($ambil_data->result_array() as $row) {
						$data1['id_alamat_reseller'] = $row['id_alamat_reseller'];
						$data1['kode_transaksi'] = $idp;
						$data1['pembayaran'] = $jenis;
						$data1['kategori_pembayaran'] = 'midtrans';
						$data1['id_pembeli'] = $row['id_pembeli'];
						$data1['id_alamat'] = $row['id_alamat'];
						$data1['id_penjual'] = $row['id_penjual'];
						$data1['status_pembeli'] = 'konsumen';
						$data1['status_penjual'] = 'reseller';
						$data1['kurir'] = $row['kurir'];
						$data1['service'] = $row['service'];
						$data1['ongkir'] = $row['ongkir'];
						$data1['diskon'] = $row['diskon'];
						$data1['flag_reseller'] = '0';
						$data1['flag_admin'] = '0';
						$data1['diskon_ongkir'] = $row['diskon_ongkir'];
						$data1['asuransi'] = $row['asuransi'];
						$data1['komisi_topsonia'] = $row['komisi_topsonia'];
						$data1['mdr'] = $row['mdr'];
						$data1['pajak'] = $row['pajak'];
						$data1['total_bayar'] = $row['total_bayar'];
						$data1['waktu_transaksi'] = $row['waktu_transaksi'];
						$data1['waktu_order'] = $row['waktu_order'];
						$data1["catatan_pelapak"] = $row['catatan_pelapak'];
						$data1["komisi_referal"] = $row['komisi_referal'];

						$this->model_app->insert('rb_penjualan', $data1);
						$id = $this->db->insert_id();

						$query_wil = $this->db->query("SELECT provinsi_id FROM alamat WHERE id_konsumen = '" . $this->session->id_konsumen . "' AND alamat_default = '1'")->row_array();
						$wilayah = $query_wil['provinsi_id'];
						$bulan = date("m");
						$tahun = date("Y");
						$no_invoice = str_pad($id, 10, '0', STR_PAD_LEFT) . "/" . $wilayah . "/TPS/" . $bulan . "/" . $tahun;

						$data_i = array('no_invoice' => $no_invoice);
						$where = array('id_penjualan' => $id);
						$this->model_app->update('rb_penjualan', $data_i, $where);

						$produk = $this->db->query("SELECT * FROM rb_penjualan_temp a JOIN rb_produk c ON a.id_produk=c.id_produk JOIN rb_reseller d ON c.id_reseller=d.id_reseller JOIN rb_konsumen e ON a.session=e.id_konsumen JOIN tipe_buyer f ON e.id_tipe_buyer=f.id_tipe_buyer WHERE a.session=" . $this->session->id_konsumen . " and d.id_reseller=" . $row['id_penjual'] . " order by d.id_reseller asc");
						foreach ($produk->result_array() as $idpr) {
							$kom = $this->db->query("SELECT round(((a.harga_jual*a.jumlah)*(c.komisi/100))) as diskon_merchant, round(((a.harga_jual*a.jumlah)*(c.komisi/100))*(f.skema_diskon/100)) as diskon_top, e.komisi as diskon_ongkir FROM `rb_penjualan_temp` a join rb_produk b on a.id_produk=b.id_produk join rb_reseller c ON b.id_reseller=c.id_reseller JOIN rb_konsumen e ON a.session=e.id_konsumen JOIN tipe_buyer f ON e.id_tipe_buyer=f.id_tipe_buyer where a.session='" . $this->session->id_konsumen . "' and a.id_produk='" . $idpr['id_produk'] . "'")->row_array();
							$data2 = array(
								'id_penjualan' => $id,
								'id_produk' => $idpr['id_produk'],
								'jumlah' => $idpr['jumlah'],
								'diskon' => $idpr['diskon'],
								'harga_jual' => $idpr['harga_jual'],
								'satuan' => $idpr['satuan'],
								'diskon_topsonia' => $kom['diskon_top'],
								'diskon_merchant' => $kom['diskon_merchant']
							);
							$this->model_app->insert('rb_penjualan_detail', $data2);
						}
					}

					$this->model_app->delete('temp_penjualan', array('id_pembeli' => $this->session->id_konsumen));
					$this->model_app->delete('rb_penjualan_temp', array('session' => $this->session->id_konsumen));

					if (in_array($jenis, array("other_bank", "bca", "bri", "mandiri", "permata", "bni", "alfamart", "indomaret"))) {
						redirect('konfirmasi?idp=' . $idp);
					} else {
						redirect('members/orders_report');
					}
				} else {
				}
			}
		} catch (Exception $e) {
			echo 'error';
		}

		$data['kota_seller'] = $this->db->query("SELECT *,sum( b.berat * a.jumlah ) AS total_berat FROM `rb_penjualan_temp` a join rb_produk b on a.id_produk=b.id_produk join rb_reseller c ON b.id_reseller=c.id_reseller JOIN rb_kota d ON c.kota_id=d.kota_id where a.session='" . $this->session->id_konsumen . "' group by c.id_reseller order by c.id_reseller asc")->result();
		$data['jumlah_seller'] = $this->db->query("SELECT * FROM `rb_penjualan_temp` a join rb_produk b on a.id_produk=b.id_produk join rb_reseller c ON b.id_reseller=c.id_reseller JOIN rb_kota d ON c.kota_id=d.kota_id where a.session='" . $this->session->id_konsumen . "' group by c.id_reseller ORDER BY c.id_reseller asc")->num_rows() + 1;
		$data['adminfee'] = $this->db->query('select value1 from konfigurasi where id_konfig="6"')->row_array();
		$checkout = $this->db->query("SELECT a.id_penjualan_detail,b.id_reseller,a.kode_transaksi FROM `rb_penjualan_temp` a join rb_produk b on a.id_produk=b.id_produk  where a.session='" . $this->session->id_konsumen . "'");
		$data['total_checkout'] = $checkout->num_rows();
		$data['id_penjualan_detail'] = $checkout->row_array()['id_penjualan'];
		$data['rows'] = $this->db->query("SELECT * FROM `rb_penjualan_temp` a join rb_produk b on a.id_produk=b.id_produk join rb_reseller c ON b.id_reseller=c.id_reseller JOIN rb_kota d ON c.kota_id=d.kota_id where a.session='" . $this->session->id_konsumen . "'")->row_array();
		$data['record'] = $this->db->query("SELECT *,c.include_PPN,c.komisi,c.komisi_nominal,f.skema_diskon,sum(a.jumlah*b.berat) AS brt,sum(a.harga_jual*a.jumlah) AS total,ROUND(sum((IF (c.komisi> 0,((IFNULL(c.komisi,0)/100)*a.harga_jual),c.komisi_nominal)*(IFNULL(f.skema_diskon/100,0)))*a.jumlah),0) AS komisi_user,round(((sum(a.harga_jual*a.jumlah))*(c.komisi/100))*(f.komisi_topsonia/100)) AS komisi_top,e.komisi AS diskon_ongkir FROM `rb_penjualan_temp` a JOIN rb_produk b ON a.id_produk=b.id_produk JOIN rb_reseller c ON b.id_reseller=c.id_reseller JOIN rb_kota d ON c.kota_id=d.kota_id JOIN rb_konsumen e ON a.SESSION=e.id_konsumen JOIN tipe_buyer f ON e.id_tipe_buyer=f.id_tipe_buyer WHERE a.SESSION='" . $this->session->id_konsumen . "' GROUP BY c.id_reseller ORDER BY c.id_reseller ASC ")->result_array();
		$data['diskon'] = $this->db->query("SELECT IFNULL(komisi,0) as komisi FROM rb_konsumen where id_konsumen='" . $this->session->id_konsumen . "' ")->row_array();
		$data['alm'] = $this->db->query("SELECT a.kota_id, a.id_alamat,a.nama_alamat,a.pic,a.kode_pos, b.nama_kota, c.nama_provinsi FROM alamat a JOIN rb_kota b ON a.kota_id=b.kota_id JOIN rb_provinsi c ON b.provinsi_id=c.provinsi_id where a.id_konsumen='" . $this->session->id_konsumen . "' and a.alamat_default='1'")->row_array();
		$data['total'] = $this->db->query("SELECT sum(( a.harga_jual * a.jumlah )- a.diskon ) AS total, sum( b.berat * a.jumlah ) AS total_berat FROM rb_penjualan_temp a JOIN rb_produk b ON a.id_produk = b.id_produk JOIN rb_reseller c ON b.id_reseller = c.id_reseller JOIN rb_konsumen d ON a.session = d.id_konsumen JOIN tipe_buyer e ON d.id_tipe_buyer = e.id_tipe_buyer where a.session='" . $this->session->id_konsumen . "' order by c.id_reseller asc")->row_array();
		
		$data['cek_cod'] = $this->db->query("SELECT b.cod FROM `temp_penjualan` a join rb_reseller b ON a.id_penjual=b.id_reseller where a.id_pembeli='" . $this->session->id_konsumen . "' and (b.cod is NULL OR b.cod<>'1')")->num_rows();
		
		$this->template->load(template() . '/template', template() . '/reseller/members/view_checkout', $data);
	}
	
	function keranjang_delete(){
		$idd = $this->uri->segment(3);
		$ida = $this->uri->segment(4);
		$id = array('id_penjualan_detail' => $this->uri->segment(3));
		$this->model_app->delete('rb_penjualan_temp',$id);
		// $seller = $this->db->query('')
				if ($this->session->id_konsumen) {
			$idseson = $this->session->id_konsumen;
		}else{
			$idseson = $this->session->sesi_no_login;
		}
		$isi_keranjang = $this->db->query("SELECT jumlah FROM rb_penjualan_temp where session='".$idseson."'")->num_rows();
		$isi_seller = $this->db->query("SELECT * FROM `rb_penjualan_temp` a join rb_produk b on a.id_produk=b.id_produk join rb_reseller c ON b.id_reseller=c.id_reseller JOIN rb_kota d ON c.kota_id=d.kota_id where a.session='".$idseson."' and c.id_reseller='".$ida."'")->num_rows();


		if ($isi_seller<1){
			$this->model_app->delete('temp_penjualan',array('id_pembeli'=>$idseson,'id_penjual'=>$ida));
			// $idp = array('session' => $idd);
			// $this->model_app->delete('rb_penjualan_temp',$idp);
		}
		if ($isi_keranjang<1){
			$this->model_app->delete('temp_penjualan',array('id_pembeli'=>$idseson));
			// $idp = array('session' => $idd);
			// $this->model_app->delete('rb_penjualan_temp',$idp);
			$this->session->unset_userdata('idp');
		}
		if ($this->session->id_konsumen!='') {
			redirect('members/keranjang');
		}else{
			redirect('produk/keranjang');
		}
	}

	function selesai_belanja()
	{
		$idp = $this->uri->segment(3);
		$idr = $this->uri->segment(4);
		$iden = $this->model_app->view_where('identitas', array('id_identitas' => '1'))->row_array();

		$data['rekening_reseller'] = $this->model_app->view('rb_rekening');
		$data1 = array(
			'kategori_pembayaran' => $this->input->post('pembayaran'),
			'diskon' => $this->input->post('diskon'),
			'total_bayar' => $this->input->post('total_bayar'),
			'id_alamat' => $this->input->post('id_alamat'),
			'mdr' 	=> $this->input->post('mdr'),
			'pajak' => $this->input->post('pajak'),
			'order' => '1',
			'waktu_order' => date('Y-m-d H:i:s')
		);
		if ($this->input->post('pembayaran') == 'midtrans') {
			// $result_midtrans = json_decode(json_encode($this->midtrans->status($idp),true);
			$result_midtrans = json_decode($this->input->post('result_data'), true);
			if ($this->input->post('jenis_mdr') == 'other_va') {
				$jenis = 'other_bank';
				$kode = $result_midtrans['va_numbers'][0]['va_number'];
			} else if ($result_midtrans['payment_type'] == 'bank_transfer') {
				$status = 'diperiksa';
				$data1['bayar'] = '0';
				if ($result_midtrans["permata_va_number"] != null) {
					$jenis = 'permata';
					$kode = $result_midtrans['permata_va_number'];
				} else {
					$jenis = $result_midtrans['va_numbers'][0]['bank'];
					$kode = $result_midtrans['va_numbers'][0]['va_number'];
				}
			} else if ($result_midtrans['payment_type'] == 'echannel') {
				$jenis = "mandiri";
				$kode = $result_midtrans['bill_key'];
				$status = 'diperiksa';
				$data1['bayar'] = '0';
			} else if ($result_midtrans['payment_type'] == 'cstore') {
				$jenis = $result_midtrans['store'];
				$kode = $result_midtrans['payment_code'];
				$status = 'diperiksa';
				$data1['bayar'] = '0';
			} else {
				if ($result_midtrans['transaction_status'] === 'settlement') {
					$jenis = $result_midtrans['payment_type'];
					$status = 'lunas';
					$data1['bayar'] = '1';
					$this->session->set_flashdata('success', 'Pembayaran Berhasil');
				} else if ($result_midtrans['transaction_status'] === 'capture') {
					$jenis = $result_midtrans['payment_type'];
					$status = 'lunas';
					$data1['bayar'] = '1';
					$this->session->set_flashdata('success', 'Pembayaran Berhasil');
				} else {
					$jenis = $result_midtrans['payment_type'];
					$status = 'gagal';
					$data1['bayar'] = '2';
					$this->session->set_flashdata('danger', 'Pembayaran Gagal');
				}
			}
			$simpan = array(
				'kode_transaksi' 	=> $result_midtrans['order_id'],
				'via' 			 	=> 'midtrans',
				'jenis_pembayaran'	=> $jenis,
				'kode_pembayaran'		=> $kode,
				'nominal'				=> $this->input->post('total_bayar'),
				'status_pembayaran'   => $status,
				'res_transactionNo'	=> $result_midtrans['transaction_id']
			);
			$this->model_app->insert('rb_konfirmasi_pembayaran_konsumen', $simpan);
			$idp = $result_midtrans['order_id'];
			$data1['pembayaran'] = $jenis;
		} else {
			$data1['pembayaran'] = $this->input->post('pembayaran');
		}

		$ambil_id = $this->db->query("select * from temp_penjualan where id_pembeli='" . $this->session->id_konsumen . "' order by id_penjual ASC")->result_array();
		$y = 1;
		foreach ($ambil_id as $row) {
			$id_alamat_reseller =  explode(',', $this->input->post("kota$y"));
			$data1['id_alamat_reseller']  = $id_alamat_reseller[1];
			$data1['kurir']  = $this->input->post("kurir$y");
			$data1['ongkir'] = $this->input->post("ongkir$y");
			$data1['diskon_ongkir'] = $this->input->post("diskon_ongkir$y");
			$data1['service'] = $this->input->post("service_code$y");
			$data1['asuransi'] = $this->input->post("asr$y");
			$data1['komisi_topsonia'] = $this->input->post("komisi_top$y");
			$data1['diskon'] = $this->input->post("komisi_user$y");
			$data1["catatan_pelapak"] = $this->input->post("catatan_pelapak$y");
			$data1['pajak'] = $this->input->post("include_ppn$y");
			$data1['komisi_referal'] = $this->input->post("komisi_referal$y");
			if ($this->input->post('pembayaran') == 'midtrans') {
				$kode_transaksi = $row['kode_transaksi'];
			} else {
				$kode_transaksi = 'TP' . str_pad($this->session->id_konsumen, 5, '0', STR_PAD_LEFT) . date('ymdHis');
			}
			$data1['kode_transaksi'] = $kode_transaksi;
			$data1['id_pembeli'] = $this->session->id_konsumen;
			$data1['id_penjual'] = $row['id_penjual'];
			$data1['flag_reseller'] = '0';
			$data1['flag_admin'] = '0';
			$data1['status_penjual'] = 'reseller';
			$data1['status_pembeli'] = 'konsumen';
			$data1['waktu_transaksi'] = $row['waktu_transaksi'];

			// if ($this->input->post('pembayaran') === 'cod') {
			// 	redirect('members/orders_report');
			// }

			$this->model_app->insert('rb_penjualan', $data1);
			$id = $this->db->insert_id();

			$query_wil = $this->db->query("SELECT provinsi_id FROM alamat WHERE id_konsumen = '" . $this->session->id_konsumen . "' AND alamat_default = '1'")->row_array();
			$wilayah = $query_wil['provinsi_id'];
			$bulan = date("m");
			$tahun = date("Y");
			$no_invoice = str_pad($id, 10, '0', STR_PAD_LEFT) . "/" . $wilayah . "/TPS/" . $bulan . "/" . $tahun;

			$data_i = array('no_invoice' => $no_invoice);
			$where = array('id_penjualan' => $id);
			$this->model_app->update('rb_penjualan', $data_i, $where);

			$idp = $kode_transaksi;
			$produk = $this->db->query("SELECT * FROM rb_penjualan_temp a JOIN rb_produk c ON a.id_produk=c.id_produk JOIN rb_reseller d ON c.id_reseller=d.id_reseller JOIN rb_konsumen e ON a.session=e.id_konsumen JOIN tipe_buyer f ON e.id_tipe_buyer=f.id_tipe_buyer WHERE a.session=" . $this->session->id_konsumen . " and d.id_reseller=" . $row['id_penjual'] . " order by d.id_reseller asc");
			// $data['record'] = $this->db->query("SELECT *,sum(a.jumlah*b.berat) as brt, sum(a.harga_jual*a.jumlah) as total, round(((sum(a.harga_jual*a.jumlah))*(c.komisi/100))*(f.skema_diskon/100)) as komisi_user, round(((sum(a.harga_jual*a.jumlah))*(c.komisi/100))*(f.komisi_topsonia/100)) as komisi_top, e.komisi as diskon_ongkir FROM `rb_penjualan_temp` a join rb_produk b on a.id_produk=b.id_produk join rb_reseller c ON b.id_reseller=c.id_reseller JOIN rb_kota d ON c.kota_id=d.kota_id JOIN rb_konsumen e ON a.session=e.id_konsumen JOIN tipe_buyer f ON e.id_tipe_buyer=f.id_tipe_buyer where a.session='".$this->session->id_konsumen."' group by c.id_reseller ORDER BY c.id_reseller ASC")->result_array();

			foreach ($produk->result_array() as $idpr) {
				$kom = $this->db->query("SELECT round(((a.harga_jual*a.jumlah)*(c.komisi/100))) as diskon_merchant, round(((a.harga_jual*a.jumlah)*(c.komisi/100))*(f.skema_diskon/100)) as diskon_top FROM `rb_penjualan_temp` a join rb_produk b on a.id_produk=b.id_produk join rb_reseller c ON b.id_reseller=c.id_reseller JOIN rb_konsumen e ON a.session=e.id_konsumen JOIN tipe_buyer f ON e.id_tipe_buyer=f.id_tipe_buyer where a.session='" . $this->session->id_konsumen . "' and a.id_produk='" . $idpr['id_produk'] . "'")->row_array();
				$data2 = array(
					'id_penjualan' => $id,
					'id_produk' => $idpr['id_produk'],
					'jumlah' => $idpr['jumlah'],
					'diskon' => $idpr['diskon'],
					'harga_jual' => $idpr['harga_jual'],
					'satuan' => $idpr['satuan'],
					'diskon_topsonia' => $kom['diskon_top'],
					'diskon_merchant' => $kom['diskon_merchant']
				);
				$this->model_app->insert('rb_penjualan_detail', $data2);
			}
			$y++;
		}
		$this->model_app->delete('rb_penjualan_temp', array('session' => $this->session->id_konsumen));
		$this->model_app->delete('temp_penjualan', array('id_pembeli' => $this->session->id_konsumen));

		$mod = $this->model_app->view_where('rb_penjualan', array('id_penjualan' => $id))->row_array();
		$cekres = $this->db->query("select * from rb_penjualan_temp where session='" . $this->session->id_konsumen . "'")->row_array();
		$kons = $this->model_reseller->profile_konsumen($this->session->id_konsumen)->row_array();
		$als = $this->db->query("SELECT a.*, b.nama_kota, c.nama_provinsi FROM alamat a JOIN rb_kota b ON a.kota_id=b.kota_id JOIN rb_provinsi c ON b.provinsi_id=c.provinsi_id  JOIN rb_penjualan d ON a.id_alamat=d.id_alamat where d.id_alamat='" . $mod['id_alamat'] . "'")->row_array();
		$res = $this->model_app->view_where('rb_reseller', array('id_reseller' => $mod['id_penjual']))->row_array();

		//email konsumen
		$tujuan = $kons['email'];
		$tglaktif = date("d-m-Y");

		$subjek     = "$iden[nama_website] - Detail Orderan anda";
		$message      = "<html><body>Hai " . $kons['nama_lengkap'] . ", <br> Pesanan #<b>" . $mod['kode_transaksi'] . "</b> 
							Berhasil diorder Hari ini " . $tglaktif . " di website Topsonia .<br>";

		if ($this->input->post('pembayaran') == 'manual-transfer') {
			$message .= "Silahkan melakukan pembayaran ke rekening berikut :
					  <table style='width:100%;' class='table table-hover table-condensed'>
						<thead>
						  <tr bgcolor='#f08519'>
							<th width='20px'>No</th>
							<th>Nama Bank</th>
							<th>No Rekening</th>
							<th>Atas Nama</th>
						  </tr>
						</thead>
						<tbody>";
			$noo = 1;
			$rekening = $this->model_app->view('rb_rekening');
			foreach ($rekening->result_array() as $row) {
				$message .= "<tr bgcolor='#e3e3e3'><td>$noo</td>
									  <td>$row[nama_bank]</td>
									  <td>$row[no_rekening]</td>
									  <td>$row[pemilik_rekening]</td>
								  </tr>";
				$noo++;
			}
			$message .= "</tbody>
					  </table><br><br>
				Jika sudah melakukan transfer, jangan lupa konfirmasi transferan anda <a href='" . base_url() . "konfirmasi?idp=" . $idp . "'>disini</a><br><br>";
		}
		$belanjaan = $this->model_app->view_join_where('rb_penjualan_detail', 'rb_produk', 'id_produk', array('id_penjualan' => $id), 'id_penjualan_detail', 'ASC');
		$message .= "<b>RINCIAN PESANAN</b><br>";
		$message .= "
				<table style='width:100%;border:0px' >
					<tr><td style='width:30%'>No Pesanan :</td><td> <b>#" . $mod['kode_transaksi'] . "</b></td></tr>
					<tr><td>Tanggal Pemesanan:</td><td>" . $mod['waktu_order'] . "</td></tr>
					<tr><td>Penjual : </td><td>" . $res['nama_reseller'] . "</td></tr><br>
				</table>";

		$no = 1;
		//$belanjaan = $this->db->query("SELECT * From rb_penjualan a JOIN rb_penjualan_detail b ON a.id_penjualan=b.id_penjualan JOIN rb_produk c ON b.id_produk=c.id_produk where a.kode_transaksi='".$mod['kode_transaksi']."' ")->row_array();
		foreach ($belanjaan as $row) {
			$ex = explode(';', $row['gambar']);
			if (trim($ex[0]) == '') {
				$foto_produk = 'no-image.png';
			} else {
				$foto_produk = $ex[0];
			}
			$sub_total = ($row['harga_jual'] * $row['jumlah']);
			$message .= "
							<img src='" . base_url() . 'asset/foto_produk/' . $foto_produk . "' style='width:150px'><br><br>
							<table style='width:100%;border:0px' >
							<tr><td>Produk</td><td>$row[nama_produk]</td></tr>
							<tr><td>Jumlah</td><td>$row[jumlah]</td></tr>
							<tr><td>Harga</td><td>" . rupiah($row['harga_jual']) . "</td></tr>
							<tr><td>Berat</td><td>" . ($row['berat'] * $row['jumlah']) . " Gram</td></tr>
							</table>";

			$no++;
		}

		$total = $this->db->query("SELECT sum(( b.harga_jual * b.jumlah )- b.diskon ) AS total, sum( c.berat * b.jumlah ) AS total_berat, ROUND(sum((((IFNULL(d.komisi,0)/100)*b.harga_jual)*(IFNULL(f.skema_diskon/100,0)))*b.jumlah), 0 ) AS totaldiskon, ROUND(sum((((IFNULL(d.komisi,0)/100)*b.harga_jual)*(IFNULL(f.komisi_topsonia/100,0)))*b.jumlah), 0 ) AS totalKomisiTopsonia FROM rb_penjualan a JOIN rb_penjualan_detail b ON a.id_penjualan = b.id_penjualan JOIN rb_produk c ON b.id_produk = c.id_produk JOIN rb_reseller d ON a.id_penjual = d.id_reseller JOIN rb_konsumen e ON a.id_pembeli = e.id_konsumen JOIN tipe_buyer f ON e.id_tipe_buyer = f.id_tipe_buyer where a.id_penjualan='" . $id . "'")->row_array();
		$sub_total = ($total['total'] + $mod['ongkir'] + $mod['asuransi'] + $mod['pajak'] + $mod['mdr']) - $mod['diskon'] - $mod['diskon_ongkir'];
		$message .=
			"<hr>
							<table style='width:100%;border:0px' >
							<tr><td>Sub Total<td><td>Rp " . rupiah($total['total']) . "</td></tr>
							<tr><td>Diskon Belanja<td><td>Rp " . rupiah($mod['diskon']) . "</td></tr>
							<tr><td>Ongkos Kirim<td><td>Rp " . rupiah($mod['ongkir']) . "</td></tr>
							<tr><td>Asuransi<td><td>Rp " . rupiah($mod['asuransi']) . "</td></tr>
							<tr><td>Diskon Ongkir<td><td>Rp " . rupiah($mod['diskon_ongkir']) . "</td></tr>
							<tr><td>Pajak<td><td>Rp " . rupiah($mod['pajak']) . "</td></tr>
							<tr><td>Biaya Admin<td><td>Rp " . rupiah($mod['mdr']) . "</td></tr>
							<tr><td>Total Pembayaran<td><td>Rp " . rupiah($sub_total) . "</td></tr>
							</table>
					</tbody>
			      </table><br>
			       Salam<br>Tim, Topsonia<br><br>
				   Butuh bantuan ? Hubungi kami <a href='" . base_url() . "hubungi'>disini</a> ";
		echo kirim_email($subjek, $message, $tujuan);

		if ($this->input->post('pembayaran') != 'manual-transfer') {
			//email reseller
			$tujuan = $res['email'];
			$tglaktif = date("d-m-Y H:i:s");

			$subjek      = "$iden[nama_website] - Anda mempunyai orderan";
			$message      = "<html><body>Halo <b>" . $res['nama_reseller'] . "</b><br>Anda Mendapatkan Pesanan baru di Topsonia, pada tanggal <b>$tglaktif</b>.
				<br><b>Berikut Detail Pemesan :</b><br>
				<table style='width:100%;border:0px'>
				<tr><td width='140px'>Nama Lengkap</td>  <td> : (" . $als['nama_alamat'] . ") " . $als['pic'] . "</td></tr>
				<tr><td>No Telpon</td>				<td> : " . $als['no_hp'] . "</td></tr>
				<tr><td>Alamat</b></td>					<td> : " . $als['alamat_lengkap'] . " </td></tr>
				<tr><td>Kabupaten/Kota</b></td>			<td> : " . $als['nama_kota'] . " </td></tr>
				<tr><td>Kecamatan</b></td>				<td> : " . $als['kecamatan'] . " </td></tr>
				<tr><td>Kode Pos</b></td>				<td> : " . $als['kode_pos'] . " </td></tr>		
				</table><br>

				<table style='width:100%;border:0px' >
					<tr><td style='width:30%'>No Pesanan :</td><td> <b>#" . $mod['kode_transaksi'] . "</b></td></tr>
					<tr><td>Tanggal Pemesanan:</td><td>" . $mod['waktu_order'] . "</td></tr>
				</table>";

			$belanjaan = $this->model_app->view_join_where('rb_penjualan_detail', 'rb_produk', 'id_produk', array('id_penjualan' => $id), 'id_penjualan_detail', 'ASC');
			$message .= "<b>RINCIAN PESANAN</b><br>";

			$no = 1;
			//$belanjaan = $this->db->query("SELECT * From rb_penjualan a JOIN rb_penjualan_detail b ON a.id_penjualan=b.id_penjualan JOIN rb_produk c ON b.id_produk=c.id_produk where a.kode_transaksi='".$mod['kode_transaksi']."' ")->row_array();
			foreach ($belanjaan as $row) {
				$sub_total = ($row['harga_jual'] * $row['jumlah']);
				$message .= "
							<img src='" . base_url() . 'asset/foto_produk/' . $belanjaan['gambar'][0] . "' style='width:150px'><br><br>
							<table style='width:100%;border:0px' >
							<tr><td>Produk</td><td>$row[nama_produk]</td></tr>
							<tr><td>Jumlah</td><td>$row[jumlah]</td></tr>
							<tr><td>Harga</td><td>" . rupiah($row['harga_jual']) . "</td></tr>
							<tr><td>Berat</td><td>" . ($row['berat'] * $row['jumlah']) . " Gram</td></tr>
							</table>";

				$no++;
			}

			$total = $this->db->query("SELECT sum(( b.harga_jual * b.jumlah )- b.diskon ) AS total, sum( c.berat * b.jumlah ) AS total_berat, ROUND(sum((((IFNULL(d.komisi,0)/100)*b.harga_jual)*(IFNULL(f.skema_diskon/100,0)))*b.jumlah), 0 ) AS totaldiskon, ROUND(sum((((IFNULL(d.komisi,0)/100)*b.harga_jual)*(IFNULL(f.komisi_topsonia/100,0)))*b.jumlah), 0 ) AS totalKomisiTopsonia FROM rb_penjualan a JOIN rb_penjualan_detail b ON a.id_penjualan = b.id_penjualan JOIN rb_produk c ON b.id_produk = c.id_produk JOIN rb_reseller d ON a.id_penjual = d.id_reseller JOIN rb_konsumen e ON a.id_pembeli = e.id_konsumen JOIN tipe_buyer f ON e.id_tipe_buyer = f.id_tipe_buyer where a.id_penjualan='" . $id . "'")->row_array();
			$sub_total = ($total['total'] + $mod['ongkir'] + $mod['asuransi'] + $mod['pajak'] + $mod['mdr']) - $mod['diskon'] - $mod['diskon_ongkir'];
			$message .=
				"<hr>
							<table style='width:100%;border:0px' >
							<tr><td>Sub Total<td><td>Rp " . rupiah($total['total']) . "</td></tr>
							<tr><td>Diskon Belanja<td><td>Rp " . rupiah($mod['diskon']) . "</td></tr>
							<tr><td>Ongkos Kirim<td><td>Rp " . rupiah($mod['ongkir']) . "</td></tr>
							<tr><td>Asuransi<td><td>Rp " . rupiah($mod['asuransi']) . "</td></tr>
							<tr><td>Diskon Ongkir<td><td>Rp " . rupiah($mod['diskon_ongkir']) . "</td></tr>
							<tr><td>Pajak<td><td>Rp " . rupiah($mod['pajak']) . "</td></tr>
							<tr><td>Biaya Admin<td><td>Rp " . rupiah($mod['mdr']) . "</td></tr>
							<tr><td>Total Pembayaran<td><td>Rp " . rupiah($sub_total) . "</td></tr>
							</table>
					</tbody>
			      </table>
				  <br>
				Silahkan untuk memproses transaksi pembelian kosumen <a href='" . base_url() . "reseller/index'>disini</a><br>
				
			    Salam<br>Tim, Topsonia<br><br><br><br>
				   Butuh bantuan ? Hubungi kami <a href='" . base_url() . "hubungi'>disini</a>";
			echo kirim_email($subjek, $message, $tujuan);
		}

		if ($this->input->post('pembayaran') === 'manual-transfer') {
			// redirect('konfirmasi?kode='.$cekres['kode_transaksi']);
			redirect('konfirmasi?idp=' . $idp);
		} else if ($this->input->post('pembayaran') === 'midtrans') {
			// redirect('konfirmasi?kode='.$cekres['kode_transaksi']);
			if (in_array($jenis, array("other_bank", "bca", "bri", "mandiri", "permata", "bni", "alfamart", "indomaret"))) {
				redirect('konfirmasi?idp=' . $idp);
			} else {
				redirect('members/orders_report');
			}
		} else if ($this->input->post('pembayaran') === 'cod') {
			redirect('members/orders_report');
		} else {
			redirect('konfirmasi/qris?idp=' . $idp);
		}
	}
	
	function tes_wa(){
			// $row = $this->db->query("SELECT b.* FROM rb_reseller a JOIN rb_konsumen b ON a.id_konsumen=b.id_konsumen where a.id_reseller='$cekres[id_penjual]'")->row_array();
            // $nama = $row['nama_lengkap'];
            $telepon = ('085828219935');
            // $email = $row['email'];
			$iden = $this->model_app->view_where('identitas',array('id_identitas'=>'1'))->row_array();
            $message = "*$iden[pengirim_email]* - Haloo ";
            $this->model_app->wa($telepon,$message);
			$this->session->set_flashdata('success', 'Berhasil');
			
			// $row = $this->db->query("SELECT a.*, b.nama_reseller, b.no_telpon, b.email FROM rb_penjualan a JOIN rb_reseller b ON a.id_penjual=b.id_reseller where a.id_penjualan='".$this->input->post('id')."' AND a.status_penjual='reseller'")->row_array();
			// $no_telpon = ('+6281351775677');
			// $isi_pesan = "Assalamualaikum tes";
			// $this->model_app->wa($no_telpon,$isi_pesan);
	}

	function batalkan_transaksi(){
		if ($this->session->id_konsumen) {
			$idseson = $this->session->id_konsumen;
		}else{
			$idseson = $this->session->sesi_no_login;
		}
		$this->session->set_flashdata('success', 'Transaksi Berhasil Di Batalkan!');
		$idp = array('session' => $idseson);
		$this->model_app->delete('rb_penjualan_temp',$idp);
		$this->model_app->delete('temp_penjualan',array('id_pembeli'=>$idseson));
		$this->session->unset_userdata('idp');
		$this->session->unset_userdata('id_konsumen');
		redirect('produk');
	}

	function terima_pesanan()
	{
		$idp = $this->input->get('idp');

		$data1 = array('selesai' => '1');
		
		$ambil = $this->db->query("select kode_transaksi,kategori_pembayaran as ktg,total_bayar from rb_penjualan where id_penjualan='$idp'")->row_array();
		
		if($ambil['ktg']=='cod'){

			$data1['bayar']= '1';
			
			$cek = $this->db->query("select jenis_pembayaran from rb_konfirmasi_pembayaran_konsumen where kode_transaksi='".$ambil['kod_transaksi']."'")->num_rows();
			if($cek>0){

			}else{
				$data_k = array('kode_transaksi'	=>$ambil['kode_transaksi'],
								'via'				=>'cod',
								'jenis_pembayaran'	=>'cod',
								'nominal'			=>$ambil['total_bayar'],
								'tanggal_bayar'		=>date("Y-m-d"),
								'status_pembayaran'	=>'lunas',
								'waktu_konfirmasi'	=>date("Y-m-d"),
								);
				$this->db->insert('rb_konfirmasi_pembayaran_konsumen',$data_k);		
			}
		}

		$where = array('id_penjualan' => $idp);
		$this->model_app->update('rb_penjualan', $data1, $where);

		redirect('members/orders_report');
	}

	function order(){
		cek_session_members();
		$this->session->set_userdata(array('produk'=>$this->uri->segment(3)));
		$cek = $this->db->query("SELECT b.nama_kota FROM rb_konsumen a JOIN rb_kota b ON a.kota_id=b.kota_id where a.id_konsumen='".$this->session->id_konsumen."'")->row_array();
		redirect('members/reseller?cari_reseller='.$cek['nama_kota']);
	}

	public function username_check(){
        // allow only Ajax request
		if($this->input->is_ajax_request()) {
	        // grab the email value from the post variable.
			$username = $this->input->post('a');

			if(!$this->form_validation->is_unique($username, 'rb_konsumen.username')) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('messageusername' => 'Maaf, Username ini sudah terdaftar,..')));
			}

		}
	}

	public function email_check(){
        // allow only Ajax request
		if($this->input->is_ajax_request()) {
	        // grab the email value from the post variable.
			$email = $this->input->post('d');

			if(!$this->form_validation->is_unique($email, 'rb_konsumen.email')) {
				$this->output->set_content_type('application/json')->set_output(json_encode(array('message' => 'Maaf, Email ini sudah terdaftar,..')));
			}
		}
	}

	function refresh_keranjang(){
		if ($this->session->id_konsumen=="") {
			$this->session->set_userdata(array('id_konsumen'=>session_id()));
		}
		$id = $this->input->post('id');
		$jumlah = $this->input->post('total',true);
		$where = array('id_penjualan_detail' => $id);
		$data = array('jumlah' => $jumlah);
		$this->model_app->update('rb_penjualan_temp', $data, $where);

		$checkout = $this->db->query("SELECT a.id_penjualan_detail,b.id_reseller,a.kode_transaksi FROM `rb_penjualan_temp` a join rb_produk b on a.id_produk=b.id_produk  where a.session='".$this->session->id_konsumen."'");
		$data['total_checkout'] = $checkout->num_rows();
		$data['id_penjualan_detail'] = $checkout->row_array()['id_penjualan'];
		
		// if ($this->session->idp != ''){
		if ($checkout->num_rows() > 0){

			$this->session->set_userdata(array('idp'=>$checkout->row_array()['id_penjualan']));
			// $data['rows'] = $this->model_reseller->penjualan_konsumen_detail($this->session->idp)->row_array();

			$data['rows'] = $this->db->query("SELECT * FROM `rb_penjualan_temp` a join rb_produk b on a.id_produk=b.id_produk join rb_reseller c ON b.id_reseller=c.id_reseller JOIN rb_kota d ON c.kota_id=d.kota_id where a.session='".$this->session->id_konsumen."' group by c.id_reseller ORDER BY c.id_reseller ASC")->result_array();

			$data['rowsk'] = $this->model_reseller->view_join_where_one('rb_konsumen','rb_kota','kota_id',array('id_konsumen'=>$this->session->id_konsumen))->row_array();
			
			// $data['record'] = $this->model_app->view_join_where('rb_penjualan_detail','rb_produk','id_produk',array('id_penjualan'=>$this->session->idp),'id_penjualan_detail','ASC');

			$data['record'] = $this->db->query("SELECT * FROM `rb_penjualan_temp` a join rb_produk b on a.id_produk=b.id_produk join rb_reseller c ON b.id_reseller=c.id_reseller JOIN rb_kota d ON c.kota_id=d.kota_id where a.session='".$this->session->id_konsumen."' ORDER BY a.id_penjualan_detail ASC")->result_array();
			
			// $data['filter_seller'] = $this->db->query("SELECT a.*,b.id_penjual, c.* FROM rb_penjualan_detail a JOIN rb_penjualan_temp b ON a.id_penjualan=b.id_penjualan JOIN rb_produk c ON a.id_produk=c.id_produk where b.id_pembeli='".$this->session->id_konsumen."' and b.order = '0' ORDER BY b.id_penjual ASC")->result_array();
		}

		// $data['record2'] = $this->db->query("SELECT a.*, c.* FROM rb_penjualan_detail a JOIN rb_penjualan_temp b ON a.id_penjualan=b.id_penjualan JOIN rb_produk c ON a.id_produk=c.id_produk where b.kode_transaksi='".$this->uri->segment(4)."' and b.order = '0' group by b.id_penjual ORDER BY b.id_penjualan asc")->result_array();
		$data['title'] = 'Keranjang Belanja';
		echo json_encode($this->load->view(template().'/reseller/members/view_keranjang_refresh',$data,true));

	}

	function logout(){
		cek_session_members();
		$this->session->sess_destroy();
		redirect('main');
	}


	function tambah_wishlist(){
		cek_session_members();
		// $id_reseller 	= $this->uri->segment(3);
		$id_produk   	= $this->uri->segment(3);
		$btn_type 		= $this->uri->segment(4);

		$data 			= $this->db->query("SELECT * FROM rb_produk where id_produk='$id_produk'");
		$row 			= $data->row();

		$data = array('id_produk'=> $id_produk, 'id_konsumen' => $this->session->id_konsumen);
		$this->model_app->insert('rb_wishlist',$data);

		if($btn_type == 'lanjut'){
			$this->session->set_flashdata('success', 'Berhasil ditambahkan ke favorite.');

			$dat = $this->db->query("SELECT * FROM rb_produk where id_produk='$id_produk' AND id_reseller!='0'");
			$row = $dat->row();
			$data['title'] = $row->nama_produk;
			$data['record'] = $this->model_app->view_where('rb_produk',array('id_produk'=>$id_produk))->row_array();
			$this->template->load(template().'/template',template().'/reseller/view_produk_detail',$data);

			$this->session->set_flashdata('success', '');
		}else{
			$this->session->set_flashdata('success', 'Berhasil ditambahkan ke keranjang.');

			redirect('members/tambah_wishlist');

			$this->session->set_flashdata('success', '');
		}
	}

	function delete_wishlist(){
		$id_produk   	= $this->uri->segment(3);
		$id = array('id_favorite' => $this->uri->segment(4));
		$this->model_app->delete('rb_wishlist',$id);
		$barang = $this->db->query("SELECT * FROM rb_wishlist where id_favorite='$id'")->row_array();

		// $barang = $this->db->query("SELECT *, rb_wishlist.id_favorite, rb_wishlist.id_konsumen, rb_wishlist.id_produk, rb_produk.*  FROM rb_wishlist INNER JOIN rb_produk ON rb_wishlist.id_produk = rb_produk.id_produk AND rb_wishlist.id_konsumen = rb_produk.id_konsumen where rb_wishlist.id_favorite!='0'");

		$id_favorite = array('id_favorite' => $this->session->id_favorite);
		$this->model_app->delete('rb_wishlist',$id);


		$this->session->set_flashdata('success', 'Berhasil dihapus dari favorite.');
		$dat = $this->db->query("SELECT * FROM rb_produk where id_produk='$id_produk' AND id_reseller!='0'");
		$row = $dat->row();
		$data['title'] = $row->nama_produk;
		$data['record'] = $this->model_app->view_where('rb_produk',array('id_produk'=>$id_produk))->row_array();
		$this->template->load(template().'/template',template().'/reseller/view_produk_detail',$data);
		$this->session->set_flashdata('success', '');
	}

	function wishlist(){
		cek_session_members();

		$jumlah= $this->model_app->view('rb_produk')->num_rows();
		$config['base_url'] = base_url().'produk/index';
		// $config['total_rows'] = $jumlah;
		$config['per_page'] = 48;
		if ($this->uri->segment('3')==''){
			$dari = 0;
		}else{
			$dari = $this->uri->segment('3');
		}

		if (is_numeric($dari)) {
			$id_konsumen = $this->session->id_konsumen;
			$data['title'] = title();
			$data['judul'] = 'Produk Favorite Anda';
			$data['description'] = description();
			$data['keywords'] = keywords();
			$this->pagination->initialize($config);
			$data['record'] = $this->db->query("SELECT *, rb_wishlist.id_favorite, rb_wishlist.id_konsumen, rb_wishlist.id_produk, rb_produk.*  FROM rb_wishlist INNER JOIN rb_produk ON rb_wishlist.id_produk = rb_produk.id_produk where rb_wishlist.id_konsumen='$id_konsumen' ORDER BY rb_produk.id_produk DESC LIMIT $dari,$config[per_page]");


			$this->template->load(template().'/template',template().'/favorite',$data);
		}else{
			redirect('main');
		}
	}

	function pesan(){
		// $chat	= $this->db->query("select * from chat where id_penjual ='$this->session->id_reseller' order by id_chat");
		$user	= $this->db->query("select a.*, b.nama_reseller from chat a join rb_reseller b on a.id_penjual=b.id_reseller where a.id_pembeli='".$this->session->id_konsumen."' and del!=1 group by id_penjual order by flag and id_chat asc");
		$data['user'] = $user;
		$this->template->load(template().'/template',template().'/reseller/members/pesan/menu_pesan.php',$data);
	}

	function chat(){
		$id_penjual	= $this->input->post('id_penjual'); //tujuan
		$id_max		= '10'; //dari
		
		$chat	= $this->db->query("select a.*, b.nama_reseller from chat a join rb_reseller b on a.id_penjual=b.id_reseller where a.id_pembeli ='".$this->session->id_konsumen."' and a.id_penjual='".$id_penjual."' and del!=1 order by id_chat");
		// $chat	= $this->db->query("select * from chat where id_penjual='24' AND id_pembeli ='27' order by id_chat");
		$this->model_app->update('chat',array("flag"=>1),array("id_penjual"=>$id_penjual,"id_pembeli"=>$this->session->id_konsumen,"ket"=>"2","flag"=>0));
		$data['id_max']		= $id_max;
		$data['id_penjual']	= $id_penjual;
		$data['chat'] 		= $chat;
		$this->load->view("ncs-custom1/reseller/members/pesan/chat_konsumen.php",$data);
	}

	function kirim_pesan(){
			$id_penjual	= $this->input->post("id_penjual"); //tujuan/user_2
			$pesan		= $this->input->post("pesan");
			
			$data	= array(
				'id_penjual' => $id_penjual,
				'id_pembeli' => $this->session->id_konsumen,
				'pesan' => $pesan,
				'flag' => 0,
				'ket' => 1,
				'del' => 0,
			);
			
			$query	=	$this->model_app->insert('chat',$data);
			
			if($query){
				$rs = 1;
			}else{
				$rs	= 2;
			}
			
			echo json_encode(array("result"=>$rs));
			
		}
		
		function hapus_pesan(){
			// echo "<script>alert('1')</script>";
			$id_penjual	= $this->input->post("id_penjual"); //tujuan/user_2

				$this->model_app->update('chat',array("del"=>1),array("id_penjual"=>$id_penjual,"id_pembeli"=>$this->session->id_konsumen,"del"=>'0'));
			
				$this->db->query("delete from chat where id_pembeli ='".$this->session->id_konsumen."' and id_penjual='".$id_penjual."' and del='2'");
			
			
			$data = array("bisa"=>"1");
			echo json_encode($data);
			// echo json_encode(array("result"=>$rs));
		}

		function get_kode_referal()
		{
		$id	= $this->session->id_konsumen;
		$kode = $this->input->post('username');
		$this->model_app->update('rb_konsumen', array("kode_referall" => $kode), array("id_konsumen" => $id));

		$data['kode_referall'] = $kode;
		echo json_encode($data);
		}
		
		function get_mdr(){
		$id = str_replace("_va", "", $this->input->post('jenis_mdr'));

		if ($id == 'echannel') {
			$id = 'mandiri';
		} else if ($id == 'other') {
			$id = 'other_bank';
		}

		if ($id != '' || $id != null) {
			$mdr = $this->model_app->view_where('tabel_mdr', array('jenis_pembayaran' => $id))->row_array();

			if ($mdr['persentase_mdr'] != null || $mdr['persentase_mdr'] != '') {
				$data['persen_mdr'] = $mdr['persentase_mdr'];
			} else {
				$data['persen_mdr'] = '0';
			}

			if ($mdr['nominal_mdr'] != null || $mdr['nominal_mdr'] != '') {
				$data['nominal_mdr'] = $mdr['nominal_mdr'];
			} else {
				$data['nominal_mdr'] = '0';
			}
		} else {
			$data['persen_mdr'] = '0';
			$data['nominal_mdr'] = '0';
		}

		echo json_encode($data);
	}

	function request_pencairan_komisi(){
		if ($this->input->post('id_konsumen') != '') {

			$data = array(	'id_konsumen' 		=> $this->input->post('id_konsumen'),
							'jumlah_pencairan' 	=> $this->input->post('jumlah_pencairan'),
							'tanggal_request'  	=> date("Y/m/d"),
							'status'			=> 'Request'
						);

            $this->db->insert("rb_pencairan_komisi",$data );
        }

		redirect("members/profile");
	}

}
