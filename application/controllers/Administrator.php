<?php
/*
-- ---------------------------------------------------------------
-- MARKETPLACE MULTI BUYER MULTI SELLER + SUPPORT RESELLER SYSTEM
-- CREATED BY : ROBBY PRIHANDAYA
-- COPYRIGHT  : Copyright (c) 2018 - 2019, PHPMU.COM. (https://phpmu.com/)
-- LICENSE    : http://opensource.org/licenses/MIT  MIT License
-- CREATED ON : 2019-03-26
-- UPDATED ON : 2019-03-27
-- ---------------------------------------------------------------
*/
defined('BASEPATH') OR exit('No direct script access allowed');
class Administrator extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->library('Pdf');

    }

    function index(){
      if (isset($_POST['submit'])){

        if ($this->session->level == 'konsumen') {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><center>Maaf, Anda sedang login sebagai Pembeli, Silahkan logout atau gunakan browser lain, untuk dapat melanjutkan aktifitas anda!<br>Logout <a href = "'.base_url().'administrator/logout">disini ??</a></center></div>');
            $data['title'] = 'Administrator &rsaquo; Log In';
            $this->load->view('administrator/view_login',$data);
        }elseif ($this->session->level == 'reseller') {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><center>Maaf, Anda sedang login sebagai Merchant, Silahkan logout atau gunakan browser lain, untuk dapat melanjutkan aktifitas belanja anda!<br>Logout <a href = "'.base_url().'administrator/logout">disini ??</a></center></div>');
            $data['title'] = 'Administrator &rsaquo; Log In';
            $this->load->view('administrator/view_login',$data);
        }else{
            
           // goto SkipCaptcha;
           if ($this->input->post() && (strtolower($this->input->post('security_code')) == strtolower($this->session->userdata('mycaptcha')))) {
            // SkipCaptcha:

            $username = $this->input->post('a');
            $password = hash("sha512", md5($this->input->post('b')));
            $cek = $this->model_app->cek_login($username,$password,'users');
            $row = $cek->row_array();
            $total = $cek->num_rows();
            if ($total > 0){
                $this->session->set_userdata('upload_image_file_manager',true);
                $this->session->set_userdata(array('username'=>$row['username'],
                   'level'=>$row['level'],
                   'id_session'=>$row['id_session']));
                redirect($this->uri->segment(1).'/home');
            }else{
                echo $this->session->set_flashdata('message', '<div class="alert alert-danger"><center>Username dan Password Salah!!</center></div>');
                redirect($this->uri->segment(1).'/index');
            }
        }else{
            echo $this->session->set_flashdata('message', '<div class="alert alert-danger"><center>Security Code salah!</center></div>');
            redirect($this->uri->segment(1).'/index');
        }
    }
}else{
    if ($this->session->level!=''){
				// redirect($this->uri->segment(1).'/home');
     $this->load->helper('captcha');
     $vals = array(
        'img_path'   => './captcha/',
        'img_url'    => base_url().'captcha/',
        'font_path' => base_url().'asset/Tahoma.ttf',
        'font_size'     => 17,
        'img_width'  => '320',
        'img_height' => 33,
        'border' => 0,
        'word_length'   => 5,
        'expiration' => 7200
    );

     $cap = create_captcha($vals);
     $data['image'] = $cap['image'];
     $this->session->set_userdata('mycaptcha', $cap['word']);
     $data['title'] = 'Administrator &rsaquo; Log In';
     $this->load->view('administrator/view_login',$data);
 }else{
    $this->load->helper('captcha');
    $vals = array(
        'img_path'   => './captcha/',
        'img_url'    => base_url().'captcha/',
        'font_path' => base_url().'asset/Tahoma.ttf',
        'font_size'     => 17,
        'img_width'  => '320',
        'img_height' => 33,
        'border' => 0,
        'word_length'   => 5,
        'expiration' => 7200
    );

    $cap = create_captcha($vals);
    $data['image'] = $cap['image'];
    $this->session->set_userdata('mycaptcha', $cap['word']);
    $data['title'] = 'Administrator &rsaquo; Log In';
    if ($this->session->level == 'konsumen') {
        $this->session->set_flashdata('message', '<div class="alert alert-danger"><center>Maaf, Anda sedang login sebagai Pembeli, Silahkan logout atau gunakan browser lain, untuk dapat melanjutkan aktifitas anda!<br>Logout <a href = "'.base_url().'administrator/logout">disini ??</a></center></div>');
    }elseif ($this->session->level == 'reseller') {
        $this->session->set_flashdata('message', '<div class="alert alert-danger"><center>Maaf, Anda sedang login sebagai Merchant, Silahkan logout atau gunakan browser lain, untuk dapat melanjutkan aktifitas belanja anda!<br>Logout <a href = "'.base_url().'administrator/logout">disini ??</a></center></div>');
    }
    $this->load->view('administrator/view_login',$data);
}
}
}

function reset_password(){
    if (isset($_POST['submit'])){
        $usr = $this->model_app->edit('users', array('id_session' => $this->input->post('id_session')));
        if ($usr->num_rows()>=1){
            if ($this->input->post('a')==$this->input->post('b')){
                $data = array('password'=>hash("sha512", md5($this->input->post('a'))));
                $where = array('id_session' => $this->input->post('id_session'));
                $this->model_app->update('users', $data, $where);

                $row = $usr->row_array();
                $this->session->set_userdata('upload_image_file_manager',true);
                $this->session->set_userdata(array('username'=>$row['username'],
                   'level'=>$row['level'],
                   'id_session'=>$row['id_session']));
                redirect($this->uri->segment(1).'/home');
            }else{
                $data['title'] = 'Password Tidak sama!';
                $this->load->view('administrator/view_reset',$data);
            }
        }else{
            $data['title'] = 'Terjadi Kesalahan!';
            $this->load->view('administrator/view_reset',$data);
        }
    }else{
        $this->session->set_userdata(array('id_session'=>$this->uri->segment(3)));
        $data['title'] = 'Reset Password';
        $this->load->view('administrator/view_reset',$data);
    }
}

function lupapassword(){
    if (isset($_POST['lupa'])){
        $email = strip_tags($this->input->post('email'));
        $cekemail = $this->model_app->edit('users', array('email' => $email))->num_rows();
        if ($cekemail <= 0){
            $data['title'] = 'Alamat email tidak ditemukan';
            $this->load->view('administrator/view_login',$data);
        }else{
            $iden = $this->model_app->edit('identitas', array('id_identitas' => 1))->row_array();
            $usr = $this->model_app->edit('users', array('email' => $email))->row_array();
            $this->load->library('email');

            $tgl = date("d-m-Y H:i:s");
            $subject      = 'Lupa Password ...';
            $message      = "<html><body>
            <table style='margin-left:25px'>
            <tr><td>Halo $usr[nama_lengkap],<br>
            Seseorang baru saja meminta untuk mengatur ulang kata sandi Anda di <span style='color:red'>$iden[url]</span>.<br>
            Klik di sini untuk mengganti kata sandi Anda.<br>
            Atau Anda dapat copas (Copy Paste) url dibawah ini ke address Bar Browser anda :<br>
            <a href='".base_url().$this->uri->segment(1)."/reset_password/$usr[id_session]'>".base_url().$this->uri->segment(1)."/reset_password/$usr[id_session]</a><br><br>

            Tidak meminta penggantian ini?<br>
            Jika Anda tidak meminta kata sandi baru, segera beri tahu kami.<br>
            Email. $iden[email], No Telp. $iden[no_telp]</td></tr>
            </table>
            </body></html> \n";

            $this->email->from($iden['email'], $iden['nama_website']);
            $this->email->to($usr['email']);
            $this->email->cc('');
            $this->email->bcc('');

            $this->email->subject($subject);
            $this->email->message($message);
            $this->email->set_mailtype("html");
            $this->email->send();

            $config['protocol'] = 'sendmail';
            $config['mailpath'] = '/usr/sbin/sendmail';
            $config['charset'] = 'utf-8';
            $config['wordwrap'] = TRUE;
            $config['mailtype'] = 'html';
            $this->email->initialize($config);

            $data['title'] = 'Password terkirim ke '.$usr['email'];
            $this->load->view('administrator/view_login',$data);
        }
    }else{
        redirect($this->uri->segment(1));
    }
}


//Upload image summernote
function upload_image(){
    if(isset($_FILES["image"]["name"])){
        $config['upload_path'] = 'asset/images/';
        $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG|swf';
$config['max_size'] = '5000'; // kb
$this->load->library('upload', $config);
if(!$this->upload->do_upload('image')){
    $this->upload->display_errors();
    return FALSE;
}else{
    $data = $this->upload->data();
//Compress Image
    $config['image_library']='gd2';
    $config['source_image']='asset/images/'.$data['file_name'];
    $config['create_thumb']= FALSE;
    $config['maintain_ratio']= TRUE;
    $config['quality']= '60%';
    $config['width']= 800;
    $config['height']= 800;
    $config['new_image']= 'asset/images/thumb_'.$data['file_name'];
    $this->load->library('image_lib', $config);
    $this->image_lib->resize();
    echo base_url().'asset/images/'.$data['file_name'];
}
}
}

//Delete image summernote
function delete_image(){
    $src = $this->input->post('src');
    $file_name = str_replace(base_url(), '', $src);
    if(unlink($file_name)){
        echo 'File Delete Successfully';
    }
}

function home(){
    if ($this->session->level=='admin'){
        $this->template->load('administrator/template','administrator/view_home_admin');
    }elseif ($this->session->level=='user'){
      $data['users'] = $this->model_app->view_where('users',array('username'=>$this->session->username))->row_array();
      $data['modul'] = $this->model_app->view_join_one('users','users_modul','id_session','id_umod','DESC');
      $this->template->load('administrator/template','administrator/view_home_users',$data);
					//masalah blank nya karena gak ada else
  }else{
     $this->template->load('administrator/template','administrator/view_home_admin');
					//karena belum difungsikan fitur multilevel usernya saya tambahkan else dulu
 }
}

function identitaswebsite(){
  cek_session_akses('identitaswebsite',$this->session->id_session);
  if (isset($_POST['submit'])){
   $config['upload_path'] = 'asset/images/';
   $config['allowed_types'] = 'gif|jpg|png|ico';
            $config['max_size'] = '500'; // kb
            $this->load->library('upload', $config);
            $this->upload->do_upload('j');
            $hasil=$this->upload->data();

            if ($hasil['file_name']==''){
            	$data = array('nama_website'=>$this->db->escape_str($this->input->post('a')),
                    'email'=>$this->db->escape_str($this->input->post('b')),
                    'url'=>$this->db->escape_str($this->input->post('c')),
                    'facebook'=>$this->input->post('d'),
                    'rekening'=>$this->db->escape_str($this->input->post('e')),
                    'no_telp'=>$this->db->escape_str($this->input->post('f')),
                    'meta_deskripsi'=>$this->input->post('g'),
                    'meta_keyword'=>$this->db->escape_str($this->input->post('h')),
                    'maps'=>$this->input->post('i'));
            }else{
            	$data = array('nama_website'=>$this->db->escape_str($this->input->post('a')),
                    'email'=>$this->db->escape_str($this->input->post('b')),
                    'url'=>$this->db->escape_str($this->input->post('c')),
                    'facebook'=>$this->input->post('d'),
                    'rekening'=>$this->db->escape_str($this->input->post('e')),
                    'no_telp'=>$this->db->escape_str($this->input->post('f')),
                    'meta_deskripsi'=>$this->input->post('g'),
                    'meta_keyword'=>$this->db->escape_str($this->input->post('h')),
                    'favicon'=>$hasil['file_name'],
                    'maps'=>$this->input->post('i'));
            }
            $where = array('id_identitas' => $this->input->post('id'));
            $this->model_app->update('identitas', $data, $where);

            redirect($this->uri->segment(1).'/identitaswebsite');
        }else{
           $proses = $this->model_app->edit('identitas', array('id_identitas' => 1))->row_array();
           $data = array('record' => $proses);
           $this->template->load('administrator/template','administrator/mod_identitas/view_identitas',$data);
       }
   }

	// Controller Modul Menu Website

   function menuwebsite(){
      cek_session_akses('menuwebsite',$this->session->id_session);
      $data['record'] = $this->db->query("SELECT * FROM menu order by position, urutan");
      $data['halaman'] = $this->model_app->view_ordering('halamanstatis','id_halaman','DESC');
      $data['kategori'] = $this->model_app->view_ordering('kategori','id_kategori','DESC');
      $this->template->load('administrator/template','administrator/mod_menu/view_menu',$data);
  }

  function save_menuwebsite(){
    cek_session_akses('menuwebsite',$this->session->id_session);
    $link = $_POST['link'].$_POST['page'].$_POST['kategori'];
    if($_POST['id'] != ''){
        $this->db->query("UPDATE menu SET nama_menu = '".$_POST['label']."', link  = '".$link."' where id_menu = '".$_POST['id']."' ");
        $arr['type']  = 'edit';
        $arr['label'] = $_POST['label'];
        $arr['link']  = $_POST['link'];
        $arr['page']  = $_POST['page'];
        $arr['kategori']  = $_POST['kategori'];
        $arr['id']    = $_POST['id'];
    }else{
        $row = $this->db->query("SELECT max(urutan)+1 as urutan FROM menu")->row_array();
        $this->db->query("INSERT into menu VALUES('','0','".$_POST['label']."', '".$link."','Ya','Bottom','".$row['urutan']."')");
        $id = $this->db->insert_id();
        $arr['menu'] = '<li class="dd-item dd3-item" data-id="'.$id.'" >
        <div class="dd-handle dd3-handle Bottom">Drag</div>
        <div class="dd3-content"><span id="label_show'.$id.'">'.$_POST['label'].'</span>
        <span class="span-right">/<span id="link_show'.$id.'">'.$link.'</span> &nbsp;&nbsp;
        <a href="'.base_url().'/'.$this->uri->segment(1).'/posisi_menuwebsite/'.$id.'" style="cursor:pointer"><i class="fa fa-chevron-circle-up text-success"></i></a> &nbsp;
        <a class="edit-button" id="'.$id.'" label="'.$_POST['label'].'" link="'.$_POST['link'].'" ><i class="fa fa-pencil"></i></a> &nbsp;
        <a class="del-button" id="'.$id.'"><i class="fa fa-trash"></i></a>
        </span>
        </div>';
        $arr['type'] = 'add';
    }
    print json_encode($arr);
}

function save(){
    $data = json_decode($_POST['data']);
    function parseJsonArray($jsonArray, $parentID = 0) {
      $return = array();
      foreach ($jsonArray as $subArray) {
        $returnSubSubArray = array();
        if (isset($subArray->children)) {
            $returnSubSubArray = parseJsonArray($subArray->children, $subArray->id);
        }

        $return[] = array('id' => $subArray->id, 'parentID' => $parentID);
        $return = array_merge($return, $returnSubSubArray);
    }
    return $return;
}
$readbleArray = parseJsonArray($data);

$i=0;
foreach($readbleArray as $row){
  $i++;
  $this->db->query("UPDATE menu SET id_parent = '".$row['parentID']."', urutan = '".$i."' where id_menu = '".$row['id']."' ");
}
}

function posisi_menuwebsite(){
    cek_session_akses('menuwebsite',$this->session->id_session);
    $cek = $this->model_app->view_where('menu',array('id_menu'=>$this->uri->segment(3)))->row_array();
    $posisi = ($cek['position'] == 'Top' ? 'Bottom' : 'Top');
    $data = array('position'=>$posisi);
    $where = array('id_menu' => $this->uri->segment(3));
    $this->model_app->update('menu', $data, $where);
    redirect($this->uri->segment(1).'/menuwebsite');
}

function delete_menuwebsite(){
    cek_session_akses('menuwebsite',$this->session->id_session);
    $idm = array('id_menu' => $this->input->post('id'));
    $this->model_app->delete('menu',$idm);
    $idm = array('id_parent' => $this->input->post('id'));
    $this->model_app->delete('menu',$idm);
}


	// Controller Modul Halaman Baru

function halamanbaru(){
  cek_session_akses('halamanbaru',$this->session->id_session);
  if ($this->session->level=='admin'){
    $data['record'] = $this->model_app->view_ordering('halamanstatis','id_halaman','DESC');
}else{
    $data['record'] = $this->model_app->view_where_ordering('halamanstatis',array('username'=>$this->session->username),'id_halaman','DESC');
}
$this->template->load('administrator/template','administrator/mod_halaman/view_halaman',$data);
}

function tambah_halamanbaru(){
  cek_session_akses('halamanbaru',$this->session->id_session);
  if (isset($_POST['submit'])){
   $config['upload_path'] = 'asset/foto_statis/';
   $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG';
            $config['max_size'] = '3000'; // kb
            $this->load->library('upload', $config);
            $this->upload->do_upload('c');
            $hasil=$this->upload->data();
            if ($hasil['file_name']==''){
                $data = array('judul'=>$this->db->escape_str($this->input->post('a')),
                    'judul_seo'=>seo_title($this->input->post('a')),
                    'isi_halaman'=>$this->input->post('b'),
                    'tgl_posting'=>date('Y-m-d'),
                    'username'=>$this->session->username,
                    'dibaca'=>'0',
                    'jam'=>date('H:i:s'),
                    'hari'=>hari_ini(date('w')));
            }else{
              $data = array('judul'=>$this->db->escape_str($this->input->post('a')),
                'judul_seo'=>seo_title($this->input->post('a')),
                'isi_halaman'=>$this->input->post('b'),
                'tgl_posting'=>date('Y-m-d'),
                'gambar'=>$hasil['file_name'],
                'username'=>$this->session->username,
                'dibaca'=>'0',
                'jam'=>date('H:i:s'),
                'hari'=>hari_ini(date('w')));
          }
          $this->model_app->insert('halamanstatis',$data);
          redirect($this->uri->segment(1).'/halamanbaru');
      }else{
       $this->template->load('administrator/template','administrator/mod_halaman/view_halaman_tambah');
   }
}

function edit_halamanbaru(){
  cek_session_akses('halamanbaru',$this->session->id_session);
  $id = $this->uri->segment(3);
  if (isset($_POST['submit'])){
   $config['upload_path'] = 'asset/foto_statis/';
   $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG';
            $config['max_size'] = '3000'; // kb
            $this->load->library('upload', $config);
            $this->upload->do_upload('c');
            $hasil=$this->upload->data();
            if ($hasil['file_name']==''){
                $data = array('judul'=>$this->db->escape_str($this->input->post('a')),
                    'judul_seo'=>seo_title($this->input->post('a')),
                    'isi_halaman'=>$this->input->post('b'));
            }else{
              $data = array('judul'=>$this->db->escape_str($this->input->post('a')),
                'judul_seo'=>seo_title($this->input->post('a')),
                'isi_halaman'=>$this->input->post('b'),
                'gambar'=>$hasil['file_name']);
          }
          $where = array('id_halaman' => $this->input->post('id'));
          $this->model_app->update('halamanstatis', $data, $where);
          redirect($this->uri->segment(1).'/halamanbaru');
      }else{
        if ($this->session->level=='admin'){
         $proses = $this->model_app->edit('halamanstatis', array('id_halaman' => $id))->row_array();
     }else{
        $proses = $this->model_app->edit('halamanstatis', array('id_halaman' => $id, 'username' => $this->session->username))->row_array();
    }
    $data = array('rows' => $proses);
    $this->template->load('administrator/template','administrator/mod_halaman/view_halaman_edit',$data);
}
}

function delete_halamanbaru(){
    cek_session_akses('halamanbaru',$this->session->id_session);
    if ($this->session->level=='admin'){
        $id = array('id_halaman' => $this->uri->segment(3));
    }else{
        $id = array('id_halaman' => $this->uri->segment(3), 'username'=>$this->session->username);
    }
    $this->model_app->delete('halamanstatis',$id);
    redirect($this->uri->segment(1).'/halamanbaru');
}

	// Controller Modul List Berita

function listberita(){
  cek_session_akses('listberita',$this->session->id_session);
  if ($this->session->level=='admin'){
    $data['record'] = $this->model_app->view_ordering('berita','id_berita','DESC');
}else{
    $data['record'] = $this->model_app->view_where_ordering('berita',array('username'=>$this->session->username),'id_berita','DESC');
}
$data['rss'] = $this->model_utama->view_joinn('berita','users','kategori','username','id_kategori','id_berita','DESC',0,10);
$data['iden'] = $this->model_utama->view_where('identitas',array('id_identitas' => 1))->row_array();
$this->load->view('administrator/rss',$data);
$this->template->load('administrator/template','administrator/mod_berita/view_berita',$data);
}

function tambah_listberita(){
  cek_session_akses('listberita',$this->session->id_session);
  if (isset($_POST['submit'])){
   $config['upload_path'] = 'asset/foto_berita/';
   $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG';
	        $config['max_size'] = '3000'; // kb
	        $this->load->library('upload', $config);
	        $this->upload->do_upload('k');
	        $hasil=$this->upload->data();

            $config['source_image'] = 'asset/foto_berita/'.$hasil['file_name'];
            $config['wm_text'] = 'phpmu.com';
            $config['wm_type'] = 'text';
            $config['wm_font_path'] = './system/fonts/texb.ttf';
            $config['wm_font_size'] = '26';
            $config['wm_font_color'] = 'ffffff';
            $config['wm_vrt_alignment'] = 'middle';
            $config['wm_hor_alignment'] = 'center';
            $config['wm_padding'] = '20';
            $this->load->library('image_lib',$config);
            $this->image_lib->watermark();

            if ($this->session->level == 'kontributor'){ $status = 'N'; }else{ $status = 'Y'; }
            if ($this->input->post('j')!=''){
                $tag_seo = $this->input->post('j');
                $tag=implode(',',$tag_seo);
            }else{
                $tag = '';
            }
            if ($hasil['file_name']==''){
                $data = array('id_kategori'=>$this->db->escape_str($this->input->post('a')),
                    'username'=>$this->session->username,
                    'judul'=>$this->db->escape_str($this->input->post('b')),
                    'sub_judul'=>$this->db->escape_str($this->input->post('c')),
                    'youtube'=>$this->db->escape_str($this->input->post('d')),
                    'judul_seo'=>seo_title($this->input->post('b')),
                    'headline'=>$this->db->escape_str($this->input->post('e')),
                    'aktif'=>$this->db->escape_str($this->input->post('f')),
                    'utama'=>$this->db->escape_str($this->input->post('g')),
                    'isi_berita'=>$this->input->post('h'),
                    'keterangan_gambar'=>$this->input->post('i'),
                    'hari'=>hari_ini(date('w')),
                    'tanggal'=>date('Y-m-d'),
                    'jam'=>date('H:i:s'),
                    'dibaca'=>'0',
                    'tag'=>$tag,
                    'status'=>$status);
            }else{
                $data = array('id_kategori'=>$this->db->escape_str($this->input->post('a')),
                    'username'=>$this->session->username,
                    'judul'=>$this->db->escape_str($this->input->post('b')),
                    'sub_judul'=>$this->db->escape_str($this->input->post('c')),
                    'youtube'=>$this->db->escape_str($this->input->post('d')),
                    'judul_seo'=>seo_title($this->input->post('b')),
                    'headline'=>$this->db->escape_str($this->input->post('e')),
                    'aktif'=>$this->db->escape_str($this->input->post('f')),
                    'utama'=>$this->db->escape_str($this->input->post('g')),
                    'isi_berita'=>$this->input->post('h'),
                    'keterangan_gambar'=>$this->input->post('i'),
                    'hari'=>hari_ini(date('w')),
                    'tanggal'=>date('Y-m-d'),
                    'jam'=>date('H:i:s'),
                    'gambar'=>$hasil['file_name'],
                    'dibaca'=>'0',
                    'tag'=>$tag,
                    'status'=>$status);
            }
            $this->model_app->insert('berita',$data);
            redirect($this->uri->segment(1).'/listberita');
        }else{
            $data['tag'] = $this->model_app->view_ordering('tag','id_tag','DESC');
            $data['record'] = $this->model_app->view_ordering('kategori','id_kategori','DESC');
            $this->template->load('administrator/template','administrator/mod_berita/view_berita_tambah',$data);
        }
    }

    function edit_listberita(){
      cek_session_akses('listberita',$this->session->id_session);
      $id = $this->uri->segment(3);
      if (isset($_POST['submit'])){
       $config['upload_path'] = 'asset/foto_berita/';
       $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG';
	        $config['max_size'] = '3000'; // kb
	        $this->load->library('upload', $config);
	        $this->upload->do_upload('k');
	        $hasil=$this->upload->data();

            $config['source_image'] = 'asset/foto_berita/'.$hasil['file_name'];
            $config['wm_text'] = '';
            $config['wm_type'] = 'text';
            $config['wm_font_path'] = './system/fonts/texb.ttf';
            $config['wm_font_size'] = '26';
            $config['wm_font_color'] = 'ffffff';
            $config['wm_vrt_alignment'] = 'middle';
            $config['wm_hor_alignment'] = 'center';
            $config['wm_padding'] = '20';
            $this->load->library('image_lib',$config);
            $this->image_lib->watermark();

            if ($this->session->level == 'kontributor'){ $status = 'N'; }else{ $status = 'Y'; }
            if ($this->input->post('j')!=''){
                $tag_seo = $this->input->post('j');
                $tag=implode(',',$tag_seo);
            }else{
                $tag = '';
            }
            if ($hasil['file_name']==''){
                $data = array('id_kategori'=>$this->db->escape_str($this->input->post('a')),
                    'username'=>$this->session->username,
                    'judul'=>$this->db->escape_str($this->input->post('b')),
                    'sub_judul'=>$this->db->escape_str($this->input->post('c')),
                    'youtube'=>$this->db->escape_str($this->input->post('d')),
                    'judul_seo'=>seo_title($this->input->post('b')),
                    'headline'=>$this->db->escape_str($this->input->post('e')),
                    'aktif'=>$this->db->escape_str($this->input->post('f')),
                    'utama'=>$this->db->escape_str($this->input->post('g')),
                    'isi_berita'=>$this->input->post('h'),
                    'keterangan_gambar'=>$this->input->post('i'),
                    'hari'=>hari_ini(date('w')),
                    'tanggal'=>date('Y-m-d'),
                    'jam'=>date('H:i:s'),
                    'dibaca'=>'0',
                    'tag'=>$tag,
                    'status'=>$status);
            }else{
                $data = array('id_kategori'=>$this->db->escape_str($this->input->post('a')),
                    'username'=>$this->session->username,
                    'judul'=>$this->db->escape_str($this->input->post('b')),
                    'sub_judul'=>$this->db->escape_str($this->input->post('c')),
                    'youtube'=>$this->db->escape_str($this->input->post('d')),
                    'judul_seo'=>seo_title($this->input->post('b')),
                    'headline'=>$this->db->escape_str($this->input->post('e')),
                    'aktif'=>$this->db->escape_str($this->input->post('f')),
                    'utama'=>$this->db->escape_str($this->input->post('g')),
                    'isi_berita'=>$this->input->post('h'),
                    'keterangan_gambar'=>$this->input->post('i'),
                    'hari'=>hari_ini(date('w')),
                    'tanggal'=>date('Y-m-d'),
                    'jam'=>date('H:i:s'),
                    'gambar'=>$hasil['file_name'],
                    'dibaca'=>'0',
                    'tag'=>$tag,
                    'status'=>$status);
            }
            $where = array('id_berita' => $this->input->post('id'));
            $this->model_app->update('berita', $data, $where);
            redirect($this->uri->segment(1).'/listberita');
        }else{
           $tag = $this->model_app->view_ordering('tag','id_tag','DESC');
           $record = $this->model_app->view_ordering('kategori','id_kategori','DESC');
           if ($this->session->level=='admin'){
             $proses = $this->model_app->edit('berita', array('id_berita' => $id))->row_array();
         }else{
            $proses = $this->model_app->edit('berita', array('id_berita' => $id, 'username' => $this->session->username))->row_array();
        }
        $data = array('rows' => $proses,'tag' => $tag,'record' => $record);
        $this->template->load('administrator/template','administrator/mod_berita/view_berita_edit',$data);
    }
}

function publish_listberita(){
  cek_session_admin();
  if ($this->uri->segment(4)=='Y'){
     $data = array('status'=>'N');
 }else{
     $data = array('status'=>'Y');
 }
 $where = array('id_berita' => $this->uri->segment(3));
 $this->model_app->update('berita', $data, $where);
 redirect($this->uri->segment(1).'/listberita');
}

function delete_listberita(){
  cek_session_akses('listberita',$this->session->id_session);
  if ($this->session->level=='admin'){
    $id = array('id_berita' => $this->uri->segment(3));
}else{
  $id = array('id_berita' => $this->uri->segment(3), 'username'=>$this->session->username);
}
$this->model_app->delete('berita',$id);
redirect($this->uri->segment(1).'/listberita');
}


		// Controller Modul Kategori Berita

function kategoriberita(){
   cek_session_akses('kategoriberita',$this->session->id_session);
   if ($this->session->level=='admin'){
      $data['record'] = $this->model_app->view_ordering('kategori','id_kategori','DESC');
  }else{
      $data['record'] = $this->model_app->view_where_ordering('kategori',array('username'=>$this->session->username),'id_kategori','DESC');
  }
  $this->template->load('administrator/template','administrator/mod_kategori/view_kategori',$data);
}

function tambah_kategoriberita(){
   cek_session_akses('kategoriberita',$this->session->id_session);
   if (isset($_POST['submit'])){
    $data = array('nama_kategori'=>$this->db->escape_str($this->input->post('a')),
      'username'=>$this->session->username,
      'kategori_seo'=>seo_title($this->input->post('a')),
      'aktif'=>$this->db->escape_str($this->input->post('b')),
      'sidebar'=>$this->db->escape_str($this->input->post('c')));
    $this->model_app->insert('kategori',$data);
    redirect($this->uri->segment(1).'/kategoriberita');
}else{
    $this->template->load('administrator/template','administrator/mod_kategori/view_kategori_tambah');
}
}

function edit_kategoriberita(){
   cek_session_akses('kategoriberita',$this->session->id_session);
   $id = $this->uri->segment(3);
   if (isset($_POST['submit'])){
    $data = array('nama_kategori'=>$this->db->escape_str($this->input->post('a')),
      'username'=>$this->session->username,
      'kategori_seo'=>seo_title($this->input->post('a')),
      'aktif'=>$this->db->escape_str($this->input->post('b')),
      'sidebar'=>$this->db->escape_str($this->input->post('c')));
    $where = array('id_kategori' => $this->input->post('id'));
    $this->model_app->update('kategori', $data, $where);
    redirect($this->uri->segment(1).'/kategoriberita');
}else{
  if ($this->session->level=='admin'){
   $proses = $this->model_app->edit('kategori', array('id_kategori' => $id))->row_array();
}else{
  $proses = $this->model_app->edit('kategori', array('id_kategori' => $id, 'username' => $this->session->username))->row_array();
}
$data = array('rows' => $proses);
$this->template->load('administrator/template','administrator/mod_kategori/view_kategori_edit',$data);
}
}

function delete_kategoriberita(){
   cek_session_akses('kategoriberita',$this->session->id_session);
   if ($this->session->level=='admin'){
      $id = array('id_kategori' => $this->uri->segment(3));
  }else{
      $id = array('id_kategori' => $this->uri->segment(3), 'username'=>$this->session->username);
  }
  $this->model_app->delete('kategori',$id);
  redirect($this->uri->segment(1).'/kategoriberita');
}


		// Controller Modul Tag Berita

function tagberita(){
   cek_session_akses('tagberita',$this->session->id_session);
   if ($this->session->level=='admin'){
      $data['record'] = $this->model_app->view_ordering('tag','id_tag','DESC');
  }else{
      $data['record'] = $this->model_app->view_where_ordering('tag',array('username'=>$this->session->username),'id_tag','DESC');
  }
  $this->template->load('administrator/template','administrator/mod_tag/view_tag',$data);
}

function tambah_tagberita(){
   cek_session_akses('tagberita',$this->session->id_session);
   if (isset($_POST['submit'])){
    $data = array('nama_tag'=>$this->db->escape_str($this->input->post('a')),
      'username'=>$this->session->username,
      'tag_seo'=>seo_title($this->input->post('a')),
      'count'=>'0');
    $this->model_app->insert('tag',$data);
    redirect($this->uri->segment(1).'/tagberita');
}else{
    $this->template->load('administrator/template','administrator/mod_tag/view_tag_tambah');
}
}

function edit_tagberita(){
    cek_session_akses('tagberita',$this->session->id_session);
    $id = $this->uri->segment(3);
    if (isset($_POST['submit'])){
     $data = array('nama_tag'=>$this->db->escape_str($this->input->post('a')),
       'username'=>$this->session->username,
       'tag_seo'=>seo_title($this->input->post('a')));
     $where = array('id_tag' => $this->input->post('id'));
     $this->model_app->update('tag', $data, $where);
     redirect($this->uri->segment(1).'/tagberita');
 }else{
   if ($this->session->level=='admin'){
    $proses = $this->model_app->edit('tag', array('id_tag' => $id))->row_array();
}else{
   $proses = $this->model_app->edit('tag', array('id_tag' => $id, 'username' => $this->session->username))->row_array();
}
$data = array('rows' => $proses);
$this->template->load('administrator/template','administrator/mod_tag/view_tag_edit',$data);
}
}

function delete_tagberita(){
  cek_session_akses('tagberita',$this->session->id_session);
  if ($this->session->level=='admin'){
   $id = array('id_tag' => $this->uri->segment(3));
}else{
   $id = array('id_tag' => $this->uri->segment(3), 'username'=>$this->session->username);
}
$this->model_app->delete('tag',$id);
redirect($this->uri->segment(1).'/tagberita');
}


		// Controller Modul Komentar Berita

function komentarberita(){
   cek_session_akses('komentarberita',$this->session->id_session);
   $data['record'] = $this->model_app->view_ordering('komentar','id_komentar','DESC');
   $this->template->load('administrator/template','administrator/mod_komentar/view_komentar',$data);
}

function edit_komentarberita(){
   cek_session_akses('komentarberita',$this->session->id_session);
   $id = $this->uri->segment(3);
   if (isset($_POST['submit'])){
    $data = array('nama_komentar'=>$this->input->post('a'),
      'url'=>$this->input->post('b'),
      'isi_komentar'=>$this->input->post('c'),
      'aktif'=>$this->input->post('d'),
      'email'=>$this->input->post('e'));
    $where = array('id_komentar' => $this->input->post('id'));
    $this->model_app->update('komentar', $data, $where);
    redirect($this->uri->segment(1).'/komentarberita');
}else{
    $proses = $this->model_app->edit('komentar', array('id_komentar' => $id))->row_array();
    $data = array('rows' => $proses);
    $this->template->load('administrator/template','administrator/mod_komentar/view_komentar_edit',$data);
}
}

function delete_komentarberita(){
  cek_session_akses('komentarberita',$this->session->id_session);
  $id = array('id_komentar' => $this->uri->segment(3));
  $this->model_app->delete('komentar',$id);
  redirect($this->uri->segment(1).'/komentarberita');
}


		// Controller Modul Sensor Komentar Berita

function sensorkomentar(){
   cek_session_akses('sensorkomentar',$this->session->id_session);
   if ($this->session->level=='admin'){
      $data['record'] = $this->model_app->view_ordering('katajelek','id_jelek','DESC');
  }else{
      $data['record'] = $this->model_app->view_where_ordering('katajelek',array('username'=>$this->session->username),'id_jelek','DESC');
  }
  $this->template->load('administrator/template','administrator/mod_sensorkomentar/view_sensorkomentar',$data);
}

function tambah_sensorkomentar(){
   cek_session_akses('sensorkomentar',$this->session->id_session);
   if (isset($_POST['submit'])){
    $data = array('kata'=>$this->input->post('a'),
      'username'=>$this->session->username,
      'ganti'=>$this->input->post('b'));
    $this->model_app->insert('katajelek',$data);
    redirect($this->uri->segment(1).'/sensorkomentar');
}else{
    $this->template->load('administrator/template','administrator/mod_sensorkomentar/view_sensorkomentar_tambah');
}
}

function edit_sensorkomentar(){
   cek_session_akses('sensorkomentar',$this->session->id_session);
   $id = $this->uri->segment(3);
   if (isset($_POST['submit'])){
    $this->model_berita->tag_berita_update();
    $data = array('kata'=>$this->input->post('a'),
      'username'=>$this->session->username,
      'ganti'=>$this->input->post('b'));
    $where = array('id_jelek' => $this->input->post('id'));
    $this->model_app->update('katajelek', $data, $where);
    redirect($this->uri->segment(1).'/sensorkomentar');
}else{
  if ($this->session->level=='admin'){
   $proses = $this->model_app->edit('katajelek', array('id_jelek' => $id))->row_array();
}else{
  $proses = $this->model_app->edit('katajelek', array('id_jelek' => $id, 'username' => $this->session->username))->row_array();
}
$data = array('rows' => $proses);
$this->template->load('administrator/template','administrator/mod_sensorkomentar/view_sensorkomentar_edit',$data);
}
}

function delete_sensorkomentar(){
  cek_session_akses('sensorkomentar',$this->session->id_session);
  if ($this->session->level=='admin'){
   $id = array('id_jelek' => $this->uri->segment(3));
}else{
   $id = array('id_jelek' => $this->uri->segment(3), 'username'=>$this->session->username);
}
$this->model_app->delete('katajelek',$id);
redirect($this->uri->segment(1).'/sensorkomentar');
}


		// Controller Modul Album

function album(){
  cek_session_akses('album',$this->session->id_session);
  if ($this->session->level=='admin'){
   $data['record'] = $this->model_app->view_ordering('album','id_album','DESC');
}else{
   $data['record'] = $this->model_app->view_where_ordering('album',array('username'=>$this->session->username),'id_album','DESC');
}
$this->template->load('administrator/template','administrator/mod_album/view_album',$data);
}

function tambah_album(){
  cek_session_akses('album',$this->session->id_session);
  if (isset($_POST['submit'])){
   $config['upload_path'] = 'asset/img_album/';
   $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG';
				$config['max_size'] = '3000'; // kb
				$this->load->library('upload', $config);
				$this->upload->do_upload('c');
				$hasil=$this->upload->data();
				if ($hasil['file_name']==''){
					$data = array('jdl_album'=>$this->input->post('a'),
						'album_seo'=>seo_title($this->input->post('a')),
						'keterangan'=>$this->input->post('b'),
						'aktif'=>'Y',
						'hits_album'=>'0',
						'tgl_posting'=>date('Y-m-d'),
						'jam'=>date('H:i:s'),
						'hari'=>hari_ini(date('w')),
						'username'=>$this->session->username);
				}else{
					$data = array('jdl_album'=>$this->input->post('a'),
						'album_seo'=>seo_title($this->input->post('a')),
						'keterangan'=>$this->input->post('b'),
						'gbr_album'=>$hasil['file_name'],
						'aktif'=>'Y',
						'hits_album'=>'0',
						'tgl_posting'=>date('Y-m-d'),
						'jam'=>date('H:i:s'),
						'hari'=>hari_ini(date('w')),
						'username'=>$this->session->username);
				}

				$this->model_app->insert('album',$data);
				redirect($this->uri->segment(1).'/album');
			}else{
				$this->template->load('administrator/template','administrator/mod_album/view_album_tambah');
			}
		}

        function edit_album(){
            cek_session_akses('album',$this->session->id_session);
            $id = $this->uri->segment(3);
            if (isset($_POST['submit'])){
                $config['upload_path'] = 'asset/img_album/';
                $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG';
            $config['max_size'] = '3000'; // kb
            $this->load->library('upload', $config);
            $this->upload->do_upload('c');
            $hasil=$this->upload->data();
            if ($hasil['file_name']==''){
                $data = array('jdl_album'=>$this->input->post('a'),
                    'album_seo'=>seo_title($this->input->post('a')),
                    'keterangan'=>$this->input->post('b'),
                    'aktif'=>$this->input->post('d'));
            }else{
                $data = array('jdl_album'=>$this->input->post('a'),
                    'album_seo'=>seo_title($this->input->post('a')),
                    'keterangan'=>$this->input->post('b'),
                    'gbr_album'=>$hasil['file_name'],
                    'aktif'=>$this->input->post('d'));
            }
            $where = array('id_album' => $this->input->post('id'));
            $this->model_app->update('album', $data, $where);
            redirect($this->uri->segment(1).'/album');
        }else{
            if ($this->session->level=='admin'){
                $proses = $this->model_app->edit('album', array('id_album' => $id))->row_array();
            }else{
                $proses = $this->model_app->edit('album', array('id_album' => $id, 'username' => $this->session->username))->row_array();
            }
            $data = array('rows' => $proses);
            $this->template->load('administrator/template','administrator/mod_album/view_album_edit',$data);
        }
    }

    function delete_album(){
        cek_session_akses('album',$this->session->id_session);
        if ($this->session->level=='admin'){
            $id = array('id_album' => $this->uri->segment(3));
        }else{
            $id = array('id_album' => $this->uri->segment(3), 'username'=>$this->session->username);
        }
        $this->model_app->delete('album',$id);
        redirect($this->uri->segment(1).'/album');
    }


    // Controller Modul Gallery

    function gallery(){
        cek_session_akses('gallery',$this->session->id_session);
        if ($this->session->level=='admin'){
            $data['record'] = $this->model_app->view_join_one('gallery','album','id_album','id_gallery','DESC');
        }else{
            $data['record'] = $this->model_app->view_join_where('gallery','album','id_album',array('gallery.username'=>$this->session->username),'id_gallery','DESC');
        }
        $this->template->load('administrator/template','administrator/mod_gallery/view_gallery',$data);
    }

    function tambah_gallery(){
        cek_session_akses('gallery',$this->session->id_session);
        if (isset($_POST['submit'])){
            $config['upload_path'] = 'asset/img_galeri/';
            $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG';
            $config['max_size'] = '3000'; // kb
            $this->load->library('upload', $config);
            $this->upload->do_upload('d');
            $hasil=$this->upload->data();
            if ($hasil['file_name']==''){
                $data = array('id_album'=>$this->input->post('a'),
                    'username'=>$this->session->username,
                    'jdl_gallery'=>$this->input->post('b'),
                    'gallery_seo'=>seo_title($this->input->post('b')),
                    'keterangan'=>$this->input->post('c'));
            }else{
                $data = array('id_album'=>$this->input->post('a'),
                    'username'=>$this->session->username,
                    'jdl_gallery'=>$this->input->post('b'),
                    'gallery_seo'=>seo_title($this->input->post('b')),
                    'keterangan'=>$this->input->post('c'),
                    'gbr_gallery'=>$hasil['file_name']);
            }
            $this->model_app->insert('gallery',$data);
            redirect($this->uri->segment(1).'/gallery');
        }else{
            $data['record'] = $this->model_app->view_ordering('album','id_album','DESC');
            $this->template->load('administrator/template','administrator/mod_gallery/view_gallery_tambah',$data);
        }
    }

    function edit_gallery(){
        cek_session_akses('gallery',$this->session->id_session);
        $id = $this->uri->segment(3);
        if (isset($_POST['submit'])){
            $config['upload_path'] = 'asset/img_galeri/';
            $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG';
            $config['max_size'] = '3000'; // kb
            $this->load->library('upload', $config);
            $this->upload->do_upload('d');
            $hasil=$this->upload->data();
            if ($hasil['file_name']==''){
                $data = array('id_album'=>$this->input->post('a'),
                    'username'=>$this->session->username,
                    'jdl_gallery'=>$this->input->post('b'),
                    'gallery_seo'=>seo_title($this->input->post('b')),
                    'keterangan'=>$this->input->post('c'));
            }else{
                $data = array('id_album'=>$this->input->post('a'),
                    'username'=>$this->session->username,
                    'jdl_gallery'=>$this->input->post('b'),
                    'gallery_seo'=>seo_title($this->input->post('b')),
                    'keterangan'=>$this->input->post('c'),
                    'gbr_gallery'=>$hasil['file_name']);
            }
            $where = array('id_gallery' => $this->input->post('id'));
            $this->model_app->update('gallery', $data, $where);
            redirect($this->uri->segment(1).'/gallery');
        }else{
            $record = $this->model_app->view_ordering('album','id_album','DESC');
            if ($this->session->level=='admin'){
                $proses = $this->model_app->edit('gallery', array('id_gallery' => $id))->row_array();
            }else{
                $proses = $this->model_app->edit('gallery', array('id_gallery' => $id, 'username' => $this->session->username))->row_array();
            }
            $data = array('rows' => $proses,'record' => $record);
            $this->template->load('administrator/template','administrator/mod_gallery/view_gallery_edit',$data);
        }
    }

    function delete_gallery(){
        cek_session_akses('gallery',$this->session->id_session);
        if ($this->session->level=='admin'){
            $id = array('id_gallery' => $this->uri->segment(3));
        }else{
            $id = array('id_gallery' => $this->uri->segment(3), 'username'=>$this->session->username);
        }
        $this->model_app->delete('gallery',$id);
        redirect($this->uri->segment(1).'/gallery');
    }


    // Controller Modul Video

    function video(){
        cek_session_akses('video',$this->session->id_session);
        if ($this->session->level=='admin'){
            $data['record'] = $this->model_app->view_join_one('video','playlist','id_playlist','id_video','DESC');
        }else{
            $data['record'] = $this->model_app->view_join_where('video','playlist','id_playlist',array('video.username'=>$this->session->username),'id_video','DESC');
        }
        $this->template->load('administrator/template','administrator/mod_video/view_video',$data);
    }

    function tambah_video(){
        cek_session_akses('video',$this->session->id_session);
        if (isset($_POST['submit'])){
            $config['upload_path'] = 'asset/img_video/';
            $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG';
            $config['max_size'] = '3000'; // kb
            $this->load->library('upload', $config);
            $this->upload->do_upload('d');
            $hasil=$this->upload->data();

            if ($this->input->post('f')!=''){
                $tag_seo = $this->input->post('f');
                $tag=implode(',',$tag_seo);
            }else{
                $tag = '';
            }

            if ($hasil['file_name']==''){
                $data = array('id_playlist'=>$this->input->post('a'),
                    'username'=>$this->session->username,
                    'jdl_video'=>$this->input->post('b'),
                    'video_seo'=>seo_title($this->input->post('b')),
                    'keterangan'=>$this->input->post('c'),
                    'video'=>'',
                    'youtube'=>$this->input->post('e'),
                    'dilihat'=>'0',
                    'hari'=>hari_ini(date('w')),
                    'tanggal'=>date('Y-m-d'),
                    'jam'=>date('H:i:s'),
                    'tagvid'=>$tag);
            }else{
                $data = array('id_playlist'=>$this->input->post('a'),
                    'username'=>$this->session->username,
                    'jdl_video'=>$this->input->post('b'),
                    'video_seo'=>seo_title($this->input->post('b')),
                    'keterangan'=>$this->input->post('c'),
                    'gbr_video'=>$hasil['file_name'],
                    'video'=>'',
                    'youtube'=>$this->input->post('e'),
                    'dilihat'=>'0',
                    'hari'=>hari_ini(date('w')),
                    'tanggal'=>date('Y-m-d'),
                    'jam'=>date('H:i:s'),
                    'tagvid'=>$tag);
            }
            $this->model_app->insert('video',$data);
            redirect($this->uri->segment(1).'/video');
        }else{
            $data['record'] = $this->model_app->view_ordering('playlist','id_playlist','DESC');
            $data['tag'] = $this->model_app->view_ordering('tagvid','id_tag','DESC');
            $this->template->load('administrator/template','administrator/mod_video/view_video_tambah',$data);
        }
    }

    function edit_video(){
        cek_session_akses('video',$this->session->id_session);
        $id = $this->uri->segment(3);
        if (isset($_POST['submit'])){
            $config['upload_path'] = 'asset/img_video/';
            $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG';
            $config['max_size'] = '3000'; // kb
            $this->load->library('upload', $config);
            $this->upload->do_upload('d');
            $hasil=$this->upload->data();

            if ($this->input->post('f')!=''){
                $tag_seo = $this->input->post('f');
                $tag=implode(',',$tag_seo);
            }else{
                $tag = '';
            }

            if ($hasil['file_name']==''){
                $data = array('id_playlist'=>$this->input->post('a'),
                    'username'=>$this->session->username,
                    'jdl_video'=>$this->input->post('b'),
                    'video_seo'=>seo_title($this->input->post('b')),
                    'keterangan'=>$this->input->post('c'),
                    'video'=>'',
                    'youtube'=>$this->input->post('e'),
                    'tagvid'=>$tag);
            }else{
                $data = array('id_playlist'=>$this->input->post('a'),
                    'username'=>$this->session->username,
                    'jdl_video'=>$this->input->post('b'),
                    'video_seo'=>seo_title($this->input->post('b')),
                    'keterangan'=>$this->input->post('c'),
                    'gbr_video'=>$hasil['file_name'],
                    'video'=>'',
                    'youtube'=>$this->input->post('e'),
                    'tagvid'=>$tag);
            }

            $where = array('id_video' => $this->input->post('id'));
            $this->model_app->update('video', $data, $where);
            redirect($this->uri->segment(1).'/video');
        }else{
            $record = $this->model_app->view_ordering('playlist','id_playlist','DESC');
            $tag = $this->model_app->view_ordering('tagvid','id_tag','DESC');
            if ($this->session->level=='admin'){
                $proses = $this->model_app->edit('video', array('id_video' => $id))->row_array();
            }else{
                $proses = $this->model_app->edit('video', array('id_video' => $id, 'username' => $this->session->username))->row_array();
            }

            $data = array('rows' => $proses,'record' => $record, 'tag' => $tag);
            $this->template->load('administrator/template','administrator/mod_video/view_video_edit',$data);
        }
    }

    function delete_video(){
        cek_session_akses('video',$this->session->id_session);
        if ($this->session->level=='admin'){
            $id = array('id_video' => $this->uri->segment(3));
        }else{
            $id = array('id_video' => $this->uri->segment(3), 'username'=>$this->session->username);
        }
        $this->model_app->delete('video',$id);
        redirect($this->uri->segment(1).'/video');
    }


    // Controller Modul Playlist

    function playlist(){
        cek_session_akses('playlist',$this->session->id_session);
        $data['record'] = $this->model_app->view_ordering('playlist','id_playlist','DESC');
        $this->template->load('administrator/template','administrator/mod_playlist/view_playlist',$data);
    }

    function tambah_playlist(){
        cek_session_akses('playlist',$this->session->id_session);
        if (isset($_POST['submit'])){
            $config['upload_path'] = 'asset/img_playlist/';
            $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG';
            $config['max_size'] = '3000'; // kb
            $this->load->library('upload', $config);
            $this->upload->do_upload('b');
            $hasil=$this->upload->data();
            if ($hasil['file_name']==''){
                $data = array('jdl_playlist'=>$this->input->post('a'),
                    'username'=>$this->session->username,
                    'playlist_seo'=>seo_title($this->input->post('a')),
                    'aktif'=>'Y');
            }else{
                $data = array('jdl_playlist'=>$this->input->post('a'),
                    'username'=>$this->session->username,
                    'playlist_seo'=>seo_title($this->input->post('a')),
                    'gbr_playlist'=>$hasil['file_name'],
                    'aktif'=>'Y');
            }
            $this->model_app->insert('playlist',$data);
            redirect($this->uri->segment(1).'/playlist');
        }else{
            $this->template->load('administrator/template','administrator/mod_playlist/view_playlist_tambah');
        }
    }

    function edit_playlist(){
        cek_session_akses('playlist',$this->session->id_session);
        $id = $this->uri->segment(3);
        if (isset($_POST['submit'])){
            $config['upload_path'] = 'asset/img_playlist/';
            $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG';
            $config['max_size'] = '3000'; // kb
            $this->load->library('upload', $config);
            $this->upload->do_upload('b');
            $hasil=$this->upload->data();
            if ($hasil['file_name']==''){
                $data = array('jdl_playlist'=>$this->input->post('a'),
                    'username'=>$this->session->username,
                    'playlist_seo'=>seo_title($this->input->post('a')),
                    'aktif'=>$this->input->post('c'));
            }else{
                $data = array('jdl_playlist'=>$this->input->post('a'),
                    'username'=>$this->session->username,
                    'playlist_seo'=>seo_title($this->input->post('a')),
                    'gbr_playlist'=>$hasil['file_name'],
                    'aktif'=>$this->input->post('c'));
            }
            $where = array('id_playlist' => $this->input->post('id'));
            $this->model_app->update('playlist', $data, $where);
            redirect($this->uri->segment(1).'/playlist');
        }else{
            $proses = $this->model_app->edit('playlist', array('id_playlist' => $id))->row_array();
            $data = array('rows' => $proses);
            $this->template->load('administrator/template','administrator/mod_playlist/view_playlist_edit',$data);
        }
    }

    function delete_playlist(){
        cek_session_akses('playlist',$this->session->id_session);
        $id = array('id_playlist' => $this->uri->segment(3));
        $this->model_app->delete('playlist',$id);
        redirect($this->uri->segment(1).'/playlist');
    }


    // Controller Modul Tag Video

    function tagvideo(){
        cek_session_akses('tagvideo',$this->session->id_session);
        if ($this->session->level=='admin'){
            $data['record'] = $this->model_app->view_ordering('tagvid','id_tag','DESC');
        }else{
            $data['record'] = $this->model_app->view_where_ordering('tagvid',array('username'=>$this->session->username),'id_tag','DESC');
        }
        $this->template->load('administrator/template','administrator/mod_tagvideo/view_tag',$data);
    }

    function tambah_tagvideo(){
        cek_session_akses('tagvideo',$this->session->id_session);
        if (isset($_POST['submit'])){
            $data = array('nama_tag'=>$this->db->escape_str($this->input->post('a')),
                'username'=>$this->session->username,
                'tag_seo'=>seo_title($this->input->post('a')),
                'count'=>'0');
            $this->model_app->insert('tagvid',$data);
            redirect($this->uri->segment(1).'/tagvideo');
        }else{
            $this->template->load('administrator/template','administrator/mod_tagvideo/view_tag_tambah');
        }
    }

    function edit_tagvideo(){
        cek_session_akses('tagvideo',$this->session->id_session);
        $id = $this->uri->segment(3);
        if (isset($_POST['submit'])){
            $data = array('nama_tag'=>$this->db->escape_str($this->input->post('a')),
                'username'=>$this->session->username,
                'tag_seo'=>seo_title($this->input->post('a')));
            $where = array('id_tag' => $this->input->post('id'));
            $this->model_app->update('tagvid', $data, $where);
            redirect($this->uri->segment(1).'/tagvideo');
        }else{
            if ($this->session->level=='admin'){
                $proses = $this->model_app->edit('tagvid', array('id_tag' => $id))->row_array();
            }else{
                $proses = $this->model_app->edit('tagvid', array('id_tag' => $id, 'username' => $this->session->username))->row_array();
            }

            $data = array('rows' => $proses);
            $this->template->load('administrator/template','administrator/mod_tagvideo/view_tag_edit',$data);
        }
    }

    function delete_tagvideo(){
        cek_session_akses('tagvideo',$this->session->id_session);
        if ($this->session->level=='admin'){
            $id = array('id_tag' => $this->uri->segment(3));
        }else{
            $id = array('id_tag' => $this->uri->segment(3), 'username'=>$this->session->username);
        }
        $this->model_app->delete('tagvid',$id);
        redirect($this->uri->segment(1).'/tagvideo');
    }


    // Controller Modul Komentar Video

    function komentarvideo(){
        cek_session_akses('komentarvideo',$this->session->id_session);
        $data['record'] = $this->model_app->view_join_one('komentarvid','video','id_video','id_komentar','DESC');
        $this->template->load('administrator/template','administrator/mod_komentarvideo/view_komentar',$data);
    }

    function edit_komentarvideo(){
        cek_session_akses('komentarvideo',$this->session->id_session);
        $id = $this->uri->segment(3);
        if (isset($_POST['submit'])){
            $data = array('nama_komentar'=>$this->input->post('a'),
                'url'=>$this->input->post('b'),
                'isi_komentar'=>$this->input->post('c'),
                'aktif'=>$this->input->post('d'));
            $where = array('id_komentar' => $this->input->post('id'));
            $this->model_app->update('komentarvid', $data, $where);
            redirect($this->uri->segment(1).'/komentarvideo');
        }else{
            $proses = $this->model_app->edit('komentarvid', array('id_komentar' => $id))->row_array();
            $data = array('rows' => $proses);
            $this->template->load('administrator/template','administrator/mod_komentarvideo/view_komentar_edit',$data);
        }
    }

    function delete_komentarvideo(){
        cek_session_akses('komentarvideo',$this->session->id_session);
        $id = array('id_komentar' => $this->uri->segment(3));
        $this->model_app->delete('komentarvid',$id);
        redirect($this->uri->segment(1).'/komentarvideo');
    }

    // Controller Modul Iklan Atas

    private function set_upload_iklanatas(){
        $config = array();
        $config['upload_path'] = 'asset/foto_iklanatas/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '5000'; // kb
        $config['encrypt_name'] = FALSE;
        $config['overwrite'] = TRUE;
        $this->load->library('upload', $config);
        return $config;
    }

    function iklanatas(){
        cek_session_akses('iklanatas',$this->session->id_session);
        if ($this->session->level=='admin'){
            $data['record'] = $this->model_app->view_ordering('iklanatas','id_iklanatas','DESC');
        }else{
            $data['record'] = $this->model_app->view_where_ordering('iklanatas',array('username'=>$this->session->username),'id_iklanatas','DESC');
        }
        $this->template->load('administrator/template','administrator/mod_iklanatas/view_iklanatas',$data);
    }

    function tambah_iklanatas(){
        cek_session_akses('iklanatas',$this->session->id_session);
        if (isset($_POST['submit'])){
            $config['upload_path'] = 'asset/foto_iklanatas/';
            $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG|swf';
            $config['max_size'] = '5000'; // kb
            $config['encrypt_name'] = FALSE;
            $config['overwrite'] = TRUE;
            $this->load->library('upload', $config);
            $this->upload->do_upload('c');
			// $hasil = $this->upload->data();
            $hasil=$this->upload->data()['file_name'];
            $this->func_resize($hasil, 'asset/foto_iklanatas/', 883, 312);

            if ($hasil['file_name']==''){
                $data = array('judul'=>$this->db->escape_str($this->input->post('a')),
                    'username'=>$this->session->username,
                    'url'=>$this->input->post('b'),
                    'tgl_posting'=>date('Y-m-d'));
            }else{
                $data = array('judul'=>$this->db->escape_str($this->input->post('a')),
                    'username'=>$this->session->username,
                    'url'=>$this->input->post('b'),
                    'gambar'=>$hasil,
                    'tgl_posting'=>date('Y-m-d'));
            }
            $this->model_app->insert('iklanatas',$data);
            redirect($this->uri->segment(1).'/iklanatas');
        }else{
            $this->template->load('administrator/template','administrator/mod_iklanatas/view_iklanatas_tambah');
        }
    }

    function edit_iklanatas(){
        cek_session_akses('iklanatas',$this->session->id_session);
        $id = $this->uri->segment(3);
        if (isset($_POST['submit'])){
            $config['upload_path'] = 'asset/foto_iklanatas/';
            $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG|swf';
            $config['max_size'] = '5000'; // kb
            $config['encrypt_name'] = FALSE;
            $config['overwrite'] = TRUE;
            $this->load->library('upload', $config);
            $this->upload->do_upload('c');
            $hasil = $this->upload->data()['file_name'];
            $this->func_resize($hasil, 'asset/foto_iklanatas/', 883, 312);
            if ($hasil['file_name']==''){
                $data = array('judul'=>$this->db->escape_str($this->input->post('a')),
                    'username'=>$this->session->username,
                    'url'=>$this->input->post('b'),
                    'tgl_posting'=>date('Y-m-d'));
            }else{
                $data = array('judul'=>$this->db->escape_str($this->input->post('a')),
                    'username'=>$this->session->username,
                    'url'=>$this->input->post('b'),
                    'gambar'=>$hasil,
                    'tgl_posting'=>date('Y-m-d'));
            }
            $where = array('id_iklanatas' => $this->input->post('id'));
            $this->model_app->update('iklanatas', $data, $where);
            redirect($this->uri->segment(1).'/iklanatas');
        }else{
            if ($this->session->level=='admin'){
                $proses = $this->model_app->edit('iklanatas', array('id_iklanatas' => $id))->row_array();
            }else{
                $proses = $this->model_app->edit('iklanatas', array('id_iklanatas' => $id, 'username' => $this->session->username))->row_array();
            }
            $data = array('rows' => $proses);
            $this->template->load('administrator/template','administrator/mod_iklanatas/view_iklanatas_edit',$data);
        }
    }

    function delete_iklanatas(){
        cek_session_akses('iklanatas',$this->session->id_session);
        if ($this->session->level=='admin'){
            $id = array('id_iklanatas' => $this->uri->segment(3));
        }else{
            $id = array('id_iklanatas' => $this->uri->segment(3), 'username'=>$this->session->username);
        }
        $this->model_app->delete('iklanatas',$id);
        redirect($this->uri->segment(1).'/iklanatas');
    }


	// Controller Modul Iklan Home

    function iklanhome(){
      cek_session_akses('iklanhome',$this->session->id_session);
      if ($this->session->level=='admin'){
        $data['record'] = $this->model_app->view_ordering('iklantengah','id_iklantengah','DESC');
    }else{
        $data['record'] = $this->model_app->view_where_ordering('iklantengah',array('username'=>$this->session->username),'id_iklantengah','DESC');
    }
    $this->template->load('administrator/template','administrator/mod_iklanhome/view_iklanhome',$data);
}

function tambah_iklanhome(){
  cek_session_akses('iklanhome',$this->session->id_session);
  if (isset($_POST['submit'])){
      $config['upload_path'] = 'asset/foto_iklantengah/';
      $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG|swf';
            $config['max_size'] = '3000'; // kb
            $this->load->library('upload', $config);
            $this->upload->do_upload('c');
            $hasil=$this->upload->data();
            if ($hasil['file_name']==''){
                $data = array('judul'=>$this->db->escape_str($this->input->post('a')),
                    'username'=>$this->session->username,
                    'url'=>$this->input->post('b'),
                    'tgl_posting'=>date('Y-m-d'));
            }else{
                $data = array('judul'=>$this->db->escape_str($this->input->post('a')),
                    'username'=>$this->session->username,
                    'url'=>$this->input->post('b'),
                    'gambar'=>$hasil['file_name'],
                    'tgl_posting'=>date('Y-m-d'));
            }
            $this->model_app->insert('iklantengah',$data);
            redirect($this->uri->segment(1).'/iklanhome');
        }else{
           $this->template->load('administrator/template','administrator/mod_iklanhome/view_iklanhome_tambah');
       }
   }

   function edit_iklanhome(){
      cek_session_akses('iklanhome',$this->session->id_session);
      $id = $this->uri->segment(3);
      if (isset($_POST['submit'])){
       $config['upload_path'] = 'asset/foto_iklantengah/';
       $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG|swf';
            $config['max_size'] = '3000'; // kb
            $this->load->library('upload', $config);
            $this->upload->do_upload('c');
            $hasil=$this->upload->data();
            if ($hasil['file_name']==''){
                $data = array('judul'=>$this->db->escape_str($this->input->post('a')),
                    'username'=>$this->session->username,
                    'url'=>$this->input->post('b'),
                    'tgl_posting'=>date('Y-m-d'));
            }else{
                $data = array('judul'=>$this->db->escape_str($this->input->post('a')),
                    'username'=>$this->session->username,
                    'url'=>$this->input->post('b'),
                    'gambar'=>$hasil['file_name'],
                    'tgl_posting'=>date('Y-m-d'));
            }
            $where = array('id_iklantengah' => $this->input->post('id'));
            $this->model_app->update('iklantengah', $data, $where);
            redirect($this->uri->segment(1).'/iklanhome');
        }else{
            if ($this->session->level=='admin'){
                $proses = $this->model_app->edit('iklantengah', array('id_iklantengah' => $id))->row_array();
            }else{
                $proses = $this->model_app->edit('iklantengah', array('id_iklantengah' => $id, 'username' => $this->session->username))->row_array();
            }
            $data = array('rows' => $proses);
            $this->template->load('administrator/template','administrator/mod_iklanhome/view_iklanhome_edit',$data);
        }
    }

    function delete_iklanhome(){
        cek_session_akses('iklanhome',$this->session->id_session);
        if ($this->session->level=='admin'){
            $id = array('id_iklantengah' => $this->uri->segment(3));
        }else{
            $id = array('id_iklantengah' => $this->uri->segment(3), 'username'=>$this->session->username);
        }
        $this->model_app->delete('iklantengah',$id);
        redirect($this->uri->segment(1).'/iklanhome');
    }

    // Controller Modul Iklan Sidebar

    function iklansidebar(){
        cek_session_akses('iklansidebar',$this->session->id_session);
        if ($this->session->level=='admin'){
            $data['record'] = $this->model_app->view_ordering('iklansidebar','id_iklansidebar','DESC');
        }else{
            $data['record'] = $this->model_app->view_where_ordering('iklansidebar',array('username'=>$this->session->username),'id_iklansidebar','DESC');
        }
        $this->template->load('administrator/template','administrator/mod_iklansidebar/view_iklansidebar',$data);
    }

    function tambah_iklansidebar(){
        cek_session_akses('iklansidebar',$this->session->id_session);
        if (isset($_POST['submit'])){
            $config['upload_path'] = 'asset/foto_iklansidebar/';
            $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG|swf';
            $config['max_size'] = '3000'; // kb
            $this->load->library('upload', $config);
            $this->upload->do_upload('c');
            $hasil=$this->upload->data()['file_name'];
            $this->func_resize($hasil, 'asset/foto_iklansidebar/', 307, 562);
            if ($hasil['file_name']==''){
                $data = array('judul'=>$this->db->escape_str($this->input->post('a')),
                    'username'=>$this->session->username,
                    'url'=>$this->input->post('b'),
                    'tgl_posting'=>date('Y-m-d'));
            }else{
                $data = array('judul'=>$this->db->escape_str($this->input->post('a')),
                    'username'=>$this->session->username,
                    'url'=>$this->input->post('b'),
                    'gambar'=>$hasil,
                    'tgl_posting'=>date('Y-m-d'));
            }
            $this->model_app->insert('iklansidebar',$data);
            redirect($this->uri->segment(1).'/iklansidebar');
        }else{
            $this->template->load('administrator/template','administrator/mod_iklansidebar/view_iklansidebar_tambah');
        }
    }

    function edit_iklansidebar(){
        cek_session_akses('iklansidebar',$this->session->id_session);
        $id = $this->uri->segment(3);
        if (isset($_POST['submit'])){
            $config['upload_path'] = 'asset/foto_iklansidebar/';
            $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG|swf';
            $config['max_size'] = '3000'; // kb
            $this->load->library('upload', $config);
            $this->upload->do_upload('c');
            $hasil=$this->upload->data()['file_name'];
            $this->func_resize($hasil, 'asset/foto_iklansidebar/', 307, 562);
            if ($hasil['file_name']==''){
                $data = array('judul'=>$this->db->escape_str($this->input->post('a')),
                    'username'=>$this->session->username,
                    'url'=>$this->input->post('b'),
                    'tgl_posting'=>date('Y-m-d'));
            }else{
                $data = array('judul'=>$this->db->escape_str($this->input->post('a')),
                    'username'=>$this->session->username,
                    'url'=>$this->input->post('b'),
                    'gambar'=>$hasil,
                    'tgl_posting'=>date('Y-m-d'));
            }
            $where = array('id_iklansidebar' => $this->input->post('id'));
            $this->model_app->update('iklansidebar', $data, $where);
            redirect($this->uri->segment(1).'/iklansidebar');
        }else{
            if ($this->session->level=='admin'){
                $proses = $this->model_app->edit('iklansidebar', array('id_iklansidebar' => $id))->row_array();
            }else{
                $proses = $this->model_app->edit('iklansidebar', array('id_iklansidebar' => $id, 'username' => $this->session->username))->row_array();
            }
            $data = array('rows' => $proses);
            $this->template->load('administrator/template','administrator/mod_iklansidebar/view_iklansidebar_edit',$data);
        }
    }

    function delete_iklansidebar(){
        cek_session_akses('iklansidebar',$this->session->id_session);
        if ($this->session->level=='admin'){
            $id = array('id_iklansidebar' => $this->uri->segment(3));
        }else{
            $id = array('id_iklansidebar' => $this->uri->segment(3), 'username'=>$this->session->username);
        }
        $this->model_app->delete('iklansidebar',$id);
        redirect($this->uri->segment(1).'/iklansidebar');
    }

    // Controller Modul Iklan Bloggg
    function iklanblog(){
        cek_session_akses('iklanblog',$this->session->id_session);
        if ($this->session->level=='admin'){
            $data['record'] = $this->model_app->view_ordering('iklanblog','id_iklanblog','DESC');
        }else{
            $data['record'] = $this->model_app->view_where_ordering('iklanblog',array('username'=>$this->session->username),'id_iklanblog','DESC');
        }
        $this->template->load('administrator/template','administrator/mod_iklanblog/view_iklanblog',$data);
    }

    function tambah_iklanblog(){
        cek_session_akses('iklanblog',$this->session->id_session);
        if (isset($_POST['submit'])){
            $config['upload_path'] = 'asset/foto_iklanblog/';
            $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG|swf';
            $config['max_size'] = '3000'; // kb
            $this->load->library('upload', $config);
            $this->upload->do_upload('c');
            $hasil=$this->upload->data();
            $this->func_resize($hasil, 'asset/foto_iklanblog/', 500, 500);
            if ($hasil['file_name']==''){
                $data = array('judul'=>$this->db->escape_str($this->input->post('a')),
                    'username'=>$this->session->username,
                    'url'=>$this->input->post('b'),
                    'tgl_posting'=>date('Y-m-d'));
            }else{
                $data = array('judul'=>$this->db->escape_str($this->input->post('a')),
                    'username'=>$this->session->username,
                    'url'=>$this->input->post('b'),
                    'gambar'=>$hasil['file_name'],
                    'tgl_posting'=>date('Y-m-d'));
            }
            $this->model_app->insert('iklanblog',$data);
            redirect($this->uri->segment(1).'/iklanblog');
        }else{
            $this->template->load('administrator/template','administrator/mod_iklanblog/view_iklanblog_tambah');
        }
    }

    function edit_iklanblog(){
        cek_session_akses('iklanblog',$this->session->id_session);
        $id = $this->uri->segment(3);
        if (isset($_POST['submit'])){
            $config['upload_path'] = 'asset/foto_iklanblog/';
            $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG|swf';
            $config['max_size'] = '3000'; // kb
            $config['encrypt_name'] = FALSE;
            $config['overwrite'] = TRUE;
            $this->load->library('upload', $config);
            $this->upload->do_upload('c');
            $hasil=$this->upload->data()['file_name'];
            $this->func_resize($hasil, 'asset/foto_iklanblog/', 500, 500);
            if ($hasil['file_name']==''){
                $data = array('judul'=>$this->db->escape_str($this->input->post('a')),
                    'username'=>$this->session->username,
                    'url'=>$this->input->post('b'),
                    'tgl_posting'=>date('Y-m-d'));
            }else{
                $data = array('judul'=>$this->db->escape_str($this->input->post('a')),
                    'username'=>$this->session->username,
                    'url'=>$this->input->post('b'),
                    'gambar'=>$hasil,
                    'tgl_posting'=>date('Y-m-d'));
            }
            $where = array('id_iklanblog' => $this->input->post('id'));
            $this->model_app->update('iklanblog', $data, $where);
            redirect($this->uri->segment(1).'/iklanblog');
        }else{
            if ($this->session->level=='admin'){
                $proses = $this->model_app->edit('iklanblog', array('id_iklanblog' => $id))->row_array();
            }else{
                $proses = $this->model_app->edit('iklanblog', array('id_iklanblog' => $id, 'username' => $this->session->username))->row_array();
            }
            $data = array('rows' => $proses);
            $this->template->load('administrator/template','administrator/mod_iklanblog/view_iklanblog_edit',$data);
        }
    }

    function delete_iklanblog(){
        cek_session_akses('iklanblog',$this->session->id_session);
        if ($this->session->level=='admin'){
            $id = array('id_iklanblog' => $this->uri->segment(3));
        }else{
            $id = array('id_iklanblog' => $this->uri->segment(3), 'username'=>$this->session->username);
        }
        $this->model_app->delete('iklanblog',$id);
        redirect($this->uri->segment(1).'/iklanblog');
    }

    // function iklansidebar(){
        // cek_session_akses('iklansidebar',$this->session->id_session);
        // if ($this->session->level=='admin'){
            // $data['record'] = $this->model_app->view_ordering('pasangiklan','id_pasangiklan','DESC');
        // }else{
            // $data['record'] = $this->model_app->view_where_ordering('pasangiklan',array('username'=>$this->session->username),'id_pasangiklan','DESC');
        // }
        // $this->template->load('administrator/template','administrator/mod_iklansidebar/view_iklansidebar',$data);
    // }

    // function tambah_iklansidebar(){
        // cek_session_akses('iklansidebar',$this->session->id_session);
        // if (isset($_POST['submit'])){
            // $config['upload_path'] = 'asset/foto_pasangiklan/';
            // $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG|swf';
            // $config['max_size'] = '3000'; // kb
            // $this->load->library('upload', $config);
            // $this->upload->do_upload('c');
            // $hasil=$this->upload->data();
            // if ($hasil['file_name']==''){
                // $data = array('judul'=>$this->db->escape_str($this->input->post('a')),
                                // 'username'=>$this->session->username,
                                // 'url'=>$this->input->post('b'),
                                // 'tgl_posting'=>date('Y-m-d'));
            // }else{
                // $data = array('judul'=>$this->db->escape_str($this->input->post('a')),
                                // 'username'=>$this->session->username,
                                // 'url'=>$this->input->post('b'),
                                // 'gambar'=>$hasil['file_name'],
                                // 'tgl_posting'=>date('Y-m-d'));
            // }
            // $this->model_app->insert('pasangiklan',$data);
            // redirect($this->uri->segment(1).'/iklansidebar');
        // }else{
            // $this->template->load('administrator/template','administrator/mod_iklansidebar/view_iklansidebar_tambah');
        // }
    // }

    // function edit_iklansidebar(){
        // cek_session_akses('iklansidebar',$this->session->id_session);
        // $id = $this->uri->segment(3);
        // if (isset($_POST['submit'])){
            // $config['upload_path'] = 'asset/foto_pasangiklan/';
            // $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG|swf';
            // $config['max_size'] = '3000'; // kb
            // $this->load->library('upload', $config);
            // $this->upload->do_upload('c');
            // $hasil=$this->upload->data();
            // if ($hasil['file_name']==''){
                // $data = array('judul'=>$this->db->escape_str($this->input->post('a')),
                                // 'username'=>$this->session->username,
                                // 'url'=>$this->input->post('b'),
                                // 'tgl_posting'=>date('Y-m-d'));
            // }else{
                // $data = array('judul'=>$this->db->escape_str($this->input->post('a')),
                                // 'username'=>$this->session->username,
                                // 'url'=>$this->input->post('b'),
                                // 'gambar'=>$hasil['file_name'],
                                // 'tgl_posting'=>date('Y-m-d'));
            // }
            // $where = array('id_pasangiklan' => $this->input->post('id'));
            // $this->model_app->update('pasangiklan', $data, $where);
            // redirect($this->uri->segment(1).'/iklansidebar');
        // }else{
            // if ($this->session->level=='admin'){
                // $proses = $this->model_app->edit('pasangiklan', array('id_pasangiklan' => $id))->row_array();
            // }else{
                // $proses = $this->model_app->edit('pasangiklan', array('id_pasangiklan' => $id, 'username' => $this->session->username))->row_array();
            // }
            // $data = array('rows' => $proses);
            // $this->template->load('administrator/template','administrator/mod_iklansidebar/view_iklansidebar_edit',$data);
        // }
    // }

    // function delete_iklansidebar(){
        // cek_session_akses('iklansidebar',$this->session->id_session);
        // if ($this->session->level=='admin'){
            // $id = array('id_pasangiklan' => $this->uri->segment(3));
        // }else{
            // $id = array('id_pasangiklan' => $this->uri->segment(3), 'username'=>$this->session->username);
        // }
        // $this->model_app->delete('pasangiklan',$id);
        // redirect($this->uri->segment(1).'/iklansidebar');
    // }


    // Controller Modul banner Link

    function banner(){
        cek_session_akses('banner',$this->session->id_session);
        $data['record'] = $this->model_app->view_ordering('banner','id_banner','DESC');
        $this->template->load('administrator/template','administrator/mod_banner/view_banner',$data);
    }

    function tambah_banner(){
        cek_session_akses('banner',$this->session->id_session);
        if (isset($_POST['submit'])){
            $data = array('judul'=>$this->db->escape_str($this->input->post('a')),
                'url'=>$this->input->post('b'),
                'tgl_posting'=>date('Y-m-d'));
            $this->model_app->insert('banner',$data);
            redirect($this->uri->segment(1).'/banner');
        }else{
            $this->template->load('administrator/template','administrator/mod_banner/view_banner_tambah');
        }
    }

    function edit_banner(){
        cek_session_akses('banner',$this->session->id_session);
        $id = $this->uri->segment(3);
        if (isset($_POST['submit'])){
            $data = array('judul'=>$this->db->escape_str($this->input->post('a')),
                'url'=>$this->input->post('b'),
                'tgl_posting'=>date('Y-m-d'));

            $where = array('id_banner' => $this->input->post('id'));
            $this->model_app->update('banner', $data, $where);
            redirect($this->uri->segment(1).'/banner');
        }else{
            $proses = $this->model_app->edit('banner', array('id_banner' => $id))->row_array();
            $data = array('rows' => $proses);
            $this->template->load('administrator/template','administrator/mod_banner/view_banner_edit',$data);
        }
    }

    function delete_banner(){
        cek_session_akses('banner',$this->session->id_session);
        $id = array('id_banner' => $this->uri->segment(3));
        $this->model_app->delete('banner',$id);
        redirect($this->uri->segment(1).'/banner');
    }


    // Controller Modul Logo

    function logowebsite(){
        cek_session_akses('logowebsite',$this->session->id_session);
        if (isset($_POST['submit'])){
            $config['upload_path'] = 'asset/logo/';
            $config['allowed_types'] = 'gif|jpg|png|JPG';
            $config['max_size'] = '2000'; // kb
            $this->load->library('upload', $config);
            $this->upload->do_upload('logo');
            $hasil=$this->upload->data();
            $datadb = array('gambar'=>$hasil['file_name']);
            $where = array('id_logo' => $this->input->post('id'));
            $this->model_app->update('logo', $datadb, $where);
            redirect($this->uri->segment(1).'/logowebsite');
        }else{
            $data['record'] = $this->model_app->view('logo');
            $this->template->load('administrator/template','administrator/mod_logowebsite/view_logowebsite',$data);
        }
    }


    // Controller Modul Template Website

    function templatewebsite(){
        cek_session_akses('templatewebsite',$this->session->id_session);
        if ($this->session->level=='admin'){
            $data['record'] = $this->model_app->view_ordering('templates','id_templates','DESC');
        }else{
            $data['record'] = $this->model_app->view_where_ordering('templates',array('username'=>$this->session->username),'id_templates','DESC');
        }
        $this->template->load('administrator/template','administrator/mod_template/view_template',$data);
    }

    function tambah_templatewebsite(){
        cek_session_akses('templatewebsite',$this->session->id_session);
        if (isset($_POST['submit'])){
            $data = array('judul'=>$this->db->escape_str($this->input->post('a')),
                'username'=>$this->session->username,
                'pembuat'=>$this->input->post('b'),
                'folder'=>$this->input->post('c'));
            $this->model_app->insert('templates',$data);
            redirect($this->uri->segment(1).'/templatewebsite');
        }else{
            $this->template->load('administrator/template','administrator/mod_template/view_template_tambah');
        }
    }

    function edit_templatewebsite(){
        cek_session_akses('templatewebsite',$this->session->id_session);
        $id = $this->uri->segment(3);
        if (isset($_POST['submit'])){
            $data = array('judul'=>$this->db->escape_str($this->input->post('a')),
                'username'=>$this->session->username,
                'pembuat'=>$this->input->post('b'),
                'folder'=>$this->input->post('c'));
            $where = array('id_templates' => $this->input->post('id'));
            $this->model_app->update('templates', $data, $where);
            redirect($this->uri->segment(1).'/templatewebsite');
        }else{
            if ($this->session->level=='admin'){
                $proses = $this->model_app->edit('templates', array('id_templates' => $id))->row_array();
            }else{
                $proses = $this->model_app->edit('templates', array('id_templates' => $id, 'username' => $this->session->username))->row_array();
            }
            $data = array('rows' => $proses);
            $this->template->load('administrator/template','administrator/mod_template/view_template_edit',$data);
        }
    }

    function aktif_templatewebsite(){
        cek_session_akses('templatewebsite',$this->session->id_session);
        $id = $this->uri->segment(3);
        if ($this->uri->segment(4)=='Y'){ $aktif = 'N'; }else{ $aktif = 'Y'; }

        $data = array('aktif'=>$aktif);
        $where = array('id_templates' => $id);
        $this->model_app->update('templates', $data, $where);

        $dataa = array('aktif'=>'N');
        $wheree = array('id_templates !=' => $id);
        $this->model_app->update('templates', $dataa, $wheree);

        redirect($this->uri->segment(1).'/templatewebsite');
    }

    function delete_templatewebsite(){
        cek_session_akses('templatewebsite',$this->session->id_session);
        if ($this->session->level=='admin'){
            $id = array('id_templates' => $this->uri->segment(3));
        }else{
            $id = array('id_templates' => $this->uri->segment(3), 'username'=>$this->session->username);
        }
        $this->model_app->delete('templates',$id);
        redirect($this->uri->segment(1).'/templatewebsite');
    }


    // Controller Modul Download

    function background(){
        cek_session_akses('background',$this->session->id_session);
        $id = $this->uri->segment(3);
        if (isset($_POST['submit'])){
            $data = array('gambar'=>$this->input->post('a'));
            $where = array('id_background' => 1);
            $this->model_app->update('background', $data, $where);
            redirect($this->uri->segment(1).'/background');
        }else{
            $proses = $this->model_app->edit('background', array('id_background' => 1))->row_array();
            $data = array('rows' => $proses);
            $this->template->load('administrator/template','administrator/mod_background/view_background',$data);
        }
    }


	// Controller Modul Download

    function download(){
      cek_session_akses('download',$this->session->id_session);
      $data['record'] = $this->model_app->view_ordering('download','id_download','DESC');
      $this->template->load('administrator/template','administrator/mod_download/view_download',$data);
  }

  function tambah_download(){
      cek_session_akses('download',$this->session->id_session);
      if (isset($_POST['submit'])){
       $config['upload_path'] = 'asset/files/';
       $config['allowed_types'] = 'gif|jpg|png|zip|rar|pdf|doc|docx|ppt|pptx|xls|xlsx|txt';
            $config['max_size'] = '25000'; // kb
            $this->load->library('upload', $config);
            $this->upload->do_upload('b');
            $hasil=$this->upload->data();
            if ($hasil['file_name']==''){
                $data = array('judul'=>$this->db->escape_str($this->input->post('a')),
                    'tgl_posting'=>date('Y-m-d'),
                    'hits'=>'0');
            }else{
                $data = array('judul'=>$this->db->escape_str($this->input->post('a')),
                    'nama_file'=>$hasil['file_name'],
                    'tgl_posting'=>date('Y-m-d'),
                    'hits'=>'0');
            }
            $this->model_app->insert('download',$data);
            redirect($this->uri->segment(1).'/download');
        }else{
           $this->template->load('administrator/template','administrator/mod_download/view_download_tambah');
       }
   }

   function edit_download(){
      cek_session_akses('download',$this->session->id_session);
      $id = $this->uri->segment(3);
      if (isset($_POST['submit'])){
       $config['upload_path'] = 'asset/files/';
       $config['allowed_types'] = 'gif|jpg|png|zip|rar|pdf|doc|docx|ppt|pptx|xls|xlsx|txt';
            $config['max_size'] = '25000'; // kb
            $this->load->library('upload', $config);
            $this->upload->do_upload('b');
            $hasil=$this->upload->data();
            if ($hasil['file_name']==''){
                $data = array('judul'=>$this->db->escape_str($this->input->post('a')));
            }else{
                $data = array('judul'=>$this->db->escape_str($this->input->post('a')),
                    'nama_file'=>$hasil['file_name']);
            }
            $where = array('id_download' => $this->input->post('id'));
            $this->model_app->update('download', $data, $where);
            redirect($this->uri->segment(1).'/download');
        }else{
           $proses = $this->model_app->edit('download', array('id_download' => $id))->row_array();
           $data = array('rows' => $proses);
           $this->template->load('administrator/template','administrator/mod_download/view_download_edit',$data);
       }
   }

   function delete_download(){
    cek_session_akses('download',$this->session->id_session);
    $id = array('id_download' => $this->uri->segment(3));
    $this->model_app->delete('download',$id);
    redirect($this->uri->segment(1).'/download');
}


	// Controller Modul Agenda

function agenda(){
  cek_session_akses('agenda',$this->session->id_session);
  if ($this->session->level=='admin'){
      $data['record'] = $this->model_app->view_ordering('agenda','id_agenda','DESC');
  }else{
    $data['record'] = $this->model_app->view_where_ordering('agenda',array('username'=>$this->session->username),'id_agenda','DESC');
}
$this->template->load('administrator/template','administrator/mod_agenda/view_agenda',$data);
}

function tambah_agenda(){
  cek_session_akses('agenda',$this->session->id_session);
  if (isset($_POST['submit'])){
   $config['upload_path'] = 'asset/foto_agenda/';
   $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG';
            $config['max_size'] = '3000'; // kb
            $this->load->library('upload', $config);
            $this->upload->do_upload('c');
            $hasil=$this->upload->data();
            $ex = explode(' - ',$this->input->post('f'));
            $exx = explode('/',$ex[0]);
            $exy = explode('/',$ex[1]);
            $mulai = $exx[2].'-'.$exx[0].'-'.$exx[1];
            $selesai = $exy[2].'-'.$exy[0].'-'.$exy[1];
            if ($hasil['file_name']==''){
                $data = array('tema'=>$this->db->escape_str($this->input->post('a')),
                    'tema_seo'=>seo_title($this->input->post('a')),
                    'isi_agenda'=>$this->input->post('b'),
                    'tempat'=>$this->db->escape_str($this->input->post('d')),
                    'pengirim'=>$this->db->escape_str($this->input->post('g')),
                    'tgl_mulai'=>$mulai,
                    'tgl_selesai'=>$selesai,
                    'tgl_posting'=>date('Y-m-d'),
                    'jam'=>$this->db->escape_str($this->input->post('e')),
                    'dibaca'=>'0',
                    'username'=>$this->session->username);
            }else{
                $data = array('tema'=>$this->db->escape_str($this->input->post('a')),
                    'tema_seo'=>seo_title($this->input->post('a')),
                    'isi_agenda'=>$this->input->post('b'),
                    'tempat'=>$this->db->escape_str($this->input->post('d')),
                    'pengirim'=>$this->db->escape_str($this->input->post('g')),
                    'gambar'=>$hasil['file_name'],
                    'tgl_mulai'=>$mulai,
                    'tgl_selesai'=>$selesai,
                    'tgl_posting'=>date('Y-m-d'),
                    'jam'=>$this->db->escape_str($this->input->post('e')),
                    'dibaca'=>'0',
                    'username'=>$this->session->username);
            }
            $this->model_app->insert('agenda',$data);
            redirect($this->uri->segment(1).'/agenda');
        }else{
           $this->template->load('administrator/template','administrator/mod_agenda/view_agenda_tambah');
       }
   }

   function edit_agenda(){
      cek_session_akses('agenda',$this->session->id_session);
      $id = $this->uri->segment(3);
      if (isset($_POST['submit'])){
       $config['upload_path'] = 'asset/foto_agenda/';
       $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG';
            $config['max_size'] = '3000'; // kb
            $this->load->library('upload', $config);
            $this->upload->do_upload('c');
            $hasil=$this->upload->data();
            $ex = explode(' - ',$this->input->post('f'));
            $exx = explode('/',$ex[0]);
            $exy = explode('/',$ex[1]);
            $mulai = $exx[2].'-'.$exx[0].'-'.$exx[1];
            $selesai = $exy[2].'-'.$exy[0].'-'.$exy[1];
            if ($hasil['file_name']==''){
                $data = array('tema'=>$this->db->escape_str($this->input->post('a')),
                    'tema_seo'=>seo_title($this->input->post('a')),
                    'isi_agenda'=>$this->input->post('b'),
                    'tempat'=>$this->db->escape_str($this->input->post('d')),
                    'pengirim'=>$this->db->escape_str($this->input->post('g')),
                    'tgl_mulai'=>$mulai,
                    'tgl_selesai'=>$selesai,
                    'jam'=>$this->db->escape_str($this->input->post('e')));
            }else{
                $data = array('tema'=>$this->db->escape_str($this->input->post('a')),
                    'tema_seo'=>seo_title($this->input->post('a')),
                    'isi_agenda'=>$this->input->post('b'),
                    'tempat'=>$this->db->escape_str($this->input->post('d')),
                    'pengirim'=>$this->db->escape_str($this->input->post('g')),
                    'gambar'=>$hasil['file_name'],
                    'tgl_mulai'=>$mulai,
                    'tgl_selesai'=>$selesai,
                    'jam'=>$this->db->escape_str($this->input->post('e')));
            }

            $where = array('id_agenda' => $this->input->post('id'));
            $this->model_app->update('agenda', $data, $where);
            redirect($this->uri->segment(1).'/agenda');
        }else{
            if ($this->session->level=='admin'){
                $proses = $this->model_app->edit('agenda', array('id_agenda' => $id))->row_array();
            }else{
                $proses = $this->model_app->edit('agenda', array('id_agenda' => $id, 'username' => $this->session->username))->row_array();
            }

            $data = array('rows' => $proses);
            $this->template->load('administrator/template','administrator/mod_agenda/view_agenda_edit',$data);
        }
    }

    function delete_agenda(){
        cek_session_akses('agenda',$this->session->id_session);
        if ($this->session->level=='admin'){
            $id = array('id_agenda' => $this->uri->segment(3));
        }else{
            $id = array('id_agenda' => $this->uri->segment(3), 'username'=>$this->session->username);
        }
        $this->model_app->delete('agenda',$id);
        redirect($this->uri->segment(1).'/agenda');
    }


    // Controller Modul Sekilas Info

    function sekilasinfo(){
        cek_session_akses('sekilasinfo',$this->session->id_session);
        $data['record'] = $this->model_app->view_ordering('sekilasinfo','id_sekilas','DESC');
        $this->template->load('administrator/template','administrator/mod_sekilasinfo/view_sekilasinfo',$data);
    }

    function tambah_sekilasinfo(){
        cek_session_akses('sekilasinfo',$this->session->id_session);
        if (isset($_POST['submit'])){
            $config['upload_path'] = 'asset/foto_info/';
            $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG';
            $config['max_size'] = '2500'; // kb
            $this->load->library('upload', $config);
            $this->upload->do_upload('b');
            $hasil=$this->upload->data();
            if ($hasil['file_name']==''){
                $data = array('info'=>$this->input->post('a'),
                  'tgl_posting'=>date('Y-m-d'),
                  'aktif'=>'Y');
            }else{
                $data = array('info'=>$this->input->post('a'),
                  'tgl_posting'=>date('Y-m-d'),
                  'gambar'=>$hasil['file_name'],
                  'aktif'=>'Y');
            }
            $this->model_app->insert('sekilasinfo',$data);
            redirect($this->uri->segment(1).'/sekilasinfo');
        }else{
            $this->template->load('administrator/template','administrator/mod_sekilasinfo/view_sekilasinfo_tambah');
        }
    }

    function edit_sekilasinfo(){
        cek_session_akses('sekilasinfo',$this->session->id_session);
        $id = $this->uri->segment(3);
        if (isset($_POST['submit'])){
            $config['upload_path'] = 'asset/foto_info/';
            $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG';
            $config['max_size'] = '2500'; // kb
            $this->load->library('upload', $config);
            $this->upload->do_upload('b');
            $hasil=$this->upload->data();
            if ($hasil['file_name']==''){
                $data = array('info'=>$this->input->post('a'),
                  'aktif'=>$this->input->post('f'));
            }else{
                $data = array('info'=>$this->input->post('a'),
                  'gambar'=>$hasil['file_name'],
                  'aktif'=>$this->input->post('f'));
            }

            $where = array('id_sekilas' => $this->input->post('id'));
            $this->model_app->update('sekilasinfo', $data, $where);
            redirect($this->uri->segment(1).'/sekilasinfo');
        }else{
            $proses = $this->model_app->edit('sekilasinfo', array('id_sekilas' => $id))->row_array();
            $data = array('rows' => $proses);
            $this->template->load('administrator/template','administrator/mod_sekilasinfo/view_sekilasinfo_edit',$data);
        }
    }

    function delete_sekilasinfo(){
        cek_session_akses('sekilasinfo',$this->session->id_session);
        $id = array('id_sekilas' => $this->uri->segment(3));
        $this->model_app->delete('sekilasinfo',$id);
        redirect($this->uri->segment(1).'/sekilasinfo');
    }



    // Controller Modul Jajak Pendapat

    function jajakpendapat(){
        cek_session_akses('jajakpendapat',$this->session->id_session);
        if ($this->session->level=='admin'){
            $data['record'] = $this->model_app->view_ordering('poling','id_poling','DESC');
        }else{
            $data['record'] = $this->model_app->view_where_ordering('poling',array('username'=>$this->session->username),'id_poling','DESC');
        }
        $this->template->load('administrator/template','administrator/mod_jajakpendapat/view_jajakpendapat',$data);
    }

    function tambah_jajakpendapat(){
        cek_session_akses('jajakpendapat',$this->session->id_session);
        if (isset($_POST['submit'])){
            $data = array('pilihan'=>$this->input->post('a'),
              'status'=>$this->input->post('b'),
              'username'=>$this->session->username,
              'rating'=>'0',
              'aktif'=>$this->input->post('c'));
            $this->model_app->insert('poling',$data);
            redirect($this->uri->segment(1).'/jajakpendapat');
        }else{
            $this->template->load('administrator/template','administrator/mod_jajakpendapat/view_jajakpendapat_tambah');
        }
    }

    function edit_jajakpendapat(){
        cek_session_akses('jajakpendapat',$this->session->id_session);
        $id = $this->uri->segment(3);
        if (isset($_POST['submit'])){
            $data = array('pilihan'=>$this->input->post('a'),
              'status'=>$this->input->post('b'),
              'aktif'=>$this->input->post('c'));
            $where = array('id_poling' => $this->input->post('id'));
            $this->model_app->update('poling', $data, $where);
            redirect($this->uri->segment(1).'/jajakpendapat');
        }else{
            if ($this->session->level=='admin'){
             $proses = $this->model_app->edit('poling', array('id_poling' => $id))->row_array();
         }else{
            $proses = $this->model_app->edit('poling', array('id_poling' => $id, 'username' => $this->session->username))->row_array();
        }
        $data = array('rows' => $proses);
        $this->template->load('administrator/template','administrator/mod_jajakpendapat/view_jajakpendapat_edit',$data);
    }
}

function delete_jajakpendapat(){
    cek_session_akses('jajakpendapat',$this->session->id_session);
    if ($this->session->level=='admin'){
        $id = array('id_poling' => $this->uri->segment(3));
    }else{
        $id = array('id_poling' => $this->uri->segment(3), 'username'=>$this->session->username);
    }
    $this->model_app->delete('poling',$id);
    redirect($this->uri->segment(1).'/jajakpendapat');
}


	// Controller Modul YM

function ym(){
  cek_session_akses('ym',$this->session->id_session);
  $data['record'] = $this->model_app->view_ordering('mod_ym','id','DESC');
  $this->template->load('administrator/template','administrator/mod_ym/view_ym',$data);
}

function tambah_ym(){
  cek_session_akses('ym',$this->session->id_session);
  if (isset($_POST['submit'])){
   $data = array('nama'=>$this->db->escape_str($this->input->post('a')),
    'username'=>seo_title($this->input->post('b')),
    'ym_icon'=>$this->input->post('c'));
   $this->model_app->insert('mod_ym',$data);
   redirect($this->uri->segment(1).'/ym');
}else{
   $this->template->load('administrator/template','administrator/mod_ym/view_ym_tambah');
}
}

function edit_ym(){
  cek_session_akses('ym',$this->session->id_session);
  $id = $this->uri->segment(3);
  if (isset($_POST['submit'])){
   $data = array('nama'=>$this->db->escape_str($this->input->post('a')),
    'username'=>seo_title($this->input->post('b')),
    'ym_icon'=>$this->input->post('c'));
   $where = array('id' => $this->input->post('id'));
   $this->model_app->update('mod_ym', $data, $where);
   redirect($this->uri->segment(1).'/ym');
}else{
   $proses = $this->model_app->edit('mod_ym', array('id' => $id))->row_array();
   $data = array('rows' => $proses);
   $this->template->load('administrator/template','administrator/mod_ym/view_ym_edit',$data);
}
}

function delete_ym(){
    cek_session_akses('ym',$this->session->id_session);
    $id = array('id' => $this->uri->segment(3));
    $this->model_app->delete('mod_ym',$id);
    redirect($this->uri->segment(1).'/ym');
}

    // Controller Modul Alamat

function alamat(){
    cek_session_akses('alamat',$this->session->id_session);
    $id = $this->uri->segment(3);
    if (isset($_POST['submit'])){
        $data = array('alamat'=>$this->input->post('a'));
        $where = array('id_alamat' => 1);
        $this->model_app->update('mod_alamat', $data, $where);
        redirect($this->uri->segment(1).'/alamat');
    }else{
        $proses = $this->model_app->edit('mod_alamat', array('id_alamat' => 1))->row_array();
        $data = array('rows' => $proses);
        $this->template->load('administrator/template','administrator/mod_alamat/view_alamat',$data);
    }
}


	// Controller Modul Pesan Masuk

function pesanmasuk(){
  cek_session_akses('pesanmasuk',$this->session->id_session);
  $data['record'] = $this->model_app->view_ordering('hubungi','id_hubungi','DESC');
  $this->template->load('administrator/template','administrator/mod_pesanmasuk/view_pesanmasuk',$data);
}

function detail_pesanmasuk(){
  cek_session_akses('pesanmasuk',$this->session->id_session);
  $id = $this->uri->segment(3);
  $this->db->query("UPDATE hubungi SET dibaca='Y' where id_hubungi='$id'");
  if (isset($_POST['submit'])){
   $nama           = $this->input->post('a');
   $email           = $this->input->post('b');
   $subject         = $this->input->post('c');
   $message         = $this->input->post('isi')." <br><hr><br> ".$this->input->post('d');

   $this->email->from('robby.prihandaya@gmail.com', 'PHPMU.COM');
   $this->email->to($email);
   $this->email->cc('');
   $this->email->bcc('');

   $this->email->subject($subject);
   $this->email->message($message);
   $this->email->set_mailtype("html");
   $this->email->send();

   $config['protocol'] = 'sendmail';
   $config['mailpath'] = '/usr/sbin/sendmail';
   $config['charset'] = 'utf-8';
   $config['wordwrap'] = TRUE;
   $config['mailtype'] = 'html';
   $this->email->initialize($config);

   $proses = $this->model_app->edit('hubungi', array('id_hubungi' => $id))->row_array();
   $data = array('rows' => $proses);
   $this->template->load('administrator/template','administrator/mod_pesanmasuk/view_pesanmasuk_detail',$data);
}else{
   $proses = $this->model_app->edit('hubungi', array('id_hubungi' => $id))->row_array();
   $data = array('rows' => $proses);
   $this->template->load('administrator/template','administrator/mod_pesanmasuk/view_pesanmasuk_detail',$data);
}
}

function delete_pesanmasuk(){
    cek_session_akses('pesanmasuk',$this->session->id_session);
    $id = array('id_hubungi' => $this->uri->segment(3));
    $this->model_app->delete('hubungi',$id);
    redirect($this->uri->segment(1).'/pesanmasuk');
}


	// Controller Modul User

function manajemenuser(){
  cek_session_akses('manajemenuser',$this->session->id_session);
  $data['record'] = $this->model_app->view_ordering('users','username','DESC');
  $this->template->load('administrator/template','administrator/mod_users/view_users',$data);
}

function tambah_manajemenuser(){
  cek_session_akses('manajemenuser',$this->session->id_session);
  $id = $this->session->username;
  if (isset($_POST['submit'])){
   $config['upload_path'] = 'asset/foto_user/';
   $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG';
            $config['max_size'] = '1000'; // kb
            $this->load->library('upload', $config);
            $this->upload->do_upload('f');
            $hasil=$this->upload->data();
            if ($hasil['file_name']==''){
                $data = array('username'=>$this->db->escape_str($this->input->post('a')),
                    'password'=>hash("sha512", md5($this->input->post('b'))),
                    'nama_lengkap'=>$this->db->escape_str($this->input->post('c')),
                    'email'=>$this->db->escape_str($this->input->post('d')),
                    'no_telp'=>$this->db->escape_str($this->input->post('e')),
                    'level'=>$this->db->escape_str($this->input->post('g')),
                    'blokir'=>'N',
                    'id_session'=>md5($this->input->post('a')).'-'.date('YmdHis'));
            }else{
                $data = array('username'=>$this->db->escape_str($this->input->post('a')),
                    'password'=>hash("sha512", md5($this->input->post('b'))),
                    'nama_lengkap'=>$this->db->escape_str($this->input->post('c')),
                    'email'=>$this->db->escape_str($this->input->post('d')),
                    'no_telp'=>$this->db->escape_str($this->input->post('e')),
                    'foto'=>$hasil['file_name'],
                    'level'=>$this->db->escape_str($this->input->post('g')),
                    'blokir'=>'N',
                    'id_session'=>md5($this->input->post('a')).'-'.date('YmdHis'));
            }
            $this->model_app->insert('users',$data);

            $mod=count($this->input->post('modul'));
            $modul=$this->input->post('modul');
            $sess = md5($this->input->post('a')).'-'.date('YmdHis');
            for($i=0;$i<$mod;$i++){
                $datam = array('id_session'=>$sess,
                  'id_modul'=>$modul[$i]);
                $this->model_app->insert('users_modul',$datam);
            }

            redirect($this->uri->segment(1).'/edit_manajemenuser/'.$this->input->post('a'));
        }else{
            $proses = $this->model_app->view_where_ordering('modul', array('publish' => 'Y','status' => 'user'), 'id_modul','DESC');
            $data = array('record' => $proses);
            $this->template->load('administrator/template','administrator/mod_users/view_users_tambah',$data);
        }
    }

    function edit_manajemenuser(){
      $id = $this->uri->segment(3);
      if (isset($_POST['submit'])){
       $config['upload_path'] = 'asset/foto_user/';
       $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG';
            $config['max_size'] = '1000'; // kb
            $this->load->library('upload', $config);
            $this->upload->do_upload('f');
            $hasil=$this->upload->data();
            if ($hasil['file_name']=='' AND $this->input->post('b') ==''){
                $data = array('username'=>$this->db->escape_str($this->input->post('a')),
                    'nama_lengkap'=>$this->db->escape_str($this->input->post('c')),
                    'email'=>$this->db->escape_str($this->input->post('d')),
                    'no_telp'=>$this->db->escape_str($this->input->post('e')),
                    'blokir'=>$this->db->escape_str($this->input->post('h')));
            }elseif ($hasil['file_name']!='' AND $this->input->post('b') ==''){
                $data = array('username'=>$this->db->escape_str($this->input->post('a')),
                    'nama_lengkap'=>$this->db->escape_str($this->input->post('c')),
                    'email'=>$this->db->escape_str($this->input->post('d')),
                    'no_telp'=>$this->db->escape_str($this->input->post('e')),
                    'foto'=>$hasil['file_name'],
                    'blokir'=>$this->db->escape_str($this->input->post('h')));
            }elseif ($hasil['file_name']=='' AND $this->input->post('b') !=''){
                $data = array('username'=>$this->db->escape_str($this->input->post('a')),
                    'password'=>hash("sha512", md5($this->input->post('b'))),
                    'nama_lengkap'=>$this->db->escape_str($this->input->post('c')),
                    'email'=>$this->db->escape_str($this->input->post('d')),
                    'no_telp'=>$this->db->escape_str($this->input->post('e')),
                    'blokir'=>$this->db->escape_str($this->input->post('h')));
            }elseif ($hasil['file_name']!='' AND $this->input->post('b') !=''){
                $data = array('username'=>$this->db->escape_str($this->input->post('a')),
                    'password'=>hash("sha512", md5($this->input->post('b'))),
                    'nama_lengkap'=>$this->db->escape_str($this->input->post('c')),
                    'email'=>$this->db->escape_str($this->input->post('d')),
                    'no_telp'=>$this->db->escape_str($this->input->post('e')),
                    'foto'=>$hasil['file_name'],
                    'blokir'=>$this->db->escape_str($this->input->post('h')));
            }
            $where = array('username' => $this->input->post('id'));
            $this->model_app->update('users', $data, $where);

            $mod=count($this->input->post('modul'));
            $modul=$this->input->post('modul');
            for($i=0;$i<$mod;$i++){
                $datam = array('id_session'=>$this->input->post('ids'),
                  'id_modul'=>$modul[$i]);
                $this->model_app->insert('users_modul',$datam);
            }

            redirect($this->uri->segment(1).'/edit_manajemenuser/'.$this->input->post('id'));
        }else{
            if ($this->session->username==$this->uri->segment(3) OR $this->session->level=='admin'){
                $proses = $this->model_app->edit('users', array('username' => $id))->row_array();
                $akses = $this->model_app->view_join_where('users_modul','modul','id_modul', array('id_session' => $proses['id_session']),'id_umod','DESC');
                $modul = $this->model_app->view_where_ordering('modul', array('publish' => 'Y','status' => 'user'), 'id_modul','DESC');
                $data = array('rows' => $proses, 'record' => $modul, 'akses' => $akses);
                $this->template->load('administrator/template','administrator/mod_users/view_users_edit',$data);
            }else{
                redirect($this->uri->segment(1).'/edit_manajemenuser/'.$this->session->username);
            }
        }
    }

    function delete_manajemenuser(){
        cek_session_akses('manajemenuser',$this->session->id_session);
        $id = array('username' => $this->uri->segment(3));
        $this->model_app->delete('users',$id);
        redirect($this->uri->segment(1).'/manajemenuser');
    }

    function delete_akses(){
        cek_session_admin();
        $id = array('id_umod' => $this->uri->segment(3));
        $this->model_app->delete('users_modul',$id);
        redirect($this->uri->segment(1).'/edit_manajemenuser/'.$this->uri->segment(4));
    }



	// Controller Modul Modul

    function manajemenmodul(){
      cek_session_akses('manajemenmodul',$this->session->id_session);
      if ($this->session->level=='admin'){
        $data['record'] = $this->model_app->view_ordering('modul','id_modul','DESC');
    }else{
        $data['record'] = $this->model_app->view_where_ordering('modul',array('username'=>$this->session->username),'id_modul','DESC');
    }
    $this->template->load('administrator/template','administrator/mod_modul/view_modul',$data);
}

function tambah_manajemenmodul(){
  cek_session_akses('manajemenmodul',$this->session->id_session);
  if (isset($_POST['submit'])){
   $data = array('nama_modul'=>$this->db->escape_str($this->input->post('a')),
    'username'=>$this->session->username,
    'link'=>$this->db->escape_str($this->input->post('b')),
    'static_content'=>'',
    'gambar'=>'',
    'publish'=>$this->db->escape_str($this->input->post('c')),
    'status'=>$this->db->escape_str($this->input->post('e')),
    'aktif'=>$this->db->escape_str($this->input->post('d')),
    'urutan'=>'0',
    'link_seo'=>'');
   $this->model_app->insert('modul',$data);
   redirect($this->uri->segment(1).'/manajemenmodul');
}else{
   $this->template->load('administrator/template','administrator/mod_modul/view_modul_tambah');
}
}

function edit_manajemenmodul(){
  cek_session_akses('manajemenmodul',$this->session->id_session);
  $id = $this->uri->segment(3);
  if (isset($_POST['submit'])){
    $data = array('nama_modul'=>$this->db->escape_str($this->input->post('a')),
        'username'=>$this->session->username,
        'link'=>$this->db->escape_str($this->input->post('b')),
        'static_content'=>'',
        'gambar'=>'',
        'publish'=>$this->db->escape_str($this->input->post('c')),
        'status'=>$this->db->escape_str($this->input->post('e')),
        'aktif'=>$this->db->escape_str($this->input->post('d')),
        'urutan'=>'0',
        'link_seo'=>'');
    $where = array('id_modul' => $this->input->post('id'));
    $this->model_app->update('modul', $data, $where);
    redirect($this->uri->segment(1).'/manajemenmodul');
}else{
    if ($this->session->level=='admin'){
     $proses = $this->model_app->edit('modul', array('id_modul' => $id))->row_array();
 }else{
    $proses = $this->model_app->edit('modul', array('id_modul' => $id, 'username' => $this->session->username))->row_array();
}
$data = array('rows' => $proses);
$this->template->load('administrator/template','administrator/mod_modul/view_modul_edit',$data);
}
}

function delete_manajemenmodul(){
    cek_session_akses('manajemenmodul',$this->session->id_session);
    if ($this->session->level=='admin'){
        $id = array('id_modul' => $this->uri->segment(3));
    }else{
        $id = array('id_modul' => $this->uri->segment(3), 'username'=>$this->session->username);
    }
    $this->model_app->delete('modul',$id);
    redirect($this->uri->segment(1).'/manajemenmodul');
}



    // RESELLER MODUL ==============================================================================================================================================================================

        // Controller Modul Konsumen

function konsumen(){
    cek_session_akses('konsumen',$this->session->id_session);
    $data['record'] = $this->model_app->view_ordering('rb_konsumen','id_konsumen','DESC');
    $this->template->load('administrator/template','administrator/additional/mod_konsumen/view_konsumen',$data);
}

function tambah_konsumen(){
    cek_session_akses('konsumen',$this->session->id_session);
    if (isset($_POST['submit'])){
        $config['upload_path'] = 'asset/foto_user/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = '5000'; // kb
            $this->load->library('upload', $config);
            $this->upload->do_upload('gg');
            $hasil=$this->upload->data()['file_name'];
            $this->func_resize($hasil, 'asset/foto_user/', 200, 200);
            if ($hasil['file_name']==''){
                $data = array('username'=>$this->input->post('aa'),
                    'password'=>hash("sha512", md5($this->input->post('a'))),
                    'nama_lengkap'=>$this->input->post('b'),
                    'email'=>$this->input->post('c'),
                    'jenis_kelamin'=>$this->input->post('d'),
                    'tanggal_lahir'=>$this->input->post('e'),
                    'alamat_lengkap'=>$this->input->post('g'),
                    'no_hp'=>$this->input->post('k'),
                    'kecamatan'=>$this->input->post('ia'),
                    'kota_id'=>$this->input->post('kota'),
                    'tanggal_daftar'=>date('Y-m-d'));
            }else{
                $data = array('username'=>$this->input->post('aa'),
                    'password'=>hash("sha512", md5($this->input->post('a'))),
                    'nama_lengkap'=>$this->input->post('b'),
                    'email'=>$this->input->post('c'),
                    'jenis_kelamin'=>$this->input->post('d'),
                    'tanggal_lahir'=>$this->input->post('e'),
                    'alamat_lengkap'=>$this->input->post('g'),
                    'no_hp'=>$this->input->post('k'),
                    'kecamatan'=>$this->input->post('ia'),
                    'kota_id'=>$this->input->post('kota'),
                    'foto'=>$hasil,
                    'tanggal_daftar'=>date('Y-m-d'));
            }
            $this->model_app->insert('rb_konsumen',$data);
            redirect('administrator/konsumen');
        }else{
            $data['negara'] = $this->model_app->view_ordering('rb_provinsi','provinsi_id','DESC');
            $this->template->load('administrator/template','administrator/additional/mod_konsumen/view_konsumen_tambah',$data);
        }
    }

    function edit_konsumen(){
        cek_session_akses('konsumen',$this->session->id_session);
        $id = $this->uri->segment(3);
        if (isset($_POST['submit'])){
            $config['upload_path'] = 'asset/foto_user/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = '5000'; // kb
            $this->load->library('upload', $config);
            $this->upload->do_upload('gg');
            $hasil=$this->upload->data()['file_name'];
            $this->func_resize($hasil, 'asset/foto_user/', 200, 200);
            if ($hasil['file_name']==''){
                // if (trim($this->input->post('a')) != ''){
                $data = array	('komisi'=>$this->input->post('ka'));
									// 'password'=>hash("sha512", md5($this->input->post('a'))),
                                    // 'nama_lengkap'=>$this->db->escape_str(strip_tags($this->input->post('b'))),
                                    // 'email'=>$this->db->escape_str(strip_tags($this->input->post('c'))),
                                    // 'jenis_kelamin'=>$this->db->escape_str($this->input->post('d')),
                                    // 'tanggal_lahir'=>$this->db->escape_str($this->input->post('e')),
                                    // 'alamat_lengkap'=>$this->db->escape_str(strip_tags($this->input->post('g'))),
                                    // 'kecamatan'=>$this->input->post('ia'),
                                    // 'kota_id'=>$this->input->post('ga'),
                                    // 'no_hp'=>$this->input->post('k'),

                // }else{
                   // $data = array	('nama_lengkap'=>$this->db->escape_str(strip_tags($this->input->post('b'))),
                                    // 'email'=>$this->db->escape_str(strip_tags($this->input->post('c'))),
                                    // 'jenis_kelamin'=>$this->db->escape_str($this->input->post('d')),
                                    // 'tanggal_lahir'=>$this->db->escape_str($this->input->post('e')),
                                    // 'alamat_lengkap'=>$this->db->escape_str(strip_tags($this->input->post('g'))),
                                    // 'kecamatan'=>$this->input->post('ia'),
                                    // 'kota_id'=>$this->input->post('ga'),
                                    // 'no_hp'=>$this->input->post('k'),
									// 'komisi'=>$this->input->post('ka'));
            }
            // }else{
                // if (trim($this->input->post('a')) != ''){
                    // $data = array	('password'=>hash("sha512", md5($this->input->post('a'))),
                                    // 'nama_lengkap'=>$this->db->escape_str(strip_tags($this->input->post('b'))),
                                    // 'email'=>$this->db->escape_str(strip_tags($this->input->post('c'))),
                                    // 'jenis_kelamin'=>$this->db->escape_str($this->input->post('d')),
                                    // 'tanggal_lahir'=>$this->db->escape_str($this->input->post('e')),
                                    // 'alamat_lengkap'=>$this->db->escape_str(strip_tags($this->input->post('g'))),
                                    // 'kecamatan'=>$this->input->post('ia'),
                                    // 'kota_id'=>$this->input->post('ga'),
                                    // 'foto'=>$hasil,
									// 'no_hp'=>$this->input->post('k'),
									// 'komisi'=>$this->input->post('ka'));
                // }else{
                   // $data = array	('nama_lengkap'=>$this->db->escape_str(strip_tags($this->input->post('b'))),
                                    // 'email'=>$this->db->escape_str(strip_tags($this->input->post('c'))),
                                    // 'jenis_kelamin'=>$this->db->escape_str($this->input->post('d')),
                                    // 'tanggal_lahir'=>$this->db->escape_str($this->input->post('e')),
                                    // 'alamat_lengkap'=>$this->db->escape_str(strip_tags($this->input->post('g'))),
                                    // 'kecamatan'=>$this->input->post('ia'),
                                    // 'kota_id'=>$this->input->post('ga'),
                                    // 'foto'=>$hasil,
									// 'no_hp'=>$this->input->post('k'),
                                    // 'komisi'=>$this->input->post('ka'));
                // }
            // }
            $where = array('id_konsumen' => $this->input->post('id'));
            $this->model_app->update('rb_konsumen', $data, $where);
            redirect('administrator/detail_konsumen/'.$this->input->post('id'));
        }else{
            $data['rows'] = $this->model_reseller->profile_konsumen($id)->row_array();
            $row = $this->model_reseller->profile_konsumen($id)->row_array();
            $data['provinsi'] = $this->model_app->view_ordering('rb_provinsi','provinsi_id','ASC');
            $data['kota'] = $this->model_app->view_ordering('rb_kota','kota_id','ASC');
            $data['rowse'] = $this->db->query("SELECT provinsi_id FROM rb_kota where kota_id='$row[kota_id]'")->row_array();
            $data['komisi'] = $this->db->query("SELECT * FROM komisi")->row_array();
            $this->template->load('administrator/template','administrator/additional/mod_konsumen/view_konsumen_edit',$data);
        }
    }

    function detail_konsumen(){
        cek_session_akses('konsumen',$this->session->id_session);
        $id = $this->uri->segment(3);
        $record = $this->model_reseller->orders_report($id,'reseller');
        $edit = $this->model_reseller->profile_konsumen($id)->row_array();
        $data = array('rows' => $edit,'record'=>$record);
        $this->template->load('administrator/template','administrator/additional/mod_konsumen/view_konsumen_detail',$data);
    }

    function delete_konsumen(){
        cek_session_akses('konsumen',$this->session->id_session);
        $id = array('id_konsumen' => $this->uri->segment(3));
        $this->model_app->delete('rb_konsumen',$id);
        redirect('administrator/konsumen');
    }



    // Controller Modul Reseller

    function reseller(){
        cek_session_akses('reseller',$this->session->id_session);
        $data['record'] = $this->model_app->view_ordering('rb_reseller','id_reseller','DESC');
        $this->template->load('administrator/template','administrator/additional/mod_reseller/view_reseller',$data);
    }

    function tambah_reseller()
    {
        cek_session_akses('reseller', $this->session->id_session);
        if (isset($_POST['submit'])) {
            $cek  = $this->model_app->view_where('rb_reseller', array('username' => $this->input->post('a')))->num_rows();
            if ($cek >= 1) {
                $username = $this->input->post('a');
                echo "<script>window.alert('Maaf, Username $username sudah dipakai oleh orang lain!');
                window.location=('index.php?view=login')</script>";
            } else {
                $route = array('administrator', 'agenda', 'auth', 'berita', 'contact', 'download', 'gallery', 'konfirmasi', 'main', 'members', 'page', 'produk', 'reseller', 'testimoni', 'video');
                if (in_array($this->input->post('a'), $route)) {
                    $username = $this->input->post('a');
                    echo "<script>window.alert('Maaf, Username $username sudah dipakai oleh orang lain!');
                    window.location=('" . base_url() . "/" . $this->input->post('i') . "')</script>";
                } else {
                    
                    $data = array(
                        'username' => $this->input->post('a'),
                        'password' => hash("sha512", md5($this->input->post('b'))),
                        'nama_reseller' => $this->input->post('d'),
                        'no_pks' => $this->input->post('no_pks'),
                        'pic' => $this->input->post('e'),
                        'jenis_kelamin' => $this->input->post('f'),
                        'no_telpon' => $this->input->post('g'),
                        'email' => $this->input->post('h'),
                        'alamat_lengkap' => $this->input->post('i'),
                        'kota_id' => $this->input->post('kota'),
                        'kode_pos' => $this->input->post('j'),
                        'keterangan' => $this->input->post('k'),
                        'komisi' => $this->input->post('kp'),
                        'komisi_nominal' => $this->input->post('kn'),
                        'komisi_topsonia' => $this->input->post('komisi_topsonia'),
                        'diskon_buyer' => $this->input->post('diskon_buyer'),
                        'include_PPN' => $this->input->post('pajak'),
                        'referral' => $this->input->post('kode_referal'),
                        'tanggal_daftar' => date('Y-m-d')
                    );
                    $config['upload_path'] = 'asset/foto_user/';
                    $config['allowed_types'] = 'gif|jpg|png|jpeg';
                    $config['max_size'] = '5000'; // kb
                    $this->load->library('upload', $config);
                    $this->upload->do_upload('m');
                    $hasil = $this->upload->data()['file_name'];

                    //$this->func_resize($hasil, 'asset/foto_user/', 200, 200);

                    if ($hasil['file_name'] != '') {
                        $data['foto'] = $hasil;
                    }

                    $config2['upload_path'] = 'asset/data_pks/';
                    $config2['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
                    $config2['max_size'] = '1000'; // kb
                    // $this->load->library('upload', $config2);
                    $this->upload->initialize($config2);
                    $this->upload->do_upload('upload_pks');
                    $hasil2 = $this->upload->data()['file_name'];

                    if ($hasil2['file_name'] != '') {
                        $data['file_pks'] = $hasil2;
                    }

                    $this->model_app->insert('rb_reseller', $data);

                    $id_reseller = $this->db->insert_id();

                    $data_reseller = array(
                        'id_reseller' => $id_reseller,
                        'pic' => $this->input->post('e'),
                        'alamat_lengkap' => $this->input->post('i'),
                        'kecamatan' => '1',
                        'kota_id' => $this->input->post('kota'),
                        'provinsi_id' => $this->input->post('state'),
                        'kode_pos' => $this->input->post('j'),
                        'no_hp' => $this->input->post('g'),
                        'alamat_default' => '1'
                    );

                    $this->model_app->insert('alamat_reseller', $data_reseller);

                    redirect('administrator/reseller');
                }
            }
        } else {
            $data['provinsi'] = $this->model_app->view_ordering('rb_provinsi', 'provinsi_id', 'ASC');
            $this->template->load('administrator/template', 'administrator/additional/mod_reseller/view_reseller_tambah', $data);
        }
    }

    //update upload_pks
    function edit_reseller()
    {
        cek_session_akses('reseller', $this->session->id_session);
        $id = $this->uri->segment(3);
        if (isset($_POST['submit'])) {

            $data = array(
                'nama_reseller' => $this->input->post('d'),
                'pic' => $this->input->post('e'),
                'jenis_kelamin' => $this->input->post('f'),
                'no_telpon' => $this->input->post('g'),
                'email' => $this->input->post('h'),
                'alamat_lengkap' => $this->input->post('i'),
                'kota_id' => $this->input->post('kota'),
                'kode_pos' => $this->input->post('j'),
                'keterangan' => $this->input->post('k'),
                'komisi' => $this->input->post('kp'),
                'komisi_nominal' => $this->input->post('kn'),
                'komisi_topsonia' => $this->input->post('komisi_topsonia'),
                'diskon_buyer' => $this->input->post('diskon_buyer'),
                'include_PPN' => $this->input->post('pajak'),
                'referral' => $this->input->post('l')
            );

            //$this->func_resize($hasil, 'asset/foto_user/', 200, 200);
            if (trim($this->input->post('b')) != '') {
                $data['password'] = hash("sha512", md5($this->input->post('b')));
            } 

            if(strlen($_FILES['m']['name']) > 0){
                $config['upload_path'] = 'asset/foto_user/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = '5000'; // kb
                $this->load->library('upload', $config);
                $this->upload->do_upload('m');
                $hasil = $this->upload->data()['file_name'];
                $data['foto'] = $hasil;

                if(trim($this->input->post('val_foto'))!=''){
                    unlink("asset/foto_user/".$this->input->post('val_foto')); 
                }
               
                 
            }
            
            if ( strlen($_FILES['upload_pks']['name']) > 0){
                $config2['upload_path'] = 'asset/data_pks/';
                $config2['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
                $config2['max_size'] = '1000'; // kb
                $this->load->library('upload', $config2);
                $this->upload->initialize($config2);
                $this->upload->do_upload('upload_pks');
                $hasil2 = $this->upload->data()['file_name'];
                $data['file_pks'] = $hasil2;

                if(trim($this->input->post('val_file_pks'))!=''){
                    unlink("asset/data_pks/".$this->input->post('val_file_pks')); 
                }
                
            }
                
            $where = array('id_reseller' => $this->input->post('id'));
            $this->model_app->update('rb_reseller', $data, $where);
            redirect('administrator/reseller');

        } else {
            $edit = $this->model_app->edit('rb_reseller', array('id_reseller' => $id))->row_array();
            $prov = $this->model_app->view_ordering('rb_provinsi', 'provinsi_id', 'ASC');

            $data = array('rows' => $edit, 'provinsi' => $prov);

            $this->template->load('administrator/template', 'administrator/additional/mod_reseller/view_reseller_edit', $data);
        }
    }

function detail_reseller(){
    cek_session_akses('reseller',$this->session->id_session);
    $id = $this->uri->segment(3);
    $record = $this->model_reseller->reseller_pembelian($id,'admin');
    $penjualan = $this->db->query("SELECT a.kode_transaksi AS kt,(sum(IFNULL(c.harga_jual,0)*IFNULL(c.jumlah,0)))+(IFNULL(ongkir,0)-IFNULL(diskon_ongkir,0))+(IFNULL(asuransi,0)+IFNULL(komisi_topsonia,0)+IFNULL(pajak,0))-IFNULL(a.diskon,0) AS total_belanja,b.nama_lengkap AS nama_lengkap,a.waktu_order AS waktu_order,a.proses AS proses,a.id_penjualan AS idp FROM `rb_penjualan` a JOIN rb_konsumen b ON a.id_pembeli=b.id_konsumen JOIN rb_penjualan_detail c ON a.id_penjualan=c.id_penjualan LEFT JOIN rb_konfirmasi_pembayaran_konsumen d ON a.kode_transaksi=d.kode_transaksi WHERE a.status_penjual='reseller' AND a.id_penjual='$id' AND a.`order`='1' AND a.proses='1' GROUP BY a.id_penjualan ORDER BY a.id_penjualan DESC");
        //$penjualan = $this->model_reseller->penjualan_list_konsumen($id,'reseller');
    $edit = $this->model_app->edit('rb_reseller',array('id_reseller'=>$id))->row_array();
    $reward = $this->model_app->view_ordering('rb_reward','id_reward','ASC');

    $data = array('rows' => $edit,'record'=>$record,'penjualan'=>$penjualan,'reward'=>$reward);
    $this->template->load('administrator/template','administrator/additional/mod_reseller/view_reseller_detail',$data);
}

function delete_reseller(){
    cek_session_akses('reseller',$this->session->id_session);
    $id = array('id_reseller' => $this->uri->segment(3));
    $this->model_app->delete('rb_reseller',$id);
    $this->model_app->delete('rb_produk',$id);
    redirect('administrator/reseller');
}


    // Controller Modul Supplier

function supplier(){
    cek_session_akses('supplier',$this->session->id_session);
    $data['record'] = $this->model_app->view_ordering('rb_supplier','id_supplier','DESC');
    $this->template->load('administrator/template','administrator/additional/mod_supplier/view_supplier',$data);
}

function tambah_supplier(){
    cek_session_akses('supplier',$this->session->id_session);
    if (isset($_POST['submit'])){
        $data = array('nama_supplier'=>$this->input->post('a'),
            'kontak_person'=>$this->input->post('b'),
            'alamat_lengkap'=>$this->input->post('c'),
            'no_hp'=>$this->input->post('d'),
            'alamat_email'=>$this->input->post('e'),
            'kode_pos'=>$this->input->post('f'),
            'no_telpon'=>$this->input->post('g'),
            'fax'=>$this->input->post('h'),
            'keterangan'=>$this->input->post('i'));
        $this->model_app->insert('rb_supplier',$data);
        redirect('administrator/supplier');
    }else{
        $this->template->load('administrator/template','administrator/additional/mod_supplier/view_supplier_tambah',$data);
    }
}

function edit_supplier(){
    cek_session_akses('supplier',$this->session->id_session);
    $id = $this->uri->segment(3);
    if (isset($_POST['submit'])){
       $data = array('nama_supplier'=>$this->input->post('a'),
        'kontak_person'=>$this->input->post('b'),
        'alamat_lengkap'=>$this->input->post('c'),
        'no_hp'=>$this->input->post('d'),
        'alamat_email'=>$this->input->post('e'),
        'kode_pos'=>$this->input->post('f'),
        'no_telpon'=>$this->input->post('g'),
        'fax'=>$this->input->post('h'),
        'keterangan'=>$this->input->post('i'));
       $where = array('id_supplier' => $this->input->post('id'));
       $this->model_app->update('rb_supplier', $data, $where);
       redirect('administrator/detail_supplier/'.$this->input->post('id'));
   }else{
    $data['rows'] = $this->model_app->view_where('rb_supplier',array('id_supplier'=>$id))->row_array();
    $this->template->load('administrator/template','administrator/additional/mod_supplier/view_supplier_edit',$data);
}
}

function detail_supplier(){
    cek_session_akses('supplier',$this->session->id_session);
    $id = $this->uri->segment(3);
    $edit = $this->model_app->view_where('rb_supplier',array('id_supplier'=>$id))->row_array();
    $data = array('rows' => $edit);
    $this->template->load('administrator/template','administrator/additional/mod_supplier/view_supplier_detail',$data);
}

function delete_supplier(){
    cek_session_akses('supplier',$this->session->id_session);
    $id = array('id_supplier' => $this->uri->segment(3));
    $this->model_app->delete('rb_supplier',$id);
    redirect('administrator/supplier');
}


    // Controller Modul Kategori Produk

function kategori_produk(){
    cek_session_akses('kategori_produk',$this->session->id_session);
    $data['record'] = $this->model_app->view_ordering('rb_kategori_produk','id_kategori_produk','DESC');
    $this->template->load('administrator/template','administrator/additional/mod_kategori_produk/view_kategori_produk',$data);
}

  function tambah_kategori_produk()
    {
        cek_session_akses('kategori_produk', $this->session->id_session);
        if (isset($_POST['submit'])) {

            $data = array(
                'nama_kategori' => $this->input->post('a'),
                'kategori_seo'  => seo_title($this->input->post('a')),
            );


            if (strlen($_FILES['foto_kategori']['name']) > 0) {
                $config['upload_path'] = 'asset/foto_kp/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = '1000'; // kb
                $config['file_name'] = seo_title($this->input->post('a'));
                $this->load->library('upload', $config);
                $this->upload->do_upload('foto_kategori');
                $hasil = $this->upload->data()['file_name'];

                $data['foto'] = $hasil;
            }

            $this->model_app->insert('rb_kategori_produk', $data);

            redirect('administrator/kategori_produk');
        } else {
            $this->template->load('administrator/template', 'administrator/additional/mod_kategori_produk/view_kategori_produk_tambah');
        }
    }

    
    function edit_kategori_produk()
    {
        cek_session_akses('kategori_produk', $this->session->id_session);
        $id = $this->uri->segment(3);
        if (isset($_POST['submit'])) {
            $data = array('nama_kategori' => $this->input->post('a'), 'kategori_seo' => seo_title($this->input->post('a')));
            $where = array('id_kategori_produk' => $this->input->post('id'));
            
            if (strlen($_FILES['foto_kategori']['name']) > 0) {
                $config['upload_path'] = 'asset/foto_kp/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = '1000'; // kb
                $config['file_name'] = seo_title($this->input->post('a'));
                $this->load->library('upload', $config);
                $this->upload->do_upload('foto_kategori');
                $hasil = $this->upload->data()['file_name'];

                $data['foto'] = $hasil;

                if (trim($this->input->post('foto_lama')) != '') {
                    unlink("asset/foto_kp/" . $this->input->post('foto_lama'));
                }
            }

            $this->model_app->update('rb_kategori_produk', $data, $where);
            redirect('administrator/kategori_produk');
        } else {
            $edit = $this->model_app->edit('rb_kategori_produk', array('id_kategori_produk' => $id))->row_array();
            $data = array('rows' => $edit);
            $this->template->load('administrator/template', 'administrator/additional/mod_kategori_produk/view_kategori_produk_edit', $data);
        }
    }

    
    function delete_kategori_produk()
    {
        cek_session_akses('kategori_produk', $this->session->id_session);
        $id = array('id_kategori_produk' => $this->uri->segment(3));
        $this->model_app->delete('rb_kategori_produk', $id);
        if (trim($this->uri->segment(4)) != '') {
            unlink("asset/foto_kp/" . $this->uri->segment(4));
        }
        redirect('administrator/kategori_produk');
    }


    // Controller Modul Sub Kategori Produk

function kategori_produk_sub(){
    cek_session_akses('kategori_produk_sub',$this->session->id_session);
    $data['record'] = $this->db->query("SELECT * FROM rb_kategori_produk_sub a JOIN rb_kategori_produk b ON a.id_kategori_produk=b.id_kategori_produk ORDER BY a.id_kategori_produk_sub DESC");
    $this->template->load('administrator/template','administrator/additional/mod_kategori_produk/view_kategori_produk_sub',$data);
}

function tambah_kategori_produk_sub(){
    cek_session_akses('kategori_produk_sub',$this->session->id_session);
    if (isset($_POST['submit'])){
        $data = array('id_kategori_produk'=>$this->input->post('b'),
          'nama_kategori_sub'=>$this->input->post('a'),
          'kategori_seo_sub'=>seo_title($this->input->post('a')));
        $this->model_app->insert('rb_kategori_produk_sub',$data);
        redirect('administrator/kategori_produk_sub');
    }else{
        $this->template->load('administrator/template','administrator/additional/mod_kategori_produk/view_kategori_produk_tambah_sub');
    }
}

function edit_kategori_produk_sub(){
    cek_session_akses('kategori_produk_sub',$this->session->id_session);
    $id = $this->uri->segment(3);
    if (isset($_POST['submit'])){
        $data = array('id_kategori_produk'=>$this->input->post('b'),
          'nama_kategori_sub'=>$this->input->post('a'),
          'kategori_seo_sub'=>seo_title($this->input->post('a')));
        $where = array('id_kategori_produk_sub' => $this->input->post('id'));
        $this->model_app->update('rb_kategori_produk_sub', $data, $where);
        redirect('administrator/kategori_produk_sub');
    }else{
        $edit = $this->model_app->edit('rb_kategori_produk_sub',array('id_kategori_produk_sub'=>$id))->row_array();
        $data = array('rows' => $edit);
        $this->template->load('administrator/template','administrator/additional/mod_kategori_produk/view_kategori_produk_edit_sub',$data);
    }
}

function delete_kategori_produk_sub(){
    cek_session_akses('kategori_produk_sub',$this->session->id_session);
    $id = array('id_kategori_produk_sub' => $this->uri->segment(3));
    $this->model_app->delete('rb_kategori_produk_sub',$id);
    redirect('administrator/kategori_produk_sub');
}


    // Controller Modul Produk

function produk(){
    cek_session_akses('produk',$this->session->id_session);
    $data['record'] = $this->model_app->view_where_ordering('rb_produk',array('id_reseller'=>$this->uri->segment(3)),'id_produk','ASC');
    $data['res'] = $this->model_app->view_where('rb_reseller',array('id_reseller'=>$this->uri->segment(3)))->row_array();
    $this->template->load('administrator/template','administrator/additional/mod_produk/view_produk',$data);
}

function produk_by_merchant(){
    cek_session_akses('produk',$this->session->id_session);
    $data['record'] = $this->model_app->view_ordering('rb_reseller','id_reseller','DESC');
    $this->template->load('administrator/template','administrator/additional/mod_produk/view_produk_by_merchant',$data);
}

function tambah_produk(){
    cek_session_akses('produk',$this->session->id_session);
    $id_reseller = $this->input->post('id_reseller');
    if (isset($_POST['submit'])){
       $data = array('id_kategori_produk'=>$this->input->post('a'),
          'id_kategori_produk_sub'=>$this->input->post('aa'),
          'id_reseller'=>$id_reseller,
          'nama_produk'=>$this->input->post('b'),
          'produk_seo' => seo_title($this->input->post('b'))."-".$id_reseller."-".substr(md5(uniqid(mt_rand(), true)) , 0, 6),
          'satuan'=>$this->input->post('c'),
                              // 'harga_beli'=>$this->input->post('d'),
                              // 'harga_reseller'=>$this->input->post('e'),
          'harga_konsumen'=>$this->input->post('f'),
          'berat'=>$this->input->post('berat'),
          'keterangan'=>$this->input->post('ff'),
          'username'=>$this->session->username,
          'waktu_input'=>date('Y-m-d H:i:s'));

       $this->model_app->insert('rb_produk',$data);
       $id_produk = $this->db->insert_id();

       $files = $_FILES;
       $cpt = count($_FILES['userfile']['name']);
       for($i=0; $i<$cpt; $i++){
        if ($files['userfile']['name'][$i] != '') {
         $no=$i+1;
         $_FILES['userfile']['name'] 	= $files['userfile']['name'][$i];
         $_FILES['userfile']['type'] 	= $files['userfile']['type'][$i];
         $_FILES['userfile']['tmp_name']	= $files['userfile']['tmp_name'][$i];
         $_FILES['userfile']['error'] 	= $files['userfile']['error'][$i];
         $_FILES['userfile']['size'] 	= $files['userfile']['size'][$i];
         $this->load->library('upload');
         $savename = $id_produk.'_'.date('ymdHis').'_'.$i.'.jpg';
         $this->upload->initialize($this->set_upload_options($savename));
         $this->upload->do_upload();
         $fileName = $this->upload->data('file_name');
         $this->func_resize($fileName, 'asset/foto_produk/', 500, 500);
         $images[] = $fileName;
     }
 }

 $fileName = implode(';',$images);
 $fileName = str_replace(' ','_',$fileName);

 $data1 = array('gambar' => $fileName);
 $where = array('id_produk' => $id_produk,'id_reseller'=>$id_reseller);
 $this->model_app->update('rb_produk', $data1, $where);

 if ($this->input->post('diskon') > 0){
    $cek = $this->db->query("SELECT * FROM rb_produk_diskon where id_produk='".$id_produk."' AND id_reseller='".$id_reseller."'");
    if ($cek->num_rows()>=1){
        $data = array('diskon'=>$this->input->post('diskon'));
        $where = array('id_produk' => $id_produk,'id_reseller' => $id_reseller);
        $this->model_app->update('rb_produk_diskon', $data, $where);
    }else{
        $data = array('id_produk'=>$id_produk,
            'id_reseller'=>$id_reseller,
            'diskon'=>$this->input->post('diskon'));
        $this->model_app->insert('rb_produk_diskon',$data);
    }
}

if ($this->input->post('stok') != ''){
    $kode_transaksi = "TRX-".date('YmdHis');
    $data = array('kode_transaksi'=>$kode_transaksi,
        'id_pembeli'=>$id_reseller,
        'id_penjual'=>'1',
        'status_pembeli'=>'reseller',
        'status_penjual'=>'admin',
        'service'=>'Stok Otomatis (Pribadi)',
        'waktu_transaksi'=>date('Y-m-d H:i:s'),
        'proses'=>'1');
    $this->model_app->insert('rb_penjualan',$data);
    $idp = $this->db->insert_id();

    $data = array('id_penjualan'=>$idp,
        'id_produk'=>$id_produk,
        'jumlah'=>$this->input->post('stok'),
        'harga_jual'=>$this->input->post('f'),
        'satuan'=>$this->input->post('c'));
    $this->model_app->insert('rb_penjualan_detail',$data);
}
redirect('administrator/produk/'.$id_reseller);
}else{ 
    $data['rows'] = $this->model_app->edit('rb_produk',array('id_produk'=>$id_reseller))->row_array();
    $data['record'] = $this->model_app->view_ordering('rb_kategori_produk','id_kategori_produk','DESC');
    $data['res'] = $this->model_app->view_where('rb_reseller',array('id_reseller'=>$id_reseller))->row_array();
    $this->template->load('administrator/template','administrator/additional/mod_produk/view_produk_tambah',$data);
}
}

function edit_produk(){
    cek_session_akses('produk',$this->session->id_session);
    $id = $this->uri->segment(3);
    $idr = $this->uri->segment(4);
    $idre = $this->input->post('id_reseller');
    if (isset($_POST['submit'])){
        $files = $_FILES;
        $cpt = count($_FILES['userfile']['name']);
        for($i=0; $i<$cpt; $i++){
            if($files['userfile']['name'][$i]!=''){
                $_FILES['userfile']['name']= $files['userfile']['name'][$i];
                $_FILES['userfile']['type']= $files['userfile']['type'][$i];
                $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
                $_FILES['userfile']['error']= $files['userfile']['error'][$i];
                $_FILES['userfile']['size']= $files['userfile']['size'][$i];
                $this->load->library('upload');
                $savename = $id_produk.'_'.date('ymdHis').'_'.$i.'.jpg';
                $this->upload->initialize($this->set_upload_options($savename));
                $this->upload->do_upload();
                $fileName = $this->upload->data()['file_name'];
                $this->func_resize($fileName, 'asset/foto_produk/', 500, 500);
                $images[] = $fileName;
                unlink("./asset/foto_produk/".$this->input->post('file_lama['.$i.']'));
            }else if($this->input->post('file_lama['.$i.']')!=null){
                $images[] = $this->input->post('file_lama['.$i.']');
            }else{

            }
        }
        $fileName = implode(';',$images);
        $fileName = str_replace(' ','_',$fileName);
        if (trim($fileName)!=''){
            $data = array('id_kategori_produk'=>$this->input->post('a'),
              'id_kategori_produk_sub'=>$this->input->post('aa'),
              'nama_produk'=>$this->input->post('b'),
              'produk_seo' => seo_title($this->input->post('b'))."-". $idre."-".substr(md5(uniqid(mt_rand(), true)) , 0, 6),
              'satuan'=>$this->input->post('c'),
                              // 'harga_beli'=>$this->input->post('d'),
                              // 'harga_reseller'=>$this->input->post('e'),
              'harga_konsumen'=>$this->input->post('f'),
              'berat'=>$this->input->post('berat'),
              'gambar'=>$fileName,
              'keterangan'=>$this->input->post('ff'),
              'username'=>$this->session->username);
        }else{
            $data = array('id_kategori_produk'=>$this->input->post('a'),
              'id_kategori_produk_sub'=>$this->input->post('aa'),
              'nama_produk'=>$this->input->post('b'),
              'produk_seo' => seo_title($this->input->post('b'))."-". $idre."-".substr(md5(uniqid(mt_rand(), true)) , 0, 6),
              'satuan'=>$this->input->post('c'),
                              // 'harga_beli'=>$this->input->post('d'),
                              // 'harga_reseller'=>$this->input->post('e'),
              'harga_konsumen'=>$this->input->post('f'),
              'berat'=>$this->input->post('berat'),
              'keterangan'=>$this->input->post('ff'),
              'username'=>$this->session->username);
        }

        $where = array('id_produk' => $this->input->post('id'));
        $this->model_app->update('rb_produk', $data, $where);

        if ($this->input->post('stok') != ''){
            $kode_transaksi = "PT-".date('YmdHis');
            $data = array('kode_transaksi'=>$kode_transaksi,
                'id_pembeli'=>$idre,
                'id_penjual'=>'1',
                'status_pembeli'=>'reseller',
                'status_penjual'=>'admin',
                'service'=>'Stok Otomatis (Admin)',
                'waktu_transaksi'=>date('Y-m-d H:i:s'),
                'proses'=>'1');
            $this->model_app->insert('rb_penjualan',$data);
            $idp = $this->db->insert_id();

            $data = array('id_penjualan'=>$idp,
                'id_produk'=>$this->input->post('id'),
                'jumlah'=>$this->input->post('stok'),
                'harga_jual'=>$this->input->post('f'),
                'satuan'=>$this->input->post('c'));
            $this->model_app->insert('rb_penjualan_detail',$data);
        }

        redirect('administrator/produk/'.$idre);
    }else{
        $data['record'] = $this->model_app->view_ordering('rb_kategori_produk','id_kategori_produk','DESC');
        $data['rows'] = $this->model_app->edit('rb_produk',array('id_produk'=>$id))->row_array();
        $this->template->load('administrator/template','administrator/additional/mod_produk/view_produk_edit',$data);
    }
}

private function set_upload_options($new_name){
  $config = array();
  $config['upload_path'] = './asset/foto_produk/';
  $config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size'] = '5000'; // kb
		$config['file_name'] = $new_name;
		$config['encrypt_name'] = FALSE;
		$this->load->library('upload', $config);
		return $config;
	}

    function delete_produk(){
        cek_session_akses('produk',$this->session->id_session);
        $id = array('id_produk' => $this->uri->segment(3));
        $this->model_app->delete('rb_produk',$id);
        redirect('administrator/produk/'.$this->uri->segment(4));
    }


    // Controller Modul Rekening

    function rekening(){
        cek_session_akses('rekening',$this->session->id_session);
        $data['record'] = $this->model_app->view_ordering('rb_rekening','id_rekening','DESC');
        $this->template->load('administrator/template','administrator/additional/mod_rekening/view_rekening',$data);
    }

    function tambah_rekening(){
        cek_session_akses('rekening',$this->session->id_session);
        if (isset($_POST['submit'])){
            // $this->model_rekening->rekening_tambah();
            $data = array('nama_bank'=>$this->db->escape_str($this->input->post('a')),
                'no_rekening'=>$this->db->escape_str($this->input->post('b')),
                'pemilik_rekening'=>$this->db->escape_str($this->input->post('c')));
            $this->model_app->insert('rb_rekening',$data);
            redirect('administrator/rekening');
        }else{
            $this->template->load('administrator/template','administrator/additional/mod_rekening/view_rekening_tambah');
        }
    }

    function edit_rekening(){
        cek_session_akses('rekening',$this->session->id_session);
        $id = $this->uri->segment(3);
        if (isset($_POST['submit'])){
            $data = array('nama_bank'=>$this->db->escape_str($this->input->post('a')),
                'no_rekening'=>$this->db->escape_str($this->input->post('b')),
                'pemilik_rekening'=>$this->db->escape_str($this->input->post('c')));
            $where = array('id_rekening' => $this->input->post('id'));
            $this->model_app->update('rb_rekening', $data, $where);
            redirect('administrator/rekening');
        }else{
            $data['rows'] = $this->model_app->edit('rb_rekening',array('id_rekening'=>$id))->row_array();
            $this->template->load('administrator/template','administrator/additional/mod_rekening/view_rekening_edit',$data);
        }
    }

    function delete_rekening(){
        cek_session_akses('rekening',$this->session->id_session);
        $id = array('id_rekening' => $this->uri->segment(3));
        $this->model_app->delete('rb_rekening',$id);
        redirect('administrator/rekening');
    }


    // Controller Modul Setting Bonus

    function settingbonus(){
        cek_session_akses('settingbonus',$this->session->id_session);
        if (isset($_POST['submit'])){
            $data = array('referral'=>$this->input->post('aa'),
                'tanggal_pencairan'=>$this->input->post('bb'));
            $where = array('id_setting' => $this->input->post('id'));
            $this->model_app->update('rb_setting', $data, $where);
            redirect('administrator/settingbonus');
        }elseif (isset($_POST['submit2'])){
            if ($this->input->post('idr')==''){
                $data = array('posisi'=>$this->input->post('a'),
                  'reward'=>$this->input->post('b'));
                $this->model_app->insert('rb_reward',$data);
            }else{
                $data = array('posisi'=>$this->input->post('a'),
                  'reward'=>$this->input->post('b'));
                $where = array('id_reward' => $this->input->post('idr'));
                $this->model_app->update('rb_reward', $data, $where);
            }
            redirect('administrator/settingbonus');
        }else{
            $data['record'] = $this->model_app->edit('rb_setting',array('aktif'=>'Y'))->row_array();
            $data['reward'] = $this->model_app->view_ordering('rb_reward','id_reward','ASC');
            if ($this->uri->segment(3)!=''){
                $data['rows'] = $this->model_app->edit('rb_reward',array('id_reward'=>$this->uri->segment(3)))->row_array();
            }
            $this->template->load('administrator/template','administrator/additional/mod_settingbonus/view_settingbonus',$data);
        }
    }

    function delete_reward(){
        cek_session_akses('settingbonus',$this->session->id_session);
        $id = array('id_reward' => $this->uri->segment(3));
        $this->model_app->delete('rb_reward',$id);
        redirect('administrator/settingbonus');
    }



    // Controller Modul Pembelian

    function pembelian(){
        cek_session_akses('pembelian',$this->session->id_session);
        $this->session->unset_userdata('idp');
        $data['record'] = $this->model_app->view_join_one('rb_pembelian','rb_supplier','id_supplier','id_pembelian','DESC');
        $this->template->load('administrator/template','administrator/additional/mod_pembelian/view_pembelian',$data);
    }

    function detail_pembelian(){
        cek_session_akses('pembelian',$this->session->id_session);
        $data['rows'] = $this->model_reseller->view_join_rows('rb_pembelian','rb_supplier','id_supplier',array('id_pembelian'=>$this->uri->segment(3)),'id_pembelian','DESC')->row_array();
        $data['record'] = $this->model_app->view_join_where('rb_pembelian_detail','rb_produk','id_produk',array('id_pembelian'=>$this->uri->segment(3)),'id_pembelian_detail','DESC');
        $this->template->load('administrator/template','administrator/additional/mod_pembelian/view_pembelian_detail',$data);
    }

    function tambah_pembelian(){
        cek_session_akses('pembelian',$this->session->id_session);
        if (isset($_POST['submit1'])){
            if ($this->session->idp == ''){
                $data = array('kode_pembelian'=>$this->input->post('a'),
                  'id_supplier'=>$this->input->post('b'),
                  'waktu_beli'=>date('Y-m-d H:i:s'));
                $this->model_app->insert('rb_pembelian',$data);
                $idp = $this->db->insert_id();
                $this->session->set_userdata(array('idp'=>$idp));
            }else{
                $data = array('kode_pembelian'=>$this->input->post('a'),
                  'id_supplier'=>$this->input->post('b'));
                $where = array('id_pembelian' => $this->session->idp);
                $this->model_app->update('rb_pembelian', $data, $where);
            }
            redirect('administrator/tambah_pembelian');

        }elseif(isset($_POST['submit'])){
            if ($this->input->post('idpd')==''){
                $data = array('id_pembelian'=>$this->session->idp,
                  'id_produk'=>$this->input->post('aa'),
                  'harga_pesan'=>$this->input->post('bb'),
                  'jumlah_pesan'=>$this->input->post('cc'),
                  'satuan'=>$this->input->post('dd'));
                $this->model_app->insert('rb_pembelian_detail',$data);
            }else{
                $data = array('id_produk'=>$this->input->post('aa'),
                  'harga_pesan'=>$this->input->post('bb'),
                  'jumlah_pesan'=>$this->input->post('cc'),
                  'satuan'=>$this->input->post('dd'));
                $where = array('id_pembelian_detail' => $this->input->post('idpd'));
                $this->model_app->update('rb_pembelian_detail', $data, $where);
            }
            redirect('administrator/tambah_pembelian');
        }else{
            $data['rows'] = $this->model_reseller->view_join_rows('rb_pembelian','rb_supplier','id_supplier',array('id_pembelian'=>$this->session->idp),'id_pembelian','DESC')->row_array();
            $data['record'] = $this->model_app->view_join_where('rb_pembelian_detail','rb_produk','id_produk',array('id_pembelian'=>$this->session->idp),'id_pembelian_detail','DESC');
            $data['barang'] = $this->model_app->view_where_ordering('rb_produk',array('id_reseller'=>'0'),'id_produk','ASC');
            $data['supplier'] = $this->model_app->view_ordering('rb_supplier','id_supplier','ASC');
            if ($this->uri->segment(3)!=''){
                $data['row'] = $this->model_app->view_where('rb_pembelian_detail',array('id_pembelian_detail'=>$this->uri->segment(3)))->row_array();
            }
            $this->template->load('administrator/template','administrator/additional/mod_pembelian/view_pembelian_tambah',$data);
        }
    }

    function edit_pembelian(){
        cek_session_akses('pembelian',$this->session->id_session);
        if (isset($_POST['submit1'])){
            $data = array('kode_pembelian'=>$this->input->post('a'),
              'id_supplier'=>$this->input->post('b'),
              'waktu_beli'=>$this->input->post('c'));
            $where = array('id_pembelian' => $this->input->post('idp'));
            $this->model_app->update('rb_pembelian', $data, $where);
            redirect('administrator/edit_pembelian/'.$this->input->post('idp'));

        }elseif(isset($_POST['submit'])){
            if ($this->input->post('idpd')==''){
                $data = array('id_pembelian'=>$this->input->post('idp'),
                  'id_produk'=>$this->input->post('aa'),
                  'harga_pesan'=>$this->input->post('bb'),
                  'jumlah_pesan'=>$this->input->post('cc'),
                  'satuan'=>$this->input->post('dd'));
                $this->model_app->insert('rb_pembelian_detail',$data);
            }else{
                $data = array('id_produk'=>$this->input->post('aa'),
                  'harga_pesan'=>$this->input->post('bb'),
                  'jumlah_pesan'=>$this->input->post('cc'),
                  'satuan'=>$this->input->post('dd'));
                $where = array('id_pembelian_detail' => $this->input->post('idpd'));
                $this->model_app->update('rb_pembelian_detail', $data, $where);
            }
            redirect('administrator/edit_pembelian/'.$this->input->post('idp'));
        }else{
            $data['rows'] = $this->model_reseller->view_join_rows('rb_pembelian','rb_supplier','id_supplier',array('id_pembelian'=>$this->uri->segment(3)),'id_pembelian','DESC')->row_array();
            $data['record'] = $this->model_app->view_join_where('rb_pembelian_detail','rb_produk','id_produk',array('id_pembelian'=>$this->uri->segment(3)),'id_pembelian_detail','DESC');
            $data['barang'] = $this->model_app->view_where_ordering('rb_produk',array('id_reseller'=>'0'),'id_produk','ASC');
            $data['supplier'] = $this->model_app->view_ordering('rb_supplier','id_supplier','ASC');
            if ($this->uri->segment(4)!=''){
                $data['row'] = $this->model_app->view_where('rb_pembelian_detail',array('id_pembelian_detail'=>$this->uri->segment(4)))->row_array();
            }
            $this->template->load('administrator/template','administrator/additional/mod_pembelian/view_pembelian_edit',$data);
        }
    }

    function delete_pembelian(){
        cek_session_akses('pembelian',$this->session->id_session);
        $id = array('id_pembelian' => $this->uri->segment(3));
        $this->model_app->delete('rb_pembelian',$id);
        $this->model_app->delete('rb_pembelian_detail',$id);
        redirect('administrator/pembelian');
    }

    function delete_pembelian_detail(){
        cek_session_akses('pembelian',$this->session->id_session);
        $id = array('id_pembelian_detail' => $this->uri->segment(4));
        $this->model_app->delete('rb_pembelian_detail',$id);
        redirect('administrator/edit_pembelian/'.$this->uri->segment(3));
    }

    function delete_pembelian_tambah_detail(){
        cek_session_akses('pembelian',$this->session->id_session);
        $id = array('id_pembelian_detail' => $this->uri->segment(3));
        $this->model_app->delete('rb_pembelian_detail',$id);
        redirect('administrator/tambah_pembelian');
    }



    // Controller Modul Penjualan

    function penjualan(){
        cek_session_akses('penjualan',$this->session->id_session);
        $this->session->unset_userdata('idp');
        $data['record'] = $this->model_reseller->penjualan_list(1,'admin');
        $this->template->load('administrator/template','administrator/additional/mod_penjualan/view_penjualan',$data);
    }

    function detail_penjualan(){
        cek_session_akses('penjualan',$this->session->id_session);
        $data['rows'] = $this->model_reseller->penjualan_detail($this->uri->segment(3))->row_array();
        $data['record'] = $this->model_app->view_join_where('rb_penjualan_detail','rb_produk','id_produk',array('id_penjualan'=>$this->uri->segment(3)),'id_penjualan_detail','DESC');
        $this->template->load('administrator/template','administrator/additional/mod_penjualan/view_penjualan_detail',$data);
    }

    function tambah_penjualan(){
        cek_session_akses('penjualan',$this->session->id_session);
        if (isset($_POST['submit1'])){
            if ($this->session->idp == ''){
                $data = array('kode_transaksi'=>$this->input->post('a'),
                  'id_pembeli'=>$this->input->post('b'),
                  'id_penjual'=>0,
                  'status_pembeli'=>'reseller',
                  'status_penjual'=>'admin',
                  'waktu_transaksi'=>date('Y-m-d H:i:s'),
                  'proses'=>'0');
                $this->model_app->insert('rb_penjualan',$data);
                $idp = $this->db->insert_id();
                $this->session->set_userdata(array('idp'=>$idp));
            }else{
                $data = array('kode_transaksi'=>$this->input->post('a'),
                  'id_pembeli'=>$this->input->post('b'));
                $where = array('id_penjualan' => $this->session->idp);
                $this->model_app->update('rb_penjualan', $data, $where);
            }
            redirect('administrator/tambah_penjualan');

        }elseif(isset($_POST['submit'])){
            $jual = $this->model_reseller->jual($this->input->post('aa'))->row_array();
            $beli = $this->model_reseller->beli($this->input->post('aa'))->row_array();
            $stok = $beli['beli']-$jual['jual'];
            if ($this->input->post('dd') > $stok){
                echo "<script>window.alert('Maaf, Stok Tidak Mencukupi!');
                window.location=('".base_url()."administrator/tambah_penjualan')</script>";
            }else{
                if ($this->input->post('idpd')==''){
                    $data = array('id_penjualan'=>$this->session->idp,
                      'id_produk'=>$this->input->post('aa'),
                      'jumlah'=>$this->input->post('dd'),
                      'diskon'=>$this->input->post('cc'),
                      'harga_jual'=>$this->input->post('bb'),
                      'satuan'=>$this->input->post('ee'));
                    $this->model_app->insert('rb_penjualan_detail',$data);
                }else{
                    $data = array('id_produk'=>$this->input->post('aa'),
                      'jumlah'=>$this->input->post('dd'),
                      'diskon'=>$this->input->post('cc'),
                      'harga_jual'=>$this->input->post('bb'),
                      'satuan'=>$this->input->post('ee'));
                    $where = array('id_penjualan_detail' => $this->input->post('idpd'));
                    $this->model_app->update('rb_penjualan_detail', $data, $where);
                }
                redirect('administrator/tambah_penjualan');
            }

        }else{
            $data['rows'] = $this->model_reseller->penjualan_detail($this->session->idp)->row_array();
            $data['record'] = $this->model_app->view_join_where('rb_penjualan_detail','rb_produk','id_produk',array('id_penjualan'=>$this->session->idp),'id_penjualan_detail','DESC');
            $data['barang'] = $this->model_app->view_ordering('rb_produk','id_produk','ASC');
            $data['reseller'] = $this->model_app->view_ordering('rb_reseller','id_reseller','ASC');
            if ($this->uri->segment(3)!=''){
                $data['row'] = $this->model_app->view_where('rb_penjualan_detail',array('id_penjualan_detail'=>$this->uri->segment(3)))->row_array();
            }
            $this->template->load('administrator/template','administrator/additional/mod_penjualan/view_penjualan_tambah',$data);
        }
    }

    function edit_penjualan(){
        cek_session_akses('penjualan',$this->session->id_session);
        if (isset($_POST['submit1'])){
            $data = array('kode_transaksi'=>$this->input->post('a'),
              'id_pembeli'=>$this->input->post('b'),
              'waktu_transaksi'=>$this->input->post('c'));
            $where = array('id_penjualan' => $this->input->post('idp'));
            $this->model_app->update('rb_penjualan', $data, $where);
            redirect('administrator/edit_penjualan/'.$this->input->post('idp'));

        }elseif(isset($_POST['submit'])){
            $cekk = $this->db->query("SELECT * FROM rb_penjualan_detail where id_penjualan='".$this->input->post('idp')."' AND id_produk='".$this->input->post('aa')."'")->row_array();
            $jual = $this->model_reseller->jual($this->input->post('aa'))->row_array();
            $beli = $this->model_reseller->beli($this->input->post('aa'))->row_array();
            $stok = $beli['beli']-$jual['jual']+$cekk['jumlah'];
            if ($this->input->post('dd') > $stok){
                echo "<script>window.alert('Maaf, Stok Tidak Mencukupi!');
                window.location=('".base_url()."administrator/edit_penjualan/".$this->input->post('idp')."')</script>";
            }else{
                if ($this->input->post('idpd')==''){
                    $data = array('id_penjualan'=>$this->input->post('idp'),
                      'id_produk'=>$this->input->post('aa'),
                      'jumlah'=>$this->input->post('dd'),
                      'diskon'=>$this->input->post('cc'),
                      'harga_jual'=>$this->input->post('bb'),
                      'satuan'=>$this->input->post('ee'));
                    $this->model_app->insert('rb_penjualan_detail',$data);
                }else{
                    $data = array('id_produk'=>$this->input->post('aa'),
                      'jumlah'=>$this->input->post('dd'),
                      'diskon'=>$this->input->post('cc'),
                      'harga_jual'=>$this->input->post('bb'),
                      'satuan'=>$this->input->post('ee'));
                    $where = array('id_penjualan_detail' => $this->input->post('idpd'));
                    $this->model_app->update('rb_penjualan_detail', $data, $where);
                }
                redirect('administrator/edit_penjualan/'.$this->input->post('idp'));
            }

        }else{
            $data['rows'] = $this->model_reseller->penjualan_detail($this->uri->segment(3))->row_array();
            $data['record'] = $this->model_app->view_join_where('rb_penjualan_detail','rb_produk','id_produk',array('id_penjualan'=>$this->uri->segment(3)),'id_penjualan_detail','DESC');
            $data['barang'] = $this->model_app->view_ordering('rb_produk','id_produk','ASC');
            $data['reseller'] = $this->model_app->view_ordering('rb_reseller','id_reseller','ASC');
            if ($this->uri->segment(4)!=''){
                $data['row'] = $this->model_app->view_where('rb_penjualan_detail',array('id_penjualan_detail'=>$this->uri->segment(4)))->row_array();
            }
            $this->template->load('administrator/template','administrator/additional/mod_penjualan/view_penjualan_edit',$data);
        }
    }

    function proses_penjualan(){
        cek_session_akses('penjualan',$this->session->id_session);
        $data = array('proses'=>$this->uri->segment(4));
        $where = array('id_penjualan' => $this->uri->segment(3));
        $this->model_app->update('rb_penjualan', $data, $where);

        $order = $this->db->query("SELECT a.*, b.id_pembeli, b.kode_transaksi FROM rb_penjualan_detail a JOIN rb_penjualan b ON a.id_penjualan=b.id_penjualan where a.id_penjualan='".$this->uri->segment(3)."'");
        foreach ($order->result_array() as $row) {
            $cek_produk = $this->db->query("SELECT * FROM rb_produk where id_produk_perusahaan='$row[id_produk]' AND id_reseller='$row[id_pembeli]'");
            if ($cek_produk->num_rows()>=1){
                $pro = $cek_produk->row_array();
                $kode_transaksi = "TRX-".date('YmdHis');
                $data = array('kode_transaksi'=>$kode_transaksi,
                  'id_pembeli'=>$row['id_pembeli'],
                  'id_penjual'=>'1',
                  'status_pembeli'=>'reseller',
                  'status_penjual'=>'admin',
                  'service'=>$row['kode_transaksi'],
                  'waktu_transaksi'=>date('Y-m-d H:i:s'),
                  'proses'=>'1');
                $this->model_app->insert('rb_penjualan',$data);
                $idp = $this->db->insert_id();

                $data = array('id_penjualan'=>$idp,
                  'id_produk'=>$pro['id_produk'],
                  'jumlah'=>$row['jumlah'],
                  'harga_jual'=>$row['harga_jual'],
                  'satuan'=>$row['satuan']);
                $this->model_app->insert('rb_penjualan_detail',$data);
            }else{
                $p = $this->db->query("SELECT * FROM rb_produk where id_produk='$row[id_produk]'")->row_array();
                $data = array('id_produk_perusahaan'=>$p['id_produk'],
                  'id_kategori_produk'=>$p['id_kategori_produk'],
                  'id_kategori_produk_sub'=>$p['id_kategori_produk_sub'],
                  'id_reseller'=>$row['id_pembeli'],
                  'nama_produk'=>$p['nama_produk'],
                  'produk_seo'=>$p['produk_seo'],
                  'satuan'=>$p['satuan'],
                  'harga_beli'=>$p['harga_beli'],
                  'harga_reseller'=>$p['harga_reseller'],
                  'harga_konsumen'=>$p['harga_konsumen'],
                  'berat'=>$p['berat'],
                  'gambar'=>$p['gambar'],
                  'keterangan'=>$p['keterangan'],
                  'username'=>$p['username'],
                  'waktu_input'=>date('Y-m-d H:i:s'));
                $this->model_app->insert('rb_produk',$data);
                $id_produk = $this->db->insert_id();

                $kode_transaksi = "TRX-".date('YmdHis');
                $data = array('kode_transaksi'=>$kode_transaksi,
                  'id_pembeli'=>$row['id_pembeli'],
                  'id_penjual'=>'1',
                  'status_pembeli'=>'reseller',
                  'status_penjual'=>'admin',
                  'service'=>$row['kode_transaksi'],
                  'waktu_transaksi'=>date('Y-m-d H:i:s'),
                  'proses'=>'1');
                $this->model_app->insert('rb_penjualan',$data);
                $idp = $this->db->insert_id();

                $data = array('id_penjualan'=>$idp,
                  'id_produk'=>$id_produk,
                  'jumlah'=>$row['jumlah'],
                  'harga_jual'=>$row['harga_jual'],
                  'satuan'=>$row['satuan']);
                $this->model_app->insert('rb_penjualan_detail',$data);
            }
        }

        redirect('administrator/penjualan');
    }

    function proses_penjualan_detail(){
        cek_session_akses('penjualan',$this->session->id_session);
        $data = array('proses'=>$this->uri->segment(4));
        $where = array('id_penjualan' => $this->uri->segment(3));
        $this->model_app->update('rb_penjualan', $data, $where);
        redirect('administrator/detail_penjualan/'.$this->uri->segment(3));
    }

    function delete_penjualan(){
        cek_session_akses('penjualan',$this->session->id_session);
        $id = array('id_penjualan' => $this->uri->segment(3));
        $this->model_app->delete('rb_penjualan',$id);
        $this->model_app->delete('rb_penjualan_detail',$id);
        redirect('administrator/penjualan');
    }

    function delete_penjualan_detail(){
        cek_session_akses('penjualan',$this->session->id_session);
        $id = array('id_penjualan_detail' => $this->uri->segment(4));
        $this->model_app->delete('rb_penjualan_detail',$id);
        redirect('administrator/edit_penjualan/'.$this->uri->segment(3));
    }

    function delete_penjualan_tambah_detail(){
        cek_session_akses('penjualan',$this->session->id_session);
        $id = array('id_penjualan_detail' => $this->uri->segment(3));
        $this->model_app->delete('rb_penjualan_detail',$id);
        redirect('administrator/tambah_penjualan');
    }

    function pembayaran_reseller(){
        cek_session_akses('konsumen',$this->session->id_session);
        $data['record'] = $this->db->query("SELECT a.*, b.*, c.kode_transaksi, c.proses FROM `rb_konfirmasi_pembayaran` a JOIN rb_rekening b ON a.id_rekening=b.id_rekening JOIN rb_penjualan c ON a.id_penjualan=c.id_penjualan ORDER BY a.id_konfirmasi_pembayaran DESC");
        $this->template->load('administrator/template','administrator/additional/mod_reseller/view_reseller_pembayaran',$data);
    }

    function download_bukti(){
        cek_session_akses('pembayaran_reseller',$this->session->id_session);
        $name = $this->uri->segment(3);
        $data = file_get_contents("asset/files/".$name);
        force_download($name, $data);
    }

    function keuangan(){
        cek_session_akses('keuangan',$this->session->id_session);
        $data['record'] = $this->model_app->view_ordering('rb_reseller','id_reseller','DESC');
        $this->template->load('administrator/template','administrator/additional/mod_keuangan/view_keuangan',$data);
    }

    function bayar_bonus(){
        cek_session_akses('keuangan',$this->session->id_session);
        if (isset($_POST['submit'])){
            $data = array('id_reseller'=>$this->input->post('idk'),
                'bonus_referral'=>$this->input->post('a'),
                'waktu_pencairan'=>date('YmdHis'));
            $this->model_app->insert('rb_pencairan_bonus',$data);
            redirect('administrator/bayar_bonus/'.$this->input->post('idk'));
        }else{
            $id = $this->uri->segment(3);
            $record = $this->model_reseller->reseller_pembelian($id,'admin');
            $penjualan = $this->model_reseller->penjualan_list_konsumen($id,'reseller');
            $edit = $this->model_app->edit('rb_reseller',array('id_reseller'=>$id))->row_array();
            $reward = $this->model_app->view_ordering('rb_reward','id_reward','ASC');

            $data = array('rows' => $edit,'record'=>$record,'penjualan'=>$penjualan,'reward'=>$reward);
            $this->template->load('administrator/template','administrator/additional/mod_keuangan/view_bayar_bonus',$data);
        }
    }

    function bayar_reward(){
        cek_session_akses('keuangan',$this->session->id_session);
        $data = array('id_reseller'=>$this->uri->segment(3),
            'id_reward'=>$this->uri->segment(4),
            'reward_date'=>$this->uri->segment(5),
            'waktu_pencairan'=>date('YmdHis'));
        $this->model_app->insert('rb_pencairan_reward',$data);
        redirect('administrator/bayar_bonus/'.$this->uri->segment(3));
    }

    function batalkan_reward(){
        cek_session_akses('keuangan',$this->session->id_session);
        $id = array('id_konsumen' => $this->uri->segment(4),'id_reward' => $this->uri->segment(3));
        $this->model_app->delete('rb_pencairan_reward',$id);
        redirect('administrator/bayar_bonus/'.$this->uri->segment(4));
    }

    function history_reward(){
        cek_session_akses('keuangan',$this->session->id_session);
        $data['record'] = $this->db->query("SELECT a.*, b.nama_reseller, c.posisi, c.reward FROM `rb_pencairan_reward` a JOIN rb_reseller b ON a.id_reseller=b.id_reseller JOIN rb_reward c ON a.id_reward=c.id_reward ORDER BY a.id_pencairan_reward DESC");
        $this->template->load('administrator/template','administrator/additional/mod_keuangan/view_history_reward',$data);
    }

    function history_referral(){
        cek_session_akses('keuangan',$this->session->id_session);
        //and c.waktu_order < DATE_ADD(a.tanggal_daftar, INTERVAL 6 MONTH) and (MONTH(c.waktu_order)='06' and YEAR(c.waktu_order)='2021')
        
        if($this->input->post('id_reseller')!=''){

             $data = array(  "id_reseller"       => $this->input->post('id_reseller'),
                            "id_konsumen"       => $this->input->post('id_konsumen'),
                            "bulan"             => $this->input->post('bulan'),
                            "tahun"             => $this->input->post('tahun'),
                            "jumlah_pencairan"  => $this->input->post('jumlah_pencairan'),
                            "tanggal_pencairan" => date("Y-m-d H:i:s")
                            );

            $this->db->insert("rb_pencairan_referal",$data);
        }
        
       $data['record'] = $this->db->query("select YEAR(c.waktu_order) as tahun, MONTHNAME(STR_TO_DATE(MONTH(c.waktu_order), '%m')) as bulan, a.id_reseller,nama_reseller,b.id_konsumen,b.nama_lengkap, a.tanggal_daftar,c.waktu_order, DATE_ADD(a.tanggal_daftar, INTERVAL 6 MONTH) as 'next_6_bulan', sum(c.komisi_topsonia) as komisi_topsonia, round((sum(c.komisi_topsonia)*1)/100) as bonus_referal from rb_reseller a join rb_konsumen b on a.referral=b.kode_referall join rb_penjualan c on a.id_reseller=c.id_penjual where c.proses='1' and c.waktu_order<DATE_ADD(a.tanggal_daftar, INTERVAL 6 MONTH) GROUP BY YEAR(c.waktu_order),MONTH(c.waktu_order),a.id_reseller");
        $this->template->load('administrator/template','administrator/additional/mod_keuangan/view_history_referral',$data);
    }
 
    function pencairan_referral(){
        cek_session_akses('keuangan',$this->session->id_session);
        //and c.waktu_order < DATE_ADD(a.tanggal_daftar, INTERVAL 6 MONTH) and (MONTH(c.waktu_order)='06' and YEAR(c.waktu_order)='2021')        
        $id_reseller = $this->uri->segment(3);
        $id_konsumen = $this->uri->segment(4);
        $bulan = $this->uri->segment(5);
        $tahun = $this->uri->segment(6);
        
        $data['record'] = $this->db->query("select b.no_rekening,YEAR(c.waktu_order) as tahun, MONTHNAME(STR_TO_DATE(MONTH(c.waktu_order), '%m')) as bulan, a.id_reseller,nama_reseller,b.id_konsumen,b.nama_lengkap, a.tanggal_daftar,c.waktu_order, DATE_ADD(a.tanggal_daftar, INTERVAL 6 MONTH) as 'next_6_bulan', sum(c.komisi_topsonia) as komisi_topsonia, round((sum(c.komisi_topsonia)*1)/100) as bonus_referal from rb_reseller a join rb_konsumen b on a.referral=b.kode_referall join rb_penjualan c on a.id_reseller=c.id_penjual where c.proses='1' and a.id_reseller='$id_reseller' and b.id_konsumen='$id_konsumen' and YEAR(c.waktu_order)='$tahun' and MONTHNAME(STR_TO_DATE(MONTH(c.waktu_order), '%m'))='$bulan' GROUP BY a.id_reseller,YEAR(c.waktu_order),MONTH(c.waktu_order)")->row_array();
        $this->template->load('administrator/template','administrator/additional/mod_keuangan/view_pencairan_referall',$data);
    }

    function delete_history_reward(){
        cek_session_akses('keuangan',$this->session->id_session);
        $id = array('id_pencairan_reward' => $this->uri->segment(3));
        $this->model_app->delete('rb_pencairan_reward',$id);
        redirect('administrator/history_reward');
    }

    function delete_history_referral(){
        cek_session_akses('keuangan',$this->session->id_session);
        $id = array('id_pencairan_bonus' => $this->uri->segment(3));
        $this->model_app->delete('rb_pencairan_bonus',$id);
        redirect('administrator/history_referral');
    }

     
    function history_komisi()
    {
        cek_session_akses('keuangan', $this->session->id_session);
        //and c.waktu_order < DATE_ADD(a.tanggal_daftar, INTERVAL 6 MONTH) and (MONTH(c.waktu_order)='06' and YEAR(c.waktu_order)='2021')

        if ($this->input->post('id_pencairan_bonus') != '') {

            $this->db->update("rb_pencairan_komisi",array('status' => 'Lunas', 'tanggal_pencairan' => date("Y/m/d")), array('id_pencairan_bonus'=>$this->input->post('id_pencairan_bonus')) );
        }

        $data['record'] = $this->db->query("select a.id_konsumen,a.nama_lengkap, b.waktu_order, sum(b.diskon) as diskon from rb_konsumen a join rb_penjualan b on a.id_konsumen=b.id_pembeli join tipe_buyer c on a.id_tipe_buyer=c.id_tipe_buyer where b.proses='1' and c.skema_diskon>0 and b.status_pembeli='konsumen' and b.diskon>0 and b.waktu_order>='2021-09-01' GROUP BY a.id_konsumen ORDER BY a.id_konsumen");
        $this->template->load('administrator/template', 'administrator/additional/mod_keuangan/view_history_komisi', $data);
    }

    //update komisi
    function filter_komisi()
    {
        if ($this->input->post('tahun') != 'p') {
            $thn = $this->input->post('tahun');
            $tahun = "and YEAR(b.waktu_order)='$thn'";
        } else {
            $tahun = "";
        }

        if ($this->input->post('bulan') != 'p') {
            $bln = $this->input->post('bulan');
            $bulan = "and MONTH(b.waktu_order)='$bln'";
        } else {
            $bulan = "";
        }

        if ($this->input->post('id_konsumen') != 'p') {
            $ksm = $this->input->post('id_konsumen');
            $konsumen = "and a.id_konsumen='$ksm'";
        } else {
            $konsumen = "";
        }

        $record = $this->db->query("select a.id_konsumen,a.nama_lengkap, b.waktu_order, sum(b.diskon) as diskon from rb_konsumen a join rb_penjualan b on a.id_konsumen=b.id_pembeli join tipe_buyer c on a.id_tipe_buyer=c.id_tipe_buyer where b.proses='1' and c.skema_diskon>0 and b.status_pembeli='konsumen' and b.waktu_order>='2021-09-01' and b.diskon>0 $tahun $bulan $konsumen GROUP BY a.id_konsumen ORDER BY id_konsumen ");
        $cek = $record->num_rows();
        if ($cek < 1) {
            $data['result'] = '0';
        } else {
            $data['result'] = '1';
            $no = 1;
            $filter = "";
            foreach ($record->result_array() as $row) {

                $sisa_saldo = "Rp " . rupiah($row['diskon']);
               
                $filter = $filter . "<tr><td>$no</td>
                  <td>$row[nama_lengkap]</td>
                  <td>-</td>
                  <td>$sisa_saldo</td>
                  
              </tr>";
                $no++;
            }
            $data['filter'] = $filter;
        }
        echo json_encode($data);
    }

    //update komisi
    function filter_request_komisi()
    {

        if ($this->input->post('id_konsumen') != 'p') {
            $ksm = $this->input->post('id_konsumen');
            $konsumen = "and a.id_konsumen='$ksm'";
        } else {
            $konsumen = "";
        }

        if ($this->input->post('status') != 'p') {
            $sts = $this->input->post('status');
            $status = "and a.status='$sts'";
        } else {
            $status = "";
        }

        $record = $this->db->query("select a.id_pencairan_bonus,a.status,a.jumlah_pencairan,a.tanggal_request,a.tanggal_pencairan,a.id_konsumen,b.nama_lengkap from rb_pencairan_komisi a join rb_konsumen b on a.id_konsumen=b.id_konsumen $konsumen $status ORDER BY status desc,id_pencairan_bonus asc");
        $cek = $record->num_rows();
        if ($cek < 1) {
            $data['result'] = '0';
        } else {
            $data['result'] = '1';
            $no = 1;
            $filter = "";
            foreach ($record->result_array() as $row) {

                if($row['status']=='Lunas'){
                    $disabled='disabled';
                  }else{$disabled='';}
               
                $filter = $filter . "<tr><td>$no</td>
                <td>$row[nama_lengkap]</td>
                <td>$row[tanggal_request]</td>
                <td>$row[jumlah_pencairan]</td>
                <td>$row[tanggal_pencairan]</td>
                <td>$row[status]</td>
                <td><center>
                <a class='btn btn-warning btn-xs' title='Pencairan Bonus' href='" . base_url() . "administrator/pencairan_komisi/$pen[id_pencairan_bonus]/' $disabled><span class='fa fa-money'></span></a>
                </center></td>
                </tr>";
                $no++;
            }
            $data['filter'] = $filter;
        }
        echo json_encode($data);
    }

    //update komisi
    function pencairan_komisi()
    {
        cek_session_akses('keuangan', $this->session->id_session);
        //and c.waktu_order < DATE_ADD(a.tanggal_daftar, INTERVAL 6 MONTH) and (MONTH(c.waktu_order)='06' and YEAR(c.waktu_order)='2021')       
        $id_pencairan_bonus = $this->uri->segment(3);


        $data['record'] = $this->db->query("select a.id_konsumen,a.nama_lengkap,a.no_rekening,b.id_pencairan_bonus,b.jumlah_pencairan,b.tanggal_request from rb_konsumen a join rb_pencairan_komisi b on a.id_konsumen=b.id_konsumen where id_pencairan_bonus='$id_pencairan_bonus' GROUP BY a.id_konsumen ORDER BY a.id_konsumen")->row_array();
        $this->template->load('administrator/template', 'administrator/additional/mod_keuangan/view_pencairan_komisi', $data);
    }

	// Controller Transaksi Penjualan
    function transaksi(){
        cek_session_akses('transaksi',$this->session->id_session);
         $this->db->query("update rb_penjualan a join rb_konfirmasi_pembayaran_konsumen b on a.kode_transaksi=b.kode_transaksi set a.flag_admin='1' where a.flag_admin='0' and status_pembayaran='lunas' and (a.selesai='1' or a.batal='1')");
      
        $id = array('id_penjual' => $this->uri->segment(3));
        // $data['record'] = $this->model_reseller->penjualan_list_transaksi(1,'admin', $id);
        // $data['record'] = $this->model_reseller->penjualan_list_transaksi_refund(1,'admin', $id);
        $data['record'] = $this->db->query("SELECT *, a.kode_transaksi as kode_trx,a.id_penjualan as idp FROM `rb_penjualan` a JOIN rb_konsumen b ON a.id_pembeli=b.id_konsumen JOIN rb_reseller c ON a.id_penjual=c.id_reseller LEFT JOIN rb_konfirmasi_pembayaran_konsumen d ON a.kode_transaksi=d.kode_transaksi where a.status_penjual='reseller' AND a.`order` = '1' group by kode_trx ORDER BY a.id_penjualan DESC");
        
        $data['record2'] = $this->db->query("SELECT *,a.kode_transaksi as kode_trx,a.id_penjualan as idp FROM `rb_penjualan` a JOIN rb_konsumen b ON a.id_pembeli=b.id_konsumen JOIN rb_reseller c ON a.id_penjual=c.id_reseller LEFT JOIN rb_konfirmasi_pembayaran_konsumen d ON a.kode_transaksi=d.kode_transaksi where a.status_penjual='reseller' AND a.`order` = '1' GROUP BY a.id_penjualan ORDER BY a.id_penjualan DESC");

        $this->template->load('administrator/template','administrator/additional/mod_transaksi_merchant/view_transaksi_merchant',$data);
    }

    function detail_transaksi(){
        cek_session_akses('transaksi',$this->session->id_session);
        
        $this->db->query("update rb_penjualan a join rb_konfirmasi_pembayaran_konsumen b on a.kode_transaksi=b.kode_transaksi set a.flag_admin='1' where a.kode_transaksi='" . $this->uri->segment(3) . "' and a.flag_admin='0' and status_pembayaran='lunas' and (a.selesai='1' or a.batal='1')");
      

       $idp = array('kode_transaksi' => $this->uri->segment(3));
        $data['rows'] = $this->db->query("SELECT *, a.id_penjualan as idp  FROM `rb_penjualan` a JOIN rb_konsumen b ON a.id_pembeli=b.id_konsumen JOIN rb_reseller c ON a.id_penjual=c.id_reseller JOIN alamat d on a.id_alamat=d.id_alamat where a.kode_transaksi='" . $this->uri->segment(3) . "' ORDER BY a.id_penjualan DESC")->row_array();
        if(trim($this->uri->segment(4))==''){
            $data['rows2'] = $this->db->query("SELECT *,a.diskon_buyer as disk_b, a.id_penjualan as idp  FROM `rb_penjualan` a JOIN rb_konsumen b ON a.id_pembeli=b.id_konsumen JOIN rb_reseller c ON a.id_penjual=c.id_reseller where a.kode_transaksi='" . $this->uri->segment(3) . "' ORDER BY a.id_penjualan DESC")->result_array();
        }else{
            $data['rows2'] = $this->db->query("SELECT *,a.diskon_buyer as disk_b, a.id_penjualan as idp  FROM `rb_penjualan` a JOIN rb_konsumen b ON a.id_pembeli=b.id_konsumen JOIN rb_reseller c ON a.id_penjual=c.id_reseller where a.kode_transaksi='" . $this->uri->segment(3) . "' and a.id_penjualan='" . $this->uri->segment(4) . "' ORDER BY a.id_penjualan DESC")->result_array();
        }
        // $data['rows'] = $this->model_reseller->penjualan_transaksi_detail($this->uri->segment(3))->row_array();
        $data['record'] = $this->model_app->view_join_where('rb_penjualan_detail','rb_produk','id_produk',array('id_penjualan'=>$this->uri->segment(3)),'id_penjualan_detail','DESC');
        $data['rec_bayar'] = $this->model_app->view_join_where('rb_konfirmasi_pembayaran_konsumen','rb_rekening','id_rekening',array('kode_transaksi'=>$this->uri->segment(3)),'id_konfirmasi_pembayaran','ASC');
        $data['pemb'] = $this->model_app->view_where('rb_konfirmasi_pembayaran_konsumen',array('kode_transaksi'=>$this->uri->segment(3)))->row_array();
        $this->template->load('administrator/template','administrator/additional/mod_transaksi_merchant/view_transaksi_merchant_detail',$data);
    }
    
    function refund(){
        cek_session_akses('transaksi',$this->session->id_session);
        if (isset($_POST['submit'])){
           $id = $this->uri->segment(3);
           $config['upload_path'] = 'asset/bukti_transfer_refund/';
           $config['allowed_types'] = 'gif|jpg|png|JPG|JPEG|swf';
                $config['max_size'] = '5000'; // kb
                $config['encrypt_name'] = FALSE;
                $config['overwrite'] = TRUE;
                $this->load->library('upload', $config);
                $this->upload->do_upload('foto');
                $hasil = $this->upload->data()['file_name'];
                // $this->func_resize($hasil, 'asset/bukti_transfer_refund/', 611, 838);
                $data = array('bukti_transfer_refund'=> $hasil,
                  'waktu_transfer_refund'=>date('Y-m-d H:i:s'));
                $where = array('kode_transaksi' => $this->uri->segment(3));
                $this->model_app->update('rb_konfirmasi_refund', $data, $where);

                $data1 = array('status_pembayaran'=>$this->uri->segment(2));
                $where1 = array('kode_transaksi' => $this->uri->segment(3));
                $this->model_app->update('rb_konfirmasi_pembayaran_konsumen', $data1, $where1);


                redirect($this->uri->segment(1).'/detail_refund/'.$this->uri->segment(3));
            }else{      

              redirect($this->uri->segment(1).'/detail_refund/'.$this->uri->segment(3));

          }
      }

      private function set_upload_options1($new_name){
        $config = array();
        $config['upload_path'] = 'asset/bukti_transfer_refund/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '5000'; // kb
        $config['file_name'] = $new_name;
        $config['encrypt_name'] = FALSE;
        $this->load->library('upload', $config);
        return $config;
    }


    function detail_refund(){
        cek_session_akses('transaksi',$this->session->id_session);
        $idp = array('kode_transaksi' => $this->uri->segment(3));
        $data['rows'] = $this->model_reseller->penjualan_transaksi_detail($this->uri->segment(3))->row_array();
        $data['record'] = $this->db->query("SELECT *, a.kode_transaksi as kode_trx,a.id_penjualan as idp FROM `rb_penjualan` a JOIN rb_penjualan_detail b ON a.id_penjualan=b.id_penjualan JOIN rb_produk c ON b.id_produk=c.id_produk where a.kode_transaksi='".$this->uri->segment(3)."'")->result_array();
        $data['rec_bayar'] = $this->model_app->view_join_where('rb_konfirmasi_pembayaran_konsumen','rb_rekening','id_rekening',array('kode_transaksi'=>$this->uri->segment(3)),'id_konfirmasi_pembayaran','ASC');
        $data['ref'] = $this->model_app->view_where('rb_konfirmasi_refund',array('kode_transaksi'=>$this->uri->segment(3)))->row_array();
        $data['pemb'] = $this->model_app->view_where('rb_konfirmasi_pembayaran_konsumen',array('kode_transaksi'=>$this->uri->segment(3)))->row_array();
        $this->template->load('administrator/template','administrator/additional/mod_transaksi_merchant/view_refund',$data);
    }
	// Controller Konfirmasi pembayaran

    function konfirmasi_pembayaran(){
        cek_session_akses('transaksi',$this->session->id_session);
        $data = array('status_pembayaran'=>$this->input->post('status_pembayaran'),
          'remarks'=>$this->input->post('remarks'));
        $where = array('kode_transaksi' => $this->uri->segment(3));
        $this->model_app->update('rb_konfirmasi_pembayaran_konsumen', $data, $where);
       // $this->model_app->update('rb_penjualan', array('flag_admin'=>'1'), $where);
        
        if ($this->input->post('status_pembayaran') == 'lunas'){
        //email reseller
            $mod = $this->db->query("SELECT *, a.id_penjualan as idp, c.email as email_reseller FROM `rb_penjualan` a JOIN rb_konsumen b ON a.id_pembeli=b.id_konsumen JOIN rb_reseller c ON a.id_penjual=c.id_reseller JOIN alamat d on a.id_alamat=d.id_alamat join rb_kota e ON d.kota_id=e.kota_id where a.kode_transaksi='".$this->uri->segment(3)."' ORDER BY a.id_penjualan DESC")->row_array();
            $belanjaan = $this->model_app->view_join_where('rb_penjualan_detail','rb_produk','id_produk',array('id_penjualan'=>$mod['idp']),'id_penjualan_detail','ASC');
            $tujuan = $mod['email_reseller'];
            $tglaktif = date("d-m-Y H:i:s");

            $subjek      = "$iden[nama_website] - Anda mempunyai orderan";
            $message      = "<html><body>Halo <b>".$mod['nama_reseller']."</b><br>Anda Mendapatkan Pesanan baru di Topsonia, pada tanggal <b>$tglaktif</b>.
            <br><b>Berikut Detail Pemesan :</b><br>
            <table style='width:100%;border:0px'>
            <tr><td width='140px'>Nama Lengkap</td>  <td> : (".$mod['nama_alamat'].") ".$mod['pic']."</td></tr>
            <tr><td>No Telpon</td>                  <td> : ".$mod['no_hp']."</td></tr>
            <tr><td>Alamat</b></td>                 <td> : ".$mod['alamat_lengkap']." </td></tr>
            <tr><td>Kabupaten/Kota</b></td>         <td> : ".$mod['nama_kota']." </td></tr>
            <tr><td>Kecamatan</b></td>              <td> : ".$mod['kecamatan']." </td></tr>
            <tr><td>Kode Pos</b></td>               <td> : ".$mod['kode_pos']." </td></tr>      
            </table><br>

            <table style='width:100%;border:0px' >
            <tr><td style='width:30%'>No Pesanan :</td><td> <b>#".$mod['kode_transaksi']."</b></td></tr>
            <tr><td>Tanggal Pemesanan:</td><td>".$mod['waktu_order']."</td></tr>
            </table>";

            $message .= "<b>RINCIAN PESANAN</b><br>";

            $no = 1;
            foreach($belanjaan as $row){ 
                $ex = explode(';', $row['gambar']);
                if (trim($ex[0])==''){ $foto_produk = 'no-image.png'; }else{ $foto_produk = $ex[0]; }
                $sub_total = ($row['harga_jual']*$row['jumlah']);
                $message .= "
                <img src='".base_url().'asset/foto_produk/'.$foto_produk ."' style='width:150px'><br><br>
                <table style='width:100%;border:0px' >
                <tr><td>Produk</td><td>$row[nama_produk]</td></tr>
                <tr><td>Jumlah</td><td>$row[jumlah]</td></tr>
                <tr><td>Harga</td><td>".rupiah($row['harga_jual'])."</td></tr>
                <tr><td>Berat</td><td>".($row['berat']*$row['jumlah'])." Gram</td></tr>
                </table>";

                $no++;
            }

            $total = $this->db->query("SELECT sum(( b.harga_jual * b.jumlah )- b.diskon ) AS total, sum( c.berat * b.jumlah ) AS total_berat, ROUND(sum((((IFNULL(d.komisi,0)/100)*b.harga_jual)*(IFNULL(f.skema_diskon/100,0)))*b.jumlah), 0 ) AS totaldiskon, ROUND(sum((((IFNULL(d.komisi,0)/100)*b.harga_jual)*(IFNULL(f.komisi_topsonia/100,0)))*b.jumlah), 0 ) AS totalKomisiTopsonia FROM rb_penjualan a JOIN rb_penjualan_detail b ON a.id_penjualan = b.id_penjualan JOIN rb_produk c ON b.id_produk = c.id_produk JOIN rb_reseller d ON a.id_penjual = d.id_reseller JOIN rb_konsumen e ON a.id_pembeli = e.id_konsumen JOIN tipe_buyer f ON e.id_tipe_buyer = f.id_tipe_buyer where a.kode_transaksi='".$this->uri->segment(3)."'")->row_array();
            $sub_total = ($total['total']+$mod['ongkir']+$mod['asuransi']+$mod['pajak']+$mod['mdr'])-$mod['diskon']-$mod['diskon_ongkir'];
            $message .=
            "<hr>
            <table style='width:100%;border:0px' >
            <tr><td>Sub Total<td><td>Rp ".rupiah($total['total'])."</td></tr>
            <tr><td>Diskon Belanja<td><td>Rp ".rupiah($mod['diskon'])."</td></tr>
            <tr><td>Ongkos Kirim<td><td>Rp ".rupiah($mod['ongkir'])."</td></tr>
            <tr><td>Asuransi<td><td>Rp ".rupiah($mod['asuransi'])."</td></tr>
            <tr><td>Diskon Ongkir<td><td>Rp ".rupiah($mod['diskon_ongkir'])."</td></tr>
            <tr><td>Pajak<td><td>Rp ".rupiah($mod['pajak'])."</td></tr>
            <tr><td>Biaya Admin<td><td>Rp ".rupiah($mod['mdr'])."</td></tr>
            <tr><td>Total Pembayaran<td><td>Rp ".rupiah($sub_total)."</td></tr>
            </table>
            </tbody>
            </table>
            <br>
            Silahkan untuk memproses transaksi pembelian kosumen <a href='".base_url()."reseller/index'>disini</a><br>

            Salam<br>Tim, Topsonia<br><br><br><br>
            Butuh bantuan ? Hubungi kami <a href='".base_url()."hubungi'>disini</a>";
            echo kirim_email($subjek,$message,$tujuan);
        }

        redirect($this->uri->segment(1).'/detail_transaksi/'.$this->uri->segment(3));
        
    }

	// function konfirmasi_lunas(){
		// cek_session_akses('transaksi',$this->session->id_session);
		// $data = array('status_pembayaran'=>$this->uri->segment(4));
		// $where = array('id_penjualan' => $this->uri->segment(3));
		// $this->model_app->update('rb_konfirmasi_pembayaran_konsumen', $data, $where);
		// $this->session->set_flashdata('success', 'Pembayaran berhasil dikonfirmasi');
		// redirect($this->uri->segment(1).'/detail_transaksi/'.$this->uri->segment(3));
    // }

	// function konfirmasi_gagal(){
		// cek_session_akses('transaksi',$this->session->id_session);
		// $data = array('status_pembayaran'=>$this->uri->segment(4));
		// $where = array('id_penjualan' => $this->uri->segment(3));
		// $this->model_app->update('rb_konfirmasi_pembayaran_konsumen', $data, $where);
		// $this->session->set_flashdata('success', 'Pembayaran Gagal dikonfirmasi');
		// redirect($this->uri->segment(1).'/detail_transaksi/'.$this->uri->segment(3));
	// }

    public function func_resize($filename, $source_image, $width, $height)
    {
      $this->image_moo
      ->load($source_image.$filename)
      ->stretch($width, $height)
      ->save($source_image.$filename, $overwrite=TRUE);
      echo $this->image_moo->display_errors();
      chmod($source_image.$filename, 0777);
  }

  function tipe_konsumen(){
      cek_session_akses('tipe_konsumen',$this->session->id_session);
      $data['record'] = $this->db->query("SELECT * FROM `tipe_buyer`");
		// $this->load->view('administrator/additional/mod_konsumen/view_tipe_konsumen');
      $this->template->load('administrator/template','administrator/additional/mod_konsumen/view_tipe konsumen',$data);
  }

  function tambah_tipe_konsumen(){
      cek_session_akses('tipe_konsumen',$this->session->id_session);
      if (isset($_POST['submit'])){
            // $this->model_rekening->rekening_tambah();
        $data = array('tipe_buyer'=>$this->db->escape_str($this->input->post('tipe_buyer')),
            'skema_diskon'=>$this->db->escape_str($this->input->post('skema_diskon')),
            'komisi_topsonia'=>$this->db->escape_str($this->input->post('komisi_topsonia')),
            'waktu'=>date('Y-m-d H:i:s'));
        $this->model_app->insert('tipe_buyer',$data);
        redirect($this->uri->segment(1).'/tipe_konsumen');
    }


}

	// function delete_tipe_buyer(){
        // cek_session_akses('tipe_konsumen',$this->session->id_session);
		// if ($this->session->level=='admin'){
            // $id = array('id_tipe_buyer' => $this->uri->segment(3));
        // }else{
            // $id = array('id_tipe_buyer' => $this->uri->segment(3));
        // }
		// $this->model_app->delete('tipe_buyer',$id);
		// redirect($this->uri->segment(1).'/tipe_konsumen');
	// }

function edit_tipe_konsumen(){
  cek_session_akses('tipe_konsumen',$this->session->id_session);
  $id_tipe_buyer = $this->uri->segment(3);
  if (isset($_POST['submit'])){
    $data = array('tipe_buyer'=>$this->input->post('tipe_buyer'),
        'skema_diskon'=>$this->input->post('skema_diskon'),
        'komisi_topsonia'=>$this->input->post('komisi_topsonia'),
        'waktu'=>date('Y-m-d H:i:s'));

    $where = array('id_tipe_buyer' => $this->input->post('id_tipe_buyer'));
    $this->model_app->update('tipe_buyer', $data, $where);
    redirect($this->uri->segment(1).'/tipe_konsumen');
}else{
    $data['rows'] = $this->model_app->edit('tipe_buyer',array('id_tipe_buyer'=>$id_tipe_buyer))->row_array();
    $this->template->load('administrator/template','administrator/additional/mod_konsumen/view_edit_tipe_buyer',$data);
}
}

function viewfinance(){
    cek_session_akses('keuangan',$this->session->id_session);
    if (isset($_POST['submit'])){
        $bln        = $this->input->get('bln');
        $thn        = $this->input->get('thn');
        $pembayaran = $this->input->post('pembayaran');
        $merchant   = $this->input->post('merchant');
        $tanggal1   = date('Y-m-d', strtotime($this->input->post('tanggal_awal')));
        $tanggal2   = date('Y-m-d', strtotime($this->input->post('tanggal_akhir')));

        $data['bulan'] = $this->db->query("SELECT *, MONTH (waktu_order) AS bulan FROM rb_penjualan WHERE proses='1' && `order`='1' && MONTH(waktu_order)='$bln'")->row_array();

        if ($merchant != "" && $pembayaran != "") {
            
            $data['detail_perbulan'] = $this->db->query("SELECT a.kategori_pembayaran, a.id_penjualan, b.id_reseller, a.kode_transaksi, b.nama_reseller,
                DATE ( a.waktu_order ) AS tanggal_order,
                ( IFNULL( a.total_bayar, 0 )+ IFNULL( a.diskon, 0 ))-( IFNULL( a.ongkir, 0 )- IFNULL( a.diskon_ongkir, 0 )) AS harga_publish, 
                a.diskon, 
                IFNULL( a.total_bayar, 0 )-( IFNULL( a.ongkir, 0 )- IFNULL( a.diskon_ongkir, 0 )) AS harga_jual,
                ( IFNULL( a.total_bayar, 0 )-( IFNULL( a.ongkir, 0 )- IFNULL( a.diskon_ongkir, 0 )))- IFNULL( a.komisi_topsonia, 0 ) AS harga_merchant,
                IFNULL( a.mdr, 0 ) AS mdr_net, IFNULL( a.komisi_topsonia, 0 ) AS topsonia_net,
                a.pajak, a.ongkir, a.diskon_ongkir,
                IFNULL( a.ongkir, 0 )- IFNULL( a.diskon_ongkir, 0 ) AS ongkir_net 
                FROM rb_penjualan a JOIN rb_reseller b ON a.id_penjual = b.id_reseller 
                WHERE a.proses = '1' && a.ORDER = '1' && a.ORDER = '1' && MONTH ( a.waktu_order )= '$bln' && YEAR(a.waktu_order)='$thn' && a.kategori_pembayaran='$pembayaran' && a.id_penjual='$merchant'
                ORDER BY a.waktu_order DESC");

            $data['total_perbulan'] = $this->db->query("SELECT
                sum(( IFNULL( total_bayar, 0 )+ IFNULL( diskon, 0 ))-( IFNULL( ongkir, 0 )- IFNULL( diskon_ongkir, 0 ))) AS total_harga_publish,
                sum( diskon ) AS total_diskon,
                sum( IFNULL( total_bayar, 0 )-( IFNULL( ongkir, 0 )- IFNULL( diskon_ongkir, 0 ))) AS total_harga_jual,
                sum(( IFNULL( total_bayar, 0 )-( IFNULL( ongkir, 0 )- IFNULL( diskon_ongkir, 0 )))- IFNULL( komisi_topsonia, 0 )) AS total_harga_merchant,
                sum( IFNULL( komisi_topsonia, 0 )) AS total_topsonia_net,
                sum( IFNULL( mdr, 0 ) ) AS total_total_mdr,
                sum( IFNULL( pajak, 0 )) AS total_total_pajak,
                sum( IFNULL( ongkir, 0 )) AS total_ongkir,
                sum( IFNULL( diskon_ongkir, 0 )) AS total_diskon_ongkir,
                sum( IFNULL( ongkir, 0 )- IFNULL( diskon_ongkir, 0 )) AS total_ongkir_net 
                FROM rb_penjualan WHERE proses = '1' && `order` = '1' && MONTH ( waktu_order )= '$bln' && YEAR(waktu_order)='$thn' && kategori_pembayaran='$pembayaran' && id_penjual='$merchant' && (DATE( waktu_order ) BETWEEN '$tanggal1' AND '$tanggal2')")->row_array();

        }elseif ($merchant != "" && $pembayaran == "") {
            
            $data['detail_perbulan'] = $this->db->query("SELECT a.kategori_pembayaran, a.id_penjualan, b.id_reseller, a.kode_transaksi, b.nama_reseller,
                DATE ( a.waktu_order ) AS tanggal_order,
                ( IFNULL( a.total_bayar, 0 )+ IFNULL( a.diskon, 0 ))-( IFNULL( a.ongkir, 0 )- IFNULL( a.diskon_ongkir, 0 )) AS harga_publish, 
                a.diskon, 
                IFNULL( a.total_bayar, 0 )-( IFNULL( a.ongkir, 0 )- IFNULL( a.diskon_ongkir, 0 )) AS harga_jual,
                ( IFNULL( a.total_bayar, 0 )-( IFNULL( a.ongkir, 0 )- IFNULL( a.diskon_ongkir, 0 )))- IFNULL( a.komisi_topsonia, 0 ) AS harga_merchant,
                IFNULL( a.mdr, 0 ) AS mdr_net, IFNULL( a.komisi_topsonia, 0 ) AS topsonia_net,
                a.pajak, a.ongkir, a.diskon_ongkir,
                IFNULL( a.ongkir, 0 )- IFNULL( a.diskon_ongkir, 0 ) AS ongkir_net 
                FROM rb_penjualan a JOIN rb_reseller b ON a.id_penjual = b.id_reseller 
                WHERE a.proses = '1' && a.ORDER = '1' && a.ORDER = '1' && MONTH ( a.waktu_order )= '$bln' && YEAR(a.waktu_order)='$thn' && a.id_penjual='$merchant'
                ORDER BY a.waktu_order DESC");

            $data['total_perbulan'] = $this->db->query("SELECT
                sum(( IFNULL( total_bayar, 0 )+ IFNULL( diskon, 0 ))-( IFNULL( ongkir, 0 )- IFNULL( diskon_ongkir, 0 ))) AS total_harga_publish,
                sum( diskon ) AS total_diskon,
                sum( IFNULL( total_bayar, 0 )-( IFNULL( ongkir, 0 )- IFNULL( diskon_ongkir, 0 ))) AS total_harga_jual,
                sum(( IFNULL( total_bayar, 0 )-( IFNULL( ongkir, 0 )- IFNULL( diskon_ongkir, 0 )))- IFNULL( komisi_topsonia, 0 )) AS total_harga_merchant,
                sum( IFNULL( komisi_topsonia, 0 )) AS total_topsonia_net,
                sum( IFNULL( mdr, 0 ) ) AS total_total_mdr,
                sum( IFNULL( pajak, 0 )) AS total_total_pajak,
                sum( IFNULL( ongkir, 0 )) AS total_ongkir,
                sum( IFNULL( diskon_ongkir, 0 )) AS total_diskon_ongkir,
                sum( IFNULL( ongkir, 0 )- IFNULL( diskon_ongkir, 0 )) AS total_ongkir_net 
                FROM rb_penjualan WHERE proses = '1' && `order` = '1' && MONTH ( waktu_order )= '$bln' && YEAR(waktu_order)='$thn' && id_penjual='$merchant'")->row_array();

        }elseif ($merchant == "" && $pembayaran != "") {
            
            $data['detail_perbulan'] = $this->db->query("SELECT a.kategori_pembayaran, a.id_penjualan, b.id_reseller, a.kode_transaksi, b.nama_reseller,
                DATE ( a.waktu_order ) AS tanggal_order,
                ( IFNULL( a.total_bayar, 0 )+ IFNULL( a.diskon, 0 ))-( IFNULL( a.ongkir, 0 )- IFNULL( a.diskon_ongkir, 0 )) AS harga_publish, 
                a.diskon, 
                IFNULL( a.total_bayar, 0 )-( IFNULL( a.ongkir, 0 )- IFNULL( a.diskon_ongkir, 0 )) AS harga_jual,
                ( IFNULL( a.total_bayar, 0 )-( IFNULL( a.ongkir, 0 )- IFNULL( a.diskon_ongkir, 0 )))- IFNULL( a.komisi_topsonia, 0 ) AS harga_merchant,
                IFNULL( a.mdr, 0 ) AS mdr_net, IFNULL( a.komisi_topsonia, 0 ) AS topsonia_net,
                a.pajak, a.ongkir, a.diskon_ongkir,
                IFNULL( a.ongkir, 0 )- IFNULL( a.diskon_ongkir, 0 ) AS ongkir_net 
                FROM rb_penjualan a JOIN rb_reseller b ON a.id_penjual = b.id_reseller 
                WHERE a.proses = '1' && a.ORDER = '1' && a.ORDER = '1' && MONTH ( a.waktu_order )= '$bln' && YEAR(a.waktu_order)='$thn' && a.kategori_pembayaran='$pembayaran'
                ORDER BY a.waktu_order DESC");

            $data['total_perbulan'] = $this->db->query("SELECT
                sum(( IFNULL( total_bayar, 0 )+ IFNULL( diskon, 0 ))-( IFNULL( ongkir, 0 )- IFNULL( diskon_ongkir, 0 ))) AS total_harga_publish,
                sum( diskon ) AS total_diskon,
                sum( IFNULL( total_bayar, 0 )-( IFNULL( ongkir, 0 )- IFNULL( diskon_ongkir, 0 ))) AS total_harga_jual,
                sum(( IFNULL( total_bayar, 0 )-( IFNULL( ongkir, 0 )- IFNULL( diskon_ongkir, 0 )))- IFNULL( komisi_topsonia, 0 )) AS total_harga_merchant,
                sum( IFNULL( komisi_topsonia, 0 )) AS total_topsonia_net,
                sum( IFNULL( mdr, 0 ) ) AS total_total_mdr,
                sum( IFNULL( pajak, 0 )) AS total_total_pajak,
                sum( IFNULL( ongkir, 0 )) AS total_ongkir,
                sum( IFNULL( diskon_ongkir, 0 )) AS total_diskon_ongkir,
                sum( IFNULL( ongkir, 0 )- IFNULL( diskon_ongkir, 0 )) AS total_ongkir_net 
                FROM rb_penjualan WHERE proses = '1' && `order` = '1' && MONTH ( waktu_order )= '$bln' && YEAR(waktu_order)='$thn' && kategori_pembayaran='$pembayaran'")->row_array();

        }else{

            $data['detail_perbulan'] = $this->db->query("SELECT a.kategori_pembayaran, a.id_penjualan, b.id_reseller, a.kode_transaksi, b.nama_reseller, a.kode_transaksi,
                DATE ( a.waktu_order ) AS tanggal_order,
                ( IFNULL( a.total_bayar, 0 )+ IFNULL( a.diskon, 0 ))-( IFNULL( a.ongkir, 0 )- IFNULL( a.diskon_ongkir, 0 )) AS harga_publish, 
                a.diskon, 
                IFNULL( a.total_bayar, 0 )-( IFNULL( a.ongkir, 0 )- IFNULL( a.diskon_ongkir, 0 )) AS harga_jual,
                ( IFNULL( a.total_bayar, 0 )-( IFNULL( a.ongkir, 0 )- IFNULL( a.diskon_ongkir, 0 )))- IFNULL( a.komisi_topsonia, 0 ) AS harga_merchant,
                IFNULL( a.mdr, 0 ) AS mdr_net, IFNULL( a.komisi_topsonia, 0 ) AS topsonia_net,
                a.pajak, a.ongkir, a.diskon_ongkir,
                IFNULL( a.ongkir, 0 )- IFNULL( a.diskon_ongkir, 0 ) AS ongkir_net 
                FROM rb_penjualan a JOIN rb_reseller b ON a.id_penjual = b.id_reseller 
                WHERE a.proses = '1' && a.ORDER = '1' && a.ORDER = '1' && MONTH ( a.waktu_order )= '$bln' && YEAR(a.waktu_order)='$thn'
                ORDER BY a.waktu_order DESC");

            $data['total_perbulan'] = $this->db->query("SELECT
                sum(( IFNULL( total_bayar, 0 )+ IFNULL( diskon, 0 ))-( IFNULL( ongkir, 0 )- IFNULL( diskon_ongkir, 0 ))) AS total_harga_publish,
                sum( diskon ) AS total_diskon,
                sum( IFNULL( total_bayar, 0 )-( IFNULL( ongkir, 0 )- IFNULL( diskon_ongkir, 0 ))) AS total_harga_jual,
                sum(( IFNULL( total_bayar, 0 )-( IFNULL( ongkir, 0 )- IFNULL( diskon_ongkir, 0 )))- IFNULL( komisi_topsonia, 0 )) AS total_harga_merchant,
                sum( IFNULL( komisi_topsonia, 0 )) AS total_topsonia_net,
                sum( IFNULL( mdr, 0 ) ) AS total_total_mdr,
                sum( IFNULL( pajak, 0 )) AS total_total_pajak,
                sum( IFNULL( ongkir, 0 )) AS total_ongkir,
                sum( IFNULL( diskon_ongkir, 0 )) AS total_diskon_ongkir,
                sum( IFNULL( ongkir, 0 )- IFNULL( diskon_ongkir, 0 )) AS total_ongkir_net 
                FROM rb_penjualan WHERE proses = '1' && `order` = '1' && MONTH ( waktu_order )= '$bln' && YEAR(waktu_order)='$thn'")->row_array();
        }


        $data['detail_header'] = $this->db->query("SELECT *, ongkir-IFNULL(diskon_ongkir,0) AS net_ongkir FROM rb_penjualan a JOIN rb_penjualan_detail b ON a.id_penjualan=b.id_penjualan JOIN rb_reseller c ON a.id_penjual = c.id_reseller JOIN rb_konsumen d ON a.id_pembeli=d.id_konsumen JOIN alamat e ON d.id_alamat = e.id_alamat JOIN rb_kota f ON e.kota_id = f.kota_id JOIN rb_provinsi g ON e.provinsi_id = g.provinsi_id");

        $this->template->load('administrator/template','administrator/additional/mod_keuangan/view_finance_per_bulan', $data);
    }

}

function finance(){
    cek_session_akses('keuangan',$this->session->id_session);
    $merchant       = $this->input->post('merchant');
    $pembayaran     = $this->input->post('pembayaran');
    $tanggal_awal   = $this->input->post('tanggal_awal');
    $tanggal_akhir  = $this->input->post('tanggal_akhir');


    if($merchant != "" && $pembayaran != "" && $tanggal_awal != "" && $tanggal_akhir != ""){
        $kondisi1 = "&& b.id_reseller = '$merchant' && a.kategori_pembayaran = '$pembayaran' && DATE_FORMAT(a.waktu_order, '%m/%d/%Y') BETWEEN '$tanggal_awal' AND '$tanggal_akhir'";
        $kondisi2 = "&& id_penjual = '$merchant' && kategori_pembayaran = '$pembayaran' && DATE_FORMAT(waktu_order, '%m/%d/%Y') BETWEEN '$tanggal_awal' AND '$tanggal_akhir'";
    }elseif($merchant != "" && $pembayaran != "" && $tanggal_awal == "" && $tanggal_akhir == ""){
        $kondisi1 = "&& b.id_reseller = '$merchant' && a.kategori_pembayaran = '$pembayaran'";
        $kondisi2 = "&& id_penjual = '$merchant' && kategori_pembayaran = '$pembayaran'";
    }elseif ($merchant != "" && $pembayaran == "" && $tanggal_awal == "" && $tanggal_akhir == "") {
        $kondisi1 = "&& b.id_reseller = '$merchant'";
        $kondisi2 = "&& id_penjual = '$merchant'";
    }elseif($merchant == "" && $pembayaran != "" && $tanggal_awal == "" && $tanggal_akhir == ""){
        $kondisi1 = "&& a.kategori_pembayaran = '$pembayaran'";
        $kondisi2 = "&& kategori_pembayaran = '$pembayaran'";
    }elseif($merchant == "" && $pembayaran == "" && $tanggal_awal != "" && $tanggal_akhir == ""){
        $kondisi1 = "&& DATE_FORMAT(a.waktu_order, '%m/%d/%Y') = '$tanggal_awal'";
        $kondisi2 = "&& DATE_FORMAT(waktu_order, '%m/%d/%Y') = '$tanggal_awal'";
    }elseif($merchant == "" && $pembayaran == "" && $tanggal_awal == "" && $tanggal_akhir != ""){
        $kondisi1 = "&& DATE_FORMAT(a.waktu_order, '%m/%d/%Y') = '$tanggal_akhir'";
        $kondisi2 = "&& DATE_FORMAT(waktu_order, '%m/%d/%Y') = '$tanggal_akhir'";
    }elseif($merchant == "" && $pembayaran == "" && $tanggal_awal != "" && $tanggal_akhir != ""){
        $kondisi1 = "&& DATE_FORMAT(a.waktu_order, '%m/%d/%Y') BETWEEN '$tanggal_awal' AND '$tanggal_akhir'";
        $kondisi2 = "&& DATE_FORMAT(waktu_order, '%m/%d/%Y') BETWEEN '$tanggal_awal' AND '$tanggal_akhir'";
    }elseif($merchant == "" && $pembayaran != "" && $tanggal_awal != "" && $tanggal_akhir != ""){
        $kondisi1 = "&& a.kategori_pembayaran = '$pembayaran' && DATE_FORMAT(a.waktu_order, '%m/%d/%Y') BETWEEN '$tanggal_awal' AND '$tanggal_akhir'";
        $kondisi2 = "&& kategori_pembayaran = '$pembayaran' && DATE_FORMAT(waktu_order, '%m/%d/%Y') BETWEEN '$tanggal_awal' AND '$tanggal_akhir'";
    }elseif($merchant != "" && $pembayaran == "" && $tanggal_awal != "" && $tanggal_akhir != ""){
        $kondisi1 = "&& b.id_reseller = '$merchant' && DATE_FORMAT(a.waktu_order, '%m/%d/%Y') BETWEEN '$tanggal_awal' AND '$tanggal_akhir'";
        $kondisi2 = "&& id_penjual = '$merchant' && DATE_FORMAT(waktu_order, '%m/%d/%Y') BETWEEN '$tanggal_awal' AND '$tanggal_akhir'";
    }elseif($merchant != "" && $pembayaran == "" && $tanggal_awal != "" && $tanggal_akhir == ""){
        $kondisi1 = "&& b.id_reseller = '$merchant' && DATE_FORMAT(a.waktu_order, '%m/%d/%Y') = '$tanggal_awal'";
        $kondisi2 = "&& id_penjual = '$merchant' && DATE_FORMAT(waktu_order, '%m/%d/%Y') = '$tanggal_awal'";
    }elseif($merchant != "" && $pembayaran == "" && $tanggal_awal == "" && $tanggal_akhir != ""){
        $kondisi1 = "&& b.id_reseller = '$merchant' && DATE_FORMAT(a.waktu_order, '%m/%d/%Y') = '$tanggal_akhir'";
        $kondisi2 = "&& id_penjual = '$merchant' && DATE_FORMAT(waktu_order, '%m/%d/%Y') = '$tanggal_akhir''";
    }elseif($merchant == "" && $pembayaran != "" && $tanggal_awal != "" && $tanggal_akhir == ""){
        $kondisi1 = "&& a.kategori_pembayaran = '$pembayaran' && DATE_FORMAT(a.waktu_order, '%m/%d/%Y') = '$tanggal_awal'";
        $kondisi2 = "&& kategori_pembayaran = '$pembayaran' && DATE_FORMAT(waktu_order, '%m/%d/%Y') = '$tanggal_awal'";
    }elseif($merchant == "" && $pembayaran != "" && $tanggal_awal == "" && $tanggal_akhir != ""){
        $kondisi1 = "&& a.kategori_pembayaran = '$pembayaran' && DATE_FORMAT(a.waktu_order, '%m/%d/%Y') = '$tanggal_akhir'";
        $kondisi2 = "&& kategori_pembayaran = '$pembayaran' && DATE_FORMAT(waktu_order, '%m/%d/%Y') = '$tanggal_akhir'";
    }else{
        $kondisi1 =  "";
        $kondisi2 =  "";
    }

    $data['detail_perbulan'] = $this->db->query("SELECT a.kategori_pembayaran, a.id_penjualan, b.id_reseller, a.kode_transaksi, b.nama_reseller, a.diskon, a.pajak, a.ongkir, a.diskon_ongkir,
        DATE ( a.waktu_order ) AS tanggal_order,
        IFNULL( a.total_bayar, 0 )-( IFNULL( a.ongkir, 0 )- IFNULL( a.diskon_ongkir, 0 )) AS harga_publish,
        (IFNULL( a.total_bayar, 0 ) - IFNULL(a.diskon,0))-( IFNULL( a.ongkir, 0 )- IFNULL( a.diskon_ongkir, 0 )) AS harga_jual,
        ( (IFNULL( a.total_bayar, 0 ) - IFNULL(a.diskon,0))-( IFNULL( a.ongkir, 0 )- IFNULL( a.diskon_ongkir, 0 )))- IFNULL( a.komisi_topsonia, 0 ) AS harga_merchant,
        IFNULL( a.mdr, 0 ) AS mdr_net,
        IFNULL( a.komisi_topsonia, 0 )- IFNULL( a.komisi_referal, 0 ) AS topsonia_net,
        IFNULL( a.komisi_referal, 0 ) AS komisi_referal,
        IFNULL( a.ongkir, 0 )- IFNULL( a.diskon_ongkir, 0 ) AS ongkir_net 
        FROM rb_penjualan a
        JOIN rb_reseller b ON a.id_penjual = b.id_reseller 
        WHERE a.proses = '1' && a.`order` = '1' $kondisi1
        ORDER BY tanggal_order DESC");

    $data['total_perbulan'] = $this->db->query("SELECT id_penjual, kategori_pembayaran,
        sum(( IFNULL( total_bayar, 0 ))-( IFNULL( ongkir, 0 )- IFNULL( diskon_ongkir, 0 ))) AS total_harga_publish,
        sum( diskon ) AS total_diskon,
        sum((IFNULL( total_bayar, 0 ) - IFNULL(diskon,0))-( IFNULL( ongkir, 0 )- IFNULL( diskon_ongkir, 0 ))) AS total_harga_jual,
        sum(( (IFNULL( total_bayar, 0 ) - IFNULL(diskon,0))-( IFNULL( ongkir, 0 )- IFNULL( diskon_ongkir, 0 )))- IFNULL( komisi_topsonia, 0 )) AS total_harga_merchant,
        sum( (IFNULL( komisi_topsonia, 0 ) - IFNULL(komisi_referal, 0 ))) AS total_topsonia_net,
        sum( IFNULL( komisi_referal, 0 )) AS total_komisi_referal,
        sum( IFNULL( mdr, 0 ) ) AS total_total_mdr,
        sum( IFNULL( pajak, 0 )) AS total_total_pajak,
        sum( IFNULL( ongkir, 0 )) AS total_ongkir,
        sum( IFNULL( diskon_ongkir, 0 )) AS total_diskon_ongkir,
        sum( IFNULL( ongkir, 0 )- IFNULL( diskon_ongkir, 0 )) AS total_ongkir_net 
        FROM rb_penjualan 
        WHERE proses = '1' && `order` = '1' $kondisi2")->row_array();

    $data['merchant']       = $merchant;
    $data['pembayaran']     = $pembayaran;
    $data['tanggal_awal']   = $tanggal_awal;
    $data['tanggal_akhir']  = $tanggal_akhir;

    $this->template->load('administrator/template','administrator/additional/mod_keuangan/view_finance', $data);
}

function finance_excel(){
    cek_session_akses('keuangan',$this->session->id_session);
    $merchant       = $this->input->get('merchant');
    $pembayaran     = $this->input->get('pembayaran');
    $tanggal_awal   = $this->input->get('tanggal_awal');
    $tanggal_akhir  = $this->input->get('tanggal_akhir');

    if($merchant != "" && $pembayaran != "" && $tanggal_awal != "" && $tanggal_akhir != ""){
        $kondisi1 = "&& b.id_reseller = '$merchant' && a.kategori_pembayaran = '$pembayaran' && DATE_FORMAT(a.waktu_order, '%m/%d/%Y') BETWEEN '$tanggal_awal' AND '$tanggal_akhir'";
        $kondisi2 = "&& id_penjual = '$merchant' && kategori_pembayaran = '$pembayaran' && DATE_FORMAT(waktu_order, '%m/%d/%Y') BETWEEN '$tanggal_awal' AND '$tanggal_akhir'";
    }elseif($merchant != "" && $pembayaran != "" && $tanggal_awal == "" && $tanggal_akhir == ""){
        $kondisi1 = "&& b.id_reseller = '$merchant' && a.kategori_pembayaran = '$pembayaran'";
        $kondisi2 = "&& id_penjual = '$merchant' && kategori_pembayaran = '$pembayaran'";
    }elseif ($merchant != "" && $pembayaran == "" && $tanggal_awal == "" && $tanggal_akhir == "") {
        $kondisi1 = "&& b.id_reseller = '$merchant'";
        $kondisi2 = "&& id_penjual = '$merchant'";
    }elseif($merchant == "" && $pembayaran != "" && $tanggal_awal == "" && $tanggal_akhir == ""){
        $kondisi1 = "&& a.kategori_pembayaran = '$pembayaran'";
        $kondisi2 = "&& kategori_pembayaran = '$pembayaran'";
    }elseif($merchant == "" && $pembayaran == "" && $tanggal_awal != "" && $tanggal_akhir == ""){
        $kondisi1 = "&& DATE_FORMAT(a.waktu_order, '%m/%d/%Y') = '$tanggal_awal'";
        $kondisi2 = "&& DATE_FORMAT(waktu_order, '%m/%d/%Y') = '$tanggal_awal'";
    }elseif($merchant == "" && $pembayaran == "" && $tanggal_awal == "" && $tanggal_akhir != ""){
        $kondisi1 = "&& DATE_FORMAT(a.waktu_order, '%m/%d/%Y') = '$tanggal_akhir'";
        $kondisi2 = "&& DATE_FORMAT(waktu_order, '%m/%d/%Y') = '$tanggal_akhir'";
    }elseif($merchant == "" && $pembayaran == "" && $tanggal_awal != "" && $tanggal_akhir != ""){
        $kondisi1 = "&& DATE_FORMAT(a.waktu_order, '%m/%d/%Y') BETWEEN '$tanggal_awal' AND '$tanggal_akhir'";
        $kondisi2 = "&& DATE_FORMAT(waktu_order, '%m/%d/%Y') BETWEEN '$tanggal_awal' AND '$tanggal_akhir'";
    }elseif($merchant == "" && $pembayaran != "" && $tanggal_awal != "" && $tanggal_akhir != ""){
        $kondisi1 = "&& a.kategori_pembayaran = '$pembayaran' && DATE_FORMAT(a.waktu_order, '%m/%d/%Y') BETWEEN '$tanggal_awal' AND '$tanggal_akhir'";
        $kondisi2 = "&& kategori_pembayaran = '$pembayaran' && DATE_FORMAT(waktu_order, '%m/%d/%Y') BETWEEN '$tanggal_awal' AND '$tanggal_akhir'";
    }elseif($merchant != "" && $pembayaran == "" && $tanggal_awal != "" && $tanggal_akhir != ""){
        $kondisi1 = "&& b.id_reseller = '$merchant' && DATE_FORMAT(a.waktu_order, '%m/%d/%Y') BETWEEN '$tanggal_awal' AND '$tanggal_akhir'";
        $kondisi2 = "&& id_penjual = '$merchant' && DATE_FORMAT(waktu_order, '%m/%d/%Y') BETWEEN '$tanggal_awal' AND '$tanggal_akhir'";
    }elseif($merchant != "" && $pembayaran == "" && $tanggal_awal != "" && $tanggal_akhir == ""){
        $kondisi1 = "&& b.id_reseller = '$merchant' && DATE_FORMAT(a.waktu_order, '%m/%d/%Y') = '$tanggal_awal'";
        $kondisi2 = "&& id_penjual = '$merchant' && DATE_FORMAT(waktu_order, '%m/%d/%Y') = '$tanggal_awal'";
    }elseif($merchant != "" && $pembayaran == "" && $tanggal_awal == "" && $tanggal_akhir != ""){
        $kondisi1 = "&& b.id_reseller = '$merchant' && DATE_FORMAT(a.waktu_order, '%m/%d/%Y') = '$tanggal_akhir'";
        $kondisi2 = "&& id_penjual = '$merchant' && DATE_FORMAT(waktu_order, '%m/%d/%Y') = '$tanggal_akhir''";
    }elseif($merchant == "" && $pembayaran != "" && $tanggal_awal != "" && $tanggal_akhir == ""){
        $kondisi1 = "&& a.kategori_pembayaran = '$pembayaran' && DATE_FORMAT(a.waktu_order, '%m/%d/%Y') = '$tanggal_awal'";
        $kondisi2 = "&& kategori_pembayaran = '$pembayaran' && DATE_FORMAT(waktu_order, '%m/%d/%Y') = '$tanggal_awal'";
    }elseif($merchant == "" && $pembayaran != "" && $tanggal_awal == "" && $tanggal_akhir != ""){
        $kondisi1 = "&& a.kategori_pembayaran = '$pembayaran' && DATE_FORMAT(a.waktu_order, '%m/%d/%Y') = '$tanggal_akhir'";
        $kondisi2 = "&& kategori_pembayaran = '$pembayaran' && DATE_FORMAT(waktu_order, '%m/%d/%Y') = '$tanggal_akhir'";
    }else{
        $kondisi1 =  "";
        $kondisi2 =  "";
    }

    $data['detail_perbulan'] = $this->db->query("SELECT a.kategori_pembayaran, a.id_penjualan, b.id_reseller, a.kode_transaksi, b.nama_reseller, a.diskon, a.pajak, a.ongkir, a.diskon_ongkir,
        DATE ( a.waktu_order ) AS tanggal_order,
        IFNULL( a.total_bayar, 0 )-( IFNULL( a.ongkir, 0 )- IFNULL( a.diskon_ongkir, 0 )) AS harga_publish,
        (IFNULL( a.total_bayar, 0 ) - IFNULL(a.diskon,0))-( IFNULL( a.ongkir, 0 )- IFNULL( a.diskon_ongkir, 0 )) AS harga_jual,
        ( (IFNULL( a.total_bayar, 0 ) - IFNULL(a.diskon,0))-( IFNULL( a.ongkir, 0 )- IFNULL( a.diskon_ongkir, 0 )))- IFNULL( a.komisi_topsonia, 0 ) AS harga_merchant,
        IFNULL( a.mdr, 0 ) AS mdr_net,
        IFNULL( a.komisi_topsonia, 0 )- IFNULL( a.komisi_referal, 0 ) AS topsonia_net,
        IFNULL( a.komisi_referal, 0 ) AS komisi_referal,
        IFNULL( a.ongkir, 0 )- IFNULL( a.diskon_ongkir, 0 ) AS ongkir_net 
        FROM rb_penjualan a
        JOIN rb_reseller b ON a.id_penjual = b.id_reseller 
        WHERE a.proses = '1' && a.`order` = '1' $kondisi1
        ORDER BY tanggal_order DESC");

    $data['total_perbulan'] = $this->db->query("SELECT id_penjual, kategori_pembayaran,
        sum(( IFNULL( total_bayar, 0 ))-( IFNULL( ongkir, 0 )- IFNULL( diskon_ongkir, 0 ))) AS total_harga_publish,
        sum( diskon ) AS total_diskon,
        sum((IFNULL( total_bayar, 0 ) - IFNULL(diskon,0))-( IFNULL( ongkir, 0 )- IFNULL( diskon_ongkir, 0 ))) AS total_harga_jual,
        sum(( (IFNULL( total_bayar, 0 ) - IFNULL(diskon,0))-( IFNULL( ongkir, 0 )- IFNULL( diskon_ongkir, 0 )))- IFNULL( komisi_topsonia, 0 )) AS total_harga_merchant,
        sum( (IFNULL( komisi_topsonia, 0 ) - IFNULL(komisi_referal, 0 ))) AS total_topsonia_net,
        sum( IFNULL( komisi_referal, 0 )) AS total_komisi_referal,
        sum( IFNULL( mdr, 0 ) ) AS total_total_mdr,
        sum( IFNULL( pajak, 0 )) AS total_total_pajak,
        sum( IFNULL( ongkir, 0 )) AS total_ongkir,
        sum( IFNULL( diskon_ongkir, 0 )) AS total_diskon_ongkir,
        sum( IFNULL( ongkir, 0 )- IFNULL( diskon_ongkir, 0 )) AS total_ongkir_net 
        FROM rb_penjualan 
        WHERE proses = '1' && `order` = '1' $kondisi2")->row_array();

    $this->load->view('administrator/additional/mod_keuangan/export_excel', $data);
}


public function finance_pdf(){
    cek_session_akses('keuangan',$this->session->id_session);
    ob_start();

    $bln = $this->input->get('bln');
    $reseler = $this->input->get('reseler');

    if ($reseler != '') {
        $data['bulan'] = $this->db->query("SELECT *, MONTH (waktu_order) AS bulan FROM rb_penjualan WHERE proses='1' && `order`='1' && MONTH(waktu_order)='$bln'")->row_array();

        if ($reseler == 0) {

            $data['detail_perbulan_merchant'] = $this->db->query("SELECT a.id_penjualan AS id_penjualan, b.id_reseller AS id_reseller, a.kode_transaksi AS kode_transaksi, b.nama_reseller AS nama_reseller,
                DATE ( a.waktu_order ) AS tanggal_order,
                sum( IFNULL( d.harga_jual, 0 ) * IFNULL( d.jumlah, 0 ) ) AS harga_awal,
                sum( IFNULL( d.diskon_merchant, 0 ) ) AS diskaun,
                ( sum( IFNULL( d.harga_jual, 0 ) * IFNULL( d.jumlah, 0 ) ) ) - ( sum( IFNULL( d.diskon_merchant, 0 ) ) ) AS harga_fix,
                ( sum( IFNULL( d.harga_jual, 0 ) * IFNULL( d.jumlah, 0 ) ) ) - ( sum( IFNULL( d.diskon_merchant, 0 ) ) ) - ( IFNULL( a.komisi_topsonia, 0 ) ) AS komisi_merchant,
                IFNULL( a.mdr, 0 ) AS mdr_net,
                sum(IFNULL( d.diskon_topsonia, 0 )) AS topsonia_gross,
                IFNULL( a.pajak, 0 ) AS pajak,
                IFNULL( a.ongkir, 0 ) AS sub_ongkir,
                IFNULL( a.diskon_ongkir, 0 ) AS diskon_ongkir,
                IFNULL( a.ongkir, 0 ) - IFNULL( a.diskon_ongkir, 0 ) AS sub_ongkir_net,
                IFNULL( a.asuransi, 0 ) AS asurans
                FROM rb_penjualan a
                JOIN rb_reseller b ON a.id_penjual = b.id_reseller

                JOIN rb_penjualan_detail d ON a.id_penjualan = d.id_penjualan 
                WHERE a.proses = '1' && a.`order` = '1' && MONTH ( a.waktu_order ) = '$bln'
                GROUP BY a.id_penjualan ORDER BY tanggal_order DESC");

            $data['total_perbulan_merchant'] = $this->db->query("SELECT bln, 
                sum(sub_harga_publish) AS total_harga_publish, 
                sum(diskon) AS total_diskon, 
                sum(harga_jual) AS total_harga_jual, 
                sum(harga_merchant) AS total_harga_merchant,
                sum(mdr_net) AS total_total_mdr,
                sum(pajak) AS total_total_pajak,
                sum(topsonia_net) AS total_topsonia_net, 
                sum(ongkir) AS total_ongkir, 
                sum(subtotal_diskon_ongkir) AS total_diskon_ongkir, 
                sum(ongkir_net) AS total_ongkir_net,
                sum(asuran) AS total_asuransi
                FROM (SELECT id_penjualan, id_reseller, kode_transaksi, nama_reseller, tanggal_order, bln,
                sum(harga_awal) AS sub_harga_publish,
                sum(diskaun) AS diskon,
                sum(harga_fix) AS harga_jual,
                sum(komisi_merchant) AS harga_merchant,
                mdr_net, pajak,
                sum(topsonia_gross) AS topsonia_net,
                sum(sub_ongkir) AS ongkir,
                sum(diskon_ongkir) AS subtotal_diskon_ongkir,
                sum(sub_ongkir_net) AS ongkir_net,
                sum(asurans) AS asuran
                FROM (SELECT a.id_penjualan AS id_penjualan, b.id_reseller AS id_reseller, a.kode_transaksi AS kode_transaksi, b.nama_reseller AS nama_reseller,
                DATE ( a.waktu_order ) AS tanggal_order,
                MONTH ( a.waktu_order ) AS bln,
                sum( IFNULL( d.harga_jual, 0 ) * IFNULL( d.jumlah, 0 ) ) AS harga_awal,
                sum( IFNULL( d.diskon_merchant, 0 ) ) AS diskaun,
                ( sum( IFNULL( d.harga_jual, 0 ) * IFNULL( d.jumlah, 0 ) ) ) - ( sum( IFNULL( d.diskon_merchant, 0 ) ) ) AS harga_fix,
                ( sum( IFNULL( d.harga_jual, 0 ) * IFNULL( d.jumlah, 0 ) ) ) - ( sum( IFNULL( d.diskon_merchant, 0 ) ) ) - ( IFNULL( a.komisi_topsonia, 0 ) ) AS komisi_merchant,
                IFNULL( a.mdr, 0 ) AS mdr_net,
                sum(IFNULL( d.diskon_topsonia, 0 )) AS topsonia_gross,
                IFNULL( a.pajak, 0 ) AS pajak,
                IFNULL( a.ongkir, 0 ) AS sub_ongkir,
                IFNULL( a.diskon_ongkir, 0 ) AS diskon_ongkir,
                IFNULL( a.ongkir, 0 ) - IFNULL( a.diskon_ongkir, 0 ) AS sub_ongkir_net,
                IFNULL( a.asuransi, 0 ) AS asurans 
                FROM rb_penjualan a
                JOIN rb_reseller b ON a.id_penjual = b.id_reseller

                JOIN rb_penjualan_detail d ON a.id_penjualan = d.id_penjualan 
                WHERE a.proses = '1' && a.`order` = '1' && MONTH ( a.waktu_order ) = '$bln'
                GROUP BY a.id_penjualan) AS myTable
                GROUP BY id_reseller) AS myTable2
                GROUP BY bln")->row_array();

        }else{
            $data['detail_perbulan_merchant'] = $this->db->query("SELECT a.id_penjualan AS id_penjualan, b.id_reseller AS id_reseller, a.kode_transaksi AS kode_transaksi, b.nama_reseller AS nama_reseller,
                DATE ( a.waktu_order ) AS tanggal_order,
                sum( IFNULL( d.harga_jual, 0 ) * IFNULL( d.jumlah, 0 ) ) AS harga_awal,
                sum( IFNULL( d.diskon_merchant, 0 ) ) AS diskaun,
                ( sum( IFNULL( d.harga_jual, 0 ) * IFNULL( d.jumlah, 0 ) ) ) - ( sum( IFNULL( d.diskon_merchant, 0 ) ) ) AS harga_fix,
                ( sum( IFNULL( d.harga_jual, 0 ) * IFNULL( d.jumlah, 0 ) ) ) - ( sum( IFNULL( d.diskon_merchant, 0 ) ) ) - ( IFNULL( a.komisi_topsonia, 0 ) ) AS komisi_merchant,
                IFNULL( a.mdr, 0 ) AS mdr_net,
                sum(IFNULL( d.diskon_topsonia, 0 )) AS topsonia_gross,
                IFNULL( a.pajak, 0 ) AS pajak,
                IFNULL( a.ongkir, 0 ) AS sub_ongkir,
                IFNULL( a.diskon_ongkir, 0 ) AS diskon_ongkir,
                IFNULL( a.ongkir, 0 ) - IFNULL( a.diskon_ongkir, 0 ) AS sub_ongkir_net,
                IFNULL( a.asuransi, 0 ) AS asurans
                FROM rb_penjualan a
                JOIN rb_reseller b ON a.id_penjual = b.id_reseller

                JOIN rb_penjualan_detail d ON a.id_penjualan = d.id_penjualan 
                WHERE a.proses = '1' && a.`order` = '1' && MONTH ( a.waktu_order ) = '$bln' && id_reseller = '$reseler'
                GROUP BY a.id_penjualan ORDER BY tanggal_order DESC");

            $data['total_perbulan_merchant'] = $this->db->query("SELECT bln, 
                sum(sub_harga_publish) AS total_harga_publish, 
                sum(diskon) AS total_diskon, 
                sum(harga_jual) AS total_harga_jual, 
                sum(harga_merchant) AS total_harga_merchant,
                sum(mdr_net) AS total_total_mdr,
                sum(pajak) AS total_total_pajak,
                sum(topsonia_net) AS total_topsonia_net, 
                sum(ongkir) AS total_ongkir, 
                sum(subtotal_diskon_ongkir) AS total_diskon_ongkir, 
                sum(ongkir_net) AS total_ongkir_net,
                sum(asuran) AS total_asuransi
                FROM
                (SELECT id_penjualan, id_reseller, kode_transaksi, nama_reseller, tanggal_order, bln,
                sum(harga_awal) AS sub_harga_publish,
                sum(diskaun) AS diskon,
                sum(harga_fix) AS harga_jual,
                sum(komisi_merchant) AS harga_merchant,
                mdr_net, pajak,
                sum(topsonia_gross) AS topsonia_net,
                sum(sub_ongkir) AS ongkir,
                sum(diskon_ongkir) AS subtotal_diskon_ongkir,
                sum(sub_ongkir_net) AS ongkir_net,
                sum(asurans) AS asuran
                FROM (SELECT a.id_penjualan AS id_penjualan, b.id_reseller AS id_reseller, a.kode_transaksi AS kode_transaksi, b.nama_reseller AS nama_reseller,
                DATE ( a.waktu_order ) AS tanggal_order,
                MONTH ( a.waktu_order ) AS bln,
                sum( IFNULL( d.harga_jual, 0 ) * IFNULL( d.jumlah, 0 ) ) AS harga_awal,
                sum( IFNULL( d.diskon_merchant, 0 ) ) AS diskaun,
                ( sum( IFNULL( d.harga_jual, 0 ) * IFNULL( d.jumlah, 0 ) ) ) - ( sum( IFNULL( d.diskon_merchant, 0 ) ) ) AS harga_fix,
                ( sum( IFNULL( d.harga_jual, 0 ) * IFNULL( d.jumlah, 0 ) ) ) - ( sum( IFNULL( d.diskon_merchant, 0 ) ) ) - ( IFNULL( a.komisi_topsonia, 0 ) ) AS komisi_merchant,
                IFNULL( a.mdr, 0 ) AS mdr_net,
                sum(IFNULL( d.diskon_topsonia, 0 )) AS topsonia_gross,
                IFNULL( a.pajak, 0 ) AS pajak,
                IFNULL( a.ongkir, 0 ) AS sub_ongkir,
                IFNULL( a.diskon_ongkir, 0 ) AS diskon_ongkir,
                IFNULL( a.ongkir, 0 ) - IFNULL( a.diskon_ongkir, 0 ) AS sub_ongkir_net,
                IFNULL( a.asuransi, 0 ) AS asurans
                FROM rb_penjualan a
                JOIN rb_reseller b ON a.id_penjual = b.id_reseller

                JOIN rb_penjualan_detail d ON a.id_penjualan = d.id_penjualan 
                WHERE a.proses = '1' && a.`order` = '1' && MONTH ( a.waktu_order ) = '$bln' && a.id_penjual = '$reseler'
                GROUP BY a.id_penjualan) AS myTable
                GROUP BY id_reseller) AS myTable2
                GROUP BY bln")->row_array();
        }

        $data['detail_perbulan'] = $this->db->query("SELECT id_penjualan, id_reseller, kode_transaksi, nama_reseller, tanggal_order,
            sum(harga_awal) AS harga_publish,
            sum(diskaun) AS diskon,
            sum(harga_fix) AS harga_jual,
            sum(komisi_merchant) AS harga_merchant,
            mdr_net, pajak,
            sum(topsonia_gross) AS topsonia_net,
            sum(sub_ongkir) AS ongkir,
            sum(diskon_ongkir) AS total_diskon_ongkir,
            sum(sub_ongkir_net) AS ongkir_net,
            sum(asuransi) AS t_asuransi
            FROM (SELECT a.id_penjualan AS id_penjualan, b.id_reseller AS id_reseller, a.kode_transaksi AS kode_transaksi, b.nama_reseller AS nama_reseller,
            DATE ( a.waktu_order ) AS tanggal_order,
            sum( IFNULL( d.harga_jual, 0 ) * IFNULL( d.jumlah, 0 ) ) AS harga_awal,
            sum( IFNULL( d.diskon_merchant, 0 ) ) AS diskaun,
            ( sum( IFNULL( d.harga_jual, 0 ) * IFNULL( d.jumlah, 0 ) ) ) - ( sum( IFNULL( d.diskon_merchant, 0 ) ) ) AS harga_fix,
            ( sum( IFNULL( d.harga_jual, 0 ) * IFNULL( d.jumlah, 0 ) ) ) - ( sum( IFNULL( d.diskon_merchant, 0 ) ) ) - ( IFNULL( a.komisi_topsonia, 0 ) ) AS komisi_merchant,
            IFNULL( a.mdr, 0 ) AS mdr_net,
            sum(IFNULL( d.diskon_topsonia, 0 )) AS topsonia_gross,
            IFNULL( a.pajak, 0 ) AS pajak,
            IFNULL( a.ongkir, 0 ) AS sub_ongkir,
            IFNULL( a.diskon_ongkir, 0 ) AS diskon_ongkir,
            IFNULL( a.ongkir, 0 ) - IFNULL( a.diskon_ongkir, 0 ) AS sub_ongkir_net,
            IFNULL( a.asuransi, 0 ) AS asuransi
            FROM rb_penjualan a
            JOIN rb_reseller b ON a.id_penjual = b.id_reseller
            JOIN rb_penjualan_detail d ON a.id_penjualan = d.id_penjualan 
            WHERE a.proses = '1' && a.`order` = '1' && MONTH ( a.waktu_order ) = '$bln' 
            GROUP BY a.id_penjualan ASC) AS myTable
            GROUP BY kode_transaksi ORDER BY tanggal_order DESC");

        $data['total_perbulan'] = $this->db->query("SELECT bln, 
            sum(sub_harga_publish) AS total_harga_publish, 
            sum(diskon) AS total_diskon, 
            sum(harga_jual) AS total_harga_jual, 
            sum(harga_merchant) AS total_harga_merchant,
            sum(mdr_net) AS total_total_mdr,
            sum(pajak) AS total_total_pajak,
            sum(topsonia_net) AS total_topsonia_net, 
            sum(ongkir) AS total_ongkir, 
            sum(subtotal_diskon_ongkir) AS total_diskon_ongkir, 
            sum(ongkir_net) AS total_ongkir_net,
            sum(asurans) AS total_asuransi
            FROM (SELECT id_penjualan, id_reseller, kode_transaksi, nama_reseller, tanggal_order, bln,
            sum(harga_awal) AS sub_harga_publish,
            sum(diskaun) AS diskon,
            sum(harga_fix) AS harga_jual,
            sum(komisi_merchant) AS harga_merchant,
            mdr_net, pajak,
            sum(topsonia_gross) AS topsonia_net,
            sum(sub_ongkir) AS ongkir,
            sum(diskon_ongkir) AS subtotal_diskon_ongkir,
            sum(sub_ongkir_net) AS ongkir_net,
            sum(asuransi) AS asurans
            FROM (SELECT a.id_penjualan AS id_penjualan, b.id_reseller AS id_reseller, a.kode_transaksi AS kode_transaksi, b.nama_reseller AS nama_reseller,
            DATE ( a.waktu_order ) AS tanggal_order,
            MONTH ( a.waktu_order ) AS bln,
            sum( IFNULL( d.harga_jual, 0 ) * IFNULL( d.jumlah, 0 ) ) AS harga_awal,
            sum( IFNULL( d.diskon_merchant, 0 ) ) AS diskaun,
            ( sum( IFNULL( d.harga_jual, 0 ) * IFNULL( d.jumlah, 0 ) ) ) - ( sum( IFNULL( d.diskon_merchant, 0 ) ) ) AS harga_fix,
            ( sum( IFNULL( d.harga_jual, 0 ) * IFNULL( d.jumlah, 0 ) ) ) - ( sum( IFNULL( d.diskon_merchant, 0 ) ) ) - ( IFNULL( a.komisi_topsonia, 0 ) ) AS komisi_merchant,
            IFNULL( a.mdr, 0 ) AS mdr_net,
            sum(IFNULL( d.diskon_topsonia, 0 )) AS topsonia_gross,
            IFNULL( a.pajak, 0 ) AS pajak,
            IFNULL( a.ongkir, 0 ) AS sub_ongkir,
            IFNULL( a.diskon_ongkir, 0 ) AS diskon_ongkir,
            IFNULL( a.ongkir, 0 ) - IFNULL( a.diskon_ongkir, 0 ) AS sub_ongkir_net,
            IFNULL( a.asuransi, 0 ) AS asuransi
            FROM rb_penjualan a
            JOIN rb_reseller b ON a.id_penjual = b.id_reseller
            JOIN rb_penjualan_detail d ON a.id_penjualan = d.id_penjualan 
            WHERE a.proses = '1' && a.`order` = '1' && MONTH ( a.waktu_order ) = '$bln'
            GROUP BY a.id_penjualan ASC) AS myTable
            GROUP BY kode_transaksi) AS myTable2
            GROUP BY bln")->row_array();


        $data['resel'] = $this->db->query("SELECT * FROM rb_reseller where id_reseller='$reseler'")->row_array();
        $data['id_r']  = $reseler;

    }else{
        $data['bulan'] = $this->db->query("SELECT *, MONTH (waktu_order) AS bulan FROM rb_penjualan WHERE proses='1' && `order`='1' && MONTH(waktu_order)='$bln'")->row_array();

        $data['detail_perbulan_merchant'] = $this->db->query("SELECT a.id_penjualan AS id_penjualan, b.id_reseller AS id_reseller, a.kode_transaksi AS kode_transaksi, b.nama_reseller AS nama_reseller,
            DATE ( a.waktu_order ) AS tanggal_order,
            sum( IFNULL( d.harga_jual, 0 ) * IFNULL( d.jumlah, 0 ) ) AS harga_awal,
            sum( IFNULL( d.diskon_merchant, 0 ) ) AS diskaun,
            ( sum( IFNULL( d.harga_jual, 0 ) * IFNULL( d.jumlah, 0 ) ) ) - ( sum( IFNULL( d.diskon_merchant, 0 ) ) ) AS harga_fix,
            ( sum( IFNULL( d.harga_jual, 0 ) * IFNULL( d.jumlah, 0 ) ) ) - ( sum( IFNULL( d.diskon_merchant, 0 ) ) ) - ( IFNULL( a.komisi_topsonia, 0 ) ) AS komisi_merchant,
            IFNULL( a.mdr, 0 ) AS mdr_net,
            sum(IFNULL( d.diskon_topsonia, 0 )) AS topsonia_gross,
            IFNULL( a.pajak, 0 ) AS pajak,
            IFNULL( a.ongkir, 0 ) AS sub_ongkir,
            IFNULL( a.diskon_ongkir, 0 ) AS diskon_ongkir,
            IFNULL( a.ongkir, 0 ) - IFNULL( a.diskon_ongkir, 0 ) AS sub_ongkir_net,
            IFNULL( a.asuransi, 0 ) AS asurans
            FROM rb_penjualan a
            JOIN rb_reseller b ON a.id_penjual = b.id_reseller
            JOIN rb_penjualan_detail d ON a.id_penjualan = d.id_penjualan 
            WHERE a.proses = '1' && a.`order` = '1' && MONTH ( a.waktu_order ) = '$bln'
            GROUP BY a.id_penjualan ORDER BY tanggal_order DESC");

        $data['total_perbulan_merchant'] = $this->db->query("SELECT bln, 
            sum(sub_harga_publish) AS total_harga_publish, 
            sum(diskon) AS total_diskon, 
            sum(harga_jual) AS total_harga_jual, 
            sum(harga_merchant) AS total_harga_merchant,
            sum(mdr_net) AS total_total_mdr,
            sum(pajak) AS total_total_pajak,
            sum(topsonia_net) AS total_topsonia_net, 
            sum(ongkir) AS total_ongkir, 
            sum(subtotal_diskon_ongkir) AS total_diskon_ongkir, 
            sum(ongkir_net) AS total_ongkir_net,
            sum(asuran) AS total_asuransi
            FROM (SELECT id_penjualan, id_reseller, kode_transaksi, nama_reseller, tanggal_order, bln,
            sum(harga_awal) AS sub_harga_publish,
            sum(diskaun) AS diskon,
            sum(harga_fix) AS harga_jual,
            sum(komisi_merchant) AS harga_merchant,
            mdr_net, pajak,
            sum(topsonia_gross) AS topsonia_net,
            sum(sub_ongkir) AS ongkir,
            sum(diskon_ongkir) AS subtotal_diskon_ongkir,
            sum(sub_ongkir_net) AS ongkir_net,
            sum(asurans) AS asuran
            FROM (SELECT a.id_penjualan AS id_penjualan, b.id_reseller AS id_reseller, a.kode_transaksi AS kode_transaksi, b.nama_reseller AS nama_reseller,
            DATE ( a.waktu_order ) AS tanggal_order,
            MONTH ( a.waktu_order ) AS bln,
            sum( IFNULL( d.harga_jual, 0 ) * IFNULL( d.jumlah, 0 ) ) AS harga_awal,
            sum( IFNULL( d.diskon_merchant, 0 ) ) AS diskaun,
            ( sum( IFNULL( d.harga_jual, 0 ) * IFNULL( d.jumlah, 0 ) ) ) - ( sum( IFNULL( d.diskon_merchant, 0 ) ) ) AS harga_fix,
            ( sum( IFNULL( d.harga_jual, 0 ) * IFNULL( d.jumlah, 0 ) ) ) - ( sum( IFNULL( d.diskon_merchant, 0 ) ) ) - ( IFNULL( a.komisi_topsonia, 0 ) ) AS komisi_merchant,
            IFNULL( a.mdr, 0 ) AS mdr_net,
            sum(IFNULL( d.diskon_topsonia, 0 )) AS topsonia_gross,
            IFNULL( a.pajak, 0 ) AS pajak,
            IFNULL( a.ongkir, 0 ) AS sub_ongkir,
            IFNULL( a.diskon_ongkir, 0 ) AS diskon_ongkir,
            IFNULL( a.ongkir, 0 ) - IFNULL( a.diskon_ongkir, 0 ) AS sub_ongkir_net,
            IFNULL( a.asuransi, 0 ) AS asurans 
            FROM rb_penjualan a
            JOIN rb_reseller b ON a.id_penjual = b.id_reseller
            JOIN rb_penjualan_detail d ON a.id_penjualan = d.id_penjualan 
            WHERE a.proses = '1' && a.`order` = '1' && MONTH ( a.waktu_order ) = '$bln'
            GROUP BY a.id_penjualan) AS myTable
            GROUP BY id_reseller) AS myTable2
            GROUP BY bln")->row_array();

        $data['detail_perbulan'] = $this->db->query("SELECT id_penjualan, id_reseller, kode_transaksi, nama_reseller, tanggal_order,
            sum(harga_awal) AS harga_publish,
            sum(diskaun) AS diskon,
            sum(harga_fix) AS harga_jual,
            sum(komisi_merchant) AS harga_merchant,
            mdr_net, pajak,
            sum(topsonia_gross) AS topsonia_net,
            sum(sub_ongkir) AS ongkir,
            sum(diskon_ongkir) AS total_diskon_ongkir,
            sum(sub_ongkir_net) AS ongkir_net,
            sum(asuransi) AS t_asuransi
            FROM (SELECT a.id_penjualan AS id_penjualan, b.id_reseller AS id_reseller, a.kode_transaksi AS kode_transaksi, b.nama_reseller AS nama_reseller,
            DATE ( a.waktu_order ) AS tanggal_order,
            sum( IFNULL( d.harga_jual, 0 ) * IFNULL( d.jumlah, 0 ) ) AS harga_awal,
            sum( IFNULL( d.diskon_merchant, 0 ) ) AS diskaun,
            ( sum( IFNULL( d.harga_jual, 0 ) * IFNULL( d.jumlah, 0 ) ) ) - ( sum( IFNULL( d.diskon_merchant, 0 ) ) ) AS harga_fix,
            ( sum( IFNULL( d.harga_jual, 0 ) * IFNULL( d.jumlah, 0 ) ) ) - ( sum( IFNULL( d.diskon_merchant, 0 ) ) ) - ( IFNULL( a.komisi_topsonia, 0 ) ) AS komisi_merchant,
            IFNULL( a.mdr, 0 ) AS mdr_net,
            sum(IFNULL( d.diskon_topsonia, 0 )) AS topsonia_gross,
            IFNULL( a.pajak, 0 ) AS pajak,
            IFNULL( a.ongkir, 0 ) AS sub_ongkir,
            IFNULL( a.diskon_ongkir, 0 ) AS diskon_ongkir,
            IFNULL( a.ongkir, 0 ) - IFNULL( a.diskon_ongkir, 0 ) AS sub_ongkir_net,
            IFNULL( a.asuransi, 0 ) AS asuransi
            FROM rb_penjualan a
            JOIN rb_reseller b ON a.id_penjual = b.id_reseller
            JOIN rb_penjualan_detail d ON a.id_penjualan = d.id_penjualan 
            WHERE a.proses = '1' && a.`order` = '1' && MONTH ( a.waktu_order ) = '$bln' 
            GROUP BY a.id_penjualan ASC) AS myTable
            GROUP BY kode_transaksi ORDER BY tanggal_order DESC");

        $data['total_perbulan'] = $this->db->query("SELECT bln, 
            sum(sub_harga_publish) AS total_harga_publish, 
            sum(diskon) AS total_diskon, 
            sum(harga_jual) AS total_harga_jual, 
            sum(harga_merchant) AS total_harga_merchant,
            sum(mdr_net) AS total_total_mdr,
            sum(pajak) AS total_total_pajak,
            sum(topsonia_net) AS total_topsonia_net, 
            sum(ongkir) AS total_ongkir, 
            sum(subtotal_diskon_ongkir) AS total_diskon_ongkir, 
            sum(ongkir_net) AS total_ongkir_net,
            sum(asurans) AS total_asuransi
            FROM
            (SELECT id_penjualan, id_reseller, kode_transaksi, nama_reseller, tanggal_order, bln,
            sum(harga_awal) AS sub_harga_publish,
            sum(diskaun) AS diskon,
            sum(harga_fix) AS harga_jual,
            sum(komisi_merchant) AS harga_merchant,
            mdr_net, pajak,
            sum(topsonia_gross) AS topsonia_net,
            sum(sub_ongkir) AS ongkir,
            sum(diskon_ongkir) AS subtotal_diskon_ongkir,
            sum(sub_ongkir_net) AS ongkir_net,
            sum(asuransi) AS asurans
            FROM (SELECT a.id_penjualan AS id_penjualan, b.id_reseller AS id_reseller, a.kode_transaksi AS kode_transaksi, b.nama_reseller AS nama_reseller,
            DATE ( a.waktu_order ) AS tanggal_order,
            MONTH ( a.waktu_order ) AS bln,
            sum( IFNULL( d.harga_jual, 0 ) * IFNULL( d.jumlah, 0 ) ) AS harga_awal,
            sum( IFNULL( d.diskon_merchant, 0 ) ) AS diskaun,
            ( sum( IFNULL( d.harga_jual, 0 ) * IFNULL( d.jumlah, 0 ) ) ) - ( sum( IFNULL( d.diskon_merchant, 0 ) ) ) AS harga_fix,
            ( sum( IFNULL( d.harga_jual, 0 ) * IFNULL( d.jumlah, 0 ) ) ) - ( sum( IFNULL( d.diskon_merchant, 0 ) ) ) - ( IFNULL( a.komisi_topsonia, 0 ) ) AS komisi_merchant,
            IFNULL( a.mdr, 0 ) AS mdr_net,
            sum(IFNULL( d.diskon_topsonia, 0 )) AS topsonia_gross,
            IFNULL( a.pajak, 0 ) AS pajak,
            IFNULL( a.ongkir, 0 ) AS sub_ongkir,
            IFNULL( a.diskon_ongkir, 0 ) AS diskon_ongkir,
            IFNULL( a.ongkir, 0 ) - IFNULL( a.diskon_ongkir, 0 ) AS sub_ongkir_net,
            IFNULL( a.asuransi, 0 ) AS asuransi
            FROM rb_penjualan a
            JOIN rb_reseller b ON a.id_penjual = b.id_reseller
            JOIN rb_penjualan_detail d ON a.id_penjualan = d.id_penjualan 
            WHERE a.proses = '1' && a.`order` = '1' && MONTH ( a.waktu_order ) = '$bln'
            GROUP BY a.id_penjualan ASC) AS myTable
            GROUP BY kode_transaksi) AS myTable2
            GROUP BY bln")->row_array();
    }

    $this->load->view('administrator/additional/mod_keuangan/view_pdf', $data);
    $html = ob_get_contents();
    ob_end_clean();
    require './asset/pdf/vendor/autoload.php';
    $pdf = new Spipu\Html2Pdf\Html2Pdf('L','A4','en');
    $pdf->WriteHTML($html);
    $pdf->Output('data-finance-per-bulan.pdf', 'D');
}

function mdr(){
  cek_session_akses('keuangan',$this->session->id_session);
  if (isset($_POST['submit'])){
    $data = array('kategori_pembayaran'=>$this->input->post('kategori_pembayaran'),
        'jenis_pembayaran'=>$this->input->post('jenis_pembayaran'),
        'persentase_mdr'=>$this->input->post('persentase_mdr'),
        'nominal_mdr'=>$this->input->post('nominal_mdr'));

    $where = array('id_mdr' => $this->input->post('id_mdr'));
    $this->model_app->update('tabel_mdr', $data, $where);
    redirect($this->uri->segment(1).'/mdr');
}else{
    $data['record'] = $this->db->query("SELECT * FROM `tabel_mdr`");
    $this->template->load('administrator/template','administrator/additional/mod_keuangan/view_mdr', $data);
}
}

function halamanfaq(){
  cek_session_akses('halamanfaq',$this->session->id_session);
  $idfaq = $this->input->get('idfaq');
  if (isset($_POST['submit_kategori_faq'])) {
      $data = array('kategori_faq'=>$this->db->escape_str($this->input->post('kategori_faq')),
        'waktu'=>date('Y-m-d H:i:s'));
      $this->model_app->insert('kategori_faq',$data);
      redirect($this->uri->segment(1).'/halamanfaq?tab=kategori');

  }elseif (isset($_POST['submit_faq'])) {
      $data = array('id_kategori_faq'=>$this->db->escape_str($this->input->post('kategori_faq')),
        'judul_faq'=>$this->db->escape_str($this->input->post('judul_faq')),
        'isi_faq'=>$this->db->escape_str($this->input->post('isi_faq')),
        'flag'=>'1',
        'waktu'=>date('Y-m-d H:i:s'));
      $this->model_app->insert('faq',$data);
      redirect($this->uri->segment(1).'/halamanfaq');

  }elseif (isset($_POST['edit_kat'])) {

      $data = array('kategori_faq'=>$this->db->escape_str($this->input->post('kategori_faq_edit')),
        'waktu'=>date('Y-m-d H:i:s'));

      $where = array('id_kategori_faq' => $this->input->post('id_kategori_faq_edit'));
      $this->model_app->update('kategori_faq', $data, $where);
      redirect($this->uri->segment(1).'/halamanfaq?tab=kategori');

  }elseif (isset($_POST['update_faq'])) {

      $data = array('id_kategori_faq'=>$this->db->escape_str($this->input->post('kategori_faq')),
        'judul_faq'=>$this->db->escape_str($this->input->post('judul_faq')),
        'isi_faq'=>$this->db->escape_str($this->input->post('isi_faq')),
        'waktu'=>date('Y-m-d H:i:s'));

      $where = array('id_faq' => $this->input->post('id_faq'));
      $this->model_app->update('faq', $data, $where);
      redirect($this->uri->segment(1).'/halamanfaq?tab=faq');

  }else{
      $data['edit']         = $this->db->query("SELECT * from faq a JOIN kategori_faq b ON a.id_kategori_faq = b.id_kategori_faq where a.id_faq='$idfaq'")->row_array();
      $data['record']       = $this->db->query("SELECT * from kategori_faq ORDER BY kategori_faq ASC");
      $data['record_faq']   = $this->db->query("SELECT * from faq a JOIN kategori_faq b ON a.id_kategori_faq = b.id_kategori_faq ORDER BY a.id_kategori_faq");
      $this->template->load('administrator/template','administrator/mod_faq/view_faq', $data);
  }
}

function deletekategorifaq(){
    cek_session_akses('halamanfaq', $this->session->id_session);
    $id = array('id_kategori_faq' => $this->input->get('idkategorifaq'));
    $this->model_app->delete('kategori_faq', $id);
    redirect($this->uri->segment(1).'/halamanfaq?tab=kategori');
}

function deletefaq(){
    cek_session_akses('halamanfaq', $this->session->id_session);
    $id = array('id_faq' => $this->input->get('idfaq'));
    $this->model_app->delete('faq', $id);
    redirect($this->uri->segment(1).'/halamanfaq?tab=faq');
}

function onoff(){
    cek_session_akses('halamanfaq', $this->session->id_session);
    $idfaq = $this->input->get('idfaq');
    $flag = $this->input->get('flag');

    if ($flag == '1') {

        $data = array('flag'=>'0',
            'waktu'=>date('Y-m-d H:i:s'));

    }else{
        $data = array('flag'=>'1',
            'waktu'=>date('Y-m-d H:i:s'));
    }
    $where = array('id_faq' => $idfaq);
    $this->model_app->update('faq', $data, $where);

    redirect($this->uri->segment(1).'/halamanfaq?tab=faq');
}


function config_payment(){
    cek_session_akses('config_payment', $this->session->id_session);

    $data['configtampil'] = $this->db->query("SELECT * from konfigurasi where value2='metode_pembayaran'");
    $this->template->load('administrator/template','administrator/mod_konfigurasi/view_payment', $data);
}

function config_timeout(){
    cek_session_akses('config_payment', $this->session->id_session);

    $data['configtampil'] = $this->db->query("SELECT * from konfigurasi where id_konfig='6'");
    $this->template->load('administrator/template','administrator/mod_konfigurasi/view_timeout', $data);
}

function config_timeout_edit(){
  cek_session_akses('config_timeout',$this->session->id_session);
  $id = $this->uri->segment(3);
  if (isset($_POST['submit'])){
    $data = array('value1'=>$this->input->post('b'),
     'value2'=>$this->input->post('c'));
    $where = array('id_konfig' => $this->input->post('id'));
    $this->model_app->update('konfigurasi', $data, $where);
    redirect($this->uri->segment(1).'/config_timeout');
}else{
   if ($this->session->level=='admin'){
    $proses = $this->model_app->edit('konfigurasi', array('id_konfig' => $id))->row_array();
}else{
    $proses = $this->model_app->edit('konfigurasi', array('id_konfig' => $id))->row_array();
}
$data = array('rows' => $proses);
$this->template->load('administrator/template','administrator/mod_konfigurasi/view_timeout_edit',$data);
}
    //$data['rows'] = $this->db->query("SELECT * from konfigurasi where id_konfig=6");
}

function onoffpayment(){
    cek_session_akses('config_payment', $this->session->id_session);
    $flag = $this->input->get('onoff');
    $id_konfig = $this->input->get('konfig');

    if ($flag == '1') {
        $data = array('flag'=>'0');
    }else{
        $data = array('flag'=>'1');
    }
    $where = array('id_konfig' => $id_konfig);
    $this->model_app->update('konfigurasi', $data, $where);

    redirect($this->uri->segment(1).'/config_payment');
}

public function  print_invoice(){
    $idp = $this->uri->segment(3);
    $gambar = ('https://www.topsonia.com/asset/logo/logo_topsonia22.png');
    $ncs = ('https://www.topsonia.com/asset/logo/ncslogonobg.png');

    $query = "SELECT pj.kode_transaksi, pj.awb, pj.id_penjualan, pj.no_invoice, pj.waktu_order as waktu_order, pj.waktu_transaksi, pj.service, pj.ongkir, pj.asuransi, pj.kurir, pj.awb, rs.nama_reseller, rs.nama_reseller as nama_merchant, rs.kota_id as id_kota_merchant, kt1.nama_kota as kota_merchant, pr1.nama_provinsi as prov_merchant, rs.alamat_lengkap as alamat_merchant, rs.kode_pos as zip_merchant, rs.no_telpon as telp_merchant, al.pic as nama_konsumen, al.alamat_lengkap as alamat_konsumen, al.kecamatan as kecamatan, al.kota_id as id_kota_konsumen, kt2.nama_kota as kota_konsumen, pr2.nama_provinsi as prov_konsumen, al.kode_pos as zip_agen, al.no_hp as telp_konsumen, dt.jumlah, dt.jumlah, dt.harga_jual*dt.jumlah as subtotalperproduk, pd.nama_produk, dt.harga_jual as harga_produk ";
// $query .= "sum((dt.harga_jual*dt.jumlah)-dt.diskon+pj.ongkir+pj.asuransi) as TotalBayar,SUM(pj.ongkir+pj.asuransi) as TotalOngkir ,  SUM(dt.jumlah) as TotalBarang, SUM(pd.berat) as TotalBerat ";
    $query .= "FROM rb_penjualan pj ";
    $query .= "LEFT JOIN rb_reseller rs ON pj.id_penjual=rs.id_reseller ";
    $query .= "LEFT JOIN rb_kota kt1 ON rs.kota_id=kt1.kota_id ";
    $query .= "LEFT JOIN rb_provinsi pr1 ON kt1.provinsi_id=pr1.provinsi_id ";
    $query .= "LEFT JOIN rb_konsumen km ON pj.id_pembeli=km.id_konsumen ";
    $query .= "LEFT JOIN alamat al ON pj.id_alamat=al.id_alamat  ";
    $query .= "LEFT JOIN rb_kota kt2 ON al.kota_id=kt2.kota_id ";
    $query .= "LEFT JOIN rb_provinsi pr2 ON al.provinsi_id=pr2.provinsi_id  ";
    $query .= "LEFT JOIN rb_penjualan_detail dt ON pj.id_penjualan=dt.id_penjualan ";
    $query .= "LEFT JOIN rb_produk pd ON dt.id_produk=pd.id_produk ";
    $query .= "WHERE pj.kode_transaksi='".$idp."'";
// $query .= "GROUP BY pj.id_penjualan ";
    $data   = $this->db->query($query);

    $semua = $this->db->query("SELECT DISTINCT mdr, sum(ongkir) as ongkir, sum(diskon_ongkir) as diskon_ongkir, sum(diskon) as diskon FROM rb_penjualan WHERE kode_transaksi='".$idp."'");

// $semua = $this->db->query("SELECT ongkir,diskon_ongkir,diskon FROM rb_penjualan WHERE kode_transaksi='".$idp."'");

    $sub_total  = $this->db->query("SELECT sum((a.harga_jual*a.jumlah)) as total_bayar FROM `rb_penjualan_detail` a JOIN rb_penjualan b ON a.id_penjualan=b.id_penjualan where b.kode_transaksi='".$idp."'");   

// $sub_total  = $this->db->query("SELECT DISTINCT total_bayar FROM rb_penjualan where kode_transaksi='".$idp."'");  


$pdf = new FPDF('P','mm',array(210,99)); //L = lanscape P= potrait
// membuat halaman baru
$pdf->AddPage("P", array(150,120));
$w = $pdf->GetPageWidth(); // Width of Current Page
$h = $pdf->GetPageHeight(); // Height of Current Page

$xpos = 5;
$ypos = 5;
$margin = 3;
$area_content = $w - ($xpos*2);
$h_content = $h - ($ypos);
$mx = $xpos + $margin;
$my = $ypos + $margin;


//Header Left
$xpos = $mx;
$ypos = $my;
$pdf->setXY($xpos,$ypos);

$pdf->SetFont('Arial','B',8);
$pdf->setXY($xpos+3 + $area_content - 115, $ypos - 2);
$pdf->Cell(20, 3, 'Thank you for purchasing at', 0, 0, 'L' );
$pdf->Image($gambar, $xpos+3, $ypos + 1, 30, 0, 'png');

$ypos += 1.5;
$ypos += 9;
$pdf->SetTextColor(0, 0, 0);

$pdf->setXY($xpos, $ypos + 10);
$ypos += 10;
$yposTmp = $ypos;

//Area Left
$pdf->setXY($xpos,$ypos);
$pdf->SetFont('Arial','',15);
$pdf->SetTextColor(255, 155, 0);
$pdf->text($xpos, $yposTmp, 'INVOICE');

$pdf->SetFont('Arial','',9);
$pdf->SetTextColor(0, 0, 0);

//PENGIRIM
$spacing = 3.5;
$yposTmp += 6;
$pdf->SetFont('Arial','B',8);
$pdf->SetTextColor(0, 0, 000);
$pdf->text($xpos, $yposTmp, 'Invoice for');

$pdf->SetTextColor(0, 0, 0);
$yposTmp += 4;
$pdf->SetFont('Arial','',8);
$pdf->text($xpos, $yposTmp, $data->row_array()['nama_konsumen'] );

$yposTmp += $spacing;
$text=$data->row_array()['alamat_konsumen'];
$nb = $pdf->WordWrap($text, ($area_content/2) - ($margin * 2));
if($nb > 1){
    $x = 0;
    $y = 32;
    for($i = 0; $i <= $nb - 1; $i++) {
        $pdf->text($xpos, $yposTmp, trim(substr($text, $x,$y)));
        $yposTmp += $spacing;
        $x += $y;
    }
}else{
    $pdf->text($xpos, $yposTmp, $text);
    $yposTmp += $spacing;
}

if($data->row_array()['zip_konsumen'] != ''){
    $pdf->text($xpos, $yposTmp, $data->row_array()['zip_konsumen'] );
    $yposTmp += $spacing;
}
$pdf->text($xpos, $yposTmp, $data->row_array()['telp_konsumen']);

$pdf->SetTextColor(0, 0, 0);
$yposTmp += 4;
$pdf->SetFont('Arial','',8);
$pdf->text($xpos, $yposTmp, "AWB: ".$data->row_array()['awb'] );

$pdf->Line($xpos,$ypos + 25, $xpos + $area_content -10 , $ypos + 25);
$pdf->Line($xpos,$ypos + 33, $xpos + $area_content -10 , $ypos + 33);
$pdf->Line($xpos,$ypos + 70, $xpos + $area_content -10 , $ypos + 70);

//Area Right
$yposTmp = $ypos;
$xposTmp = ($xpos+7) + ($area_content/3);

$pdf->SetFont('Arial','B',8);
$yposTmp += 6;
$pdf->text($xposTmp, $yposTmp, 'Payable to');
$yposTmp += 4;
$pdf->SetFont('Arial','',8);
$pdf->text($xposTmp, $yposTmp, 'Topsonia');

$yposTmp = $ypos;
$xposTmp = ($xpos+34) + ($area_content/3);
$pdf->SetFont('Arial','B',8);
$yposTmp += 6;
$pdf->text($xposTmp, $yposTmp, 'Tanggal Order');
$yposTmp += 4;
$pdf->SetFont('Arial','',8);
$pdf->text($xposTmp, $yposTmp, $data->row_array()['waktu_order']);
$yposTmp += 6;
$pdf->text($xposTmp, $yposTmp, 'Invoice');
$yposTmp += 4;
if($data->row_array()['no_invoice'] != ''){
    $pdf->SetFont('Arial','',8);
    $pdf->text($xposTmp, $yposTmp, $data->row_array()['no_invoice']);
}else{
    $pdf->SetFont('Arial','',8);
    $pdf->text($xposTmp, $yposTmp, $data->row_array()['no_invoice']);
}
// if($data->row_array()['no_invoice'] != ''){
// $pdf->SetFont('Arial','',8);
// $pdf->text($xposTmp, $yposTmp, $data->row_array()['no_invoice']);
// }else{
// $pdf->SetFont('Arial','',8);
// $pdf->text($xposTmp, $yposTmp, $data->row_array()['no_invoice']);
// }


$spacing = 2;
$yposTmp += 20;
$pdf->SetFont('Arial','B',8);
$pdf->SetTextColor(0, 0, 000);
$pdf->text($xpos, $yposTmp, 'Product Description');
$pdf->text($xpos+60, $yposTmp, 'Qty');
$pdf->text($xpos+68, $yposTmp, 'Unit Price');
$pdf->text($xpos+85, $yposTmp, 'Total Price');

if ($data >=1){
    $spacing = 1;
    $yposTmp +=3;       
    foreach ($data->result_array() as $row){
        $spacing = 2.5;
        $yposTmp +=3;
        $pdf->SetFont('Arial','I',6);
        $text=$row['nama_produk'];
        $nb = $pdf->WordWrap($text, ($area_content/2) - ($margin * 2));
        if($nb > 1){
            $x = 0;
            $y = 55;
            for($i = 0; $i <= $nb - 1; $i++) {
                $pdf->text($xpos, $yposTmp, trim(substr($text, $x,$y)));
                $yposTmp += $spacing;
                $x += $y;
            }
        }else{

            $pdf->text($xpos, $yposTmp, $text);
            $yposTmp += $spacing;
        }
//$pdf->text($xpos, $yposTmp, $text=$row['nama_produk'], 0, 0, 'L');
        $pdf->text($xpos+64, $yposTmp-2, $text=$row['jumlah'], 0, 0, 'C');
        $pdf->text($xpos+72, $yposTmp-2, ''.rupiah($row['harga_produk']).'', 0, 0, 'R');
        $pdf->text($xpos+89, $yposTmp-2, ''.rupiah($row['subtotalperproduk']).'', 0, 0, 'R');
        $pdf->SetFont('Arial','',7);
    }
}else{

    foreach ($data->result_array() as $row){
        $spacing = 2.5;
        $yposTmp +=6;
        $pdf->SetFont('Arial','I',6);
        $text=$row['nama_produk'];
        $nb = $pdf->WordWrap($text, ($area_content/2) - ($margin * 2));
        if($nb > 1){
            $x = 0;
            $y = 55;
            for($i = 0; $i <= $nb - 1; $i++) {
                $pdf->text($xpos, $yposTmp, trim(substr($text, $x,$y)));
                $yposTmp += $spacing;
                $x += $y;
            }
        }else{

            $pdf->text($xpos, $yposTmp, $text);
            $yposTmp += $spacing;
        }
//$pdf->text($xpos, $yposTmp, $text=$row['nama_produk'], 0, 0, 'L');
        $pdf->text($xpos+64, $yposTmp-1, $text=$row['jumlah'], 0, 0, 'C');
        $pdf->text($xpos+72, $yposTmp-1, ''.rupiah($row['harga_produk']).'', 0, 0, 'R');
        $pdf->text($xpos+89, $yposTmp-1, ''.rupiah($row['subtotalperproduk']).'', 0, 0, 'R');
        $pdf->SetFont('Arial','',7);
    }
}

$yposTmp += 40;
$pdf->text($xpos+2, $yposTmp, 'Notes : ');
$pdf->SetFont('Arial','',7);
$pdf->text($xpos+56, $yposTmp, 'Subtotal ');
$pdf->text($xpos+56, $yposTmp+5, 'Admin Fee ');
// $pdf->text($xpos+56, $yposTmp+10, 'Discount Topsonia ');
$pdf->text($xpos+56, $yposTmp+10, 'Shipping ');
$pdf->text($xpos+56, $yposTmp+15, 'Discount Shipping ');
$pdf->SetFont('Arial','B',7);
$pdf->text($xpos+89, $yposTmp, 'Rp. '.rupiah($sub_total->row_array()['total_bayar']).'');
$pdf->text($xpos+89, $yposTmp+5,'Rp. '.rupiah($semua->row_array()['mdr']).'');
$pdf->text($xpos+89, $yposTmp+10, 'Rp. '.rupiah($semua->row_array()['ongkir']).''); 
$pdf->SetTextColor(255, 0, 0);
// $pdf->text($xpos+88, $yposTmp+10, '(Rp. '.rupiah($semua->row_array()['diskon']).')');
$pdf->text($xpos+88, $yposTmp+15, '(Rp. '.rupiah($semua->row_array()['diskon_ongkir']).')');

$pdf->SetFont('Arial','B',10);
$pdf->SetTextColor(0, 0, 0);
$pdf->text($xpos+74, $yposTmp+25, 'Rp. '.rupiah(($sub_total->row_array()['total_bayar']+$semua->row_array()['mdr']+$semua->row_array()['ongkir'])-$semua->row_array()['diskon_ongkir']).'');
$pdf->SetFont('Arial','I',7);
$pdf->SetTextColor(0, 0, 0);

$ypos += 5;
$pdf->Ln();
$pdf->setX($xpos + 20);
$pdf->write(5, '');
$m = $data->row_array()['nama_merchant'];
$i = $data->row_array()['no_invoice'];

//Generate PDF
$pdf->Output($idp,'I'); 
}

public function  print_invoice_admin(){
    $idp = $this->uri->segment(3);
    $gambar = ('https://www.topsonia.com/asset/logo/logo_topsonia22.png');
    $ncs = ('https://www.topsonia.com/asset/logo/ncslogonobg.png');

    $query = "SELECT pj.kode_transaksi, pj.awb, pj.id_penjualan, pj.no_invoice, pj.waktu_order as waktu_order, pj.waktu_transaksi, pj.service, pj.ongkir, pj.asuransi, pj.kurir, pj.awb, rs.nama_reseller, rs.nama_reseller as nama_merchant, rs.kota_id as id_kota_merchant, kt1.nama_kota as kota_merchant, pr1.nama_provinsi as prov_merchant, rs.alamat_lengkap as alamat_merchant, rs.kode_pos as zip_merchant, rs.no_telpon as telp_merchant, al.pic as nama_konsumen, al.alamat_lengkap as alamat_konsumen, al.kecamatan as kecamatan, al.kota_id as id_kota_konsumen, kt2.nama_kota as kota_konsumen, pr2.nama_provinsi as prov_konsumen, al.kode_pos as zip_agen, al.no_hp as telp_konsumen, dt.jumlah, dt.jumlah, dt.harga_jual*dt.jumlah as subtotalperproduk, pd.nama_produk, dt.harga_jual as harga_produk ";
// $query .= "sum((dt.harga_jual*dt.jumlah)-dt.diskon+pj.ongkir+pj.asuransi) as TotalBayar,SUM(pj.ongkir+pj.asuransi) as TotalOngkir ,  SUM(dt.jumlah) as TotalBarang, SUM(pd.berat) as TotalBerat ";
    $query .= "FROM rb_penjualan pj ";
    $query .= "LEFT JOIN rb_reseller rs ON pj.id_penjual=rs.id_reseller ";
    $query .= "LEFT JOIN rb_kota kt1 ON rs.kota_id=kt1.kota_id ";
    $query .= "LEFT JOIN rb_provinsi pr1 ON kt1.provinsi_id=pr1.provinsi_id ";
    $query .= "LEFT JOIN rb_konsumen km ON pj.id_pembeli=km.id_konsumen ";
    $query .= "LEFT JOIN alamat al ON pj.id_alamat=al.id_alamat  ";
    $query .= "LEFT JOIN rb_kota kt2 ON al.kota_id=kt2.kota_id ";
    $query .= "LEFT JOIN rb_provinsi pr2 ON al.provinsi_id=pr2.provinsi_id  ";
    $query .= "LEFT JOIN rb_penjualan_detail dt ON pj.id_penjualan=dt.id_penjualan ";
    $query .= "LEFT JOIN rb_produk pd ON dt.id_produk=pd.id_produk ";
    $query .= "WHERE pj.kode_transaksi='".$idp."'";
// $query .= "GROUP BY pj.id_penjualan ";
    $data   = $this->db->query($query);

    $semua = $this->db->query("SELECT DISTINCT mdr, sum(ongkir) as ongkir, sum(diskon_ongkir) as diskon_ongkir, sum(diskon+komisi_topsonia) as diskon FROM rb_penjualan WHERE kode_transaksi='".$idp."'");

// $semua = $this->db->query("SELECT ongkir,diskon_ongkir,diskon FROM rb_penjualan WHERE kode_transaksi='".$idp."'");

    $sub_total  = $this->db->query("SELECT sum((a.harga_jual*a.jumlah)) as total_bayar FROM `rb_penjualan_detail` a JOIN rb_penjualan b ON a.id_penjualan=b.id_penjualan where b.kode_transaksi='".$idp."'");  

// $sub_total  = $this->db->query("SELECT DISTINCT total_bayar FROM rb_penjualan where kode_transaksi='".$idp."'");  


$pdf = new FPDF('P','mm',array(210,99)); //L = lanscape P= potrait
// membuat halaman baru
$pdf->AddPage("P", array(150,120));
$w = $pdf->GetPageWidth(); // Width of Current Page
$h = $pdf->GetPageHeight(); // Height of Current Page

$xpos = 5;
$ypos = 5;
$margin = 3;
$area_content = $w - ($xpos*2);
$h_content = $h - ($ypos);
$mx = $xpos + $margin;
$my = $ypos + $margin;


//Header Left
$xpos = $mx;
$ypos = $my;
$pdf->setXY($xpos,$ypos);

$pdf->SetFont('Arial','B',8);
$pdf->setXY($xpos+3 + $area_content - 115, $ypos - 2);
$pdf->Cell(20, 3, 'Thank you for purchasing at', 0, 0, 'L' );
$pdf->Image($gambar, $xpos+3, $ypos + 1, 30, 0, 'png');

$ypos += 1.5;
$ypos += 9;
$pdf->SetTextColor(0, 0, 0);

$pdf->setXY($xpos, $ypos + 10);
$ypos += 10;
$yposTmp = $ypos;

//Area Left
$pdf->setXY($xpos,$ypos);
$pdf->SetFont('Arial','',15);
$pdf->SetTextColor(255, 155, 0);
$pdf->text($xpos, $yposTmp, 'INVOICE');

$pdf->SetFont('Arial','',9);
$pdf->SetTextColor(0, 0, 0);

//PENGIRIM
$spacing = 3.5;
$yposTmp += 6;
$pdf->SetFont('Arial','B',8);
$pdf->SetTextColor(0, 0, 000);
$pdf->text($xpos, $yposTmp, 'Payable to');

$pdf->SetTextColor(0, 0, 0);
$yposTmp += 4;
$pdf->SetFont('Arial','',8);
$pdf->text($xpos, $yposTmp, $data->row_array()['nama_merchant'] );

$yposTmp += $spacing;
$text=$data->row_array()['kota_merchant'];
$nb = $pdf->WordWrap($text, ($area_content/2) - ($margin * 2));
if($nb > 1){
    $x = 0;
    $y = 32;
    for($i = 0; $i <= $nb - 1; $i++) {
        $pdf->text($xpos, $yposTmp, trim(substr($text, $x,$y)));
        $yposTmp += $spacing;
        $x += $y;
    }
}else{
    $pdf->text($xpos, $yposTmp, $text);
    $yposTmp += $spacing;
}

$pdf->SetTextColor(0, 0, 0);
$yposTmp += 4;
$pdf->SetFont('Arial','',8);
$pdf->text($xpos, $yposTmp, "AWB: ".$data->row_array()['awb'] );

if($data->row_array()['zip_konsumen'] != ''){
    $pdf->text($xpos, $yposTmp, $data->row_array()['zip_konsumen'] );
    $yposTmp += $spacing;
}
// $pdf->text($xpos, $yposTmp, $data->row_array()['telp_konsumen']);

$pdf->Line($xpos,$ypos + 25, $xpos + $area_content -10 , $ypos + 25);
$pdf->Line($xpos,$ypos + 33, $xpos + $area_content -10 , $ypos + 33);
$pdf->Line($xpos,$ypos + 70, $xpos + $area_content -10 , $ypos + 70);

//Area Right
$yposTmp = $ypos;
$xposTmp = ($xpos+7) + ($area_content/3);

$pdf->SetFont('Arial','B',8);
$yposTmp += 6;
$pdf->text($xposTmp, $yposTmp, 'Invoice for');
$yposTmp += 4;
$pdf->SetFont('Arial','',8);
$pdf->text($xposTmp, $yposTmp, 'Topsonia');

$yposTmp = $ypos;
$xposTmp = ($xpos+34) + ($area_content/3);
$pdf->SetFont('Arial','B',8);
$yposTmp += 6;
$pdf->text($xposTmp, $yposTmp, 'Tanggal Order');
$yposTmp += 4;
$pdf->SetFont('Arial','',8);
$pdf->text($xposTmp, $yposTmp, $data->row_array()['waktu_order']);
$yposTmp += 6;
$pdf->text($xposTmp, $yposTmp, 'Invoice');
$yposTmp += 4;
if($data->row_array()['no_invoice'] != ''){
    $pdf->SetFont('Arial','',8);
    $pdf->text($xposTmp, $yposTmp, $data->row_array()['no_invoice']);
}else{
    $pdf->SetFont('Arial','',8);
    $pdf->text($xposTmp, $yposTmp, $data->row_array()['no_invoice']);
}
// if($data->row_array()['no_invoice'] != ''){
// $pdf->SetFont('Arial','',8);
// $pdf->text($xposTmp, $yposTmp, $data->row_array()['no_invoice']);
// }else{
// $pdf->SetFont('Arial','',8);
// $pdf->text($xposTmp, $yposTmp, $data->row_array()['no_invoice']);
// }


$spacing = 2;
$yposTmp += 20;
$pdf->SetFont('Arial','B',8);
$pdf->SetTextColor(0, 0, 000);
$pdf->text($xpos, $yposTmp, 'Product Description');
$pdf->text($xpos+60, $yposTmp, 'Qty');
$pdf->text($xpos+68, $yposTmp, 'Unit Price');
$pdf->text($xpos+85, $yposTmp, 'Total Price');

if ($data >=1){
    $spacing = 1;
    $yposTmp +=3;       
    foreach ($data->result_array() as $row){
        $spacing = 2.5;
        $yposTmp +=3;
        $pdf->SetFont('Arial','I',6);
        $text=$row['nama_produk'];
        $nb = $pdf->WordWrap($text, ($area_content/2) - ($margin * 2));
        if($nb > 1){
            $x = 0;
            $y = 55;
            for($i = 0; $i <= $nb - 1; $i++) {
                $pdf->text($xpos, $yposTmp, trim(substr($text, $x,$y)));
                $yposTmp += $spacing;
                $x += $y;
            }
        }else{

            $pdf->text($xpos, $yposTmp, $text);
            $yposTmp += $spacing;
        }
//$pdf->text($xpos, $yposTmp, $text=$row['nama_produk'], 0, 0, 'L');
        $pdf->text($xpos+64, $yposTmp-2, $text=$row['jumlah'], 0, 0, 'C');
        $pdf->text($xpos+72, $yposTmp-2, ''.rupiah($row['harga_produk']).'', 0, 0, 'R');
        $pdf->text($xpos+89, $yposTmp-2, ''.rupiah($row['subtotalperproduk']).'', 0, 0, 'R');
        $pdf->SetFont('Arial','',7);
    }
}else{

    foreach ($data->result_array() as $row){
        $spacing = 2.5;
        $yposTmp +=6;
        $pdf->SetFont('Arial','I',6);
        $text=$row['nama_produk'];
        $nb = $pdf->WordWrap($text, ($area_content/2) - ($margin * 2));
        if($nb > 1){
            $x = 0;
            $y = 55;
            for($i = 0; $i <= $nb - 1; $i++) {
                $pdf->text($xpos, $yposTmp, trim(substr($text, $x,$y)));
                $yposTmp += $spacing;
                $x += $y;
            }
        }else{

            $pdf->text($xpos, $yposTmp, $text);
            $yposTmp += $spacing;
        }
//$pdf->text($xpos, $yposTmp, $text=$row['nama_produk'], 0, 0, 'L');
        $pdf->text($xpos+64, $yposTmp-1, $text=$row['jumlah'], 0, 0, 'C');
        $pdf->text($xpos+72, $yposTmp-1, ''.rupiah($row['harga_produk']).'', 0, 0, 'R');
        $pdf->text($xpos+89, $yposTmp-1, ''.rupiah($row['subtotalperproduk']).'', 0, 0, 'R');
        $pdf->SetFont('Arial','',7);
    }
}

$yposTmp += 20;
$pdf->text($xpos+2, $yposTmp, 'Notes : ');
$pdf->SetFont('Arial','',7);
$pdf->text($xpos+56, $yposTmp, 'Subtotal ');
// $pdf->text($xpos+56, $yposTmp+5, 'Admin Fee ');
$pdf->text($xpos+56, $yposTmp+5, 'Discount Topsonia ');
// $pdf->text($xpos+56, $yposTmp+15, 'Shipping ');
// $pdf->text($xpos+56, $yposTmp+20, 'Discount Shipping ');
$pdf->SetFont('Arial','B',7);
$pdf->text($xpos+89, $yposTmp, 'Rp. '.rupiah($sub_total->row_array()['total_bayar']).'');
// $pdf->text($xpos+89, $yposTmp+5,'Rp. '.rupiah($semua->row_array()['mdr']).'');
// $pdf->text($xpos+89, $yposTmp+15, 'Rp. '.rupiah($semua->row_array()['ongkir']).''); 
$pdf->SetTextColor(255, 0, 0);
$pdf->text($xpos+88, $yposTmp+5, '(Rp. '.rupiah($semua->row_array()['diskon']).')');
// $pdf->text($xpos+88, $yposTmp+20, '(Rp. '.rupiah($semua->row_array()['diskon_ongkir']).')');

$pdf->SetFont('Arial','B',10);
$pdf->SetTextColor(0, 0, 0);
// $pdf->text($xpos+74, $yposTmp+25, 'Rp. '.rupiah($sub_total->row_array()['total_bayar']-$semua->row_array()['diskon']+$semua->row_array()['ongkir']-$semua->row_array()['diskon_ongkir']+$semua->row_array()['mdr']).'');
$pdf->text($xpos+74, $yposTmp+15, 'Rp. '.rupiah($sub_total->row_array()['total_bayar']-$semua->row_array()['diskon']).'');
$pdf->SetFont('Arial','I',7);
$pdf->SetTextColor(0, 0, 0);

$ypos += 5;
$pdf->Ln();
$pdf->setX($xpos + 20);
$pdf->write(5, '');
$m = $data->row_array()['nama_merchant'];
$i = $data->row_array()['no_invoice'];

//Generate PDF
$pdf->Output($idp,'I'); 
}

function pesan_admin(){
    $user   = $this->db->query("select * from chat_admin a join rb_reseller b on a.id_penjual=b.id_reseller where del!=1 group by id_penjual order by id_chat");
    
    $data['user'] = $user;
    $this->template->load($this->uri->segment(1).'/template',$this->uri->segment(1).'/pesan/menu_pesan',$data);
}

function chat_admin(){
    
        $id_penjual  = $this->input->post('id_penjual'); //tujuan
        $nama_reseller  = $this->input->post('nama_reseller');
        $id_max     = '10'; //dari
        $chat   = $this->db->query("select * from chat_admin where id_penjual='".$id_penjual."' and del!=1 order by id_chat");
        $this->model_app->update('chat_admin',array("flag"=>1),array("id_penjual"=>$id_penjual,"ket"=>"2","flag"=>0));
        
        $data['id_penjual'] = $id_penjual;
        $data['nama_reseller'] = $nama_reseller;
        $data['id_max']     = $id_max;
        $data['chat']       = $chat;
        $this->load->view("administrator/pesan/chat_admin.php",$data);

    }

    function kirim_pesan_admin(){
        $pesan      = $this->input->post("pesan");
        
        $data   = array(
            'id_penjual' => $this->input->post("id_penjual"),
            'pesan' => $pesan,
            'flag' => 0,
            'ket' => 1,
            'del' =>0
        );
        
        $query  =   $this->model_app->insert('chat_admin',$data);
        
        if($query){
            $rs = 1;
        }else{
            $rs = 2;
        }
        
        echo json_encode(array("result"=>$rs));
        
    }

    function hapus_pesan_admin(){
        $this->model_app->update('chat_admin',array("del"=>'1'),array("id_penjual"=>$this->input->post("id_penjual"),"del"=>'0'));
        $this->db->query("delete from chat_admin where id_penjual='".$this->input->post("id_penjual")."' and del='2'");          
        $data = array("bisa"=>"1");
        echo json_encode($data);            
    }

    function about_us(){
        cek_session_akses('about_us', $this->session->id_session);
        if (isset($_POST['update_about'])){
            $data = array('judul_about'=>$this->db->escape_str($this->input->post('judul_about')),
                'isi_about'=>$this->db->escape_str($this->input->post('isi_about')),
                'user'=>$this->db->escape_str($this->session->username),
                'waktu'=>date('Y-m-d H:i:s'));

            $where = array('id_about' => '1');
            $this->model_app->update('about', $data, $where);
            redirect($this->uri->segment(1).'/about_us?tab=faq');
        }else{
            $data['view_about'] = $this->db->query("SELECT * from about where id_about='1'")->row_array();
            $this->template->load('administrator/template','administrator/mod_faq/view_about',$data);
        }
    }

    function cek_kode_referal(){
        $kode = $this->input->post('kode_referal');
        $query = $this->db->query("select id_konsumen, nama_lengkap from rb_konsumen where kode_referall='$kode'")->row_array();
        if($query['id_konsumen']==null){
            $data = array("result"=>"0");
        }else{
            $data = array("result"=>"1",'nama' =>$query['nama_lengkap']);
        }
        echo json_encode($data);
    }
    
    
    function filter_referal(){
        if($this->input->post('tahun')!='p'){
            $thn = $this->input->post('tahun');
            $tahun="and YEAR(c.waktu_order)='$thn'";
        }else{
            $tahun="";
        }

        if($this->input->post('bulan')!='p'){
            $bln = $this->input->post('bulan');
            $bulan="and MONTH(c.waktu_order)='$bln'";
        }else{
            $bulan="";
        }

        if($this->input->post('id_konsumen')!='p'){
            $ksm = $this->input->post('id_konsumen');
            $konsumen="and id_konsumen='$ksm'";
        }else{
            $konsumen="";
        }

        $record = $this->db->query("select YEAR(c.waktu_order) as tahun, MONTHNAME(STR_TO_DATE(MONTH(c.waktu_order), '%m')) as bulan, a.id_reseller,nama_reseller,b.id_konsumen,b.nama_lengkap, a.tanggal_daftar,c.waktu_order, DATE_ADD(a.tanggal_daftar, INTERVAL 6 MONTH) as 'next_6_bulan', sum(c.komisi_topsonia) as komisi_topsonia, round((sum(c.komisi_topsonia)*1)/100) as bonus_referal from rb_reseller a join rb_konsumen b on a.referral=b.kode_referall join rb_penjualan c on a.id_reseller=c.id_penjual where c.proses='1' $tahun $bulan $konsumen GROUP BY a.id_reseller,YEAR(c.waktu_order),MONTH(c.waktu_order)");
        $cek = $record->num_rows();
        if($cek<1){
            $data['result']='0';
        }else{
            $data['result']='1';
            $no = 1;
            $filter = "";
            foreach ($record->result_array() as $row){
            
                $id_reseller = $row['id_reseller'];
                $id_konsumen = $row['id_konsumen'];
                $tahun = $row['tahun'];
                $bulan = date('m',strtotime($row['bulan']));

                $pencairan = $this->db->query("select sum(jumlah_pencairan) as jumlah_pencairan, max(tanggal_pencairan) as tanggal_pencairan from rb_pencairan_referal where id_reseller='$id_reseller' and id_konsumen='$id_konsumen' and tahun='$tahun' and bulan='$bulan' GROUP BY id_reseller,tahun,bulan ORDER BY id_pencairan_bonus desc");
                $pencairan_cek = $pencairan->num_rows();
                
                if($pencairan_cek>0){

                  $pencairan_detail = $pencairan->row_array();
                  $tanggal_pencairan = $pencairan_detail['tanggal_pencairan'];
                  $jumlah_pencairan = "Rp ".rupiah($pencairan_detail['jumlah_pencairan']);
                  $sisa_saldo = "Rp ".rupiah($row['bonus_referal']-$pencairan_detail['jumlah_pencairan']);
               
                }else{
                  $tanggal_pencairan = "";
                  $jumlah_pencairan = "";
                  $sisa_saldo = "Rp ".rupiah($row['bonus_referal']);
                }

            $filter =$filter."<tr><td>$no</td>
                  <td>$row[nama_reseller]</td>
                  <td>$row[nama_lengkap]</td>
                  <td>$row[bulan]</td>
                  <td>$row[tahun]</td>
                  <td>$tanggal_pencairan</td>
                  <td>$jumlah_pencairan</td>
                  <td>$sisa_saldo</td>
                  <td><center>
                  <a class='btn btn-warning btn-xs' title='Pencairan Bonus' href='" . base_url() . "administrator/pencairan_referral/$row[id_reseller]/$row[id_konsumen]/$row[bulan]/$row[tahun]'><span class='fa fa-money'></span></a>
                  </center></td>
              </tr>";
            $no++;
            }
            $data['filter']=$filter;
        }
        echo json_encode($data);
    }

    
function layanan(){
    cek_session_akses('layanan',$this->session->id_session);
    $idlayanan = $this->uri->segment(3);
    if (isset($_POST['insert_layanan'])) {
        $data = array('nama_layanan'=>$this->db->escape_str($this->input->post('nama_layanan')),
            'deskripsi'=>$this->db->escape_str($this->input->post('deskripsi')),
            'flag'=>'1',
            'link'=>$this->db->escape_str($this->input->post('link')),
            'log_user' => $this->session->username,
            'tgl'=>date('Y-m-d H:i:s'));

        $this->model_app->insert('layanan',$data);
        $id_lay = $this->db->insert_id();

        $files = $_FILES;
        $cpt = count($_FILES['userfile']['name']);
        for($i=0; $i<$cpt; $i++){
            if ($files['userfile']['name'][$i] != '') {
                $no=$i+1;
                $_FILES['userfile']['name']    = $files['userfile']['name'][$i];
                $_FILES['userfile']['type']    = $files['userfile']['type'][$i];
                $_FILES['userfile']['tmp_name']    = $files['userfile']['tmp_name'][$i];
                $_FILES['userfile']['error']   = $files['userfile']['error'][$i];
                $_FILES['userfile']['size']    = $files['userfile']['size'][$i];
                $this->load->library('upload');
                $savename = $id_lay.'_'.date('ymdHis').'_'.$i.'.jpg';
                $this->upload->initialize($this->set_upload_layanan($savename));
                $this->upload->do_upload();
                $fileName = $this->upload->data('file_name');
                $this->func_resize($fileName, 'asset/layanan/', 500, 375);
                $images[] = $fileName;
            }
        }

        $fileName = implode(';',$images);
        $fileName = str_replace(' ','_',$fileName);

        $data1 = array('logo_layanan' => $fileName);
        $where = array('id_layanan' => $id_lay);
        $this->model_app->update('layanan', $data1, $where);

        redirect($this->uri->segment(1).'/layanan');

    }elseif (isset($_POST['update_layanan'])) {
            $files = $_FILES;
            $cpt = count($_FILES['userfile']['name']);
            for($i=0; $i<$cpt; $i++){
                if($files['userfile']['name'][$i]!=''){
                    $_FILES['userfile']['name']= $files['userfile']['name'][$i];
                    $_FILES['userfile']['type']= $files['userfile']['type'][$i];
                    $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
                    $_FILES['userfile']['error']= $files['userfile']['error'][$i];
                    $_FILES['userfile']['size']= $files['userfile']['size'][$i];
                    $this->load->library('upload');
                    $savename = $idlayanan.'_'.date('ymdHis').'_'.$i.'.jpg';
                    $this->upload->initialize($this->set_upload_layanan($savename));
                    $this->upload->do_upload();
                    $fileName = $this->upload->data()['file_name'];
                    $this->func_resize($fileName, 'asset/layanan/', 500, 375);
                    $images[] = $fileName;
                    unlink("./asset/layanan/".$this->input->post('file_lama'));
                }else if($this->input->post('file_lama')!=null){
                    $images[] = $this->input->post('file_lama');
                }else{

                }
            }
            $fileName = implode(';',$images);
            $fileName = str_replace(' ','_',$fileName);
            if (trim($fileName)!=''){
                $data = array('nama_layanan'=>$this->db->escape_str($this->input->post('nama_layanan')),
                    'deskripsi'=>$this->db->escape_str($this->input->post('deskripsi')),
                    'link'=>$this->db->escape_str($this->input->post('link')),
                    'log_user' => $this->session->username,
                    'logo_layanan' => $fileName,
                    'tgl'=>date('Y-m-d H:i:s'));
            }else{
                $data = array('nama_layanan'=>$this->db->escape_str($this->input->post('nama_layanan')),
                    'deskripsi'=>$this->db->escape_str($this->input->post('deskripsi')),
                    'link'=>$this->db->escape_str($this->input->post('link')),
                    'log_user' => $this->session->username,
                    'tgl'=>date('Y-m-d H:i:s'));
            }

            $where = array('id_layanan' => $idlayanan);
            $this->model_app->update('layanan', $data, $where);
            redirect($this->uri->segment(1).'/layanan/'.$this->uri->segment(3));

        }else{
            $data['update_layanan']   = $this->db->query("SELECT * from layanan WHERE id_layanan='$idlayanan'")->row_array();
            $data['record_layanan']   = $this->db->query("SELECT * from layanan ORDER BY id_layanan DESC");
            $this->template->load('administrator/template','administrator/mod_faq/view_layanan', $data);
        }
}

function deletelayanan(){
    cek_session_akses('layanan', $this->session->id_session);
    $id = array('id_layanan' => $this->uri->segment(3));
    $this->model_app->delete('layanan', $id);
    redirect($this->uri->segment(1).'/layanan');
}

function onofflayanan(){
    cek_session_akses('layanan', $this->session->id_session);
    $idlayanan  = $this->uri->segment(3);
    $flag       = $this->uri->segment(4);

    if ($flag == '1') {

        $data = array('flag'=>'0',
            'tgl'=>date('Y-m-d H:i:s'));

    }else{
        $data = array('flag'=>'1',
            'tgl'=>date('Y-m-d H:i:s'));
    }
    $where = array('id_layanan' => $idlayanan);
    $this->model_app->update('layanan', $data, $where);

    redirect($this->uri->segment(1).'/layanan');
}

private function set_upload_layanan($new_name){
    $config = array();
    $config['upload_path'] = './asset/layanan/';
    $config['allowed_types'] = 'gif|jpg|png|jpeg';
    $config['max_size'] = '5000'; // kb
    $config['file_name'] = $new_name;
    $config['encrypt_name'] = FALSE;
    $this->load->library('upload', $config);
    return $config;
}

    private function set_upload_options_t($new_name)
    {
        $config = array();
        $config['upload_path'] = './asset/bukti_transfer/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '5000'; // kb
        $config['file_name'] = $new_name;
        $config['encrypt_name'] = FALSE;
        $this->load->library('upload', $config);
        return $config;
    }

  
    public function upload_transfer()
    {   
        $files = $_FILES;
        if($files['bukti_transfer']['name']!=''){
        $_FILES['userfile']['name'] = $files['bukti_transfer']['name'];
        $_FILES['userfile']['type'] = $files['bukti_transfer']['type'];
        $_FILES['userfile']['tmp_name'] = $files['bukti_transfer']['tmp_name'];
        $_FILES['userfile']['error'] = $files['bukti_transfer']['error'];
        $_FILES['userfile']['size'] = $files['bukti_transfer']['size'];
        $this->load->library('upload');
        $savename = $this->input->post('kode_transaksi') . '.jpg';
        $this->upload->initialize($this->set_upload_options_t($savename));
        $this->upload->do_upload();
        $fileName = $this->upload->data('file_name');
        }else{
            $fileName = $this->input->post('kode_transaksi') . '.jpg';
        }
        
        $transfer_ke = explode(";",$this->input->post('transfer_ke'));
        $data = array(
            'kode_transaksi' => $this->input->post('kode_transaksi'),
            'via' => 'manual-transfer',
            'nominal' => $this->input->post('jumlah'),
            'id_rekening' => $transfer_ke[0],
            'nama_penerima' => $transfer_ke[1],
            'nama_pengirim' => $this->input->post('nama_pengirim'),
            'tanggal_bayar' => $this->input->post('tanggal_transfer'),
            'rekening_pengirim' => $this->input->post('rekening_pengirim'),
            'bukti_bayar' => $fileName,
            'status_pembayaran' => $this->input->post('status_pembayaran'),
            'waktu_konfirmasi' => date('Y-m-d H:i:s')
        );

        if($this->input->post('metod')=='0'){
            $this->model_app->insert('rb_konfirmasi_pembayaran_konsumen', $data);
        }else{
            $where = array('kode_transaksi' => $this->input->post('kode_transaksi'));
            $this->model_app->update('rb_konfirmasi_pembayaran_konsumen', $data, $where);
        }
        
        $data1 = array('bayar' => '2');
        $where = array('kode_transaksi' => $this->input->post('kode_transaksi'));
        $this->model_app->update('rb_penjualan', $data1, $where);

        redirect("administrator/detail_transaksi/".$this->input->post('kode_transaksi'));
    }

    function logout(){
      $this->session->sess_destroy();
		// redirect('main');
      redirect($this->uri->segment(1).'/index');
  }
  
}
