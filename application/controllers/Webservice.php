<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class Webservice extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
        $params = array('server_key' => 'SB-Mid-server-ZITPm-4tKXc1LbcogWCVqPR3', 'production' => false);
		$this->load->library('midtrans');
		$this->midtrans->config($params);
	}

	public function index(){
		
		$datasql = $this->db->query("SELECT a.kode_transaksi,a.waktu_order,a.pembayaran,a.kategori_pembayaran,a.bayar,a.batal,b.status_pembayaran FROM rb_penjualan a LEFT JOIN rb_konfirmasi_pembayaran_konsumen b ON a.kode_transaksi=b.kode_transaksi WHERE (b.status_pembayaran='diperiksa' OR b.status_pembayaran IS NULL) AND a.bayar='0' AND a.status_pembeli='konsumen' AND a.waktu_order IS NOT NULL ORDER BY a.waktu_order ASC");
		$konfig  = $this->db->query("SELECT * FROM konfigurasi WHERE id_konfig = 6")->row_array();

		$waktu_sekarang = strtotime(date('Y-m-d H:i:s'));
		
		foreach ($datasql->result_array() as $key) {
			$waktu_order 	= strtotime($key['waktu_order']);
			$interval		= $waktu_sekarang - $waktu_order;
			$jam    		= floor($interval / (60 * 60))*60;
			// $menit    	= floor($interval - $jam * (60 * 60))/60;
			$exp 			= $konfig['value2'] * 60;
			$kode_transaksi = $key['kode_transaksi'];

			if ($jam > $exp) {
				$data_batal = array('batal' => '1');
				$where_batal = array('kode_transaksi' => $kode_transaksi);
				$this->model_app->update('rb_penjualan', $data_batal, $where_batal);
				
				$data_status = array('status_pembayaran' => 'gagal');
				$where_status = array('kode_transaksi' => $kode_transaksi);
				$this->model_app->update('rb_konfirmasi_pembayaran_konsumen', $data_status, $where_status);

			//echo "Kode transaksi ".$kode_transaksi." - ".$jam."<br>";
			}
		}
	}

	public function autoselesai(){
		$waktu_sekarang = strtotime(date('Y-m-d H:i:s'));
		$pembayaran = $this->db->query("SELECT kode_transaksi,kategori_pembayaran,total_bayar FROM rb_penjualan WHERE kategori_pembayaran IS NOT NULL")->result_array();

		foreach ($pembayaran as $key) {
			echo $key['kode_transaksi']." ---> ".$key['kategori_pembayaran']."<br>";
		}

	}

}


?>