<?php
/*
-- ---------------------------------------------------------------
-- MARKETPLACE MULTI BUYER MULTI SELLER + SUPPORT RESELLER SYSTEM
-- CREATED BY : ROBBY PRIHANDAYA
-- COPYRIGHT  : Copyright (c) 2018 - 2019, PHPMU.COM. (https://phpmu.com/)
-- LICENSE    : http://opensource.org/licenses/MIT  MIT License
-- CREATED ON : 2019-03-26
-- UPDATED ON : 2019-03-27
-- ---------------------------------------------------------------
*/
defined('BASEPATH') OR exit('No direct script access allowed');
class Faq extends CI_Controller {
	
	public function index(){
		$id_kategori 	= $this->input->get('id');
		$idk			= $this->input->get('idk');

		$data['all']	=$this->db->query("SELECT * FROM faq where flag='1'")->num_rows();
		$data['record'] = $this->db->query("SELECT * FROM faq where flag='1'");
		$data['record2']= $this->db->query("SELECT * FROM kategori_faq");
		$data['record3']= $this->db->query("SELECT * FROM faq a JOIN kategori_faq b ON a.id_kategori_faq = b.id_kategori_faq where a.id_kategori_faq='$id_kategori' && flag='1'");
		$data['record4']= $this->db->query("SELECT * FROM kategori_faq where id_kategori_faq='$id_kategori'")->row_array();
		$data['cek']	=$this->db->query("SELECT id_kategori_faq FROM faq where id_kategori_faq='$id_kategori' && flag='1'")->num_rows();
		$data['idk']	= $id_kategori;
		
		$this->load->view('/faq/faq', $data);
		
	}

	public function blog(){
		$jumlah= $this->model_utama->view('berita')->num_rows();
		$config['base_url'] = base_url().'faq/blog/';
		$config['total_rows'] = $jumlah;
		$config['per_page'] = 15; 	
		if ($this->uri->segment('3')==''){
			$dari = 0;
		}else{
			$dari = $this->uri->segment('3');
		}

		if ($this->input->post('kata')){
			$data['title'] = "Hasil Pencarian keyword - ".cetak($this->input->post('kata'));
			$data['description'] = description();
			$data['keywords'] = keywords();
			$data['berita'] = $this->model_utama->cari_berita($this->input->post('kata'));
		}else{
			$data['title'] = "BLOG TOPSONIA";
			$data['description'] = description();
			$data['keywords'] = keywords();
			$data['berita'] = $this->model_utama->view_joinn('berita','users','kategori','username','id_kategori','id_berita','DESC',$dari,$config['per_page']);
			$this->pagination->initialize($config);
		}
		$this->template->load(template().'/template',template().'/berita',$data);
	}

	
	public function layanan(){
		$jumlah= $this->model_utama->view('layanan')->num_rows();
		$config['base_url'] = base_url().'faq/layanan/';
		$config['total_rows'] = $jumlah;
		$config['per_page'] = 15; 	
		if ($this->uri->segment('3')==''){
			$dari = 0;
		}else{
			$dari = $this->uri->segment('3');
		}

			$data['title'] = "LAYANAN TOPSONIA";
			$data['description'] = description();
			$data['keywords'] = keywords();
			$data['berita'] = $this->db->query("SELECT * FROM layanan WHERE flag='1' order by id_layanan DESC");
			$this->pagination->initialize($config);
		
		$this->template->load(template().'/template',template().'/layanan',$data);
	}


	public function about_us(){
		$data['view_about'] = $this->db->query("SELECT * FROM about")->row_array();
		$this->template->load(template().'/template',template().'/about_us',$data);
		
	}
}




?>
