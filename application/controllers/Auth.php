<?php
/*
-- ---------------------------------------------------------------
-- MARKETPLACE MULTI BUYER MULTI SELLER + SUPPORT RESELLER SYSTEM
-- CREATED BY : ROBBY PRIHANDAYA
-- COPYRIGHT  : Copyright (c) 2018 - 2019, PHPMU.COM. (https://phpmu.com/)
-- LICENSE    : http://opensource.org/licenses/MIT  MIT License
-- CREATED ON : 2019-03-26
-- UPDATED ON : 2019-03-27
-- ---------------------------------------------------------------
*/
defined('BASEPATH') OR exit('No direct script access allowed');
class Auth extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
        $params = array('server_key' => 'SB-Mid-server-ZITPm-4tKXc1LbcogWCVqPR3', 'production' => false);
		$this->load->library('midtrans');
		$this->midtrans->config($params);
	}
	function city(){
		$state_id = $this->input->post('stat_id');
		$data['kota'] = $this->model_app->view_where_ordering('rb_kota',array('provinsi_id' => $state_id),'kota_id','DESC');
		$this->load->view(template().'/reseller/view_city',$data);		
	}

	public function register(){
		if (isset($_POST['submit1'])){
		$kon = $this->db->query("SELECT * FROM konfigurasi")->result_array();
		foreach($kon as $row){
				$arraydata = array(
					"flag_".$row['nama_konfig'] =>$row['flag'],
					"value1_".$row['nama_konfig'] =>$row['value1'],
					"value2_".$row['nama_konfig'] =>$row['value2'],
				);
				$this->session->set_userdata($arraydata);
		}
		
			$data = array('username'=>$this->input->post('a'),
				'password'=>hash("sha512", md5($this->input->post('b'))),
				'nama_lengkap'=>$this->input->post('c'),
				'id_tipe_buyer'=>3,
				'email'=>$this->input->post('d'),
				'alamat_lengkap'=>$this->input->post('e'),
				'provinsi_id'=>$this->input->post('pr'),
				'kota_id'=>$this->input->post('h'),
				'kecamatan'=>$this->input->post('i'),
				'kode_pos'=>$this->input->post('kp'),
				'no_hp'=>$this->input->post('j'),
				'tanggal_daftar'=>date('Y-m-d H:i:s'));
			$this->model_app->insert('rb_konsumen',$data);
			$id_k = $this->db->insert_id();
			
			
			
			
			$address = array('id_konsumen'=>$id_k,
				'nama_alamat'=>'Rumah',
				'pic'=>$this->db->escape_str(strip_tags($this->input->post('c'))),
				'alamat_lengkap'=>$this->db->escape_str(strip_tags($this->input->post('e'))),
				'kecamatan'=>$this->db->escape_str(strip_tags($this->input->post('i'))),
				'provinsi_id'=>$this->db->escape_str(strip_tags($this->input->post('pr'))),
				'kota_id'=>$this->db->escape_str(strip_tags($this->input->post('h'))),
				'kode_pos'=>$this->db->escape_str(strip_tags($this->input->post('kp'))),
				'no_hp'=>$this->db->escape_str(strip_tags($this->input->post('j'))),
				'alamat_default'=>'1');
			$this->db->insert('alamat',$address);

		
			if ($this->session->id_konsumen =="" ){
				$red = 'members/keranjang';
            }else{

				$lihat_seller = $this->db->query("select id_penjual from temp_penjualan where id_pembeli='".$this->session->id_konsumen."' order by id_penjual")->result_array();

				foreach ($lihat_seller as $row) {

				$cek_seller2 = $this->db->query("select id_penjual from temp_penjualan where id_pembeli='".$id_k."' order by id_penjual")->num_rows();
				if($cek_seller2>'4'){
					 break;
				}

				$cek_seller = $this->db->query("select id_penjual from temp_penjualan where id_pembeli='".$id_k."' and id_penjual='".$row['id_penjual']."'")->num_rows();
				$where2 = array('id_pembeli'=>$this->session->id_konsumen,'id_penjual'=>$row['id_penjual']);
				$data2 	= array('id_pembeli'=>$id_k);

				if($cek_seller > 0){
					$this->model_app->delete('temp_penjualan',$where2);
				}else{
					$this->model_app->update('temp_penjualan', $data2, $where2);
				}

				}

				$lihat_produk = $this->db->query("select a.id_produk prdk from rb_penjualan_temp a join rb_produk b on a.id_produk=b.id_produk where session='".$this->session->id_konsumen."' order by b.id_reseller")->result_array();

				foreach ($lihat_produk as $row) {
					$cek_produk2 = $this->db->query("select a.id_produk prdk from rb_penjualan_temp a join rb_produk b on a.id_produk=b.id_produk where session='".$id_k."' group by b.id_reseller order by b.id_reseller")->num_rows();
				if($cek_produk2>'4'){
					 break;
				}

				$where1 = array('session'=>$this->session->id_konsumen,'id_produk'=>$row['prdk']);
				$data1 	= array('session'=>$id_k);
				$cek_produk = $this->db->query("select id_produk from rb_penjualan_temp where session='".$id_k."' and id_produk='".$row['prdk']."'")->num_rows();
				if($cek_produk > 0){
					$this->model_app->delete('rb_penjualan_temp',$where1);
				}else{
					$this->model_app->update('rb_penjualan_temp', $data1, $where1);
				}
					
					
				}				

				$this->session->unset_userdata('sesi_no_login');
				$red = 'members/checkout/';
			}
			$this->session->set_userdata(array('id_konsumen'=>$id_k, 'level'=>'konsumen'));
			
			redirect($red);

			// if ($this->session->idp!=''){
				// $data = array('kode_transaksi'=>$this->session->idp,
					// 'id_pembeli'=>$id,
					// 'id_penjual'=>$this->session->reseller,
					// 'status_pembeli'=>'konsumen',
					// 'status_penjual'=>'reseller',
					// 'waktu_transaksi'=>date('Y-m-d H:i:s'),
					// 'proses'=>'0');
				// $this->model_app->insert('rb_penjualan',$data);
				// $idp = $this->db->insert_id();

				// $keranjang = $this->model_app->view_where('rb_penjualan_temp',array('session'=>$this->session->idp));
				// foreach ($keranjang->result_array() as $row) {
					// $dataa = array('id_penjualan'=>$idp,
						// 'id_produk'=>$row['id_produk'],
						// 'jumlah'=>$row['jumlah'],
						// 'harga_jual'=>$row['harga_jual'],
						// 'satuan'=>$row['satuan']);
					// $this->model_app->insert('rb_penjualan_detail',$dataa);
				// }

				// $this->db->query("DELETE FROM rb_penjualan_temp where session='".$this->session->idp."'");
				// $this->session->unset_userdata('reseller');
				// $this->session->unset_userdata('idp');
				// $this->session->set_userdata(array('idp'=>$idp));
			// }
			
				
			
		// }elseif (isset($_POST['submit2'])){
		// 	$cek  = $this->model_app->view_where('rb_reseller',array('username'=>$this->input->post('a')))->num_rows();
		// 	if ($cek >= 1){
		// 		$username = $this->input->post('a');
		// 		echo "<script>window.alert('Maaf, Username $username sudah dipakai oleh orang lain!');
  //                                 window.location=('".base_url()."/auth/register')</script>";
		// 	}else{
		// 		$route = array('administrator','agenda','auth','berita','contact','download','gallery','konfirmasi','main','members','page','produk','reseller','testimoni','video');
		// 		if (in_array($this->input->post('a'), $route)){
		// 			$username = $this->input->post('a');
		// 			echo "<script>window.alert('Maaf, Username $username sudah dipakai oleh orang lain!');
	 //                                  window.location=('".base_url()."/".$this->input->post('i')."')</script>";
		// 		}else{
		// 		$data = array('username'=>$this->input->post('a'),
		//         			  'password'=>hash("sha512", md5($this->input->post('b'))),
		//         			  'nama_reseller'=>$this->input->post('c'),
		//         			  'jenis_kelamin'=>$this->input->post('d'),
		//         			  'kota_id'=>$this->input->post('kota'),
		//         			  'alamat_lengkap'=>$this->input->post('e'),
		//         			  'no_telpon'=>$this->input->post('f'),
		// 					  'email'=>$this->input->post('g'),
		// 					  'kode_pos'=>$this->input->post('h'),
		// 					  'referral'=>$this->input->post('i'),
		// 					  'tanggal_daftar'=>date('Y-m-d H:i:s'));
		// 		$this->model_app->insert('rb_reseller',$data);
		// 		$id = $this->db->insert_id();
		// 		$this->session->set_userdata(array('id_reseller'=>$id, 'level'=>'reseller'));
		// 		redirect('reseller/home');
		// 		}
		// }
		
		}else{
			$data['title'] = 'Formulir Pendaftaran';
			$data['provinsi'] = $this->model_app->view_ordering('rb_provinsi','provinsi_id','ASC');
			$this->template->load(template().'/template',template().'/reseller/view_register',$data);
		}
	}

	//update diskon buyer
	public function login(){
		if (isset($_POST['login'])){

			if ($this->session->level == 'reseller') {
				$this->session->set_flashdata('message', '<div class="alert alert-danger"><center>Maaf, Anda sedang login sebagai Merchant, Silahkan logout atau gunakan browser lain, untuk dapat melanjutkan aktifitas belanja anda!<br>Logout <a href = "'.base_url().'auth/logout">disini</a></center></div>');
				$data['title'] = 'User Login';
				$this->template->load(template().'/template',template().'/reseller/view_login',$data);

			}elseif ($this->session->level == 'admin') {
				$this->session->set_flashdata('message', '<div class="alert alert-danger"><center>Maaf, Anda sedang login sebagai Administrator, Silahkan logout atau gunakan browser lain, untuk dapat melanjutkan aktifitas belanja anda!<br>Logout <a href = "'.base_url().'auth/logout">disini</a></center></div>');
				$data['title'] = 'User Login';
				$this->template->load(template().'/template',template().'/reseller/view_login',$data);
			}else{

			$kon = $this->db->query("SELECT * FROM konfigurasi")->result_array();
			foreach($kon as $row){
						$arraydata = array(
							"flag_".$row['nama_konfig'] =>$row['flag'],
							"value1_".$row['nama_konfig'] =>$row['value1'],
							"value2_".$row['nama_konfig'] =>$row['value2'],
						);
						$this->session->set_userdata($arraydata);
					}
			if ($this->session->id_konsumen!='') {
				
				$username = strip_tags($this->input->post('a'));
				$password = hash("sha512", md5(strip_tags($this->input->post('b'))));
				$cek = $this->db->query("SELECT * FROM rb_konsumen where username='".$this->db->escape_str($username)."' AND password='".$this->db->escape_str($password)."'");
				$ro = $cek->row_array();
				$total = $cek->num_rows();
				if ($total > 0){

				$id_k = $ro['id_konsumen'];
				try {
					$ambil_data = $this->db->query("SELECT * FROM temp_penjualan where id_pembeli='".$id_k."' order by id_penjual");
				if($ambil_data->row_array()['kode_transaksi']!='' || $ambil_data->row_array()['kode_transaksi']!=null){
					$idp = $ambil_data->row_array()['kode_transaksi'];
				if($this->midtrans->status($idp)->status_message!=null){
					// $cek_other = $this->model_app->view_where('rb_penjualan_temp',array('kode_transaksi'=>$idp))->result_array();
					// $cek_other2 = $this->model_app->view_where('rb_penjualan_temp',array('kode_transaksi'=>$idp))->row_array(0);
					$data1 = array('order'=>'1');
					$result_midtrans = json_decode(json_encode($this->midtrans->status($idp)),true);
					if($ambil_data->row_array()['pembayaran']==='other_bank'){
						$jenis = 'other_bank';
						$kode = $result_midtrans['va_numbers'][0]['va_number'];
					} else if($result_midtrans['payment_type']=='bank_transfer'){
							$status = 'diperiksa';
							$data1['bayar']='0';	
							if($result_midtrans["permata_va_number"]!=null){
								$jenis = 'permata';
								$kode = $result_midtrans['permata_va_number'];
							}else{
							$jenis = $result_midtrans['va_numbers'][0]['bank'];
							$kode = $result_midtrans['va_numbers'][0]['va_number'];
							}
						}else if($result_midtrans['payment_type']=='echannel'){
							$jenis = "mandiri";
							$kode = $result_midtrans['bill_key'];
							$status = 'diperiksa';
							$data1['bayar']='0';
						}else if($result_midtrans['payment_type']=='cstore'){
							$jenis = $result_midtrans['store'];
							$kode = $result_midtrans['payment_code'];
							$status = 'diperiksa';
							$data1['bayar']='0';
						}else{
							if($result_midtrans['transaction_status']==='settlement'){
							$jenis = $result_midtrans['payment_type'];
							$status = 'lunas';
							$data1['bayar']='1';
							$this->session->set_flashdata('success', 'Pembayaran Berhasil');
							}else if($result_midtrans['transaction_status']==='capture'){
								$jenis = $result_midtrans['payment_type'];
								$status = 'lunas';
								$data1['bayar']='1';
								$this->session->set_flashdata('success', 'Pembayaran Berhasil');
							}else{
							$jenis = $result_midtrans['payment_type'];
							$status = 'gagal';
							$data1['bayar']='2';
							$this->session->set_flashdata('danger', 'Pembayaran Gagal');
							}
						}
					$simpan =array( 'kode_transaksi'	=>$idp,
					'via' 			 	=>'midtrans',
					'jenis_pembayaran'	=> $jenis,
					'kode_pembayaran'	=> $kode,
					'nominal'			=> floor($result_midtrans['gross_amount']),
					'status_pembayaran' => $status,	
					'res_transactionNo'	=> $result_midtrans['transaction_id']);
					$this->model_app->insert('rb_konfirmasi_pembayaran_konsumen',$simpan);
		
					foreach ($ambil_data->result_array() as $row){
					$data1['kode_transaksi']=$idp;
					$data1['pembayaran']= $jenis;
					$data1['kategori_pembayaran']='midtrans';
					$data1['id_pembeli']=$row['id_pembeli'];
					$data1['id_alamat']=$row['id_alamat'];	
					$data1['id_penjual']=$row['id_penjual'];	
					$data1['status_pembeli']='konsumen';	
					$data1['status_penjual']='reseller';
					$data1['kurir']=$row['kurir'];
					$data1['service']=$row['service'];
					$data1['ongkir']=$row['ongkir'];
					$data1['diskon']=$row['diskon'];
					$data1['flag_reseller'] = '0';
					$data1['flag_admin'] = '0';
					$data1['diskon_ongkir']=$row['diskon_ongkir'];
					$data1['asuransi']=$row['asuransi'];
					$data1['komisi_topsonia']=$row['komisi_topsonia'];
					$data1['mdr']=$row['mdr'];
					$data1['pajak']=$row['pajak'];
					$data1['total_bayar']=$row['total_bayar'];
					$data1['waktu_transaksi'] = $row['waktu_transaksi'];
					$data1['waktu_order'] = date('Y-m-d H:i:s');
					$data1["catatan_pelapak"] = $row['catatan_pelapak'];
					$data1["komisi_referal"] = $row['komisi_referal'];
					$data1["diskon_buyer"] = $row['diskon_buyer'];
					
					$this->model_app->insert('rb_penjualan', $data1);
					$id = $this->db->insert_id();

					$query_wil = $this->db->query("SELECT provinsi_id FROM alamat WHERE id_konsumen = '".$this->session->id_konsumen."' AND alamat_default = '1'")->row_array();
					$wilayah = $query_wil['provinsi_id'];
					$bulan = date("m");
					$tahun = date("Y");
					$no_invoice = str_pad($id, 10, '0', STR_PAD_LEFT)."/".$wilayah."/TPS/".$bulan."/".$tahun;

					$data_i = array('no_invoice'=>$no_invoice);
		    		$where = array('id_penjualan' => $id);
		    		$this->model_app->update('rb_penjualan', $data_i, $where);
					
					$produk = $this->db->query("SELECT * FROM rb_penjualan_temp a JOIN rb_produk c ON a.id_produk=c.id_produk JOIN rb_reseller d ON c.id_reseller=d.id_reseller JOIN rb_konsumen e ON a.session=e.id_konsumen JOIN tipe_buyer f ON e.id_tipe_buyer=f.id_tipe_buyer WHERE a.session='".$this->session->id_konsumen."' and d.id_reseller=".$row['id_penjual']." order by d.id_reseller asc");
					foreach($produk->result_array() as $idpr){ 
						$kom = $this->db->query("SELECT round(((a.harga_jual*a.jumlah)*(c.komisi/100))) as diskon_merchant, round(((a.harga_jual*a.jumlah)*(c.komisi/100))*(f.skema_diskon/100)) as diskon_top, e.komisi as diskon_ongkir FROM `rb_penjualan_temp` a join rb_produk b on a.id_produk=b.id_produk join rb_reseller c ON b.id_reseller=c.id_reseller JOIN rb_konsumen e ON a.session=e.id_konsumen JOIN tipe_buyer f ON e.id_tipe_buyer=f.id_tipe_buyer where a.session='".$this->session->id_konsumen."' and a.id_produk='".$idpr['id_produk']."'")->row_array();
						$data2 = array('id_penjualan'=>$id,
										'id_produk'=>$idpr['id_produk'],
										'jumlah'=>$idpr['jumlah'],
										'diskon'=>$idpr['diskon'],
										'harga_jual'=>$idpr['harga_jual'],
										'satuan'=>$idpr['satuan'],
										'diskon_topsonia'=>$kom['diskon_top'],
										'diskon_merchant'=>$kom['diskon_merchant']);
						$this->model_app->insert('rb_penjualan_detail', $data2);
					}	
					}
		
					$this->model_app->delete('temp_penjualan',array('id_pembeli'=>$this->session->id_konsumen));
					$this->model_app->delete('rb_penjualan_temp',array('session'=>$this->session->id_konsumen));
					$this->session->set_userdata(array('id_konsumen'=>$ro['id_konsumen'], 'level'=>'konsumen'));

					if(in_array($jenis,array("other_bank","bca","bri","mandiri","permata","bni","alfamart","indomaret"))){
						redirect('konfirmasi?idp='.$idp);
					}else{
						redirect('members/orders_report');
					}
					}else{ }
					}
				} catch (Exception $e) {
					echo 'error';
				}

				$lihat_seller = $this->db->query("select id_penjual from temp_penjualan where id_pembeli='".$this->session->id_konsumen."' order by id_penjual")->result_array();

				foreach ($lihat_seller as $row) {

				$cek_seller2 = $this->db->query("select id_penjual from temp_penjualan where id_pembeli='".$id_k."' order by id_penjual")->num_rows();
				if($cek_seller2>'4'){
					 break;
				}

				$cek_seller = $this->db->query("select id_penjual from temp_penjualan where id_pembeli='".$id_k."' and id_penjual='".$row['id_penjual']."'")->num_rows();
				$where2 = array('id_pembeli'=>$this->session->id_konsumen,'id_penjual'=>$row['id_penjual']);
				$data2 	= array('id_pembeli'=>$id_k);

				if($cek_seller > 0){
					$this->model_app->delete('temp_penjualan',$where2);
				}else{
					$this->model_app->update('temp_penjualan', $data2, $where2);
				}

				}

				$lihat_produk = $this->db->query("select a.id_produk prdk from rb_penjualan_temp a join rb_produk b on a.id_produk=b.id_produk where session='".$this->session->id_konsumen."' order by b.id_reseller")->result_array();

				foreach ($lihat_produk as $row) {
					$cek_produk2 = $this->db->query("select a.id_produk prdk from rb_penjualan_temp a join rb_produk b on a.id_produk=b.id_produk where session='".$id_k."' group by b.id_reseller order by b.id_reseller")->num_rows();
				if($cek_produk2>'4'){
					 break;
				}

				$where1 = array('session'=>$this->session->id_konsumen,'id_produk'=>$row['prdk']);
				$data1 	= array('session'=>$id_k);
				$cek_produk = $this->db->query("select id_produk from rb_penjualan_temp where session='".$id_k."' and id_produk='".$row['prdk']."'")->num_rows();
				if($cek_produk > 0){
					$this->model_app->delete('rb_penjualan_temp',$where1);
				}else{
					$this->model_app->update('rb_penjualan_temp', $data1, $where1);
				}
					
					
				}		
	
					$this->session->set_userdata(array('id_konsumen'=>$ro['id_konsumen'], 'level'=>'konsumen'));

					redirect('members/checkout');
				}else{
					$url = 'https://idss.ptncs.com/api/AccountAgent.aspx?UserName='.$username;
					//Request API Agent User 
					$curl = curl_init();
					curl_setopt_array($curl, array(
						CURLOPT_URL => "https://idss.ptncs.com/api/AccountAgent.aspx?UserName=".$username,

						CURLOPT_RETURNTRANSFER => true,
						CURLOPT_ENCODING => "",
						CURLOPT_MAXREDIRS => 10,
						CURLOPT_TIMEOUT => 0,
						CURLOPT_FOLLOWLOCATION => true,
						CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
						CURLOPT_CUSTOMREQUEST => "GET",
					));

					$response = curl_exec($curl);
					curl_close($curl);

					$outputjson=json_decode($response,TRUE);
					if(!empty($outputjson)){
						try {
							if($outputjson['StatusCode'] == '200'){

							//insert tabel rb_konsumen
								$data = array('username'=>$this->input->post('a'),
									'password'=>hash("sha512", md5($this->input->post('a'))),
									'nama_lengkap'=>$outputjson['NamaLengkap'],
									'email'=>$outputjson['Email'],
									'alamat_lengkap'=>trim($outputjson['Alamat']),
									'kecamatan'=>$outputjson['Kecamatan'],
									'kota_id'=>$outputjson['Kota'],
									'no_hp'=>$outputjson['Phone'],
									'komisi'=>$outputjson['KomisiAgen'],
									'id_tipe_buyer'=>1,
									'tanggal_daftar'=>date('Y-m-d H:i:s'));
								$this->model_app->insert('rb_konsumen',$data);
								$id_k = $this->db->insert_id();
								
								$lihat_seller = $this->db->query("select id_penjual from temp_penjualan where id_pembeli='".$this->session->id_konsumen."' order by id_penjual")->result_array();

								foreach ($lihat_seller as $row) {

									$cek_seller2 = $this->db->query("select id_penjual from temp_penjualan where id_pembeli='".$id_k."' order by id_penjual")->num_rows();
									if($cek_seller2>'4'){
						 				break;
									}

									$cek_seller = $this->db->query("select id_penjual from temp_penjualan where id_pembeli='".$id_k."' and id_penjual='".$row['id_penjual']."'")->num_rows();
									$where2 = array('id_pembeli'=>$this->session->id_konsumen,'id_penjual'=>$row['id_penjual']);
									$data2 	= array('id_pembeli'=>$id_k);

									if($cek_seller > 0){
										$this->model_app->delete('temp_penjualan',$where2);
									}else{
										$this->model_app->update('temp_penjualan', $data2, $where2);
									}

								}

								$lihat_produk = $this->db->query("select a.id_produk prdk from rb_penjualan_temp a join rb_produk b on a.id_produk=b.id_produk where session='".$this->session->id_konsumen."' order by b.id_reseller")->result_array();

								foreach ($lihat_produk as $row) {
									$cek_produk2 = $this->db->query("select a.id_produk prdk from rb_penjualan_temp a join rb_produk b on a.id_produk=b.id_produk where session='".$id_k."' group by b.id_reseller order by b.id_reseller")->num_rows();
									
									if($cek_produk2>'4'){
					 					break;
									}

									$where1 = array('session'=>$this->session->id_konsumen,'id_produk'=>$row['prdk']);
									$data1 	= array('session'=>$id_k);
									$cek_produk = $this->db->query("select id_produk from rb_penjualan_temp where session='".$id_k."' and id_produk='".$row['prdk']."'")->num_rows();
									
									if($cek_produk > 0){
										$this->model_app->delete('rb_penjualan_temp',$where1);
									}else{
										$this->model_app->update('rb_penjualan_temp', $data1, $where1);
									}
					
					
								}		
								$this->session->set_userdata(array('id_konsumen'=>$id_k, 'level'=>'konsumen'));
								redirect('members/checkout/');
							}else{

							$url = 'https://idss.ptncs.com/api/AccountNCS.aspx?UserName='.$username;
							//Request API Karyawan 
							$curl = curl_init();
							curl_setopt_array($curl, array(
								CURLOPT_URL => "https://idss.ptncs.com/api/AccountNCS.aspx?UserName=".$username,

								CURLOPT_RETURNTRANSFER => true,
								CURLOPT_ENCODING => "",
								CURLOPT_MAXREDIRS => 10,
								CURLOPT_TIMEOUT => 0,
								CURLOPT_FOLLOWLOCATION => true,
								CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
								CURLOPT_CUSTOMREQUEST => "GET",
							));

							$response = curl_exec($curl);
							curl_close($curl);

							$outputjson=json_decode($response,TRUE);
							if(!empty($outputjson)){
								try {
									if($outputjson['StatusCode'] == '200'){

							//insert tabel rb_konsumen
										$data = array('username'=>$this->input->post('a'),
											'password'=>hash("sha512", md5($this->input->post('a'))),
											'nama_lengkap'=>$outputjson['NamaLengkap'],
											'email'=>$outputjson['Email'],
											'alamat_lengkap'=>trim($outputjson['Alamat']),
											'kecamatan'=>$outputjson['Kecamatan'],
											'kota_id'=>$outputjson['Kota'],
											'no_hp'=>$outputjson['Phone'],
											'komisi'=>$outputjson['KomisiAgen'],
											'id_tipe_buyer'=>1,
											'tanggal_daftar'=>date('Y-m-d H:i:s'),
											'kode_referall'=>$this->input->post('a'));
										$this->model_app->insert('rb_konsumen',$data);
										$id_k = $this->db->insert_id();
								
										$lihat_seller = $this->db->query("select id_penjual from temp_penjualan where id_pembeli='".$this->session->id_konsumen."' order by id_penjual")->result_array();

										foreach ($lihat_seller as $row) {

											$cek_seller2 = $this->db->query("select id_penjual from temp_penjualan where id_pembeli='".$id_k."' order by id_penjual")->num_rows();
											if($cek_seller2>'4'){
						 						break;
											}

											$cek_seller = $this->db->query("select id_penjual from temp_penjualan where id_pembeli='".$id_k."' and id_penjual='".$row['id_penjual']."'")->num_rows();
											$where2 = array('id_pembeli'=>$this->session->id_konsumen,'id_penjual'=>$row['id_penjual']);
											$data2 	= array('id_pembeli'=>$id_k);

											if($cek_seller > 0){
												$this->model_app->delete('temp_penjualan',$where2);
											}else{
												$this->model_app->update('temp_penjualan', $data2, $where2);
											}

										}

										$lihat_produk = $this->db->query("select a.id_produk prdk from rb_penjualan_temp a join rb_produk b on a.id_produk=b.id_produk where session='".$this->session->id_konsumen."' order by b.id_reseller")->result_array();

										foreach ($lihat_produk as $row) {
											$cek_produk2 = $this->db->query("select a.id_produk prdk from rb_penjualan_temp a join rb_produk b on a.id_produk=b.id_produk where session='".$id_k."' group by b.id_reseller order by b.id_reseller")->num_rows();
									
											if($cek_produk2>'4'){
					 							break;
											}

											$where1 = array('session'=>$this->session->id_konsumen,'id_produk'=>$row['prdk']);
											$data1 	= array('session'=>$id_k);
											$cek_produk = $this->db->query("select id_produk from rb_penjualan_temp where session='".$id_k."' and id_produk='".$row['prdk']."'")->num_rows();
									
											if($cek_produk > 0){
												$this->model_app->delete('rb_penjualan_temp',$where1);
											}else{
												$this->model_app->update('rb_penjualan_temp', $data1, $where1);
											}
					
					
										}		
										$this->session->set_userdata(array('id_konsumen'=>$id_k, 'level'=>'konsumen'));
										redirect('members/checkout/');
									}else{
										$data['title'] = 'Gagal Login';
										$this->session->set_flashdata('message', '<div class="alert alert-danger"><center>Maaf, Username atau password salah!'.$outputjson['StatusCode'].'</center></div>');
										redirect('auth/login');
									}

								}catch (\Exception $e) {				
									$data['title'] = 'Gagal Login';
									$this->session->set_flashdata('message', '<div class="alert alert-danger"><center>Maaf, Username atau password salah!</center></div>');
									redirect('auth/login');
								}
							}
							}

						}catch (\Exception $e) {				
							$data['title'] = 'Gagal Login';
							$this->session->set_flashdata('message', '<div class="alert alert-danger"><center>Maaf, Username atau password salah!</center></div>');
							redirect('auth/login');
						}
					}
				}
			}else{
				$username = strip_tags($this->input->post('a'));
				$password = hash("sha512", md5(strip_tags($this->input->post('b'))));
				$cek = $this->db->query("SELECT * FROM rb_konsumen where username='".$this->db->escape_str($username)."' AND password='".$this->db->escape_str($password)."'");
				$ro = $cek->row_array();
				$total = $cek->num_rows();
				if ($total > 0){
				$id_k = $ro['id_konsumen'];
				try {
						$ambil_data = $this->db->query("SELECT * FROM temp_penjualan where id_pembeli='".$id_k."' order by id_penjual");
						if($ambil_data->row_array()['kode_transaksi']!='' || $ambil_data->row_array()['kode_transaksi']!=null){
							$idp = $ambil_data->row_array()['kode_transaksi'];
							if($this->midtrans->status($idp)->status_message!=null){
								// $cek_other = $this->model_app->view_where('rb_penjualan_temp',array('kode_transaksi'=>$idp))->result_array();
								// $cek_other2 = $this->model_app->view_where('rb_penjualan_temp',array('kode_transaksi'=>$idp))->row_array(0);
								$data1 = array('order'=>'1');
								$result_midtrans = json_decode(json_encode($this->midtrans->status($idp)),true);
								if($ambil_data->row_array()['pembayaran']==='other_bank'){
									$jenis = 'other_bank';
									$kode = $result_midtrans['va_numbers'][0]['va_number'];
								} else if($result_midtrans['payment_type']=='bank_transfer'){
										$status = 'diperiksa';
										$data1['bayar']='0';	
										if($result_midtrans["permata_va_number"]!=null){
											$jenis = 'permata';
											$kode = $result_midtrans['permata_va_number'];
										}else{
										$jenis = $result_midtrans['va_numbers'][0]['bank'];
										$kode = $result_midtrans['va_numbers'][0]['va_number'];
										}
									}else if($result_midtrans['payment_type']=='echannel'){
										$jenis = "mandiri";
										$kode = $result_midtrans['bill_key'];
										$status = 'diperiksa';
										$data1['bayar']='0';
									}else if($result_midtrans['payment_type']=='cstore'){
										$jenis = $result_midtrans['store'];
										$kode = $result_midtrans['payment_code'];
										$status = 'diperiksa';
										$data1['bayar']='0';
									}else{
										if($result_midtrans['transaction_status']==='settlement'){
										$jenis = $result_midtrans['payment_type'];
										$status = 'lunas';
										$data1['bayar']='1';
										$this->session->set_flashdata('success', 'Pembayaran Berhasil');
										}else if($result_midtrans['transaction_status']==='capture'){
											$jenis = $result_midtrans['payment_type'];
											$status = 'lunas';
											$data1['bayar']='1';
											$this->session->set_flashdata('success', 'Pembayaran Berhasil');
										}else{
										$jenis = $result_midtrans['payment_type'];
										$status = 'gagal';
										$data1['bayar']='2';
										$this->session->set_flashdata('danger', 'Pembayaran Gagal');
										}
									}
								$simpan =array( 'kode_transaksi'	=>$idp,
								'via' 			 	=>'midtrans',
								'jenis_pembayaran'	=> $jenis,
								'kode_pembayaran'	=> $kode,
								'nominal'			=> floor($result_midtrans['gross_amount']),
								'status_pembayaran' => $status,	
								'res_transactionNo'	=> $result_midtrans['transaction_id']);
								$this->model_app->insert('rb_konfirmasi_pembayaran_konsumen',$simpan);
		
								foreach ($ambil_data->result_array() as $row){
								$data1['kode_transaksi']=$idp;
								$data1['pembayaran']= $jenis;
								$data1['kategori_pembayaran']='midtrans';
								$data1['id_pembeli']=$row['id_pembeli'];
								$data1['id_alamat']=$row['id_alamat'];	
								$data1['id_penjual']=$row['id_penjual'];	
								$data1['status_pembeli']='konsumen';	
								$data1['status_penjual']='reseller';
								$data1['kurir']=$row['kurir'];
								$data1['service']=$row['service'];
								$data1['ongkir']=$row['ongkir'];
								$data1['diskon']=$row['diskon'];
								$data1['flag_reseller'] = '0';
								$data1['flag_admin'] = '0';
								$data1['diskon_ongkir']=$row['diskon_ongkir'];
								$data1['asuransi']=$row['asuransi'];
								$data1['komisi_topsonia']=$row['komisi_topsonia'];
								$data1['mdr']=$row['mdr'];
								$data1['pajak']=$row['pajak'];
								$data1['total_bayar']=$row['total_bayar'];
								$data1['waktu_transaksi'] = $row['waktu_transaksi'];
								$data1['waktu_order'] = date('Y-m-d H:i:s');
								$data1["catatan_pelapak"] = $row['catatan_pelapak'];
								$data1["komisi_referal"] = $row['komisi_referal'];
								$data1["diskon_buyer"] = $row['diskon_buyer'];
					
								$this->model_app->insert('rb_penjualan', $data1);
								$id = $this->db->insert_id();

								$query_wil = $this->db->query("SELECT provinsi_id FROM alamat WHERE id_konsumen = '".$this->session->id_konsumen."' AND alamat_default = '1'")->row_array();
								$wilayah = $query_wil['provinsi_id'];
								$bulan = date("m");
								$tahun = date("Y");
								$no_invoice = str_pad($id, 10, '0', STR_PAD_LEFT)."/".$wilayah."/TPS/".$bulan."/".$tahun;

								$data_i = array('no_invoice'=>$no_invoice);
		    					$where = array('id_penjualan' => $id);
		    					$this->model_app->update('rb_penjualan', $data_i, $where);
					
								$produk = $this->db->query("SELECT * FROM rb_penjualan_temp a JOIN rb_produk c ON a.id_produk=c.id_produk JOIN rb_reseller d ON c.id_reseller=d.id_reseller JOIN rb_konsumen e ON a.session=e.id_konsumen JOIN tipe_buyer f ON e.id_tipe_buyer=f.id_tipe_buyer WHERE a.session='".$this->session->id_konsumen."' and d.id_reseller=".$row['id_penjual']." order by d.id_reseller asc");
								foreach($produk->result_array() as $idpr){ 
									$kom = $this->db->query("SELECT round(((a.harga_jual*a.jumlah)*(c.komisi/100))) as diskon_merchant, round(((a.harga_jual*a.jumlah)*(c.komisi/100))*(f.skema_diskon/100)) as diskon_top, e.komisi as diskon_ongkir FROM `rb_penjualan_temp` a join rb_produk b on a.id_produk=b.id_produk join rb_reseller c ON b.id_reseller=c.id_reseller JOIN rb_konsumen e ON a.session=e.id_konsumen JOIN tipe_buyer f ON e.id_tipe_buyer=f.id_tipe_buyer where a.session='".$this->session->id_konsumen."' and a.id_produk='".$idpr['id_produk']."'")->row_array();
									$data2 = array('id_penjualan'=>$id,
													'id_produk'=>$idpr['id_produk'],
													'jumlah'=>$idpr['jumlah'],
													'diskon'=>$idpr['diskon'],
													'harga_jual'=>$idpr['harga_jual'],
													'satuan'=>$idpr['satuan'],
													'diskon_topsonia'=>$kom['diskon_top'],
													'diskon_merchant'=>$kom['diskon_merchant']);
									$this->model_app->insert('rb_penjualan_detail', $data2);
								}	
								}
		
								$this->model_app->delete('temp_penjualan',array('id_pembeli'=>$ro['id_konsumen']));
								$this->model_app->delete('rb_penjualan_temp',array('session'=>$ro['id_konsumen']));
								$this->session->set_userdata(array('id_konsumen'=>$ro['id_konsumen'], 'level'=>'konsumen'));

								if(in_array($jenis,array("other_bank","bca","bri","mandiri","permata","bni","alfamart","indomaret"))){
									redirect('konfirmasi?idp='.$idp);
								}else{
									redirect('members/orders_report');
								}
							}else{ }
						}
					} catch (Exception $e) {
					echo 'error';
					}		
					$this->session->set_userdata(array('id_konsumen'=>$ro['id_konsumen'], 'level'=>'konsumen'));
					redirect('');
				}else{
					$url = 'https://idss.ptncs.com/api/AccountAgent.aspx?UserName='.$username;
					//Request API Agent User 
					$curl = curl_init();
					curl_setopt_array($curl, array(
						CURLOPT_URL => "https://idss.ptncs.com/api/AccountAgent.aspx?UserName=".$username,

						CURLOPT_RETURNTRANSFER => true,
						CURLOPT_ENCODING => "",
						CURLOPT_MAXREDIRS => 10,
						CURLOPT_TIMEOUT => 0,
						CURLOPT_FOLLOWLOCATION => true,
						CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
						CURLOPT_CUSTOMREQUEST => "GET",
					));

					$response = curl_exec($curl);
					curl_close($curl);

					$outputjson=json_decode($response,TRUE);
					if(!empty($outputjson)){
						try {
							if($outputjson['StatusCode'] == '200'){

							//insert tabel rb_konsumen
								$data = array('username'=>$this->input->post('a'),
									'password'=>hash("sha512", md5($this->input->post('a'))),
									'nama_lengkap'=>$outputjson['NamaLengkap'],
									'email'=>$outputjson['Email'],
									'alamat_lengkap'=>trim($outputjson['Alamat']),
									'kecamatan'=>$outputjson['Kecamatan'],
									'kota_id'=>$outputjson['Kota'],
									'no_hp'=>$outputjson['Phone'],
									'komisi'=>$outputjson['KomisiAgen'],
									'id_tipe_buyer'=>1,
									'tanggal_daftar'=>date('Y-m-d H:i:s'));
								$this->model_app->insert('rb_konsumen',$data);
								$id= $this->db->insert_id();
								$this->session->set_userdata(array('id_konsumen'=>$id, 'level'=>'konsumen'));
								redirect('');
							}else{

							$url = 'https://idss.ptncs.com/api/AccountNCS.aspx?UserName='.$username;
							//Request API Karyawan 
							$curl = curl_init();
							curl_setopt_array($curl, array(
								CURLOPT_URL => "https://idss.ptncs.com/api/AccountNCS.aspx?UserName=".$username,

								CURLOPT_RETURNTRANSFER => true,
								CURLOPT_ENCODING => "",
								CURLOPT_MAXREDIRS => 10,
								CURLOPT_TIMEOUT => 0,
								CURLOPT_FOLLOWLOCATION => true,
								CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
								CURLOPT_CUSTOMREQUEST => "GET",
							));

							$response = curl_exec($curl);
							curl_close($curl);

							$outputjson=json_decode($response,TRUE);
							if(!empty($outputjson)){
								try {
									if($outputjson['StatusCode'] == '200'){

							//insert tabel rb_konsumen
										$data = array('username'=>$this->input->post('a'),
											'password'=>hash("sha512", md5($this->input->post('a'))),
											'nama_lengkap'=>$outputjson['NamaLengkap'],
											'email'=>$outputjson['Email'],
											'alamat_lengkap'=>trim($outputjson['Alamat']),
											'kecamatan'=>$outputjson['Kecamatan'],
											'kota_id'=>$outputjson['Kota'],
											'no_hp'=>$outputjson['Phone'],
											'komisi'=>$outputjson['KomisiAgen'],
											'id_tipe_buyer'=>1,
											'tanggal_daftar'=>date('Y-m-d H:i:s'),
											'kode_referall'=>$this->input->post('a'));
										$this->model_app->insert('rb_konsumen',$data);
										$id= $this->db->insert_id();
										$this->session->set_userdata(array('id_konsumen'=>$id, 'level'=>'konsumen'));
										redirect('');
									}else{
										$data['title'] = 'Gagal Login';
										$this->session->set_flashdata('message', '<div class="alert alert-danger"><center>Maaf, Username atau password salah!'.$outputjson['StatusCode'].'</center></div>');
										redirect('auth/login');
									}

								}catch (\Exception $e) {				
									$data['title'] = 'Gagal Login';
									$this->session->set_flashdata('message', '<div class="alert alert-danger"><center>Maaf, Username atau password salah!</center></div>');
									redirect('auth/login');
								}
							}
							}

						}catch (\Exception $e) {				
							$data['title'] = 'Gagal Login';
							$this->session->set_flashdata('message', '<div class="alert alert-danger"><center>Maaf, Username atau password salah!</center></div>');
							redirect('auth/login');
						}
					}
				}
			
			}
		}
		}else{
			$data['title'] = 'User Login';
			if ($this->session->level == 'reseller') {
				$this->session->set_flashdata('message', '<div class="alert alert-danger"><center>Maaf, Anda sedang login sebagai Merchant, Silahkan logout atau gunakan browser lain, untuk dapat melanjutkan aktifitas belanja anda!<br>Logout <a href = "'.base_url().'auth/logout">disini</a></center></div>');
			}elseif ($this->session->level == 'admin') {
				$this->session->set_flashdata('message', '<div class="alert alert-danger"><center>Maaf, Anda sedang login sebagai Administrator, Silahkan logout atau gunakan browser lain, untuk dapat melanjutkan aktifitas belanja anda!<br>Logout <a href = "'.base_url().'auth/logout">disini</a></center></div>');
			}
			
			if($this->session->flashdata('message')!='')
			{
				$this->session->set_flashdata('message', '<div class="alert alert-danger"><center>Maaf, Username atau password salah!</center></div>');
			}
			
			redirect('auth/signin');
			// $this->template->load(template().'/template',template().'/reseller/view_login',$data);
			
		}
	}

	function signin(){
		$data['title'] = 'User Login';
		if ($this->session->level == 'reseller') {
				$this->session->set_flashdata('message', '<div class="alert alert-danger"><center>Maaf, Anda sedang login sebagai Merchant, Silahkan logout atau gunakan browser lain, untuk dapat melanjutkan aktifitas belanja anda!<br>Logout <a href = "'.base_url().'auth/logout">disini</a></center></div>');
			}elseif ($this->session->level == 'admin') {
				$this->session->set_flashdata('message', '<div class="alert alert-danger"><center>Maaf, Anda sedang login sebagai Administrator, Silahkan logout atau gunakan browser lain, untuk dapat melanjutkan aktifitas belanja anda!<br>Logout <a href = "'.base_url().'auth/logout">disini</a></center></div>');
			}
			// $this->template->load(template().'/template',template().'/reseller/view_register',$data);
			$this->template->load(template().'/template',template().'/reseller/view_login',$data);
			// echo "bisa";
	}

	function logout(){
		$this->session->sess_destroy();
		redirect('auth/login');
	}

	public function lupass(){
		if (isset($_POST['lupa'])){
			$email = strip_tags($this->input->post('a'));
			$cek = $this->db->query("SELECT * FROM rb_konsumen where email='".$this->db->escape_str($email)."'");
			$row = $cek->row_array();
			$total = $cek->num_rows();
			if ($total > 0){
				$identitas = $this->db->query("SELECT * FROM identitas where id_identitas='1'")->row_array();
				$randompass = generateRandomString(10);
				$passwordbaru = hash("sha512", md5($randompass));
				$this->db->query("UPDATE rb_konsumen SET password='$passwordbaru' where email='".$this->db->escape_str($email)."'");

				if ($row['jenis_kelamin']=='Laki-laki'){ $panggill = 'Bpk.'; }else{ $panggill = 'Ibuk.'; }
				$email_tujuan = $row['email'];
				$tglaktif = date("d-m-Y H:i:s");
				$subject      = 'Permintaan Reset Password ...';
				$message      = "<html><body>Halooo! <b>$panggill ".$row['nama_lengkap']."</b> ... <br> Hari ini pada tanggal <span style='color:red'>$tglaktif</span> Anda Mengirimkan Permintaan untuk Reset Password
				<table style='width:100%; margin-left:25px'>
				<tr><td style='background:#337ab7; color:#fff; pading:20px' cellpadding=6 colspan='2'><b>Berikut Data Informasi akun Anda : </b></td></tr>
				<tr><td><b>Nama Lengkap</b></td>			<td> : ".$row['nama_lengkap']."</td></tr>
				<tr><td><b>Alamat Email</b></td>			<td> : ".$row['email']."</td></tr>
				<tr><td><b>No Telpon</b></td>				<td> : ".$row['no_hp']."</td></tr>
				<tr><td><b>Jenis Kelamin</b></td>			<td> : ".$row['jenis_kelamin']." </td></tr>
				<tr><td><b>Tempat Lahir</b></td>				<td> : ".$row['tempat_lahir']." </td></tr>
				<tr><td><b>Tanggal Lahir</b></td>			<td> : ".$row['tanggal_lahir']." </td></tr>
				<tr><td><b>Alamat Lengkap</b></td>			<td> : ".$row['alamat_lengkap']." </td></tr>
				<tr><td><b>Waktu Daftar</b></td>			<td> : ".$row['tanggal_daftar']."</td></tr>
				</table>
				<br> Username Login : <b style='color:red'>$row[username]</b>
				<br> Password Login : <b style='color:red'>$randompass</b>
				<br> Silahkan Login di : <a href='$identitas[url]'>$identitas[url]</a> <br>
				Admin, $identitas[nama_website] </body></html> \n";
				
				$this->email->from($identitas['email'], $identitas['nama_website']);
				$this->email->to($email_tujuan);
				$this->email->cc('');
				$this->email->bcc('');

				$this->email->subject($subject);
				$this->email->message($message);
				$this->email->set_mailtype("html");
				$this->email->send();
				
				$config['protocol'] = 'sendmail';
				$config['mailpath'] = '/usr/sbin/sendmail';
				$config['charset'] = 'utf-8';
				$config['wordwrap'] = TRUE;
				$config['mailtype'] = 'html';
				$this->email->initialize($config);

				$data['email'] = $email;
				$data['title'] = 'Permintaan Reset Password Sudah Terkirim...';
				$this->template->load('phpmu-one/template','phpmu-one/view_lupass_success',$data);
			}else{
				$data['email'] = $email;
				$data['title'] = 'Email Tidak Ditemukan...';
				$this->template->load('phpmu-one/template','phpmu-one/view_lupass_error',$data);
			}
		}
	}	
	
	function lupa_password(){
		if (isset($_POST['lupa'])){
			$email = strip_tags($this->input->post('email'));
			$cekemail = $this->model_app->edit('rb_konsumen', array('email' => $email))->num_rows();
			if ($cekemail <= 0){
				$data['title'] = 'Alamat email tidak ditemukan';
				redirect('auth/login');
			}else{
				$iden = $this->model_app->edit('identitas', array('id_identitas' => 1))->row_array();
				// $usr =  $this->model_reseller->profile_konsumen($this->session->id_konsumen)->row_array();
				$usr = $this->model_app->edit('rb_konsumen', array('email' => $email))->row_array();
				$tujuan = $usr['email'];
				$tgl = date("d-m-Y H:i:s");
				$subjek      = 'Lupa Password ';
				$message      = "<html><body>
					<table style='margin-left:25px'>
						<tr><td>Halo $usr[nama_lengkap],<br>
						Seseorang baru saja meminta untuk mengatur ulang kata sandi Anda di Topsonia.<br><br>
						<br>klik   <a href='".base_url().$this->uri->segment(1)."/reset_password/$usr[id_konsumen]'>disini</a> untuk mengganti kata sandi Anda<br><br>
						<br><br>

						Tidak meminta penggantian ini ?<br>
						Abaikan email ini jika Anda tidak meminta untuk reset password baru<br>
						Butuh bantuan ? Hubungi kami <a href='".base_url()."hubungi'>disini</a>
					</table>
				<br>
			       Salam<br>Tim, Topsonia<br><br>
				 
				</body></html> \n";
			echo kirim_email($subjek,$message,$tujuan);
				
				$data['title'] = 'Password terkirim ke '.$usr['email'];
				$this->session->set_flashdata('message', '<div class="alert alert-info"><center>Email berhasil dikirim, silahkan cek email anda untuk reset password</center></div>');
			
				redirect('auth/login');
				//$this->load->view('auth/reset_password',$data);
			}
		}else{
			redirect($this->uri->segment(1));
		}
	}

	function reset_password(){
        if (isset($_POST['reset'])){
            // $usr = $this->model_app->edit('rb_konsumen', array('id_konsumen' =>$this->uri->segment(3)));
			$usr = $this->model_app->edit('rb_konsumen', array('id_konsumen' => $this->input->post('id_konsumen')));
			if ($usr->num_rows()<=1){
			// if (trim($this->input->post('a')) != ''){
                if ($this->input->post('a')=== $this->input->post('b')){
					$data = array('password'=>hash("sha512", md5($this->input->post('a'))));
                    $where = array('id_konsumen' => $this->input->post('id_konsumen'));
					$this->model_app->update('rb_konsumen', $data, $where);

					$row = $usr->row_array();
                    $this->session->set_userdata('upload_image_file_manager',true);
                    $this->session->set_userdata(array('username'=>$row['username'],
                                       'id_tipe_buyer'=>$row['id_tipe_buyer'],
                                       'id_konsumen'=>$id));

					$data['title'] = 'Password Berhasil dirubah';
					$this->session->set_flashdata('message', '<div class="alert alert-success"><center>Password berhasil dirubah, silahkan login ulang </center></div>');
					$this->template->load(template().'/template',template().'/reseller/view_login',$data);
                }else{
                    $data['title'] = 'Password Tidak sama!';
					$this->session->set_flashdata('message', '<div class="alert alert-danger"><center>Password tidak sama</center></div>');
					$this->template->load(template().'/template',template().'/reseller/view_reset',$data);
                }
            }else{
                $data['title'] = 'Terjadi Kesalahan!';
				$this->session->set_flashdata('message', '<div class="alert alert-danger"><center>Password Gagal dirubah</center></div>');
				$this->template->load(template().'/template',template().'/reseller/view_reset',$data);
            }
        }else{
            $this->session->set_userdata(array('id_konsumen'=>$id));
            $data['title'] = 'Reset Password';
			$this->session->set_flashdata('message', '<div class="alert alert-warning"><center>Silahkan masukan password baru anda</center></div>');
			$this->template->load(template().'/template',template().'/reseller/view_reset',$data);
        }
    }
}
