<?php
/*
-- ---------------------------------------------------------------
-- MARKETPLACE MULTI BUYER MULTI SELLER + SUPPORT RESELLER SYSTEM
-- CREATED BY : ROBBY PRIHANDAYA
-- COPYRIGHT  : Copyright (c) 2018 - 2019, PHPMU.COM. (https://phpmu.com/)
-- LICENSE    : http://opensource.org/licenses/MIT  MIT License
-- CREATED ON : 2019-03-26
-- UPDATED ON : 2019-03-27
-- ---------------------------------------------------------------
*/
defined('BASEPATH') OR exit('No direct script access allowed');
class Konfirmasi extends CI_Controller {
	
	function __construct (){	
		parent::__construct();
		// $this->load->library('phpqrcode/qrlib');
		// $this->load->helper('url');
		// $this->_ci = & get_instance();
        // $this->_ci->load->config('phpqrcode/qrlib', TRUE);
		$params = array('server_key' => 'SB-Mid-server-ZITPm-4tKXc1LbcogWCVqPR3', 'production' => false);
		$this->load->library('midtrans');
		$this->midtrans->config($params);
	}
	
	function index(){
		$idp = $this->input->get('idp');

		
		if (isset($_POST['submit'])){
			try{
				$files = $_FILES;
				$_FILES['userfile']['name']= $files['userfile']['name'];
				$_FILES['userfile']['type']= $files['userfile']['type'];
				$_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'];
				$_FILES['userfile']['error']= $files['userfile']['error'];
				$_FILES['userfile']['size']= $files['userfile']['size'];
				$this->load->library('upload');
				$savename = $this->input->post('idk').'.jpg';
				$this->upload->initialize($this->set_upload_options($savename));
				$this->upload->do_upload();
				$fileName = $this->upload->data('file_name');		
			}catch (\Exception $e){				
				$this->session->set_flashdata('warning', $e->getMessage());
				redirect('konfirmasi?idp='.$this->input->post('id'));
				$this->session->set_flashdata('warning', '');
			}
			
			if ($fileName!=''){
				$data = array('kode_transaksi'=>$this->input->post('idk'),
							  'via'=>'manual-transfer',
							  'nominal'=>$this->input->post('b'),
							  'id_rekening'=>$this->input->post('c'),
							  'nama_penerima'=>$this->input->post('rek_penerima'),
							  'nama_pengirim'=>$this->input->post('d'),
							  'tanggal_bayar'=>$this->input->post('e'),
							  'rekening_pengirim'=>$this->input->post('f'),
							  'bukti_bayar'=>$fileName,
							  'status_pembayaran'=>'diperiksa',
							  'waktu_konfirmasi'=>date('Y-m-d H:i:s'));
				$this->model_app->insert('rb_konfirmasi_pembayaran_konsumen',$data);
			}
			
			$data1 = array('bayar'=>'2');
			$where = array('kode_transaksi' => $this->input->post('idk'));
			$this->model_app->update('rb_penjualan', $data1, $where);
			$this->session->set_flashdata('success', 'Berhasil mengupload bukti pembayaran');
			
			$mod = $this->model_app->view_where('rb_penjualan',array('kode_transaksi'=>$this->input->post('idk')))->row_array();
			//email admin
			//$tujuan     		= "yumnahasiany@gmail.com";
			$tujuan     		= "topsonia2021@gmail.com";
			$subject_notif      = "Notifikasi Transaksi Pembelian";
			$message_notif		= "<html><body>Hai, admin<br><br> Kita mempunyai orderan nih,
								<table style='width:100%;border:0px' >
									<tr style='width:30%'><td>No Pemesanan</td><td><b>#".$this->input->post('idk')."</b></td></tr>
									<tr><td>total pembayaran</td><td> Rp ".rupiah($mod['total_bayar'])."</td></tr>
									<tr><td>Bukti Pembayaran</td><td><img src='".base_url().'asset/bukti_transfer/'.$fileName."' style='width:250px'></td></tr>
								</table>
								<br>Pembayaran menggunakan transfer manual ya, Silahkan <b>cek pembayaran pada rekening</b> dan <b>Konfirmasi pembayaran</b> agar transaksi dapat diproses oleh merchant. <br>klik <a href='".base_url()."administrator/index'>disini</a> untuk konfirmasi<br><br>
								Salam,<br> Tim Topsonia </body></html>";
			echo kirim_email($subject_notif,$message_notif,$tujuan);

			
			redirect('members/keranjang_detail/'.$this->input->post('id'));
			$this->session->set_flashdata('success', '');
		}else{
			
			$pembayaran = $this->db->query("SELECT kategori_pembayaran FROM `rb_penjualan` where kode_transaksi='".$idp."'")->row_array()['kategori_pembayaran'];
			
			if($pembayaran == "qris"){
				redirect('konfirmasi/qris?idp='.$idp);
			}else if($pembayaran == "midtrans"){	
				$data['detail'] = $this->model_app->view_where('rb_konfirmasi_pembayaran_konsumen',array('kode_transaksi'=>$idp))->row_array();	
				$data['rows'] = $this->model_app->view_where('rb_penjualan',array('kode_transaksi'=>$idp))->row_array();
				$this->template->load(template().'/template',template().'/reseller/view_konfirmasi_pembayaran_midtrans',$data);
			}else{
				$data['title'] = 'Konfirmasi Orderan anda';
				$data['description'] = description();
				$data['keywords'] = keywords();
				if (isset($_POST['submit1']) OR $idp != ''){

					// if ($_GET['kode']!=''){
						// $kode_transaksi = filter($this->input->get('kode'));
					// }else{
						// $kode_transaksi = filter($this->input->post('a'));
					// }
					if(isset($_POST['submit1'])){
						$kode_transaksi = filter($this->input->post('a'));
						$row = $this->db->query("SELECT a.kode_transaksi, b.id_reseller FROM `rb_penjualan` a jOIN rb_reseller b ON a.id_penjual=b.id_reseller where status_penjual='reseller' AND a.kode_transaksi='$kode_transaksi'")->row_array();
						$idp = $row['kode_transaksi'];
					}else{
						$cek = $this->db->query("select bukti_bayar from rb_konfirmasi_pembayaran_konsumen where kode_transaksi='$idp'"); 
						if($cek->num_rows()>0){
							if($cek->row_array()['bukti_bayar']!=null || $cek->row_array()['bukti_bayar']!=''){
								redirect('members/orders_report');
							}
						}
					}
					
					$data['record'] = $this->model_app->view('rb_rekening');
					// $data['total'] = $this->db->query("SELECT sum((a.harga_jual*a.jumlah)-a.diskon) as total, a.id_penjualan FROM `rb_penjualan_detail` a where a.id_penjualan='".$row['id_penjualan']."'")->row_array();
					// $data['total'] = $this->db->query("SELECT sum((a.harga_jual*a.jumlah)-a.diskon) as total, a.id_penjualan FROM `rb_penjualan_detail` a where a.id_penjualan='".$idp."'")->row_array();
					// $data['rows'] = $this->model_app->view_where('rb_penjualan',array('id_penjualan'=>$row['id_penjualan']))->row_array();
					$data['rows'] = $this->model_app->view_where('rb_penjualan',array('kode_transaksi'=>$idp))->row_array();
					$this->template->load(template().'/template',template().'/reseller/view_konfirmasi_pembayaran',$data);
				}else{
					$this->template->load(template().'/template',template().'/reseller/view_konfirmasi_pembayaran',$data);
				}
			}
			
			
		}
	}
	
	private function set_upload_options($new_name){
        $config = array();
        $config['upload_path'] = 'asset/bukti_transfer/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '5000'; // kb
		$config['file_name'] = $new_name;
        $config['encrypt_name'] = FALSE;
        $this->load->library('upload', $config);
      return $config;
    }
	
	public function func_resize($filename, $source_image, $width, $height){
		$this->image_moo
			->load($source_image.$filename)
			->stretch($width, $height)
			->save($source_image.$filename, $overwrite=TRUE);
		echo $this->image_moo->display_errors();
		chmod($source_image.$filename, 0777);
	}
	
	function refund(){
		$idp = $this->input->get('idp');
		if (isset($_POST['submit'])){

			$data = array('kode_transaksi'=>$this->input->post('id'),
			'no_rekening_penerima'		  =>$this->input->post('b'),
			'id_bank' 					  =>$this->input->post('a'),
			'nama_penerima'				  =>$this->input->post('c'),
			'waktu_pengajuan_refund'	  =>date('Y-m-d H:i:s'),
			'flag'						  =>'1');
			$this->model_app->insert('rb_konfirmasi_refund',$data);

			$data1 = array('status_pembayaran' => 'proses refund');
			$where = array('kode_transaksi' => $this->input->post('id'));
			$this->model_app->update('rb_konfirmasi_pembayaran_konsumen', $data1, $where);
			
			$this->session->set_flashdata('success', 'Berhasil mengirim permintaan refund');
			redirect('members/transaksi_detail/'.$this->input->post('id'));
			$this->session->set_flashdata('success', '');
		}else{
			$data['record'] = $this->model_app->view('bank');
			$data['rows'] = $this->model_app->view_where('rb_penjualan',array('kode_transaksi'=>$idp))->row_array();;
			$data['rowse'] = $this->model_app->view_where('rb_konfirmasi_refund',array('kode_transaksi'=>$idp))->row_array();;
			$this->template->load(template().'/template',template().'/reseller/view_konfirmasi_refund',$data);
		}
	}
	
	function qris(){		
		$idp = $this->input->get('idp');
		$QRISCode = '';
		$transactionNo = '';
		
		$url = 'https://api.ptncs.com/bot/qrisPayment/nobu/bm9idWxpdmV2';
		
		//getAmountTrx
		
		$query = $this->db->query("SELECT a.kode_transaksi, a.total_bayar, DATE_ADD(waktu_order, INTERVAL 5 MINUTE) as TimeOrder, no_transaksi_qris FROM `rb_penjualan` a JOIN `rb_penjualan_detail` b ON a.id_penjualan = b.id_penjualan WHERE a.kode_transaksi='".$idp."'")->row_array();
		
		$filename = $query['kode_transaksi'].".png";
		$data['totalbayar'] = $query['total_bayar'];
		// $data['totalbayar'] = 1;
		$data['idp'] = $idp;
		$data['kode_trx'] = $query['kode_transaksi'];
		$timeoffer = (strtotime($query['TimeOrder']) - time()) / 60;
		
		if(file_exists("./asset/qrcode/$filename") == false){		
			generateQRIS:
			
			//Request API QRIS 

			$data_array =  array(
								  "referenceNo" => $query['kode_transaksi'],
								  "amount" => $query['total_bayar'],
								  "reqtype" => "TOP",
								  "user" => str_pad($this->session->id_konsumen, 5, '0', STR_PAD_LEFT),
								  "customername" => "NCS"
								);
			$data_string = json_encode($data_array);
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			
			$server_output = curl_exec($ch);
			
			curl_close($ch);	
			
			$outputjson = json_decode($server_output, true);
			
			try {
				if($outputjson['responseCode'] == '200'){
					$QRISCode = $outputjson['QRISCode'];
					$data['no_qris'] = $outputjson['transactionNo'];
					//SAVE transactionNo
					$data1 = array(
						'no_transaksi_qris'=>$outputjson['transactionNo'],
						'waktu_order'=>date('Y-m-d H:i:s'));
					$where1 = array('kode_transaksi'=>$idp);
					$this->model_app->update('rb_penjualan', $data1, $where1);
					
				}else{
					$data['status'] = "gagal request api (".$outputjson['messageDetail'].")";
					$data['no_qris'] = "";
					goto finishline;
				}
			}catch (\Exception $e) {				
				$data['status'] = "gagal request api (".$e->getMessage().")";
				goto finishline;
			}
				
			
			//Generate QR Code
			
			$this->load->library('Ciqrcode'); //pemanggilan library QR CODE
	 
			$config['cacheable']    = true; //boolean, the default is true
			$config['cachedir']     = './asset/'; //string, the default is application/cache/
			$config['errorlog']     = './asset/'; //string, the default is application/logs/
			$config['imagedir']     = './asset/qrcode/'; //direktori penyimpanan qr code
			$config['quality']      = true; //boolean, the default is true
			$config['size']         = '1024'; //interger, the default is 1024
			$config['black']        = array(224,255,255); // array, default is array(255,255,255)
			$config['white']        = array(70,130,180); // array, default is array(0,0,0)
			$this->ciqrcode->initialize($config);
	 
			$image_name=$query['kode_transaksi'].'.png'; //buat name dari qr code sesuai dengan nip
	 
			$params['data'] = $QRISCode;
			$params['level'] = 'M'; //H=High
			$params['size'] = 5;
			$params['savename'] = FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE 
						
			$resultqr = $this->ciqrcode->generate($params); // fungsi untuk generate QR CODE	
			
			
			if($resultqr != ""){
				$data['status'] = "ok";
			}else{
				$data['status'] = "gagal generate barcode";
			}
		}else{
			if($timeoffer > 0){
				$data['status'] = "ok";
				$data['no_qris'] = $query['no_transaksi_qris'];
			}else{
				$url = 'https://api.ptncs.com/bot/paymentStatus/nobu/bm9idWxpdmV2';
				// $transactionNo = $query['no_transaksi_qris'];
				
				$data_array =  array("transactionNo" => $query['no_transaksi_qris']);
				$data_string = json_encode($data_array);
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				
				$server_output = curl_exec($ch);		
				curl_close($ch);	
				
				$outputjson = json_decode($server_output, true);
				
				try {
					if($outputjson['responseStatus'] == 'Success'){
						
						//update flag proses
						$data1 = array('bayar'=>'1');
						$where = array('kode_transaksi' => $idp);
						$this->model_app->update('rb_penjualan', $data1, $where);
						
						//insert rb_konfirmasi_pembayaran_konsumen
						$insert_data = array('kode_transaksi'=>$query['kode_transaksi'],
									  'via'=>'qris',
									  'nominal'=>$outputjson['data']['amount'],
									  'nama_pengirim'=>$outputjson['data']['payerName'],
									  'tanggal_bayar'=>$outputjson['data']['paymentDate'],
									  'res_transactionNo'=>$outputjson['data']['transactionNo'],
									  'res_paymentReferenceNo'=>$outputjson['data']['paymentReferenceNo'],
									  'res_issuerID'=>$outputjson['data']['issuerID'],
									  'res_paymentDate'=>$outputjson['data']['paymentDate'],
									   'status_pembayaran'=>'lunas',
									  'waktu_konfirmasi'=>date('Y-m-d H:i:s'));
						$this->model_app->insert('rb_konfirmasi_pembayaran_konsumen',$insert_data);
						
						//parsing data
						// $data['status']= "sukses";
						// $data['record'] = $this->model_app->view_join_where('rb_penjualan','rb_konfirmasi_pembayaran_konsumen','id_penjualan',array('rb_penjualan.id_penjualan'=>$idp),'rb_penjualan.id_penjualan','ASC');
						// $this->template->load(template().'/template',template().'/reseller/members/view_qris_status',$data);
						
						
						redirect('konfirmasi/qrisStatus?idp='.$idp.'&status=sukses');
						
					}else{
						$url = 'https://api.ptncs.com/bot/qrisPayment/nobu/bm9idWxpdmV2';
						goto generateQRIS;
					}
				}catch (\Exception $e) {				
					//pesan error
				}
			}
		}
		
		finishline:
		
		$data['record'] = $this->model_app->view_where('rb_penjualan',array('kode_transaksi'=>$idp))->row_array();
		
		$this->template->load(template().'/template',template().'/reseller/members/view_qris_code',$data);
		
	}
	
	function checkStatusPayment(){
		// redirect('konfirmasi/qrisStatus');
		//goto complete;
		
		$url = 'https://api.ptncs.com/bot/paymentStatus/nobu/bm9idWxpdmV2';
		$transactionNo = $this->input->get('qrisno');
		$idp = $this->input->get('idp');
		
		$data_array =  array("transactionNo" => $transactionNo);
		$data_string = json_encode($data_array);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		
		$server_output = curl_exec($ch);		
		curl_close($ch);	
		
		$outputjson = json_decode($server_output, true);
		
		try {
			if($outputjson['responseStatus'] == 'Success'){
				
				//update flag proses
				$data1 = array('bayar'=>'1');
				$where = array('kode_transaksi' => $idp);
				$this->model_app->update('rb_penjualan', $data1, $where);
				
				//insert rb_konfirmasi_pembayaran_konsumen
				$insert_data = array('kode_transaksi'=>$idp,
			        		  'via'=>'qris',
			        		  'nominal'=>$outputjson['data']['amount'],
			        		  'nama_pengirim'=>$outputjson['data']['payerName'],
			        		  'tanggal_bayar'=>$outputjson['data']['paymentDate'],
			        		  'res_transactionNo'=>$outputjson['data']['transactionNo'],
			        		  'res_paymentReferenceNo'=>$outputjson['data']['paymentReferenceNo'],
			        		  'res_issuerID'=>$outputjson['data']['issuerID'],
			        		  'res_paymentDate'=>$outputjson['data']['paymentDate'],
							  'status_pembayaran'=>'lunas',
			        		  'waktu_konfirmasi'=>date('Y-m-d H:i:s'));
				$this->model_app->insert('rb_konfirmasi_pembayaran_konsumen',$insert_data);
				
				//parsing data
				$data['status']= "sukses";
				$data['record'] = $this->model_app->view_join_where('rb_penjualan','rb_konfirmasi_pembayaran_konsumen','kode_transaksi',array('rb_penjualan.kode_transaksi'=>$idp),'rb_penjualan.kode_transaksi','ASC');
				
				
				
			}else{
				$data['status']= "gagal";
			}
			
			complete:
			$data['idp'] = $this->input->get('idp');
			$this->template->load(template().'/template',template().'/reseller/members/view_qris_status',$data);
		}catch (\Exception $e) {				
		
		}
	}
	
	function checkStatusRealtime(){
		$url = 'https://api.ptncs.com/bot/paymentStatus/nobu/bm9idWxpdmV2';
		$transactionNo = $this->input->get('qrisno');
		$idp = $this->input->get('idp');
		
		$data_array =  array("transactionNo" => $transactionNo);
		$data_string = json_encode($data_array);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		
		$server_output = curl_exec($ch);		
		curl_close($ch);	
		
		$outputjson = json_decode($server_output, true);
		try {
			if($outputjson['responseStatus'] == 'Success'){
				//update flag proses
				$data1 = array('bayar'=>'1');
				$where = array('kode_transaksi' => $idp);
				$this->model_app->update('rb_penjualan', $data1, $where);
				
				//insert rb_konfirmasi_pembayaran_konsumen
				$data = array('kode_transaksi'=>$idp,
							  'via'=>'qris',
							  'nominal'=>$outputjson['data']['amount'],
							  'nama_pengirim'=>$outputjson['data']['payerName'],
							  'tanggal_bayar'=>$outputjson['data']['paymentDate'],
							  'res_transactionNo'=>$outputjson['data']['transactionNo'],
							  'res_paymentReferenceNo'=>$outputjson['data']['paymentReferenceNo'],
							  'res_issuerID'=>$outputjson['data']['issuerID'],
							  'res_paymentDate'=>$outputjson['data']['paymentDate'],
							  'status_pembayaran'=>'lunas',
							  'waktu_konfirmasi'=>date('Y-m-d H:i:s'));
				$this->model_app->insert('rb_konfirmasi_pembayaran_konsumen',$data);
			}
	
			echo $server_output;
			
		}catch (\Exception $e) {				
		
		}
	}
	
	function checkStatusManual(){
		$url = 'https://api.ptncs.com/bot/paymentStatus/nobu/bm9idWxpdmV2';
		$transactionNo = $this->input->get('qrisno');
		$idp = $this->input->get('idp');
		
		$data_array =  array("transactionNo" => $transactionNo);
		$data_string = json_encode($data_array);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		
		$server_output = curl_exec($ch);		
		curl_close($ch);	
		
		$outputjson = json_decode($server_output, true);
		
		try {
			if($outputjson['responseStatus'] == 'Success'){
				
				$data['index'] += "3";
				//update flag proses
				$data1 = array('bayar'=>'1');
				$where = array('kode_transaksi' => $idp);
				$this->model_app->update('rb_penjualan', $data1, $where);
				
				//insert rb_konfirmasi_pembayaran_konsumen
				$data = array('kode_transaksi'=>$idp,
			        		  'via'=>'qris',
			        		  'nominal'=>$outputjson['data']['amount'],
			        		  'nama_pengirim'=>$outputjson['data']['payerName'],
			        		  'tanggal_bayar'=>$outputjson['data']['paymentDate'],
			        		  'res_transactionNo'=>$outputjson['data']['transactionNo'],
			        		  'res_paymentReferenceNo'=>$outputjson['data']['paymentReferenceNo'],
			        		  'res_issuerID'=>$outputjson['data']['issuerID'],
			        		  'res_paymentDate'=>$outputjson['data']['paymentDate'],
							  'status_pembayaran'=>'lunas',
			        		  'waktu_konfirmasi'=>date('Y-m-d H:i:s'));
				$this->model_app->insert('rb_konfirmasi_pembayaran_konsumen',$data);
				
				//parsing data
				$data['status']= "sukses";
				$data['record'] = $this->model_app->view_join_where('rb_penjualan','rb_konfirmasi_pembayaran_konsumen','kode_transaksi',array('rb_penjualan.kode_transaksi'=>$idp),'rb_penjualan.kode_transaksi','ASC');
				
				redirect('konfirmasi/qrisStatus?idp='.$idp.'&status=sukses');
				
			}else{
				redirect('konfirmasi/qris?idp='.$idp);
			}
			
		}catch (\Exception $e) {				
		
		}
	}
	
	function qrisStatus(){
		$idp = $this->input->get('idp');
		$status = $this->input->get('status');
		
		$data['status']= $status ;
		$data['idp'] = $idp;
		
		$data['record'] = $this->model_app->view_join_where('rb_penjualan','rb_konfirmasi_pembayaran_konsumen','kode_transaksi',array('rb_penjualan.kode_transaksi'=>$idp),'rb_penjualan.kode_transaksi','ASC');
		 
		$this->template->load(template().'/template',template().'/reseller/members/view_qris_status',$data);
	}

	function tracking(){
		if (isset($_POST['submit1']) OR $this->uri->segment(3)!=''){
			if ($this->uri->segment(3)!=''){
				$kode_transaksi = filter($this->uri->segment(3));
			}else{
				$kode_transaksi = filter($this->input->post('a'));
			}

			$cek = $this->model_app->view_where('rb_penjualan',array('id_penjualan'=>$kode_transaksi));
			if ($cek->num_rows()>=1){
				$data['title'] = 'Tracking Order '.$kode_transaksi;
				$data['description'] = description();
				$data['keywords'] = keywords();
				$data['rows'] = $this->db->query("SELECT * FROM rb_penjualan a JOIN rb_konsumen b ON a.id_pembeli=b.id_konsumen JOIN rb_kota c ON b.kota_id=c.kota_id where a.kode_transaksi='$kode_transaksi'")->row_array();
				$data['record'] = $this->db->query("SELECT a.kode_transaksi, b.*, c.nama_produk, c.satuan, c.berat, c.produk_seo FROM `rb_penjualan` a JOIN rb_penjualan_detail b ON a.id_penjualan=b.id_penjualan JOIN rb_produk c ON b.id_produk=c.id_produk where a.kode_transaksi='".$kode_transaksi."'");
				$data['total'] = $this->db->query("SELECT a.kode_transaksi, a.kurir, a.service, a.proses, a.ongkir, a.total_bayar as total, a.diskon as diskon_total, sum(c.berat*b.jumlah) as total_berat FROM `rb_penjualan` a JOIN rb_penjualan_detail b ON a.id_penjualan=b.id_penjualan JOIN rb_produk c ON b.id_produk=c.id_produk where a.kode_transaksi='".$kode_transaksi."'")->row_array();
				$this->template->load(template().'/template',template().'/reseller/view_tracking_view',$data);
			}else{
				redirect('konfirmasi/tracking');
			}
		}else{
			$data['title'] = 'Tracking Order';
			$data['description'] = description();
			$data['keywords'] = keywords();
			$this->template->load(template().'/template',template().'/reseller/view_tracking',$data);
		}
	}
}
