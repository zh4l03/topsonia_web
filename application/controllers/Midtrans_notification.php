<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class midtrans_notification extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */


	public function __construct()
    {
        parent::__construct();
        $params = array('server_key' => 'Mid-server-JKGstAIU1IJ-6OkFQAU-0rbB', 'production' => true);
		$this->load->library('veritrans');
		$this->veritrans->config($params);
		$this->load->helper('url');
		
    }

	public function index()
	{
			$json_result = file_get_contents('php://input');
			$result = json_decode($json_result,"true");
			$transaction = $result['transaction_status'];
			$order_id =  $result['order_id'];
			
			if ($transaction == 'capture') {
				$this->model_app->update('rb_penjualan',array('bayar'=>'1'), array('kode_transaksi'=>$order_id,'bayar'=>'0'));
				$this->model_app->update('rb_konfirmasi_pembayaran_konsumen',array('status_pembayaran'=>'lunas'), array('kode_transaksi'=>$order_id,'status_pembayaran'=>'diperiksa'));

		  	}else if ($transaction == 'settlement'){
		  		$this->model_app->update('rb_penjualan',array('bayar'=>'1'), array('kode_transaksi'=>$order_id,'bayar'=>'0'));
				$this->model_app->update('rb_konfirmasi_pembayaran_konsumen',array('status_pembayaran'=>'lunas'), array('kode_transaksi'=>$order_id,'status_pembayaran'=>'diperiksa'));
		  	} else if($transaction == 'pending'){
		  	
		 	} else if ($transaction == 'deny') {
		 	 	$this->model_app->update('rb_penjualan',array('bayar'=>'2','batal'=>'1'), array('kode_transaksi'=>$order_id));
				$this->model_app->update('rb_konfirmasi_pembayaran_konsumen',array('status_pembayaran'=>'gagal'), array('kode_transaksi'=>$order_id,'status_pembayaran'=>'diperiksa'));	
			}else if ($transaction == 'expire') {
		 	 	$this->model_app->update('rb_penjualan',array('bayar'=>'2','batal'=>'1'), array('kode_transaksi'=>$order_id));
				$this->model_app->update('rb_konfirmasi_pembayaran_konsumen',array('status_pembayaran'=>'gagal'), array('kode_transaksi'=>$order_id,'status_pembayaran'=>'diperiksa'));	
			}	
				

	}
}