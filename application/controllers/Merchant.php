<?php
/*
-- ---------------------------------------------------------------
-- MARKETPLACE MULTI BUYER MULTI SELLER + SUPPORT RESELLER SYSTEM
-- CREATED BY : ROBBY PRIHANDAYA
-- COPYRIGHT  : Copyright (c) 2018 - 2019, PHPMU.COM. (https://phpmu.com/)
-- LICENSE    : http://opensource.org/licenses/MIT  MIT License
-- CREATED ON : 2019-03-26
-- UPDATED ON : 2019-03-27
-- ---------------------------------------------------------------
*/
defined('BASEPATH') OR exit('No direct script access allowed');
class Merchant extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('Pdf');

	}

	function index(){
		if (isset($_POST['submit'])){
			if ($this->session->level == 'konsumen') {
				$this->session->set_flashdata('message', '<div class="alert alert-danger"><center>Maaf, Anda sedang login sebagai Pembeli, Silahkan logout atau gunakan browser lain, untuk dapat melanjutkan aktifitas anda!<br>Logout <a href = "'.base_url().$this->uri->segment(1).'/logout">disini ??</a></center></div>');
				$data['title'] = 'Pelapak &rsaquo; Log In';
				$this->load->view($this->uri->segment(1).'/view_login',$data);
			}elseif ($this->session->level == 'admin') {
				$this->session->set_flashdata('message', '<div class="alert alert-danger"><center>Maaf, Anda sedang login sebagai Administrator, Silahkan logout atau gunakan browser lain, untuk dapat melanjutkan aktifitas anda!<br>Logout <a href = "'.base_url().$this->uri->segment(1).'/logout">disini ??</a></center></div>');
				$data['title'] = 'Pelapak &rsaquo; Log In';
				$this->load->view($this->uri->segment(1).'/view_login',$data);
			}else{
				
				if ($this->input->post() && (strtolower($this->input->post('security_code')) == strtolower($this->session->userdata('mycaptcha')))) {
				// SkipCaptcha:
				$username = $this->input->post('a');
				$password = hash("sha512", md5($this->input->post('b')));
				$cek = $this->db->query("SELECT * FROM rb_reseller where username='".$this->db->escape_str($username)."' AND password='".$this->db->escape_str($password)."'");
				$row = $cek->row_array();
				$total = $cek->num_rows();
				if ($total > 0){
					$this->session->set_userdata(array('id_reseller'=>$row['id_reseller'],
					'username'=>$row['username'],
					'level'=>'reseller'));
					redirect($this->uri->segment(1).'/home');
				}else{
					$data['title'] = 'Pelapak &rsaquo; Log In';
					echo $this->session->set_flashdata('message', '<div class="alert alert-danger"><center>Username dan Password Salah!!</center></div>');
					redirect($this->uri->segment(1).'/index');
				}
				}else{
					echo $this->session->set_flashdata('message', '<div class="alert alert-danger"><center>Security Code salah!</center></div>');
					redirect($this->uri->segment(1).'/index');
				}
			}
		}else{
			// echo $this->session->set_flashdata('message', '<div class="alert alert-danger"><center>Session expired silahkan login ulang</center></div>');
			if ($this->session->level!=''){

				$this->load->helper('captcha');
    			$vals = array(
        			'img_path'   => './captcha/',
        			'img_url'    => base_url().'captcha/',
        			'font_path' => base_url().'asset/Tahoma.ttf',
        			'font_size'     => 17,
        			'img_width'  => '320',
        			'img_height' => 33,
        			'border' => 0,
        			'word_length'   => 5,
        			'expiration' => 7200
    			);
     			$cap = create_captcha($vals);
     			$data['image'] = $cap['image'];
     			$this->session->set_userdata('mycaptcha', $cap['word']);

				if ($this->session->level == 'konsumen') {
					$this->session->set_flashdata('message', '<div class="alert alert-danger"><center>Maaf, Anda sedang login sebagai Pembeli, Silahkan logout atau gunakan browser lain, untuk dapat melanjutkan aktifitas anda!<br>Logout <a href = "'.base_url().$this->uri->segment(1).'/logout">disini ??</a></center></div>');
				}elseif ($this->session->level == 'admin') {
					$this->session->set_flashdata('message', '<div class="alert alert-danger"><center>Maaf, Anda sedang login sebagai Administrator, Silahkan logout atau gunakan browser lain, untuk dapat melanjutkan aktifitas anda!<br>Logout <a href = "'.base_url().$this->uri->segment(1).'/logout">disini ??</a></center></div>');
				}

				$data['title'] = 'Pelapak &rsaquo; Log In';
				$this->load->view($this->uri->segment(1).'/view_login',$data);
			}else{

				$this->load->helper('captcha');
    			$vals = array(
        			'img_path'   => './captcha/',
        			'img_url'    => base_url().'captcha/',
        			'font_path' => base_url().'asset/Tahoma.ttf',
        			'font_size'     => 17,
        			'img_width'  => '320',
        			'img_height' => 33,
        			'border' => 0,
        			'word_length'   => 5,
        			'expiration' => 7200
    			);
     			$cap = create_captcha($vals);
     			$data['image'] = $cap['image'];
     			$this->session->set_userdata('mycaptcha', $cap['word']);

				if ($this->session->level == 'konsumen') {
					$this->session->set_flashdata('message', '<div class="alert alert-danger"><center>Maaf, Anda sedang login sebagai Pembeli, Silahkan logout atau gunakan browser lain, untuk dapat melanjutkan aktifitas anda!<br>Logout <a href = "'.base_url().$this->uri->segment(1).'/logout">disini ??</a></center></div>');
				}elseif ($this->session->level == 'admin') {
					$this->session->set_flashdata('message', '<div class="alert alert-danger"><center>Maaf, Anda sedang login sebagai Administrator, Silahkan logout atau gunakan browser lain, untuk dapat melanjutkan aktifitas anda!<br>Logout <a href = "'.base_url().$this->uri->segment(1).'/logout">disini ??</a></center></div>');
				}

				$data['title'] = 'Pelapak &rsaquo; Log In';
				$this->load->view($this->uri->segment(1).'/view_login',$data);
			}
		}
	}

	function ref(){
		if (isset($_POST['submit2'])){
			$cek  = $this->model_app->view_where('rb_reseller',array('username'=>$this->input->post('a')))->num_rows();
			if ($cek >= 1){
				$username = $this->input->post('a');
				echo "<script>window.alert('Maaf, Username $username sudah dipakai oleh orang lain!');
				window.location=('".base_url()."/".$this->input->post('i')."')</script>";
			}else{
				$route = array('administrator','agenda','auth','berita','contact','download','gallery','konfirmasi','main','members','page','produk','reseller','testimoni','video');
				if (in_array($this->input->post('a'), $route)){
					$username = $this->input->post('a');
					echo "<script>window.alert('Maaf, Username $username sudah dipakai oleh orang lain!');
					window.location=('".base_url()."/".$this->input->post('i')."')</script>";
				}else{
					$data = array('username'=>$this->input->post('a'),
					'password'=>hash("sha512", md5($this->input->post('b'))),
					'nama_reseller'=>$this->input->post('c'),
					'jenis_kelamin'=>$this->input->post('d'),
					'alamat_lengkap'=>$this->input->post('e'),
					'no_telpon'=>$this->input->post('f'),
					'email'=>$this->input->post('g'),
					'kode_pos'=>$this->input->post('h'),
					'referral'=>$this->input->post('i'),
					'tanggal_daftar'=>date('Y-m-d H:i:s'));
					$this->model_app->insert('rb_reseller',$data);
					$id = $this->db->insert_id();
					$this->session->set_userdata(array('id_reseller'=>$id, 'level'=>'reseller'));
					$identitas = $this->model_app->view_where('identitas',array('id_identitas'=>'1'))->row_array();

					$ref = $this->model_app->view_where('rb_reseller',array('username'=>$this->input->post('i')))->row_array();
					$email_tujuan = $ref['email'];
					$tglaktif = date("d-m-Y H:i:s");
					$subject      = 'Pendaftaran Sebagai Reseller Berhasil...';

					$message      = "<html><body>Selamat, Pada Hari ini tanggal $tglaktif<br> Bpk/Ibk <b>".$this->input->post('c')."</b> Sukses Mendafatar Sebagai reseller dengan referral <b>".$ref['nama_reseller']."</b>...";
					$message      .= "<table style='width:100%; margin-left:25px'>
					<tr><td style='background:#337ab7; color:#fff; pading:20px' cellpadding=6 colspan='2'><b>Berikut Informasi akun : </b></td></tr>
					<tr><td><b>Nama Reseller</b></td>			<td> : ".$this->input->post('c')."</td></tr>
					<tr><td><b>Alamat Email</b></td>			<td> : ".$this->input->post('g')."</td></tr>
					<tr><td><b>No Telpon</b></td>				<td> : ".$this->input->post('f')."</td></tr>
					<tr><td><b>Jenis Kelamin</b></td>			<td> : ".$this->input->post('d')." </td></tr>
					<tr><td><b>Alamat Lengkap</b></td>			<td> : ".$this->input->post('e')." </td></tr>
					<tr><td><b>Kode Pos</b></td>				<td> : ".$this->input->post('h')." </td></tr>
					<tr><td><b>Waktu Daftar</b></td>			<td> : ".date('Y-m-d H:i:s')."</td></tr>
					</table><br>

					Admin, $identitas[nama_website] </body></html> \n";

					$this->email->from($identitas['email'], $identitas['nama_website']);
					$this->email->to($email_tujuan,$this->input->post('g'));
					$this->email->cc('');
					$this->email->bcc('');

					$this->email->subject($subject);
					$this->email->message($message);
					$this->email->set_mailtype("html");
					$this->email->send();

					$config['protocol'] = 'sendmail';
					$config['mailpath'] = '/usr/sbin/sendmail';
					$config['charset'] = 'utf-8';
					$config['wordwrap'] = TRUE;
					$config['mailtype'] = 'html';
					$this->email->initialize($config);

					redirect($this->uri->segment(1).'/home');
				}
			}
		}
	}

	function home(){
		$this->template->load($this->uri->segment(1).'/template',$this->uri->segment(1).'/view_home');
	}

	function edit_reseller(){
		cek_session_reseller();
		$id = $this->session->id_reseller;
		if (isset($_POST['submit'])){
			$config['upload_path'] = 'asset/foto_user/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size'] = '5000'; // kb
			$this->load->library('upload', $config);
			$this->upload->do_upload('m');
			$hasil=$this->upload->data();
			if ($hasil['file_name']==''){
				if (trim($this->input->post('b')) != ''){
					$data = array('password'=>hash("sha512", md5($this->input->post('b'))),
					'account_ncs'=>$this->input->post('c'),
					'nama_reseller'=>$this->input->post('d'),
					'pic'=>$this->input->post('e'),
					'jenis_kelamin'=>$this->input->post('f'),
					'alamat_lengkap'=>$this->input->post('g'),
					'no_telpon'=>$this->input->post('h'),
					'email'=>$this->input->post('i'),
					'kode_pos'=>$this->input->post('j'),
					'keterangan'=>$this->input->post('k'),
					'referral'=>$this->input->post('l'),
					'kota_id'=>$this->input->post('kota'));
				}else{
					$data = array('account_ncs'=>$this->input->post('c'),
					'nama_reseller'=>$this->input->post('d'),
					'pic'=>$this->input->post('e'),
					'jenis_kelamin'=>$this->input->post('f'),
					'alamat_lengkap'=>$this->input->post('g'),
					'no_telpon'=>$this->input->post('h'),
					'email'=>$this->input->post('i'),
					'kode_pos'=>$this->input->post('j'),
					'keterangan'=>$this->input->post('k'),
					'referral'=>$this->input->post('l'),
					'kota_id'=>$this->input->post('kota'));
				}
			}else{
				if (trim($this->input->post('b')) != ''){
					$data = array('password'=>hash("sha512", md5($this->input->post('b'))),
					'account_ncs'=>$this->input->post('c'),
					'nama_reseller'=>$this->input->post('d'),
					'pic'=>$this->input->post('e'),
					'jenis_kelamin'=>$this->input->post('f'),
					'alamat_lengkap'=>$this->input->post('g'),
					'no_telpon'=>$this->input->post('h'),
					'email'=>$this->input->post('i'),
					'kode_pos'=>$this->input->post('j'),
					'keterangan'=>$this->input->post('k'),
					'foto'=>$hasil['file_name'],
					'referral'=>$this->input->post('l'),
					'kota_id'=>$this->input->post('kota'));
				}else{
					$data = array( 'account_ncs'=>$this->input->post('c'),
					'nama_reseller'=>$this->input->post('d'),
					'pic'=>$this->input->post('e'),
					'jenis_kelamin'=>$this->input->post('f'),
					'alamat_lengkap'=>$this->input->post('g'),
					'no_telpon'=>$this->input->post('h'),
					'email'=>$this->input->post('i'),
					'kode_pos'=>$this->input->post('j'),
					'keterangan'=>$this->input->post('k'),
					'foto'=>$hasil['file_name'],
					'referral'=>$this->input->post('l'),
					'kota_id'=>$this->input->post('kota'));
				}
			}
			$where = array('id_reseller' => $this->input->post('id'));
			$this->model_app->update('rb_reseller', $data, $where);
			redirect($this->uri->segment(1).'/detail_reseller');
		}else{
			$edit = $this->model_app->edit('rb_reseller',array('id_reseller'=>$id))->row_array();
			$data = array('rows' => $edit);
			$this->template->load($this->uri->segment(1).'/template',$this->uri->segment(1).'/mod_reseller/view_reseller_edit',$data);
		}
	}

	function detail_reseller(){
		cek_session_reseller();
		$id = $this->session->id_reseller;
		$edit = $this->model_app->edit('rb_reseller',array('id_reseller'=>$id))->row_array();
		$data = array('rows' => $edit);
		$this->template->load($this->uri->segment(1).'/template',$this->uri->segment(1).'/mod_reseller/view_reseller_detail',$data);
	}


	// Controller Modul Produk

	function produk(){
		cek_session_reseller();
		if (isset($_POST['submit'])){
			$jml = $this->model_app->view('rb_produk')->num_rows();
			for ($i=1; $i<=$jml; $i++){
				$a  = $_POST['a'][$i];
				$b  = $_POST['b'][$i];
				$cek = $this->model_app->edit('rb_produk_diskon',array('id_produk'=>$a,'id_reseller'=>$this->session->id_reseller))->num_rows();
				if ($cek >= 1){
					if ($b > 0){
						$data = array('diskon'=>$b);
						$where = array('id_produk' => $a,'id_reseller' => $this->session->id_reseller);
						$this->model_app->update('rb_produk_diskon', $data, $where);
					}else{
						$this->model_app->delete('rb_produk_diskon',array('id_produk'=>$a,'id_reseller'=>$this->session->id_reseller));
					}
				}else{
					if ($b > 0){
						$data = array('id_produk'=>$a,
						'id_reseller'=>$this->session->id_reseller,
						'diskon'=>$b);
						$this->model_app->insert('rb_produk_diskon',$data);
					}
				}
			}
			redirect($this->uri->segment(1).'/produk');
		}else{
			$data['record'] = $this->model_app->view_where_ordering('rb_produk',array('id_reseller'=>$this->session->id_reseller),'id_produk','DESC');
			$this->template->load($this->uri->segment(1).'/template',$this->uri->segment(1).'/mod_produk/view_produk',$data);
		}
	}

	
	function tambah_produk(){
		cek_session_reseller();
		if (isset($_POST['submit'])){
			$data = array('id_kategori_produk'=>$this->input->post('a'),
				'id_kategori_produk_sub'=>$this->input->post('aa'),
				'id_reseller'=>$this->session->id_reseller,
				'nama_produk'=>$this->input->post('b'),
				'produk_seo'=>seo_title($this->input->post('b'))."-".$this->session->id_reseller."-".substr(md5(uniqid(mt_rand(), true)) , 0, 6),
				'satuan'=>$this->input->post('c'),
				'harga_beli'=>$this->input->post('d'),
				'harga_reseller'=>$this->input->post('e'),
				'harga_konsumen'=>$this->input->post('f'),
				'berat'=>$this->input->post('berat'),
			// 'gambar'=>$fileName,
				'keterangan'=>$this->input->post('ff'),
				'username'=>$this->session->username,
				'waktu_input'=>date('Y-m-d H:i:s'));

			if($this->input->post('free_ongkir')=='Active'){
				$data['gratis_ongkir'] ='1';
			}

			$this->model_app->insert('rb_produk',$data);
			$id_produk = $this->db->insert_id();

			$files = $_FILES;
			$cpt = count($_FILES['userfile']['name']);
			for($i=0; $i<$cpt; $i++){
				if ($files['userfile']['name'][$i] != '') {
					$no=$i+1;
					$_FILES['userfile']['name'] 	= $files['userfile']['name'][$i];
					$_FILES['userfile']['type'] 	= $files['userfile']['type'][$i];
					$_FILES['userfile']['tmp_name']	= $files['userfile']['tmp_name'][$i];
					$_FILES['userfile']['error'] 	= $files['userfile']['error'][$i];
					$_FILES['userfile']['size'] 	= $files['userfile']['size'][$i];
					$this->load->library('upload');
					$savename = $id_produk.'_'.date('ymdHis').'_'.$i.'.jpg';
					$this->upload->initialize($this->set_upload_options($savename));
					$this->upload->do_upload();
					$fileName = $this->upload->data('file_name');
					$this->func_resize($fileName, 'asset/foto_produk/', 500, 500);
					$images[] = $fileName;
				}
			}

			$fileName = implode(';',$images);
			$fileName = str_replace(' ','_',$fileName);

			$data1 = array('gambar' => $fileName);
			$where = array('id_produk' => $id_produk,'id_reseller'=>$this->session->id_reseller);
			$this->model_app->update('rb_produk', $data1, $where);

			if ($this->input->post('diskon') > 0){
				$cek = $this->db->query("SELECT * FROM rb_produk_diskon where id_produk='".$id_produk."' AND id_reseller='".$this->session->id_reseller."'");
				if ($cek->num_rows()>=1){
					$data = array('diskon'=>$this->input->post('diskon'));
					$where = array('id_produk' => $id_produk,'id_reseller' => $this->session->id_reseller);
					$this->model_app->update('rb_produk_diskon', $data, $where);
				}else{
					$data = array('id_produk'=>$id_produk,
						'id_reseller'=>$this->session->id_reseller,
						'diskon'=>$this->input->post('diskon'));
					$this->model_app->insert('rb_produk_diskon',$data);
				}
			}


			if ($this->input->post('stok') != ''){
				$kode_transaksi = "TRX-".date('YmdHis');
				$data = array('kode_transaksi'=>$kode_transaksi,
					'id_pembeli'=>$this->session->id_reseller,
					'id_penjual'=>'1',
					'status_pembeli'=>'reseller',
					'status_penjual'=>'admin',
					'service'=>'Stok Otomatis (Pribadi)',
					'waktu_transaksi'=>date('Y-m-d H:i:s'),
					'proses'=>'1');
				$this->model_app->insert('rb_penjualan',$data);
				$idp = $this->db->insert_id();

				$data = array('id_penjualan'=>$idp,
					'id_produk'=>$id_produk,
					'jumlah'=>$this->input->post('stok'),
					'harga_jual'=>$this->input->post('e'),
					'satuan'=>$this->input->post('c'));
				$this->model_app->insert('rb_penjualan_detail',$data);
			}

			redirect($this->uri->segment(1).'/produk');
		}else{
			$data['record'] = $this->model_app->view_ordering('rb_kategori_produk','id_kategori_produk','DESC');
			$this->template->load($this->uri->segment(1).'/template',$this->uri->segment(1).'/mod_produk/view_produk_tambah',$data);
		}
	}

	function edit_produk(){
		cek_session_reseller();
		$id = $this->uri->segment(3);
		$id_produk = $this->input->post('id');
		if (isset($_POST['submit'])){
			$files = $_FILES;
			$cpt = count($_FILES['userfile']['name']);
			for($i=0; $i<$cpt; $i++){
			if($files['userfile']['name'][$i]!=''){
				$_FILES['userfile']['name']= $files['userfile']['name'][$i];
				$_FILES['userfile']['type']= $files['userfile']['type'][$i];
				$_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
				$_FILES['userfile']['error']= $files['userfile']['error'][$i];
				$_FILES['userfile']['size']= $files['userfile']['size'][$i];
				$this->load->library('upload');
				$savename = $id_produk.'_'.date('ymdHis').'_'.$i.'.jpg';
				$this->upload->initialize($this->set_upload_options($savename));
				$this->upload->do_upload();
				$fileName = $this->upload->data()['file_name'];
				$this->func_resize($fileName, 'asset/foto_produk/', 500, 500);
				$images[] = $fileName;
				unlink("./asset/foto_produk/".$this->input->post('file_lama['.$i.']'));
			}else if($this->input->post('file_lama['.$i.']')!=null){
				$images[] = $this->input->post('file_lama['.$i.']');
			}else{
					
			}
		}
			$fileName = implode(';',$images);
			$fileName = str_replace(' ','_',$fileName);
			if (trim($fileName)!=''){
				$data = array('id_kategori_produk'=>$this->input->post('a'),
				'id_kategori_produk_sub'=>$this->input->post('aa'),
				'nama_produk'=>$this->input->post('b'),
				// 'id_produk_satuan'=>$this->input->post('c'),
				'satuan'=>$this->input->post('c'),
				'harga_beli'=>$this->input->post('d'),
				'harga_reseller'=>$this->input->post('e'),
				'harga_konsumen'=>$this->input->post('f'),
				'berat'=>$this->input->post('berat'),
				'gambar'=>$fileName,
				'keterangan'=>$this->input->post('ff'),
				'username'=>$this->session->username);
			}else{
				$data = array('id_kategori_produk'=>$this->input->post('a'),
				'id_kategori_produk_sub'=>$this->input->post('aa'),
				'nama_produk'=>$this->input->post('b'),
				// 'id_produk_satuan'=>$this->input->post('c'),
				'satuan'=>$this->input->post('c'),
				'harga_beli'=>$this->input->post('d'),
				'harga_reseller'=>$this->input->post('e'),
				'harga_konsumen'=>$this->input->post('f'),
				'berat'=>$this->input->post('berat'),
				'keterangan'=>$this->input->post('ff'),
				'username'=>$this->session->username);
			}

			$cek = $this->db->query("select nama_produk from rb_produk where id_produk='".$this->input->post('id')."'")->row_array()['nama_produk'];

			if($cek!=$this->input->post('b')){
				$data['produk_seo']=seo_title($this->input->post('b'))."-".$this->session->id_reseller."-".substr(md5(uniqid(mt_rand(), true)) , 0, 6);
			}

			if($this->input->post('free_ongkir')=='Active'){
				$data['gratis_ongkir'] ='1';
			}

			$where = array('id_produk' => $this->input->post('id'),'id_reseller'=>$this->session->id_reseller);
			$this->model_app->update('rb_produk', $data, $where);

			if ($this->input->post('diskon') >= 0){
				$cek = $this->db->query("SELECT * FROM rb_produk_diskon where id_produk='".$this->input->post('id')."' AND id_reseller='".$this->session->id_reseller."'");
				if ($cek->num_rows()>=1){
					$data = array('diskon'=>$this->input->post('diskon'));
					$where = array('id_produk' => $this->input->post('id'),'id_reseller' => $this->session->id_reseller);
					$this->model_app->update('rb_produk_diskon', $data, $where);
				}else{
					$data = array('id_produk'=>$this->input->post('id'),
					'id_reseller'=>$this->session->id_reseller,
					'diskon'=>$this->input->post('diskon'));
					$this->model_app->insert('rb_produk_diskon',$data);
				}
			}

			if ($this->input->post('stok') != ''){
				$kode_transaksi = "TRX-".date('YmdHis');
				$data = array('kode_transaksi'=>$kode_transaksi,
				'id_pembeli'=>$this->session->id_reseller,
				'id_penjual'=>'1',
				'status_pembeli'=>'reseller',
				'status_penjual'=>'admin',
				'service'=>'Stok Otomatis (Pribadi)',
				'waktu_transaksi'=>date('Y-m-d H:i:s'),
				'proses'=>'1');
				$this->model_app->insert('rb_penjualan',$data);
				$idp = $this->db->insert_id();

				$data = array('id_penjualan'=>$idp,
				'id_produk'=>$this->input->post('id'),
				'jumlah'=>$this->input->post('stok'),
				'harga_jual'=>$this->input->post('e'),
				'satuan'=>$this->input->post('c'));
				$this->model_app->insert('rb_penjualan_detail',$data);
			}

			redirect($this->uri->segment(1).'/produk');
		}else{
			$data['record'] = $this->model_app->view_ordering('rb_kategori_produk','id_kategori_produk','DESC');
			$data['rows'] = $this->model_app->edit('rb_produk',array('id_produk'=>$id,'id_reseller'=>$this->session->id_reseller))->row_array();
			$this->template->load($this->uri->segment(1).'/template',$this->uri->segment(1).'/mod_produk/view_produk_edit',$data);
		}
	}

	private function set_upload_options($new_name){
		$config = array();
		$config['upload_path'] = 'asset/foto_produk/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size'] = '5000'; // kb
		$config['file_name'] = $new_name;
		$config['encrypt_name'] = FALSE;
		$this->load->library('upload', $config);
		return $config;
	}


	function delete_produk(){
		cek_session_reseller();
		$id = array('id_produk' => $this->uri->segment(3));
		$record = $this->model_app->view_where('rb_produk',$id)->result_array();
		foreach ($record as $row){
			$gambar = $row['gambar'];
		}
		$arr_gambar = explode (";",$gambar);
		$this->load->helper("file");
		foreach ($arr_gambar as $gmbr){
			unlink("./asset/foto_produk/".$gmbr);
		}
		$this->model_app->delete('rb_produk',$id);
		redirect($this->uri->segment(1).'/produk');
	}




	// Controller Modul Rekening

	function rekening(){
		cek_session_reseller();
		$data['record'] = $this->model_app->view_where('rb_rekening_reseller',array('id_reseller'=>$this->session->id_reseller));
		$this->template->load($this->uri->segment(1).'/template',$this->uri->segment(1).'/mod_rekening/view_rekening',$data);
	}

	function tambah_rekening(){
		cek_session_reseller();
		if (isset($_POST['submit'])){
			$data = array('id_reseller'=>$this->session->id_reseller,
			'nama_bank'=>$this->input->post('a'),
			'no_rekening'=>$this->input->post('b'),
			'pemilik_rekening'=>$this->input->post('c'));
			$this->model_app->insert('rb_rekening_reseller',$data);
			redirect($this->uri->segment(1).'/rekening');
		}else{
			$this->template->load($this->uri->segment(1).'/template',$this->uri->segment(1).'/mod_rekening/view_rekening_tambah');
		}
	}

	function edit_rekening(){
		cek_session_reseller();
		$id = $this->uri->segment(3);
		if (isset($_POST['submit'])){
			$data = array('id_reseller'=>$this->session->id_reseller,
			'nama_bank'=>$this->input->post('a'),
			'no_rekening'=>$this->input->post('b'),
			'pemilik_rekening'=>$this->input->post('c'));
			$where = array('id_rekening_reseller' => $this->input->post('id'),'id_reseller' => $this->session->id_reseller);
			$this->model_app->update('rb_rekening_reseller', $data, $where);
			redirect($this->uri->segment(1).'/rekening');
		}else{
			$data['rows'] = $this->model_app->edit('rb_rekening_reseller',array('id_rekening_reseller'=>$id))->row_array();
			$this->template->load($this->uri->segment(1).'/template',$this->uri->segment(1).'/mod_rekening/view_rekening_edit',$data);
		}
	}

	function delete_rekening(){
		cek_session_reseller();
		$id = array('id_rekening_reseller' => $this->uri->segment(3));
		$this->model_app->delete('rb_rekening_reseller',$id);
		redirect($this->uri->segment(1).'/rekening');
	}



	// Controller Modul Pembelian

	function pembelian(){
		cek_session_reseller();
		$data['record'] = $this->model_reseller->reseller_pembelian($this->session->id_reseller,'admin');
		$this->template->load($this->uri->segment(1).'/template',$this->uri->segment(1).'/mod_pembelian/view_pembelian',$data);
	}

	function detail_pembelian(){
		cek_session_reseller();
		$data['rows'] = $this->model_reseller->penjualan_detail($this->uri->segment(3))->row_array();
		$data['record'] = $this->model_app->view_join_where('rb_penjualan_detail','rb_produk','id_produk',array('id_penjualan'=>$this->uri->segment(3)),'id_penjualan_detail','DESC');
		$this->template->load($this->uri->segment(1).'/template',$this->uri->segment(1).'/mod_pembelian/view_pembelian_detail',$data);
	}

	function tambah_pembelian(){
		cek_session_reseller();
		if(isset($_POST['submit'])){
			if ($this->session->idp == ''){
				$kode_transaksi = "TRX-".date('YmdHis');
				$data = array('kode_transaksi'=>$kode_transaksi,
				'id_pembeli'=>$this->session->id_reseller,
				'id_penjual'=>'1',
				'status_pembeli'=>'reseller',
				'status_penjual'=>'admin',
				'waktu_transaksi'=>date('Y-m-d H:i:s'),
				'proses'=>'0');
				$this->model_app->insert('rb_penjualan',$data);
				$idp = $this->db->insert_id();
				$this->session->set_userdata(array('idp'=>$idp));
			}

			if ($this->input->post('idpd')==''){
				$data = array('id_penjualan'=>$this->session->idp,
				'id_produk'=>$this->input->post('aa'),
				'jumlah'=>$this->input->post('dd'),
				'harga_jual'=>$this->input->post('bb'),
				'satuan'=>$this->input->post('ee'));
				$this->model_app->insert('rb_penjualan_detail',$data);
			}else{
				$data = array('id_produk'=>$this->input->post('aa'),
				'jumlah'=>$this->input->post('dd'),
				'harga_jual'=>$this->input->post('bb'),
				'satuan'=>$this->input->post('ee'));
				$where = array('id_penjualan_detail' => $this->input->post('idpd'));
				$this->model_app->update('rb_penjualan_detail', $data, $where);
			}
			redirect($this->uri->segment(1).'/tambah_pembelian');
		}else{
			$data['rows'] = $this->model_reseller->penjualan_detail($this->session->idp)->row_array();
			$data['record'] = $this->model_app->view_join_where('rb_penjualan_detail','rb_produk','id_produk',array('id_penjualan'=>$this->session->idp),'id_penjualan_detail','DESC');
			$data['barang'] = $this->model_app->view_where_ordering('rb_produk',array('id_reseller'=>'0'),'id_produk','ASC');
			$data['reseller'] = $this->model_app->view_ordering('rb_reseller','id_reseller','ASC');
			if ($this->uri->segment(3)!=''){
				$data['row'] = $this->model_app->view_where('rb_penjualan_detail',array('id_penjualan_detail'=>$this->uri->segment(3)))->row_array();
			}
			$this->template->load($this->uri->segment(1).'/template',$this->uri->segment(1).'/mod_pembelian/view_pembelian_tambah',$data);
		}
	}

	function delete_pembelian(){
		cek_session_reseller();
		$id = array('id_penjualan' => $this->uri->segment(3));
		$this->model_app->delete('rb_penjualan',$id);
		$this->model_app->delete('rb_penjualan_detail',$id);
		redirect($this->uri->segment(1).'/pembelian');
	}

	function delete_pembelian_tambah_detail(){
		cek_session_reseller();
		$id = array('id_penjualan_detail' => $this->uri->segment(3));
		$this->model_app->delete('rb_penjualan_detail',$id);
		redirect($this->uri->segment(1).'/tambah_pembelian');
	}

	function konfirmasi_pembayaran(){
		cek_session_reseller();
		$id = $this->uri->segment(3);
		if (isset($_POST['submit'])){
			$config['upload_path'] = 'asset/files/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size'] = '10000'; // kb
			$this->load->library('upload', $config);
			$this->upload->do_upload('f');
			$hasil=$this->upload->data();
			if ($hasil['file_name']==''){
				$data = array('id_penjualan'=>$this->input->post('id'),
				'total_transfer'=>$this->input->post('b'),
				'id_rekening'=>$this->input->post('c'),
				'nama_pengirim'=>$this->input->post('d'),
				'tanggal_transfer'=>$this->input->post('e'),
				'waktu_konfirmasi'=>date('Y-m-d H:i:s'));
				$this->model_app->insert('rb_konfirmasi_pembayaran',$data);
			}else{
				$data = array('id_penjualan'=>$this->input->post('id'),
				'total_transfer'=>$this->input->post('b'),
				'id_rekening'=>$this->input->post('c'),
				'nama_pengirim'=>$this->input->post('d'),
				'tanggal_transfer'=>$this->input->post('e'),
				'bukti_transfer'=>$hasil['file_name'],
				'waktu_konfirmasi'=>date('Y-m-d H:i:s'));
				$this->model_app->insert('rb_konfirmasi_pembayaran',$data);
			}
			$data1 = array('proses'=>'2');
			$where = array('id_penjualan' => $this->input->post('id'));
			$this->model_app->update('rb_penjualan', $data1, $where);
			redirect($this->uri->segment(1).'/pembelian');
		}else{
			$data['record'] = $this->model_app->view('rb_rekening');
			$data['total'] = $this->db->query("SELECT sum((a.harga_jual*a.jumlah)-a.diskon) as total FROM `rb_penjualan_detail` a where a.id_penjualan='".$this->uri->segment(3)."'")->row_array();
			$data['rows'] = $this->model_app->view_where('rb_penjualan',array('id_penjualan'=>$this->uri->segment(3)))->row_array();
			$this->template->load($this->uri->segment(1).'/template',$this->uri->segment(1).'/mod_pembelian/view_konfirmasi_pembayaran',$data);
		}
	}

	function keterangan(){
		cek_session_reseller();
		if (isset($_POST['submit'])){
			$cek = $this->model_app->view_where('rb_keterangan',array('id_reseller'=>$this->session->id_reseller))->num_rows();
			if ($cek>=1){
				$data1 = array('keterangan'=>$this->input->post('a'));
				$where = array('id_keterangan' => $this->input->post('id'),'id_reseller'=>$this->session->id_reseller);
				$this->model_app->update('rb_keterangan', $data1, $where);
			}else{
				$data = array('id_reseller'=>$this->session->id_reseller,
				'keterangan'=>$this->input->post('a'),
				'tanggal_posting'=>date('Y-m-d H:i:s'));
				$this->model_app->insert('rb_keterangan',$data);
			}
			redirect($this->uri->segment(1).'/keterangan');
		}else{
			$data['record'] = $this->model_app->edit('rb_keterangan',array('id_reseller'=>$this->session->id_reseller))->row_array();
			$this->template->load($this->uri->segment(1).'/template',$this->uri->segment(1).'/mod_keterangan/view_keterangan',$data);
		}
	}

	function penjualan(){
		cek_session_reseller();
		$this->session->unset_userdata('idp');
		$id = $this->session->id_reseller;
		$data['record'] = $this->model_reseller->penjualan_list_konsumen($id,'reseller');
		$this->template->load($this->uri->segment(1).'/template',$this->uri->segment(1).'/mod_penjualan/view_penjualan',$data);
	}

	function detail_penjualan(){
		cek_session_reseller();
		$id = $this->session->id_reseller;
		$data['rows'] = $this->model_reseller->penjualan_konsumen_detail_reseller($this->uri->segment(3))->row_array();
		$data['record'] = $this->db->query("SELECT a.*, c.* FROM rb_penjualan_detail a JOIN rb_penjualan b ON a.id_penjualan=b.id_penjualan JOIN rb_produk c ON a.id_produk=c.id_produk  where b.kode_transaksi='".$this->uri->segment(3)."' AND b.id_penjual='".$id."' ORDER BY a.id_penjualan_detail DESC")->result_array();
		// $data['record'] = $this->model_app->view_join_where('rb_penjualan_detail','rb_produk','id_produk',array('id_penjualan'=>$this->uri->segment(3)),'id_penjualan_detail','DESC');
		$data['rec_bayar'] = $this->model_app->view_join_where('rb_konfirmasi_pembayaran_konsumen','rb_rekening','id_rekening',array('kode_transaksi'=>$this->uri->segment(3)),'id_konfirmasi_pembayaran','ASC');
		$data['pemb'] = $this->model_app->view_where('rb_konfirmasi_pembayaran_konsumen',array('kode_transaksi'=>$this->uri->segment(3)))->row_array();
		$this->template->load($this->uri->segment(1).'/template',$this->uri->segment(1).'/mod_penjualan/view_penjualan_detail',$data);
	}

	function detail_konfirmasi_penjualan(){
		cek_session_reseller();
		$data['rows'] = $this->model_reseller->penjualan_konsumen_detail_reseller($this->uri->segment(3))->row_array();
		$data['record'] = $this->model_app->view_join_where('rb_penjualan_detail','rb_produk','id_produk',array('id_penjualan'=>$this->uri->segment(3)),'id_penjualan_detail','DESC');
		$this->template->load($this->uri->segment(1).'/template',$this->uri->segment(1).'/mod_penjualan/view_penjualan_proses.php',$data);
	}

	function tambah_penjualan(){
		cek_session_reseller();
		if (isset($_POST['submit1'])){
			if ($this->session->idp == ''){
				$data = array('kode_transaksi'=>$this->input->post('a'),
				'id_pembeli'=>$this->input->post('b'),
				'id_penjual'=>$this->session->id_reseller,
				'status_pembeli'=>'konsumen',
				'status_penjual'=>'reseller',
				'waktu_transaksi'=>date('Y-m-d H:i:s'),
				'proses'=>'0');
				$this->model_app->insert('rb_penjualan',$data);
				$idp = $this->db->insert_id();
				$this->session->set_userdata(array('idp'=>$idp));
			}else{
				$data = array('kode_transaksi'=>$this->input->post('a'),
				'id_pembeli'=>$this->input->post('b'));
				$where = array('id_penjualan' => $this->session->idp);
				$this->model_app->update('rb_penjualan', $data, $where);
			}
			redirect($this->uri->segment(1).'/tambah_penjualan');

		}elseif(isset($_POST['submit'])){
			$jual = $this->model_reseller->jual_reseller($this->session->id_reseller, $this->input->post('aa'))->row_array();
			$beli = $this->model_reseller->beli_reseller($this->session->id_reseller, $this->input->post('aa'))->row_array();
			$stok = $beli['beli']-$jual['jual'];
			if ($this->input->post('dd') > $stok){
				echo "<script>window.alert('Maaf, Stok Tidak Mencukupi!');
				window.location=('".base_url().$this->uri->segment(1)."/tambah_penjualan')</script>";
			}else{
				if ($this->input->post('idpd')==''){
					$data = array('id_penjualan'=>$this->session->idp,
					'id_produk'=>$this->input->post('aa'),
					'jumlah'=>$this->input->post('dd'),
					'harga_jual'=>$this->input->post('bb'),
					'satuan'=>$this->input->post('ee'));
					$this->model_app->insert('rb_penjualan_detail',$data);
				}else{
					$data = array('id_produk'=>$this->input->post('aa'),
					'jumlah'=>$this->input->post('dd'),
					'harga_jual'=>$this->input->post('bb'),
					'satuan'=>$this->input->post('ee'));
					$where = array('id_penjualan_detail' => $this->input->post('idpd'));
					$this->model_app->update('rb_penjualan_detail', $data, $where);
				}
				redirect($this->uri->segment(1).'/tambah_penjualan');
			}

		}else{
			$data['rows'] = $this->model_reseller->penjualan_konsumen_detail_reseller($this->session->idp)->row_array();
			$data['record'] = $this->model_app->view_join_where('rb_penjualan_detail','rb_produk','id_produk',array('id_penjualan'=>$this->session->idp),'id_penjualan_detail','DESC');
			$data['barang'] = $this->model_app->view_ordering('rb_produk','id_produk','ASC');
			$data['konsumen'] = $this->model_app->view_ordering('rb_konsumen','id_konsumen','ASC');
			if ($this->uri->segment(3)!=''){
				$data['row'] = $this->model_app->view_where('rb_penjualan_detail',array('id_penjualan_detail'=>$this->uri->segment(3)))->row_array();
			}
			$this->template->load($this->uri->segment(1).'/template',$this->uri->segment(1).'/mod_penjualan/view_penjualan_tambah',$data);
		}
	}

	function edit_penjualan(){
		cek_session_reseller();
		if (isset($_POST['submit1'])){
			$data = array('kode_transaksi'=>$this->input->post('a'),
			'id_pembeli'=>$this->input->post('b'),
			'waktu_transaksi'=>$this->input->post('c'));
			$where = array('id_penjualan' => $this->input->post('idp'));
			$this->model_app->update('rb_penjualan', $data, $where);
			redirect($this->uri->segment(1).'/edit_penjualan/'.$this->input->post('idp'));

		}elseif(isset($_POST['submit'])){
			$cekk = $this->db->query("SELECT * FROM rb_penjualan_detail where id_penjualan='".$this->input->post('idp')."' AND id_produk='".$this->input->post('aa')."'")->row_array();
			$jual = $this->model_reseller->jual_reseller($this->session->id_reseller, $this->input->post('aa'))->row_array();
			$beli = $this->model_reseller->beli_reseller($this->session->id_reseller, $this->input->post('aa'))->row_array();
			$stok = $beli['beli']-$jual['jual']+$cekk['jumlah'];
			if ($this->input->post('dd') > $stok){
				echo "<script>window.alert('Maaf, Stok $stok Tidak Mencukupi!');
				window.location=('".base_url().$this->uri->segment(1)."/edit_penjualan/".$this->input->post('idp')."')</script>";
			}else{
				if ($this->input->post('idpd')==''){
					$data = array('id_penjualan'=>$this->input->post('idp'),
					'id_produk'=>$this->input->post('aa'),
					'jumlah'=>$this->input->post('dd'),
					'harga_jual'=>$this->input->post('bb'),
					'satuan'=>$this->input->post('ee'));
					$this->model_app->insert('rb_penjualan_detail',$data);
				}else{
					$data = array('id_produk'=>$this->input->post('aa'),
					'jumlah'=>$this->input->post('dd'),
					'harga_jual'=>$this->input->post('bb'),
					'satuan'=>$this->input->post('ee'));
					$where = array('id_penjualan_detail' => $this->input->post('idpd'));
					$this->model_app->update('rb_penjualan_detail', $data, $where);
				}
				redirect($this->uri->segment(1).'/edit_penjualan/'.$this->input->post('idp'));
			}

		}else{
			$data['rows'] = $this->model_reseller->penjualan_konsumen_detail_reseller($this->uri->segment(3))->row_array();
			$data['record'] = $this->model_app->view_join_where('rb_penjualan_detail','rb_produk','id_produk',array('id_penjualan'=>$this->uri->segment(3)),'id_penjualan_detail','DESC');
			$data['barang'] = $this->model_app->view_ordering('rb_produk','id_produk','ASC');
			$data['konsumen'] = $this->model_app->view_ordering('rb_konsumen','id_konsumen','ASC');
			if ($this->uri->segment(4)!=''){
				$data['row'] = $this->model_app->view_where('rb_penjualan_detail',array('id_penjualan_detail'=>$this->uri->segment(4)))->row_array();
			}
			$this->template->load($this->uri->segment(1).'/template',$this->uri->segment(1).'/mod_penjualan/view_penjualan_edit',$data);
		}
	}

	
	function adress_pickup(){
		cek_session_reseller();
		$data['seller'] = $this->db->query("select cod from rb_reseller where id_reseller=".$this->session->id_reseller)->row_array(); 
		$data['record'] = $this->db->query("select * from alamat_reseller a JOIN rb_kota b ON a.kota_id=b.kota_id where a.id_reseller=".$this->session->id_reseller." ");        
		$this->template->load($this->uri->segment(1).'/template',$this->uri->segment(1).'/mod_alamat_pickup/view_address_pickup',$data);
	}

	function tambah_adress_pickup(){
		cek_session_reseller();
		if (isset($_POST['submit'])){
			$data = array(	'id_reseller'=>$this->session->id_reseller,
							'pic'=>$this->input->post('a'),
							'no_hp'=>$this->input->post('b'),
							'alamat_lengkap'=>$this->input->post('c'),
							'provinsi_id'=>$this->input->post('e'),
							'kota_id'=>$this->input->post('f'),
							'kecamatan'=>$this->input->post('d'),
							'kode_pos'=>$this->input->post('g'));
							$this->model_app->insert('alamat_reseller',$data);
			redirect($this->uri->segment(1).'/adress_pickup');
		}else{
			$this->template->load($this->uri->segment(1).'/template',$this->uri->segment(1).'/mod_alamat_pickup/view_address_pickup_tambah');
		}
	}

	function edit_adress_pickup(){
		cek_session_reseller();
		$id = $this->uri->segment(3);
		if (isset($_POST['submit'])){
				$data = array(	'id_reseller'=>$this->session->id_reseller,
								'pic'=>$this->input->post('a'),
								'no_hp'=>$this->input->post('b'),
								'alamat_lengkap'=>$this->input->post('c'),
								'provinsi_id'=>$this->input->post('e'),
								'kota_id'=>$this->input->post('f'),
								'kecamatan'=>$this->input->post('d'),
								'kode_pos'=>$this->input->post('g'));	
			$where = array('id_alamat_reseller' => $this->input->post('id'),'id_reseller' => $this->session->id_reseller);
			$this->model_app->update('alamat_reseller', $data, $where);
			
			redirect($this->uri->segment(1).'/adress_pickup');
		}else{
			$data['rows'] = $this->model_app->view_where('alamat_reseller',array('id_reseller'=>$this->session->id_reseller, 'id_alamat_reseller'=>$this->uri->segment(3)))->row_array();
			$this->template->load($this->uri->segment(1).'/template',$this->uri->segment(1).'/mod_alamat_pickup/view_address_pickup_edit',$data);
		}
	}

	function delete_adress_pickup(){
		cek_session_reseller();
		$id = array('id_alamat_reseller' => $this->uri->segment(3));
		$this->model_app->delete('alamat_reseller',$id);
		redirect($this->uri->segment(1).'/adress_pickup');
	}


	function request_pickup(){
		cek_session_reseller();
		$id = $this->session->id_reseller;
		$idp = $this->uri->segment(3);
		$idr = $this->uri->segment(4);

		$query = "SELECT pj.kode_transaksi, pj.id_penjualan, pj.waktu_transaksi, pj.waktu_order, pj.kurir, LEFT(pj.service, 3) as service_code, pj.service, pj.pembayaran, pj.ongkir, pj.kurir, pj.drop, rs.account_ncs, rs.nama_reseller as nama_merchant, rs.pic, rs.kota_id as id_kota_merchant, kt1.nama_kota as kota_merchant, pr1.nama_provinsi as prov_merchant, rs.alamat_lengkap as alamat_merchant, rs.kode_pos as zip_merchant, rs.no_telpon as telp_merchant, rs.email as email_merchant, km.id_konsumen, km.username, al.pic as nama_konsumen, al.alamat_lengkap as alamat_konsumen, al.kota_id as id_kota_konsumen, kt2.nama_kota as kota_konsumen, pr2.nama_provinsi as prov_konsumen, al.kode_pos as zip_konsumen, al.no_hp as telp_konsumen, km.email as email_konsumen, ";
		$query .= "SUM((dt.harga_jual*dt.jumlah)-dt.diskon) as TotalBelanja, pj.total_bayar as TotalBayar, SUM(dt.jumlah) as TotalBarang, SUM(pd.berat) as TotalBerat, LEFT(GROUP_CONCAT(pd.nama_produk ORDER BY dt.id_penjualan_detail SEPARATOR ', '), 350) as konten  ";
		$query .= "FROM rb_penjualan pj ";
		$query .= "LEFT JOIN rb_reseller rs ON pj.id_penjual=rs.id_reseller ";
		$query .= "LEFT JOIN rb_kota kt1 ON rs.kota_id=kt1.kota_id ";
		$query .= "LEFT JOIN rb_provinsi pr1 ON kt1.provinsi_id=pr1.provinsi_id ";
		$query .= "LEFT JOIN rb_konsumen km ON pj.id_pembeli=km.id_konsumen ";
		$query .= "LEFT JOIN alamat al ON pj.id_alamat=al.id_alamat ";
		$query .= "LEFT JOIN rb_kota kt2 ON al.kota_id=kt2.kota_id ";
		$query .= "LEFT JOIN rb_provinsi pr2 ON al.provinsi_id=pr2.provinsi_id  ";
		$query .= "LEFT JOIN rb_penjualan_detail dt ON pj.id_penjualan=dt.id_penjualan ";
		$query .= "LEFT JOIN rb_produk pd ON dt.id_produk=pd.id_produk ";
		$query .= "WHERE pj.kode_transaksi='".$idp."' AND pj.id_penjual='".$id."' ";
		$query .= "GROUP BY pj.id_penjualan ";
		$data['record'] = $this->db->query($query)->row_array();

		$query = "SELECT pd.nama_produk, dt.jumlah, dt.harga_jual, pd.berat ";
		$query .= "FROM rb_penjualan pj ";
		$query .= "LEFT JOIN rb_penjualan_detail dt ON pj.id_penjualan=dt.id_penjualan ";
		$query .= "LEFT JOIN rb_produk pd ON dt.id_produk=pd.id_produk ";
		$query .= "WHERE pj.kode_transaksi='".$idp."' AND pj.id_penjual='".$id."' ";
		$data['prod_detail'] = $this->db->query("SELECT a.*,b.*, c.* FROM rb_penjualan_detail a JOIN rb_penjualan b ON a.id_penjualan=b.id_penjualan JOIN rb_produk c ON a.id_produk=c.id_produk where b.kode_transaksi='".$idp."' AND b.id_penjual='".$id."'ORDER BY a.id_penjualan_detail DESC")->result_array();

		if (isset($_POST['submit'])){
			//Request API QRIS
			// $url = 'https://api.ptncs.com/bot/pickRequestGeneric/demo/demo';
			//   $url = 'https://api.ptncs.com/bot/pickRequestGeneric/topsonia/dG9wc29uaWE='; //Production

			$url = "https://api.ptncs.com/bot/picktranspay/topsonia/dG9wc29uaWE=";//production
			//$url = "https://sandbox-api.ptncs.com/bot/picktranspay/topsonia/dG9wc29uaWE=";
			
			// $data_array =  array(
			// "accno" => "4231731000200190",
			// "comname" => "Toko iDss ",
			// "pickpoint" => "Jl. Brigjen Katamso",
			// "PickAddress" => "Jl. Brigjen Katamso",
			// "pickcp" => "Boohel",
			// "pickphone" => "021-3865971",
			// "pickkecid" => "317308",
			// "pickdate" => "2021-01-13",
			// "pickemail" => "idss@ptncs.co.id",
			// "picktimefrom" => "08:00",
			// "picktimeto" => "10:00",
			// "picktransport" => "101",
			// "picktype" => "01",
			// "PickService" => "",
			// "si" => "tolong jangan di banting",
			// "transaction" =>  array(
			// "refno" => "ARI003",
			// "s_add1" => "Jl. Brigjend. Katamso No.7",
			// "s_add2" => "Kota Bambu Selatan",
			// "s_add3" => "Jakarta Barat",
			// "s_add4" => "DKI Jakarta",
			// "s_kecid" => "317308",
			// "s_zip" => "s_zip",
			// "s_phnno" => "0215829191",
			// "s_email" => "toko@topsonia.com",
			// "c_cnename" => "RIZAL",
			// "c_add1" => "POLITEKNIK ILMU PELAYARAN SEMARANGJL.SINGOSARI 2A",
			// "c_add2" => "SEMARANG SELATAN",
			// "c_add3" => "KOTA SEMARANG",
			// "c_add4" => "JAWA TENGAH",
			// "c_kecid" => "337407",
			// "c_zip" => "50242",
			// "c_phnno" => "081281552xxx",
			// "c_email" => "test@gmail.com",
			// "value" => "11000",
			// "service" => "NRS",
			// "specinstruction" => "tolong jangan dibanting",
			// "weight" => "7",
			// "vweight" => "10",
			// "content" => "ALAT HISAP",
			// "pieces" => "3"
			// )
			// );

			// $data_string = '{"accno" : "'.$this->input->post('account_ncs').'",';
			 
			if($this->input->post('pembayaran')=='cod'){
				$cod_type='BOTH';
			}else{
				$cod_type='';
			}
			
			if($this->input->post('zip_merchant')==''){
				$zip_merchant ='-';
			}else{
				$zip_merchant =$this->input->post('zip_merchant');
			}

			$data_string = '{"accno" : "4231731000116533",';
			$data_string .= '"comname" : "'.$this->input->post('nama_merchant').'",';
			$data_string .= '"pickpoint" : "'.$this->input->post('nama_merchant').'",';
			$data_string .= '"PickAddress" : "'.$this->input->post('alamat_merchant').'",';
			$data_string .= '"pickcp" : "'.$this->input->post('pic').'",';
			$data_string .= '"pickphone" : "'.$this->input->post('telp_merchant').'",';
			$data_string .= '"pickkecid" : "'.$this->input->post('id_kota_merchant').'",';
			$data_string .= '"pickzip" : "'.$zip_merchant.'",';
			$data_string .= '"pickdate" : "'.date('Y-m-d', strtotime($this->input->post('pickup_date'))).'",';
			$data_string .= '"pickemail" : "'.$this->input->post('email_merchant').'",';
			$data_string .= '"picktimefrom" : "'.$this->input->post('pickup_time_start').'",';
			$data_string .= '"picktimeto" : "'.$this->input->post('pickup_time_end').'",';
			$data_string .= '"picktransport" : "'.$this->input->post('pickup_transportation').'",';
			$data_string .= '"pickpayment" : "0",';
			$data_string .= '"picktype" : "'.$this->input->post('request-type').'",';
			$data_string .= '"PickService" : "",';
			$data_string .= '"si" : "'.$this->input->post('pickup_instruction').'",';
			$data_string .= '"transaction" :  [{';
			$refid = $this->input->post('kode_transaksi').'-'.$this->input->post('idp');
			$data_string .= '"refno" : "'.$refid.'",';
			$data_string .= '"s_comname" : "'.$this->input->post('nama_merchant').'",';
			$data_string .= '"s_add1" : "'.$this->input->post('alamat_merchant').'",';
			$data_string .= '"s_add2" : "-",';
			$data_string .= '"s_add3" : "'.$this->input->post('kota_merchant').'",'; 
			$data_string .= '"s_add4" : "'.$this->input->post('prov_merchant').'",'; 
			$data_string .= '"s_kecid" : "'.$this->input->post('id_kota_merchant').'",';
			$data_string .= '"s_zip" : "'.$zip_merchant.'",';
			$data_string .= '"s_phnno" : "'.$this->input->post('telp_merchant').'",';
			$data_string .= '"s_email" : "'.$this->input->post('email_merchant').'",';
			$data_string .= '"c_cnename" : "'.$this->input->post('nama_konsumen').'",';
			$data_string .= '"c_add1" : "'.$this->input->post('alamat_konsumen').'",';
			$data_string .= '"c_add2" : "-",';
			$data_string .= '"c_add3" : "'.$this->input->post('kota_konsumen').'",'; 
			$data_string .= '"c_add4" : "'.$this->input->post('prov_konsumen').'",'; 
			$data_string .= '"c_kecid" : "'.$this->input->post('id_kota_konsumen').'",';
			if($this->input->post('zip_konsumen') != ''){
				$data_string .= '"c_zip" : "'.$this->input->post('zip_konsumen').'",';
			}else{
				$data_string .= '"c_zip" : "-",';
			}
			$data_string .= '"c_phnno" : "'.$this->input->post('telp_konsumen').'",';
			$data_string .= '"c_email" : "'.$this->input->post('email_konsumen').'",';
			$data_string .= '"value" : "'.$this->input->post('TotalBelanja').'",';
			$data_string .= '"service" : "'.$this->input->post('service_code').'",';
			$data_string .= '"specinstruction" : "'.$this->input->post('pickup_instruction').'",';
			$data_string .= '"weight" : "'.$this->input->post('TotalBerat').'",';
			$data_string .= '"vweight" : "'.$this->input->post('TotalBerat').'",';
			$data_string .= '"content" : "'.$this->input->post('konten').'",';
			$data_string .= '"pieces" : "'.$this->input->post('TotalBarang').'",';
			$data_string .= '"paymentype" : "1",';
			$data_string .= '"paymentgetaway" : "'.$this->input->post('pembayaran').'",';
			$data_string .= '"buyerorderno" : "'.$this->input->post('username').'",';
			$data_string .= '"CODType" : "'.$cod_type.'"';
			$data_string .= '}]}';

			
			// $data_string = json_encode($data_string);
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

			$server_output = curl_exec($ch);

			curl_close($ch);

			// echo "<script>alert('".$data_string."');</script>";
			// exit;

			// $server_output = '{';
			// $server_output .= '"code": "200",';
			// $server_output .= '"message": "REQUEST WAS SUCCESSFUL AND RESOURCE CREATED",';
			// $server_output .= '"requestid": "20210115072143309",';
			// $server_output .= '"awb": "3100000244842"';
			// $server_output .= '}';

			$outputjson = json_decode($server_output, true);

			try {
				if($outputjson['code'] == '200'){
					//cek drop
					$drop = "0";
					if($this->input->post('request-type')=="1"){
						$drop = "1";
					}
					// echo "<script>alert('".$this->input->post('idp')." - ".$outputjson."');</script>";
					// exit;

					//update awb
					$data1 = array(
						'awb'=>$outputjson['Transact'][0]['awb'],
						'res_request_id'=>$outputjson['reqid'],
						'request_pickup'=>'1',
						'drop'=>$drop
					);
					$where1 = array('id_penjualan'=>$this->input->post('idp'));
					$this->model_app->update('rb_penjualan', $data1, $where1);

					$data['response'] = $data1;
					$data['success'] = "1";
					echo $this->session->set_flashdata('message', '<div class="alert alert-success" id="message-form"><center>Request Berhasil dikirim</center></div>');
					$this->template->load($this->uri->segment(1).'/template',$this->uri->segment(1).'/mod_penjualan/view_penjualan_pickup',$data);


					// redirect($this->uri->segment(1).'/penjualan/'.$this->input->post('idp'));
				}else{
					//Pesan Error
					$data['success'] = "0";
					$out_message = '';
					if($outputjson['message'] <> ''){
						$out_message = $outputjson['message'];
					}else{
						$out_message = "$outputjson('0')";
					}
					echo $this->session->set_flashdata('message', '<div class="alert alert-danger" id="message-form"><center>Gagal mengirim request (response message: '.$out_message.')</center></div>');
					$this->template->load($this->uri->segment(1).'/template',$this->uri->segment(1).'/mod_penjualan/view_penjualan_pickup',$data);
				}

			}catch (\Exception $e) {
				//Pesan Error
				$data['success'] = "0";
				echo $this->session->set_flashdata('message', '<div class="alert alert-danger" id="message-form"><center>Gagal mengirim request ('.$e->getMessage().' - '.$x.')</center></div>');
				$this->template->load($this->uri->segment(1).'/template',$this->uri->segment(1).'/mod_penjualan/view_penjualan_pickup',$data);
			}
		}else{
			$data['success'] = "0";
			$data['address'] = $this->db->query("SELECT b.kota_id as id_kota_merchant,a.id_alamat_reseller, a.kode_transaksi, b.pic, b.no_hp, b.alamat_lengkap, b.kode_pos, c.nama_kota, d.nama_provinsi from rb_penjualan a JOIN alamat_reseller b ON a.id_alamat_reseller=b.id_alamat_reseller JOIN rb_kota c ON b.kota_id=c.kota_id JOIN rb_provinsi d ON b.provinsi_id=d.provinsi_id where a.id_penjualan=".$this->uri->segment(4))->row_array(); 
			$this->template->load($this->uri->segment(1).'/template',$this->uri->segment(1).'/mod_penjualan/view_penjualan_pickup',$data);
		}
	}

	function proses_penjualan(){
		cek_session_reseller();
		$data = array('proses'=>$this->uri->segment(4),'selesai'=>$this->uri->segment(5), 'flag_reseller'=>'1');
		$where = array('id_penjualan' => $this->uri->segment(3));
		$this->model_app->update('rb_penjualan', $data, $where);
		redirect($this->uri->segment(1).'/penjualan');
	}

	function proses_penjualan_detail(){
		cek_session_reseller();
		$data = array('proses'=>$this->uri->segment(4),'tanggal_proses'=>date('Y-m-d H:i:s'),'selesai'=>$this->uri->segment(5),'flag_reseller'=>'1');
		$where = array('id_penjualan' => $this->uri->segment(3));
		$this->model_app->update('rb_penjualan', $data, $where);
		redirect($this->uri->segment(1).'/detail_penjualan/'.$this->uri->segment(6));
	}

	function proses_penjualan_konfirmasi(){
		cek_session_reseller();
		redirect($this->uri->segment(1).'/detail_konfirmasi_penjualan/'.$this->uri->segment(3));
	}

	function delete_penjualan(){
		cek_session_reseller();
		$id = array('id_penjualan' => $this->uri->segment(3));
		$this->model_app->delete('rb_penjualan',$id);
		$this->model_app->delete('rb_penjualan_detail',$id);
		redirect($this->uri->segment(1).'/penjualan');
	}

	function delete_penjualan_detail(){
		cek_session_reseller();
		$id = array('id_penjualan_detail' => $this->uri->segment(4));
		$this->model_app->delete('rb_penjualan_detail',$id);
		redirect($this->uri->segment(1).'/edit_penjualan/'.$this->uri->segment(3));
	}

	function delete_penjualan_tambah_detail(){
		cek_session_reseller();
		$id = array('id_penjualan_detail' => $this->uri->segment(3));
		$this->model_app->delete('rb_penjualan_detail',$id);
		redirect($this->uri->segment(1).'/tambah_penjualan');
	}

	function detail_konsumen(){
		cek_session_reseller();
		$id = $this->uri->segment(3);
		$edit = $this->model_app->edit('rb_konsumen',array('id_konsumen'=>$id))->row_array();
		$data = array('rows' => $edit);
		$this->template->load($this->uri->segment(1).'/template',$this->uri->segment(1).'/mod_konsumen/view_konsumen_detail',$data);
	}

	function pembayaran_konsumen(){
		cek_session_reseller();
		$data['record'] = $this->db->query("SELECT a.*, b.*, c.kode_transaksi, c.proses FROM `rb_konfirmasi_pembayaran_konsumen` a JOIN rb_rekening b ON a.id_rekening=b.id_rekening JOIN rb_penjualan c ON a.id_penjualan=c.id_penjualan where c.id_penjual='".$this->session->id_reseller."' AND c.status_penjual='reseller'");
		$this->template->load($this->uri->segment(1).'/template',$this->uri->segment(1).'/mod_konsumen/view_konsumen_pembayaran',$data);
	}

	function download(){
		$name = $this->uri->segment(3);
		$data = file_get_contents("asset/files/".$name);
		force_download($name, $data);
	}

	function keuangan(){
		cek_session_reseller();
		$id = $this->session->id_reseller;
		$record = $this->model_reseller->reseller_pembelian($id,'admin');
		$penjualan = $this->model_reseller->penjualan_list_konsumen($id,'reseller');
		$edit = $this->model_app->edit('rb_reseller',array('id_reseller'=>$id))->row_array();
		$reward = $this->model_app->view_ordering('rb_reward','id_reward','ASC');

		$data = array('rows' => $edit,'record'=>$record,'penjualan'=>$penjualan,'reward'=>$reward);
		$this->template->load($this->uri->segment(1).'/template',$this->uri->segment(1).'/mod_reseller/view_reseller_keuangan',$data);
	}

	// Controller Modul COD

	function alamat_cod(){
		cek_session_reseller();
		$data['record'] = $this->model_app->view_where('rb_reseller_cod',array('id_reseller'=>$this->session->id_reseller));
		$this->template->load($this->uri->segment(1).'/template',$this->uri->segment(1).'/mod_alamat_cod/view',$data);
	}

	function tambah_cod(){
		cek_session_reseller();
		if (isset($_POST['submit'])){
			$data = array('id_reseller'=>$this->session->id_reseller,
			'nama_alamat'=>$this->input->post('a'),
			'biaya_cod'=>$this->input->post('b'));
			$this->model_app->insert('rb_reseller_cod',$data);
			redirect($this->uri->segment(1).'/alamat_cod');
		}else{
			$this->template->load($this->uri->segment(1).'/template',$this->uri->segment(1).'/mod_alamat_cod/tambah');
		}
	}

	function edit_cod(){
		cek_session_reseller();
		$id = $this->uri->segment(3);
		if (isset($_POST['submit'])){
			$data = array('id_reseller'=>$this->session->id_reseller,
			'nama_alamat'=>$this->input->post('a'),
			'biaya_cod'=>$this->input->post('b'));
			$where = array('id_cod' => $this->input->post('id'),'id_reseller' => $this->session->id_reseller);
			$this->model_app->update('rb_reseller_cod', $data, $where);
			redirect($this->uri->segment(1).'/alamat_cod');
		}else{
			$data['rows'] = $this->model_app->edit('rb_reseller_cod',array('id_cod'=>$id))->row_array();
			$this->template->load($this->uri->segment(1).'/template',$this->uri->segment(1).'/mod_alamat_cod/edit',$data);
		}
	}

	function delete_cod(){
		cek_session_reseller();
		$id = array('id_cod' => $this->uri->segment(3));
		$this->model_app->delete('rb_reseller_cod',$id);
		redirect($this->uri->segment(1).'/alamat_cod');
	}

	public function func_resize($filename, $source_image, $width, $height)
	{
		$this->image_moo
		->load($source_image.$filename)
		->stretch($width, $height)
		->save($source_image.$filename, $overwrite=TRUE);
		echo $this->image_moo->display_errors();
		chmod($source_image.$filename, 0777);
	}

	public function  print_invoice(){
	$idp = $this->uri->segment(3);
		$gambar = ('https://www.topsonia.com/asset/logo/logo_topsonia22.png');
		$ncs = ('https://www.topsonia.com/asset/logo/ncslogonobg.png');
	
		$query = "SELECT pj.kode_transaksi, pj.id_penjualan, pj.no_invoice,pj.waktu_order, pj.waktu_transaksi, pj.service, pj.ongkir, pj.asuransi, pj.kurir, pj.awb, rs.nama_reseller, rs.nama_reseller as nama_merchant, rs.kota_id as id_kota_merchant, kt1.nama_kota as kota_merchant, pr1.nama_provinsi as prov_merchant, rs.alamat_lengkap as alamat_merchant, rs.kode_pos as zip_merchant, rs.no_telpon as telp_merchant, al.pic as nama_konsumen, al.alamat_lengkap as alamat_konsumen, al.kecamatan as kecamatan, al.kota_id as id_kota_konsumen, kt2.nama_kota as kota_konsumen, pr2.nama_provinsi as prov_konsumen, al.kode_pos as zip_agen, al.no_hp as telp_konsumen, dt.jumlah, dt.jumlah, dt.harga_jual, pd.nama_produk, pd.harga_konsumen as harga_produk ";
		// $query .= "sum((dt.harga_jual*dt.jumlah)-dt.diskon+pj.ongkir+pj.asuransi) as TotalBayar,SUM(pj.ongkir+pj.asuransi) as TotalOngkir ,  SUM(dt.jumlah) as TotalBarang, SUM(pd.berat) as TotalBerat ";
		$query .= "FROM rb_penjualan pj ";
		$query .= "LEFT JOIN rb_reseller rs ON pj.id_penjual=rs.id_reseller ";
		$query .= "LEFT JOIN rb_kota kt1 ON rs.kota_id=kt1.kota_id ";
		$query .= "LEFT JOIN rb_provinsi pr1 ON kt1.provinsi_id=pr1.provinsi_id ";
		$query .= "LEFT JOIN rb_konsumen km ON pj.id_pembeli=km.id_konsumen ";
		$query .= "LEFT JOIN alamat al ON pj.id_alamat=al.id_alamat  ";
		$query .= "LEFT JOIN rb_kota kt2 ON al.kota_id=kt2.kota_id ";
		$query .= "LEFT JOIN rb_provinsi pr2 ON al.provinsi_id=pr2.provinsi_id  ";
		$query .= "LEFT JOIN rb_penjualan_detail dt ON pj.id_penjualan=dt.id_penjualan ";
		$query .= "LEFT JOIN rb_produk pd ON dt.id_produk=pd.id_produk ";
		$query .= "WHERE pj.id_penjualan='".$idp."'";
		// $query .= "GROUP BY pj.id_penjualan ";
		$data   = $this->db->query($query);
		$semua = $this->db->query("SELECT ongkir,diskon_ongkir,diskon FROM rb_penjualan WHERE id_penjualan='".$idp."'");
		$sub_total  = $this->db->query("SELECT sum((a.harga_jual*a.jumlah)-a.diskon) as total_bayar FROM `rb_penjualan_detail` a JOIN rb_penjualan b ON a.id_penjualan=b.id_penjualan where b.id_penjualan='".$idp."'");	
		$pdf = new FPDF('P','mm',array(210,99)); //L = lanscape P= potrait
		// membuat halaman baru
		$pdf->AddPage("P", array(150,120));
		$w = $pdf->GetPageWidth(); // Width of Current Page
		$h = $pdf->GetPageHeight(); // Height of Current Page
	
		$xpos = 5;
		$ypos = 5;
		$margin = 3;
		$area_content = $w - ($xpos*2);
		$h_content = $h - ($ypos);
		$mx = $xpos + $margin;
		$my = $ypos + $margin;
	
		
			//Header Left
			$xpos = $mx;
			$ypos = $my;
			$pdf->setXY($xpos,$ypos);
			
			$pdf->SetFont('Arial','B',8);
			$pdf->setXY($xpos+3 + $area_content - 115, $ypos - 2);
			$pdf->Cell(20, 3, 'Thank you for purchasing at', 0, 0, 'L' );
			$pdf->Image($gambar, $xpos+3, $ypos + 1, 30, 0, 'png');
			
			$ypos += 1.5;
			$ypos += 9;
			$pdf->SetTextColor(0, 0, 0);
	
			$pdf->setXY($xpos, $ypos + 10);
			$ypos += 10;
			$yposTmp = $ypos;
	
			//Area Left
			$pdf->setXY($xpos,$ypos);
			$pdf->SetFont('Arial','',15);
			$pdf->SetTextColor(255, 155, 0);
			$pdf->text($xpos, $yposTmp, 'INVOICE');
			
			$pdf->SetFont('Arial','',9);
			$pdf->SetTextColor(0, 0, 0);
			
			//PENGIRIM
			$spacing = 3.5;
			$yposTmp += 6;
			$pdf->SetFont('Arial','B',8);
			$pdf->SetTextColor(0, 0, 000);
			$pdf->text($xpos, $yposTmp, 'Invoice tor');
			
			$pdf->SetTextColor(0, 0, 0);
			$yposTmp += 4;
			$pdf->SetFont('Arial','',8);
			$pdf->text($xpos, $yposTmp, $data->row_array()['nama_konsumen'] );
	
			$yposTmp += $spacing;
			$text=$data->row_array()['alamat_konsumen'];
			$nb = $pdf->WordWrap($text, ($area_content/2) - ($margin * 2));
			if($nb > 1){
				$x = 0;
				$y = 32;
				for($i = 0; $i <= $nb - 1; $i++) {
					$pdf->text($xpos, $yposTmp, trim(substr($text, $x,$y)));
					$yposTmp += $spacing;
					$x += $y;
				}
			}else{
				$pdf->text($xpos, $yposTmp, $text);
				$yposTmp += $spacing;
			}
	
			if($data->row_array()['zip_konsumen'] != ''){
				$pdf->text($xpos, $yposTmp, $data->row_array()['zip_konsumen'] );
				$yposTmp += $spacing;
			}
			$pdf->text($xpos, $yposTmp, $data->row_array()['telp_konsumen']);
	
			$pdf->Line($xpos,$ypos + 25, $xpos + $area_content -10 , $ypos + 25);
			$pdf->Line($xpos,$ypos + 33, $xpos + $area_content -10 , $ypos + 33);
			$pdf->Line($xpos,$ypos + 70, $xpos + $area_content -10 , $ypos + 70);
			
			//Area Right
			$yposTmp = $ypos;
			$xposTmp = ($xpos+7) + ($area_content/3);
			
			$pdf->SetFont('Arial','B',8);
			$yposTmp += 6;
			$pdf->text($xposTmp, $yposTmp, 'Payable to');
			$yposTmp += 4;
			$pdf->SetFont('Arial','',8);
			$pdf->text($xposTmp, $yposTmp, 'Topsonia');
	
			$yposTmp = $ypos;
			$xposTmp = ($xpos+34) + ($area_content/3);
			$pdf->SetFont('Arial','B',8);
			$yposTmp += 6;
			$pdf->text($xposTmp, $yposTmp, 'Invoice');
			$yposTmp += 4;
			if($data->row_array()['no_invoice'] != ''){
				$pdf->SetFont('Arial','',8);
				$pdf->text($xposTmp, $yposTmp, $data->row_array()['no_invoice']);
			}else{
				$pdf->SetFont('Arial','',8);
				$pdf->text($xposTmp, $yposTmp, $data->row_array()['no_invoice']);
			}
			
			
			$spacing = 2;
			$yposTmp += 20;
			$pdf->SetFont('Arial','B',8);
			$pdf->SetTextColor(0, 0, 000);
			$pdf->text($xpos, $yposTmp, 'Product Description');
			$pdf->text($xpos+62, $yposTmp, 'Qty');
			$pdf->text($xpos+70, $yposTmp, 'Unit Price');
			$pdf->text($xpos+87, $yposTmp, 'Total Price');

			if ($data >=1){
			$spacing = 1;
			$yposTmp +=3;		
				foreach ($data->result_array() as $row){
				$spacing = 1;
				$yposTmp +=3;
				$pdf->SetFont('Arial','I',6);
				$text=$row['nama_produk'];
				$nb = $pdf->WordWrap($text, ($area_content/2) - ($margin * 2));
				if($nb > 1){
					$x = 4;
					$y = 60;
					for($i = 0; $i <= $nb - 1; $i++) {
						$pdf->text($xpos, $yposTmp, trim(substr($text, $x,$y)));
						$yposTmp += $spacing;
						$x += $y;
					}
				}else{
					
					$pdf->text($xpos, $yposTmp, $text);
					$yposTmp += $spacing;
				}
					//$pdf->text($xpos, $yposTmp, $text=$row['nama_produk'], 0, 0, 'L');
					$pdf->text($xpos+64, $yposTmp-2, $text=$row['jumlah'], 0, 0, 'C');
					$pdf->text($xpos+72, $yposTmp-2, ''.rupiah($row['harga_produk']).'', 0, 0, 'R');
					$pdf->text($xpos+89, $yposTmp-2, ''.rupiah($row['harga_jual']).'', 0, 0, 'R');
					$pdf->SetFont('Arial','',7);
				}
			}else{
	
				foreach ($data->result_array() as $row){
				$spacing = 1;
				$yposTmp +=6;
				$pdf->SetFont('Arial','I',6);
				$text=$row['nama_produk'];
				$nb = $pdf->WordWrap($text, ($area_content/2) - ($margin * 2));
				if($nb > 1){
					$x = 4;
					$y = 60;
					for($i = 0; $i <= $nb - 1; $i++) {
						$pdf->text($xpos, $yposTmp, trim(substr($text, $x,$y)));
						$yposTmp += $spacing;
						$x += $y;
					}
				}else{
					
					$pdf->text($xpos, $yposTmp, $text);
					$yposTmp += $spacing;
				}
					//$pdf->text($xpos, $yposTmp, $text=$row['nama_produk'], 0, 0, 'L');
					$pdf->text($xpos+64, $yposTmp-1, $text=$row['jumlah'], 0, 0, 'C');
					$pdf->text($xpos+72, $yposTmp-1, ''.rupiah($row['harga_produk']).'', 0, 0, 'R');
					$pdf->text($xpos+89, $yposTmp-1, ''.rupiah($row['harga_jual']).'', 0, 0, 'R');
				$pdf->SetFont('Arial','',7);
				}
			}
					
			$yposTmp += 35;
			$pdf->text($xpos+2, $yposTmp, 'Notes : ');
			$pdf->SetFont('Arial','',7);
			$pdf->text($xpos+56, $yposTmp, 'Subtotal ');
			$pdf->text($xpos+56, $yposTmp+5, 'Discount Topsonia ');
			$pdf->text($xpos+56, $yposTmp+10, 'Shipping ');
			$pdf->text($xpos+56, $yposTmp+15, 'Discount Shipping ');
			$pdf->SetFont('Arial','B',7);
			$pdf->text($xpos+89, $yposTmp, 'Rp. '.rupiah($sub_total->row_array()['total_bayar']).'');
			$pdf->text($xpos+89, $yposTmp+10, 'Rp. '.rupiah($semua->row_array()['ongkir']).'');	
			$pdf->SetTextColor(255, 0, 0);
			$pdf->text($xpos+88, $yposTmp+5, '(Rp. '.rupiah($semua->row_array()['diskon']).')');
			$pdf->text($xpos+88, $yposTmp+15, '(Rp. '.rupiah($semua->row_array()['diskon_ongkir']).')');
			
			$pdf->SetFont('Arial','B',10);
			$pdf->SetTextColor(0, 0, 0);
			$pdf->text($xpos+74, $yposTmp+25, 'Rp. '.rupiah($sub_total->row_array()['total_bayar']-$semua->row_array()['diskon']+$semua->row_array()['ongkir']-$semua->row_array()['diskon_ongkir']).'');
			$pdf->SetFont('Arial','I',7);
			$pdf->SetTextColor(0, 0, 0);
	
			$ypos += 5;
			$pdf->Ln();
			$pdf->setX($xpos + 20);
			$pdf->write(5, '');
	
			//Generate PDF
			$pdf->Output("Invoice(".$data->row_array()['kode_transaksi']."-".$idp.")",'I');
	}

	public function print_label(){
		$idp = $this->uri->segment(3);
		$papper = $this->uri->segment(4);
		
		$query = "SELECT pj.kode_transaksi, pj.id_penjualan, pj.waktu_order, pj.service, pj.ongkir, pj.kurir, pj.awb, pj.asuransi, pj.diskon_ongkir, rs.nama_reseller, rs.nama_reseller as nama_merchant, rs.kota_id as id_kota_merchant, kt1.nama_kota as kota_merchant, pr1.nama_provinsi as prov_merchant, rs.alamat_lengkap as alamat_merchant, rs.kode_pos as zip_merchant, rs.no_telpon as telp_merchant, al.pic as nama_konsumen, al.alamat_lengkap as alamat_konsumen, al.kecamatan as kecamatan, al.kota_id as id_kota_konsumen, kt2.nama_kota as kota_konsumen, pr2.nama_provinsi as prov_konsumen, al.kode_pos as zip_agen, al.no_hp as telp_konsumen, dt.jumlah, dt.jumlah, ";
		$query .= "pj.total_bayar as TotalBayar, SUM(dt.jumlah) as TotalBarang, SUM(pd.berat) as TotalBerat ";
		$query .= "FROM rb_penjualan pj ";
		$query .= "LEFT JOIN rb_reseller rs ON pj.id_penjual=rs.id_reseller ";
		$query .= "LEFT JOIN rb_kota kt1 ON rs.kota_id=kt1.kota_id ";
		$query .= "LEFT JOIN rb_provinsi pr1 ON kt1.provinsi_id=pr1.provinsi_id ";
		$query .= "LEFT JOIN rb_konsumen km ON pj.id_pembeli=km.id_konsumen ";
		$query .= "LEFT JOIN alamat al ON pj.id_alamat=al.id_alamat  ";
		$query .= "LEFT JOIN rb_kota kt2 ON al.kota_id=kt2.kota_id ";
		$query .= "LEFT JOIN rb_provinsi pr2 ON al.provinsi_id=pr2.provinsi_id  ";
		$query .= "LEFT JOIN rb_penjualan_detail dt ON pj.id_penjualan=dt.id_penjualan ";
		$query .= "LEFT JOIN rb_produk pd ON dt.id_produk=pd.id_produk ";
		$query .= "WHERE pj.id_penjualan='".$idp."' ";
		$query .= "GROUP BY pj.id_penjualan ";
		
		$data = $this->db->query($query);
		$ongkir = $this->db->query("SELECT sum(ongkir+asuransi)-diskon_ongkir as total FROM rb_penjualan WHERE id_penjualan='".$idp."'");
		$total = $this->db->query("SELECT sum((a.harga_jual*a.jumlah)-a.diskon) as total_bayar FROM `rb_penjualan_detail` a JOIN rb_penjualan b ON a.id_penjualan=b.id_penjualan where b.id_penjualan='".$idp."'");	
		$refid = $data->row_array()['kode_transaksi'].'-'.$data->row_array()['id_penjualan'];

		if($papper == '3'){
			$pdf = new FPDF('P','mm',array(210,99)); //L = lanscape P= potrait
			// membuat halaman baru
			$pdf->AddPage("L", array(210,99));
			$w = $pdf->GetPageWidth(); // Width of Current Page
			$h = $pdf->GetPageHeight(); // Height of Current Page

			$xpos = 5;
			$ypos = 5;
			$margin = 5;
			$area_content = $w - ($xpos*2);
			$h_content = $h - ($ypos);
			$mx = $xpos + $margin;
			$my = $ypos + $margin;

			// garis
			$pdf->Line($xpos,$ypos, $xpos + $area_content, $ypos);
			$pdf->Line($xpos,$ypos + 20, $xpos + $area_content, $ypos + 20);
			$pdf->Line($xpos,$ypos,$xpos,$h_content);
			$pdf->Line($xpos + ($area_content/2),$ypos,$xpos + ($area_content/2),$h_content);
			$pdf->Line($xpos + $area_content,$ypos,$xpos + $area_content,$h_content);
			$pdf->Line($xpos,$h_content,$xpos + $area_content,$h_content);

			//Header Left
			$xpos = $mx;
			$ypos = $my;
			$pdf->setXY($xpos,$ypos);

			$pdf->Image(base_url().'asset/logo/logo_topsonia22.png', $xpos, $ypos + 1, 35, 0, 'png', 'https://topsonia.com/');
			$pdf->Image(base_url().'asset/logo/ncslogonobg.png', $xpos + ($area_content/2) - 15 - $xpos, $ypos, 15, 0, 'png', 'http://ncskurir.com//');

			//Header Right
			$pdf->SetFont('Arial','B',20);
			$pdf->setXY($xpos + ($area_content/2), $ypos + 4);
			$pdf->Cell(60, 3, substr($data->row_array()['service'], 0, 3), 0, 0, 'L' );

			$pdf->SetFont('Arial','',7);
			$pdf->setXY($xpos + $area_content - 70, $ypos - 2);
			$pdf->Cell(60, 3, 'No Pengiriman /  AWB :', 0, 0, 'L' );
			$ypos += 1.5;
			$pdf->Barcode128($xpos + $area_content - 70,$ypos,$data->row_array()['awb'],60,8);
			$ypos += 9;
			$pdf->SetFont('Arial','',9);
			$pdf->SetTextColor(100, 0, 0);
			$pdf->setXY($xpos + ($area_content/2), $ypos);
			$pdf->Cell(($area_content/2) - 10, 3, $data->row_array()['awb'], 0, 0, 'R' );
			$pdf->SetTextColor(0, 0, 0);

			$pdf->setXY($xpos, $ypos + 10);
			$ypos += 10;
			$yposTmp = $ypos;

			//Area Left
			$pdf->setXY($xpos,$ypos);

			$pdf->text($xpos, $yposTmp, 'Transaksi No/Refference No');
			$pdf->text($xpos + 41, $yposTmp, ': ' .$refid.'');
			$yposTmp += 4;
			$pdf->text($xpos, $yposTmp, 'Waktu Order');
			$pdf->text($xpos + 41, $yposTmp, ': '.$data->row_array()['waktu_order'].'');
			$yposTmp += 4;
			// $pdf->text($xpos, $yposTmp, 'Total Belanja');
			// $pdf->text($xpos + 20, $yposTmp, ': Rp. '.$data->row_array()['TotalBayar'].'');

			// $pdf->SetFont('Arial','B',9);
			// $yposTmp += 6;
			// $pdf->text($xpos, $yposTmp, 'PENGIRIM');

			// $pdf->SetFont('Arial','',8);
			// $yposTmp += 4.5;
			// $pdf->setXY($xpos, $yposTmp - 1);
			// $pdf->Write(1,$data->row_array()['nama_merchant']);
			// // $pdf->text($xpos, $yposTmp, $data->row_array()['nama_merchant'] );
			// $yposTmp += 3.5;
			// $pdf->setY($yposTmp - 1);
			// $text=$data->row_array()['alamat_merchant'];
			// $nb = $pdf->WordWrap($text, 90);
			// // $pdf->text($xpos, $yposTmp, $data->row_array()['alamat_merchant'] );
			// $pdf->Write(3,$text);
			// $spacing = 0;
			// if($nb > 1){
			// $spacing = 3.5 * $nb;
			// }else{
			// $spacing = 3.5;
			// }
			// $yposTmp += $spacing  ;
			// $pdf->setY($yposTmp);
			// $pdf->Write(1,$data->row_array()['zip_merchant']);
			// // $pdf->text($xpos, $yposTmp, $data->row_array()['zip_merchant'] );
			// $yposTmp += 3.5;
			// $pdf->setY($yposTmp);
			// $pdf->Write(1,$data->row_array()['telp_merchant']);

			//PENGIRIM
			$spacing = 3.5;
			$yposTmp += 6;
			$pdf->SetFont('Arial','B',8);
			$pdf->text($xpos, $yposTmp, 'PENGIRIM');

			$yposTmp += 5;
			$pdf->SetFont('Arial','',8);
			$pdf->text($xpos, $yposTmp, $data->row_array()['nama_merchant'] );

			$yposTmp += $spacing;
			$text=$data->row_array()['prov_merchant'];
			$nb = $pdf->WordWrap($text, ($area_content/2) - ($margin * 2));
			if($nb > 1){
				$x = 0;
				$y = 67;
				for($i = 0; $i <= $nb - 1; $i++) {
					$pdf->text($xpos, $yposTmp, trim(substr($text, $x,$y)));
					$yposTmp += $spacing;
					$x += $y;
				}
			}else{
				$pdf->text($xpos, $yposTmp, $text);
				$yposTmp += $spacing;
			}

			// if($data->row_array()['zip_merchant'] != ''){
				// $pdf->text($xpos, $yposTmp, $data->row_array()['zip_merchant'] );
				// $yposTmp += $spacing;
			// }

			$pdf->text($xpos, $yposTmp, $data->row_array()['telp_merchant']);

			//PENERIMA
			$pdf->SetFont('Arial','B',8);
			$yposTmp += 8;
			$pdf->text($xpos, $yposTmp, 'PENERIMA');

			$yposTmp += 5;
			$pdf->SetFont('Arial','',8);
			$pdf->text($xpos, $yposTmp, $data->row_array()['nama_konsumen'] );

			$yposTmp += $spacing;
			$text=$data->row_array()['alamat_konsumen'].' '.$data->row_array()['prov_konsumen'].' '.$data->row_array()['kecamatan'].' '.$data->row_array()['kota_konsumen'];
			$nb = $pdf->WordWrap($text, $area_content);
			if($nb > 1){
				$x = 0;
				$y = 50;
				for($i = 0; $i <= $nb - 1; $i++) {
					$pdf->text($xpos, $yposTmp, substr($text, $x,$y));
					$yposTmp += $spacing;
					$x += $y;
				}
			}else{
				$pdf->text($xpos, $yposTmp, $text);
				$yposTmp += $spacing;
			}

			if($data->row_array()['zip_agen'] != ''){
				$pdf->text($xpos, $yposTmp, $data->row_array()['zip_agen'] );
				$yposTmp += $spacing;
			}

			$pdf->text($xpos, $yposTmp, $data->row_array()['telp_konsumen'] );

			//Area Right
			$yposTmp = $ypos;
			$xposTmp = $xpos + ($area_content/2);


			$pdf->SetFont('Arial','B',9);
			$pdf->text($xposTmp, $yposTmp, 'RINCIAN PENGIRIMAN');
			$yposTmp += 4.5;

			$pdf->SetFont('Arial','',8);
			$pdf->text($xposTmp, $yposTmp, 'Ongkos Kirim');
			$pdf->text($xposTmp + 20, $yposTmp, ': Rp. '.rupiah($data->row_array()['ongkir']).'');
			$yposTmp += 4;
			$pdf->text($xposTmp, $yposTmp, 'Asuransi');
			$pdf->text($xposTmp + 20, $yposTmp, ': Rp. '.rupiah($data->row_array()['asuransi']).'');
			$yposTmp += 4;
			$pdf->SetTextColor(100, 0, 0);
			$pdf->text($xposTmp, $yposTmp, 'Diskon Ongkir');
			$pdf->text($xposTmp + 20, $yposTmp, ': - (Rp. '.rupiah($data->row_array()['diskon_ongkir']).')');
			$yposTmp += 4;
			$pdf->SetTextColor(0, 0, 0);
			$pdf->text($xposTmp, $yposTmp, 'Total Biaya');
			$pdf->SetTextColor(0, 0, 200);
			$pdf->text($xposTmp + 20, $yposTmp, ': Rp. '.rupiah($ongkir->row_array()['total']).'');
			$pdf->SetTextColor(0, 0, 0);

			$yposTmp += 6;
			$pdf->text($xposTmp, $yposTmp, 'Jumlah');
			$pdf->text($xposTmp + 20, $yposTmp, ': '.$data->row_array()['TotalBarang'].' '.$data->row_array()['satuan']);
			$yposTmp += 4;
			$pdf->text($xposTmp, $yposTmp, 'Berat');
			$pdf->text($xposTmp + 20, $yposTmp, ': '.$data->row_array()['TotalBerat'].' gram');
			$yposTmp += 4;
			$pdf->text($xposTmp, $yposTmp, 'Service');
			$pdf->SetTextColor(0, 0, 200);
			$pdf->text($xposTmp + 20, $yposTmp, ': '.$data->row_array()['service'].'');
			$pdf->SetTextColor(0, 0, 0);

			$yposTmp += 6;
			$pdf->text($xposTmp, $yposTmp, 'Keterangan');
			$pdf->text($xposTmp + 20, $yposTmp, ':');

			$yposTmp +=8;
			$pdf->SetLineWidth(0.7);
			$pdf->Line($xposTmp,$yposTmp, $area_content, $yposTmp);

			$yposTmp += 4;
			$pdf->text($xposTmp, $yposTmp, 'Penting');


			$ypos += 5;
			$pdf->Ln();
			$pdf->setX($xpos + 20);
			$pdf->write(5, '');
		}elseif($papper == '6'){
			$pdf = new FPDF('P','mm',array(105,99)); //L = lanscape P= potrait
			// membuat halaman baru
			$pdf->AddPage("P", array(105,99));
			$w = $pdf->GetPageWidth(); // Width of Current Page
			$h = $pdf->GetPageHeight(); // Height of Current Page

			// $pdf->SetMargins(50,50,50);
			$xpos = 5;
			$ypos = 5;
			$margin = 3;
			$area_content = $w - ($xpos*2);
			$h_content = $h - ($ypos);
			$mx = $xpos + $margin;
			$my = $ypos + $margin;



			// garis
			$pdf->Line($xpos,$ypos, $xpos + $area_content, $ypos);
			$pdf->Line($xpos,$ypos + 15, $xpos + $area_content, $ypos + 15);
			$pdf->Line($xpos,$ypos,$xpos,$h_content);
			$pdf->Line($xpos + $area_content,$ypos,$xpos + $area_content,$h_content);
			$pdf->Line($xpos,$h_content,$xpos + $area_content,$h_content);

			//Header Left
			$xposTmp = $mx;
			$yposTmp = $my;
			$pdf->setXY($xpos,$ypos);

			$wLogoTopsonia = 25;
			$pdf->Image(base_url().'asset/logo/logo_topsonia22.png', $xposTmp, $yposTmp + 2, $wLogoTopsonia , 0, 'png', 'https://topsonia.com/');

			//Header Mid
			$pdf->SetFont('Arial','B',12);
			$pdf->setXY($xpos, $ypos + 6);
			$pdf->Cell($area_content, 3, substr($data->row_array()['service'], 0, 3), 0, 0, 'C' );

			//Header Right
			$wLogoNCS = 14;
			$xposTmp = $xpos + $area_content - $margin - $wLogoNCS;
			$pdf->Image(base_url().'asset/logo/ncslogonobg.png', $xposTmp, $yposTmp - 0, $wLogoNCS, 0, 'png', 'http://ncskurir.com//');

			$yposTmp += 17;

			//Barcode
			$wBarcode = $area_content - ($margin * 2);
			$xposTmp = $my;
			$pdf->SetFont('Arial','',8);
			$pdf->setXY($xposTmp - 1, $yposTmp - 3);
			$pdf->Cell($wBarcode, 3, 'No Pengiriman /  AWB :', 0, 0, 'L' );

			$pdf->SetFont('Arial','',9);
			$pdf->SetTextColor(100, 0, 0);
			$pdf->setXY($xposTmp, $yposTmp - 3);
			$pdf->Cell($wBarcode, 3, $data->row_array()['awb'], 0, 0, 'R' );
			$pdf->SetTextColor(0, 0, 0);

			$yposTmp += 1.5;
			$pdf->Barcode128($xposTmp,$yposTmp,$data->row_array()['awb'],$wBarcode,12);


			//Transaksi No
			$yposTmp += 16;
			$pdf->setXY($xposTmp, $yposTmp);
			$pdf->SetFont('Arial','',8);
			$pdf->text($xposTmp, $yposTmp, 'Transaksi No/Refference No');
			$pdf->text($xposTmp + 40, $yposTmp, ': '.$refid.'');

			$spacing = 4;

			//PENGIRIM
			$yposTmp += 6;
			$pdf->SetFont('Arial','B',8);
			$pdf->text($xposTmp, $yposTmp, 'PENGIRIM');

			$yposTmp += $spacing;
			$pdf->SetFont('Arial','',8);
			$pdf->text($xposTmp, $yposTmp, $data->row_array()['nama_merchant'] );

			$yposTmp += $spacing;
			$text=$data->row_array()['prov_merchant'].'';
			// $data->row_array()['zip_merchant'];
			$nb = $pdf->WordWrap($text, $area_content);
			if($nb > 1){
				$x = 0;
				$y = 60;
				for($i = 0; $i <= $nb - 1; $i++) {
					$pdf->text($xposTmp, $yposTmp, substr($text, $x,$y));
					$yposTmp += $spacing;
					$x += $y;
				}
			}else{
				$pdf->text($xposTmp, $yposTmp, $text);
				$yposTmp += $spacing;
			}

			$pdf->text($xposTmp, $yposTmp, $data->row_array()['telp_merchant']);

			//PENERIMA
			$yposTmp += 7;
			$pdf->SetFont('Arial','B',8);
			$pdf->text($xposTmp, $yposTmp, 'PENERIMA');

			$yposTmp += $spacing;
			$pdf->SetFont('Arial','',8);
			$pdf->text($xposTmp, $yposTmp, $data->row_array()['nama_konsumen'] );

			$yposTmp += $spacing;
			$text=$data->row_array()['alamat_konsumen'].' '.$data->row_array()['prov_konsumen'].' '.$data->row_array()['kecamatan'].' '.$data->row_array()['kota_konsumen'].' '.$data->row_array()['zip_agen'];
			$nb = $pdf->WordWrap($text, $area_content);
			if($nb > 1){
				$x = 0;
				$y = 50;
				for($i = 0; $i <= $nb - 1; $i++) {
					$pdf->text($xposTmp, $yposTmp, substr($text, $x,$y));
					$yposTmp += $spacing;
					$x += $y;
				}
			}else{
				$pdf->text($xposTmp, $yposTmp, $text);
				$yposTmp += $spacing;
			}

			$pdf->text($xposTmp, $yposTmp, $data->row_array()['telp_konsumen']);

			//Detail Transaksi
			$yposTmp +=6;
			$pdf->SetLineWidth(0.5);
			$pdf->Line($xposTmp,$yposTmp, $area_content, $yposTmp);

			$yposTmp += 4.5;
			$pdf->text($xposTmp, $yposTmp, 'Jumlah');
			$pdf->text($xposTmp + 13, $yposTmp, ': '.$data->row_array()['TotalBarang'].' '.$data->row_array()['satuan']);

			$pdf->text($xposTmp + ($area_content / 2) - 10, $yposTmp, 'Ongkos Kirim');
			$pdf->SetTextColor(0, 0, 200);
			$pdf->text($xposTmp + ($area_content / 2) + 9, $yposTmp, ': Rp. '.rupiah($ongkir->row_array()['total']).'');
			$pdf->SetTextColor(0, 0, 0);

			$yposTmp += 4;
			$pdf->text($xposTmp, $yposTmp, 'Berat');
			$pdf->text($xposTmp + 13, $yposTmp, ': '.$data->row_array()['TotalBerat'].' gram');
			$pdf->text($xposTmp + ($area_content / 2) - 10, $yposTmp, 'Asuransi');
			$pdf->text($xposTmp + ($area_content / 2) + 3, $yposTmp, ': Rp. '.rupiah($data->row_array()['asuransi']).'');
		}else{
			$pdf = new FPDF('L','mm',array(105,49.5)); //L = lanscape P= potrait
			// membuat halaman baru
			$pdf->AddPage("L", array(105,49.5));
			$w = $pdf->GetPageWidth(); // Width of Current Page
			$h = $pdf->GetPageHeight(); // Height of Current Page

			// $pdf->SetMargins(50,50,50);
			$xpos = 5;
			$ypos = 5;
			$margin = 3;
			$area_content = $w - ($xpos*2);
			$h_content = $h - ($ypos);
			$mx = $xpos + $margin;
			$my = $ypos + $margin;



			// garis
			$pdf->Line($xpos,$ypos, $xpos + $area_content, $ypos);
			$pdf->Line($mx,$ypos + 10, $area_content, $ypos + 10);
			$pdf->Line($xpos,$ypos,$xpos,$h_content);
			// $pdf->Line($xpos + ($area_content/2),$ypos,$xpos + ($area_content/2),$h_content);
			$pdf->Line($xpos + $area_content,$ypos,$xpos + $area_content,$h_content);
			$pdf->Line($xpos,$h_content,$xpos + $area_content,$h_content);

			//Header Left
			$xposTmp = $mx;
			$yposTmp = $my;
			$pdf->setXY($xpos,$ypos);

			$wLogoTopsonia = 20;
			$pdf->Image(base_url().'asset/logo/logo_topsonia22.png', $xposTmp, $yposTmp , $wLogoTopsonia , 0, 'png', 'https://topsonia.com/');

			//Header Mid
			$pdf->SetFont('Arial','B',11);
			$pdf->setXY($xpos, $ypos + 4);
			$pdf->Cell($area_content, 3, substr($data->row_array()['service'], 0, 3), 0, 0, 'C' );

			//Header Right
			$wLogoNCS = 10;
			$xposTmp = $xpos + $area_content - $xpos - $wLogoNCS;
			$pdf->Image(base_url().'asset/logo/ncslogonobg.png', $xposTmp, $yposTmp - 1, $wLogoNCS, 0, 'png', 'http://ncskurir.com//');

			$yposTmp += 13;

			//Barcode
			$wBarcode = $area_content - ($margin * 2);
			$xposTmp = $my;
			$pdf->SetFont('Arial','',7);
			$pdf->setXY($xposTmp, $yposTmp - 3);
			$pdf->Cell($wBarcode, 3, 'No Pengiriman /  AWB :', 0, 0, 'L' );


			$pdf->SetFont('Arial','',9);
			$pdf->SetTextColor(100, 0, 0);
			$pdf->setXY($xposTmp, $yposTmp - 3);
			$pdf->Cell($wBarcode, 3, $data->row_array()['awb'], 0, 0, 'R' );
			$pdf->SetTextColor(0, 0, 0);

			$yposTmp += 1.5;
			$pdf->Barcode128($xposTmp,$yposTmp,$data->row_array()['awb'],$wBarcode,12);
			$yposTmp += 18;

			$pdf->setXY($xposTmp, $yposTmp);
			$pdf->SetFont('Arial','',8);
			$pdf->text($xposTmp, $yposTmp, 'Transaksi No/Refference No');
			$pdf->text($xposTmp + 40, $yposTmp, ': '.$refid.'');
		}

		//Generate PDF
		$pdf->Output("Label(".$data->row_array()['kode_transaksi']."-".$idp.")",'I');
	}

	function pesan(){
		$user	= $this->db->query("select a.*, b.nama_lengkap from chat a join rb_konsumen b on a.id_pembeli=b.id_konsumen where id_penjual ='".$this->session->id_reseller."' and a.del!=2 group by id_pembeli order by id_chat");
		
		$data['user'] = $user;
		$this->template->load($this->uri->segment(1).'/template',$this->uri->segment(1).'/pesan/menu_pesan',$data);
	}

	function chat(){
		
		$id_pembeli	= $this->input->post('id_pembeli'); //tujuan
		$id_max		= '10'; //dari

		$chat	= $this->db->query("select a.*, b.nama_lengkap from chat a join rb_konsumen b on a.id_pembeli=b.id_konsumen where a.id_penjual='".$this->session->id_reseller."' AND a.id_pembeli ='".$id_pembeli."' and a.del!=2 order by id_chat");
		$this->model_app->update('chat',array("flag"=>1),array("id_penjual"=>$this->session->id_reseller,"id_pembeli"=>$id_pembeli,"ket"=>"1","flag"=>0));
		
		$data['id_max']		= $id_max;
		$data['id_pembeli']	=$id_pembeli;
		$data['chat'] 		= $chat;
		$this->load->view($this->uri->segment(1)."/pesan/chat_seller.php",$data);

	}
	
	function chat_konsumen()
	{

		$id_pembeli	= $this->input->post('id_konsumen'); //tujuan
		$id_max		= '10'; //dari

		$chat	= $this->db->query("select a.*, b.nama_lengkap from chat a join rb_konsumen b on a.id_pembeli=b.id_konsumen where a.id_penjual='" . $this->session->id_reseller . "' AND a.id_pembeli ='" . $id_pembeli . "' and a.del!=2 order by id_chat");
		$this->model_app->update('chat', array("flag" => 1), array("id_penjual" => $this->session->id_reseller, "id_pembeli" => $id_pembeli, "ket" => "1", "flag" => 0));

		$data['id_max']		= $id_max;
		$data['id_pembeli']	= $id_pembeli;
		$data['chat'] 		= $chat;
		$this->load->view($this->uri->segment(1) . "/pesan/chat_konsumen.php", $data);
	}

	function kirim_pesan(){

		$id_pembeli	= $this->input->post("id_pembeli"); //tujuan/user_2
		$pesan		= $this->input->post("pesan");

		if ($this->input->post("kode_trx") != '') {
			if ($this->session->kode != '') {
				if ($this->session->kode != $this->input->post("kode_trx")) {

					$pesan_2 = "<a style='color:blue' href='" . base_url() . "members/transaksi_detail/" . $this->input->post("kode_trx") . "'  target='_blank'>Detail Transaksi " . $this->input->post("kode_trx") . "</a>";
					$data2	= array(
						'id_penjual' =>  $this->session->id_reseller,
						'id_pembeli' => $id_pembeli,
						'pesan' => $pesan_2,
						// 'pesan' => "www.youtube.com",
						'flag' => 0,
						'ket' => 2,
						'del' => 0
					);
					$this->session->set_userdata(array('kode' => $this->input->post("kode_trx")));
					$this->model_app->insert('chat', $data2);
					$this->session->set_userdata(array('prod' => '2'));
				}
			} else {
				$pesan_2 = "<a style='color:blue' href='" . base_url() . "members/transaksi_detail/" . $this->input->post("kode_trx") . "'  target='_blank'>Detail Transaksi " . $this->input->post("kode_trx") . "</a>";
				$data2	= array(
					'id_penjual' =>  $this->session->id_reseller,
					'id_pembeli' => $id_pembeli,
					'pesan' => $pesan_2,
					// 'pesan' => "www.youtube.com",
					'flag' => 0,
					'ket' => 2,
					'del' => 0
				);
				$this->session->set_userdata(array('kode' => $this->input->post("kode_trx")));
				$this->model_app->insert('chat', $data2);
				$this->session->set_userdata(array('prod' => '2'));
			}
		}
		$data	= array(
			'id_penjual' => $this->session->id_reseller,
			'id_pembeli' => $id_pembeli,
			'pesan' => $pesan,
			'flag' => 0,
			'ket' => 2,
			'del' => 0
		);
		$query	=	$this->model_app->insert('chat', $data);


		if ($query) {
			$rs = 1;
		} else {
			$rs	= 2;
		}

		echo json_encode(array("result" => $rs));
			
	}

		function hapus_pesan(){
			$id_pembeli	= $this->input->post("id_pembeli"); //tujuan/user_2

				$this->model_app->update('chat',array("del"=>2),array("id_pembeli"=>$id_pembeli,"id_penjual"=>$this->session->id_reseller,"del"=>'0'));
			
				$this->db->query("delete from chat where id_penjual ='".$this->session->id_reseller."' and id_pembeli='".$id_pembeli."' and del='1'");
			
			
			$data = array("bisa"=>"1");
			echo json_encode($data);
		}

	function pesan_admin(){
		$user	= $this->db->query("select * from chat_admin where id_penjual ='".$this->session->id_reseller."' and del!=2 group by id_penjual order by id_chat");
		
		$data['user'] = $user;
		$this->template->load($this->uri->segment(1).'/template',$this->uri->segment(1).'/pesan/admin',$data);
	}

	function chat_admin(){
		
		$id_max		= '10'; //dari
		$chat	= $this->db->query("select * from chat_admin where id_penjual='".$this->session->id_reseller."' and del!=2 order by id_chat");
		$this->model_app->update('chat_admin',array("flag"=>1),array("id_penjual"=>$this->session->id_reseller,"ket"=>"1","flag"=>0));
		
		$data['id_max']		= $id_max;
		$data['chat'] 		= $chat;
		$this->load->view($this->uri->segment(1)."/pesan/chat_admin.php",$data);

	}

	function kirim_pesan_admin(){
			$pesan		= $this->input->post("pesan");
			
			$data	= array(
				'id_penjual' => $this->session->id_reseller,
				'pesan' => $pesan,
				'flag' => 0,
				'ket' => 2,
				'del' =>0
			);
			
			$query	=	$this->model_app->insert('chat_admin',$data);
			
			if($query){
				$rs = 1;
			}else{
				$rs	= 2;
			}
			
			echo json_encode(array("result"=>$rs));
			
	}

	function hapus_pesan_admin(){
	
		$this->model_app->update('chat_admin',array("del"=>'2'),array("id_penjual"=>$this->session->id_reseller,"del"=>'0'));
		$this->db->query("delete from chat_admin where id_penjual='".$this->session->id_reseller."' and del='1'");			
		$data = array("bisa"=>"1");
		echo json_encode($data);
			
	}

	function save_cod(){
		$cod = $this->input->post('cod') ;
		if($cod!=''){
			$this->model_app->update('rb_reseller',array("cod"=>$cod),array("id_reseller"=>$this->session->id_reseller));
			if($this->db->affected_rows()>0){
				$data['sukses'] = 1;
			}else{
				$data['sukses'] = 0;
			}
			echo json_encode($data);
		}
		
	}

	function logout(){
		$this->session->sess_destroy();
		
		redirect($this->uri->segment(1).'/index');
	}
}
