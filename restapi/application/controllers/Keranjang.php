<?php
// defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class keranjang extends REST_Controller
{

	function __construct()
	{
		parent::__construct();
	}

	public function add_post()
	{
			$id_produk = $this->post('produk');
			$id_konsumen = $this->post('id_konsumen');
			$qty = $this->post('qty');

			$j = $this->model_app->jual_reseller($id_produk)->row_array();
			$b = $this->model_app->beli_reseller($id_produk)->row_array();
			$stok = $b['beli'] - $j['jual'];

			if ($qty > $stok) {
				$return = [
					'status_code' => 500,
					'message' => 'Erro Jumlah Quantity Melebihi Stock'
				];
			} else {
				$id_reseller = $this->db->query("select id_reseller from rb_produk where id_produk=$id_produk")->row_array()['id_reseller'];

				$cek_j_seller = $this->db->query("SELECT b.id_reseller FROM `rb_penjualan_temp` a join rb_produk b on a.id_produk=b.id_produk  where a.session='" . $id_konsumen . "' group by b.id_reseller ");
				if ($cek_j_seller->num_rows() >= 5) {
					$cek5 = $this->db->query("SELECT b.id_reseller FROM `rb_penjualan_temp` a join rb_produk b on a.id_produk=b.id_produk  where a.session='" . $id_konsumen . "' and b.id_reseller='" . $id_reseller . "'");
					if ($cek5->num_rows() < 1) {
						$return = [
							'status_code' => 500,
							'message' => '! Transaksi Maksimal 5 Toko'
						];
					} else {
						
						$seller = $this->db->query("SELECT id_penjual FROM temp_penjualan where id_pembeli='" . $id_konsumen . "' and id_penjual='" . $id_reseller . "'");
						if ($seller->num_rows() == 0) {
							$data2 = array(
							'id_pembeli' => $id_konsumen,
							'id_penjual' => $id_reseller,
							'waktu_transaksi' => date('Y-m-d H:i:s')
						);
						$this->db->insert('temp_penjualan', $data2);
						}
						
						$checkout = $this->db->query("SELECT b.id_reseller FROM `rb_penjualan_temp` a join rb_produk b on a.id_produk=b.id_produk  where a.session='" . $id_konsumen . "' and a.id_produk='$id_produk'");
						if ($checkout->num_rows() == 0) {
							$harga = $this->db->query("select satuan,harga_konsumen from rb_produk where id_produk=$id_produk")->row_array();
							$disk = $this->db->query("select diskon from rb_produk_diskon where id_produk=$id_produk")->row_array();
							$harga_konsumen = $harga['harga_konsumen'] - $disk['diskon'];
							$data = array(
								'session' => $id_konsumen,
								'id_produk' => $id_produk,
								'jumlah' => $qty,
								'harga_jual' => $harga_konsumen,
								'satuan' => $harga['satuan'],
								'diskon' => 0,
								'waktu_order' => date('Y-m-d H:i:s')
							);
							$this->db->insert('rb_penjualan_temp', $data);
						} else {
							$where = array('id_produk' => $id_produk, 'session' => $id_konsumen);
							$data = array('jumlah' => $qty);
							$this->db->update('rb_penjualan_temp', $data, $where);
						}
						$return = [
							'status_code' => 200,
							'message' => 'Berhasil Memasukan Ke keranjang',
						];
					}
				} else {
					$seller = $this->db->query("SELECT id_penjual FROM temp_penjualan where id_pembeli='" . $id_konsumen . "' and id_penjual='" . $id_reseller . "'");
					if ($seller->num_rows() == 0) {
						$data2 = array(
							'id_pembeli' => $id_konsumen,
							'id_penjual' => $id_reseller,
							'waktu_transaksi' => date('Y-m-d H:i:s')
						);
						$this->db->insert('temp_penjualan', $data2);
					}
					
					$checkout = $this->db->query("SELECT b.id_reseller FROM `rb_penjualan_temp` a join rb_produk b on a.id_produk=b.id_produk  where a.session='" . $id_konsumen . "' and a.id_produk='$id_produk'");
					if ($checkout->num_rows() == 0) {
						$harga = $this->db->query("select satuan,harga_konsumen from rb_produk where id_produk=$id_produk")->row_array();
						$disk = $this->db->query("select diskon from rb_produk_diskon where id_produk=$id_produk")->row_array();
						$harga_konsumen = $harga['harga_konsumen'] - $disk['diskon'];
						$data = array(
							'session' => $id_konsumen,
							'id_produk' => $id_produk,
							'jumlah' => $qty,
							'harga_jual' => $harga_konsumen,
							'satuan' => $harga['satuan'],
							'diskon' => 0,
							'waktu_order' => date('Y-m-d H:i:s')
						);
						$this->db->insert('rb_penjualan_temp', $data);
					} else {
						$where = array('id_produk' => $id_produk, 'session' => $id_konsumen);
						$data = array('jumlah' => $qty);
						$this->db->update('rb_penjualan_temp', $data, $where);
					}
					$return = [
						'status_code' => 200,
						'message' => 'Berhasil Memasukan Ke keranjang',
					];
				}
			}
		$this->response($return, REST_Controller::HTTP_OK);
	}

	public function get_keranjang_post()
	{
			$id_konsumen = $this->post('id_konsumen');
			$produk = $this->db->query("SELECT b.gambar,c.nama_reseller,b.nama_produk,b.id_produk,a.diskon,a.harga_jual,a.jumlah,a.satuan,b.berat FROM `rb_penjualan_temp` a join rb_produk b on a.id_produk=b.id_produk join rb_reseller c ON b.id_reseller=c.id_reseller JOIN rb_kota d ON c.kota_id=d.kota_id where a.session='$id_konsumen' ORDER BY c.id_reseller ASC")->result_array();
			$return = [
				'status_code' => 200,
				'message' => 'Success',
				'produk'=>$produk,
			];
		
		$this->response($return, REST_Controller::HTTP_OK);
	}
	
	public function produk_delete_post()
	{
		$id_produk = $this->post('id_produk');
		$id_konsumen = $this->post('id_konsumen');
		$id = array('id_produk' => $id_produk, 'session'=>$id_konsumen);
		$this->model_app->delete('rb_penjualan_temp',$id);
		$id_reseller = $this->db->query("select id_reseller from rb_produk where id_produk='$id_produk'")->row_array()['id_reseller'];
		$isi_keranjang = $this->db->query("SELECT jumlah FROM rb_penjualan_temp where session='".$id_konsumen."'")->num_rows();
		$isi_seller = $this->db->query("SELECT * FROM `rb_penjualan_temp` a join rb_produk b on a.id_produk=b.id_produk join rb_reseller c ON b.id_reseller=c.id_reseller JOIN rb_kota d ON c.kota_id=d.kota_id where a.session='".$id_konsumen."' and c.id_reseller='".$id_reseller."'")->num_rows();


		if ($isi_seller<1){
			$this->model_app->delete('temp_penjualan',array('id_pembeli'=>$id_konsumen,'id_penjual'=>$id_reseller));
		}
		if ($isi_keranjang<1){
			$this->model_app->delete('temp_penjualan',array('id_pembeli'=>$id_konsumen));
		}
		$return = [
			'status_code' => 200,
			'message' => 'Success',
		];
		$this->response($return, REST_Controller::HTTP_OK);
	}
	
	public function produk_update_post()
	{
		$id_produk = $this->post('id_produk');
		$id_konsumen = $this->post('id_konsumen');
		$qty = $this->post('qty');
		
		$j = $this->model_app->jual_reseller($id_produk)->row_array();
		$b = $this->model_app->beli_reseller($id_produk)->row_array();
		$stok = $b['beli'] - $j['jual'];

			if ($qty > $stok) {
				$return = [
					'status_code' => 500,
					'message' => 'Error Jumlah Quantity Melebihi Stock'
				];
			} else {
				$this->db->query("update rb_penjualan_temp set jumlah='$qty' where id_produk='$id_produk' and session='$id_konsumen'");
				$return = [
					'status_code' => 200,
					'message' => 'Success',
				];
			}
			
		$this->response($return, REST_Controller::HTTP_OK);
	}
}
