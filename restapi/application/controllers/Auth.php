<?php
// defined('BASEPATH') OR exit('No direct script access allowed');

use Firebase\JWT\JWT;

require APPPATH . '/libraries/REST_Controller.php';

class auth extends REST_Controller
{

	function __construct()
	{
		// Construct the parent class
		parent::__construct();

		$this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
		$this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
		$this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
	}

	public function login_post()
	{
		// return Validation::validate($this, 'keys', 'key_top', function ($token, $output) {
			$username = strip_tags($this->post('username'));
			$password = hash("sha512", md5(strip_tags($this->post('password'))));
			$cek = $this->db->query("SELECT username,nama_lengkap,id_konsumen FROM rb_konsumen where username='" . $this->db->escape_str($username) . "' AND password='" . $this->db->escape_str($password) . "'");
			$total = $cek->num_rows();
			if ($total > 0) {
				$token = $cek->row_array();
				$users = [
					'status_code' => 200,
					'message' => 'Login Sukses',
					'token' => JWT::encode('wpu123', $this->config->item('jwt_key')),
					'user' => $cek->row_array()
				];
			} else {
				$users = [
					'status_code' => 500,
					'message' => 'Username Atau Password Salah'
				];
			}
			$this->response($users, REST_Controller::HTTP_OK);
			//return $output;
		// });
	}
}
