<?php
// defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class product extends REST_Controller {

	function __construct()
    {
        parent::__construct();

      	$this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
    }

	public function data_get(){
		
	if ($this->input->get('cari') != null) {
		$car = $this->input->get('cari');
		//$car = 'topi';
		$cari = "and (a.nama_produk LIKE '%" . $car . "%' OR a.keterangan LIKE '%" . $car . "%' OR b.nama_reseller LIKE '%" . $car . "%')";
	} else {
		$cari = null;
	}
	
	if ($this->input->get('kategori') == null || $this->input->get('kategori')==0 ) {
		$kategori = null;
	} else {
		$kate = $this->input->get('kategori');
		//$car = 'topi';
		$kategori = "and a.id_kategori_produk='".$kate."'";
		
	}
		
	$data = $this->db->query("select a.id_produk, a.nama_produk, d.nama_kategori, a.keterangan,a.berat,a.satuan, a.gambar, a.harga_konsumen, c.nama_kota,IFNULL((SELECT sum(1b.jumlah) as beli FROM `rb_penjualan` 1a JOIN rb_penjualan_detail 1b ON 1a.id_penjualan=1b.id_penjualan where 1a.status_pembeli='reseller' AND 1a.status_penjual='admin' AND 1b.id_produk=a.id_produk AND 1a.proses='1'),0) as stock, IFNULL((SELECT sum(2b.jumlah) as jual FROM `rb_penjualan` 2a JOIN rb_penjualan_detail 2b ON 2a.id_penjualan=2b.id_penjualan where 2a.status_pembeli='konsumen' AND 2a.status_penjual='reseller' AND 2b.id_produk=a.id_produk AND 2a.proses='1' and 2a.batal='0'),'0') as terjual from rb_produk a join rb_reseller b on a.id_reseller=b.id_reseller join rb_kota c on b.kota_id=c.kota_id join rb_kategori_produk d on a.id_kategori_produk=d.id_kategori_produk where a.id_produk<>'' $cari $kategori GROUP BY a.id_produk ORDER BY a.id_produk asc limit 10");
		
	$users = ['status_code'=>200,'message'=>'Product List','product'=>$data->result_array()];	
		
	$this->response($users, REST_Controller::HTTP_OK);

	}
	
	public function category_get(){
		
	$data = $this->db->query("select id_kategori_produk,nama_kategori from rb_kategori_produk");
		
	$users = ['status_code'=>200,'message'=>'Category List','category'=>$data->result_array()];	
		
	$this->response($users, REST_Controller::HTTP_OK);

	}
	
	

}
